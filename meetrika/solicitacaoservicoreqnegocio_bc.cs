/*
               File: SolicitacaoServicoReqNegocio_BC
        Description: Requisitos de Neg�cio da Solicita��o de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:45:30.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaoservicoreqnegocio_bc : GXHttpHandler, IGxSilentTrn
   {
      public solicitacaoservicoreqnegocio_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaoservicoreqnegocio_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4T214( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4T214( ) ;
         standaloneModal( ) ;
         AddRow4T214( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E114T2 */
            E114T2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4T0( )
      {
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4T214( ) ;
            }
            else
            {
               CheckExtendedTable4T214( ) ;
               if ( AnyError == 0 )
               {
                  ZM4T214( 4) ;
                  ZM4T214( 5) ;
               }
               CloseExtendedTableCursors4T214( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E124T2( )
      {
         /* Start Routine */
      }

      protected void E114T2( )
      {
         /* After Trn Routine */
      }

      protected void ZM4T214( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1915SolicServicoReqNeg_Identificador = A1915SolicServicoReqNeg_Identificador;
            Z1909SolicServicoReqNeg_Nome = A1909SolicServicoReqNeg_Nome;
            Z1911SolicServicoReqNeg_Prioridade = A1911SolicServicoReqNeg_Prioridade;
            Z1912SolicServicoReqNeg_Agrupador = A1912SolicServicoReqNeg_Agrupador;
            Z1913SolicServicoReqNeg_Tamanho = A1913SolicServicoReqNeg_Tamanho;
            Z1914SolicServicoReqNeg_Situacao = A1914SolicServicoReqNeg_Situacao;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z1947SolicServicoReqNeg_ReqNeqCod = A1947SolicServicoReqNeg_ReqNeqCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -3 )
         {
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1915SolicServicoReqNeg_Identificador = A1915SolicServicoReqNeg_Identificador;
            Z1909SolicServicoReqNeg_Nome = A1909SolicServicoReqNeg_Nome;
            Z1910SolicServicoReqNeg_Descricao = A1910SolicServicoReqNeg_Descricao;
            Z1911SolicServicoReqNeg_Prioridade = A1911SolicServicoReqNeg_Prioridade;
            Z1912SolicServicoReqNeg_Agrupador = A1912SolicServicoReqNeg_Agrupador;
            Z1913SolicServicoReqNeg_Tamanho = A1913SolicServicoReqNeg_Tamanho;
            Z1914SolicServicoReqNeg_Situacao = A1914SolicServicoReqNeg_Situacao;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z1947SolicServicoReqNeg_ReqNeqCod = A1947SolicServicoReqNeg_ReqNeqCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4T214( )
      {
         /* Using cursor BC004T6 */
         pr_default.execute(4, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound214 = 1;
            A1915SolicServicoReqNeg_Identificador = BC004T6_A1915SolicServicoReqNeg_Identificador[0];
            A1909SolicServicoReqNeg_Nome = BC004T6_A1909SolicServicoReqNeg_Nome[0];
            A1910SolicServicoReqNeg_Descricao = BC004T6_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = BC004T6_A1911SolicServicoReqNeg_Prioridade[0];
            A1912SolicServicoReqNeg_Agrupador = BC004T6_A1912SolicServicoReqNeg_Agrupador[0];
            A1913SolicServicoReqNeg_Tamanho = BC004T6_A1913SolicServicoReqNeg_Tamanho[0];
            A1914SolicServicoReqNeg_Situacao = BC004T6_A1914SolicServicoReqNeg_Situacao[0];
            A456ContagemResultado_Codigo = BC004T6_A456ContagemResultado_Codigo[0];
            A1947SolicServicoReqNeg_ReqNeqCod = BC004T6_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = BC004T6_n1947SolicServicoReqNeg_ReqNeqCod[0];
            ZM4T214( -3) ;
         }
         pr_default.close(4);
         OnLoadActions4T214( ) ;
      }

      protected void OnLoadActions4T214( )
      {
      }

      protected void CheckExtendedTable4T214( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004T4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! ( ( A1911SolicServicoReqNeg_Prioridade == 1 ) || ( A1911SolicServicoReqNeg_Prioridade == 2 ) || ( A1911SolicServicoReqNeg_Prioridade == 3 ) || ( A1911SolicServicoReqNeg_Prioridade == 4 ) || ( A1911SolicServicoReqNeg_Prioridade == 5 ) || ( A1911SolicServicoReqNeg_Prioridade == 6 ) || ( A1911SolicServicoReqNeg_Prioridade == 7 ) || ( A1911SolicServicoReqNeg_Prioridade == 8 ) || ( A1911SolicServicoReqNeg_Prioridade == 9 ) ) )
         {
            GX_msglist.addItem("Campo Prioridade fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1914SolicServicoReqNeg_Situacao == 1 ) || ( A1914SolicServicoReqNeg_Situacao == 2 ) || ( A1914SolicServicoReqNeg_Situacao == 3 ) || ( A1914SolicServicoReqNeg_Situacao == 4 ) || ( A1914SolicServicoReqNeg_Situacao == 5 ) ) )
         {
            GX_msglist.addItem("Campo Situa��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC004T5 */
         pr_default.execute(3, new Object[] {n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1947SolicServicoReqNeg_ReqNeqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sol Ser Req Neg_Sol Ser Req Neg'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_REQNEQCOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4T214( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4T214( )
      {
         /* Using cursor BC004T7 */
         pr_default.execute(5, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound214 = 1;
         }
         else
         {
            RcdFound214 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004T3 */
         pr_default.execute(1, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4T214( 3) ;
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = BC004T3_A1895SolicServicoReqNeg_Codigo[0];
            A1915SolicServicoReqNeg_Identificador = BC004T3_A1915SolicServicoReqNeg_Identificador[0];
            A1909SolicServicoReqNeg_Nome = BC004T3_A1909SolicServicoReqNeg_Nome[0];
            A1910SolicServicoReqNeg_Descricao = BC004T3_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = BC004T3_A1911SolicServicoReqNeg_Prioridade[0];
            A1912SolicServicoReqNeg_Agrupador = BC004T3_A1912SolicServicoReqNeg_Agrupador[0];
            A1913SolicServicoReqNeg_Tamanho = BC004T3_A1913SolicServicoReqNeg_Tamanho[0];
            A1914SolicServicoReqNeg_Situacao = BC004T3_A1914SolicServicoReqNeg_Situacao[0];
            A456ContagemResultado_Codigo = BC004T3_A456ContagemResultado_Codigo[0];
            A1947SolicServicoReqNeg_ReqNeqCod = BC004T3_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = BC004T3_n1947SolicServicoReqNeg_ReqNeqCod[0];
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            sMode214 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4T214( ) ;
            if ( AnyError == 1 )
            {
               RcdFound214 = 0;
               InitializeNonKey4T214( ) ;
            }
            Gx_mode = sMode214;
         }
         else
         {
            RcdFound214 = 0;
            InitializeNonKey4T214( ) ;
            sMode214 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode214;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4T0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4T214( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004T2 */
            pr_default.execute(0, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1915SolicServicoReqNeg_Identificador, BC004T2_A1915SolicServicoReqNeg_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z1909SolicServicoReqNeg_Nome, BC004T2_A1909SolicServicoReqNeg_Nome[0]) != 0 ) || ( Z1911SolicServicoReqNeg_Prioridade != BC004T2_A1911SolicServicoReqNeg_Prioridade[0] ) || ( StringUtil.StrCmp(Z1912SolicServicoReqNeg_Agrupador, BC004T2_A1912SolicServicoReqNeg_Agrupador[0]) != 0 ) || ( Z1913SolicServicoReqNeg_Tamanho != BC004T2_A1913SolicServicoReqNeg_Tamanho[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1914SolicServicoReqNeg_Situacao != BC004T2_A1914SolicServicoReqNeg_Situacao[0] ) || ( Z456ContagemResultado_Codigo != BC004T2_A456ContagemResultado_Codigo[0] ) || ( Z1947SolicServicoReqNeg_ReqNeqCod != BC004T2_A1947SolicServicoReqNeg_ReqNeqCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4T214( )
      {
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4T214( 0) ;
            CheckOptimisticConcurrency4T214( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4T214( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4T214( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004T8 */
                     pr_default.execute(6, new Object[] {A1915SolicServicoReqNeg_Identificador, A1909SolicServicoReqNeg_Nome, A1910SolicServicoReqNeg_Descricao, A1911SolicServicoReqNeg_Prioridade, A1912SolicServicoReqNeg_Agrupador, A1913SolicServicoReqNeg_Tamanho, A1914SolicServicoReqNeg_Situacao, A456ContagemResultado_Codigo, n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod});
                     A1895SolicServicoReqNeg_Codigo = BC004T8_A1895SolicServicoReqNeg_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4T214( ) ;
            }
            EndLevel4T214( ) ;
         }
         CloseExtendedTableCursors4T214( ) ;
      }

      protected void Update4T214( )
      {
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4T214( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4T214( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4T214( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004T9 */
                     pr_default.execute(7, new Object[] {A1915SolicServicoReqNeg_Identificador, A1909SolicServicoReqNeg_Nome, A1910SolicServicoReqNeg_Descricao, A1911SolicServicoReqNeg_Prioridade, A1912SolicServicoReqNeg_Agrupador, A1913SolicServicoReqNeg_Tamanho, A1914SolicServicoReqNeg_Situacao, A456ContagemResultado_Codigo, n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod, A1895SolicServicoReqNeg_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4T214( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4T214( ) ;
         }
         CloseExtendedTableCursors4T214( ) ;
      }

      protected void DeferredUpdate4T214( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4T214( ) ;
            AfterConfirm4T214( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4T214( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004T10 */
                  pr_default.execute(8, new Object[] {A1895SolicServicoReqNeg_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode214 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4T214( ) ;
         Gx_mode = sMode214;
      }

      protected void OnDeleteControls4T214( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor BC004T11 */
            pr_default.execute(9, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Requisitos de Neg�cio da Solicita��o de Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004T12 */
            pr_default.execute(10, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Req Neg Req Tec"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
         }
      }

      protected void EndLevel4T214( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4T214( )
      {
         /* Scan By routine */
         /* Using cursor BC004T13 */
         pr_default.execute(11, new Object[] {A1895SolicServicoReqNeg_Codigo});
         RcdFound214 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = BC004T13_A1895SolicServicoReqNeg_Codigo[0];
            A1915SolicServicoReqNeg_Identificador = BC004T13_A1915SolicServicoReqNeg_Identificador[0];
            A1909SolicServicoReqNeg_Nome = BC004T13_A1909SolicServicoReqNeg_Nome[0];
            A1910SolicServicoReqNeg_Descricao = BC004T13_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = BC004T13_A1911SolicServicoReqNeg_Prioridade[0];
            A1912SolicServicoReqNeg_Agrupador = BC004T13_A1912SolicServicoReqNeg_Agrupador[0];
            A1913SolicServicoReqNeg_Tamanho = BC004T13_A1913SolicServicoReqNeg_Tamanho[0];
            A1914SolicServicoReqNeg_Situacao = BC004T13_A1914SolicServicoReqNeg_Situacao[0];
            A456ContagemResultado_Codigo = BC004T13_A456ContagemResultado_Codigo[0];
            A1947SolicServicoReqNeg_ReqNeqCod = BC004T13_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = BC004T13_n1947SolicServicoReqNeg_ReqNeqCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4T214( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound214 = 0;
         ScanKeyLoad4T214( ) ;
      }

      protected void ScanKeyLoad4T214( )
      {
         sMode214 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = BC004T13_A1895SolicServicoReqNeg_Codigo[0];
            A1915SolicServicoReqNeg_Identificador = BC004T13_A1915SolicServicoReqNeg_Identificador[0];
            A1909SolicServicoReqNeg_Nome = BC004T13_A1909SolicServicoReqNeg_Nome[0];
            A1910SolicServicoReqNeg_Descricao = BC004T13_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = BC004T13_A1911SolicServicoReqNeg_Prioridade[0];
            A1912SolicServicoReqNeg_Agrupador = BC004T13_A1912SolicServicoReqNeg_Agrupador[0];
            A1913SolicServicoReqNeg_Tamanho = BC004T13_A1913SolicServicoReqNeg_Tamanho[0];
            A1914SolicServicoReqNeg_Situacao = BC004T13_A1914SolicServicoReqNeg_Situacao[0];
            A456ContagemResultado_Codigo = BC004T13_A456ContagemResultado_Codigo[0];
            A1947SolicServicoReqNeg_ReqNeqCod = BC004T13_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = BC004T13_n1947SolicServicoReqNeg_ReqNeqCod[0];
         }
         Gx_mode = sMode214;
      }

      protected void ScanKeyEnd4T214( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm4T214( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4T214( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4T214( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4T214( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4T214( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4T214( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4T214( )
      {
      }

      protected void AddRow4T214( )
      {
         VarsToRow214( bcSolicitacaoServicoReqNegocio) ;
      }

      protected void ReadRow4T214( )
      {
         RowToVars214( bcSolicitacaoServicoReqNegocio, 1) ;
      }

      protected void InitializeNonKey4T214( )
      {
         A456ContagemResultado_Codigo = 0;
         A1915SolicServicoReqNeg_Identificador = "";
         A1909SolicServicoReqNeg_Nome = "";
         A1910SolicServicoReqNeg_Descricao = "";
         A1911SolicServicoReqNeg_Prioridade = 0;
         A1912SolicServicoReqNeg_Agrupador = "";
         A1913SolicServicoReqNeg_Tamanho = 0;
         A1914SolicServicoReqNeg_Situacao = 0;
         A1947SolicServicoReqNeg_ReqNeqCod = 0;
         n1947SolicServicoReqNeg_ReqNeqCod = false;
         Z1915SolicServicoReqNeg_Identificador = "";
         Z1909SolicServicoReqNeg_Nome = "";
         Z1911SolicServicoReqNeg_Prioridade = 0;
         Z1912SolicServicoReqNeg_Agrupador = "";
         Z1913SolicServicoReqNeg_Tamanho = 0;
         Z1914SolicServicoReqNeg_Situacao = 0;
         Z456ContagemResultado_Codigo = 0;
         Z1947SolicServicoReqNeg_ReqNeqCod = 0;
      }

      protected void InitAll4T214( )
      {
         A1895SolicServicoReqNeg_Codigo = 0;
         InitializeNonKey4T214( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow214( SdtSolicitacaoServicoReqNegocio obj214 )
      {
         obj214.gxTpr_Mode = Gx_mode;
         obj214.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj214.gxTpr_Solicservicoreqneg_identificador = A1915SolicServicoReqNeg_Identificador;
         obj214.gxTpr_Solicservicoreqneg_nome = A1909SolicServicoReqNeg_Nome;
         obj214.gxTpr_Solicservicoreqneg_descricao = A1910SolicServicoReqNeg_Descricao;
         obj214.gxTpr_Solicservicoreqneg_prioridade = A1911SolicServicoReqNeg_Prioridade;
         obj214.gxTpr_Solicservicoreqneg_agrupador = A1912SolicServicoReqNeg_Agrupador;
         obj214.gxTpr_Solicservicoreqneg_tamanho = A1913SolicServicoReqNeg_Tamanho;
         obj214.gxTpr_Solicservicoreqneg_situacao = A1914SolicServicoReqNeg_Situacao;
         obj214.gxTpr_Solicservicoreqneg_reqneqcod = A1947SolicServicoReqNeg_ReqNeqCod;
         obj214.gxTpr_Solicservicoreqneg_codigo = A1895SolicServicoReqNeg_Codigo;
         obj214.gxTpr_Solicservicoreqneg_codigo_Z = Z1895SolicServicoReqNeg_Codigo;
         obj214.gxTpr_Contagemresultado_codigo_Z = Z456ContagemResultado_Codigo;
         obj214.gxTpr_Solicservicoreqneg_identificador_Z = Z1915SolicServicoReqNeg_Identificador;
         obj214.gxTpr_Solicservicoreqneg_nome_Z = Z1909SolicServicoReqNeg_Nome;
         obj214.gxTpr_Solicservicoreqneg_prioridade_Z = Z1911SolicServicoReqNeg_Prioridade;
         obj214.gxTpr_Solicservicoreqneg_agrupador_Z = Z1912SolicServicoReqNeg_Agrupador;
         obj214.gxTpr_Solicservicoreqneg_tamanho_Z = Z1913SolicServicoReqNeg_Tamanho;
         obj214.gxTpr_Solicservicoreqneg_situacao_Z = Z1914SolicServicoReqNeg_Situacao;
         obj214.gxTpr_Solicservicoreqneg_reqneqcod_Z = Z1947SolicServicoReqNeg_ReqNeqCod;
         obj214.gxTpr_Solicservicoreqneg_reqneqcod_N = (short)(Convert.ToInt16(n1947SolicServicoReqNeg_ReqNeqCod));
         obj214.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow214( SdtSolicitacaoServicoReqNegocio obj214 )
      {
         obj214.gxTpr_Solicservicoreqneg_codigo = A1895SolicServicoReqNeg_Codigo;
         return  ;
      }

      public void RowToVars214( SdtSolicitacaoServicoReqNegocio obj214 ,
                                int forceLoad )
      {
         Gx_mode = obj214.gxTpr_Mode;
         A456ContagemResultado_Codigo = obj214.gxTpr_Contagemresultado_codigo;
         A1915SolicServicoReqNeg_Identificador = obj214.gxTpr_Solicservicoreqneg_identificador;
         A1909SolicServicoReqNeg_Nome = obj214.gxTpr_Solicservicoreqneg_nome;
         A1910SolicServicoReqNeg_Descricao = obj214.gxTpr_Solicservicoreqneg_descricao;
         A1911SolicServicoReqNeg_Prioridade = obj214.gxTpr_Solicservicoreqneg_prioridade;
         A1912SolicServicoReqNeg_Agrupador = obj214.gxTpr_Solicservicoreqneg_agrupador;
         A1913SolicServicoReqNeg_Tamanho = obj214.gxTpr_Solicservicoreqneg_tamanho;
         A1914SolicServicoReqNeg_Situacao = obj214.gxTpr_Solicservicoreqneg_situacao;
         A1947SolicServicoReqNeg_ReqNeqCod = obj214.gxTpr_Solicservicoreqneg_reqneqcod;
         n1947SolicServicoReqNeg_ReqNeqCod = false;
         A1895SolicServicoReqNeg_Codigo = obj214.gxTpr_Solicservicoreqneg_codigo;
         Z1895SolicServicoReqNeg_Codigo = obj214.gxTpr_Solicservicoreqneg_codigo_Z;
         Z456ContagemResultado_Codigo = obj214.gxTpr_Contagemresultado_codigo_Z;
         Z1915SolicServicoReqNeg_Identificador = obj214.gxTpr_Solicservicoreqneg_identificador_Z;
         Z1909SolicServicoReqNeg_Nome = obj214.gxTpr_Solicservicoreqneg_nome_Z;
         Z1911SolicServicoReqNeg_Prioridade = obj214.gxTpr_Solicservicoreqneg_prioridade_Z;
         Z1912SolicServicoReqNeg_Agrupador = obj214.gxTpr_Solicservicoreqneg_agrupador_Z;
         Z1913SolicServicoReqNeg_Tamanho = obj214.gxTpr_Solicservicoreqneg_tamanho_Z;
         Z1914SolicServicoReqNeg_Situacao = obj214.gxTpr_Solicservicoreqneg_situacao_Z;
         Z1947SolicServicoReqNeg_ReqNeqCod = obj214.gxTpr_Solicservicoreqneg_reqneqcod_Z;
         n1947SolicServicoReqNeg_ReqNeqCod = (bool)(Convert.ToBoolean(obj214.gxTpr_Solicservicoreqneg_reqneqcod_N));
         Gx_mode = obj214.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1895SolicServicoReqNeg_Codigo = (long)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4T214( ) ;
         ScanKeyStart4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
         }
         ZM4T214( -3) ;
         OnLoadActions4T214( ) ;
         AddRow4T214( ) ;
         ScanKeyEnd4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars214( bcSolicitacaoServicoReqNegocio, 0) ;
         ScanKeyStart4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
         }
         ZM4T214( -3) ;
         OnLoadActions4T214( ) ;
         AddRow4T214( ) ;
         ScanKeyEnd4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars214( bcSolicitacaoServicoReqNegocio, 0) ;
         nKeyPressed = 1;
         GetKey4T214( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4T214( ) ;
         }
         else
         {
            if ( RcdFound214 == 1 )
            {
               if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
               {
                  A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4T214( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4T214( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4T214( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow214( bcSolicitacaoServicoReqNegocio) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars214( bcSolicitacaoServicoReqNegocio, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4T214( ) ;
         if ( RcdFound214 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
            {
               A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "SolicitacaoServicoReqNegocio_BC");
         VarsToRow214( bcSolicitacaoServicoReqNegocio) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSolicitacaoServicoReqNegocio.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSolicitacaoServicoReqNegocio.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSolicitacaoServicoReqNegocio )
         {
            bcSolicitacaoServicoReqNegocio = (SdtSolicitacaoServicoReqNegocio)(sdt);
            if ( StringUtil.StrCmp(bcSolicitacaoServicoReqNegocio.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacaoServicoReqNegocio.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow214( bcSolicitacaoServicoReqNegocio) ;
            }
            else
            {
               RowToVars214( bcSolicitacaoServicoReqNegocio, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSolicitacaoServicoReqNegocio.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacaoServicoReqNegocio.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars214( bcSolicitacaoServicoReqNegocio, 1) ;
         return  ;
      }

      public SdtSolicitacaoServicoReqNegocio SolicitacaoServicoReqNegocio_BC
      {
         get {
            return bcSolicitacaoServicoReqNegocio ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1915SolicServicoReqNeg_Identificador = "";
         A1915SolicServicoReqNeg_Identificador = "";
         Z1909SolicServicoReqNeg_Nome = "";
         A1909SolicServicoReqNeg_Nome = "";
         Z1912SolicServicoReqNeg_Agrupador = "";
         A1912SolicServicoReqNeg_Agrupador = "";
         Z1910SolicServicoReqNeg_Descricao = "";
         A1910SolicServicoReqNeg_Descricao = "";
         BC004T6_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T6_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         BC004T6_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         BC004T6_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         BC004T6_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         BC004T6_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         BC004T6_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         BC004T6_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         BC004T6_A456ContagemResultado_Codigo = new int[1] ;
         BC004T6_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T6_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         BC004T4_A456ContagemResultado_Codigo = new int[1] ;
         BC004T5_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T5_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         BC004T7_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T3_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T3_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         BC004T3_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         BC004T3_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         BC004T3_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         BC004T3_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         BC004T3_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         BC004T3_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         BC004T3_A456ContagemResultado_Codigo = new int[1] ;
         BC004T3_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T3_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         sMode214 = "";
         BC004T2_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T2_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         BC004T2_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         BC004T2_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         BC004T2_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         BC004T2_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         BC004T2_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         BC004T2_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         BC004T2_A456ContagemResultado_Codigo = new int[1] ;
         BC004T2_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T2_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         BC004T8_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T11_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T11_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         BC004T12_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T12_A1919Requisito_Codigo = new int[1] ;
         BC004T13_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004T13_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         BC004T13_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         BC004T13_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         BC004T13_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         BC004T13_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         BC004T13_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         BC004T13_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         BC004T13_A456ContagemResultado_Codigo = new int[1] ;
         BC004T13_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         BC004T13_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaoservicoreqnegocio_bc__default(),
            new Object[][] {
                new Object[] {
               BC004T2_A1895SolicServicoReqNeg_Codigo, BC004T2_A1915SolicServicoReqNeg_Identificador, BC004T2_A1909SolicServicoReqNeg_Nome, BC004T2_A1910SolicServicoReqNeg_Descricao, BC004T2_A1911SolicServicoReqNeg_Prioridade, BC004T2_A1912SolicServicoReqNeg_Agrupador, BC004T2_A1913SolicServicoReqNeg_Tamanho, BC004T2_A1914SolicServicoReqNeg_Situacao, BC004T2_A456ContagemResultado_Codigo, BC004T2_A1947SolicServicoReqNeg_ReqNeqCod,
               BC004T2_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               BC004T3_A1895SolicServicoReqNeg_Codigo, BC004T3_A1915SolicServicoReqNeg_Identificador, BC004T3_A1909SolicServicoReqNeg_Nome, BC004T3_A1910SolicServicoReqNeg_Descricao, BC004T3_A1911SolicServicoReqNeg_Prioridade, BC004T3_A1912SolicServicoReqNeg_Agrupador, BC004T3_A1913SolicServicoReqNeg_Tamanho, BC004T3_A1914SolicServicoReqNeg_Situacao, BC004T3_A456ContagemResultado_Codigo, BC004T3_A1947SolicServicoReqNeg_ReqNeqCod,
               BC004T3_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               BC004T4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC004T5_A1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               BC004T6_A1895SolicServicoReqNeg_Codigo, BC004T6_A1915SolicServicoReqNeg_Identificador, BC004T6_A1909SolicServicoReqNeg_Nome, BC004T6_A1910SolicServicoReqNeg_Descricao, BC004T6_A1911SolicServicoReqNeg_Prioridade, BC004T6_A1912SolicServicoReqNeg_Agrupador, BC004T6_A1913SolicServicoReqNeg_Tamanho, BC004T6_A1914SolicServicoReqNeg_Situacao, BC004T6_A456ContagemResultado_Codigo, BC004T6_A1947SolicServicoReqNeg_ReqNeqCod,
               BC004T6_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               BC004T7_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               BC004T8_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004T11_A1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               BC004T12_A1895SolicServicoReqNeg_Codigo, BC004T12_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004T13_A1895SolicServicoReqNeg_Codigo, BC004T13_A1915SolicServicoReqNeg_Identificador, BC004T13_A1909SolicServicoReqNeg_Nome, BC004T13_A1910SolicServicoReqNeg_Descricao, BC004T13_A1911SolicServicoReqNeg_Prioridade, BC004T13_A1912SolicServicoReqNeg_Agrupador, BC004T13_A1913SolicServicoReqNeg_Tamanho, BC004T13_A1914SolicServicoReqNeg_Situacao, BC004T13_A456ContagemResultado_Codigo, BC004T13_A1947SolicServicoReqNeg_ReqNeqCod,
               BC004T13_n1947SolicServicoReqNeg_ReqNeqCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E124T2 */
         E124T2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1911SolicServicoReqNeg_Prioridade ;
      private short A1911SolicServicoReqNeg_Prioridade ;
      private short Z1914SolicServicoReqNeg_Situacao ;
      private short A1914SolicServicoReqNeg_Situacao ;
      private short RcdFound214 ;
      private int trnEnded ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private long Z1895SolicServicoReqNeg_Codigo ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private long Z1947SolicServicoReqNeg_ReqNeqCod ;
      private long A1947SolicServicoReqNeg_ReqNeqCod ;
      private decimal Z1913SolicServicoReqNeg_Tamanho ;
      private decimal A1913SolicServicoReqNeg_Tamanho ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode214 ;
      private bool n1947SolicServicoReqNeg_ReqNeqCod ;
      private bool Gx_longc ;
      private String Z1910SolicServicoReqNeg_Descricao ;
      private String A1910SolicServicoReqNeg_Descricao ;
      private String Z1915SolicServicoReqNeg_Identificador ;
      private String A1915SolicServicoReqNeg_Identificador ;
      private String Z1909SolicServicoReqNeg_Nome ;
      private String A1909SolicServicoReqNeg_Nome ;
      private String Z1912SolicServicoReqNeg_Agrupador ;
      private String A1912SolicServicoReqNeg_Agrupador ;
      private SdtSolicitacaoServicoReqNegocio bcSolicitacaoServicoReqNegocio ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] BC004T6_A1895SolicServicoReqNeg_Codigo ;
      private String[] BC004T6_A1915SolicServicoReqNeg_Identificador ;
      private String[] BC004T6_A1909SolicServicoReqNeg_Nome ;
      private String[] BC004T6_A1910SolicServicoReqNeg_Descricao ;
      private short[] BC004T6_A1911SolicServicoReqNeg_Prioridade ;
      private String[] BC004T6_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] BC004T6_A1913SolicServicoReqNeg_Tamanho ;
      private short[] BC004T6_A1914SolicServicoReqNeg_Situacao ;
      private int[] BC004T6_A456ContagemResultado_Codigo ;
      private long[] BC004T6_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T6_n1947SolicServicoReqNeg_ReqNeqCod ;
      private int[] BC004T4_A456ContagemResultado_Codigo ;
      private long[] BC004T5_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T5_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] BC004T7_A1895SolicServicoReqNeg_Codigo ;
      private long[] BC004T3_A1895SolicServicoReqNeg_Codigo ;
      private String[] BC004T3_A1915SolicServicoReqNeg_Identificador ;
      private String[] BC004T3_A1909SolicServicoReqNeg_Nome ;
      private String[] BC004T3_A1910SolicServicoReqNeg_Descricao ;
      private short[] BC004T3_A1911SolicServicoReqNeg_Prioridade ;
      private String[] BC004T3_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] BC004T3_A1913SolicServicoReqNeg_Tamanho ;
      private short[] BC004T3_A1914SolicServicoReqNeg_Situacao ;
      private int[] BC004T3_A456ContagemResultado_Codigo ;
      private long[] BC004T3_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T3_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] BC004T2_A1895SolicServicoReqNeg_Codigo ;
      private String[] BC004T2_A1915SolicServicoReqNeg_Identificador ;
      private String[] BC004T2_A1909SolicServicoReqNeg_Nome ;
      private String[] BC004T2_A1910SolicServicoReqNeg_Descricao ;
      private short[] BC004T2_A1911SolicServicoReqNeg_Prioridade ;
      private String[] BC004T2_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] BC004T2_A1913SolicServicoReqNeg_Tamanho ;
      private short[] BC004T2_A1914SolicServicoReqNeg_Situacao ;
      private int[] BC004T2_A456ContagemResultado_Codigo ;
      private long[] BC004T2_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T2_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] BC004T8_A1895SolicServicoReqNeg_Codigo ;
      private long[] BC004T11_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T11_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] BC004T12_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004T12_A1919Requisito_Codigo ;
      private long[] BC004T13_A1895SolicServicoReqNeg_Codigo ;
      private String[] BC004T13_A1915SolicServicoReqNeg_Identificador ;
      private String[] BC004T13_A1909SolicServicoReqNeg_Nome ;
      private String[] BC004T13_A1910SolicServicoReqNeg_Descricao ;
      private short[] BC004T13_A1911SolicServicoReqNeg_Prioridade ;
      private String[] BC004T13_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] BC004T13_A1913SolicServicoReqNeg_Tamanho ;
      private short[] BC004T13_A1914SolicServicoReqNeg_Situacao ;
      private int[] BC004T13_A456ContagemResultado_Codigo ;
      private long[] BC004T13_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] BC004T13_n1947SolicServicoReqNeg_ReqNeqCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class solicitacaoservicoreqnegocio_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004T6 ;
          prmBC004T6 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T4 ;
          prmBC004T4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004T5 ;
          prmBC004T5 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T7 ;
          prmBC004T7 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T3 ;
          prmBC004T3 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T2 ;
          prmBC004T2 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T8 ;
          prmBC004T8 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@SolicServicoReqNeg_Nome",SqlDbType.VarChar,250,0} ,
          new Object[] {"@SolicServicoReqNeg_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SolicServicoReqNeg_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@SolicServicoReqNeg_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@SolicServicoReqNeg_Tamanho",SqlDbType.Decimal,7,3} ,
          new Object[] {"@SolicServicoReqNeg_Situacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T9 ;
          prmBC004T9 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@SolicServicoReqNeg_Nome",SqlDbType.VarChar,250,0} ,
          new Object[] {"@SolicServicoReqNeg_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SolicServicoReqNeg_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@SolicServicoReqNeg_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@SolicServicoReqNeg_Tamanho",SqlDbType.Decimal,7,3} ,
          new Object[] {"@SolicServicoReqNeg_Situacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T10 ;
          prmBC004T10 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T11 ;
          prmBC004T11 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T12 ;
          prmBC004T12 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004T13 ;
          prmBC004T13 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004T2", "SELECT [SolicServicoReqNeg_Codigo], [SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (UPDLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T2,1,0,true,false )
             ,new CursorDef("BC004T3", "SELECT [SolicServicoReqNeg_Codigo], [SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T3,1,0,true,false )
             ,new CursorDef("BC004T4", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T4,1,0,true,false )
             ,new CursorDef("BC004T5", "SELECT [SolicServicoReqNeg_Codigo] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_ReqNeqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T5,1,0,true,false )
             ,new CursorDef("BC004T6", "SELECT TM1.[SolicServicoReqNeg_Codigo], TM1.[SolicServicoReqNeg_Identificador], TM1.[SolicServicoReqNeg_Nome], TM1.[SolicServicoReqNeg_Descricao], TM1.[SolicServicoReqNeg_Prioridade], TM1.[SolicServicoReqNeg_Agrupador], TM1.[SolicServicoReqNeg_Tamanho], TM1.[SolicServicoReqNeg_Situacao], TM1.[ContagemResultado_Codigo], TM1.[SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] TM1 WITH (NOLOCK) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T6,100,0,true,false )
             ,new CursorDef("BC004T7", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T7,1,0,true,false )
             ,new CursorDef("BC004T8", "INSERT INTO [SolicitacaoServicoReqNegocio]([SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod]) VALUES(@SolicServicoReqNeg_Identificador, @SolicServicoReqNeg_Nome, @SolicServicoReqNeg_Descricao, @SolicServicoReqNeg_Prioridade, @SolicServicoReqNeg_Agrupador, @SolicServicoReqNeg_Tamanho, @SolicServicoReqNeg_Situacao, @ContagemResultado_Codigo, @SolicServicoReqNeg_ReqNeqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004T8)
             ,new CursorDef("BC004T9", "UPDATE [SolicitacaoServicoReqNegocio] SET [SolicServicoReqNeg_Identificador]=@SolicServicoReqNeg_Identificador, [SolicServicoReqNeg_Nome]=@SolicServicoReqNeg_Nome, [SolicServicoReqNeg_Descricao]=@SolicServicoReqNeg_Descricao, [SolicServicoReqNeg_Prioridade]=@SolicServicoReqNeg_Prioridade, [SolicServicoReqNeg_Agrupador]=@SolicServicoReqNeg_Agrupador, [SolicServicoReqNeg_Tamanho]=@SolicServicoReqNeg_Tamanho, [SolicServicoReqNeg_Situacao]=@SolicServicoReqNeg_Situacao, [ContagemResultado_Codigo]=@ContagemResultado_Codigo, [SolicServicoReqNeg_ReqNeqCod]=@SolicServicoReqNeg_ReqNeqCod  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo", GxErrorMask.GX_NOMASK,prmBC004T9)
             ,new CursorDef("BC004T10", "DELETE FROM [SolicitacaoServicoReqNegocio]  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo", GxErrorMask.GX_NOMASK,prmBC004T10)
             ,new CursorDef("BC004T11", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_ReqNeqCod] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T11,1,0,true,true )
             ,new CursorDef("BC004T12", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T12,1,0,true,true )
             ,new CursorDef("BC004T13", "SELECT TM1.[SolicServicoReqNeg_Codigo], TM1.[SolicServicoReqNeg_Identificador], TM1.[SolicServicoReqNeg_Nome], TM1.[SolicServicoReqNeg_Descricao], TM1.[SolicServicoReqNeg_Prioridade], TM1.[SolicServicoReqNeg_Agrupador], TM1.[SolicServicoReqNeg_Tamanho], TM1.[SolicServicoReqNeg_Situacao], TM1.[ContagemResultado_Codigo], TM1.[SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] TM1 WITH (NOLOCK) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004T13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 10 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (long)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (long)parms[9]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (long)parms[9]);
                }
                stmt.SetParameter(10, (long)parms[10]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
