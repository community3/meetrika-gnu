/*
               File: NaoConformidadeGeneral
        Description: Nao Conformidade General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:45.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class naoconformidadegeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public naoconformidadegeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public naoconformidadegeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_NaoConformidade_Codigo )
      {
         this.A426NaoConformidade_Codigo = aP0_NaoConformidade_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbNaoConformidade_Tipo = new GXCombobox();
         cmbNaoConformidade_EhImpeditiva = new GXCombobox();
         chkNaoConformidade_Glosavel = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A426NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A426NaoConformidade_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "NaoConformidadeGeneral";
               context.Gx_err = 0;
               WSAN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Nao Conformidade General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117204573");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("naoconformidadegeneral.aspx") + "?" + UrlEncode("" +A426NaoConformidade_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA426NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( sPrefix, A1121NaoConformidade_EhImpeditiva));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_NAOCONFORMIDADE_GLOSAVEL", GetSecureSignedToken( sPrefix, A2029NaoConformidade_Glosavel));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAN2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("naoconformidadegeneral.js", "?20203117204575");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "NaoConformidadeGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nao Conformidade General" ;
      }

      protected void WBAN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "naoconformidadegeneral.aspx");
            }
            wb_table1_2_AN2( true) ;
         }
         else
         {
            wb_table1_2_AN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNaoConformidade_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A426NaoConformidade_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNaoConformidade_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtNaoConformidade_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_NaoConformidadeGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTAN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Nao Conformidade General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAN0( ) ;
            }
         }
      }

      protected void WSAN2( )
      {
         STARTAN2( ) ;
         EVTAN2( ) ;
      }

      protected void EVTAN2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AN2 */
                                    E11AN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AN2 */
                                    E12AN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AN2 */
                                    E13AN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AN2 */
                                    E14AN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAN2( ) ;
            }
         }
      }

      protected void PAAN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbNaoConformidade_Tipo.Name = "NAOCONFORMIDADE_TIPO";
            cmbNaoConformidade_Tipo.WebTags = "";
            cmbNaoConformidade_Tipo.addItem("1", "Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("2", "Itens da Contagem", 0);
            cmbNaoConformidade_Tipo.addItem("3", "Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("4", "Check List Demandas", 0);
            cmbNaoConformidade_Tipo.addItem("5", "Check List Contagens", 0);
            cmbNaoConformidade_Tipo.addItem("6", "Homologa��o", 0);
            if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
            {
               A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
            }
            cmbNaoConformidade_EhImpeditiva.Name = "NAOCONFORMIDADE_EHIMPEDITIVA";
            cmbNaoConformidade_EhImpeditiva.WebTags = "";
            cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbNaoConformidade_EhImpeditiva.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
            {
               A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
               n1121NaoConformidade_EhImpeditiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( sPrefix, A1121NaoConformidade_EhImpeditiva));
            }
            chkNaoConformidade_Glosavel.Name = "NAOCONFORMIDADE_GLOSAVEL";
            chkNaoConformidade_Glosavel.WebTags = "";
            chkNaoConformidade_Glosavel.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkNaoConformidade_Glosavel_Internalname, "TitleCaption", chkNaoConformidade_Glosavel.Caption);
            chkNaoConformidade_Glosavel.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbNaoConformidade_Tipo.ItemCount > 0 )
         {
            A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cmbNaoConformidade_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
         }
         if ( cmbNaoConformidade_EhImpeditiva.ItemCount > 0 )
         {
            A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cmbNaoConformidade_EhImpeditiva.getValidValue(StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva)));
            n1121NaoConformidade_EhImpeditiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( sPrefix, A1121NaoConformidade_EhImpeditiva));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "NaoConformidadeGeneral";
         context.Gx_err = 0;
      }

      protected void RFAN2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00AN2 */
            pr_default.execute(0, new Object[] {A426NaoConformidade_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A2029NaoConformidade_Glosavel = H00AN2_A2029NaoConformidade_Glosavel[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_GLOSAVEL", GetSecureSignedToken( sPrefix, A2029NaoConformidade_Glosavel));
               n2029NaoConformidade_Glosavel = H00AN2_n2029NaoConformidade_Glosavel[0];
               A1121NaoConformidade_EhImpeditiva = H00AN2_A1121NaoConformidade_EhImpeditiva[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( sPrefix, A1121NaoConformidade_EhImpeditiva));
               n1121NaoConformidade_EhImpeditiva = H00AN2_n1121NaoConformidade_EhImpeditiva[0];
               A429NaoConformidade_Tipo = H00AN2_A429NaoConformidade_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
               /* Execute user event: E12AN2 */
               E12AN2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBAN0( ) ;
         }
      }

      protected void STRUPAN0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "NaoConformidadeGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11AN2 */
         E11AN2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbNaoConformidade_Tipo.CurrentValue = cgiGet( cmbNaoConformidade_Tipo_Internalname);
            A429NaoConformidade_Tipo = (short)(NumberUtil.Val( cgiGet( cmbNaoConformidade_Tipo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A429NaoConformidade_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A429NaoConformidade_Tipo), "Z9")));
            cmbNaoConformidade_EhImpeditiva.CurrentValue = cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname);
            A1121NaoConformidade_EhImpeditiva = StringUtil.StrToBool( cgiGet( cmbNaoConformidade_EhImpeditiva_Internalname));
            n1121NaoConformidade_EhImpeditiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1121NaoConformidade_EhImpeditiva", A1121NaoConformidade_EhImpeditiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_EHIMPEDITIVA", GetSecureSignedToken( sPrefix, A1121NaoConformidade_EhImpeditiva));
            A2029NaoConformidade_Glosavel = StringUtil.StrToBool( cgiGet( chkNaoConformidade_Glosavel_Internalname));
            n2029NaoConformidade_Glosavel = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2029NaoConformidade_Glosavel", A2029NaoConformidade_Glosavel);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_NAOCONFORMIDADE_GLOSAVEL", GetSecureSignedToken( sPrefix, A2029NaoConformidade_Glosavel));
            /* Read saved values. */
            wcpOA426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA426NaoConformidade_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11AN2 */
         E11AN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11AN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12AN2( )
      {
         /* Load Routine */
         edtNaoConformidade_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtNaoConformidade_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNaoConformidade_Codigo_Visible), 5, 0)));
      }

      protected void E13AN2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A426NaoConformidade_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14AN2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("naoconformidade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A426NaoConformidade_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "NaoConformidade";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "NaoConformidade_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7NaoConformidade_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_AN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_AN2( true) ;
         }
         else
         {
            wb_table2_8_AN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_AN2( true) ;
         }
         else
         {
            wb_table3_26_AN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_AN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AN2e( true) ;
         }
         else
         {
            wb_table1_2_AN2e( false) ;
         }
      }

      protected void wb_table3_26_AN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_NaoConformidadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_NaoConformidadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_AN2e( true) ;
         }
         else
         {
            wb_table3_26_AN2e( false) ;
         }
      }

      protected void wb_table2_8_AN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_tipo_Internalname, "Tipo", "", "", lblTextblocknaoconformidade_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbNaoConformidade_Tipo, cmbNaoConformidade_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0)), 1, cmbNaoConformidade_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_NaoConformidadeGeneral.htm");
            cmbNaoConformidade_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A429NaoConformidade_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbNaoConformidade_Tipo_Internalname, "Values", (String)(cmbNaoConformidade_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_ehimpeditiva_Internalname, "� impeditiva?", "", "", lblTextblocknaoconformidade_ehimpeditiva_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbNaoConformidade_EhImpeditiva, cmbNaoConformidade_EhImpeditiva_Internalname, StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva), 1, cmbNaoConformidade_EhImpeditiva_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_NaoConformidadeGeneral.htm");
            cmbNaoConformidade_EhImpeditiva.CurrentValue = StringUtil.BoolToStr( A1121NaoConformidade_EhImpeditiva);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbNaoConformidade_EhImpeditiva_Internalname, "Values", (String)(cmbNaoConformidade_EhImpeditiva.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_glosavel_Internalname, "Glos�vel", "", "", lblTextblocknaoconformidade_glosavel_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NaoConformidadeGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkNaoConformidade_Glosavel_Internalname, StringUtil.BoolToStr( A2029NaoConformidade_Glosavel), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AN2e( true) ;
         }
         else
         {
            wb_table2_8_AN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A426NaoConformidade_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAN2( ) ;
         WSAN2( ) ;
         WEAN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA426NaoConformidade_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAN2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "naoconformidadegeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAN2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A426NaoConformidade_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         }
         wcpOA426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA426NaoConformidade_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A426NaoConformidade_Codigo != wcpOA426NaoConformidade_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA426NaoConformidade_Codigo = cgiGet( sPrefix+"A426NaoConformidade_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA426NaoConformidade_Codigo) > 0 )
         {
            A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA426NaoConformidade_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         }
         else
         {
            A426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A426NaoConformidade_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAN2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAN2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A426NaoConformidade_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA426NaoConformidade_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A426NaoConformidade_Codigo_CTRL", StringUtil.RTrim( sCtrlA426NaoConformidade_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311720464");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("naoconformidadegeneral.js", "?2020311720464");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocknaoconformidade_tipo_Internalname = sPrefix+"TEXTBLOCKNAOCONFORMIDADE_TIPO";
         cmbNaoConformidade_Tipo_Internalname = sPrefix+"NAOCONFORMIDADE_TIPO";
         lblTextblocknaoconformidade_ehimpeditiva_Internalname = sPrefix+"TEXTBLOCKNAOCONFORMIDADE_EHIMPEDITIVA";
         cmbNaoConformidade_EhImpeditiva_Internalname = sPrefix+"NAOCONFORMIDADE_EHIMPEDITIVA";
         lblTextblocknaoconformidade_glosavel_Internalname = sPrefix+"TEXTBLOCKNAOCONFORMIDADE_GLOSAVEL";
         chkNaoConformidade_Glosavel_Internalname = sPrefix+"NAOCONFORMIDADE_GLOSAVEL";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtNaoConformidade_Codigo_Internalname = sPrefix+"NAOCONFORMIDADE_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbNaoConformidade_EhImpeditiva_Jsonclick = "";
         cmbNaoConformidade_Tipo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         chkNaoConformidade_Glosavel.Caption = "";
         edtNaoConformidade_Codigo_Jsonclick = "";
         edtNaoConformidade_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13AN2',iparms:[{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14AN2',iparms:[{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00AN2_A426NaoConformidade_Codigo = new int[1] ;
         H00AN2_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         H00AN2_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         H00AN2_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00AN2_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00AN2_A429NaoConformidade_Tipo = new short[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocknaoconformidade_tipo_Jsonclick = "";
         lblTextblocknaoconformidade_ehimpeditiva_Jsonclick = "";
         lblTextblocknaoconformidade_glosavel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA426NaoConformidade_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.naoconformidadegeneral__default(),
            new Object[][] {
                new Object[] {
               H00AN2_A426NaoConformidade_Codigo, H00AN2_A2029NaoConformidade_Glosavel, H00AN2_n2029NaoConformidade_Glosavel, H00AN2_A1121NaoConformidade_EhImpeditiva, H00AN2_n1121NaoConformidade_EhImpeditiva, H00AN2_A429NaoConformidade_Tipo
               }
            }
         );
         AV14Pgmname = "NaoConformidadeGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "NaoConformidadeGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A429NaoConformidade_Tipo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A426NaoConformidade_Codigo ;
      private int wcpOA426NaoConformidade_Codigo ;
      private int edtNaoConformidade_Codigo_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7NaoConformidade_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtNaoConformidade_Codigo_Internalname ;
      private String edtNaoConformidade_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkNaoConformidade_Glosavel_Internalname ;
      private String scmdbuf ;
      private String cmbNaoConformidade_Tipo_Internalname ;
      private String cmbNaoConformidade_EhImpeditiva_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocknaoconformidade_tipo_Internalname ;
      private String lblTextblocknaoconformidade_tipo_Jsonclick ;
      private String cmbNaoConformidade_Tipo_Jsonclick ;
      private String lblTextblocknaoconformidade_ehimpeditiva_Internalname ;
      private String lblTextblocknaoconformidade_ehimpeditiva_Jsonclick ;
      private String cmbNaoConformidade_EhImpeditiva_Jsonclick ;
      private String lblTextblocknaoconformidade_glosavel_Internalname ;
      private String lblTextblocknaoconformidade_glosavel_Jsonclick ;
      private String sCtrlA426NaoConformidade_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1121NaoConformidade_EhImpeditiva ;
      private bool A2029NaoConformidade_Glosavel ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1121NaoConformidade_EhImpeditiva ;
      private bool n2029NaoConformidade_Glosavel ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbNaoConformidade_Tipo ;
      private GXCombobox cmbNaoConformidade_EhImpeditiva ;
      private GXCheckbox chkNaoConformidade_Glosavel ;
      private IDataStoreProvider pr_default ;
      private int[] H00AN2_A426NaoConformidade_Codigo ;
      private bool[] H00AN2_A2029NaoConformidade_Glosavel ;
      private bool[] H00AN2_n2029NaoConformidade_Glosavel ;
      private bool[] H00AN2_A1121NaoConformidade_EhImpeditiva ;
      private bool[] H00AN2_n1121NaoConformidade_EhImpeditiva ;
      private short[] H00AN2_A429NaoConformidade_Tipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class naoconformidadegeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AN2 ;
          prmH00AN2 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AN2", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Glosavel], [NaoConformidade_EhImpeditiva], [NaoConformidade_Tipo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ORDER BY [NaoConformidade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AN2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
