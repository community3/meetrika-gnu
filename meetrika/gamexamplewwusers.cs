/*
               File: GAMExampleWWUsers
        Description: Users
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:0.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplewwusers : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplewwusers( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gamexamplewwusers( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavFilusergender = new GXCombobox();
         cmbavFilauttype = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridww") == 0 )
            {
               nRC_GXsfl_60 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_60_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_60_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridww_newrow( ) ;
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA222( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavUserid_Enabled = 0;
               edtavAuthenticationtypename_Enabled = 0;
               edtavName_Enabled = 0;
               edtavFirstname_Enabled = 0;
               edtavLastname_Enabled = 0;
               WS222( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE222( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Users ") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311732466");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexamplewwusers.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_60", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_60), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERSXPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37UsersXPage), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOUNTUSERS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36CountUsers), 4, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm222( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcmenu == null ) )
         {
            WebComp_Wcmenu.componentjscripts();
         }
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleWWUsers" ;
      }

      public override String GetPgmdesc( )
      {
         return "Users " ;
      }

      protected void WB220( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_222( true) ;
         }
         else
         {
            wb_table1_2_222( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_222e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_60_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15CurrentPage), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15CurrentPage), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavCurrentpage_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleWWUsers.htm");
         }
         wbLoad = true;
      }

      protected void START222( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Users ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
            {
               WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
               WebComp_Wcmenu.ComponentInit();
               WebComp_Wcmenu.Name = "GAMMenu";
               WebComp_Wcmenu_Component = "GAMMenu";
            }
            WebComp_Wcmenu.componentrestorestate("W0005", "");
         }
         wbErr = false;
         STRUP220( ) ;
      }

      protected void WS222( )
      {
         START222( ) ;
         EVT222( ) ;
      }

      protected void EVT222( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDNEW'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11222 */
                           E11222 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'SEARCH'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12222 */
                           E12222 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'FIRST'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13222 */
                           E13222 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'PREVIOUS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14222 */
                           E14222 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'NEXT'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15222 */
                           E15222 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "GRIDWW.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNUPD.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNUPD.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) )
                        {
                           nGXsfl_60_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_60_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_60_idx), 4, 0)), 4, "0");
                           SubsflControlProps_602( ) ;
                           AV35UserId = cgiGet( edtavUserid_Internalname);
                           AV14BtnUpd = cgiGet( edtavBtnupd_Internalname);
                           AV12BtnRole = cgiGet( edtavBtnrole_Internalname);
                           AV13BtnSetPwd = cgiGet( edtavBtnsetpwd_Internalname);
                           AV9BtnDlt = cgiGet( edtavBtndlt_Internalname);
                           AV6AuthenticationTypeName = cgiGet( edtavAuthenticationtypename_Internalname);
                           AV31Name = cgiGet( edtavName_Internalname);
                           AV29FirstName = cgiGet( edtavFirstname_Internalname);
                           AV30LastName = cgiGet( edtavLastname_Internalname);
                           if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
                           {
                              WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
                              WebComp_Wcmenu.ComponentInit();
                              WebComp_Wcmenu.Name = "GAMMenu";
                              WebComp_Wcmenu_Component = "GAMMenu";
                           }
                           WebComp_Wcmenu.componentrestorestate("W0005", "");
                           if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
                           {
                              WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
                              WebComp_Wcmenu.ComponentInit();
                              WebComp_Wcmenu.Name = "GAMMenu";
                              WebComp_Wcmenu_Component = "GAMMenu";
                           }
                           WebComp_Wcmenu.componentrestorestate("W0005", "");
                           CheckSecurityRow2260( sGXsfl_60_idx) ;
                           if ( GxWebError != 0 )
                           {
                              return  ;
                           }
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E16222 */
                                 E16222 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E17222 */
                                 E17222 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRIDWW.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E18222 */
                                 E18222 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "VBTNUPD.CLICK") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E19222 */
                                 E19222 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "VBTNDLT.CLICK") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E20222 */
                                 E20222 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    if ( ! Rfr0gs )
                                    {
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 4);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 5 )
                     {
                        WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
                        WebComp_Wcmenu.ComponentInit();
                        WebComp_Wcmenu.Name = "GAMMenu";
                        WebComp_Wcmenu_Component = "GAMMenu";
                        WebComp_Wcmenu.componentprocess("W0005", "", sEvt);
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE222( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm222( ) ;
            }
         }
      }

      protected void PA222( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavFilusergender.Name = "vFILUSERGENDER";
            cmbavFilusergender.WebTags = "";
            cmbavFilusergender.addItem("", "(Nenhum)", 0);
            cmbavFilusergender.addItem("N", "Not Specified", 0);
            cmbavFilusergender.addItem("F", "Female", 0);
            cmbavFilusergender.addItem("M", "Male", 0);
            if ( cmbavFilusergender.ItemCount > 0 )
            {
               AV28FilUserGender = cmbavFilusergender.getValidValue(AV28FilUserGender);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FilUserGender", AV28FilUserGender);
            }
            cmbavFilauttype.Name = "vFILAUTTYPE";
            cmbavFilauttype.WebTags = "";
            cmbavFilauttype.addItem("", "(Nenhum)", 0);
            if ( cmbavFilauttype.ItemCount > 0 )
            {
               AV19FilAutType = cmbavFilauttype.getValidValue(AV19FilAutType);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FilAutType", AV19FilAutType);
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFilname_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridww_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_602( ) ;
         while ( nGXsfl_60_idx <= nRC_GXsfl_60 )
         {
            sendrow_602( ) ;
            sendsecurityrow_602( ) ;
            nGXsfl_60_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_60_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_60_idx+1));
            sGXsfl_60_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_60_idx), 4, 0)), 4, "0");
            SubsflControlProps_602( ) ;
         }
         context.GX_webresponse.AddString(GridwwContainer.ToJavascriptSource());
         /* End function gxnrGridww_newrow */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavFilusergender.ItemCount > 0 )
         {
            AV28FilUserGender = cmbavFilusergender.getValidValue(AV28FilUserGender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FilUserGender", AV28FilUserGender);
         }
         if ( cmbavFilauttype.ItemCount > 0 )
         {
            AV19FilAutType = cmbavFilauttype.getValidValue(AV19FilAutType);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FilAutType", AV19FilAutType);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF222( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         edtavAuthenticationtypename_Enabled = 0;
         edtavName_Enabled = 0;
         edtavFirstname_Enabled = 0;
         edtavLastname_Enabled = 0;
      }

      protected void RF222( )
      {
         if ( isAjaxCallMode( ) )
         {
            GridwwContainer.ClearRows();
         }
         wbStart = 60;
         /* Execute user event: E17222 */
         E17222 ();
         nGXsfl_60_idx = 1;
         sGXsfl_60_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_60_idx), 4, 0)), 4, "0");
         SubsflControlProps_602( ) ;
         nGXsfl_60_Refreshing = 1;
         GridwwContainer.AddObjectProperty("GridName", "Gridww");
         GridwwContainer.AddObjectProperty("CmpContext", "");
         GridwwContainer.AddObjectProperty("InMasterPage", "false");
         GridwwContainer.AddObjectProperty("Class", "WorkWith");
         GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
         GridwwContainer.PageSize = subGridww_Recordsperpage( );
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
            {
               WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
               WebComp_Wcmenu.ComponentInit();
               WebComp_Wcmenu.Name = "GAMMenu";
               WebComp_Wcmenu_Component = "GAMMenu";
            }
            if ( ! context.isAjaxRequest( ) )
            {
               WebComp_Wcmenu.setjustcreated();
            }
            WebComp_Wcmenu.componentprepare(new Object[] {(String)"W0005",(String)""});
            WebComp_Wcmenu.componentbind(new Object[] {});
            if ( 1 != 0 )
            {
               WebComp_Wcmenu.componentstart();
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_602( ) ;
            /* Execute user event: E18222 */
            E18222 ();
            wbEnd = 60;
            WB220( ) ;
         }
         nGXsfl_60_Refreshing = 0;
      }

      protected void CheckSecurityRow2260( String sGXChecksfl_idx )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected int subGridww_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridww_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridww_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridww_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP220( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         edtavAuthenticationtypename_Enabled = 0;
         edtavName_Enabled = 0;
         edtavFirstname_Enabled = 0;
         edtavLastname_Enabled = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16222 */
         E16222 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV24FilName = cgiGet( edtavFilname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FilName", AV24FilName);
            AV25FilNames = cgiGet( edtavFilnames_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FilNames", AV25FilNames);
            AV20FilEmail = cgiGet( edtavFilemail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20FilEmail", AV20FilEmail);
            cmbavFilusergender.Name = cmbavFilusergender_Internalname;
            cmbavFilusergender.CurrentValue = cgiGet( cmbavFilusergender_Internalname);
            AV28FilUserGender = cgiGet( cmbavFilusergender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FilUserGender", AV28FilUserGender);
            cmbavFilauttype.Name = cmbavFilauttype_Internalname;
            cmbavFilauttype.CurrentValue = cgiGet( cmbavFilauttype_Internalname);
            AV19FilAutType = cgiGet( cmbavFilauttype_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FilAutType", AV19FilAutType);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCURRENTPAGE");
               GX_FocusControl = edtavCurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15CurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
            }
            else
            {
               AV15CurrentPage = (short)(context.localUtil.CToN( cgiGet( edtavCurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_60 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_60"), ",", "."));
            if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
            {
               WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
               WebComp_Wcmenu.ComponentInit();
               WebComp_Wcmenu.Name = "GAMMenu";
               WebComp_Wcmenu_Component = "GAMMenu";
            }
            WebComp_Wcmenu.componentrestorestate("W0005", "");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16222 */
         E16222 ();
         if (returnInSub) return;
      }

      protected void E16222( )
      {
         /* Start Routine */
         AV15CurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
         edtavCurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCurrentpage_Visible), 5, 0)));
         AV36CountUsers = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36CountUsers", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36CountUsers), 4, 0)));
         AV37UsersXPage = 15;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37UsersXPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37UsersXPage), 4, 0)));
      }

      protected void E17222( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         cmbavFilauttype.removeAllItems();
         cmbavFilauttype.addItem("", "(All)", 0);
         AV7AuthenticationTypes = new SdtGAMRepository(context).getauthenticationtypes(AV27FilterAutType, out  AV18Errors);
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV7AuthenticationTypes.Count )
         {
            AV5AuthenticationType = ((SdtGAMAuthenticationType)AV7AuthenticationTypes.Item(AV40GXV1));
            cmbavFilauttype.addItem(AV5AuthenticationType.gxTpr_Name, AV5AuthenticationType.gxTpr_Description, 0);
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         AV33Repository = new SdtGAMRepository(context).get();
      }

      private void E18222( )
      {
         /* Gridww_Load Routine */
         AV26Filter.gxTpr_Name = AV24FilName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Names = AV25FilNames;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Email = AV20FilEmail;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Gender = AV28FilUserGender;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Authenticationtypename = AV19FilAutType;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Loadcustomattributes = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Returnanonymoususer = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Limit = AV37UsersXPage;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV26Filter.gxTpr_Start = (int)((AV15CurrentPage-1)*AV26Filter.gxTpr_Limit+1);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26Filter", AV26Filter);
         AV42GXV3 = 1;
         AV41GXV2 = new SdtGAMRepository(context).getusersorderby(AV26Filter, 0, out  AV18Errors);
         while ( AV42GXV3 <= AV41GXV2.Count )
         {
            AV34User = ((SdtGAMUser)AV41GXV2.Item(AV42GXV3));
            AV36CountUsers = (short)(AV36CountUsers+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36CountUsers", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36CountUsers), 4, 0)));
            AV14BtnUpd = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            AV43Btnupd_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            AV12BtnRole = context.GetImagePath( "1399ecfa-9434-44f7-87ac-cbf49b0a76d5", "", context.GetTheme( ));
            AV44Btnrole_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "1399ecfa-9434-44f7-87ac-cbf49b0a76d5", "", context.GetTheme( )));
            AV13BtnSetPwd = context.GetImagePath( "846cc5aa-e497-452c-bf2c-fdd868279a72", "", context.GetTheme( ));
            AV45Btnsetpwd_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "846cc5aa-e497-452c-bf2c-fdd868279a72", "", context.GetTheme( )));
            AV9BtnDlt = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            AV46Btndlt_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            AV6AuthenticationTypeName = AV34User.gxTpr_Authenticationtypename;
            AV35UserId = AV34User.gxTpr_Guid;
            AV31Name = AV34User.gxTpr_Name;
            AV29FirstName = AV34User.gxTpr_Firstname;
            AV30LastName = AV34User.gxTpr_Lastname;
            AV16Email = AV34User.gxTpr_Email;
            edtavBtnupd_Visible = 1;
            edtavBtnrole_Visible = 1;
            edtavBtnsetpwd_Visible = 1;
            edtavBtndlt_Visible = 1;
            if ( StringUtil.StrCmp(StringUtil.Trim( AV34User.gxTpr_Guid), StringUtil.Trim( AV33Repository.gxTpr_Anonymoususerguid)) == 0 )
            {
               edtavBtnupd_Visible = 0;
               edtavBtnsetpwd_Visible = 0;
               edtavBtndlt_Visible = 0;
            }
            if ( AV34User.gxTpr_Isautoregistereduser )
            {
               edtavBtnupd_Visible = 0;
               edtavBtnrole_Visible = 0;
               edtavBtnsetpwd_Visible = 0;
            }
            if ( AV34User.gxTpr_Isdeleted )
            {
               edtavBtndlt_Visible = 0;
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 60;
            }
            sendrow_602( ) ;
            sendsecurityrow_602( ) ;
            AV42GXV3 = (int)(AV42GXV3+1);
         }
         if ( AV36CountUsers < AV37UsersXPage )
         {
            imgNext_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgNext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNext_Visible), 5, 0)));
         }
         else
         {
            imgNext_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgNext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgNext_Visible), 5, 0)));
         }
      }

      protected void E11222( )
      {
         /* 'AddNew' Routine */
         /* Window Datatype Object Property */
         AV32Popup.Url = formatLink("gamexampleentryuser.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(StringUtil.RTrim(""));
         AV32Popup.SetReturnParms(new Object[] {"",});
         context.NewWindow(AV32Popup);
         context.DoAjaxRefresh();
      }

      protected void E19222( )
      {
         /* Btnupd_Click Routine */
         /* Window Datatype Object Property */
         AV32Popup.Url = formatLink("gamexampleentryuser.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(StringUtil.RTrim(AV35UserId));
         AV32Popup.SetReturnParms(new Object[] {"AV35UserId",});
         context.NewWindow(AV32Popup);
      }

      protected void E20222( )
      {
         /* Btndlt_Click Routine */
         AV47GXLvl90 = 0;
         /* Using cursor H00222 */
         pr_default.execute(0, new Object[] {AV35UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A341Usuario_UserGamGuid = H00222_A341Usuario_UserGamGuid[0];
            AV47GXLvl90 = 1;
            GX_msglist.addItem("Este usu�rio tem informa��es referenciadas em outras tabelas!");
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV47GXLvl90 == 0 )
         {
            /* Window Datatype Object Property */
            AV32Popup.Url = formatLink("gamexampleentryuser.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(StringUtil.RTrim(AV35UserId));
            AV32Popup.SetReturnParms(new Object[] {"AV35UserId",});
            context.NewWindow(AV32Popup);
            context.DoAjaxRefresh();
         }
      }

      protected void E12222( )
      {
         /* 'Search' Routine */
         AV15CurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
      }

      protected void E13222( )
      {
         /* 'First' Routine */
         AV15CurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
      }

      protected void E14222( )
      {
         /* 'Previous' Routine */
         if ( AV15CurrentPage > 1 )
         {
            AV15CurrentPage = (short)(AV15CurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
         }
      }

      protected void E15222( )
      {
         /* 'Next' Routine */
         AV15CurrentPage = (short)(AV15CurrentPage+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage), 4, 0)));
      }

      protected void wb_table1_2_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(144), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(350), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* WebComponent */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent");
            context.WriteHtmlText( " id=\""+"gxHTMLWrpW0005"+""+"\""+"") ;
            context.WriteHtmlText( ">") ;
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0005"+"");
            }
            WebComp_Wcmenu.componentdraw();
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_7_222( true) ;
         }
         else
         {
            wb_table2_7_222( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_222e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_14_222( true) ;
         }
         else
         {
            wb_table3_14_222( false) ;
         }
         return  ;
      }

      protected void wb_table3_14_222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:1px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_47_222( true) ;
         }
         else
         {
            wb_table4_47_222( false) ;
         }
         return  ;
      }

      protected void wb_table4_47_222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_222e( true) ;
         }
         else
         {
            wb_table1_2_222e( false) ;
         }
      }

      protected void wb_table4_47_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblgrid_Internalname, tblTblgrid_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top;height:16px")+"\" class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnadd_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(60), 2, 0)+","+"null"+");", "Add", bttBtnadd_Jsonclick, 5, "Add", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ADDNEW\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:2px")+"\" class='Table'>") ;
            wb_table5_54_222( true) ;
         }
         else
         {
            wb_table5_54_222( false) ;
         }
         return  ;
      }

      protected void wb_table5_54_222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:1633px")+"\">") ;
            /*  Grid Control  */
            GridwwContainer.SetWrapped(nGXWrapped);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"DivS\" data-gxgridid=\"60\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridww_Internalname, subGridww_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridww_Backcolorstyle == 0 )
               {
                  subGridww_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridww_Class) > 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Title";
                  }
               }
               else
               {
                  subGridww_Titlebackstyle = 1;
                  if ( subGridww_Backcolorstyle == 1 )
                  {
                     subGridww_Titlebackcolor = subGridww_Allbackcolor;
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "User Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+((edtavBtnupd_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Update") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+((edtavBtnrole_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Roles") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+((edtavBtnsetpwd_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Password") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+((edtavBtndlt_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Delete") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Authentication") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(150), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "First Name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(150), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Last Name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
            }
            else
            {
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
               GridwwContainer.AddObjectProperty("Class", "WorkWith");
               GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("CmpContext", "");
               GridwwContainer.AddObjectProperty("InMasterPage", "false");
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV35UserId));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUserid_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV14BtnUpd));
               GridwwColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnupd_Visible), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV12BtnRole));
               GridwwColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnrole_Visible), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV13BtnSetPwd));
               GridwwColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnsetpwd_Visible), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV9BtnDlt));
               GridwwColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtndlt_Visible), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV6AuthenticationTypeName));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAuthenticationtypename_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV31Name));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavName_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV29FirstName));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFirstname_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV30LastName));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLastname_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowselection), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Selectioncolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowhovering), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Hoveringcolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowcollapsing), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 60 )
         {
            wbEnd = 0;
            nRC_GXsfl_60 = (short)(nGXsfl_60_idx-1);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridww", GridwwContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData", GridwwContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData"+"V", GridwwContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridwwContainerData"+"V"+"\" value='"+GridwwContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td bgcolor=\"#C60B44\" colspan=\"2\"  class='Table'>") ;
            wb_table6_73_222( true) ;
         }
         else
         {
            wb_table6_73_222( false) ;
         }
         return  ;
      }

      protected void wb_table6_73_222e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_47_222e( true) ;
         }
         else
         {
            wb_table4_47_222e( false) ;
         }
      }

      protected void wb_table6_73_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgFirst_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgFirst_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'FIRST\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgPrevious_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgPrevious_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'PREVIOUS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgNext_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgNext_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgNext_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'NEXT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_73_222e( true) ;
         }
         else
         {
            wb_table6_73_222e( false) ;
         }
      }

      protected void wb_table5_54_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblwc_Internalname, tblTblwc_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:9px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_54_222e( true) ;
         }
         else
         {
            wb_table5_54_222e( false) ;
         }
      }

      protected void wb_table3_14_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblfilter2_Internalname, tblTblfilter2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbloginname2_Internalname, "Login Name", "", "", lblTbloginname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_60_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilname_Internalname, AV24FilName, StringUtil.RTrim( context.localUtil.Format( AV24FilName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfirstlastname2_Internalname, "First or Last Name", "", "", lblTbfirstlastname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_60_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilnames_Internalname, StringUtil.RTrim( AV25FilNames), StringUtil.RTrim( context.localUtil.Format( AV25FilNames, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilnames_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbemail2_Internalname, "Email", "", "", lblTbemail2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_60_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilemail_Internalname, AV20FilEmail, StringUtil.RTrim( context.localUtil.Format( AV20FilEmail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilemail_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMEMail", "left", true, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender2_Internalname, "Gender", "", "", lblTbgender2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_60_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFilusergender, cmbavFilusergender_Internalname, StringUtil.RTrim( AV28FilUserGender), 1, cmbavFilusergender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_GAMExampleWWUsers.htm");
            cmbavFilusergender.CurrentValue = StringUtil.RTrim( AV28FilUserGender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFilusergender_Internalname, "Values", (String)(cmbavFilusergender.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbauthtype2_Internalname, "Authentication Type", "", "", lblTbauthtype2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_60_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFilauttype, cmbavFilauttype_Internalname, StringUtil.RTrim( AV19FilAutType), 1, cmbavFilauttype_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_GAMExampleWWUsers.htm");
            cmbavFilauttype.CurrentValue = StringUtil.RTrim( AV19FilAutType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFilauttype_Internalname, "Values", (String)(cmbavFilauttype.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnsearch2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(60), 2, 0)+","+"null"+");", "Search", bttBtnsearch2_Jsonclick, 5, "Search", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SEARCH\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_14_222e( true) ;
         }
         else
         {
            wb_table3_14_222e( false) ;
         }
      }

      protected void wb_table2_7_222( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle2_Internalname, "Usu�rios", "", "", lblTbformtitle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:12.0pt; font-weight:bold; font-style:normal;", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleWWUsers.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_222e( true) ;
         }
         else
         {
            wb_table2_7_222e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA222( ) ;
         WS222( ) ;
         WE222( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
         {
            WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
            WebComp_Wcmenu.ComponentInit();
            WebComp_Wcmenu.Name = "GAMMenu";
            WebComp_Wcmenu_Component = "GAMMenu";
         }
         if ( ! ( WebComp_Wcmenu == null ) )
         {
            WebComp_Wcmenu.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311732677");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexamplewwusers.js", "?2020311732678");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_602( )
      {
         edtavUserid_Internalname = "vUSERID_"+sGXsfl_60_idx;
         edtavBtnupd_Internalname = "vBTNUPD_"+sGXsfl_60_idx;
         edtavBtnrole_Internalname = "vBTNROLE_"+sGXsfl_60_idx;
         edtavBtnsetpwd_Internalname = "vBTNSETPWD_"+sGXsfl_60_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_60_idx;
         edtavAuthenticationtypename_Internalname = "vAUTHENTICATIONTYPENAME_"+sGXsfl_60_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_60_idx;
         edtavFirstname_Internalname = "vFIRSTNAME_"+sGXsfl_60_idx;
         edtavLastname_Internalname = "vLASTNAME_"+sGXsfl_60_idx;
      }

      protected void SubsflControlProps_fel_602( )
      {
         edtavUserid_Internalname = "vUSERID_"+sGXsfl_60_fel_idx;
         edtavBtnupd_Internalname = "vBTNUPD_"+sGXsfl_60_fel_idx;
         edtavBtnrole_Internalname = "vBTNROLE_"+sGXsfl_60_fel_idx;
         edtavBtnsetpwd_Internalname = "vBTNSETPWD_"+sGXsfl_60_fel_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_60_fel_idx;
         edtavAuthenticationtypename_Internalname = "vAUTHENTICATIONTYPENAME_"+sGXsfl_60_fel_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_60_fel_idx;
         edtavFirstname_Internalname = "vFIRSTNAME_"+sGXsfl_60_fel_idx;
         edtavLastname_Internalname = "vLASTNAME_"+sGXsfl_60_fel_idx;
      }

      protected void sendrow_602( )
      {
         SubsflControlProps_602( ) ;
         WB220( ) ;
         GridwwRow = GXWebRow.GetNew(context,GridwwContainer);
         if ( subGridww_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridww_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
            {
               subGridww_Linesclass = subGridww_Class+"Odd";
            }
         }
         else if ( subGridww_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridww_Backstyle = 0;
            subGridww_Backcolor = subGridww_Allbackcolor;
            if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
            {
               subGridww_Linesclass = subGridww_Class+"Uniform";
            }
         }
         else if ( subGridww_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridww_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
            {
               subGridww_Linesclass = subGridww_Class+"Odd";
            }
            subGridww_Backcolor = (int)(0x0);
         }
         else if ( subGridww_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridww_Backstyle = 1;
            if ( ((int)((nGXsfl_60_idx) % (2))) == 0 )
            {
               subGridww_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Even";
               }
            }
            else
            {
               subGridww_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Odd";
               }
            }
         }
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridww_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_60_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavUserid_Enabled!=0)&&(edtavUserid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 61,'',false,'"+sGXsfl_60_idx+"',60)\"" : " ");
         ROClassString = "Attribute";
         GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUserid_Internalname,StringUtil.RTrim( AV35UserId),(String)"",TempTags+((edtavUserid_Enabled!=0)&&(edtavUserid_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavUserid_Enabled!=0)&&(edtavUserid_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,61);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUserid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavUserid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)60,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMGUID",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnupd_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavBtnupd_Enabled!=0)&&(edtavBtnupd_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 62,'',false,'',60)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV14BtnUpd_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14BtnUpd))&&String.IsNullOrEmpty(StringUtil.RTrim( AV43Btnupd_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14BtnUpd)));
         GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnupd_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14BtnUpd)) ? AV43Btnupd_GXI : context.PathToRelativeUrl( AV14BtnUpd)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtnupd_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnupd_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNUPD.CLICK."+sGXsfl_60_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV14BtnUpd_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnrole_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavBtnrole_Enabled!=0)&&(edtavBtnrole_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 63,'',false,'',60)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV12BtnRole_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV12BtnRole))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Btnrole_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV12BtnRole)));
         GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnrole_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV12BtnRole)) ? AV44Btnrole_GXI : context.PathToRelativeUrl( AV12BtnRole)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtnrole_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBtnrole_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e21222_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV12BtnRole_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnsetpwd_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavBtnsetpwd_Enabled!=0)&&(edtavBtnsetpwd_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 64,'',false,'',60)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV13BtnSetPwd_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV13BtnSetPwd))&&String.IsNullOrEmpty(StringUtil.RTrim( AV45Btnsetpwd_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV13BtnSetPwd)));
         GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnsetpwd_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV13BtnSetPwd)) ? AV45Btnsetpwd_GXI : context.PathToRelativeUrl( AV13BtnSetPwd)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtnsetpwd_Visible,(short)1,(String)"",(String)"Set new password",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBtnsetpwd_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e22222_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV13BtnSetPwd_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtndlt_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavBtndlt_Enabled!=0)&&(edtavBtndlt_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 65,'',false,'',60)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV9BtnDlt_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV9BtnDlt))&&String.IsNullOrEmpty(StringUtil.RTrim( AV46Btndlt_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV9BtnDlt)));
         GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtndlt_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV9BtnDlt)) ? AV46Btndlt_GXI : context.PathToRelativeUrl( AV9BtnDlt)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtndlt_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtndlt_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNDLT.CLICK."+sGXsfl_60_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV9BtnDlt_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavAuthenticationtypename_Enabled!=0)&&(edtavAuthenticationtypename_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 66,'',false,'"+sGXsfl_60_idx+"',60)\"" : " ");
         ROClassString = "Attribute";
         GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAuthenticationtypename_Internalname,StringUtil.RTrim( AV6AuthenticationTypeName),(String)"",TempTags+((edtavAuthenticationtypename_Enabled!=0)&&(edtavAuthenticationtypename_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAuthenticationtypename_Enabled!=0)&&(edtavAuthenticationtypename_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,66);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAuthenticationtypename_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAuthenticationtypename_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)60,(short)0,(short)0,(short)60,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionShort",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 67,'',false,'"+sGXsfl_60_idx+"',60)\"" : " ");
         ROClassString = "Attribute";
         GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavName_Internalname,StringUtil.RTrim( AV31Name),(String)"",TempTags+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,67);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+"e23222_client"+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavName_Jsonclick,(short)7,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavName_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)60,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionMedium",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavFirstname_Enabled!=0)&&(edtavFirstname_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 68,'',false,'"+sGXsfl_60_idx+"',60)\"" : " ");
         ROClassString = "Attribute";
         GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFirstname_Internalname,StringUtil.RTrim( AV29FirstName),(String)"",TempTags+((edtavFirstname_Enabled!=0)&&(edtavFirstname_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFirstname_Enabled!=0)&&(edtavFirstname_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,68);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFirstname_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFirstname_Enabled,(short)0,(String)"text",(String)"",(short)150,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)60,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionMedium",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridwwContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavLastname_Enabled!=0)&&(edtavLastname_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 69,'',false,'"+sGXsfl_60_idx+"',60)\"" : " ");
         ROClassString = "Attribute";
         GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLastname_Internalname,StringUtil.RTrim( AV30LastName),(String)"",TempTags+((edtavLastname_Enabled!=0)&&(edtavLastname_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLastname_Enabled!=0)&&(edtavLastname_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,69);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLastname_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLastname_Enabled,(short)0,(String)"text",(String)"",(short)150,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)60,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionMedium",(String)"left",(bool)true});
         GridwwContainer.AddRow(GridwwRow);
         sendsecurityrow_602( ) ;
         nGXsfl_60_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_60_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_60_idx+1));
         sGXsfl_60_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_60_idx), 4, 0)), 4, "0");
         SubsflControlProps_602( ) ;
         /* End function sendrow_602 */
      }

      protected void sendsecurityrow_602( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         /* End function sendsecurityrow_602 */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle2_Internalname = "TBFORMTITLE2";
         tblTable4_Internalname = "TABLE4";
         lblTbloginname2_Internalname = "TBLOGINNAME2";
         edtavFilname_Internalname = "vFILNAME";
         lblTbfirstlastname2_Internalname = "TBFIRSTLASTNAME2";
         edtavFilnames_Internalname = "vFILNAMES";
         lblTbemail2_Internalname = "TBEMAIL2";
         edtavFilemail_Internalname = "vFILEMAIL";
         lblTbgender2_Internalname = "TBGENDER2";
         cmbavFilusergender_Internalname = "vFILUSERGENDER";
         lblTbauthtype2_Internalname = "TBAUTHTYPE2";
         cmbavFilauttype_Internalname = "vFILAUTTYPE";
         bttBtnsearch2_Internalname = "BTNSEARCH2";
         tblTblfilter2_Internalname = "TBLFILTER2";
         bttBtnadd_Internalname = "BTNADD";
         tblTblwc_Internalname = "TBLWC";
         imgFirst_Internalname = "FIRST";
         imgPrevious_Internalname = "PREVIOUS";
         imgNext_Internalname = "NEXT";
         tblTable1_Internalname = "TABLE1";
         tblTblgrid_Internalname = "TBLGRID";
         tblTable3_Internalname = "TABLE3";
         edtavCurrentpage_Internalname = "vCURRENTPAGE";
         Form.Internalname = "FORM";
         subGridww_Internalname = "GRIDWW";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavLastname_Jsonclick = "";
         edtavLastname_Visible = -1;
         edtavFirstname_Jsonclick = "";
         edtavFirstname_Visible = -1;
         edtavName_Jsonclick = "";
         edtavName_Visible = -1;
         edtavAuthenticationtypename_Jsonclick = "";
         edtavAuthenticationtypename_Visible = -1;
         edtavBtndlt_Jsonclick = "";
         edtavBtndlt_Enabled = 1;
         edtavBtnsetpwd_Jsonclick = "";
         edtavBtnsetpwd_Enabled = 1;
         edtavBtnrole_Jsonclick = "";
         edtavBtnrole_Enabled = 1;
         edtavBtnupd_Jsonclick = "";
         edtavBtnupd_Enabled = 1;
         edtavUserid_Jsonclick = "";
         edtavUserid_Visible = 0;
         cmbavFilauttype_Jsonclick = "";
         cmbavFilusergender_Jsonclick = "";
         edtavFilemail_Jsonclick = "";
         edtavFilnames_Jsonclick = "";
         edtavFilname_Jsonclick = "";
         imgNext_Visible = 1;
         subGridww_Allowcollapsing = 0;
         subGridww_Allowselection = 0;
         edtavLastname_Enabled = 1;
         edtavFirstname_Enabled = 1;
         edtavName_Enabled = 1;
         edtavAuthenticationtypename_Enabled = 1;
         edtavUserid_Enabled = 1;
         edtavBtndlt_Visible = -1;
         edtavBtnsetpwd_Visible = -1;
         edtavBtnrole_Visible = -1;
         edtavBtnupd_Visible = -1;
         subGridww_Class = "WorkWith";
         subGridww_Backcolorstyle = 0;
         edtavCurrentpage_Jsonclick = "";
         edtavCurrentpage_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         Form = new GXWebForm();
         WebComp_Wcmenu_Component = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV35UserId = "";
         edtavUserid_Internalname = "";
         AV14BtnUpd = "";
         edtavBtnupd_Internalname = "";
         AV12BtnRole = "";
         edtavBtnrole_Internalname = "";
         AV13BtnSetPwd = "";
         edtavBtnsetpwd_Internalname = "";
         AV9BtnDlt = "";
         edtavBtndlt_Internalname = "";
         AV6AuthenticationTypeName = "";
         edtavAuthenticationtypename_Internalname = "";
         AV31Name = "";
         edtavName_Internalname = "";
         AV29FirstName = "";
         edtavFirstname_Internalname = "";
         AV30LastName = "";
         edtavLastname_Internalname = "";
         AV28FilUserGender = "";
         AV19FilAutType = "";
         GridwwContainer = new GXWebGrid( context);
         AV24FilName = "";
         AV25FilNames = "";
         AV20FilEmail = "";
         AV7AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationType", "GeneXus.Programs");
         AV27FilterAutType = new SdtGAMAuthenticationTypeFilter(context);
         AV18Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV5AuthenticationType = new SdtGAMAuthenticationType(context);
         AV33Repository = new SdtGAMRepository(context);
         AV26Filter = new SdtGAMUserFilter(context);
         AV41GXV2 = new GxExternalCollection( context, "SdtGAMUser", "GeneXus.Programs");
         AV34User = new SdtGAMUser(context);
         AV43Btnupd_GXI = "";
         AV44Btnrole_GXI = "";
         AV45Btnsetpwd_GXI = "";
         AV46Btndlt_GXI = "";
         AV16Email = "";
         AV32Popup = new GXWindow();
         scmdbuf = "";
         H00222_A1Usuario_Codigo = new int[1] ;
         H00222_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnadd_Jsonclick = "";
         subGridww_Linesclass = "";
         GridwwColumn = new GXWebColumn();
         imgFirst_Jsonclick = "";
         imgPrevious_Jsonclick = "";
         imgNext_Jsonclick = "";
         lblTbloginname2_Jsonclick = "";
         lblTbfirstlastname2_Jsonclick = "";
         lblTbemail2_Jsonclick = "";
         lblTbgender2_Jsonclick = "";
         lblTbauthtype2_Jsonclick = "";
         bttBtnsearch2_Jsonclick = "";
         lblTbformtitle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         GridwwRow = new GXWebRow();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexamplewwusers__default(),
            new Object[][] {
                new Object[] {
               H00222_A1Usuario_Codigo, H00222_A341Usuario_UserGamGuid
               }
            }
         );
         WebComp_Wcmenu = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUserid_Enabled = 0;
         edtavAuthenticationtypename_Enabled = 0;
         edtavName_Enabled = 0;
         edtavFirstname_Enabled = 0;
         edtavLastname_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_60 ;
      private short nGXsfl_60_idx=1 ;
      private short initialized ;
      private short AV37UsersXPage ;
      private short AV36CountUsers ;
      private short wbEnd ;
      private short wbStart ;
      private short AV15CurrentPage ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_60_Refreshing=0 ;
      private short subGridww_Backcolorstyle ;
      private short AV47GXLvl90 ;
      private short subGridww_Titlebackstyle ;
      private short subGridww_Allowselection ;
      private short subGridww_Allowhovering ;
      private short subGridww_Allowcollapsing ;
      private short subGridww_Collapsed ;
      private short nGXWrapped ;
      private short subGridww_Backstyle ;
      private short GRIDWW_nEOF ;
      private int edtavUserid_Enabled ;
      private int edtavAuthenticationtypename_Enabled ;
      private int edtavName_Enabled ;
      private int edtavFirstname_Enabled ;
      private int edtavLastname_Enabled ;
      private int edtavCurrentpage_Visible ;
      private int subGridww_Islastpage ;
      private int AV40GXV1 ;
      private int AV42GXV3 ;
      private int edtavBtnupd_Visible ;
      private int edtavBtnrole_Visible ;
      private int edtavBtnsetpwd_Visible ;
      private int edtavBtndlt_Visible ;
      private int imgNext_Visible ;
      private int subGridww_Titlebackcolor ;
      private int subGridww_Allbackcolor ;
      private int subGridww_Selectioncolor ;
      private int subGridww_Hoveringcolor ;
      private int idxLst ;
      private int subGridww_Backcolor ;
      private int edtavUserid_Visible ;
      private int edtavBtnupd_Enabled ;
      private int edtavBtnrole_Enabled ;
      private int edtavBtnsetpwd_Enabled ;
      private int edtavBtndlt_Enabled ;
      private int edtavAuthenticationtypename_Visible ;
      private int edtavName_Visible ;
      private int edtavFirstname_Visible ;
      private int edtavLastname_Visible ;
      private long GRIDWW_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_60_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavCurrentpage_Internalname ;
      private String edtavCurrentpage_Jsonclick ;
      private String WebComp_Wcmenu_Component ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV35UserId ;
      private String edtavUserid_Internalname ;
      private String edtavBtnupd_Internalname ;
      private String edtavBtnrole_Internalname ;
      private String edtavBtnsetpwd_Internalname ;
      private String edtavBtndlt_Internalname ;
      private String AV6AuthenticationTypeName ;
      private String edtavAuthenticationtypename_Internalname ;
      private String AV31Name ;
      private String edtavName_Internalname ;
      private String AV29FirstName ;
      private String edtavFirstname_Internalname ;
      private String AV30LastName ;
      private String edtavLastname_Internalname ;
      private String AV28FilUserGender ;
      private String AV19FilAutType ;
      private String edtavFilname_Internalname ;
      private String AV25FilNames ;
      private String edtavFilnames_Internalname ;
      private String edtavFilemail_Internalname ;
      private String cmbavFilusergender_Internalname ;
      private String cmbavFilauttype_Internalname ;
      private String imgNext_Internalname ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String tblTblgrid_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnadd_Internalname ;
      private String bttBtnadd_Jsonclick ;
      private String subGridww_Internalname ;
      private String subGridww_Class ;
      private String subGridww_Linesclass ;
      private String tblTable1_Internalname ;
      private String imgFirst_Internalname ;
      private String imgFirst_Jsonclick ;
      private String imgPrevious_Internalname ;
      private String imgPrevious_Jsonclick ;
      private String imgNext_Jsonclick ;
      private String tblTblwc_Internalname ;
      private String tblTblfilter2_Internalname ;
      private String lblTbloginname2_Internalname ;
      private String lblTbloginname2_Jsonclick ;
      private String edtavFilname_Jsonclick ;
      private String lblTbfirstlastname2_Internalname ;
      private String lblTbfirstlastname2_Jsonclick ;
      private String edtavFilnames_Jsonclick ;
      private String lblTbemail2_Internalname ;
      private String lblTbemail2_Jsonclick ;
      private String edtavFilemail_Jsonclick ;
      private String lblTbgender2_Internalname ;
      private String lblTbgender2_Jsonclick ;
      private String cmbavFilusergender_Jsonclick ;
      private String lblTbauthtype2_Internalname ;
      private String lblTbauthtype2_Jsonclick ;
      private String cmbavFilauttype_Jsonclick ;
      private String bttBtnsearch2_Internalname ;
      private String bttBtnsearch2_Jsonclick ;
      private String tblTable4_Internalname ;
      private String lblTbformtitle2_Internalname ;
      private String lblTbformtitle2_Jsonclick ;
      private String sGXsfl_60_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavUserid_Jsonclick ;
      private String edtavBtnupd_Jsonclick ;
      private String edtavBtnrole_Jsonclick ;
      private String edtavBtnsetpwd_Jsonclick ;
      private String edtavBtndlt_Jsonclick ;
      private String edtavAuthenticationtypename_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String edtavFirstname_Jsonclick ;
      private String edtavLastname_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14BtnUpd_IsBlob ;
      private bool AV12BtnRole_IsBlob ;
      private bool AV13BtnSetPwd_IsBlob ;
      private bool AV9BtnDlt_IsBlob ;
      private String AV24FilName ;
      private String AV20FilEmail ;
      private String AV43Btnupd_GXI ;
      private String AV44Btnrole_GXI ;
      private String AV45Btnsetpwd_GXI ;
      private String AV46Btndlt_GXI ;
      private String AV16Email ;
      private String AV14BtnUpd ;
      private String AV12BtnRole ;
      private String AV13BtnSetPwd ;
      private String AV9BtnDlt ;
      private GXWebComponent WebComp_Wcmenu ;
      private GXWebGrid GridwwContainer ;
      private GXWebRow GridwwRow ;
      private GXWebColumn GridwwColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavFilusergender ;
      private GXCombobox cmbavFilauttype ;
      private IDataStoreProvider pr_default ;
      private int[] H00222_A1Usuario_Codigo ;
      private String[] H00222_A341Usuario_UserGamGuid ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationType ))]
      private IGxCollection AV7AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV18Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMUser ))]
      private IGxCollection AV41GXV2 ;
      private GXWindow AV32Popup ;
      private SdtGAMAuthenticationType AV5AuthenticationType ;
      private SdtGAMUserFilter AV26Filter ;
      private SdtGAMAuthenticationTypeFilter AV27FilterAutType ;
      private SdtGAMRepository AV33Repository ;
      private SdtGAMUser AV34User ;
   }

   public class gamexamplewwusers__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00222 ;
          prmH00222 = new Object[] {
          new Object[] {"@AV35UserId",SqlDbType.Char,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00222", "SELECT TOP 1 [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_UserGamGuid] = @AV35UserId ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00222,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
