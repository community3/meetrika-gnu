/*
               File: PRC_QtdeServicosVinculados
        Description: Qtde Servicos Vinculados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:17.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_qtdeservicosvinculados : GXProcedure
   {
      public prc_qtdeservicosvinculados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_qtdeservicosvinculados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Codigo ,
                           out short aP1_Qtde )
      {
         this.AV8Servico_Codigo = aP0_Servico_Codigo;
         this.AV9Qtde = 0 ;
         initialize();
         executePrivate();
         aP1_Qtde=this.AV9Qtde;
      }

      public short executeUdp( int aP0_Servico_Codigo )
      {
         this.AV8Servico_Codigo = aP0_Servico_Codigo;
         this.AV9Qtde = 0 ;
         initialize();
         executePrivate();
         aP1_Qtde=this.AV9Qtde;
         return AV9Qtde ;
      }

      public void executeSubmit( int aP0_Servico_Codigo ,
                                 out short aP1_Qtde )
      {
         prc_qtdeservicosvinculados objprc_qtdeservicosvinculados;
         objprc_qtdeservicosvinculados = new prc_qtdeservicosvinculados();
         objprc_qtdeservicosvinculados.AV8Servico_Codigo = aP0_Servico_Codigo;
         objprc_qtdeservicosvinculados.AV9Qtde = 0 ;
         objprc_qtdeservicosvinculados.context.SetSubmitInitialConfig(context);
         objprc_qtdeservicosvinculados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_qtdeservicosvinculados);
         aP1_Qtde=this.AV9Qtde;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_qtdeservicosvinculados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004P3 */
         pr_default.execute(0, new Object[] {AV8Servico_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = P004P3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         AV9Qtde = (short)(A40000GXC1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004P3_A40000GXC1 = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_qtdeservicosvinculados__default(),
            new Object[][] {
                new Object[] {
               P004P3_A40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Qtde ;
      private int AV8Servico_Codigo ;
      private int A40000GXC1 ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P004P3_A40000GXC1 ;
      private short aP1_Qtde ;
   }

   public class prc_qtdeservicosvinculados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004P3 ;
          prmP004P3 = new Object[] {
          new Object[] {"@AV8Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004P3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Ativo] = 1) AND ([Servico_Vinculado] = @AV8Servico_Codigo) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004P3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
