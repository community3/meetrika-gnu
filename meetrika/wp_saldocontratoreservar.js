/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:23:31.75
*/
gx.evt.autoSkip = false;
gx.define('wp_saldocontratoreservar', false, function () {
   this.ServerClass =  "wp_saldocontratoreservar" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV11CheckRequiredFieldsResult=gx.fn.getControlValue("vCHECKREQUIREDFIELDSRESULT") ;
      this.AV13tipo=gx.fn.getControlValue("vTIPO") ;
      this.AV12SaldoContrato_Codigo=gx.fn.getIntegerValue("vSALDOCONTRATO_CODIGO",'.') ;
      this.AV16Contrato_Codigo=gx.fn.getIntegerValue("vCONTRATO_CODIGO",'.') ;
      this.AV17NotaEmpenho_Codigo=gx.fn.getIntegerValue("vNOTAEMPENHO_CODIGO",'.') ;
      this.AV19ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
      this.AV15SaldoContrato_Credito=gx.fn.getDecimalValue("vSALDOCONTRATO_CREDITO",'.',',') ;
      this.AV18Contagem_Codigo=gx.fn.getIntegerValue("vCONTAGEM_CODIGO",'.') ;
      this.AV6Messages=gx.fn.getControlValue("vMESSAGES") ;
   };
   this.s112_client=function()
   {
      this.AV11CheckRequiredFieldsResult =  true  ;
      if ( ((0.0==this.AV8AuxSaldoContrato_Reservado)) )
      {
         this.addMessage("Valor é obrigatório.");
         this.AV11CheckRequiredFieldsResult =  false  ;
      }
   };
   this.s122_client=function()
   {
      this.AV23GXV1 = gx.num.trunc( 1 ,0) ;
      while ( this.AV23GXV1 <= this.AV6Messages.length )
      {
         this.AV5Message =  this.AV6Messages[this.AV23GXV1 - 1]  ;
         this.addMessage(this.AV5Message.Description);
         this.AV23GXV1 = gx.num.trunc( this.AV23GXV1 + 1 ,0) ;
      }
   };
   this.e12mr2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e15mr1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,8,16,19,21,24,26,29,31,36,39,41,44];
   this.GXLastCtrlId =44;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 14, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Valores", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   this.DVPANEL_TABLERESERVARContainer = gx.uc.getNew(this, 34, 21, "BootstrapPanel", "DVPANEL_TABLERESERVARContainer", "Dvpanel_tablereservar");
   var DVPANEL_TABLERESERVARContainer = this.DVPANEL_TABLERESERVARContainer;
   DVPANEL_TABLERESERVARContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLERESERVARContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLERESERVARContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLERESERVARContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLERESERVARContainer.setDynProp("Title", "Title", "Reservar", "str");
   DVPANEL_TABLERESERVARContainer.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLERESERVARContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLERESERVARContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLERESERVARContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLERESERVARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLERESERVARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TEXTBLOCKTITLE", format:0,grid:0};
   GXValidFnc[8]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[16]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[19]={fld:"TEXTBLOCKSALDOCONTRATO_SALDO", format:0,grid:0};
   GXValidFnc[21]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSALDOCONTRATO_SALDO",gxz:"ZV9SaldoContrato_Saldo",gxold:"OV9SaldoContrato_Saldo",gxvar:"AV9SaldoContrato_Saldo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV9SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV9SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vSALDOCONTRATO_SALDO",gx.O.AV9SaldoContrato_Saldo,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV9SaldoContrato_Saldo=this.val()},val:function(){return gx.fn.getDecimalValue("vSALDOCONTRATO_SALDO",'.',',')},nac:gx.falseFn};
   GXValidFnc[24]={fld:"TEXTBLOCKSALDOCONTRATO_RESERVADO", format:0,grid:0};
   GXValidFnc[26]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSALDOCONTRATO_RESERVADO",gxz:"ZV10SaldoContrato_Reservado",gxold:"OV10SaldoContrato_Reservado",gxvar:"AV10SaldoContrato_Reservado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV10SaldoContrato_Reservado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV10SaldoContrato_Reservado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vSALDOCONTRATO_RESERVADO",gx.O.AV10SaldoContrato_Reservado,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV10SaldoContrato_Reservado=this.val()},val:function(){return gx.fn.getDecimalValue("vSALDOCONTRATO_RESERVADO",'.',',')},nac:gx.falseFn};
   GXValidFnc[29]={fld:"TEXTBLOCKSALDOCONTRATO_DISPONIVEL", format:0,grid:0};
   GXValidFnc[31]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSALDOCONTRATO_DISPONIVEL",gxz:"ZV14SaldoContrato_Disponivel",gxold:"OV14SaldoContrato_Disponivel",gxvar:"AV14SaldoContrato_Disponivel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14SaldoContrato_Disponivel=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV14SaldoContrato_Disponivel=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vSALDOCONTRATO_DISPONIVEL",gx.O.AV14SaldoContrato_Disponivel,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV14SaldoContrato_Disponivel=this.val()},val:function(){return gx.fn.getDecimalValue("vSALDOCONTRATO_DISPONIVEL",'.',',')},nac:gx.falseFn};
   GXValidFnc[36]={fld:"TABLERESERVAR",grid:0};
   GXValidFnc[39]={fld:"TEXTBLOCKAUXSALDOCONTRATO_RESERVADO", format:0,grid:0};
   GXValidFnc[41]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vAUXSALDOCONTRATO_RESERVADO",gxz:"ZV8AuxSaldoContrato_Reservado",gxold:"OV8AuxSaldoContrato_Reservado",gxvar:"AV8AuxSaldoContrato_Reservado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV8AuxSaldoContrato_Reservado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV8AuxSaldoContrato_Reservado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vAUXSALDOCONTRATO_RESERVADO",gx.O.AV8AuxSaldoContrato_Reservado,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV8AuxSaldoContrato_Reservado=this.val()},val:function(){return gx.fn.getDecimalValue("vAUXSALDOCONTRATO_RESERVADO",'.',',')},nac:gx.falseFn};
   GXValidFnc[44]={fld:"TABLEACTIONS",grid:0};
   this.AV9SaldoContrato_Saldo = 0 ;
   this.ZV9SaldoContrato_Saldo = 0 ;
   this.OV9SaldoContrato_Saldo = 0 ;
   this.AV10SaldoContrato_Reservado = 0 ;
   this.ZV10SaldoContrato_Reservado = 0 ;
   this.OV10SaldoContrato_Reservado = 0 ;
   this.AV14SaldoContrato_Disponivel = 0 ;
   this.ZV14SaldoContrato_Disponivel = 0 ;
   this.OV14SaldoContrato_Disponivel = 0 ;
   this.AV8AuxSaldoContrato_Reservado = 0 ;
   this.ZV8AuxSaldoContrato_Reservado = 0 ;
   this.OV8AuxSaldoContrato_Reservado = 0 ;
   this.AV9SaldoContrato_Saldo = 0 ;
   this.AV10SaldoContrato_Reservado = 0 ;
   this.AV14SaldoContrato_Disponivel = 0 ;
   this.AV8AuxSaldoContrato_Reservado = 0 ;
   this.AV12SaldoContrato_Codigo = 0 ;
   this.AV16Contrato_Codigo = 0 ;
   this.AV17NotaEmpenho_Codigo = 0 ;
   this.AV18Contagem_Codigo = 0 ;
   this.AV15SaldoContrato_Credito = 0 ;
   this.AV13tipo = "" ;
   this.A1561SaldoContrato_Codigo = 0 ;
   this.A1574SaldoContrato_Reservado = 0 ;
   this.A1576SaldoContrato_Saldo = 0 ;
   this.AV11CheckRequiredFieldsResult = false ;
   this.AV19ContagemResultado_Codigo = 0 ;
   this.AV6Messages = [ ] ;
   this.AV5Message = {} ;
   this.AV23GXV1 = 0 ;
   this.Events = {"e12mr2_client": ["ENTER", true] ,"e15mr1_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["ENTER"] = [[{av:'AV11CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV13tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV8AuxSaldoContrato_Reservado',fld:'vAUXSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV14SaldoContrato_Disponivel',fld:'vSALDOCONTRATO_DISPONIVEL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV12SaldoContrato_Codigo',fld:'vSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9SaldoContrato_Saldo',fld:'vSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV10SaldoContrato_Reservado',fld:'vSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV15SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Messages',fld:'vMESSAGES',pic:'',nv:null}],[{av:'AV11CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV11CheckRequiredFieldsResult", "vCHECKREQUIREDFIELDSRESULT", 0, "boolean");
   this.setVCMap("AV13tipo", "vTIPO", 0, "char");
   this.setVCMap("AV12SaldoContrato_Codigo", "vSALDOCONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV16Contrato_Codigo", "vCONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV17NotaEmpenho_Codigo", "vNOTAEMPENHO_CODIGO", 0, "int");
   this.setVCMap("AV19ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV15SaldoContrato_Credito", "vSALDOCONTRATO_CREDITO", 0, "decimal");
   this.setVCMap("AV18Contagem_Codigo", "vCONTAGEM_CODIGO", 0, "int");
   this.setVCMap("AV6Messages", "vMESSAGES", 0, "CollMessages.Message");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_saldocontratoreservar);
