/*
               File: GetSistemaFronteiraFilterData
        Description: Get Sistema Fronteira Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:40.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsistemafronteirafilterdata : GXProcedure
   {
      public getsistemafronteirafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsistemafronteirafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsistemafronteirafilterdata objgetsistemafronteirafilterdata;
         objgetsistemafronteirafilterdata = new getsistemafronteirafilterdata();
         objgetsistemafronteirafilterdata.AV22DDOName = aP0_DDOName;
         objgetsistemafronteirafilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetsistemafronteirafilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetsistemafronteirafilterdata.AV26OptionsJson = "" ;
         objgetsistemafronteirafilterdata.AV29OptionsDescJson = "" ;
         objgetsistemafronteirafilterdata.AV31OptionIndexesJson = "" ;
         objgetsistemafronteirafilterdata.context.SetSubmitInitialConfig(context);
         objgetsistemafronteirafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsistemafronteirafilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsistemafronteirafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_FUNCAODADOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("SistemaFronteiraGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SistemaFronteiraGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("SistemaFronteiraGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "FUNCAODADOS_TIPO") == 0 )
            {
               AV38FuncaoDados_Tipo = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV10TFFuncaoDados_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV11TFFuncaoDados_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV12TFFuncaoDados_Complexidade_SelsJson = AV36GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoDados_Complexidade_Sels.FromJSonString(AV12TFFuncaoDados_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV14TFFuncaoDados_DER = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV15TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV16TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV17TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV18TFFuncaoDados_PF = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, ".");
               AV19TFFuncaoDados_PF_To = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAODADOS_SISTEMACOD") == 0 )
            {
               AV44FuncaoDados_SistemaCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV40FuncaoDados_Nome1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV41DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV42DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV43FuncaoDados_Nome2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAODADOS_NOMEOPTIONS' Routine */
         AV10TFFuncaoDados_Nome = AV20SearchTxt;
         AV11TFFuncaoDados_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV13TFFuncaoDados_Complexidade_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40FuncaoDados_Nome1 ,
                                              AV41DynamicFiltersEnabled2 ,
                                              AV42DynamicFiltersSelector2 ,
                                              AV43FuncaoDados_Nome2 ,
                                              AV11TFFuncaoDados_Nome_Sel ,
                                              AV10TFFuncaoDados_Nome ,
                                              A369FuncaoDados_Nome ,
                                              AV13TFFuncaoDados_Complexidade_Sels.Count ,
                                              AV14TFFuncaoDados_DER ,
                                              A374FuncaoDados_DER ,
                                              AV15TFFuncaoDados_DER_To ,
                                              AV16TFFuncaoDados_RLR ,
                                              A375FuncaoDados_RLR ,
                                              AV17TFFuncaoDados_RLR_To ,
                                              AV18TFFuncaoDados_PF ,
                                              A377FuncaoDados_PF ,
                                              AV19TFFuncaoDados_PF_To ,
                                              A373FuncaoDados_Tipo ,
                                              AV44FuncaoDados_SistemaCod ,
                                              A370FuncaoDados_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV40FuncaoDados_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV40FuncaoDados_Nome1), "%", "");
         lV43FuncaoDados_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV43FuncaoDados_Nome2), "%", "");
         lV10TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoDados_Nome), "%", "");
         /* Using cursor P00FX2 */
         pr_default.execute(0, new Object[] {AV44FuncaoDados_SistemaCod, AV13TFFuncaoDados_Complexidade_Sels.Count, lV40FuncaoDados_Nome1, lV43FuncaoDados_Nome2, lV10TFFuncaoDados_Nome, AV11TFFuncaoDados_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFX2 = false;
            A370FuncaoDados_SistemaCod = P00FX2_A370FuncaoDados_SistemaCod[0];
            A373FuncaoDados_Tipo = P00FX2_A373FuncaoDados_Tipo[0];
            A369FuncaoDados_Nome = P00FX2_A369FuncaoDados_Nome[0];
            A755FuncaoDados_Contar = P00FX2_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00FX2_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00FX2_A368FuncaoDados_Codigo[0];
            GXt_char1 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
            A376FuncaoDados_Complexidade = GXt_char1;
            if ( ( AV13TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV13TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
            {
               GXt_int2 = A374FuncaoDados_DER;
               new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
               A374FuncaoDados_DER = GXt_int2;
               if ( (0==AV14TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV14TFFuncaoDados_DER ) ) )
               {
                  if ( (0==AV15TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV15TFFuncaoDados_DER_To ) ) )
                  {
                     GXt_int2 = A375FuncaoDados_RLR;
                     new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                     A375FuncaoDados_RLR = GXt_int2;
                     if ( (0==AV16TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV16TFFuncaoDados_RLR ) ) )
                     {
                        if ( (0==AV17TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV17TFFuncaoDados_RLR_To ) ) )
                        {
                           if ( A755FuncaoDados_Contar )
                           {
                              GXt_int2 = (short)(A377FuncaoDados_PF);
                              new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                              A377FuncaoDados_PF = (decimal)(GXt_int2);
                           }
                           else
                           {
                              A377FuncaoDados_PF = 0;
                           }
                           if ( (Convert.ToDecimal(0)==AV18TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV18TFFuncaoDados_PF ) ) )
                           {
                              if ( (Convert.ToDecimal(0)==AV19TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV19TFFuncaoDados_PF_To ) ) )
                              {
                                 AV32count = 0;
                                 while ( (pr_default.getStatus(0) != 101) && ( P00FX2_A370FuncaoDados_SistemaCod[0] == A370FuncaoDados_SistemaCod ) && ( StringUtil.StrCmp(P00FX2_A369FuncaoDados_Nome[0], A369FuncaoDados_Nome) == 0 ) )
                                 {
                                    BRKFX2 = false;
                                    A368FuncaoDados_Codigo = P00FX2_A368FuncaoDados_Codigo[0];
                                    AV32count = (long)(AV32count+1);
                                    BRKFX2 = true;
                                    pr_default.readNext(0);
                                 }
                                 if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
                                 {
                                    AV24Option = A369FuncaoDados_Nome;
                                    AV25Options.Add(AV24Option, 0);
                                    AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                 }
                                 if ( AV25Options.Count == 50 )
                                 {
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFX2 )
            {
               BRKFX2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV38FuncaoDados_Tipo = "";
         AV10TFFuncaoDados_Nome = "";
         AV11TFFuncaoDados_Nome_Sel = "";
         AV12TFFuncaoDados_Complexidade_SelsJson = "";
         AV13TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV40FuncaoDados_Nome1 = "";
         AV42DynamicFiltersSelector2 = "";
         AV43FuncaoDados_Nome2 = "";
         scmdbuf = "";
         lV40FuncaoDados_Nome1 = "";
         lV43FuncaoDados_Nome2 = "";
         lV10TFFuncaoDados_Nome = "";
         A376FuncaoDados_Complexidade = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         P00FX2_A370FuncaoDados_SistemaCod = new int[1] ;
         P00FX2_A373FuncaoDados_Tipo = new String[] {""} ;
         P00FX2_A369FuncaoDados_Nome = new String[] {""} ;
         P00FX2_A755FuncaoDados_Contar = new bool[] {false} ;
         P00FX2_n755FuncaoDados_Contar = new bool[] {false} ;
         P00FX2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsistemafronteirafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FX2_A370FuncaoDados_SistemaCod, P00FX2_A373FuncaoDados_Tipo, P00FX2_A369FuncaoDados_Nome, P00FX2_A755FuncaoDados_Contar, P00FX2_n755FuncaoDados_Contar, P00FX2_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFFuncaoDados_DER ;
      private short AV15TFFuncaoDados_DER_To ;
      private short AV16TFFuncaoDados_RLR ;
      private short AV17TFFuncaoDados_RLR_To ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int2 ;
      private int AV47GXV1 ;
      private int AV44FuncaoDados_SistemaCod ;
      private int AV13TFFuncaoDados_Complexidade_Sels_Count ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private long AV32count ;
      private decimal AV18TFFuncaoDados_PF ;
      private decimal AV19TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private String AV38FuncaoDados_Tipo ;
      private String scmdbuf ;
      private String A376FuncaoDados_Complexidade ;
      private String A373FuncaoDados_Tipo ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV41DynamicFiltersEnabled2 ;
      private bool BRKFX2 ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV12TFFuncaoDados_Complexidade_SelsJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV10TFFuncaoDados_Nome ;
      private String AV11TFFuncaoDados_Nome_Sel ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV40FuncaoDados_Nome1 ;
      private String AV42DynamicFiltersSelector2 ;
      private String AV43FuncaoDados_Nome2 ;
      private String lV40FuncaoDados_Nome1 ;
      private String lV43FuncaoDados_Nome2 ;
      private String lV10TFFuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FX2_A370FuncaoDados_SistemaCod ;
      private String[] P00FX2_A373FuncaoDados_Tipo ;
      private String[] P00FX2_A369FuncaoDados_Nome ;
      private bool[] P00FX2_A755FuncaoDados_Contar ;
      private bool[] P00FX2_n755FuncaoDados_Contar ;
      private int[] P00FX2_A368FuncaoDados_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getsistemafronteirafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FX2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV13TFFuncaoDados_Complexidade_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             String AV40FuncaoDados_Nome1 ,
                                             bool AV41DynamicFiltersEnabled2 ,
                                             String AV42DynamicFiltersSelector2 ,
                                             String AV43FuncaoDados_Nome2 ,
                                             String AV11TFFuncaoDados_Nome_Sel ,
                                             String AV10TFFuncaoDados_Nome ,
                                             String A369FuncaoDados_Nome ,
                                             int AV13TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV14TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV15TFFuncaoDados_DER_To ,
                                             short AV16TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV17TFFuncaoDados_RLR_To ,
                                             decimal AV18TFFuncaoDados_PF ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV19TFFuncaoDados_PF_To ,
                                             String A373FuncaoDados_Tipo ,
                                             int AV44FuncaoDados_SistemaCod ,
                                             int A370FuncaoDados_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoDados_SistemaCod] = @AV44FuncaoDados_SistemaCod)";
         scmdbuf = scmdbuf + " and ([FuncaoDados_Tipo] = 'ALI')";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoDados_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Nome] like '%' + @lV40FuncaoDados_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43FuncaoDados_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Nome] like '%' + @lV43FuncaoDados_Nome2)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoDados_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_SistemaCod], [FuncaoDados_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FX2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FX2 ;
          prmP00FX2 = new Object[] {
          new Object[] {"@AV44FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV40FuncaoDados_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV43FuncaoDados_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV10TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FX2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsistemafronteirafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsistemafronteirafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsistemafronteirafilterdata") )
          {
             return  ;
          }
          getsistemafronteirafilterdata worker = new getsistemafronteirafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
