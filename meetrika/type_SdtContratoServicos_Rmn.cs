/*
               File: type_SdtContratoServicos_Rmn
        Description: Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:54:44.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicos.Rmn" )]
   [XmlType(TypeName =  "ContratoServicos.Rmn" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoServicos_Rmn : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtContratoServicos_Rmn( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicos_Rmn_Mode = "";
      }

      public SdtContratoServicos_Rmn( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Rmn");
         metadata.Set("BT", "ContratoServicosRmn");
         metadata.Set("PK", "[ \"ContratoServicosRmn_Sequencial\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoServicosRmn_Sequencial\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosrmn_sequencial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosrmn_inicio_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosrmn_fim_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosrmn_valor_Z_double" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicos_Rmn deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicos_Rmn)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicos_Rmn obj ;
         obj = this;
         obj.gxTpr_Contratoservicosrmn_sequencial = deserialized.gxTpr_Contratoservicosrmn_sequencial;
         obj.gxTpr_Contratoservicosrmn_inicio = deserialized.gxTpr_Contratoservicosrmn_inicio;
         obj.gxTpr_Contratoservicosrmn_fim = deserialized.gxTpr_Contratoservicosrmn_fim;
         obj.gxTpr_Contratoservicosrmn_valor = deserialized.gxTpr_Contratoservicosrmn_valor;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicosrmn_sequencial_Z = deserialized.gxTpr_Contratoservicosrmn_sequencial_Z;
         obj.gxTpr_Contratoservicosrmn_inicio_Z = deserialized.gxTpr_Contratoservicosrmn_inicio_Z;
         obj.gxTpr_Contratoservicosrmn_fim_Z = deserialized.gxTpr_Contratoservicosrmn_fim_Z;
         obj.gxTpr_Contratoservicosrmn_valor_Z = deserialized.gxTpr_Contratoservicosrmn_valor_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Sequencial") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Inicio") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Fim") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Valor") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicos_Rmn_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtContratoServicos_Rmn_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicos_Rmn_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Sequencial_Z") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Inicio_Z") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Fim_Z") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosRmn_Valor_Z") )
               {
                  gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicos.Rmn";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicosRmn_Sequencial", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosRmn_Inicio", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosRmn_Fim", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosRmn_Valor", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicos_Rmn_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Rmn_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Rmn_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosRmn_Sequencial_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosRmn_Inicio_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosRmn_Fim_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosRmn_Valor_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicosRmn_Sequencial", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial, false);
         AddObjectProperty("ContratoServicosRmn_Inicio", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio, false);
         AddObjectProperty("ContratoServicosRmn_Fim", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim, false);
         AddObjectProperty("ContratoServicosRmn_Valor", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicos_Rmn_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtContratoServicos_Rmn_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicos_Rmn_Initialized, false);
            AddObjectProperty("ContratoServicosRmn_Sequencial_Z", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z, false);
            AddObjectProperty("ContratoServicosRmn_Inicio_Z", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z, false);
            AddObjectProperty("ContratoServicosRmn_Fim_Z", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z, false);
            AddObjectProperty("ContratoServicosRmn_Valor_Z", gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Sequencial" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Sequencial"   )]
      public short gxTpr_Contratoservicosrmn_sequencial
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial = (short)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Inicio" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Inicio"   )]
      public double gxTpr_Contratoservicosrmn_inicio_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_inicio
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Fim" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Fim"   )]
      public double gxTpr_Contratoservicosrmn_fim_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_fim
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Valor" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Valor"   )]
      public double gxTpr_Contratoservicosrmn_valor_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_valor
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Mode ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Mode_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Modified ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Modified = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Modified_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Initialized ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Initialized = (short)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Sequencial_Z" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Sequencial_Z"   )]
      public short gxTpr_Contratoservicosrmn_sequencial_Z
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z = (short)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Inicio_Z" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Inicio_Z"   )]
      public double gxTpr_Contratoservicosrmn_inicio_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_inicio_Z
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Fim_Z" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Fim_Z"   )]
      public double gxTpr_Contratoservicosrmn_fim_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_fim_Z
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosRmn_Valor_Z" )]
      [  XmlElement( ElementName = "ContratoServicosRmn_Valor_Z"   )]
      public double gxTpr_Contratoservicosrmn_valor_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z) ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z = (decimal)(Convert.ToDecimal( value));
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratoservicosrmn_valor_Z
      {
         get {
            return gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z ;
         }

         set {
            gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z = (decimal)(value);
            gxTv_SdtContratoServicos_Rmn_Modified = 1;
         }

      }

      public void gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z_SetNull( )
      {
         gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicos_Rmn_Mode = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial ;
      private short gxTv_SdtContratoServicos_Rmn_Modified ;
      private short gxTv_SdtContratoServicos_Rmn_Initialized ;
      private short gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_sequencial_Z ;
      private short readOk ;
      private short nOutParmCount ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_inicio_Z ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_fim_Z ;
      private decimal gxTv_SdtContratoServicos_Rmn_Contratoservicosrmn_valor_Z ;
      private String gxTv_SdtContratoServicos_Rmn_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoServicos.Rmn", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoServicos_Rmn_RESTInterface : GxGenericCollectionItem<SdtContratoServicos_Rmn>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicos_Rmn_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicos_Rmn_RESTInterface( SdtContratoServicos_Rmn psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicosRmn_Sequencial" , Order = 0 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratoservicosrmn_sequencial
      {
         get {
            return sdt.gxTpr_Contratoservicosrmn_sequencial ;
         }

         set {
            sdt.gxTpr_Contratoservicosrmn_sequencial = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosRmn_Inicio" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosrmn_inicio
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicosrmn_inicio, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contratoservicosrmn_inicio = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicosRmn_Fim" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosrmn_fim
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicosrmn_fim, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contratoservicosrmn_fim = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratoServicosRmn_Valor" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosrmn_valor
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratoservicosrmn_valor, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contratoservicosrmn_valor = NumberUtil.Val( (String)(value), ".");
         }

      }

      public SdtContratoServicos_Rmn sdt
      {
         get {
            return (SdtContratoServicos_Rmn)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicos_Rmn() ;
         }
      }

   }

}
