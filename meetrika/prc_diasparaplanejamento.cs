/*
               File: PRC_DiasParaPlanejamento
        Description: Dias Para Planejamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:7.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diasparaplanejamento : GXProcedure
   {
      public prc_diasparaplanejamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diasparaplanejamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                           decimal aP1_Unidades ,
                           short aP2_DiasComplexidade ,
                           out short aP3_Dias )
      {
         this.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         this.AV14Unidades = aP1_Unidades;
         this.AV10DiasComplexidade = aP2_DiasComplexidade;
         this.AV9Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV9Dias;
      }

      public short executeUdp( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                               decimal aP1_Unidades ,
                               short aP2_DiasComplexidade )
      {
         this.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         this.AV14Unidades = aP1_Unidades;
         this.AV10DiasComplexidade = aP2_DiasComplexidade;
         this.AV9Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV9Dias;
         return AV9Dias ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                                 decimal aP1_Unidades ,
                                 short aP2_DiasComplexidade ,
                                 out short aP3_Dias )
      {
         prc_diasparaplanejamento objprc_diasparaplanejamento;
         objprc_diasparaplanejamento = new prc_diasparaplanejamento();
         objprc_diasparaplanejamento.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         objprc_diasparaplanejamento.AV14Unidades = aP1_Unidades;
         objprc_diasparaplanejamento.AV10DiasComplexidade = aP2_DiasComplexidade;
         objprc_diasparaplanejamento.AV9Dias = 0 ;
         objprc_diasparaplanejamento.context.SetSubmitInitialConfig(context);
         objprc_diasparaplanejamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diasparaplanejamento);
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV9Dias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diasparaplanejamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DF2 */
         pr_default.execute(0, new Object[] {A903ContratoServicosPrazo_CntSrvCod, AV14Unidades});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1735ContratoServicosPrazoPlnj_Fim = P00DF2_A1735ContratoServicosPrazoPlnj_Fim[0];
            A1734ContratoServicosPrazoPlnj_Inicio = P00DF2_A1734ContratoServicosPrazoPlnj_Inicio[0];
            A1736ContratoServicosPrazoPlnj_Dias = P00DF2_A1736ContratoServicosPrazoPlnj_Dias[0];
            A1724ContratoServicosPrazoPlnj_Sequencial = P00DF2_A1724ContratoServicosPrazoPlnj_Sequencial[0];
            AV9Dias = A1736ContratoServicosPrazoPlnj_Dias;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DF2_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00DF2_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         P00DF2_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         P00DF2_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         P00DF2_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_diasparaplanejamento__default(),
            new Object[][] {
                new Object[] {
               P00DF2_A903ContratoServicosPrazo_CntSrvCod, P00DF2_A1735ContratoServicosPrazoPlnj_Fim, P00DF2_A1734ContratoServicosPrazoPlnj_Inicio, P00DF2_A1736ContratoServicosPrazoPlnj_Dias, P00DF2_A1724ContratoServicosPrazoPlnj_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10DiasComplexidade ;
      private short AV9Dias ;
      private short A1736ContratoServicosPrazoPlnj_Dias ;
      private short A1724ContratoServicosPrazoPlnj_Sequencial ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private decimal AV14Unidades ;
      private decimal A1735ContratoServicosPrazoPlnj_Fim ;
      private decimal A1734ContratoServicosPrazoPlnj_Inicio ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosPrazo_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00DF2_A903ContratoServicosPrazo_CntSrvCod ;
      private decimal[] P00DF2_A1735ContratoServicosPrazoPlnj_Fim ;
      private decimal[] P00DF2_A1734ContratoServicosPrazoPlnj_Inicio ;
      private short[] P00DF2_A1736ContratoServicosPrazoPlnj_Dias ;
      private short[] P00DF2_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private short aP3_Dias ;
   }

   public class prc_diasparaplanejamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DF2 ;
          prmP00DF2 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Unidades",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DF2", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Dias], [ContratoServicosPrazoPlnj_Sequencial] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE ([ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod) AND ([ContratoServicosPrazoPlnj_Inicio] <= @AV14Unidades) AND ([ContratoServicosPrazoPlnj_Fim] = 0 or [ContratoServicosPrazoPlnj_Fim] >= @AV14Unidades) ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DF2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                return;
       }
    }

 }

}
