/*
               File: UnidadeMedicao
        Description: Unidades de Medi��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:45:38.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class unidademedicao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7UnidadeMedicao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UnidadeMedicao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUNIDADEMEDICAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UnidadeMedicao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkUnidadeMedicao_Ativo.Name = "UNIDADEMEDICAO_ATIVO";
         chkUnidadeMedicao_Ativo.WebTags = "";
         chkUnidadeMedicao_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeMedicao_Ativo_Internalname, "TitleCaption", chkUnidadeMedicao_Ativo.Caption);
         chkUnidadeMedicao_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Unidades de Medi��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public unidademedicao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public unidademedicao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_UnidadeMedicao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7UnidadeMedicao_Codigo = aP1_UnidadeMedicao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkUnidadeMedicao_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3B140( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3B140e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUnidadeMedicao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0, ",", "")), ((edtUnidadeMedicao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeMedicao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUnidadeMedicao_Codigo_Visible, edtUnidadeMedicao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UnidadeMedicao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3B140( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3B140( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3B140e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_32_3B140( true) ;
         }
         return  ;
      }

      protected void wb_table3_32_3B140e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3B140e( true) ;
         }
         else
         {
            wb_table1_2_3B140e( false) ;
         }
      }

      protected void wb_table3_32_3B140( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_32_3B140e( true) ;
         }
         else
         {
            wb_table3_32_3B140e( false) ;
         }
      }

      protected void wb_table2_5_3B140( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3B140( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3B140e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3B140e( true) ;
         }
         else
         {
            wb_table2_5_3B140e( false) ;
         }
      }

      protected void wb_table4_13_3B140( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidademedicao_nome_Internalname, "Nome", "", "", lblTextblockunidademedicao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUnidadeMedicao_Nome_Internalname, StringUtil.RTrim( A1198UnidadeMedicao_Nome), StringUtil.RTrim( context.localUtil.Format( A1198UnidadeMedicao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeMedicao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUnidadeMedicao_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidademedicao_sigla_Internalname, "Sigla", "", "", lblTextblockunidademedicao_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUnidadeMedicao_Sigla_Internalname, StringUtil.RTrim( A1200UnidadeMedicao_Sigla), StringUtil.RTrim( context.localUtil.Format( A1200UnidadeMedicao_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUnidadeMedicao_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUnidadeMedicao_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockunidademedicao_ativo_Internalname, "Ativo?", "", "", lblTextblockunidademedicao_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockunidademedicao_ativo_Visible, 1, 0, "HLP_UnidadeMedicao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkUnidadeMedicao_Ativo_Internalname, StringUtil.BoolToStr( A1199UnidadeMedicao_Ativo), "", "", chkUnidadeMedicao_Ativo.Visible, chkUnidadeMedicao_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(27, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3B140e( true) ;
         }
         else
         {
            wb_table4_13_3B140e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113B2 */
         E113B2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1198UnidadeMedicao_Nome = StringUtil.Upper( cgiGet( edtUnidadeMedicao_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1198UnidadeMedicao_Nome", A1198UnidadeMedicao_Nome);
               A1200UnidadeMedicao_Sigla = StringUtil.Upper( cgiGet( edtUnidadeMedicao_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1200UnidadeMedicao_Sigla", A1200UnidadeMedicao_Sigla);
               A1199UnidadeMedicao_Ativo = StringUtil.StrToBool( cgiGet( chkUnidadeMedicao_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
               A1197UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeMedicao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
               /* Read saved values. */
               Z1197UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1197UnidadeMedicao_Codigo"), ",", "."));
               Z1198UnidadeMedicao_Nome = cgiGet( "Z1198UnidadeMedicao_Nome");
               Z1200UnidadeMedicao_Sigla = cgiGet( "Z1200UnidadeMedicao_Sigla");
               Z1199UnidadeMedicao_Ativo = StringUtil.StrToBool( cgiGet( "Z1199UnidadeMedicao_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vUNIDADEMEDICAO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "UnidadeMedicao";
               A1197UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeMedicao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1197UnidadeMedicao_Codigo != Z1197UnidadeMedicao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("unidademedicao:[SecurityCheckFailed value for]"+"UnidadeMedicao_Codigo:"+context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("unidademedicao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1197UnidadeMedicao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode140 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode140;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound140 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3B0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "UNIDADEMEDICAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtUnidadeMedicao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113B2 */
                           E113B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123B2 */
                           E123B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123B2 */
            E123B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3B140( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3B140( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3B0( )
      {
         BeforeValidate3B140( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3B140( ) ;
            }
            else
            {
               CheckExtendedTable3B140( ) ;
               CloseExtendedTableCursors3B140( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3B0( )
      {
      }

      protected void E113B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtUnidadeMedicao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Codigo_Visible), 5, 0)));
      }

      protected void E123B2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwunidademedicao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3B140( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1198UnidadeMedicao_Nome = T003B3_A1198UnidadeMedicao_Nome[0];
               Z1200UnidadeMedicao_Sigla = T003B3_A1200UnidadeMedicao_Sigla[0];
               Z1199UnidadeMedicao_Ativo = T003B3_A1199UnidadeMedicao_Ativo[0];
            }
            else
            {
               Z1198UnidadeMedicao_Nome = A1198UnidadeMedicao_Nome;
               Z1200UnidadeMedicao_Sigla = A1200UnidadeMedicao_Sigla;
               Z1199UnidadeMedicao_Ativo = A1199UnidadeMedicao_Ativo;
            }
         }
         if ( GX_JID == -6 )
         {
            Z1197UnidadeMedicao_Codigo = A1197UnidadeMedicao_Codigo;
            Z1198UnidadeMedicao_Nome = A1198UnidadeMedicao_Nome;
            Z1200UnidadeMedicao_Sigla = A1200UnidadeMedicao_Sigla;
            Z1199UnidadeMedicao_Ativo = A1199UnidadeMedicao_Ativo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtUnidadeMedicao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtUnidadeMedicao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7UnidadeMedicao_Codigo) )
         {
            A1197UnidadeMedicao_Codigo = AV7UnidadeMedicao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         lblTextblockunidademedicao_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockunidademedicao_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockunidademedicao_ativo_Visible), 5, 0)));
         chkUnidadeMedicao_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeMedicao_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUnidadeMedicao_Ativo.Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1199UnidadeMedicao_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1199UnidadeMedicao_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
         }
      }

      protected void Load3B140( )
      {
         /* Using cursor T003B4 */
         pr_default.execute(2, new Object[] {A1197UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound140 = 1;
            A1198UnidadeMedicao_Nome = T003B4_A1198UnidadeMedicao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1198UnidadeMedicao_Nome", A1198UnidadeMedicao_Nome);
            A1200UnidadeMedicao_Sigla = T003B4_A1200UnidadeMedicao_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1200UnidadeMedicao_Sigla", A1200UnidadeMedicao_Sigla);
            A1199UnidadeMedicao_Ativo = T003B4_A1199UnidadeMedicao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
            ZM3B140( -6) ;
         }
         pr_default.close(2);
         OnLoadActions3B140( ) ;
      }

      protected void OnLoadActions3B140( )
      {
      }

      protected void CheckExtendedTable3B140( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors3B140( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3B140( )
      {
         /* Using cursor T003B5 */
         pr_default.execute(3, new Object[] {A1197UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound140 = 1;
         }
         else
         {
            RcdFound140 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003B3 */
         pr_default.execute(1, new Object[] {A1197UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3B140( 6) ;
            RcdFound140 = 1;
            A1197UnidadeMedicao_Codigo = T003B3_A1197UnidadeMedicao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
            A1198UnidadeMedicao_Nome = T003B3_A1198UnidadeMedicao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1198UnidadeMedicao_Nome", A1198UnidadeMedicao_Nome);
            A1200UnidadeMedicao_Sigla = T003B3_A1200UnidadeMedicao_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1200UnidadeMedicao_Sigla", A1200UnidadeMedicao_Sigla);
            A1199UnidadeMedicao_Ativo = T003B3_A1199UnidadeMedicao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
            Z1197UnidadeMedicao_Codigo = A1197UnidadeMedicao_Codigo;
            sMode140 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3B140( ) ;
            if ( AnyError == 1 )
            {
               RcdFound140 = 0;
               InitializeNonKey3B140( ) ;
            }
            Gx_mode = sMode140;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound140 = 0;
            InitializeNonKey3B140( ) ;
            sMode140 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode140;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3B140( ) ;
         if ( RcdFound140 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound140 = 0;
         /* Using cursor T003B6 */
         pr_default.execute(4, new Object[] {A1197UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T003B6_A1197UnidadeMedicao_Codigo[0] < A1197UnidadeMedicao_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T003B6_A1197UnidadeMedicao_Codigo[0] > A1197UnidadeMedicao_Codigo ) ) )
            {
               A1197UnidadeMedicao_Codigo = T003B6_A1197UnidadeMedicao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
               RcdFound140 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound140 = 0;
         /* Using cursor T003B7 */
         pr_default.execute(5, new Object[] {A1197UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T003B7_A1197UnidadeMedicao_Codigo[0] > A1197UnidadeMedicao_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T003B7_A1197UnidadeMedicao_Codigo[0] < A1197UnidadeMedicao_Codigo ) ) )
            {
               A1197UnidadeMedicao_Codigo = T003B7_A1197UnidadeMedicao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
               RcdFound140 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3B140( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3B140( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound140 == 1 )
            {
               if ( A1197UnidadeMedicao_Codigo != Z1197UnidadeMedicao_Codigo )
               {
                  A1197UnidadeMedicao_Codigo = Z1197UnidadeMedicao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "UNIDADEMEDICAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUnidadeMedicao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3B140( ) ;
                  GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1197UnidadeMedicao_Codigo != Z1197UnidadeMedicao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3B140( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "UNIDADEMEDICAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUnidadeMedicao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3B140( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1197UnidadeMedicao_Codigo != Z1197UnidadeMedicao_Codigo )
         {
            A1197UnidadeMedicao_Codigo = Z1197UnidadeMedicao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "UNIDADEMEDICAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUnidadeMedicao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtUnidadeMedicao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3B140( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003B2 */
            pr_default.execute(0, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UnidadeMedicao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1198UnidadeMedicao_Nome, T003B2_A1198UnidadeMedicao_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z1200UnidadeMedicao_Sigla, T003B2_A1200UnidadeMedicao_Sigla[0]) != 0 ) || ( Z1199UnidadeMedicao_Ativo != T003B2_A1199UnidadeMedicao_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z1198UnidadeMedicao_Nome, T003B2_A1198UnidadeMedicao_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("unidademedicao:[seudo value changed for attri]"+"UnidadeMedicao_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1198UnidadeMedicao_Nome);
                  GXUtil.WriteLogRaw("Current: ",T003B2_A1198UnidadeMedicao_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z1200UnidadeMedicao_Sigla, T003B2_A1200UnidadeMedicao_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("unidademedicao:[seudo value changed for attri]"+"UnidadeMedicao_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z1200UnidadeMedicao_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T003B2_A1200UnidadeMedicao_Sigla[0]);
               }
               if ( Z1199UnidadeMedicao_Ativo != T003B2_A1199UnidadeMedicao_Ativo[0] )
               {
                  GXUtil.WriteLog("unidademedicao:[seudo value changed for attri]"+"UnidadeMedicao_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1199UnidadeMedicao_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T003B2_A1199UnidadeMedicao_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UnidadeMedicao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3B140( )
      {
         BeforeValidate3B140( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3B140( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3B140( 0) ;
            CheckOptimisticConcurrency3B140( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3B140( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3B140( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003B8 */
                     pr_default.execute(6, new Object[] {A1198UnidadeMedicao_Nome, A1200UnidadeMedicao_Sigla, A1199UnidadeMedicao_Ativo});
                     A1197UnidadeMedicao_Codigo = T003B8_A1197UnidadeMedicao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("UnidadeMedicao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3B0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3B140( ) ;
            }
            EndLevel3B140( ) ;
         }
         CloseExtendedTableCursors3B140( ) ;
      }

      protected void Update3B140( )
      {
         BeforeValidate3B140( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3B140( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3B140( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3B140( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3B140( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003B9 */
                     pr_default.execute(7, new Object[] {A1198UnidadeMedicao_Nome, A1200UnidadeMedicao_Sigla, A1199UnidadeMedicao_Ativo, A1197UnidadeMedicao_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("UnidadeMedicao") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UnidadeMedicao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3B140( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3B140( ) ;
         }
         CloseExtendedTableCursors3B140( ) ;
      }

      protected void DeferredUpdate3B140( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3B140( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3B140( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3B140( ) ;
            AfterConfirm3B140( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3B140( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003B10 */
                  pr_default.execute(8, new Object[] {A1197UnidadeMedicao_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("UnidadeMedicao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode140 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3B140( ) ;
         Gx_mode = sMode140;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3B140( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T003B11 */
            pr_default.execute(9, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades de Convers�o do Servi�os do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T003B12 */
            pr_default.execute(10, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T179"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T003B13 */
            pr_default.execute(11, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T197"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T003B14 */
            pr_default.execute(12, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T003B15 */
            pr_default.execute(13, new Object[] {A1197UnidadeMedicao_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades Contratadas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel3B140( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3B140( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "UnidadeMedicao");
            if ( AnyError == 0 )
            {
               ConfirmValues3B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "UnidadeMedicao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3B140( )
      {
         /* Scan By routine */
         /* Using cursor T003B16 */
         pr_default.execute(14);
         RcdFound140 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound140 = 1;
            A1197UnidadeMedicao_Codigo = T003B16_A1197UnidadeMedicao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3B140( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound140 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound140 = 1;
            A1197UnidadeMedicao_Codigo = T003B16_A1197UnidadeMedicao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3B140( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm3B140( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3B140( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3B140( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3B140( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3B140( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3B140( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3B140( )
      {
         edtUnidadeMedicao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Nome_Enabled), 5, 0)));
         edtUnidadeMedicao_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Sigla_Enabled), 5, 0)));
         chkUnidadeMedicao_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUnidadeMedicao_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkUnidadeMedicao_Ativo.Enabled), 5, 0)));
         edtUnidadeMedicao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUnidadeMedicao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUnidadeMedicao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120453938");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("unidademedicao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UnidadeMedicao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1197UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1198UnidadeMedicao_Nome", StringUtil.RTrim( Z1198UnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, "Z1200UnidadeMedicao_Sigla", StringUtil.RTrim( Z1200UnidadeMedicao_Sigla));
         GxWebStd.gx_boolean_hidden_field( context, "Z1199UnidadeMedicao_Ativo", Z1199UnidadeMedicao_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vUNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUNIDADEMEDICAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UnidadeMedicao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "UnidadeMedicao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("unidademedicao:[SendSecurityCheck value for]"+"UnidadeMedicao_Codigo:"+context.localUtil.Format( (decimal)(A1197UnidadeMedicao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("unidademedicao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("unidademedicao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UnidadeMedicao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "UnidadeMedicao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Unidades de Medi��o" ;
      }

      protected void InitializeNonKey3B140( )
      {
         A1198UnidadeMedicao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1198UnidadeMedicao_Nome", A1198UnidadeMedicao_Nome);
         A1200UnidadeMedicao_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1200UnidadeMedicao_Sigla", A1200UnidadeMedicao_Sigla);
         A1199UnidadeMedicao_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
         Z1198UnidadeMedicao_Nome = "";
         Z1200UnidadeMedicao_Sigla = "";
         Z1199UnidadeMedicao_Ativo = false;
      }

      protected void InitAll3B140( )
      {
         A1197UnidadeMedicao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1197UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0)));
         InitializeNonKey3B140( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1199UnidadeMedicao_Ativo = i1199UnidadeMedicao_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1199UnidadeMedicao_Ativo", A1199UnidadeMedicao_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120453949");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("unidademedicao.js", "?20203120453949");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockunidademedicao_nome_Internalname = "TEXTBLOCKUNIDADEMEDICAO_NOME";
         edtUnidadeMedicao_Nome_Internalname = "UNIDADEMEDICAO_NOME";
         lblTextblockunidademedicao_sigla_Internalname = "TEXTBLOCKUNIDADEMEDICAO_SIGLA";
         edtUnidadeMedicao_Sigla_Internalname = "UNIDADEMEDICAO_SIGLA";
         lblTextblockunidademedicao_ativo_Internalname = "TEXTBLOCKUNIDADEMEDICAO_ATIVO";
         chkUnidadeMedicao_Ativo_Internalname = "UNIDADEMEDICAO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtUnidadeMedicao_Codigo_Internalname = "UNIDADEMEDICAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Unidade de Medi��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Unidades de Medi��o";
         chkUnidadeMedicao_Ativo.Enabled = 1;
         chkUnidadeMedicao_Ativo.Visible = 1;
         lblTextblockunidademedicao_ativo_Visible = 1;
         edtUnidadeMedicao_Sigla_Jsonclick = "";
         edtUnidadeMedicao_Sigla_Enabled = 1;
         edtUnidadeMedicao_Nome_Jsonclick = "";
         edtUnidadeMedicao_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtUnidadeMedicao_Codigo_Jsonclick = "";
         edtUnidadeMedicao_Codigo_Enabled = 0;
         edtUnidadeMedicao_Codigo_Visible = 1;
         chkUnidadeMedicao_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7UnidadeMedicao_Codigo',fld:'vUNIDADEMEDICAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123B2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1198UnidadeMedicao_Nome = "";
         Z1200UnidadeMedicao_Sigla = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockunidademedicao_nome_Jsonclick = "";
         A1198UnidadeMedicao_Nome = "";
         lblTextblockunidademedicao_sigla_Jsonclick = "";
         A1200UnidadeMedicao_Sigla = "";
         lblTextblockunidademedicao_ativo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode140 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T003B4_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B4_A1198UnidadeMedicao_Nome = new String[] {""} ;
         T003B4_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         T003B4_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         T003B5_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B3_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B3_A1198UnidadeMedicao_Nome = new String[] {""} ;
         T003B3_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         T003B3_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         T003B6_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B7_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B2_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B2_A1198UnidadeMedicao_Nome = new String[] {""} ;
         T003B2_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         T003B2_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         T003B8_A1197UnidadeMedicao_Codigo = new int[1] ;
         T003B11_A160ContratoServicos_Codigo = new int[1] ;
         T003B11_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         T003B12_A1561SaldoContrato_Codigo = new int[1] ;
         T003B13_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T003B14_A160ContratoServicos_Codigo = new int[1] ;
         T003B15_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003B15_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003B16_A1197UnidadeMedicao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.unidademedicao__default(),
            new Object[][] {
                new Object[] {
               T003B2_A1197UnidadeMedicao_Codigo, T003B2_A1198UnidadeMedicao_Nome, T003B2_A1200UnidadeMedicao_Sigla, T003B2_A1199UnidadeMedicao_Ativo
               }
               , new Object[] {
               T003B3_A1197UnidadeMedicao_Codigo, T003B3_A1198UnidadeMedicao_Nome, T003B3_A1200UnidadeMedicao_Sigla, T003B3_A1199UnidadeMedicao_Ativo
               }
               , new Object[] {
               T003B4_A1197UnidadeMedicao_Codigo, T003B4_A1198UnidadeMedicao_Nome, T003B4_A1200UnidadeMedicao_Sigla, T003B4_A1199UnidadeMedicao_Ativo
               }
               , new Object[] {
               T003B5_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003B6_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003B7_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003B8_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003B11_A160ContratoServicos_Codigo, T003B11_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               T003B12_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003B13_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T003B14_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003B15_A1207ContratoUnidades_ContratoCod, T003B15_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003B16_A1197UnidadeMedicao_Codigo
               }
            }
         );
         Z1199UnidadeMedicao_Ativo = true;
         A1199UnidadeMedicao_Ativo = true;
         i1199UnidadeMedicao_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound140 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7UnidadeMedicao_Codigo ;
      private int Z1197UnidadeMedicao_Codigo ;
      private int AV7UnidadeMedicao_Codigo ;
      private int trnEnded ;
      private int A1197UnidadeMedicao_Codigo ;
      private int edtUnidadeMedicao_Codigo_Enabled ;
      private int edtUnidadeMedicao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtUnidadeMedicao_Nome_Enabled ;
      private int edtUnidadeMedicao_Sigla_Enabled ;
      private int lblTextblockunidademedicao_ativo_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1198UnidadeMedicao_Nome ;
      private String Z1200UnidadeMedicao_Sigla ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkUnidadeMedicao_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtUnidadeMedicao_Nome_Internalname ;
      private String edtUnidadeMedicao_Codigo_Internalname ;
      private String edtUnidadeMedicao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockunidademedicao_nome_Internalname ;
      private String lblTextblockunidademedicao_nome_Jsonclick ;
      private String A1198UnidadeMedicao_Nome ;
      private String edtUnidadeMedicao_Nome_Jsonclick ;
      private String lblTextblockunidademedicao_sigla_Internalname ;
      private String lblTextblockunidademedicao_sigla_Jsonclick ;
      private String edtUnidadeMedicao_Sigla_Internalname ;
      private String A1200UnidadeMedicao_Sigla ;
      private String edtUnidadeMedicao_Sigla_Jsonclick ;
      private String lblTextblockunidademedicao_ativo_Internalname ;
      private String lblTextblockunidademedicao_ativo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode140 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z1199UnidadeMedicao_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1199UnidadeMedicao_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i1199UnidadeMedicao_Ativo ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkUnidadeMedicao_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T003B4_A1197UnidadeMedicao_Codigo ;
      private String[] T003B4_A1198UnidadeMedicao_Nome ;
      private String[] T003B4_A1200UnidadeMedicao_Sigla ;
      private bool[] T003B4_A1199UnidadeMedicao_Ativo ;
      private int[] T003B5_A1197UnidadeMedicao_Codigo ;
      private int[] T003B3_A1197UnidadeMedicao_Codigo ;
      private String[] T003B3_A1198UnidadeMedicao_Nome ;
      private String[] T003B3_A1200UnidadeMedicao_Sigla ;
      private bool[] T003B3_A1199UnidadeMedicao_Ativo ;
      private int[] T003B6_A1197UnidadeMedicao_Codigo ;
      private int[] T003B7_A1197UnidadeMedicao_Codigo ;
      private int[] T003B2_A1197UnidadeMedicao_Codigo ;
      private String[] T003B2_A1198UnidadeMedicao_Nome ;
      private String[] T003B2_A1200UnidadeMedicao_Sigla ;
      private bool[] T003B2_A1199UnidadeMedicao_Ativo ;
      private int[] T003B8_A1197UnidadeMedicao_Codigo ;
      private int[] T003B11_A160ContratoServicos_Codigo ;
      private int[] T003B11_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] T003B12_A1561SaldoContrato_Codigo ;
      private int[] T003B13_A1774AutorizacaoConsumo_Codigo ;
      private int[] T003B14_A160ContratoServicos_Codigo ;
      private int[] T003B15_A1207ContratoUnidades_ContratoCod ;
      private int[] T003B15_A1204ContratoUnidades_UndMedCod ;
      private int[] T003B16_A1197UnidadeMedicao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class unidademedicao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003B4 ;
          prmT003B4 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B5 ;
          prmT003B5 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B3 ;
          prmT003B3 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B6 ;
          prmT003B6 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B7 ;
          prmT003B7 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B2 ;
          prmT003B2 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B8 ;
          prmT003B8 = new Object[] {
          new Object[] {"@UnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@UnidadeMedicao_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT003B9 ;
          prmT003B9 = new Object[] {
          new Object[] {"@UnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@UnidadeMedicao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B10 ;
          prmT003B10 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B11 ;
          prmT003B11 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B12 ;
          prmT003B12 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B13 ;
          prmT003B13 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B14 ;
          prmT003B14 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B15 ;
          prmT003B15 = new Object[] {
          new Object[] {"@UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003B16 ;
          prmT003B16 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T003B2", "SELECT [UnidadeMedicao_Codigo], [UnidadeMedicao_Nome], [UnidadeMedicao_Sigla], [UnidadeMedicao_Ativo] FROM [UnidadeMedicao] WITH (UPDLOCK) WHERE [UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B2,1,0,true,false )
             ,new CursorDef("T003B3", "SELECT [UnidadeMedicao_Codigo], [UnidadeMedicao_Nome], [UnidadeMedicao_Sigla], [UnidadeMedicao_Ativo] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B3,1,0,true,false )
             ,new CursorDef("T003B4", "SELECT TM1.[UnidadeMedicao_Codigo], TM1.[UnidadeMedicao_Nome], TM1.[UnidadeMedicao_Sigla], TM1.[UnidadeMedicao_Ativo] FROM [UnidadeMedicao] TM1 WITH (NOLOCK) WHERE TM1.[UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo ORDER BY TM1.[UnidadeMedicao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003B4,100,0,true,false )
             ,new CursorDef("T003B5", "SELECT [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003B5,1,0,true,false )
             ,new CursorDef("T003B6", "SELECT TOP 1 [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE ( [UnidadeMedicao_Codigo] > @UnidadeMedicao_Codigo) ORDER BY [UnidadeMedicao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003B6,1,0,true,true )
             ,new CursorDef("T003B7", "SELECT TOP 1 [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE ( [UnidadeMedicao_Codigo] < @UnidadeMedicao_Codigo) ORDER BY [UnidadeMedicao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003B7,1,0,true,true )
             ,new CursorDef("T003B8", "INSERT INTO [UnidadeMedicao]([UnidadeMedicao_Nome], [UnidadeMedicao_Sigla], [UnidadeMedicao_Ativo]) VALUES(@UnidadeMedicao_Nome, @UnidadeMedicao_Sigla, @UnidadeMedicao_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003B8)
             ,new CursorDef("T003B9", "UPDATE [UnidadeMedicao] SET [UnidadeMedicao_Nome]=@UnidadeMedicao_Nome, [UnidadeMedicao_Sigla]=@UnidadeMedicao_Sigla, [UnidadeMedicao_Ativo]=@UnidadeMedicao_Ativo  WHERE [UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo", GxErrorMask.GX_NOMASK,prmT003B9)
             ,new CursorDef("T003B10", "DELETE FROM [UnidadeMedicao]  WHERE [UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo", GxErrorMask.GX_NOMASK,prmT003B10)
             ,new CursorDef("T003B11", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicosUnidConversao_Codigo] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B11,1,0,true,true )
             ,new CursorDef("T003B12", "SELECT TOP 1 [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_UnidadeMedicao_Codigo] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B12,1,0,true,true )
             ,new CursorDef("T003B13", "SELECT TOP 1 [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [AutorizacaoConsumo_UnidadeMedicaoCod] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B13,1,0,true,true )
             ,new CursorDef("T003B14", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_UnidadeContratada] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B14,1,0,true,true )
             ,new CursorDef("T003B15", "SELECT TOP 1 [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_UndMedCod] = @UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003B15,1,0,true,true )
             ,new CursorDef("T003B16", "SELECT [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK) ORDER BY [UnidadeMedicao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003B16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
