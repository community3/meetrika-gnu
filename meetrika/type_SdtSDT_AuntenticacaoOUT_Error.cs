/*
               File: type_SdtSDT_AuntenticacaoOUT_Error
        Description: SDT_AuntenticacaoOUT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:58.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_AuntenticacaoOUT.Error" )]
   [XmlType(TypeName =  "SDT_AuntenticacaoOUT.Error" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_AuntenticacaoOUT_Error : GxUserType
   {
      public SdtSDT_AuntenticacaoOUT_Error( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription = "";
      }

      public SdtSDT_AuntenticacaoOUT_Error( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_AuntenticacaoOUT_Error deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_AuntenticacaoOUT_Error)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_AuntenticacaoOUT_Error obj ;
         obj = this;
         obj.gxTpr_Errorcode = deserialized.gxTpr_Errorcode;
         obj.gxTpr_Errordescription = deserialized.gxTpr_Errordescription;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ErrorCode") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ErrorDescription") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_AuntenticacaoOUT.Error";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ErrorCode", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ErrorDescription", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ErrorCode", gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode, false);
         AddObjectProperty("ErrorDescription", gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription, false);
         return  ;
      }

      [  SoapElement( ElementName = "ErrorCode" )]
      [  XmlElement( ElementName = "ErrorCode"   )]
      public long gxTpr_Errorcode
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ErrorDescription" )]
      [  XmlElement( ElementName = "ErrorDescription"   )]
      public String gxTpr_Errordescription
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected long gxTv_SdtSDT_AuntenticacaoOUT_Error_Errorcode ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Error_Errordescription ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_AuntenticacaoOUT.Error", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_AuntenticacaoOUT_Error_RESTInterface : GxGenericCollectionItem<SdtSDT_AuntenticacaoOUT_Error>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_AuntenticacaoOUT_Error_RESTInterface( ) : base()
      {
      }

      public SdtSDT_AuntenticacaoOUT_Error_RESTInterface( SdtSDT_AuntenticacaoOUT_Error psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ErrorCode" , Order = 0 )]
      public String gxTpr_Errorcode
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Errorcode), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Errorcode = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ErrorDescription" , Order = 1 )]
      public String gxTpr_Errordescription
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Errordescription) ;
         }

         set {
            sdt.gxTpr_Errordescription = (String)(value);
         }

      }

      public SdtSDT_AuntenticacaoOUT_Error sdt
      {
         get {
            return (SdtSDT_AuntenticacaoOUT_Error)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_AuntenticacaoOUT_Error() ;
         }
      }

   }

}
