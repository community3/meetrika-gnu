/*
               File: PRC_GetGpoObjCtrl_Responsavel
        Description: Get Gpo Obj Ctrl_Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:13.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getgpoobjctrl_responsavel : GXProcedure
   {
      public prc_getgpoobjctrl_responsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getgpoobjctrl_responsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref short aP0_GpoObjCtrlResponsavel_GpoCod ,
                           out int aP1_GpoObjCtrl_Responsavel )
      {
         this.A1838GpoObjCtrlResponsavel_GpoCod = aP0_GpoObjCtrlResponsavel_GpoCod;
         this.AV10GpoObjCtrl_Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_GpoObjCtrlResponsavel_GpoCod=this.A1838GpoObjCtrlResponsavel_GpoCod;
         aP1_GpoObjCtrl_Responsavel=this.AV10GpoObjCtrl_Responsavel;
      }

      public int executeUdp( ref short aP0_GpoObjCtrlResponsavel_GpoCod )
      {
         this.A1838GpoObjCtrlResponsavel_GpoCod = aP0_GpoObjCtrlResponsavel_GpoCod;
         this.AV10GpoObjCtrl_Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_GpoObjCtrlResponsavel_GpoCod=this.A1838GpoObjCtrlResponsavel_GpoCod;
         aP1_GpoObjCtrl_Responsavel=this.AV10GpoObjCtrl_Responsavel;
         return AV10GpoObjCtrl_Responsavel ;
      }

      public void executeSubmit( ref short aP0_GpoObjCtrlResponsavel_GpoCod ,
                                 out int aP1_GpoObjCtrl_Responsavel )
      {
         prc_getgpoobjctrl_responsavel objprc_getgpoobjctrl_responsavel;
         objprc_getgpoobjctrl_responsavel = new prc_getgpoobjctrl_responsavel();
         objprc_getgpoobjctrl_responsavel.A1838GpoObjCtrlResponsavel_GpoCod = aP0_GpoObjCtrlResponsavel_GpoCod;
         objprc_getgpoobjctrl_responsavel.AV10GpoObjCtrl_Responsavel = 0 ;
         objprc_getgpoobjctrl_responsavel.context.SetSubmitInitialConfig(context);
         objprc_getgpoobjctrl_responsavel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getgpoobjctrl_responsavel);
         aP0_GpoObjCtrlResponsavel_GpoCod=this.A1838GpoObjCtrlResponsavel_GpoCod;
         aP1_GpoObjCtrl_Responsavel=this.AV10GpoObjCtrl_Responsavel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getgpoobjctrl_responsavel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Using cursor P00SY3 */
         pr_default.execute(0, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, AV9WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1837GpoObjCtrlResponsavel_Codigo = P00SY3_A1837GpoObjCtrlResponsavel_Codigo[0];
            A1835GpoObjCtrlResponsavel_CteUsrCod = P00SY3_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
            n1835GpoObjCtrlResponsavel_CteUsrCod = P00SY3_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
            A1836GpoObjCtrlResponsavel_CteAreaCod = P00SY3_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            n1836GpoObjCtrlResponsavel_CteAreaCod = P00SY3_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
            A1836GpoObjCtrlResponsavel_CteAreaCod = P00SY3_A1836GpoObjCtrlResponsavel_CteAreaCod[0];
            n1836GpoObjCtrlResponsavel_CteAreaCod = P00SY3_n1836GpoObjCtrlResponsavel_CteAreaCod[0];
            AV10GpoObjCtrl_Responsavel = A1835GpoObjCtrlResponsavel_CteUsrCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00SY3_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         P00SY3_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         P00SY3_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         P00SY3_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         P00SY3_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         P00SY3_A1836GpoObjCtrlResponsavel_CteAreaCod = new int[1] ;
         P00SY3_n1836GpoObjCtrlResponsavel_CteAreaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getgpoobjctrl_responsavel__default(),
            new Object[][] {
                new Object[] {
               P00SY3_A1837GpoObjCtrlResponsavel_Codigo, P00SY3_A1838GpoObjCtrlResponsavel_GpoCod, P00SY3_n1838GpoObjCtrlResponsavel_GpoCod, P00SY3_A1835GpoObjCtrlResponsavel_CteUsrCod, P00SY3_n1835GpoObjCtrlResponsavel_CteUsrCod, P00SY3_A1836GpoObjCtrlResponsavel_CteAreaCod, P00SY3_n1836GpoObjCtrlResponsavel_CteAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1838GpoObjCtrlResponsavel_GpoCod ;
      private int AV10GpoObjCtrl_Responsavel ;
      private int A1837GpoObjCtrlResponsavel_Codigo ;
      private int A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private String scmdbuf ;
      private bool n1838GpoObjCtrlResponsavel_GpoCod ;
      private bool n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP0_GpoObjCtrlResponsavel_GpoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00SY3_A1837GpoObjCtrlResponsavel_Codigo ;
      private short[] P00SY3_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] P00SY3_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] P00SY3_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] P00SY3_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] P00SY3_A1836GpoObjCtrlResponsavel_CteAreaCod ;
      private bool[] P00SY3_n1836GpoObjCtrlResponsavel_CteAreaCod ;
      private int aP1_GpoObjCtrl_Responsavel ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class prc_getgpoobjctrl_responsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SY3 ;
          prmP00SY3 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SY3", "SELECT TOP 1 T1.[GpoObjCtrlResponsavel_Codigo], T1.[GpoObjCtrlResponsavel_GpoCod], T1.[GpoObjCtrlResponsavel_CteUsrCod], COALESCE( T2.[GpoObjCtrlResponsavel_CteAreaCod], 0) AS GpoObjCtrlResponsavel_CteAreaCod FROM ([GpoObjCtrlResponsavel] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS GpoObjCtrlResponsavel_CteAreaCod, T4.[GpoObjCtrlResponsavel_Codigo] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [GpoObjCtrlResponsavel] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[GpoObjCtrlResponsavel_CteCteCod] GROUP BY T4.[GpoObjCtrlResponsavel_Codigo] ) T2 ON T2.[GpoObjCtrlResponsavel_Codigo] = T1.[GpoObjCtrlResponsavel_Codigo]) WHERE (T1.[GpoObjCtrlResponsavel_GpoCod] = @GpoObjCtrlResponsavel_GpoCod) AND (COALESCE( T2.[GpoObjCtrlResponsavel_CteAreaCod], 0) = @AV9WWPCo_1Areatrabalho_codigo) ORDER BY T1.[GpoObjCtrlResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SY3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
