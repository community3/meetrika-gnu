/*
               File: DP_ContadoresFS
        Description: Contadores FS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:37.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_contadoresfs : GXProcedure
   {
      public dp_contadoresfs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_contadoresfs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratadaUsuario_ContratadaCod ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Usuarios.Usuario", "GxEv3Up14_Meetrika", "SdtSDT_Usuarios_Usuario", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_ContratadaUsuario_ContratadaCod )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Usuarios.Usuario", "GxEv3Up14_Meetrika", "SdtSDT_Usuarios_Usuario", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_ContratadaUsuario_ContratadaCod ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_contadoresfs objdp_contadoresfs;
         objdp_contadoresfs = new dp_contadoresfs();
         objdp_contadoresfs.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         objdp_contadoresfs.Gxm2rootcol = new GxObjectCollection( context, "SDT_Usuarios.Usuario", "GxEv3Up14_Meetrika", "SdtSDT_Usuarios_Usuario", "GeneXus.Programs") ;
         objdp_contadoresfs.context.SetSubmitInitialConfig(context);
         objdp_contadoresfs.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_contadoresfs);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_contadoresfs)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00072 */
         pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A70ContratadaUsuario_UsuarioPessoaCod = P00072_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00072_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00072_A69ContratadaUsuario_UsuarioCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00072_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00072_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00072_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00072_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00072_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00072_n71ContratadaUsuario_UsuarioPessoaNom[0];
            Gxm1sdt_usuarios = new SdtSDT_Usuarios_Usuario(context);
            Gxm2rootcol.Add(Gxm1sdt_usuarios, 0);
            Gxm1sdt_usuarios.gxTpr_Codigo = A69ContratadaUsuario_UsuarioCod;
            Gxm1sdt_usuarios.gxTpr_Nome = A71ContratadaUsuario_UsuarioPessoaNom;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00072_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00072_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00072_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00072_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00072_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00072_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         Gxm1sdt_usuarios = new SdtSDT_Usuarios_Usuario(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_contadoresfs__default(),
            new Object[][] {
                new Object[] {
               P00072_A70ContratadaUsuario_UsuarioPessoaCod, P00072_n70ContratadaUsuario_UsuarioPessoaCod, P00072_A66ContratadaUsuario_ContratadaCod, P00072_A69ContratadaUsuario_UsuarioCod, P00072_A71ContratadaUsuario_UsuarioPessoaNom, P00072_n71ContratadaUsuario_UsuarioPessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private String scmdbuf ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00072_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00072_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] P00072_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00072_A69ContratadaUsuario_UsuarioCod ;
      private String[] P00072_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00072_n71ContratadaUsuario_UsuarioPessoaNom ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Usuarios_Usuario ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Usuarios_Usuario Gxm1sdt_usuarios ;
   }

   public class dp_contadoresfs__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00072 ;
          prmP00072 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00072", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00072,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
