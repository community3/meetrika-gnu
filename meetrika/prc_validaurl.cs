/*
               File: PRC_ValidaUrl
        Description: Valida Url
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_validaurl : GXProcedure
   {
      public prc_validaurl( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_validaurl( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Url ,
                           out String aP1_Link )
      {
         this.AV10Url = aP0_Url;
         this.AV9Link = "" ;
         initialize();
         executePrivate();
         aP1_Link=this.AV9Link;
      }

      public String executeUdp( String aP0_Url )
      {
         this.AV10Url = aP0_Url;
         this.AV9Link = "" ;
         initialize();
         executePrivate();
         aP1_Link=this.AV9Link;
         return AV9Link ;
      }

      public void executeSubmit( String aP0_Url ,
                                 out String aP1_Link )
      {
         prc_validaurl objprc_validaurl;
         objprc_validaurl = new prc_validaurl();
         objprc_validaurl.AV10Url = aP0_Url;
         objprc_validaurl.AV9Link = "" ;
         objprc_validaurl.context.SetSubmitInitialConfig(context);
         objprc_validaurl.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_validaurl);
         aP1_Link=this.AV9Link;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_validaurl)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Link = StringUtil.Lower( AV10Url);
         if ( ( StringUtil.StringSearch( StringUtil.Lower( AV10Url), "www", 1) == 0 ) || ( StringUtil.StringSearch( StringUtil.Lower( AV10Url), ".com", 1) == 0 ) || ( StringUtil.StringSearch( StringUtil.Lower( AV10Url), ".net", 1) == 0 ) )
         {
            this.cleanup();
            if (true) return;
         }
         AV11i = (short)(StringUtil.StringSearch( AV10Url, "//", 1));
         if ( AV11i > 0 )
         {
            if ( StringUtil.StrCmp(StringUtil.Substring( AV10Url, 1, 2), "//") == 0 )
            {
               AV9Link = StringUtil.Substring( AV10Url, 3, StringUtil.Len( AV10Url));
            }
            else
            {
               AV9Link = StringUtil.Lower( AV10Url);
               AV9Link = StringUtil.StringReplace( AV9Link, "/", "");
            }
         }
         else
         {
            AV9Link = StringUtil.Lower( AV10Url);
         }
         AV8Inicio = "http://";
         if ( ( StringUtil.StringSearch( AV9Link, "https", 1) > 0 ) || ( StringUtil.StringSearch( AV9Link, "htts", 1) > 0 ) )
         {
            AV8Inicio = "https://";
         }
         if ( ( StringUtil.StrCmp(StringUtil.Substring( AV9Link, 1, 5), "http:") != 0 ) || ( StringUtil.StrCmp(StringUtil.Substring( AV9Link, 1, 6), "https:") != 0 ) )
         {
            if ( StringUtil.StringSearch( AV10Url, ":", 1) == 0 )
            {
               AV9Link = AV8Inicio + AV9Link;
            }
            else
            {
               AV11i = (short)(StringUtil.StringSearch( AV10Url, ":", 1)+1);
               AV9Link = AV8Inicio + StringUtil.Substring( AV9Link, AV11i, StringUtil.Len( AV9Link));
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Inicio = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11i ;
      private String AV8Inicio ;
      private String AV10Url ;
      private String AV9Link ;
      private String aP1_Link ;
   }

}
