/*
               File: WWTipoRequisito
        Description:  Tipos de Requisitos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:12:34.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwtiporequisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwtiporequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwtiporequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbTipoRequisito_Classificacao = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_34 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_34_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_34_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV36TFTipoRequisito_Identificador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoRequisito_Identificador", AV36TFTipoRequisito_Identificador);
               AV58TFTipoRequisito_Identificador_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoRequisito_Identificador_Sel", AV58TFTipoRequisito_Identificador_Sel);
               AV40TFTipoRequisito_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoRequisito_Descricao", AV40TFTipoRequisito_Descricao);
               AV41TFTipoRequisito_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoRequisito_Descricao_Sel", AV41TFTipoRequisito_Descricao_Sel);
               AV48TFTipoRequisito_LinNegCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0)));
               AV49TFTipoRequisito_LinNegCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTipoRequisito_LinNegCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0)));
               AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace", AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace);
               AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace", AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace);
               AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace", AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace);
               AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace", AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV45TFTipoRequisito_Classificacao_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV71Pgmname = GetNextPar( );
               A2041TipoRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PARR2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTRR2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203119123446");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwtiporequisito.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_IDENTIFICADOR", AV36TFTipoRequisito_Identificador);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_IDENTIFICADOR_SEL", AV58TFTipoRequisito_Identificador_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_DESCRICAO", AV40TFTipoRequisito_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_DESCRICAO_SEL", AV41TFTipoRequisito_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_LINNEGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOREQUISITO_LINNEGCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_34", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_34), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOREQUISITO_IDENTIFICADORTITLEFILTERDATA", AV35TipoRequisito_IdentificadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOREQUISITO_IDENTIFICADORTITLEFILTERDATA", AV35TipoRequisito_IdentificadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOREQUISITO_DESCRICAOTITLEFILTERDATA", AV39TipoRequisito_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOREQUISITO_DESCRICAOTITLEFILTERDATA", AV39TipoRequisito_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOREQUISITO_CLASSIFICACAOTITLEFILTERDATA", AV43TipoRequisito_ClassificacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOREQUISITO_CLASSIFICACAOTITLEFILTERDATA", AV43TipoRequisito_ClassificacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOREQUISITO_LINNEGCODTITLEFILTERDATA", AV47TipoRequisito_LinNegCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOREQUISITO_LINNEGCODTITLEFILTERDATA", AV47TipoRequisito_LinNegCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFTIPOREQUISITO_CLASSIFICACAO_SELS", AV45TFTipoRequisito_Classificacao_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFTIPOREQUISITO_CLASSIFICACAO_SELS", AV45TFTipoRequisito_Classificacao_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV71Pgmname));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Caption", StringUtil.RTrim( Ddo_tiporequisito_identificador_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Tooltip", StringUtil.RTrim( Ddo_tiporequisito_identificador_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Cls", StringUtil.RTrim( Ddo_tiporequisito_identificador_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_tiporequisito_identificador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_tiporequisito_identificador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_tiporequisito_identificador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tiporequisito_identificador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_tiporequisito_identificador_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_tiporequisito_identificador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortedstatus", StringUtil.RTrim( Ddo_tiporequisito_identificador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Includefilter", StringUtil.BoolToStr( Ddo_tiporequisito_identificador_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Filtertype", StringUtil.RTrim( Ddo_tiporequisito_identificador_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_tiporequisito_identificador_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_tiporequisito_identificador_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalisttype", StringUtil.RTrim( Ddo_tiporequisito_identificador_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalistproc", StringUtil.RTrim( Ddo_tiporequisito_identificador_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tiporequisito_identificador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortasc", StringUtil.RTrim( Ddo_tiporequisito_identificador_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortdsc", StringUtil.RTrim( Ddo_tiporequisito_identificador_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Loadingdata", StringUtil.RTrim( Ddo_tiporequisito_identificador_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Cleanfilter", StringUtil.RTrim( Ddo_tiporequisito_identificador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Noresultsfound", StringUtil.RTrim( Ddo_tiporequisito_identificador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_tiporequisito_identificador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_tiporequisito_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_tiporequisito_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_tiporequisito_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_tiporequisito_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_tiporequisito_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tiporequisito_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tiporequisito_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_tiporequisito_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_tiporequisito_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_tiporequisito_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_tiporequisito_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_tiporequisito_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_tiporequisito_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_tiporequisito_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_tiporequisito_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_tiporequisito_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tiporequisito_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_tiporequisito_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_tiporequisito_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_tiporequisito_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_tiporequisito_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_tiporequisito_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_tiporequisito_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Caption", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Tooltip", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Cls", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Includesortasc", StringUtil.BoolToStr( Ddo_tiporequisito_classificacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_tiporequisito_classificacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortedstatus", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Includefilter", StringUtil.BoolToStr( Ddo_tiporequisito_classificacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Includedatalist", StringUtil.BoolToStr( Ddo_tiporequisito_classificacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Datalisttype", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_tiporequisito_classificacao_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortasc", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortdsc", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Cleanfilter", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Searchbuttontext", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Caption", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Tooltip", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Cls", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtext_set", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Includesortasc", StringUtil.BoolToStr( Ddo_tiporequisito_linnegcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_tiporequisito_linnegcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Sortedstatus", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Includefilter", StringUtil.BoolToStr( Ddo_tiporequisito_linnegcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filtertype", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filterisrange", StringUtil.BoolToStr( Ddo_tiporequisito_linnegcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Includedatalist", StringUtil.BoolToStr( Ddo_tiporequisito_linnegcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Sortasc", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Sortdsc", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Cleanfilter", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Rangefilterto", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Searchbuttontext", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Activeeventkey", StringUtil.RTrim( Ddo_tiporequisito_identificador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_tiporequisito_identificador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_IDENTIFICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_tiporequisito_identificador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_tiporequisito_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_tiporequisito_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_tiporequisito_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Activeeventkey", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_CLASSIFICACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_tiporequisito_classificacao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Activeeventkey", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtext_get", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_tiporequisito_linnegcod_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WERR2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTRR2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwtiporequisito.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWTipoRequisito" ;
      }

      public override String GetPgmdesc( )
      {
         return " Tipos de Requisitos" ;
      }

      protected void WBRR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_RR2( true) ;
         }
         else
         {
            wb_table1_2_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftiporequisito_identificador_Internalname, AV36TFTipoRequisito_Identificador, StringUtil.RTrim( context.localUtil.Format( AV36TFTipoRequisito_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftiporequisito_identificador_Jsonclick, 0, "Attribute", "", "", "", edtavTftiporequisito_identificador_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftiporequisito_identificador_sel_Internalname, AV58TFTipoRequisito_Identificador_Sel, StringUtil.RTrim( context.localUtil.Format( AV58TFTipoRequisito_Identificador_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftiporequisito_identificador_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftiporequisito_identificador_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTftiporequisito_descricao_Internalname, AV40TFTipoRequisito_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, edtavTftiporequisito_descricao_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTftiporequisito_descricao_sel_Internalname, AV41TFTipoRequisito_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavTftiporequisito_descricao_sel_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftiporequisito_linnegcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFTipoRequisito_LinNegCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftiporequisito_linnegcod_Jsonclick, 0, "Attribute", "", "", "", edtavTftiporequisito_linnegcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTipoRequisito.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftiporequisito_linnegcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49TFTipoRequisito_LinNegCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftiporequisito_linnegcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTftiporequisito_linnegcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTipoRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOREQUISITO_IDENTIFICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Internalname, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOREQUISITO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Internalname, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOREQUISITO_CLASSIFICACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Internalname, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOREQUISITO_LINNEGCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Internalname, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoRequisito.htm");
         }
         wbLoad = true;
      }

      protected void STARTRR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Tipos de Requisitos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRR0( ) ;
      }

      protected void WSRR2( )
      {
         STARTRR2( ) ;
         EVTRR2( ) ;
      }

      protected void EVTRR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11RR2 */
                              E11RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOREQUISITO_IDENTIFICADOR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12RR2 */
                              E12RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOREQUISITO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13RR2 */
                              E13RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOREQUISITO_CLASSIFICACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14RR2 */
                              E14RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOREQUISITO_LINNEGCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15RR2 */
                              E15RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16RR2 */
                              E16RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17RR2 */
                              E17RR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_34_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
                              SubsflControlProps_342( ) ;
                              AV55Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV55Update)) ? AV68Update_GXI : context.convertURL( context.PathToRelativeUrl( AV55Update))));
                              AV56Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV56Delete)) ? AV69Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV56Delete))));
                              AV57Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV70Display_GXI : context.convertURL( context.PathToRelativeUrl( AV57Display))));
                              A2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoRequisito_Codigo_Internalname), ",", "."));
                              A2042TipoRequisito_Identificador = cgiGet( edtTipoRequisito_Identificador_Internalname);
                              A2043TipoRequisito_Descricao = cgiGet( edtTipoRequisito_Descricao_Internalname);
                              n2043TipoRequisito_Descricao = false;
                              cmbTipoRequisito_Classificacao.Name = cmbTipoRequisito_Classificacao_Internalname;
                              cmbTipoRequisito_Classificacao.CurrentValue = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
                              A2044TipoRequisito_Classificacao = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
                              n2044TipoRequisito_Classificacao = false;
                              A2039TipoRequisito_LinNegCod = (int)(context.localUtil.CToN( cgiGet( edtTipoRequisito_LinNegCod_Internalname), ",", "."));
                              n2039TipoRequisito_LinNegCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18RR2 */
                                    E18RR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19RR2 */
                                    E19RR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20RR2 */
                                    E20RR2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_identificador Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_IDENTIFICADOR"), AV36TFTipoRequisito_Identificador) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_identificador_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_IDENTIFICADOR_SEL"), AV58TFTipoRequisito_Identificador_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_DESCRICAO"), AV40TFTipoRequisito_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_DESCRICAO_SEL"), AV41TFTipoRequisito_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_linnegcod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOREQUISITO_LINNEGCOD"), ",", ".") != Convert.ToDecimal( AV48TFTipoRequisito_LinNegCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftiporequisito_linnegcod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOREQUISITO_LINNEGCOD_TO"), ",", ".") != Convert.ToDecimal( AV49TFTipoRequisito_LinNegCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PARR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            GXCCtl = "TIPOREQUISITO_CLASSIFICACAO_" + sGXsfl_34_idx;
            cmbTipoRequisito_Classificacao.Name = GXCCtl;
            cmbTipoRequisito_Classificacao.WebTags = "";
            cmbTipoRequisito_Classificacao.addItem("", "(Nenhum)", 0);
            cmbTipoRequisito_Classificacao.addItem("M", "Must", 0);
            cmbTipoRequisito_Classificacao.addItem("S", "Should", 0);
            cmbTipoRequisito_Classificacao.addItem("C", "Could", 0);
            cmbTipoRequisito_Classificacao.addItem("W", "Would", 0);
            if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
            {
               A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
               n2044TipoRequisito_Classificacao = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_342( ) ;
         while ( nGXsfl_34_idx <= nRC_GXsfl_34 )
         {
            sendrow_342( ) ;
            nGXsfl_34_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_34_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_34_idx+1));
            sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
            SubsflControlProps_342( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV36TFTipoRequisito_Identificador ,
                                       String AV58TFTipoRequisito_Identificador_Sel ,
                                       String AV40TFTipoRequisito_Descricao ,
                                       String AV41TFTipoRequisito_Descricao_Sel ,
                                       int AV48TFTipoRequisito_LinNegCod ,
                                       int AV49TFTipoRequisito_LinNegCod_To ,
                                       String AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace ,
                                       String AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace ,
                                       String AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace ,
                                       String AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace ,
                                       IGxCollection AV45TFTipoRequisito_Classificacao_Sels ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV71Pgmname ,
                                       int A2041TipoRequisito_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFRR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_IDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2042TipoRequisito_Identificador, ""))));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_IDENTIFICADOR", A2042TipoRequisito_Identificador);
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2043TipoRequisito_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_DESCRICAO", A2043TipoRequisito_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_CLASSIFICACAO", StringUtil.RTrim( A2044TipoRequisito_Classificacao));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_LINNEGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV71Pgmname = "WWTipoRequisito";
         context.Gx_err = 0;
      }

      protected void RFRR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 34;
         /* Execute user event: E19RR2 */
         E19RR2 ();
         nGXsfl_34_idx = 1;
         sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
         SubsflControlProps_342( ) ;
         nGXsfl_34_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_342( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A2044TipoRequisito_Classificacao ,
                                                 AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                                 AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                                 AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                                 AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                                 AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                                 AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels.Count ,
                                                 AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                                 AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                                 A2042TipoRequisito_Identificador ,
                                                 A2043TipoRequisito_Descricao ,
                                                 A2039TipoRequisito_LinNegCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = StringUtil.Concat( StringUtil.RTrim( AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador), "%", "");
            lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao), "%", "");
            /* Using cursor H00RR2 */
            pr_default.execute(0, new Object[] {lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador, AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel, lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao, AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel, AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod, AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_34_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2039TipoRequisito_LinNegCod = H00RR2_A2039TipoRequisito_LinNegCod[0];
               n2039TipoRequisito_LinNegCod = H00RR2_n2039TipoRequisito_LinNegCod[0];
               A2044TipoRequisito_Classificacao = H00RR2_A2044TipoRequisito_Classificacao[0];
               n2044TipoRequisito_Classificacao = H00RR2_n2044TipoRequisito_Classificacao[0];
               A2043TipoRequisito_Descricao = H00RR2_A2043TipoRequisito_Descricao[0];
               n2043TipoRequisito_Descricao = H00RR2_n2043TipoRequisito_Descricao[0];
               A2042TipoRequisito_Identificador = H00RR2_A2042TipoRequisito_Identificador[0];
               A2041TipoRequisito_Codigo = H00RR2_A2041TipoRequisito_Codigo[0];
               /* Execute user event: E20RR2 */
               E20RR2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 34;
            WBRR0( ) ;
         }
         nGXsfl_34_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A2044TipoRequisito_Classificacao ,
                                              AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                              AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                              AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                              AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                              AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                              AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels.Count ,
                                              AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                              AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                              A2042TipoRequisito_Identificador ,
                                              A2043TipoRequisito_Descricao ,
                                              A2039TipoRequisito_LinNegCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = StringUtil.Concat( StringUtil.RTrim( AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador), "%", "");
         lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao), "%", "");
         /* Using cursor H00RR3 */
         pr_default.execute(1, new Object[] {lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador, AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel, lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao, AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel, AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod, AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to});
         GRID_nRecordCount = H00RR3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV36TFTipoRequisito_Identificador, AV58TFTipoRequisito_Identificador_Sel, AV40TFTipoRequisito_Descricao, AV41TFTipoRequisito_Descricao_Sel, AV48TFTipoRequisito_LinNegCod, AV49TFTipoRequisito_LinNegCod_To, AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, AV45TFTipoRequisito_Classificacao_Sels, AV6WWPContext, AV71Pgmname, A2041TipoRequisito_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPRR0( )
      {
         /* Before Start, stand alone formulas. */
         AV71Pgmname = "WWTipoRequisito";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E18RR2 */
         E18RR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV51DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOREQUISITO_IDENTIFICADORTITLEFILTERDATA"), AV35TipoRequisito_IdentificadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOREQUISITO_DESCRICAOTITLEFILTERDATA"), AV39TipoRequisito_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOREQUISITO_CLASSIFICACAOTITLEFILTERDATA"), AV43TipoRequisito_ClassificacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOREQUISITO_LINNEGCODTITLEFILTERDATA"), AV47TipoRequisito_LinNegCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            AV36TFTipoRequisito_Identificador = cgiGet( edtavTftiporequisito_identificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoRequisito_Identificador", AV36TFTipoRequisito_Identificador);
            AV58TFTipoRequisito_Identificador_Sel = cgiGet( edtavTftiporequisito_identificador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoRequisito_Identificador_Sel", AV58TFTipoRequisito_Identificador_Sel);
            AV40TFTipoRequisito_Descricao = cgiGet( edtavTftiporequisito_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoRequisito_Descricao", AV40TFTipoRequisito_Descricao);
            AV41TFTipoRequisito_Descricao_Sel = cgiGet( edtavTftiporequisito_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoRequisito_Descricao_Sel", AV41TFTipoRequisito_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOREQUISITO_LINNEGCOD");
               GX_FocusControl = edtavTftiporequisito_linnegcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFTipoRequisito_LinNegCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0)));
            }
            else
            {
               AV48TFTipoRequisito_LinNegCod = (int)(context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOREQUISITO_LINNEGCOD_TO");
               GX_FocusControl = edtavTftiporequisito_linnegcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFTipoRequisito_LinNegCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTipoRequisito_LinNegCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0)));
            }
            else
            {
               AV49TFTipoRequisito_LinNegCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTftiporequisito_linnegcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTipoRequisito_LinNegCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0)));
            }
            AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace = cgiGet( edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace", AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace);
            AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace", AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace);
            AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace = cgiGet( edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace", AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace);
            AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace = cgiGet( edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace", AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_34 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_34"), ",", "."));
            AV53GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV54GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tiporequisito_identificador_Caption = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Caption");
            Ddo_tiporequisito_identificador_Tooltip = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Tooltip");
            Ddo_tiporequisito_identificador_Cls = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Cls");
            Ddo_tiporequisito_identificador_Filteredtext_set = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Filteredtext_set");
            Ddo_tiporequisito_identificador_Selectedvalue_set = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Selectedvalue_set");
            Ddo_tiporequisito_identificador_Dropdownoptionstype = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Dropdownoptionstype");
            Ddo_tiporequisito_identificador_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Titlecontrolidtoreplace");
            Ddo_tiporequisito_identificador_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Includesortasc"));
            Ddo_tiporequisito_identificador_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Includesortdsc"));
            Ddo_tiporequisito_identificador_Sortedstatus = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortedstatus");
            Ddo_tiporequisito_identificador_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Includefilter"));
            Ddo_tiporequisito_identificador_Filtertype = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Filtertype");
            Ddo_tiporequisito_identificador_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Filterisrange"));
            Ddo_tiporequisito_identificador_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Includedatalist"));
            Ddo_tiporequisito_identificador_Datalisttype = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalisttype");
            Ddo_tiporequisito_identificador_Datalistproc = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalistproc");
            Ddo_tiporequisito_identificador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tiporequisito_identificador_Sortasc = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortasc");
            Ddo_tiporequisito_identificador_Sortdsc = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Sortdsc");
            Ddo_tiporequisito_identificador_Loadingdata = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Loadingdata");
            Ddo_tiporequisito_identificador_Cleanfilter = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Cleanfilter");
            Ddo_tiporequisito_identificador_Noresultsfound = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Noresultsfound");
            Ddo_tiporequisito_identificador_Searchbuttontext = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Searchbuttontext");
            Ddo_tiporequisito_descricao_Caption = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Caption");
            Ddo_tiporequisito_descricao_Tooltip = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Tooltip");
            Ddo_tiporequisito_descricao_Cls = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Cls");
            Ddo_tiporequisito_descricao_Filteredtext_set = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Filteredtext_set");
            Ddo_tiporequisito_descricao_Selectedvalue_set = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Selectedvalue_set");
            Ddo_tiporequisito_descricao_Dropdownoptionstype = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Dropdownoptionstype");
            Ddo_tiporequisito_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_tiporequisito_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Includesortasc"));
            Ddo_tiporequisito_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Includesortdsc"));
            Ddo_tiporequisito_descricao_Sortedstatus = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Sortedstatus");
            Ddo_tiporequisito_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Includefilter"));
            Ddo_tiporequisito_descricao_Filtertype = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Filtertype");
            Ddo_tiporequisito_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Filterisrange"));
            Ddo_tiporequisito_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Includedatalist"));
            Ddo_tiporequisito_descricao_Datalisttype = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Datalisttype");
            Ddo_tiporequisito_descricao_Datalistproc = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Datalistproc");
            Ddo_tiporequisito_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tiporequisito_descricao_Sortasc = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Sortasc");
            Ddo_tiporequisito_descricao_Sortdsc = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Sortdsc");
            Ddo_tiporequisito_descricao_Loadingdata = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Loadingdata");
            Ddo_tiporequisito_descricao_Cleanfilter = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Cleanfilter");
            Ddo_tiporequisito_descricao_Noresultsfound = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Noresultsfound");
            Ddo_tiporequisito_descricao_Searchbuttontext = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Searchbuttontext");
            Ddo_tiporequisito_classificacao_Caption = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Caption");
            Ddo_tiporequisito_classificacao_Tooltip = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Tooltip");
            Ddo_tiporequisito_classificacao_Cls = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Cls");
            Ddo_tiporequisito_classificacao_Selectedvalue_set = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Selectedvalue_set");
            Ddo_tiporequisito_classificacao_Dropdownoptionstype = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Dropdownoptionstype");
            Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Titlecontrolidtoreplace");
            Ddo_tiporequisito_classificacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Includesortasc"));
            Ddo_tiporequisito_classificacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Includesortdsc"));
            Ddo_tiporequisito_classificacao_Sortedstatus = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortedstatus");
            Ddo_tiporequisito_classificacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Includefilter"));
            Ddo_tiporequisito_classificacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Includedatalist"));
            Ddo_tiporequisito_classificacao_Datalisttype = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Datalisttype");
            Ddo_tiporequisito_classificacao_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Allowmultipleselection"));
            Ddo_tiporequisito_classificacao_Datalistfixedvalues = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Datalistfixedvalues");
            Ddo_tiporequisito_classificacao_Sortasc = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortasc");
            Ddo_tiporequisito_classificacao_Sortdsc = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Sortdsc");
            Ddo_tiporequisito_classificacao_Cleanfilter = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Cleanfilter");
            Ddo_tiporequisito_classificacao_Searchbuttontext = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Searchbuttontext");
            Ddo_tiporequisito_linnegcod_Caption = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Caption");
            Ddo_tiporequisito_linnegcod_Tooltip = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Tooltip");
            Ddo_tiporequisito_linnegcod_Cls = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Cls");
            Ddo_tiporequisito_linnegcod_Filteredtext_set = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtext_set");
            Ddo_tiporequisito_linnegcod_Filteredtextto_set = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtextto_set");
            Ddo_tiporequisito_linnegcod_Dropdownoptionstype = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Dropdownoptionstype");
            Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Titlecontrolidtoreplace");
            Ddo_tiporequisito_linnegcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Includesortasc"));
            Ddo_tiporequisito_linnegcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Includesortdsc"));
            Ddo_tiporequisito_linnegcod_Sortedstatus = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Sortedstatus");
            Ddo_tiporequisito_linnegcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Includefilter"));
            Ddo_tiporequisito_linnegcod_Filtertype = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filtertype");
            Ddo_tiporequisito_linnegcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filterisrange"));
            Ddo_tiporequisito_linnegcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Includedatalist"));
            Ddo_tiporequisito_linnegcod_Sortasc = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Sortasc");
            Ddo_tiporequisito_linnegcod_Sortdsc = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Sortdsc");
            Ddo_tiporequisito_linnegcod_Cleanfilter = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Cleanfilter");
            Ddo_tiporequisito_linnegcod_Rangefilterfrom = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Rangefilterfrom");
            Ddo_tiporequisito_linnegcod_Rangefilterto = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Rangefilterto");
            Ddo_tiporequisito_linnegcod_Searchbuttontext = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tiporequisito_identificador_Activeeventkey = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Activeeventkey");
            Ddo_tiporequisito_identificador_Filteredtext_get = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Filteredtext_get");
            Ddo_tiporequisito_identificador_Selectedvalue_get = cgiGet( "DDO_TIPOREQUISITO_IDENTIFICADOR_Selectedvalue_get");
            Ddo_tiporequisito_descricao_Activeeventkey = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Activeeventkey");
            Ddo_tiporequisito_descricao_Filteredtext_get = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Filteredtext_get");
            Ddo_tiporequisito_descricao_Selectedvalue_get = cgiGet( "DDO_TIPOREQUISITO_DESCRICAO_Selectedvalue_get");
            Ddo_tiporequisito_classificacao_Activeeventkey = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Activeeventkey");
            Ddo_tiporequisito_classificacao_Selectedvalue_get = cgiGet( "DDO_TIPOREQUISITO_CLASSIFICACAO_Selectedvalue_get");
            Ddo_tiporequisito_linnegcod_Activeeventkey = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Activeeventkey");
            Ddo_tiporequisito_linnegcod_Filteredtext_get = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtext_get");
            Ddo_tiporequisito_linnegcod_Filteredtextto_get = cgiGet( "DDO_TIPOREQUISITO_LINNEGCOD_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_IDENTIFICADOR"), AV36TFTipoRequisito_Identificador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_IDENTIFICADOR_SEL"), AV58TFTipoRequisito_Identificador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_DESCRICAO"), AV40TFTipoRequisito_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOREQUISITO_DESCRICAO_SEL"), AV41TFTipoRequisito_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOREQUISITO_LINNEGCOD"), ",", ".") != Convert.ToDecimal( AV48TFTipoRequisito_LinNegCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOREQUISITO_LINNEGCOD_TO"), ",", ".") != Convert.ToDecimal( AV49TFTipoRequisito_LinNegCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E18RR2 */
         E18RR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18RR2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTftiporequisito_identificador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_identificador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_identificador_Visible), 5, 0)));
         edtavTftiporequisito_identificador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_identificador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_identificador_sel_Visible), 5, 0)));
         edtavTftiporequisito_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_descricao_Visible), 5, 0)));
         edtavTftiporequisito_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_descricao_sel_Visible), 5, 0)));
         edtavTftiporequisito_linnegcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_linnegcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_linnegcod_Visible), 5, 0)));
         edtavTftiporequisito_linnegcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftiporequisito_linnegcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftiporequisito_linnegcod_to_Visible), 5, 0)));
         Ddo_tiporequisito_identificador_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoRequisito_Identificador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "TitleControlIdToReplace", Ddo_tiporequisito_identificador_Titlecontrolidtoreplace);
         AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace = Ddo_tiporequisito_identificador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace", AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace);
         edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tiporequisito_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoRequisito_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "TitleControlIdToReplace", Ddo_tiporequisito_descricao_Titlecontrolidtoreplace);
         AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace = Ddo_tiporequisito_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace", AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace);
         edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoRequisito_Classificacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "TitleControlIdToReplace", Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace);
         AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace = Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace", AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace);
         edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoRequisito_LinNegCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "TitleControlIdToReplace", Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace);
         AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace = Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace", AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace);
         edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Tipos de Requisitos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Identificador", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "Classifica��o", 0);
         cmbavOrderedby.addItem("4", "Linha de Neg�cio", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV51DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV51DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E19RR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV35TipoRequisito_IdentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39TipoRequisito_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43TipoRequisito_ClassificacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TipoRequisito_LinNegCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTipoRequisito_Identificador_Titleformat = 2;
         edtTipoRequisito_Identificador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Identificador", AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Identificador_Internalname, "Title", edtTipoRequisito_Identificador_Title);
         edtTipoRequisito_Descricao_Titleformat = 2;
         edtTipoRequisito_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_Descricao_Internalname, "Title", edtTipoRequisito_Descricao_Title);
         cmbTipoRequisito_Classificacao_Titleformat = 2;
         cmbTipoRequisito_Classificacao.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Classifica��o", AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoRequisito_Classificacao_Internalname, "Title", cmbTipoRequisito_Classificacao.Title.Text);
         edtTipoRequisito_LinNegCod_Titleformat = 2;
         edtTipoRequisito_LinNegCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Linha de Neg�cio", AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoRequisito_LinNegCod_Internalname, "Title", edtTipoRequisito_LinNegCod_Title);
         AV53GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridCurrentPage), 10, 0)));
         AV54GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridPageCount), 10, 0)));
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = AV36TFTipoRequisito_Identificador;
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = AV58TFTipoRequisito_Identificador_Sel;
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = AV40TFTipoRequisito_Descricao;
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = AV41TFTipoRequisito_Descricao_Sel;
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = AV45TFTipoRequisito_Classificacao_Sels;
         AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod = AV48TFTipoRequisito_LinNegCod;
         AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to = AV49TFTipoRequisito_LinNegCod_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35TipoRequisito_IdentificadorTitleFilterData", AV35TipoRequisito_IdentificadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39TipoRequisito_DescricaoTitleFilterData", AV39TipoRequisito_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43TipoRequisito_ClassificacaoTitleFilterData", AV43TipoRequisito_ClassificacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47TipoRequisito_LinNegCodTitleFilterData", AV47TipoRequisito_LinNegCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11RR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV52PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV52PageToGo) ;
         }
      }

      protected void E12RR2( )
      {
         /* Ddo_tiporequisito_identificador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tiporequisito_identificador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_identificador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "SortedStatus", Ddo_tiporequisito_identificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_identificador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_identificador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "SortedStatus", Ddo_tiporequisito_identificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_identificador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFTipoRequisito_Identificador = Ddo_tiporequisito_identificador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoRequisito_Identificador", AV36TFTipoRequisito_Identificador);
            AV58TFTipoRequisito_Identificador_Sel = Ddo_tiporequisito_identificador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoRequisito_Identificador_Sel", AV58TFTipoRequisito_Identificador_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13RR2( )
      {
         /* Ddo_tiporequisito_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tiporequisito_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "SortedStatus", Ddo_tiporequisito_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "SortedStatus", Ddo_tiporequisito_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFTipoRequisito_Descricao = Ddo_tiporequisito_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoRequisito_Descricao", AV40TFTipoRequisito_Descricao);
            AV41TFTipoRequisito_Descricao_Sel = Ddo_tiporequisito_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoRequisito_Descricao_Sel", AV41TFTipoRequisito_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14RR2( )
      {
         /* Ddo_tiporequisito_classificacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tiporequisito_classificacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_classificacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "SortedStatus", Ddo_tiporequisito_classificacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_classificacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_classificacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "SortedStatus", Ddo_tiporequisito_classificacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_classificacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFTipoRequisito_Classificacao_SelsJson = Ddo_tiporequisito_classificacao_Selectedvalue_get;
            AV45TFTipoRequisito_Classificacao_Sels.FromJSonString(AV44TFTipoRequisito_Classificacao_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45TFTipoRequisito_Classificacao_Sels", AV45TFTipoRequisito_Classificacao_Sels);
      }

      protected void E15RR2( )
      {
         /* Ddo_tiporequisito_linnegcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tiporequisito_linnegcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_linnegcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "SortedStatus", Ddo_tiporequisito_linnegcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_linnegcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tiporequisito_linnegcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "SortedStatus", Ddo_tiporequisito_linnegcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tiporequisito_linnegcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFTipoRequisito_LinNegCod = (int)(NumberUtil.Val( Ddo_tiporequisito_linnegcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0)));
            AV49TFTipoRequisito_LinNegCod_To = (int)(NumberUtil.Val( Ddo_tiporequisito_linnegcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTipoRequisito_LinNegCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E20RR2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2041TipoRequisito_Codigo);
            AV55Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV55Update);
            AV68Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV55Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV55Update);
            AV68Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2041TipoRequisito_Codigo);
            AV56Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV56Delete);
            AV69Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV56Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV56Delete);
            AV69Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewtiporequisito.aspx") + "?" + UrlEncode("" +A2041TipoRequisito_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV57Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV57Display);
            AV70Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV57Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV57Display);
            AV70Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 34;
         }
         sendrow_342( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_34_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(34, GridRow);
         }
      }

      protected void E16RR2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E17RR2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tiporequisito_identificador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "SortedStatus", Ddo_tiporequisito_identificador_Sortedstatus);
         Ddo_tiporequisito_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "SortedStatus", Ddo_tiporequisito_descricao_Sortedstatus);
         Ddo_tiporequisito_classificacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "SortedStatus", Ddo_tiporequisito_classificacao_Sortedstatus);
         Ddo_tiporequisito_linnegcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "SortedStatus", Ddo_tiporequisito_linnegcod_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tiporequisito_identificador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "SortedStatus", Ddo_tiporequisito_identificador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tiporequisito_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "SortedStatus", Ddo_tiporequisito_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_tiporequisito_classificacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "SortedStatus", Ddo_tiporequisito_classificacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_tiporequisito_linnegcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "SortedStatus", Ddo_tiporequisito_linnegcod_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get(AV71Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV71Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV28Session.Get(AV71Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV72GXV1 = 1;
         while ( AV72GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV72GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_IDENTIFICADOR") == 0 )
            {
               AV36TFTipoRequisito_Identificador = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoRequisito_Identificador", AV36TFTipoRequisito_Identificador);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFTipoRequisito_Identificador)) )
               {
                  Ddo_tiporequisito_identificador_Filteredtext_set = AV36TFTipoRequisito_Identificador;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "FilteredText_set", Ddo_tiporequisito_identificador_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_IDENTIFICADOR_SEL") == 0 )
            {
               AV58TFTipoRequisito_Identificador_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoRequisito_Identificador_Sel", AV58TFTipoRequisito_Identificador_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoRequisito_Identificador_Sel)) )
               {
                  Ddo_tiporequisito_identificador_Selectedvalue_set = AV58TFTipoRequisito_Identificador_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_identificador_Internalname, "SelectedValue_set", Ddo_tiporequisito_identificador_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_DESCRICAO") == 0 )
            {
               AV40TFTipoRequisito_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoRequisito_Descricao", AV40TFTipoRequisito_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFTipoRequisito_Descricao)) )
               {
                  Ddo_tiporequisito_descricao_Filteredtext_set = AV40TFTipoRequisito_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "FilteredText_set", Ddo_tiporequisito_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_DESCRICAO_SEL") == 0 )
            {
               AV41TFTipoRequisito_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoRequisito_Descricao_Sel", AV41TFTipoRequisito_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipoRequisito_Descricao_Sel)) )
               {
                  Ddo_tiporequisito_descricao_Selectedvalue_set = AV41TFTipoRequisito_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_descricao_Internalname, "SelectedValue_set", Ddo_tiporequisito_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_CLASSIFICACAO_SEL") == 0 )
            {
               AV44TFTipoRequisito_Classificacao_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV45TFTipoRequisito_Classificacao_Sels.FromJSonString(AV44TFTipoRequisito_Classificacao_SelsJson);
               if ( ! ( AV45TFTipoRequisito_Classificacao_Sels.Count == 0 ) )
               {
                  Ddo_tiporequisito_classificacao_Selectedvalue_set = AV44TFTipoRequisito_Classificacao_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_classificacao_Internalname, "SelectedValue_set", Ddo_tiporequisito_classificacao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPOREQUISITO_LINNEGCOD") == 0 )
            {
               AV48TFTipoRequisito_LinNegCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFTipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0)));
               AV49TFTipoRequisito_LinNegCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFTipoRequisito_LinNegCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0)));
               if ( ! (0==AV48TFTipoRequisito_LinNegCod) )
               {
                  Ddo_tiporequisito_linnegcod_Filteredtext_set = StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "FilteredText_set", Ddo_tiporequisito_linnegcod_Filteredtext_set);
               }
               if ( ! (0==AV49TFTipoRequisito_LinNegCod_To) )
               {
                  Ddo_tiporequisito_linnegcod_Filteredtextto_set = StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tiporequisito_linnegcod_Internalname, "FilteredTextTo_set", Ddo_tiporequisito_linnegcod_Filteredtextto_set);
               }
            }
            AV72GXV1 = (int)(AV72GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV28Session.Get(AV71Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFTipoRequisito_Identificador)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_IDENTIFICADOR";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFTipoRequisito_Identificador;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoRequisito_Identificador_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_IDENTIFICADOR_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV58TFTipoRequisito_Identificador_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFTipoRequisito_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFTipoRequisito_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipoRequisito_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFTipoRequisito_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV45TFTipoRequisito_Classificacao_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_CLASSIFICACAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFTipoRequisito_Classificacao_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV48TFTipoRequisito_LinNegCod) && (0==AV49TFTipoRequisito_LinNegCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOREQUISITO_LINNEGCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV48TFTipoRequisito_LinNegCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV49TFTipoRequisito_LinNegCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV71Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV71Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "TipoRequisito";
         AV28Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RR2( true) ;
         }
         else
         {
            wb_table2_8_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_28_RR2( true) ;
         }
         else
         {
            wb_table3_28_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_28_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RR2e( true) ;
         }
         else
         {
            wb_table1_2_RR2e( false) ;
         }
      }

      protected void wb_table3_28_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_31_RR2( true) ;
         }
         else
         {
            wb_table4_31_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_31_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_RR2e( true) ;
         }
         else
         {
            wb_table3_28_RR2e( false) ;
         }
      }

      protected void wb_table4_31_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"34\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoRequisito_Identificador_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoRequisito_Identificador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoRequisito_Identificador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoRequisito_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoRequisito_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoRequisito_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbTipoRequisito_Classificacao_Titleformat == 0 )
               {
                  context.SendWebValue( cmbTipoRequisito_Classificacao.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbTipoRequisito_Classificacao.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoRequisito_LinNegCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoRequisito_LinNegCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoRequisito_LinNegCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV55Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV56Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV57Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2042TipoRequisito_Identificador);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoRequisito_Identificador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoRequisito_Identificador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2043TipoRequisito_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoRequisito_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoRequisito_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2044TipoRequisito_Classificacao));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbTipoRequisito_Classificacao.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoRequisito_Classificacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoRequisito_LinNegCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoRequisito_LinNegCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 34 )
         {
            wbEnd = 0;
            nRC_GXsfl_34 = (short)(nGXsfl_34_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_31_RR2e( true) ;
         }
         else
         {
            wb_table4_31_RR2e( false) ;
         }
      }

      protected void wb_table2_8_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_RR2( true) ;
         }
         else
         {
            wb_table5_11_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_RR2( true) ;
         }
         else
         {
            wb_table6_23_RR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_RR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RR2e( true) ;
         }
         else
         {
            wb_table2_8_RR2e( false) ;
         }
      }

      protected void wb_table6_23_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_RR2e( true) ;
         }
         else
         {
            wb_table6_23_RR2e( false) ;
         }
      }

      protected void wb_table5_11_RR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTiporequisitotitle_Internalname, "Tipos de Requisitos", "", "", lblTiporequisitotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWTipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWTipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_34_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWTipoRequisito.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWTipoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_RR2e( true) ;
         }
         else
         {
            wb_table5_11_RR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARR2( ) ;
         WSRR2( ) ;
         WERR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119123897");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwtiporequisito.js", "?20203119123897");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_342( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_34_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_34_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_34_idx;
         edtTipoRequisito_Codigo_Internalname = "TIPOREQUISITO_CODIGO_"+sGXsfl_34_idx;
         edtTipoRequisito_Identificador_Internalname = "TIPOREQUISITO_IDENTIFICADOR_"+sGXsfl_34_idx;
         edtTipoRequisito_Descricao_Internalname = "TIPOREQUISITO_DESCRICAO_"+sGXsfl_34_idx;
         cmbTipoRequisito_Classificacao_Internalname = "TIPOREQUISITO_CLASSIFICACAO_"+sGXsfl_34_idx;
         edtTipoRequisito_LinNegCod_Internalname = "TIPOREQUISITO_LINNEGCOD_"+sGXsfl_34_idx;
      }

      protected void SubsflControlProps_fel_342( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_34_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_34_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_34_fel_idx;
         edtTipoRequisito_Codigo_Internalname = "TIPOREQUISITO_CODIGO_"+sGXsfl_34_fel_idx;
         edtTipoRequisito_Identificador_Internalname = "TIPOREQUISITO_IDENTIFICADOR_"+sGXsfl_34_fel_idx;
         edtTipoRequisito_Descricao_Internalname = "TIPOREQUISITO_DESCRICAO_"+sGXsfl_34_fel_idx;
         cmbTipoRequisito_Classificacao_Internalname = "TIPOREQUISITO_CLASSIFICACAO_"+sGXsfl_34_fel_idx;
         edtTipoRequisito_LinNegCod_Internalname = "TIPOREQUISITO_LINNEGCOD_"+sGXsfl_34_fel_idx;
      }

      protected void sendrow_342( )
      {
         SubsflControlProps_342( ) ;
         WBRR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_34_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_34_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_34_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV55Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV55Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV68Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV55Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV55Update)) ? AV68Update_GXI : context.PathToRelativeUrl( AV55Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV55Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV56Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV56Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV69Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV56Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV56Delete)) ? AV69Delete_GXI : context.PathToRelativeUrl( AV56Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV56Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV57Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV57Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV70Display_GXI : context.PathToRelativeUrl( AV57Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV57Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoRequisito_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoRequisito_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoRequisito_Identificador_Internalname,(String)A2042TipoRequisito_Identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoRequisito_Identificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoRequisito_Descricao_Internalname,(String)A2043TipoRequisito_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoRequisito_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_34_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "TIPOREQUISITO_CLASSIFICACAO_" + sGXsfl_34_idx;
               cmbTipoRequisito_Classificacao.Name = GXCCtl;
               cmbTipoRequisito_Classificacao.WebTags = "";
               cmbTipoRequisito_Classificacao.addItem("", "(Nenhum)", 0);
               cmbTipoRequisito_Classificacao.addItem("M", "Must", 0);
               cmbTipoRequisito_Classificacao.addItem("S", "Should", 0);
               cmbTipoRequisito_Classificacao.addItem("C", "Could", 0);
               cmbTipoRequisito_Classificacao.addItem("W", "Would", 0);
               if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
               {
                  A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
                  n2044TipoRequisito_Classificacao = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTipoRequisito_Classificacao,(String)cmbTipoRequisito_Classificacao_Internalname,StringUtil.RTrim( A2044TipoRequisito_Classificacao),(short)1,(String)cmbTipoRequisito_Classificacao_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbTipoRequisito_Classificacao.CurrentValue = StringUtil.RTrim( A2044TipoRequisito_Classificacao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoRequisito_Classificacao_Internalname, "Values", (String)(cmbTipoRequisito_Classificacao.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoRequisito_LinNegCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoRequisito_LinNegCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_CODIGO"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_IDENTIFICADOR"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, StringUtil.RTrim( context.localUtil.Format( A2042TipoRequisito_Identificador, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_DESCRICAO"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, StringUtil.RTrim( context.localUtil.Format( A2043TipoRequisito_Descricao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_CLASSIFICACAO"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOREQUISITO_LINNEGCOD"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_34_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_34_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_34_idx+1));
            sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
            SubsflControlProps_342( ) ;
         }
         /* End function sendrow_342 */
      }

      protected void init_default_properties( )
      {
         lblTiporequisitotitle_Internalname = "TIPOREQUISITOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtTipoRequisito_Codigo_Internalname = "TIPOREQUISITO_CODIGO";
         edtTipoRequisito_Identificador_Internalname = "TIPOREQUISITO_IDENTIFICADOR";
         edtTipoRequisito_Descricao_Internalname = "TIPOREQUISITO_DESCRICAO";
         cmbTipoRequisito_Classificacao_Internalname = "TIPOREQUISITO_CLASSIFICACAO";
         edtTipoRequisito_LinNegCod_Internalname = "TIPOREQUISITO_LINNEGCOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTftiporequisito_identificador_Internalname = "vTFTIPOREQUISITO_IDENTIFICADOR";
         edtavTftiporequisito_identificador_sel_Internalname = "vTFTIPOREQUISITO_IDENTIFICADOR_SEL";
         edtavTftiporequisito_descricao_Internalname = "vTFTIPOREQUISITO_DESCRICAO";
         edtavTftiporequisito_descricao_sel_Internalname = "vTFTIPOREQUISITO_DESCRICAO_SEL";
         edtavTftiporequisito_linnegcod_Internalname = "vTFTIPOREQUISITO_LINNEGCOD";
         edtavTftiporequisito_linnegcod_to_Internalname = "vTFTIPOREQUISITO_LINNEGCOD_TO";
         Ddo_tiporequisito_identificador_Internalname = "DDO_TIPOREQUISITO_IDENTIFICADOR";
         edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Internalname = "vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE";
         Ddo_tiporequisito_descricao_Internalname = "DDO_TIPOREQUISITO_DESCRICAO";
         edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Internalname = "vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_tiporequisito_classificacao_Internalname = "DDO_TIPOREQUISITO_CLASSIFICACAO";
         edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Internalname = "vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE";
         Ddo_tiporequisito_linnegcod_Internalname = "DDO_TIPOREQUISITO_LINNEGCOD";
         edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Internalname = "vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTipoRequisito_LinNegCod_Jsonclick = "";
         cmbTipoRequisito_Classificacao_Jsonclick = "";
         edtTipoRequisito_Descricao_Jsonclick = "";
         edtTipoRequisito_Identificador_Jsonclick = "";
         edtTipoRequisito_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtTipoRequisito_LinNegCod_Titleformat = 0;
         cmbTipoRequisito_Classificacao_Titleformat = 0;
         edtTipoRequisito_Descricao_Titleformat = 0;
         edtTipoRequisito_Identificador_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtTipoRequisito_LinNegCod_Title = "Linha de Neg�cio";
         cmbTipoRequisito_Classificacao.Title.Text = "Classifica��o";
         edtTipoRequisito_Descricao_Title = "Descri��o";
         edtTipoRequisito_Identificador_Title = "Identificador";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Visible = 1;
         edtavTftiporequisito_linnegcod_to_Jsonclick = "";
         edtavTftiporequisito_linnegcod_to_Visible = 1;
         edtavTftiporequisito_linnegcod_Jsonclick = "";
         edtavTftiporequisito_linnegcod_Visible = 1;
         edtavTftiporequisito_descricao_sel_Visible = 1;
         edtavTftiporequisito_descricao_Visible = 1;
         edtavTftiporequisito_identificador_sel_Jsonclick = "";
         edtavTftiporequisito_identificador_sel_Visible = 1;
         edtavTftiporequisito_identificador_Jsonclick = "";
         edtavTftiporequisito_identificador_Visible = 1;
         Ddo_tiporequisito_linnegcod_Searchbuttontext = "Pesquisar";
         Ddo_tiporequisito_linnegcod_Rangefilterto = "At�";
         Ddo_tiporequisito_linnegcod_Rangefilterfrom = "Desde";
         Ddo_tiporequisito_linnegcod_Cleanfilter = "Limpar pesquisa";
         Ddo_tiporequisito_linnegcod_Sortdsc = "Ordenar de Z � A";
         Ddo_tiporequisito_linnegcod_Sortasc = "Ordenar de A � Z";
         Ddo_tiporequisito_linnegcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_tiporequisito_linnegcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_tiporequisito_linnegcod_Filtertype = "Numeric";
         Ddo_tiporequisito_linnegcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_tiporequisito_linnegcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_linnegcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace = "";
         Ddo_tiporequisito_linnegcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tiporequisito_linnegcod_Cls = "ColumnSettings";
         Ddo_tiporequisito_linnegcod_Tooltip = "Op��es";
         Ddo_tiporequisito_linnegcod_Caption = "";
         Ddo_tiporequisito_classificacao_Searchbuttontext = "Filtrar Selecionados";
         Ddo_tiporequisito_classificacao_Cleanfilter = "Limpar pesquisa";
         Ddo_tiporequisito_classificacao_Sortdsc = "Ordenar de Z � A";
         Ddo_tiporequisito_classificacao_Sortasc = "Ordenar de A � Z";
         Ddo_tiporequisito_classificacao_Datalistfixedvalues = "M:Must,S:Should,C:Could,W:Would";
         Ddo_tiporequisito_classificacao_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_tiporequisito_classificacao_Datalisttype = "FixedValues";
         Ddo_tiporequisito_classificacao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tiporequisito_classificacao_Includefilter = Convert.ToBoolean( 0);
         Ddo_tiporequisito_classificacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_classificacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace = "";
         Ddo_tiporequisito_classificacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tiporequisito_classificacao_Cls = "ColumnSettings";
         Ddo_tiporequisito_classificacao_Tooltip = "Op��es";
         Ddo_tiporequisito_classificacao_Caption = "";
         Ddo_tiporequisito_descricao_Searchbuttontext = "Pesquisar";
         Ddo_tiporequisito_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tiporequisito_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_tiporequisito_descricao_Loadingdata = "Carregando dados...";
         Ddo_tiporequisito_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_tiporequisito_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_tiporequisito_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_tiporequisito_descricao_Datalistproc = "GetWWTipoRequisitoFilterData";
         Ddo_tiporequisito_descricao_Datalisttype = "Dynamic";
         Ddo_tiporequisito_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tiporequisito_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tiporequisito_descricao_Filtertype = "Character";
         Ddo_tiporequisito_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_tiporequisito_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_descricao_Titlecontrolidtoreplace = "";
         Ddo_tiporequisito_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tiporequisito_descricao_Cls = "ColumnSettings";
         Ddo_tiporequisito_descricao_Tooltip = "Op��es";
         Ddo_tiporequisito_descricao_Caption = "";
         Ddo_tiporequisito_identificador_Searchbuttontext = "Pesquisar";
         Ddo_tiporequisito_identificador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tiporequisito_identificador_Cleanfilter = "Limpar pesquisa";
         Ddo_tiporequisito_identificador_Loadingdata = "Carregando dados...";
         Ddo_tiporequisito_identificador_Sortdsc = "Ordenar de Z � A";
         Ddo_tiporequisito_identificador_Sortasc = "Ordenar de A � Z";
         Ddo_tiporequisito_identificador_Datalistupdateminimumcharacters = 0;
         Ddo_tiporequisito_identificador_Datalistproc = "GetWWTipoRequisitoFilterData";
         Ddo_tiporequisito_identificador_Datalisttype = "Dynamic";
         Ddo_tiporequisito_identificador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tiporequisito_identificador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tiporequisito_identificador_Filtertype = "Character";
         Ddo_tiporequisito_identificador_Includefilter = Convert.ToBoolean( -1);
         Ddo_tiporequisito_identificador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_identificador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tiporequisito_identificador_Titlecontrolidtoreplace = "";
         Ddo_tiporequisito_identificador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tiporequisito_identificador_Cls = "ColumnSettings";
         Ddo_tiporequisito_identificador_Tooltip = "Op��es";
         Ddo_tiporequisito_identificador_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Tipos de Requisitos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV35TipoRequisito_IdentificadorTitleFilterData',fld:'vTIPOREQUISITO_IDENTIFICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV39TipoRequisito_DescricaoTitleFilterData',fld:'vTIPOREQUISITO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV43TipoRequisito_ClassificacaoTitleFilterData',fld:'vTIPOREQUISITO_CLASSIFICACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47TipoRequisito_LinNegCodTitleFilterData',fld:'vTIPOREQUISITO_LINNEGCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtTipoRequisito_Identificador_Titleformat',ctrl:'TIPOREQUISITO_IDENTIFICADOR',prop:'Titleformat'},{av:'edtTipoRequisito_Identificador_Title',ctrl:'TIPOREQUISITO_IDENTIFICADOR',prop:'Title'},{av:'edtTipoRequisito_Descricao_Titleformat',ctrl:'TIPOREQUISITO_DESCRICAO',prop:'Titleformat'},{av:'edtTipoRequisito_Descricao_Title',ctrl:'TIPOREQUISITO_DESCRICAO',prop:'Title'},{av:'cmbTipoRequisito_Classificacao'},{av:'edtTipoRequisito_LinNegCod_Titleformat',ctrl:'TIPOREQUISITO_LINNEGCOD',prop:'Titleformat'},{av:'edtTipoRequisito_LinNegCod_Title',ctrl:'TIPOREQUISITO_LINNEGCOD',prop:'Title'},{av:'AV53GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV54GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TIPOREQUISITO_IDENTIFICADOR.ONOPTIONCLICKED","{handler:'E12RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tiporequisito_identificador_Activeeventkey',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'ActiveEventKey'},{av:'Ddo_tiporequisito_identificador_Filteredtext_get',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'FilteredText_get'},{av:'Ddo_tiporequisito_identificador_Selectedvalue_get',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tiporequisito_identificador_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'SortedStatus'},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_tiporequisito_descricao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tiporequisito_classificacao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'SortedStatus'},{av:'Ddo_tiporequisito_linnegcod_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPOREQUISITO_DESCRICAO.ONOPTIONCLICKED","{handler:'E13RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tiporequisito_descricao_Activeeventkey',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_tiporequisito_descricao_Filteredtext_get',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_tiporequisito_descricao_Selectedvalue_get',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tiporequisito_descricao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_tiporequisito_identificador_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_tiporequisito_classificacao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'SortedStatus'},{av:'Ddo_tiporequisito_linnegcod_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPOREQUISITO_CLASSIFICACAO.ONOPTIONCLICKED","{handler:'E14RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tiporequisito_classificacao_Activeeventkey',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'ActiveEventKey'},{av:'Ddo_tiporequisito_classificacao_Selectedvalue_get',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tiporequisito_classificacao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'SortedStatus'},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'Ddo_tiporequisito_identificador_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_tiporequisito_descricao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tiporequisito_linnegcod_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPOREQUISITO_LINNEGCOD.ONOPTIONCLICKED","{handler:'E15RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tiporequisito_linnegcod_Activeeventkey',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'ActiveEventKey'},{av:'Ddo_tiporequisito_linnegcod_Filteredtext_get',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'FilteredText_get'},{av:'Ddo_tiporequisito_linnegcod_Filteredtextto_get',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tiporequisito_linnegcod_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_LINNEGCOD',prop:'SortedStatus'},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tiporequisito_identificador_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_IDENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_tiporequisito_descricao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tiporequisito_classificacao_Sortedstatus',ctrl:'DDO_TIPOREQUISITO_CLASSIFICACAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E20RR2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV55Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV56Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV57Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16RR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFTipoRequisito_Identificador',fld:'vTFTIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV58TFTipoRequisito_Identificador_Sel',fld:'vTFTIPOREQUISITO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFTipoRequisito_Descricao',fld:'vTFTIPOREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV41TFTipoRequisito_Descricao_Sel',fld:'vTFTIPOREQUISITO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV48TFTipoRequisito_LinNegCod',fld:'vTFTIPOREQUISITO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV49TFTipoRequisito_LinNegCod_To',fld:'vTFTIPOREQUISITO_LINNEGCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_CLASSIFICACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace',fld:'vDDO_TIPOREQUISITO_LINNEGCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45TFTipoRequisito_Classificacao_Sels',fld:'vTFTIPOREQUISITO_CLASSIFICACAO_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E17RR2',iparms:[{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tiporequisito_identificador_Activeeventkey = "";
         Ddo_tiporequisito_identificador_Filteredtext_get = "";
         Ddo_tiporequisito_identificador_Selectedvalue_get = "";
         Ddo_tiporequisito_descricao_Activeeventkey = "";
         Ddo_tiporequisito_descricao_Filteredtext_get = "";
         Ddo_tiporequisito_descricao_Selectedvalue_get = "";
         Ddo_tiporequisito_classificacao_Activeeventkey = "";
         Ddo_tiporequisito_classificacao_Selectedvalue_get = "";
         Ddo_tiporequisito_linnegcod_Activeeventkey = "";
         Ddo_tiporequisito_linnegcod_Filteredtext_get = "";
         Ddo_tiporequisito_linnegcod_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV36TFTipoRequisito_Identificador = "";
         AV58TFTipoRequisito_Identificador_Sel = "";
         AV40TFTipoRequisito_Descricao = "";
         AV41TFTipoRequisito_Descricao_Sel = "";
         AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace = "";
         AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace = "";
         AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace = "";
         AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace = "";
         AV45TFTipoRequisito_Classificacao_Sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV71Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV51DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35TipoRequisito_IdentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39TipoRequisito_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43TipoRequisito_ClassificacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47TipoRequisito_LinNegCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tiporequisito_identificador_Filteredtext_set = "";
         Ddo_tiporequisito_identificador_Selectedvalue_set = "";
         Ddo_tiporequisito_identificador_Sortedstatus = "";
         Ddo_tiporequisito_descricao_Filteredtext_set = "";
         Ddo_tiporequisito_descricao_Selectedvalue_set = "";
         Ddo_tiporequisito_descricao_Sortedstatus = "";
         Ddo_tiporequisito_classificacao_Selectedvalue_set = "";
         Ddo_tiporequisito_classificacao_Sortedstatus = "";
         Ddo_tiporequisito_linnegcod_Filteredtext_set = "";
         Ddo_tiporequisito_linnegcod_Filteredtextto_set = "";
         Ddo_tiporequisito_linnegcod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV55Update = "";
         AV68Update_GXI = "";
         AV56Delete = "";
         AV69Delete_GXI = "";
         AV57Display = "";
         AV70Display_GXI = "";
         A2042TipoRequisito_Identificador = "";
         A2043TipoRequisito_Descricao = "";
         A2044TipoRequisito_Classificacao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = "";
         lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = "";
         AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel = "";
         AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador = "";
         AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel = "";
         AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao = "";
         H00RR2_A2039TipoRequisito_LinNegCod = new int[1] ;
         H00RR2_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         H00RR2_A2044TipoRequisito_Classificacao = new String[] {""} ;
         H00RR2_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         H00RR2_A2043TipoRequisito_Descricao = new String[] {""} ;
         H00RR2_n2043TipoRequisito_Descricao = new bool[] {false} ;
         H00RR2_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00RR2_A2041TipoRequisito_Codigo = new int[1] ;
         H00RR3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV44TFTipoRequisito_Classificacao_SelsJson = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV28Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTiporequisitotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwtiporequisito__default(),
            new Object[][] {
                new Object[] {
               H00RR2_A2039TipoRequisito_LinNegCod, H00RR2_n2039TipoRequisito_LinNegCod, H00RR2_A2044TipoRequisito_Classificacao, H00RR2_n2044TipoRequisito_Classificacao, H00RR2_A2043TipoRequisito_Descricao, H00RR2_n2043TipoRequisito_Descricao, H00RR2_A2042TipoRequisito_Identificador, H00RR2_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               H00RR3_AGRID_nRecordCount
               }
            }
         );
         AV71Pgmname = "WWTipoRequisito";
         /* GeneXus formulas. */
         AV71Pgmname = "WWTipoRequisito";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_34 ;
      private short nGXsfl_34_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_34_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTipoRequisito_Identificador_Titleformat ;
      private short edtTipoRequisito_Descricao_Titleformat ;
      private short cmbTipoRequisito_Classificacao_Titleformat ;
      private short edtTipoRequisito_LinNegCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV48TFTipoRequisito_LinNegCod ;
      private int AV49TFTipoRequisito_LinNegCod_To ;
      private int A2041TipoRequisito_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tiporequisito_identificador_Datalistupdateminimumcharacters ;
      private int Ddo_tiporequisito_descricao_Datalistupdateminimumcharacters ;
      private int edtavTftiporequisito_identificador_Visible ;
      private int edtavTftiporequisito_identificador_sel_Visible ;
      private int edtavTftiporequisito_descricao_Visible ;
      private int edtavTftiporequisito_descricao_sel_Visible ;
      private int edtavTftiporequisito_linnegcod_Visible ;
      private int edtavTftiporequisito_linnegcod_to_Visible ;
      private int edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Visible ;
      private int A2039TipoRequisito_LinNegCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ;
      private int AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ;
      private int AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV52PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgInsert_Enabled ;
      private int AV72GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV53GridCurrentPage ;
      private long AV54GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tiporequisito_identificador_Activeeventkey ;
      private String Ddo_tiporequisito_identificador_Filteredtext_get ;
      private String Ddo_tiporequisito_identificador_Selectedvalue_get ;
      private String Ddo_tiporequisito_descricao_Activeeventkey ;
      private String Ddo_tiporequisito_descricao_Filteredtext_get ;
      private String Ddo_tiporequisito_descricao_Selectedvalue_get ;
      private String Ddo_tiporequisito_classificacao_Activeeventkey ;
      private String Ddo_tiporequisito_classificacao_Selectedvalue_get ;
      private String Ddo_tiporequisito_linnegcod_Activeeventkey ;
      private String Ddo_tiporequisito_linnegcod_Filteredtext_get ;
      private String Ddo_tiporequisito_linnegcod_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_34_idx="0001" ;
      private String AV71Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tiporequisito_identificador_Caption ;
      private String Ddo_tiporequisito_identificador_Tooltip ;
      private String Ddo_tiporequisito_identificador_Cls ;
      private String Ddo_tiporequisito_identificador_Filteredtext_set ;
      private String Ddo_tiporequisito_identificador_Selectedvalue_set ;
      private String Ddo_tiporequisito_identificador_Dropdownoptionstype ;
      private String Ddo_tiporequisito_identificador_Titlecontrolidtoreplace ;
      private String Ddo_tiporequisito_identificador_Sortedstatus ;
      private String Ddo_tiporequisito_identificador_Filtertype ;
      private String Ddo_tiporequisito_identificador_Datalisttype ;
      private String Ddo_tiporequisito_identificador_Datalistproc ;
      private String Ddo_tiporequisito_identificador_Sortasc ;
      private String Ddo_tiporequisito_identificador_Sortdsc ;
      private String Ddo_tiporequisito_identificador_Loadingdata ;
      private String Ddo_tiporequisito_identificador_Cleanfilter ;
      private String Ddo_tiporequisito_identificador_Noresultsfound ;
      private String Ddo_tiporequisito_identificador_Searchbuttontext ;
      private String Ddo_tiporequisito_descricao_Caption ;
      private String Ddo_tiporequisito_descricao_Tooltip ;
      private String Ddo_tiporequisito_descricao_Cls ;
      private String Ddo_tiporequisito_descricao_Filteredtext_set ;
      private String Ddo_tiporequisito_descricao_Selectedvalue_set ;
      private String Ddo_tiporequisito_descricao_Dropdownoptionstype ;
      private String Ddo_tiporequisito_descricao_Titlecontrolidtoreplace ;
      private String Ddo_tiporequisito_descricao_Sortedstatus ;
      private String Ddo_tiporequisito_descricao_Filtertype ;
      private String Ddo_tiporequisito_descricao_Datalisttype ;
      private String Ddo_tiporequisito_descricao_Datalistproc ;
      private String Ddo_tiporequisito_descricao_Sortasc ;
      private String Ddo_tiporequisito_descricao_Sortdsc ;
      private String Ddo_tiporequisito_descricao_Loadingdata ;
      private String Ddo_tiporequisito_descricao_Cleanfilter ;
      private String Ddo_tiporequisito_descricao_Noresultsfound ;
      private String Ddo_tiporequisito_descricao_Searchbuttontext ;
      private String Ddo_tiporequisito_classificacao_Caption ;
      private String Ddo_tiporequisito_classificacao_Tooltip ;
      private String Ddo_tiporequisito_classificacao_Cls ;
      private String Ddo_tiporequisito_classificacao_Selectedvalue_set ;
      private String Ddo_tiporequisito_classificacao_Dropdownoptionstype ;
      private String Ddo_tiporequisito_classificacao_Titlecontrolidtoreplace ;
      private String Ddo_tiporequisito_classificacao_Sortedstatus ;
      private String Ddo_tiporequisito_classificacao_Datalisttype ;
      private String Ddo_tiporequisito_classificacao_Datalistfixedvalues ;
      private String Ddo_tiporequisito_classificacao_Sortasc ;
      private String Ddo_tiporequisito_classificacao_Sortdsc ;
      private String Ddo_tiporequisito_classificacao_Cleanfilter ;
      private String Ddo_tiporequisito_classificacao_Searchbuttontext ;
      private String Ddo_tiporequisito_linnegcod_Caption ;
      private String Ddo_tiporequisito_linnegcod_Tooltip ;
      private String Ddo_tiporequisito_linnegcod_Cls ;
      private String Ddo_tiporequisito_linnegcod_Filteredtext_set ;
      private String Ddo_tiporequisito_linnegcod_Filteredtextto_set ;
      private String Ddo_tiporequisito_linnegcod_Dropdownoptionstype ;
      private String Ddo_tiporequisito_linnegcod_Titlecontrolidtoreplace ;
      private String Ddo_tiporequisito_linnegcod_Sortedstatus ;
      private String Ddo_tiporequisito_linnegcod_Filtertype ;
      private String Ddo_tiporequisito_linnegcod_Sortasc ;
      private String Ddo_tiporequisito_linnegcod_Sortdsc ;
      private String Ddo_tiporequisito_linnegcod_Cleanfilter ;
      private String Ddo_tiporequisito_linnegcod_Rangefilterfrom ;
      private String Ddo_tiporequisito_linnegcod_Rangefilterto ;
      private String Ddo_tiporequisito_linnegcod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTftiporequisito_identificador_Internalname ;
      private String edtavTftiporequisito_identificador_Jsonclick ;
      private String edtavTftiporequisito_identificador_sel_Internalname ;
      private String edtavTftiporequisito_identificador_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTftiporequisito_descricao_Internalname ;
      private String edtavTftiporequisito_descricao_sel_Internalname ;
      private String edtavTftiporequisito_linnegcod_Internalname ;
      private String edtavTftiporequisito_linnegcod_Jsonclick ;
      private String edtavTftiporequisito_linnegcod_to_Internalname ;
      private String edtavTftiporequisito_linnegcod_to_Jsonclick ;
      private String edtavDdo_tiporequisito_identificadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tiporequisito_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tiporequisito_classificacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tiporequisito_linnegcodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtTipoRequisito_Codigo_Internalname ;
      private String edtTipoRequisito_Identificador_Internalname ;
      private String edtTipoRequisito_Descricao_Internalname ;
      private String cmbTipoRequisito_Classificacao_Internalname ;
      private String A2044TipoRequisito_Classificacao ;
      private String edtTipoRequisito_LinNegCod_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tiporequisito_identificador_Internalname ;
      private String Ddo_tiporequisito_descricao_Internalname ;
      private String Ddo_tiporequisito_classificacao_Internalname ;
      private String Ddo_tiporequisito_linnegcod_Internalname ;
      private String edtTipoRequisito_Identificador_Title ;
      private String edtTipoRequisito_Descricao_Title ;
      private String edtTipoRequisito_LinNegCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String lblTiporequisitotitle_Internalname ;
      private String lblTiporequisitotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_34_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTipoRequisito_Codigo_Jsonclick ;
      private String edtTipoRequisito_Identificador_Jsonclick ;
      private String edtTipoRequisito_Descricao_Jsonclick ;
      private String cmbTipoRequisito_Classificacao_Jsonclick ;
      private String edtTipoRequisito_LinNegCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tiporequisito_identificador_Includesortasc ;
      private bool Ddo_tiporequisito_identificador_Includesortdsc ;
      private bool Ddo_tiporequisito_identificador_Includefilter ;
      private bool Ddo_tiporequisito_identificador_Filterisrange ;
      private bool Ddo_tiporequisito_identificador_Includedatalist ;
      private bool Ddo_tiporequisito_descricao_Includesortasc ;
      private bool Ddo_tiporequisito_descricao_Includesortdsc ;
      private bool Ddo_tiporequisito_descricao_Includefilter ;
      private bool Ddo_tiporequisito_descricao_Filterisrange ;
      private bool Ddo_tiporequisito_descricao_Includedatalist ;
      private bool Ddo_tiporequisito_classificacao_Includesortasc ;
      private bool Ddo_tiporequisito_classificacao_Includesortdsc ;
      private bool Ddo_tiporequisito_classificacao_Includefilter ;
      private bool Ddo_tiporequisito_classificacao_Includedatalist ;
      private bool Ddo_tiporequisito_classificacao_Allowmultipleselection ;
      private bool Ddo_tiporequisito_linnegcod_Includesortasc ;
      private bool Ddo_tiporequisito_linnegcod_Includesortdsc ;
      private bool Ddo_tiporequisito_linnegcod_Includefilter ;
      private bool Ddo_tiporequisito_linnegcod_Filterisrange ;
      private bool Ddo_tiporequisito_linnegcod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2043TipoRequisito_Descricao ;
      private bool n2044TipoRequisito_Classificacao ;
      private bool n2039TipoRequisito_LinNegCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV55Update_IsBlob ;
      private bool AV56Delete_IsBlob ;
      private bool AV57Display_IsBlob ;
      private String AV44TFTipoRequisito_Classificacao_SelsJson ;
      private String AV36TFTipoRequisito_Identificador ;
      private String AV58TFTipoRequisito_Identificador_Sel ;
      private String AV40TFTipoRequisito_Descricao ;
      private String AV41TFTipoRequisito_Descricao_Sel ;
      private String AV38ddo_TipoRequisito_IdentificadorTitleControlIdToReplace ;
      private String AV42ddo_TipoRequisito_DescricaoTitleControlIdToReplace ;
      private String AV46ddo_TipoRequisito_ClassificacaoTitleControlIdToReplace ;
      private String AV50ddo_TipoRequisito_LinNegCodTitleControlIdToReplace ;
      private String AV68Update_GXI ;
      private String AV69Delete_GXI ;
      private String AV70Display_GXI ;
      private String A2042TipoRequisito_Identificador ;
      private String A2043TipoRequisito_Descricao ;
      private String lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ;
      private String lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ;
      private String AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ;
      private String AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ;
      private String AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ;
      private String AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ;
      private String AV55Update ;
      private String AV56Delete ;
      private String AV57Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV28Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbTipoRequisito_Classificacao ;
      private IDataStoreProvider pr_default ;
      private int[] H00RR2_A2039TipoRequisito_LinNegCod ;
      private bool[] H00RR2_n2039TipoRequisito_LinNegCod ;
      private String[] H00RR2_A2044TipoRequisito_Classificacao ;
      private bool[] H00RR2_n2044TipoRequisito_Classificacao ;
      private String[] H00RR2_A2043TipoRequisito_Descricao ;
      private bool[] H00RR2_n2043TipoRequisito_Descricao ;
      private String[] H00RR2_A2042TipoRequisito_Identificador ;
      private int[] H00RR2_A2041TipoRequisito_Codigo ;
      private long[] H00RR3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV45TFTipoRequisito_Classificacao_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35TipoRequisito_IdentificadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39TipoRequisito_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43TipoRequisito_ClassificacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47TipoRequisito_LinNegCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV51DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwtiporequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RR2( IGxContext context ,
                                             String A2044TipoRequisito_Classificacao ,
                                             IGxCollection AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                             String AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                             String AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                             String AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                             String AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                             int AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ,
                                             int AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                             int AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                             String A2042TipoRequisito_Identificador ,
                                             String A2043TipoRequisito_Descricao ,
                                             int A2039TipoRequisito_LinNegCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [TipoRequisito_LinNegCod], [TipoRequisito_Classificacao], [TipoRequisito_Descricao], [TipoRequisito_Identificador], [TipoRequisito_Codigo]";
         sFromString = " FROM [TipoRequisito] WITH (NOLOCK)";
         sOrderString = "";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] like @lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] like @lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] = @AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] = @AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] like @lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] like @lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] = @AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] = @AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] >= @AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] >= @AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] <= @AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] <= @AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Identificador]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Identificador] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Classificacao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Classificacao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_LinNegCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_LinNegCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [TipoRequisito_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00RR3( IGxContext context ,
                                             String A2044TipoRequisito_Classificacao ,
                                             IGxCollection AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels ,
                                             String AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel ,
                                             String AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador ,
                                             String AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel ,
                                             String AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao ,
                                             int AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count ,
                                             int AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod ,
                                             int AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to ,
                                             String A2042TipoRequisito_Identificador ,
                                             String A2043TipoRequisito_Descricao ,
                                             int A2039TipoRequisito_LinNegCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [TipoRequisito] WITH (NOLOCK)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] like @lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] like @lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Identificador] = @AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Identificador] = @AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] like @lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] like @lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_Descricao] = @AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_Descricao] = @AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65WWTipoRequisitoDS_5_Tftiporequisito_classificacao_sels, "[TipoRequisito_Classificacao] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] >= @AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] >= @AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoRequisito_LinNegCod] <= @AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoRequisito_LinNegCod] <= @AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00RR2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] );
               case 1 :
                     return conditional_H00RR3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RR2 ;
          prmH00RR2 = new Object[] {
          new Object[] {"@lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00RR3 ;
          prmH00RR3 = new Object[] {
          new Object[] {"@lV61WWTipoRequisitoDS_1_Tftiporequisito_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV62WWTipoRequisitoDS_2_Tftiporequisito_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWTipoRequisitoDS_3_Tftiporequisito_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV64WWTipoRequisitoDS_4_Tftiporequisito_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV66WWTipoRequisitoDS_6_Tftiporequisito_linnegcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV67WWTipoRequisitoDS_7_Tftiporequisito_linnegcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RR2,11,0,true,false )
             ,new CursorDef("H00RR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RR3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
       }
    }

 }

}
