/*
               File: ExportExtraWWContagemResultadoOS
        Description: Export Extra WWContagem Resultado OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:59:0.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportextrawwcontagemresultadoos : GXProcedure
   {
      public exportextrawwcontagemresultadoos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportextrawwcontagemresultadoos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           String aP2_TFContagemResultado_OsFsOsFm ,
                           String aP3_TFContagemResultado_OsFsOsFm_Sel ,
                           DateTime aP4_TFContagemResultado_DataDmn ,
                           DateTime aP5_TFContagemResultado_DataDmn_To ,
                           DateTime aP6_TFContagemResultado_DataUltCnt ,
                           DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                           String aP8_TFContagemrResultado_SistemaSigla ,
                           String aP9_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP10_TFContagemResultado_ContratadaSigla ,
                           String aP11_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP12_TFContagemResultado_ServicoSigla ,
                           String aP13_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP14_TFContagemResultado_DeflatorCnt ,
                           decimal aP15_TFContagemResultado_DeflatorCnt_To ,
                           String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                           String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                           short aP18_TFContagemResultado_Baseline_Sel ,
                           decimal aP19_TFContagemResultado_PFBFSUltima ,
                           decimal aP20_TFContagemResultado_PFBFSUltima_To ,
                           decimal aP21_TFContagemResultado_PFLFSUltima ,
                           decimal aP22_TFContagemResultado_PFLFSUltima_To ,
                           decimal aP23_TFContagemResultado_PFBFMUltima ,
                           decimal aP24_TFContagemResultado_PFBFMUltima_To ,
                           decimal aP25_TFContagemResultado_PFLFMUltima ,
                           decimal aP26_TFContagemResultado_PFLFMUltima_To ,
                           decimal aP27_TFContagemResultado_PFFinal ,
                           decimal aP28_TFContagemResultado_PFFinal_To ,
                           short aP29_OrderedBy ,
                           bool aP30_OrderedDsc ,
                           String aP31_GridStateXML ,
                           out String aP32_Filename ,
                           out String aP33_ErrorMessage )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV84Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV88TFContagemResultado_OsFsOsFm = aP2_TFContagemResultado_OsFsOsFm;
         this.AV89TFContagemResultado_OsFsOsFm_Sel = aP3_TFContagemResultado_OsFsOsFm_Sel;
         this.AV90TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV91TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV92TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV93TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV94TFContagemrResultado_SistemaSigla = aP8_TFContagemrResultado_SistemaSigla;
         this.AV95TFContagemrResultado_SistemaSigla_Sel = aP9_TFContagemrResultado_SistemaSigla_Sel;
         this.AV96TFContagemResultado_ContratadaSigla = aP10_TFContagemResultado_ContratadaSigla;
         this.AV97TFContagemResultado_ContratadaSigla_Sel = aP11_TFContagemResultado_ContratadaSigla_Sel;
         this.AV98TFContagemResultado_ServicoSigla = aP12_TFContagemResultado_ServicoSigla;
         this.AV99TFContagemResultado_ServicoSigla_Sel = aP13_TFContagemResultado_ServicoSigla_Sel;
         this.AV100TFContagemResultado_DeflatorCnt = aP14_TFContagemResultado_DeflatorCnt;
         this.AV101TFContagemResultado_DeflatorCnt_To = aP15_TFContagemResultado_DeflatorCnt_To;
         this.AV102TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         this.AV105TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV108TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         this.AV109TFContagemResultado_PFBFSUltima = aP19_TFContagemResultado_PFBFSUltima;
         this.AV110TFContagemResultado_PFBFSUltima_To = aP20_TFContagemResultado_PFBFSUltima_To;
         this.AV111TFContagemResultado_PFLFSUltima = aP21_TFContagemResultado_PFLFSUltima;
         this.AV112TFContagemResultado_PFLFSUltima_To = aP22_TFContagemResultado_PFLFSUltima_To;
         this.AV113TFContagemResultado_PFBFMUltima = aP23_TFContagemResultado_PFBFMUltima;
         this.AV114TFContagemResultado_PFBFMUltima_To = aP24_TFContagemResultado_PFBFMUltima_To;
         this.AV115TFContagemResultado_PFLFMUltima = aP25_TFContagemResultado_PFLFMUltima;
         this.AV116TFContagemResultado_PFLFMUltima_To = aP26_TFContagemResultado_PFLFMUltima_To;
         this.AV117TFContagemResultado_PFFinal = aP27_TFContagemResultado_PFFinal;
         this.AV118TFContagemResultado_PFFinal_To = aP28_TFContagemResultado_PFFinal_To;
         this.AV16OrderedBy = aP29_OrderedBy;
         this.AV17OrderedDsc = aP30_OrderedDsc;
         this.AV66GridStateXML = aP31_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP32_Filename=this.AV11Filename;
         aP33_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_Contratada_Codigo ,
                                String aP2_TFContagemResultado_OsFsOsFm ,
                                String aP3_TFContagemResultado_OsFsOsFm_Sel ,
                                DateTime aP4_TFContagemResultado_DataDmn ,
                                DateTime aP5_TFContagemResultado_DataDmn_To ,
                                DateTime aP6_TFContagemResultado_DataUltCnt ,
                                DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                String aP8_TFContagemrResultado_SistemaSigla ,
                                String aP9_TFContagemrResultado_SistemaSigla_Sel ,
                                String aP10_TFContagemResultado_ContratadaSigla ,
                                String aP11_TFContagemResultado_ContratadaSigla_Sel ,
                                String aP12_TFContagemResultado_ServicoSigla ,
                                String aP13_TFContagemResultado_ServicoSigla_Sel ,
                                decimal aP14_TFContagemResultado_DeflatorCnt ,
                                decimal aP15_TFContagemResultado_DeflatorCnt_To ,
                                String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                                String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                                short aP18_TFContagemResultado_Baseline_Sel ,
                                decimal aP19_TFContagemResultado_PFBFSUltima ,
                                decimal aP20_TFContagemResultado_PFBFSUltima_To ,
                                decimal aP21_TFContagemResultado_PFLFSUltima ,
                                decimal aP22_TFContagemResultado_PFLFSUltima_To ,
                                decimal aP23_TFContagemResultado_PFBFMUltima ,
                                decimal aP24_TFContagemResultado_PFBFMUltima_To ,
                                decimal aP25_TFContagemResultado_PFLFMUltima ,
                                decimal aP26_TFContagemResultado_PFLFMUltima_To ,
                                decimal aP27_TFContagemResultado_PFFinal ,
                                decimal aP28_TFContagemResultado_PFFinal_To ,
                                short aP29_OrderedBy ,
                                bool aP30_OrderedDsc ,
                                String aP31_GridStateXML ,
                                out String aP32_Filename )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV84Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV88TFContagemResultado_OsFsOsFm = aP2_TFContagemResultado_OsFsOsFm;
         this.AV89TFContagemResultado_OsFsOsFm_Sel = aP3_TFContagemResultado_OsFsOsFm_Sel;
         this.AV90TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV91TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV92TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV93TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV94TFContagemrResultado_SistemaSigla = aP8_TFContagemrResultado_SistemaSigla;
         this.AV95TFContagemrResultado_SistemaSigla_Sel = aP9_TFContagemrResultado_SistemaSigla_Sel;
         this.AV96TFContagemResultado_ContratadaSigla = aP10_TFContagemResultado_ContratadaSigla;
         this.AV97TFContagemResultado_ContratadaSigla_Sel = aP11_TFContagemResultado_ContratadaSigla_Sel;
         this.AV98TFContagemResultado_ServicoSigla = aP12_TFContagemResultado_ServicoSigla;
         this.AV99TFContagemResultado_ServicoSigla_Sel = aP13_TFContagemResultado_ServicoSigla_Sel;
         this.AV100TFContagemResultado_DeflatorCnt = aP14_TFContagemResultado_DeflatorCnt;
         this.AV101TFContagemResultado_DeflatorCnt_To = aP15_TFContagemResultado_DeflatorCnt_To;
         this.AV102TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         this.AV105TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV108TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         this.AV109TFContagemResultado_PFBFSUltima = aP19_TFContagemResultado_PFBFSUltima;
         this.AV110TFContagemResultado_PFBFSUltima_To = aP20_TFContagemResultado_PFBFSUltima_To;
         this.AV111TFContagemResultado_PFLFSUltima = aP21_TFContagemResultado_PFLFSUltima;
         this.AV112TFContagemResultado_PFLFSUltima_To = aP22_TFContagemResultado_PFLFSUltima_To;
         this.AV113TFContagemResultado_PFBFMUltima = aP23_TFContagemResultado_PFBFMUltima;
         this.AV114TFContagemResultado_PFBFMUltima_To = aP24_TFContagemResultado_PFBFMUltima_To;
         this.AV115TFContagemResultado_PFLFMUltima = aP25_TFContagemResultado_PFLFMUltima;
         this.AV116TFContagemResultado_PFLFMUltima_To = aP26_TFContagemResultado_PFLFMUltima_To;
         this.AV117TFContagemResultado_PFFinal = aP27_TFContagemResultado_PFFinal;
         this.AV118TFContagemResultado_PFFinal_To = aP28_TFContagemResultado_PFFinal_To;
         this.AV16OrderedBy = aP29_OrderedBy;
         this.AV17OrderedDsc = aP30_OrderedDsc;
         this.AV66GridStateXML = aP31_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP32_Filename=this.AV11Filename;
         aP33_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_TFContagemResultado_OsFsOsFm ,
                                 String aP3_TFContagemResultado_OsFsOsFm_Sel ,
                                 DateTime aP4_TFContagemResultado_DataDmn ,
                                 DateTime aP5_TFContagemResultado_DataDmn_To ,
                                 DateTime aP6_TFContagemResultado_DataUltCnt ,
                                 DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                 String aP8_TFContagemrResultado_SistemaSigla ,
                                 String aP9_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP10_TFContagemResultado_ContratadaSigla ,
                                 String aP11_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP12_TFContagemResultado_ServicoSigla ,
                                 String aP13_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP14_TFContagemResultado_DeflatorCnt ,
                                 decimal aP15_TFContagemResultado_DeflatorCnt_To ,
                                 String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                                 String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                                 short aP18_TFContagemResultado_Baseline_Sel ,
                                 decimal aP19_TFContagemResultado_PFBFSUltima ,
                                 decimal aP20_TFContagemResultado_PFBFSUltima_To ,
                                 decimal aP21_TFContagemResultado_PFLFSUltima ,
                                 decimal aP22_TFContagemResultado_PFLFSUltima_To ,
                                 decimal aP23_TFContagemResultado_PFBFMUltima ,
                                 decimal aP24_TFContagemResultado_PFBFMUltima_To ,
                                 decimal aP25_TFContagemResultado_PFLFMUltima ,
                                 decimal aP26_TFContagemResultado_PFLFMUltima_To ,
                                 decimal aP27_TFContagemResultado_PFFinal ,
                                 decimal aP28_TFContagemResultado_PFFinal_To ,
                                 short aP29_OrderedBy ,
                                 bool aP30_OrderedDsc ,
                                 String aP31_GridStateXML ,
                                 out String aP32_Filename ,
                                 out String aP33_ErrorMessage )
      {
         exportextrawwcontagemresultadoos objexportextrawwcontagemresultadoos;
         objexportextrawwcontagemresultadoos = new exportextrawwcontagemresultadoos();
         objexportextrawwcontagemresultadoos.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportextrawwcontagemresultadoos.AV84Contratada_Codigo = aP1_Contratada_Codigo;
         objexportextrawwcontagemresultadoos.AV88TFContagemResultado_OsFsOsFm = aP2_TFContagemResultado_OsFsOsFm;
         objexportextrawwcontagemresultadoos.AV89TFContagemResultado_OsFsOsFm_Sel = aP3_TFContagemResultado_OsFsOsFm_Sel;
         objexportextrawwcontagemresultadoos.AV90TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         objexportextrawwcontagemresultadoos.AV91TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         objexportextrawwcontagemresultadoos.AV92TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         objexportextrawwcontagemresultadoos.AV93TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         objexportextrawwcontagemresultadoos.AV94TFContagemrResultado_SistemaSigla = aP8_TFContagemrResultado_SistemaSigla;
         objexportextrawwcontagemresultadoos.AV95TFContagemrResultado_SistemaSigla_Sel = aP9_TFContagemrResultado_SistemaSigla_Sel;
         objexportextrawwcontagemresultadoos.AV96TFContagemResultado_ContratadaSigla = aP10_TFContagemResultado_ContratadaSigla;
         objexportextrawwcontagemresultadoos.AV97TFContagemResultado_ContratadaSigla_Sel = aP11_TFContagemResultado_ContratadaSigla_Sel;
         objexportextrawwcontagemresultadoos.AV98TFContagemResultado_ServicoSigla = aP12_TFContagemResultado_ServicoSigla;
         objexportextrawwcontagemresultadoos.AV99TFContagemResultado_ServicoSigla_Sel = aP13_TFContagemResultado_ServicoSigla_Sel;
         objexportextrawwcontagemresultadoos.AV100TFContagemResultado_DeflatorCnt = aP14_TFContagemResultado_DeflatorCnt;
         objexportextrawwcontagemresultadoos.AV101TFContagemResultado_DeflatorCnt_To = aP15_TFContagemResultado_DeflatorCnt_To;
         objexportextrawwcontagemresultadoos.AV102TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         objexportextrawwcontagemresultadoos.AV105TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         objexportextrawwcontagemresultadoos.AV108TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         objexportextrawwcontagemresultadoos.AV109TFContagemResultado_PFBFSUltima = aP19_TFContagemResultado_PFBFSUltima;
         objexportextrawwcontagemresultadoos.AV110TFContagemResultado_PFBFSUltima_To = aP20_TFContagemResultado_PFBFSUltima_To;
         objexportextrawwcontagemresultadoos.AV111TFContagemResultado_PFLFSUltima = aP21_TFContagemResultado_PFLFSUltima;
         objexportextrawwcontagemresultadoos.AV112TFContagemResultado_PFLFSUltima_To = aP22_TFContagemResultado_PFLFSUltima_To;
         objexportextrawwcontagemresultadoos.AV113TFContagemResultado_PFBFMUltima = aP23_TFContagemResultado_PFBFMUltima;
         objexportextrawwcontagemresultadoos.AV114TFContagemResultado_PFBFMUltima_To = aP24_TFContagemResultado_PFBFMUltima_To;
         objexportextrawwcontagemresultadoos.AV115TFContagemResultado_PFLFMUltima = aP25_TFContagemResultado_PFLFMUltima;
         objexportextrawwcontagemresultadoos.AV116TFContagemResultado_PFLFMUltima_To = aP26_TFContagemResultado_PFLFMUltima_To;
         objexportextrawwcontagemresultadoos.AV117TFContagemResultado_PFFinal = aP27_TFContagemResultado_PFFinal;
         objexportextrawwcontagemresultadoos.AV118TFContagemResultado_PFFinal_To = aP28_TFContagemResultado_PFFinal_To;
         objexportextrawwcontagemresultadoos.AV16OrderedBy = aP29_OrderedBy;
         objexportextrawwcontagemresultadoos.AV17OrderedDsc = aP30_OrderedDsc;
         objexportextrawwcontagemresultadoos.AV66GridStateXML = aP31_GridStateXML;
         objexportextrawwcontagemresultadoos.AV11Filename = "" ;
         objexportextrawwcontagemresultadoos.AV12ErrorMessage = "" ;
         objexportextrawwcontagemresultadoos.context.SetSubmitInitialConfig(context);
         objexportextrawwcontagemresultadoos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportextrawwcontagemresultadoos);
         aP32_Filename=this.AV11Filename;
         aP33_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportextrawwcontagemresultadoos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         AV120Contratada_DoUsuario = AV9WWPContext.gxTpr_Contratada_codigo;
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportExtraWWContagemResultadoOS-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportExtraWWContagemResultadoOS-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Resultado das Contagens";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( ! ( (0==AV18Contratada_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18Contratada_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV84Contratada_Codigo) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Contratada";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV84Contratada_Codigo;
         }
         AV67GridState.gxTpr_Dynamicfilters.FromXml(AV66GridStateXML, "");
         if ( AV67GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV68GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV67GridState.gxTpr_Dynamicfilters.Item(1));
            AV19DynamicFiltersSelector1 = AV68GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV20DynamicFiltersOperator1 = AV68GridStateDynamicFilter.gxTpr_Operator;
               AV21ContagemResultado_OsFsOsFm1 = AV68GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContagemResultado_OsFsOsFm1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV20DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV21ContagemResultado_OsFsOsFm1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV22ContagemResultado_DataDmn1 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
               AV23ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV23ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV22ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV23ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV75ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
               AV76ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV75ContagemResultado_DataUltCnt1) || ! (DateTime.MinValue==AV76ContagemResultado_DataUltCnt_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV75ContagemResultado_DataUltCnt1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV76ContagemResultado_DataUltCnt_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV24ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV24ContagemResultado_ContadorFM1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV24ContagemResultado_ContadorFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV25ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV25ContagemResultado_SistemaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV25ContagemResultado_SistemaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV26ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV26ContagemResultado_ContratadaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV26ContagemResultado_ContratadaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV27ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV27ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV27ContagemResultado_NaoCnfDmnCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV28ContagemResultado_StatusDmn1 = AV68GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_StatusDmn1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_StatusDmn1)) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV28ContagemResultado_StatusDmn1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
            {
               AV81ContagemResultado_StatusDmnVnc1 = AV68GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81ContagemResultado_StatusDmnVnc1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn Vinculada";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81ContagemResultado_StatusDmnVnc1)) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV81ContagemResultado_StatusDmnVnc1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
            {
               AV20DynamicFiltersOperator1 = AV68GridStateDynamicFilter.gxTpr_Operator;
               AV31ContagemResultado_EsforcoSoma1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV31ContagemResultado_EsforcoSoma1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV20DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV31ContagemResultado_EsforcoSoma1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV32ContagemResultado_Baseline1 = AV68GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultado_Baseline1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV32ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV32ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV33ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV33ContagemResultado_Servico1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV33ContagemResultado_Servico1;
               }
            }
            if ( AV67GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV68GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV67GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV68GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV36DynamicFiltersOperator2 = AV68GridStateDynamicFilter.gxTpr_Operator;
                  AV37ContagemResultado_OsFsOsFm2 = AV68GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContagemResultado_OsFsOsFm2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV36DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV37ContagemResultado_OsFsOsFm2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV38ContagemResultado_DataDmn2 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
                  AV39ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV38ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV39ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV38ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV39ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV77ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
                  AV78ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV77ContagemResultado_DataUltCnt2) || ! (DateTime.MinValue==AV78ContagemResultado_DataUltCnt_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV77ContagemResultado_DataUltCnt2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV78ContagemResultado_DataUltCnt_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV40ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV40ContagemResultado_ContadorFM2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV40ContagemResultado_ContadorFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV41ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV41ContagemResultado_SistemaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV41ContagemResultado_SistemaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV42ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV42ContagemResultado_ContratadaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV42ContagemResultado_ContratadaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV43ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV43ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV43ContagemResultado_NaoCnfDmnCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV44ContagemResultado_StatusDmn2 = AV68GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV44ContagemResultado_StatusDmn2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
               {
                  AV82ContagemResultado_StatusDmnVnc2 = AV68GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ContagemResultado_StatusDmnVnc2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn Vinculada";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ContagemResultado_StatusDmnVnc2)) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV82ContagemResultado_StatusDmnVnc2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
               {
                  AV36DynamicFiltersOperator2 = AV68GridStateDynamicFilter.gxTpr_Operator;
                  AV47ContagemResultado_EsforcoSoma2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV47ContagemResultado_EsforcoSoma2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV36DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV47ContagemResultado_EsforcoSoma2;
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV48ContagemResultado_Baseline2 = AV68GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_Baseline2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV48ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV48ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV49ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV49ContagemResultado_Servico2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV49ContagemResultado_Servico2;
                  }
               }
               if ( AV67GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV50DynamicFiltersEnabled3 = true;
                  AV68GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV67GridState.gxTpr_Dynamicfilters.Item(3));
                  AV51DynamicFiltersSelector3 = AV68GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV52DynamicFiltersOperator3 = AV68GridStateDynamicFilter.gxTpr_Operator;
                     AV53ContagemResultado_OsFsOsFm3 = AV68GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_OsFsOsFm3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV52DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV52DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV52DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV53ContagemResultado_OsFsOsFm3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV54ContagemResultado_DataDmn3 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
                     AV55ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV54ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV55ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV79ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Value, 2);
                     AV80ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV68GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV79ContagemResultado_DataUltCnt3) || ! (DateTime.MinValue==AV80ContagemResultado_DataUltCnt_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV79ContagemResultado_DataUltCnt3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV80ContagemResultado_DataUltCnt_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV56ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV56ContagemResultado_ContadorFM3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV56ContagemResultado_ContadorFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV57ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV57ContagemResultado_SistemaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV57ContagemResultado_SistemaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV58ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV58ContagemResultado_ContratadaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV58ContagemResultado_ContratadaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV59ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV59ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV59ContagemResultado_NaoCnfDmnCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV60ContagemResultado_StatusDmn3 = AV68GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_StatusDmn3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_StatusDmn3)) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV60ContagemResultado_StatusDmn3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
                  {
                     AV83ContagemResultado_StatusDmnVnc3 = AV68GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ContagemResultado_StatusDmnVnc3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn Vinculada";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ContagemResultado_StatusDmnVnc3)) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV83ContagemResultado_StatusDmnVnc3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                  {
                     AV52DynamicFiltersOperator3 = AV68GridStateDynamicFilter.gxTpr_Operator;
                     AV63ContagemResultado_EsforcoSoma3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV63ContagemResultado_EsforcoSoma3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV52DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                        }
                        else if ( AV52DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                        }
                        else if ( AV52DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV63ContagemResultado_EsforcoSoma3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV64ContagemResultado_Baseline3 = AV68GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_Baseline3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV64ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV64ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV65ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV68GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV65ContagemResultado_Servico3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV65ContagemResultado_Servico3;
                     }
                  }
               }
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV89TFContagemResultado_OsFsOsFm_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV89TFContagemResultado_OsFsOsFm_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContagemResultado_OsFsOsFm)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV88TFContagemResultado_OsFsOsFm;
            }
         }
         if ( ! ( (DateTime.MinValue==AV90TFContagemResultado_DataDmn) && (DateTime.MinValue==AV91TFContagemResultado_DataDmn_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Demanda";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV90TFContagemResultado_DataDmn ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV91TFContagemResultado_DataDmn_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV92TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV93TFContagemResultado_DataUltCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV92TFContagemResultado_DataUltCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV93TFContagemResultado_DataUltCnt_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV95TFContagemrResultado_SistemaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV95TFContagemrResultado_SistemaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV94TFContagemrResultado_SistemaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV94TFContagemrResultado_SistemaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV97TFContagemResultado_ContratadaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV97TFContagemResultado_ContratadaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV96TFContagemResultado_ContratadaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV96TFContagemResultado_ContratadaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV99TFContagemResultado_ServicoSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV99TFContagemResultado_ServicoSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV98TFContagemResultado_ServicoSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV98TFContagemResultado_ServicoSigla;
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV100TFContagemResultado_DeflatorCnt) && (Convert.ToDecimal(0)==AV101TFContagemResultado_DeflatorCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Deflator";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV100TFContagemResultado_DeflatorCnt);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV101TFContagemResultado_DeflatorCnt_To);
         }
         AV103TFContagemResultado_StatusDmn_Sels.FromJSonString(AV102TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( ( AV103TFContagemResultado_StatusDmn_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV119i = 1;
            AV123GXV1 = 1;
            while ( AV123GXV1 <= AV103TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV104TFContagemResultado_StatusDmn_Sel = AV103TFContagemResultado_StatusDmn_Sels.GetString(AV123GXV1);
               if ( AV119i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatusdemanda.getDescription(context,AV104TFContagemResultado_StatusDmn_Sel);
               AV119i = (long)(AV119i+1);
               AV123GXV1 = (int)(AV123GXV1+1);
            }
         }
         AV106TFContagemResultado_StatusUltCnt_Sels.FromJSonString(AV105TFContagemResultado_StatusUltCnt_SelsJson);
         if ( ! ( ( AV106TFContagemResultado_StatusUltCnt_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Cnt";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV119i = 1;
            AV124GXV2 = 1;
            while ( AV124GXV2 <= AV106TFContagemResultado_StatusUltCnt_Sels.Count )
            {
               AV107TFContagemResultado_StatusUltCnt_Sel = (short)(AV106TFContagemResultado_StatusUltCnt_Sels.GetNumeric(AV124GXV2));
               if ( AV119i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatuscontagem.getDescription(context,AV107TFContagemResultado_StatusUltCnt_Sel);
               AV119i = (long)(AV119i+1);
               AV124GXV2 = (int)(AV124GXV2+1);
            }
         }
         if ( ! ( (0==AV108TFContagemResultado_Baseline_Sel) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "BS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( AV108TFContagemResultado_Baseline_Sel == 1 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Marcado";
            }
            else if ( AV108TFContagemResultado_Baseline_Sel == 2 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Desmarcado";
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV109TFContagemResultado_PFBFSUltima) && (Convert.ToDecimal(0)==AV110TFContagemResultado_PFBFSUltima_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB FS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV109TFContagemResultado_PFBFSUltima);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV110TFContagemResultado_PFBFSUltima_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV111TFContagemResultado_PFLFSUltima) && (Convert.ToDecimal(0)==AV112TFContagemResultado_PFLFSUltima_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL FS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV111TFContagemResultado_PFLFSUltima);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV112TFContagemResultado_PFLFSUltima_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV113TFContagemResultado_PFBFMUltima) && (Convert.ToDecimal(0)==AV114TFContagemResultado_PFBFMUltima_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB FM";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV113TFContagemResultado_PFBFMUltima);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV114TFContagemResultado_PFBFMUltima_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV115TFContagemResultado_PFLFMUltima) && (Convert.ToDecimal(0)==AV116TFContagemResultado_PFLFMUltima_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL FM";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV115TFContagemResultado_PFLFMUltima);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV116TFContagemResultado_PFLFMUltima_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV117TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV118TFContagemResultado_PFFinal_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PF Final";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV117TFContagemResultado_PFFinal);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV118TFContagemResultado_PFFinal_To);
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "OS Ref|OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Demanda";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "Contagem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "Prestadora";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Servi�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Deflator";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Status Dmn";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Status Cnt";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "BS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "PFB FS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "PFL FS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = "PFB FM";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "PFL FM";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Text = "PF Final";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV18Contratada_AreaTrabalhoCod;
         AV127ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV84Contratada_Codigo;
         AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV19DynamicFiltersSelector1;
         AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV20DynamicFiltersOperator1;
         AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV21ContagemResultado_OsFsOsFm1;
         AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV22ContagemResultado_DataDmn1;
         AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV23ContagemResultado_DataDmn_To1;
         AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV75ContagemResultado_DataUltCnt1;
         AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV76ContagemResultado_DataUltCnt_To1;
         AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV24ContagemResultado_ContadorFM1;
         AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV25ContagemResultado_SistemaCod1;
         AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV26ContagemResultado_ContratadaCod1;
         AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV27ContagemResultado_NaoCnfDmnCod1;
         AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV28ContagemResultado_StatusDmn1;
         AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV81ContagemResultado_StatusDmnVnc1;
         AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV31ContagemResultado_EsforcoSoma1;
         AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV32ContagemResultado_Baseline1;
         AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV33ContagemResultado_Servico1;
         AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV34DynamicFiltersEnabled2;
         AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV35DynamicFiltersSelector2;
         AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV36DynamicFiltersOperator2;
         AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV37ContagemResultado_OsFsOsFm2;
         AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV38ContagemResultado_DataDmn2;
         AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV39ContagemResultado_DataDmn_To2;
         AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV77ContagemResultado_DataUltCnt2;
         AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV78ContagemResultado_DataUltCnt_To2;
         AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV40ContagemResultado_ContadorFM2;
         AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV41ContagemResultado_SistemaCod2;
         AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV42ContagemResultado_ContratadaCod2;
         AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV43ContagemResultado_NaoCnfDmnCod2;
         AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV44ContagemResultado_StatusDmn2;
         AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV82ContagemResultado_StatusDmnVnc2;
         AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV47ContagemResultado_EsforcoSoma2;
         AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV48ContagemResultado_Baseline2;
         AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV49ContagemResultado_Servico2;
         AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV50DynamicFiltersEnabled3;
         AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV51DynamicFiltersSelector3;
         AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV52DynamicFiltersOperator3;
         AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV53ContagemResultado_OsFsOsFm3;
         AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV54ContagemResultado_DataDmn3;
         AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV55ContagemResultado_DataDmn_To3;
         AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV79ContagemResultado_DataUltCnt3;
         AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV80ContagemResultado_DataUltCnt_To3;
         AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV56ContagemResultado_ContadorFM3;
         AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV57ContagemResultado_SistemaCod3;
         AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV58ContagemResultado_ContratadaCod3;
         AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV59ContagemResultado_NaoCnfDmnCod3;
         AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV60ContagemResultado_StatusDmn3;
         AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV83ContagemResultado_StatusDmnVnc3;
         AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV63ContagemResultado_EsforcoSoma3;
         AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV64ContagemResultado_Baseline3;
         AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV65ContagemResultado_Servico3;
         AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV88TFContagemResultado_OsFsOsFm;
         AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV89TFContagemResultado_OsFsOsFm_Sel;
         AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV90TFContagemResultado_DataDmn;
         AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV91TFContagemResultado_DataDmn_To;
         AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV92TFContagemResultado_DataUltCnt;
         AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV93TFContagemResultado_DataUltCnt_To;
         AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV94TFContagemrResultado_SistemaSigla;
         AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV95TFContagemrResultado_SistemaSigla_Sel;
         AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV96TFContagemResultado_ContratadaSigla;
         AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV97TFContagemResultado_ContratadaSigla_Sel;
         AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV98TFContagemResultado_ServicoSigla;
         AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV99TFContagemResultado_ServicoSigla_Sel;
         AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV100TFContagemResultado_DeflatorCnt;
         AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV101TFContagemResultado_DeflatorCnt_To;
         AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV103TFContagemResultado_StatusDmn_Sels;
         AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV106TFContagemResultado_StatusUltCnt_Sels;
         AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV108TFContagemResultado_Baseline_Sel;
         AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV109TFContagemResultado_PFBFSUltima;
         AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV110TFContagemResultado_PFBFSUltima_To;
         AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV111TFContagemResultado_PFLFSUltima;
         AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV112TFContagemResultado_PFLFSUltima_To;
         AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV113TFContagemResultado_PFBFMUltima;
         AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV114TFContagemResultado_PFBFMUltima_To;
         AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV115TFContagemResultado_PFLFMUltima;
         AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV116TFContagemResultado_PFLFMUltima_To;
         AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV117TFContagemResultado_PFFinal;
         AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV118TFContagemResultado_PFFinal_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV120Contratada_DoUsuario ,
                                              AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV67GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV16OrderedBy ,
                                              AV17OrderedDsc ,
                                              AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT
                                              }
         });
         lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00534 */
         pr_default.execute(0, new Object[] {AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV120Contratada_DoUsuario, AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00534_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00534_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00534_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00534_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00534_A1583ContagemResultado_TipoRegistro[0];
            A801ContagemResultado_ServicoSigla = P00534_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00534_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00534_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00534_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P00534_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00534_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00534_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00534_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00534_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00534_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00534_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00534_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00534_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00534_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00534_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00534_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00534_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00534_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00534_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00534_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00534_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00534_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00534_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00534_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00534_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00534_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00534_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00534_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00534_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00534_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00534_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00534_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00534_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00534_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00534_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00534_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00534_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00534_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00534_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00534_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00534_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00534_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00534_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00534_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00534_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00534_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00534_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00534_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00534_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00534_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00534_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00534_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00534_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00534_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00534_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00534_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00534_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00534_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00534_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00534_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00534_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            if ( (Convert.ToDecimal(0)==AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV13CellRow = (int)(AV13CellRow+1);
                  /* Execute user subroutine: 'BEFOREWRITELINE' */
                  S172 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A501ContagemResultado_OsFsOsFm;
                  GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = A803ContagemResultado_ContratadaSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = A801ContagemResultado_ServicoSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Number = (double)(A802ContagemResultado_DeflatorCnt);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,A531ContagemResultado_StatusUltCnt);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "*";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Number = (double)(A684ContagemResultado_PFBFSUltima);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Number = (double)(A685ContagemResultado_PFLFSUltima);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Number = (double)(A682ContagemResultado_PFBFMUltima);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Number = (double)(A683ContagemResultado_PFLFMUltima);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Number = (double)(A574ContagemResultado_PFFinal);
                  /* Execute user subroutine: 'AFTERWRITELINE' */
                  S182 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV67GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV68GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV19DynamicFiltersSelector1 = "";
         AV21ContagemResultado_OsFsOsFm1 = "";
         AV22ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV23ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV75ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV76ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV28ContagemResultado_StatusDmn1 = "";
         AV81ContagemResultado_StatusDmnVnc1 = "";
         AV32ContagemResultado_Baseline1 = "";
         AV35DynamicFiltersSelector2 = "";
         AV37ContagemResultado_OsFsOsFm2 = "";
         AV38ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV39ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV77ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV78ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV44ContagemResultado_StatusDmn2 = "";
         AV82ContagemResultado_StatusDmnVnc2 = "";
         AV48ContagemResultado_Baseline2 = "";
         AV51DynamicFiltersSelector3 = "";
         AV53ContagemResultado_OsFsOsFm3 = "";
         AV54ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV55ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV79ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV80ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV60ContagemResultado_StatusDmn3 = "";
         AV83ContagemResultado_StatusDmnVnc3 = "";
         AV64ContagemResultado_Baseline3 = "";
         AV103TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV104TFContagemResultado_StatusDmn_Sel = "";
         AV106TFContagemResultado_StatusUltCnt_Sels = new GxSimpleCollection();
         AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = "";
         AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = "";
         AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = "";
         AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = "";
         AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = "";
         AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = "";
         AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = "";
         AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = "";
         AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = "";
         AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = "";
         AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = "";
         AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = "";
         AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = "";
         AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = "";
         AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = "";
         AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = "";
         AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A771ContagemResultado_StatusDmnVnc = "";
         A509ContagemrResultado_SistemaSigla = "";
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00534_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00534_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00534_A602ContagemResultado_OSVinculada = new int[1] ;
         P00534_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00534_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00534_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00534_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00534_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00534_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00534_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00534_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00534_A601ContagemResultado_Servico = new int[1] ;
         P00534_n601ContagemResultado_Servico = new bool[] {false} ;
         P00534_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00534_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00534_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00534_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00534_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00534_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00534_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00534_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00534_A489ContagemResultado_SistemaCod = new int[1] ;
         P00534_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00534_A508ContagemResultado_Owner = new int[1] ;
         P00534_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00534_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00534_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00534_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00534_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00534_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00534_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00534_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00534_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00534_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00534_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00534_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00534_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00534_A584ContagemResultado_ContadorFM = new int[1] ;
         P00534_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00534_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00534_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00534_A457ContagemResultado_Demanda = new String[] {""} ;
         P00534_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00534_A456ContagemResultado_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportextrawwcontagemresultadoos__default(),
            new Object[][] {
                new Object[] {
               P00534_A1553ContagemResultado_CntSrvCod, P00534_n1553ContagemResultado_CntSrvCod, P00534_A602ContagemResultado_OSVinculada, P00534_n602ContagemResultado_OSVinculada, P00534_A1583ContagemResultado_TipoRegistro, P00534_A801ContagemResultado_ServicoSigla, P00534_n801ContagemResultado_ServicoSigla, P00534_A803ContagemResultado_ContratadaSigla, P00534_n803ContagemResultado_ContratadaSigla, P00534_A509ContagemrResultado_SistemaSigla,
               P00534_n509ContagemrResultado_SistemaSigla, P00534_A601ContagemResultado_Servico, P00534_n601ContagemResultado_Servico, P00534_A598ContagemResultado_Baseline, P00534_n598ContagemResultado_Baseline, P00534_A771ContagemResultado_StatusDmnVnc, P00534_n771ContagemResultado_StatusDmnVnc, P00534_A484ContagemResultado_StatusDmn, P00534_n484ContagemResultado_StatusDmn, P00534_A468ContagemResultado_NaoCnfDmnCod,
               P00534_n468ContagemResultado_NaoCnfDmnCod, P00534_A489ContagemResultado_SistemaCod, P00534_n489ContagemResultado_SistemaCod, P00534_A508ContagemResultado_Owner, P00534_A471ContagemResultado_DataDmn, P00534_A490ContagemResultado_ContratadaCod, P00534_n490ContagemResultado_ContratadaCod, P00534_A52Contratada_AreaTrabalhoCod, P00534_n52Contratada_AreaTrabalhoCod, P00534_A683ContagemResultado_PFLFMUltima,
               P00534_A682ContagemResultado_PFBFMUltima, P00534_A685ContagemResultado_PFLFSUltima, P00534_A684ContagemResultado_PFBFSUltima, P00534_A531ContagemResultado_StatusUltCnt, P00534_A802ContagemResultado_DeflatorCnt, P00534_A510ContagemResultado_EsforcoSoma, P00534_n510ContagemResultado_EsforcoSoma, P00534_A584ContagemResultado_ContadorFM, P00534_A566ContagemResultado_DataUltCnt, P00534_A493ContagemResultado_DemandaFM,
               P00534_n493ContagemResultado_DemandaFM, P00534_A457ContagemResultado_Demanda, P00534_n457ContagemResultado_Demanda, P00534_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV108TFContagemResultado_Baseline_Sel ;
      private short AV16OrderedBy ;
      private short AV20DynamicFiltersOperator1 ;
      private short AV36DynamicFiltersOperator2 ;
      private short AV52DynamicFiltersOperator3 ;
      private short AV107TFContagemResultado_StatusUltCnt_Sel ;
      private short AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ;
      private short AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ;
      private short AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ;
      private short AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV18Contratada_AreaTrabalhoCod ;
      private int AV84Contratada_Codigo ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV120Contratada_DoUsuario ;
      private int AV15Random ;
      private int AV24ContagemResultado_ContadorFM1 ;
      private int AV25ContagemResultado_SistemaCod1 ;
      private int AV26ContagemResultado_ContratadaCod1 ;
      private int AV27ContagemResultado_NaoCnfDmnCod1 ;
      private int AV31ContagemResultado_EsforcoSoma1 ;
      private int AV33ContagemResultado_Servico1 ;
      private int AV40ContagemResultado_ContadorFM2 ;
      private int AV41ContagemResultado_SistemaCod2 ;
      private int AV42ContagemResultado_ContratadaCod2 ;
      private int AV43ContagemResultado_NaoCnfDmnCod2 ;
      private int AV47ContagemResultado_EsforcoSoma2 ;
      private int AV49ContagemResultado_Servico2 ;
      private int AV56ContagemResultado_ContadorFM3 ;
      private int AV57ContagemResultado_SistemaCod3 ;
      private int AV58ContagemResultado_ContratadaCod3 ;
      private int AV59ContagemResultado_NaoCnfDmnCod3 ;
      private int AV63ContagemResultado_EsforcoSoma3 ;
      private int AV65ContagemResultado_Servico3 ;
      private int AV123GXV1 ;
      private int AV124GXV2 ;
      private int AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ;
      private int AV127ExtraWWContagemResultadoOSDS_2_Contratada_codigo ;
      private int AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ;
      private int AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ;
      private int AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ;
      private int AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ;
      private int AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ;
      private int AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ;
      private int AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ;
      private int AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ;
      private int AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ;
      private int AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ;
      private int AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ;
      private int AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ;
      private int AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ;
      private int AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ;
      private int AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ;
      private int AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ;
      private int AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ;
      private int AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ;
      private int AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV67GridState_gxTpr_Dynamicfilters_Count ;
      private int AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private long AV119i ;
      private decimal AV100TFContagemResultado_DeflatorCnt ;
      private decimal AV101TFContagemResultado_DeflatorCnt_To ;
      private decimal AV109TFContagemResultado_PFBFSUltima ;
      private decimal AV110TFContagemResultado_PFBFSUltima_To ;
      private decimal AV111TFContagemResultado_PFLFSUltima ;
      private decimal AV112TFContagemResultado_PFLFSUltima_To ;
      private decimal AV113TFContagemResultado_PFBFMUltima ;
      private decimal AV114TFContagemResultado_PFBFMUltima_To ;
      private decimal AV115TFContagemResultado_PFLFMUltima ;
      private decimal AV116TFContagemResultado_PFLFMUltima_To ;
      private decimal AV117TFContagemResultado_PFFinal ;
      private decimal AV118TFContagemResultado_PFFinal_To ;
      private decimal AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ;
      private decimal AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ;
      private decimal AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ;
      private decimal AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ;
      private decimal AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ;
      private decimal AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ;
      private decimal AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ;
      private decimal AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ;
      private decimal AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ;
      private decimal AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ;
      private decimal AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ;
      private decimal AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ;
      private decimal A802ContagemResultado_DeflatorCnt ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal2 ;
      private String AV94TFContagemrResultado_SistemaSigla ;
      private String AV95TFContagemrResultado_SistemaSigla_Sel ;
      private String AV96TFContagemResultado_ContratadaSigla ;
      private String AV97TFContagemResultado_ContratadaSigla_Sel ;
      private String AV98TFContagemResultado_ServicoSigla ;
      private String AV99TFContagemResultado_ServicoSigla_Sel ;
      private String AV28ContagemResultado_StatusDmn1 ;
      private String AV81ContagemResultado_StatusDmnVnc1 ;
      private String AV32ContagemResultado_Baseline1 ;
      private String AV44ContagemResultado_StatusDmn2 ;
      private String AV82ContagemResultado_StatusDmnVnc2 ;
      private String AV48ContagemResultado_Baseline2 ;
      private String AV60ContagemResultado_StatusDmn3 ;
      private String AV83ContagemResultado_StatusDmnVnc3 ;
      private String AV64ContagemResultado_Baseline3 ;
      private String AV104TFContagemResultado_StatusDmn_Sel ;
      private String AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ;
      private String AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ;
      private String AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ;
      private String AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ;
      private String AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ;
      private String AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ;
      private String AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ;
      private String AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ;
      private String AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ;
      private String AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ;
      private String AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime GXt_dtime1 ;
      private DateTime AV90TFContagemResultado_DataDmn ;
      private DateTime AV91TFContagemResultado_DataDmn_To ;
      private DateTime AV92TFContagemResultado_DataUltCnt ;
      private DateTime AV93TFContagemResultado_DataUltCnt_To ;
      private DateTime AV22ContagemResultado_DataDmn1 ;
      private DateTime AV23ContagemResultado_DataDmn_To1 ;
      private DateTime AV75ContagemResultado_DataUltCnt1 ;
      private DateTime AV76ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV38ContagemResultado_DataDmn2 ;
      private DateTime AV39ContagemResultado_DataDmn_To2 ;
      private DateTime AV77ContagemResultado_DataUltCnt2 ;
      private DateTime AV78ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV54ContagemResultado_DataDmn3 ;
      private DateTime AV55ContagemResultado_DataDmn_To3 ;
      private DateTime AV79ContagemResultado_DataUltCnt3 ;
      private DateTime AV80ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ;
      private DateTime AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ;
      private DateTime AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ;
      private DateTime AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ;
      private DateTime AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ;
      private DateTime AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ;
      private DateTime AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ;
      private DateTime AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ;
      private DateTime AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ;
      private DateTime AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ;
      private DateTime AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool AV17OrderedDsc ;
      private bool returnInSub ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool AV50DynamicFiltersEnabled3 ;
      private bool AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ;
      private bool AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ;
      private bool A598ContagemResultado_Baseline ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n601ContagemResultado_Servico ;
      private bool n598ContagemResultado_Baseline ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private String AV102TFContagemResultado_StatusDmn_SelsJson ;
      private String AV105TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV66GridStateXML ;
      private String AV88TFContagemResultado_OsFsOsFm ;
      private String AV89TFContagemResultado_OsFsOsFm_Sel ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV19DynamicFiltersSelector1 ;
      private String AV21ContagemResultado_OsFsOsFm1 ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV37ContagemResultado_OsFsOsFm2 ;
      private String AV51DynamicFiltersSelector3 ;
      private String AV53ContagemResultado_OsFsOsFm3 ;
      private String AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ;
      private String AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ;
      private String AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ;
      private String AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ;
      private String lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00534_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00534_n1553ContagemResultado_CntSrvCod ;
      private int[] P00534_A602ContagemResultado_OSVinculada ;
      private bool[] P00534_n602ContagemResultado_OSVinculada ;
      private short[] P00534_A1583ContagemResultado_TipoRegistro ;
      private String[] P00534_A801ContagemResultado_ServicoSigla ;
      private bool[] P00534_n801ContagemResultado_ServicoSigla ;
      private String[] P00534_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00534_n803ContagemResultado_ContratadaSigla ;
      private String[] P00534_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00534_n509ContagemrResultado_SistemaSigla ;
      private int[] P00534_A601ContagemResultado_Servico ;
      private bool[] P00534_n601ContagemResultado_Servico ;
      private bool[] P00534_A598ContagemResultado_Baseline ;
      private bool[] P00534_n598ContagemResultado_Baseline ;
      private String[] P00534_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00534_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00534_A484ContagemResultado_StatusDmn ;
      private bool[] P00534_n484ContagemResultado_StatusDmn ;
      private int[] P00534_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00534_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00534_A489ContagemResultado_SistemaCod ;
      private bool[] P00534_n489ContagemResultado_SistemaCod ;
      private int[] P00534_A508ContagemResultado_Owner ;
      private DateTime[] P00534_A471ContagemResultado_DataDmn ;
      private int[] P00534_A490ContagemResultado_ContratadaCod ;
      private bool[] P00534_n490ContagemResultado_ContratadaCod ;
      private int[] P00534_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00534_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00534_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00534_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00534_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00534_A684ContagemResultado_PFBFSUltima ;
      private short[] P00534_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00534_A802ContagemResultado_DeflatorCnt ;
      private int[] P00534_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00534_n510ContagemResultado_EsforcoSoma ;
      private int[] P00534_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00534_A566ContagemResultado_DataUltCnt ;
      private String[] P00534_A493ContagemResultado_DemandaFM ;
      private bool[] P00534_n493ContagemResultado_DemandaFM ;
      private String[] P00534_A457ContagemResultado_Demanda ;
      private bool[] P00534_n457ContagemResultado_Demanda ;
      private int[] P00534_A456ContagemResultado_Codigo ;
      private String aP32_Filename ;
      private String aP33_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV106TFContagemResultado_StatusUltCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV103TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV67GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV68GridStateDynamicFilter ;
   }

   public class exportextrawwcontagemresultadoos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00534( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                             int AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                             int AV120Contratada_DoUsuario ,
                                             String AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                             short AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                             String AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                             int AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                             int AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                             int AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                             String AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                             String AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                             int AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                             String AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                             int AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                             bool AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                             String AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                             short AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                             String AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                             DateTime AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                             DateTime AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                             int AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                             int AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                             int AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                             String AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                             String AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                             int AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                             String AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                             int AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                             bool AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                             String AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                             short AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                             String AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                             DateTime AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                             DateTime AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                             int AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                             int AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                             int AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                             String AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                             String AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                             int AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                             String AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                             int AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                             String AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                             DateTime AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                             String AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                             String AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                             String AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                             String AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                             int AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                             int AV67GridState_gxTpr_Dynamicfilters_Count ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A771ContagemResultado_StatusDmnVnc ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV16OrderedBy ,
                                             bool AV17OrderedDsc ,
                                             DateTime AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                             int AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                             DateTime AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                             int AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                             DateTime AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                             DateTime AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                             int AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                             DateTime AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                             decimal A802ContagemResultado_DeflatorCnt ,
                                             decimal AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                             int AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             decimal AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                             decimal AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                             decimal AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                             decimal AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             decimal AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                             decimal AV203ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV204ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [124] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV193ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV193ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         if ( AV120Contratada_DoUsuario > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV120Contratada_DoUsuario)";
         }
         else
         {
            GXv_int3[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[63] = 1;
            GXv_int3[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[65] = 1;
            GXv_int3[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[67] = 1;
            GXv_int3[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int3[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int3[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int3[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int3[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV129ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int3[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV142ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[79] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[80] = 1;
            GXv_int3[81] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[82] = 1;
            GXv_int3[83] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[84] = 1;
            GXv_int3[85] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[86] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[87] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[88] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[89] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int3[92] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int3[93] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int3[94] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV146ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int3[95] = 1;
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[96] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[97] = 1;
            GXv_int3[98] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[99] = 1;
            GXv_int3[100] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[101] = 1;
            GXv_int3[102] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[103] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[104] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[105] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[106] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int3[107] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[108] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int3[109] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int3[110] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int3[111] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV163ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int3[112] = 1;
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV176ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int3[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int3[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int3[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int3[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int3[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int3[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int3[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int3[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[123] = 1;
         }
         if ( AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV192ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV194ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV67GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV16OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         }
         else if ( AV16OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV16OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T2.[Servico_Codigo], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV16OrderedBy == 4 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV16OrderedBy == 5 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T2.[Servico_Codigo], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( ( AV16OrderedBy == 6 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         }
         else if ( ( AV16OrderedBy == 6 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla] DESC";
         }
         else if ( ( AV16OrderedBy == 7 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline]";
         }
         else if ( ( AV16OrderedBy == 7 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00534(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (short)dynConstraints[75] , (bool)dynConstraints[76] , (DateTime)dynConstraints[77] , (DateTime)dynConstraints[78] , (DateTime)dynConstraints[79] , (int)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (DateTime)dynConstraints[83] , (DateTime)dynConstraints[84] , (int)dynConstraints[85] , (DateTime)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (decimal)dynConstraints[91] , (decimal)dynConstraints[92] , (decimal)dynConstraints[93] , (int)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (decimal)dynConstraints[108] , (decimal)dynConstraints[109] , (short)dynConstraints[110] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00534 ;
          prmP00534 = new Object[] {
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV150ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV190ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV191ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV193ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV195ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV196ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV197ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV198ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV199ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV200ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV201ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV202ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV126ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV120Contratada_DoUsuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV147ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV157ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV165ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV171ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV178ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV184ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV186ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV188ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00534", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00534,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
       }
    }

 }

}
