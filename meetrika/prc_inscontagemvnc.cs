/*
               File: PRC_INSContagemVnc
        Description: Inserir Contagem na Vinculada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:28.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inscontagemvnc : GXProcedure
   {
      public prc_inscontagemvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_inscontagemvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_ContagemResultado_Codigo ,
                           decimal aP2_ContagemResultado_PFBFS ,
                           decimal aP3_ContagemResultado_PFLFS ,
                           decimal aP4_ContagemResultado_PFBFM )
      {
         this.AV8Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV20ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         this.AV10ContagemResultado_PFBFS = aP2_ContagemResultado_PFBFS;
         this.AV11ContagemResultado_PFLFS = aP3_ContagemResultado_PFLFS;
         this.AV12ContagemResultado_PFBFM = aP4_ContagemResultado_PFBFM;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_ContagemResultado_Codigo ,
                                 decimal aP2_ContagemResultado_PFBFS ,
                                 decimal aP3_ContagemResultado_PFLFS ,
                                 decimal aP4_ContagemResultado_PFBFM )
      {
         prc_inscontagemvnc objprc_inscontagemvnc;
         objprc_inscontagemvnc = new prc_inscontagemvnc();
         objprc_inscontagemvnc.AV8Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_inscontagemvnc.AV20ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         objprc_inscontagemvnc.AV10ContagemResultado_PFBFS = aP2_ContagemResultado_PFBFS;
         objprc_inscontagemvnc.AV11ContagemResultado_PFLFS = aP3_ContagemResultado_PFLFS;
         objprc_inscontagemvnc.AV12ContagemResultado_PFBFM = aP4_ContagemResultado_PFBFM;
         objprc_inscontagemvnc.context.SetSubmitInitialConfig(context);
         objprc_inscontagemvnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inscontagemvnc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inscontagemvnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23GXLvl2 = 0;
         /* Using cursor P005L2 */
         pr_default.execute(0, new Object[] {AV20ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A602ContagemResultado_OSVinculada = P005L2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P005L2_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = P005L2_A456ContagemResultado_Codigo[0];
            AV23GXLvl2 = 1;
            AV19ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV23GXLvl2 == 0 )
         {
            this.cleanup();
            if (true) return;
         }
         /* Using cursor P005L3 */
         pr_default.execute(1, new Object[] {AV19ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P005L3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P005L3_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P005L3_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P005L3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005L3_n601ContagemResultado_Servico[0];
            A601ContagemResultado_Servico = P005L3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005L3_n601ContagemResultado_Servico[0];
            AV9Servico_Codigo = A601ContagemResultado_Servico;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         /* Using cursor P005L4 */
         pr_default.execute(2, new Object[] {AV8Contratada_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A74Contrato_Codigo = P005L4_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P005L4_A39Contratada_Codigo[0];
            /* Using cursor P005L5 */
            pr_default.execute(3, new Object[] {A74Contrato_Codigo, AV9Servico_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A155Servico_Codigo = P005L5_A155Servico_Codigo[0];
               A558Servico_Percentual = P005L5_A558Servico_Percentual[0];
               n558Servico_Percentual = P005L5_n558Servico_Percentual[0];
               A160ContratoServicos_Codigo = P005L5_A160ContratoServicos_Codigo[0];
               AV14Servico_Percentual = A558Servico_Percentual;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            pr_default.readNext(2);
         }
         pr_default.close(2);
         /* Optimized UPDATE. */
         /* Using cursor P005L6 */
         pr_default.execute(4, new Object[] {AV19ContagemResultado_OSVinculada});
         pr_default.close(4);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         /* End optimized UPDATE. */
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV19ContagemResultado_OSVinculada;
         A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         A511ContagemResultado_HoraCnt = context.localUtil.ServerTime( context, "DEFAULT");
         A458ContagemResultado_PFBFS = AV10ContagemResultado_PFBFS;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = AV11ContagemResultado_PFLFS;
         n459ContagemResultado_PFLFS = false;
         A460ContagemResultado_PFBFM = AV12ContagemResultado_PFBFM;
         n460ContagemResultado_PFBFM = false;
         A461ContagemResultado_PFLFM = (decimal)(AV12ContagemResultado_PFBFM*AV14Servico_Percentual);
         n461ContagemResultado_PFLFM = false;
         A800ContagemResultado_Deflator = AV14Servico_Percentual;
         n800ContagemResultado_Deflator = false;
         A517ContagemResultado_Ultima = true;
         A483ContagemResultado_StatusCnt = 5;
         A470ContagemResultado_ContadorFMCod = AV18ContagemResultado_ContadorFMCod;
         A482ContagemResultadoContagens_Esforco = 0;
         A462ContagemResultado_Divergencia = 0;
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         n469ContagemResultado_NaoCnfCntCod = true;
         /* Using cursor P005L7 */
         pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator});
         pr_default.close(5);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(5) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_INSContagemVnc");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005L2_A602ContagemResultado_OSVinculada = new int[1] ;
         P005L2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P005L2_A456ContagemResultado_Codigo = new int[1] ;
         P005L3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P005L3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P005L3_A456ContagemResultado_Codigo = new int[1] ;
         P005L3_A601ContagemResultado_Servico = new int[1] ;
         P005L3_n601ContagemResultado_Servico = new bool[] {false} ;
         P005L4_A74Contrato_Codigo = new int[1] ;
         P005L4_A39Contratada_Codigo = new int[1] ;
         P005L5_A74Contrato_Codigo = new int[1] ;
         P005L5_A155Servico_Codigo = new int[1] ;
         P005L5_A558Servico_Percentual = new decimal[1] ;
         P005L5_n558Servico_Percentual = new bool[] {false} ;
         P005L5_A160ContratoServicos_Codigo = new int[1] ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_inscontagemvnc__default(),
            new Object[][] {
                new Object[] {
               P005L2_A602ContagemResultado_OSVinculada, P005L2_n602ContagemResultado_OSVinculada, P005L2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P005L3_A1553ContagemResultado_CntSrvCod, P005L3_n1553ContagemResultado_CntSrvCod, P005L3_A456ContagemResultado_Codigo, P005L3_A601ContagemResultado_Servico, P005L3_n601ContagemResultado_Servico
               }
               , new Object[] {
               P005L4_A74Contrato_Codigo, P005L4_A39Contratada_Codigo
               }
               , new Object[] {
               P005L5_A74Contrato_Codigo, P005L5_A155Servico_Codigo, P005L5_A558Servico_Percentual, P005L5_n558Servico_Percentual, P005L5_A160ContratoServicos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV23GXLvl2 ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private int AV8Contratada_Codigo ;
      private int AV20ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int AV19ContagemResultado_OSVinculada ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int AV9Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int GX_INS72 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int AV18ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private decimal AV10ContagemResultado_PFBFS ;
      private decimal AV11ContagemResultado_PFLFS ;
      private decimal AV12ContagemResultado_PFBFM ;
      private decimal A558Servico_Percentual ;
      private decimal AV14Servico_Percentual ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A462ContagemResultado_Divergencia ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private String Gx_emsg ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n558Servico_Percentual ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P005L2_A602ContagemResultado_OSVinculada ;
      private bool[] P005L2_n602ContagemResultado_OSVinculada ;
      private int[] P005L2_A456ContagemResultado_Codigo ;
      private int[] P005L3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P005L3_n1553ContagemResultado_CntSrvCod ;
      private int[] P005L3_A456ContagemResultado_Codigo ;
      private int[] P005L3_A601ContagemResultado_Servico ;
      private bool[] P005L3_n601ContagemResultado_Servico ;
      private int[] P005L4_A74Contrato_Codigo ;
      private int[] P005L4_A39Contratada_Codigo ;
      private int[] P005L5_A74Contrato_Codigo ;
      private int[] P005L5_A155Servico_Codigo ;
      private decimal[] P005L5_A558Servico_Percentual ;
      private bool[] P005L5_n558Servico_Percentual ;
      private int[] P005L5_A160ContratoServicos_Codigo ;
   }

   public class prc_inscontagemvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005L2 ;
          prmP005L2 = new Object[] {
          new Object[] {"@AV20ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005L3 ;
          prmP005L3 = new Object[] {
          new Object[] {"@AV19ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005L4 ;
          prmP005L4 = new Object[] {
          new Object[] {"@AV8Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005L5 ;
          prmP005L5 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005L6 ;
          prmP005L6 = new Object[] {
          new Object[] {"@AV19ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005L7 ;
          prmP005L7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005L2", "SELECT TOP 1 [ContagemResultado_OSVinculada], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_OSVinculada] = @AV20ContagemResultado_Codigo ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005L2,1,0,false,true )
             ,new CursorDef("P005L3", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Servico_Codigo] AS ContagemResultado_Servico FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV19ContagemResultado_OSVinculada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005L3,1,0,false,true )
             ,new CursorDef("P005L4", "SELECT [Contrato_Codigo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV8Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005L4,100,0,true,false )
             ,new CursorDef("P005L5", "SELECT [Contrato_Codigo], [Servico_Codigo], [Servico_Percentual], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([Servico_Codigo] = @AV9Servico_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005L5,100,0,false,false )
             ,new CursorDef("P005L6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=CONVERT(BIT, 0)  WHERE ([ContagemResultado_Codigo] = @AV19ContagemResultado_OSVinculada) AND ([ContagemResultado_Ultima] = 1)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005L6)
             ,new CursorDef("P005L7", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, convert( DATETIME, '17530101', 112 ), '', convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP005L7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                return;
       }
    }

 }

}
