/*
               File: PRC_DltLote_NfeArq
        Description: Deleta Nfe anexa ao lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:49.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltlote_nfearq : GXProcedure
   {
      public prc_dltlote_nfearq( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltlote_nfearq( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      public int executeUdp( )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.A596Lote_Codigo;
         return A596Lote_Codigo ;
      }

      public void executeSubmit( ref int aP0_Lote_Codigo )
      {
         prc_dltlote_nfearq objprc_dltlote_nfearq;
         objprc_dltlote_nfearq = new prc_dltlote_nfearq();
         objprc_dltlote_nfearq.A596Lote_Codigo = aP0_Lote_Codigo;
         objprc_dltlote_nfearq.context.SetSubmitInitialConfig(context);
         objprc_dltlote_nfearq.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltlote_nfearq);
         aP0_Lote_Codigo=this.A596Lote_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltlote_nfearq)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007S2 */
         pr_default.execute(0, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A679Lote_NFeNomeArq = P007S2_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = P007S2_n679Lote_NFeNomeArq[0];
            A680Lote_NFeTipoArq = P007S2_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = P007S2_n680Lote_NFeTipoArq[0];
            A678Lote_NFeArq = P007S2_A678Lote_NFeArq[0];
            n678Lote_NFeArq = P007S2_n678Lote_NFeArq[0];
            A678Lote_NFeArq = "";
            n678Lote_NFeArq = false;
            n678Lote_NFeArq = true;
            A679Lote_NFeNomeArq = "";
            n679Lote_NFeNomeArq = false;
            n679Lote_NFeNomeArq = true;
            A680Lote_NFeTipoArq = "";
            n680Lote_NFeTipoArq = false;
            n680Lote_NFeTipoArq = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P007S3 */
            A680Lote_NFeTipoArq = FileUtil.GetFileType( A678Lote_NFeArq);
            n680Lote_NFeTipoArq = false;
            A679Lote_NFeNomeArq = FileUtil.GetFileName( A678Lote_NFeArq);
            n679Lote_NFeNomeArq = false;
            pr_default.execute(1, new Object[] {n678Lote_NFeArq, A678Lote_NFeArq, n680Lote_NFeTipoArq, A680Lote_NFeTipoArq, n679Lote_NFeNomeArq, A679Lote_NFeNomeArq, A596Lote_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
            if (true) break;
            /* Using cursor P007S4 */
            A680Lote_NFeTipoArq = FileUtil.GetFileType( A678Lote_NFeArq);
            n680Lote_NFeTipoArq = false;
            A679Lote_NFeNomeArq = FileUtil.GetFileName( A678Lote_NFeArq);
            n679Lote_NFeNomeArq = false;
            pr_default.execute(2, new Object[] {n678Lote_NFeArq, A678Lote_NFeArq, n680Lote_NFeTipoArq, A680Lote_NFeTipoArq, n679Lote_NFeNomeArq, A679Lote_NFeNomeArq, A596Lote_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DltLote_NfeArq");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007S2_A596Lote_Codigo = new int[1] ;
         P007S2_A679Lote_NFeNomeArq = new String[] {""} ;
         P007S2_n679Lote_NFeNomeArq = new bool[] {false} ;
         P007S2_A680Lote_NFeTipoArq = new String[] {""} ;
         P007S2_n680Lote_NFeTipoArq = new bool[] {false} ;
         P007S2_A678Lote_NFeArq = new String[] {""} ;
         P007S2_n678Lote_NFeArq = new bool[] {false} ;
         A679Lote_NFeNomeArq = "";
         A680Lote_NFeTipoArq = "";
         A678Lote_NFeArq = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltlote_nfearq__default(),
            new Object[][] {
                new Object[] {
               P007S2_A596Lote_Codigo, P007S2_A679Lote_NFeNomeArq, P007S2_n679Lote_NFeNomeArq, P007S2_A680Lote_NFeTipoArq, P007S2_n680Lote_NFeTipoArq, P007S2_A678Lote_NFeArq, P007S2_n678Lote_NFeArq
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A596Lote_Codigo ;
      private String scmdbuf ;
      private String A679Lote_NFeNomeArq ;
      private String A680Lote_NFeTipoArq ;
      private bool n679Lote_NFeNomeArq ;
      private bool n680Lote_NFeTipoArq ;
      private bool n678Lote_NFeArq ;
      private String A678Lote_NFeArq ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P007S2_A596Lote_Codigo ;
      private String[] P007S2_A679Lote_NFeNomeArq ;
      private bool[] P007S2_n679Lote_NFeNomeArq ;
      private String[] P007S2_A680Lote_NFeTipoArq ;
      private bool[] P007S2_n680Lote_NFeTipoArq ;
      private String[] P007S2_A678Lote_NFeArq ;
      private bool[] P007S2_n678Lote_NFeArq ;
   }

   public class prc_dltlote_nfearq__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007S2 ;
          prmP007S2 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007S3 ;
          prmP007S3 = new Object[] {
          new Object[] {"@Lote_NFeArq",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Lote_NFeTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_NFeNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007S4 ;
          prmP007S4 = new Object[] {
          new Object[] {"@Lote_NFeArq",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Lote_NFeTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_NFeNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007S2", "SELECT TOP 1 [Lote_Codigo], [Lote_NFeNomeArq], [Lote_NFeTipoArq], [Lote_NFeArq] FROM [Lote] WITH (UPDLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ORDER BY [Lote_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007S2,1,0,true,true )
             ,new CursorDef("P007S3", "UPDATE [Lote] SET [Lote_NFeArq]=@Lote_NFeArq, [Lote_NFeTipoArq]=@Lote_NFeTipoArq, [Lote_NFeNomeArq]=@Lote_NFeNomeArq  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007S3)
             ,new CursorDef("P007S4", "UPDATE [Lote] SET [Lote_NFeArq]=@Lote_NFeArq, [Lote_NFeTipoArq]=@Lote_NFeTipoArq, [Lote_NFeNomeArq]=@Lote_NFeNomeArq  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP007S4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
