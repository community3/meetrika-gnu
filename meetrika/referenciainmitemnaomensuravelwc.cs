/*
               File: ReferenciaINMItemNaoMensuravelWC
        Description: Referencia INMItem Nao Mensuravel WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:49.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class referenciainmitemnaomensuravelwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public referenciainmitemnaomensuravelwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public referenciainmitemnaomensuravelwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ReferenciaINM_Codigo )
      {
         this.AV7ReferenciaINM_Codigo = aP0_ReferenciaINM_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavItemnaomensuravel_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavItemnaomensuravel_tipo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavItemnaomensuravel_tipo3 = new GXCombobox();
         cmbItemNaoMensuravel_Tipo = new GXCombobox();
         chkItemNaoMensuravel_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ReferenciaINM_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_84_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV32ItemNaoMensuravel_Codigo1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
                  AV18ItemNaoMensuravel_Descricao1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
                  AV34ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV35ItemNaoMensuravel_Codigo2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
                  AV22ItemNaoMensuravel_Descricao2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
                  AV37ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
                  AV24DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                  AV38ItemNaoMensuravel_Codigo3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
                  AV26ItemNaoMensuravel_Descricao3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
                  AV40ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV48TFItemNaoMensuravel_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
                  AV49TFItemNaoMensuravel_Codigo_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFItemNaoMensuravel_Codigo_Sel", AV49TFItemNaoMensuravel_Codigo_Sel);
                  AV52TFItemNaoMensuravel_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
                  AV53TFItemNaoMensuravel_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFItemNaoMensuravel_Descricao_Sel", AV53TFItemNaoMensuravel_Descricao_Sel);
                  AV56TFItemNaoMensuravel_Referencia = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
                  AV57TFItemNaoMensuravel_Referencia_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFItemNaoMensuravel_Referencia_Sel", AV57TFItemNaoMensuravel_Referencia_Sel);
                  AV60TFItemNaoMensuravel_Valor = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
                  AV61TFItemNaoMensuravel_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
                  AV68TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
                  AV7ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
                  AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
                  AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
                  AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace", AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace);
                  AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
                  AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
                  AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
                  AV78Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV65TFItemNaoMensuravel_Tipo_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
                  AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
                  A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
                  A715ItemNaoMensuravel_Codigo = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAE52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV78Pgmname = "ReferenciaINMItemNaoMensuravelWC";
               context.Gx_err = 0;
               WSE52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Referencia INMItem Nao Mensuravel WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117225037");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("referenciainmitemnaomensuravelwc.aspx") + "?" + UrlEncode("" +AV7ReferenciaINM_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO1", StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO1", AV18ItemNaoMensuravel_Descricao1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO2", StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO2", AV22ItemNaoMensuravel_Descricao2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO3", StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO3", AV26ItemNaoMensuravel_Descricao3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL", StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO", AV52TFItemNaoMensuravel_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL", AV53TFItemNaoMensuravel_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA", StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA_SEL", StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR", StringUtil.LTrim( StringUtil.NToC( AV60TFItemNaoMensuravel_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV61TFItemNaoMensuravel_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV70DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV70DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA", AV47ItemNaoMensuravel_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA", AV47ItemNaoMensuravel_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA", AV51ItemNaoMensuravel_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA", AV51ItemNaoMensuravel_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_REFERENCIATITLEFILTERDATA", AV55ItemNaoMensuravel_ReferenciaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_REFERENCIATITLEFILTERDATA", AV55ItemNaoMensuravel_ReferenciaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA", AV59ItemNaoMensuravel_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA", AV59ItemNaoMensuravel_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA", AV63ItemNaoMensuravel_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA", AV63ItemNaoMensuravel_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA", AV67ItemNaoMensuravel_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA", AV67ItemNaoMensuravel_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV78Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFITEMNAOMENSURAVEL_TIPO_SELS", AV65TFItemNaoMensuravel_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFITEMNAOMENSURAVEL_TIPO_SELS", AV65TFItemNaoMensuravel_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistproc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_referencia_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_referencia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_referencia_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_referencia_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_referencia_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalistproc", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_itemnaomensuravel_referencia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Loadingdata", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Noresultsfound", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_referencia_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormE52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("referenciainmitemnaomensuravelwc.js", "?20203117225377");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ReferenciaINMItemNaoMensuravelWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Referencia INMItem Nao Mensuravel WC" ;
      }

      protected void WBE50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "referenciainmitemnaomensuravelwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_E52( true) ;
         }
         else
         {
            wb_table1_2_E52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtReferenciaINM_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReferenciaINM_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtReferenciaINM_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_codigo_Internalname, StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo), StringUtil.RTrim( context.localUtil.Format( AV48TFItemNaoMensuravel_Codigo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_codigo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_codigo_sel_Internalname, StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel), StringUtil.RTrim( context.localUtil.Format( AV49TFItemNaoMensuravel_Codigo_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_codigo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_codigo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfitemnaomensuravel_descricao_Internalname, AV52TFItemNaoMensuravel_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavTfitemnaomensuravel_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfitemnaomensuravel_descricao_sel_Internalname, AV53TFItemNaoMensuravel_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavTfitemnaomensuravel_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_referencia_Internalname, StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia), StringUtil.RTrim( context.localUtil.Format( AV56TFItemNaoMensuravel_Referencia, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_referencia_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_referencia_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_referencia_sel_Internalname, StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel), StringUtil.RTrim( context.localUtil.Format( AV57TFItemNaoMensuravel_Referencia_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_referencia_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_referencia_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFItemNaoMensuravel_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,108);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV61TFItemNaoMensuravel_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV61TFItemNaoMensuravel_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,109);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Internalname, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTE52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Referencia INMItem Nao Mensuravel WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPE50( ) ;
            }
         }
      }

      protected void WSE52( )
      {
         STARTE52( ) ;
         EVTE52( ) ;
      }

      protected void EVTE52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11E52 */
                                    E11E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12E52 */
                                    E12E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13E52 */
                                    E13E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_REFERENCIA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14E52 */
                                    E14E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_VALOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15E52 */
                                    E15E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16E52 */
                                    E16E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17E52 */
                                    E17E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18E52 */
                                    E18E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19E52 */
                                    E19E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20E52 */
                                    E20E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21E52 */
                                    E21E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22E52 */
                                    E22E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23E52 */
                                    E23E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24E52 */
                                    E24E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E25E52 */
                                    E25E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E26E52 */
                                    E26E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E27E52 */
                                    E27E52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE50( ) ;
                              }
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV76Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV30Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV77Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV30Delete))));
                              A715ItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtItemNaoMensuravel_Codigo_Internalname));
                              A714ItemNaoMensuravel_Descricao = cgiGet( edtItemNaoMensuravel_Descricao_Internalname);
                              A1804ItemNaoMensuravel_Referencia = cgiGet( edtItemNaoMensuravel_Referencia_Internalname);
                              n1804ItemNaoMensuravel_Referencia = false;
                              A719ItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".");
                              cmbItemNaoMensuravel_Tipo.Name = cmbItemNaoMensuravel_Tipo_Internalname;
                              cmbItemNaoMensuravel_Tipo.CurrentValue = cgiGet( cmbItemNaoMensuravel_Tipo_Internalname);
                              A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cgiGet( cmbItemNaoMensuravel_Tipo_Internalname), "."));
                              A716ItemNaoMensuravel_Ativo = StringUtil.StrToBool( cgiGet( chkItemNaoMensuravel_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E28E52 */
                                          E28E52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E29E52 */
                                          E29E52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E30E52 */
                                          E30E52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_codigo1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO1"), AV32ItemNaoMensuravel_Codigo1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_descricao1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO1"), AV18ItemNaoMensuravel_Descricao1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_tipo1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO1"), ",", ".") != Convert.ToDecimal( AV34ItemNaoMensuravel_Tipo1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_codigo2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO2"), AV35ItemNaoMensuravel_Codigo2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_descricao2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO2"), AV22ItemNaoMensuravel_Descricao2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_tipo2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO2"), ",", ".") != Convert.ToDecimal( AV37ItemNaoMensuravel_Tipo2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_codigo3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO3"), AV38ItemNaoMensuravel_Codigo3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_descricao3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO3"), AV26ItemNaoMensuravel_Descricao3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Itemnaomensuravel_tipo3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO3"), ",", ".") != Convert.ToDecimal( AV40ItemNaoMensuravel_Tipo3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_codigo Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO"), AV48TFItemNaoMensuravel_Codigo) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_codigo_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL"), AV49TFItemNaoMensuravel_Codigo_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO"), AV52TFItemNaoMensuravel_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL"), AV53TFItemNaoMensuravel_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_referencia Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA"), AV56TFItemNaoMensuravel_Referencia) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_referencia_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA_SEL"), AV57TFItemNaoMensuravel_Referencia_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_valor Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR"), ",", ".") != AV60TFItemNaoMensuravel_Valor )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_valor_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR_TO"), ",", ".") != AV61TFItemNaoMensuravel_Valor_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfitemnaomensuravel_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV68TFItemNaoMensuravel_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPE50( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEE52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormE52( ) ;
            }
         }
      }

      protected void PAE52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ITEMNAOMENSURAVEL_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector1.addItem("ITEMNAOMENSURAVEL_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("ITEMNAOMENSURAVEL_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavItemnaomensuravel_tipo1.Name = "vITEMNAOMENSURAVEL_TIPO1";
            cmbavItemnaomensuravel_tipo1.WebTags = "";
            cmbavItemnaomensuravel_tipo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavItemnaomensuravel_tipo1.addItem("1", "PC", 0);
            cmbavItemnaomensuravel_tipo1.addItem("2", "PF", 0);
            if ( cmbavItemnaomensuravel_tipo1.ItemCount > 0 )
            {
               AV34ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ITEMNAOMENSURAVEL_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector2.addItem("ITEMNAOMENSURAVEL_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("ITEMNAOMENSURAVEL_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavItemnaomensuravel_tipo2.Name = "vITEMNAOMENSURAVEL_TIPO2";
            cmbavItemnaomensuravel_tipo2.WebTags = "";
            cmbavItemnaomensuravel_tipo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavItemnaomensuravel_tipo2.addItem("1", "PC", 0);
            cmbavItemnaomensuravel_tipo2.addItem("2", "PF", 0);
            if ( cmbavItemnaomensuravel_tipo2.ItemCount > 0 )
            {
               AV37ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ITEMNAOMENSURAVEL_CODIGO", "C�digo", 0);
            cmbavDynamicfiltersselector3.addItem("ITEMNAOMENSURAVEL_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector3.addItem("ITEMNAOMENSURAVEL_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            cmbavItemnaomensuravel_tipo3.Name = "vITEMNAOMENSURAVEL_TIPO3";
            cmbavItemnaomensuravel_tipo3.WebTags = "";
            cmbavItemnaomensuravel_tipo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavItemnaomensuravel_tipo3.addItem("1", "PC", 0);
            cmbavItemnaomensuravel_tipo3.addItem("2", "PF", 0);
            if ( cmbavItemnaomensuravel_tipo3.ItemCount > 0 )
            {
               AV40ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
            }
            GXCCtl = "ITEMNAOMENSURAVEL_TIPO_" + sGXsfl_84_idx;
            cmbItemNaoMensuravel_Tipo.Name = GXCCtl;
            cmbItemNaoMensuravel_Tipo.WebTags = "";
            cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
            cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
            cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
            if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
            {
               A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
            }
            GXCCtl = "ITEMNAOMENSURAVEL_ATIVO_" + sGXsfl_84_idx;
            chkItemNaoMensuravel_Ativo.Name = GXCCtl;
            chkItemNaoMensuravel_Ativo.WebTags = "";
            chkItemNaoMensuravel_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkItemNaoMensuravel_Ativo_Internalname, "TitleCaption", chkItemNaoMensuravel_Ativo.Caption);
            chkItemNaoMensuravel_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV32ItemNaoMensuravel_Codigo1 ,
                                       String AV18ItemNaoMensuravel_Descricao1 ,
                                       short AV34ItemNaoMensuravel_Tipo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV35ItemNaoMensuravel_Codigo2 ,
                                       String AV22ItemNaoMensuravel_Descricao2 ,
                                       short AV37ItemNaoMensuravel_Tipo2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       String AV38ItemNaoMensuravel_Codigo3 ,
                                       String AV26ItemNaoMensuravel_Descricao3 ,
                                       short AV40ItemNaoMensuravel_Tipo3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV48TFItemNaoMensuravel_Codigo ,
                                       String AV49TFItemNaoMensuravel_Codigo_Sel ,
                                       String AV52TFItemNaoMensuravel_Descricao ,
                                       String AV53TFItemNaoMensuravel_Descricao_Sel ,
                                       String AV56TFItemNaoMensuravel_Referencia ,
                                       String AV57TFItemNaoMensuravel_Referencia_Sel ,
                                       decimal AV60TFItemNaoMensuravel_Valor ,
                                       decimal AV61TFItemNaoMensuravel_Valor_To ,
                                       short AV68TFItemNaoMensuravel_Ativo_Sel ,
                                       int AV7ReferenciaINM_Codigo ,
                                       String AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace ,
                                       String AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace ,
                                       String AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace ,
                                       String AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace ,
                                       String AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace ,
                                       String AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace ,
                                       String AV78Pgmname ,
                                       IGxCollection AV65TFItemNaoMensuravel_Tipo_Sels ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                       String A715ItemNaoMensuravel_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFE52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_DESCRICAO", GetSecureSignedToken( sPrefix, A714ItemNaoMensuravel_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_DESCRICAO", A714ItemNaoMensuravel_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_REFERENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_REFERENCIA", StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_VALOR", StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_ATIVO", GetSecureSignedToken( sPrefix, A716ItemNaoMensuravel_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_ATIVO", StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavItemnaomensuravel_tipo1.ItemCount > 0 )
         {
            AV34ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavItemnaomensuravel_tipo2.ItemCount > 0 )
         {
            AV37ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavItemnaomensuravel_tipo3.ItemCount > 0 )
         {
            AV40ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( cmbavItemnaomensuravel_tipo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFE52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV78Pgmname = "ReferenciaINMItemNaoMensuravelWC";
         context.Gx_err = 0;
      }

      protected void RFE52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 84;
         /* Execute user event: E29E52 */
         E29E52 ();
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         nGXsfl_84_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A717ItemNaoMensuravel_Tipo ,
                                                 AV65TFItemNaoMensuravel_Tipo_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV32ItemNaoMensuravel_Codigo1 ,
                                                 AV18ItemNaoMensuravel_Descricao1 ,
                                                 AV34ItemNaoMensuravel_Tipo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV35ItemNaoMensuravel_Codigo2 ,
                                                 AV22ItemNaoMensuravel_Descricao2 ,
                                                 AV37ItemNaoMensuravel_Tipo2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV38ItemNaoMensuravel_Codigo3 ,
                                                 AV26ItemNaoMensuravel_Descricao3 ,
                                                 AV40ItemNaoMensuravel_Tipo3 ,
                                                 AV49TFItemNaoMensuravel_Codigo_Sel ,
                                                 AV48TFItemNaoMensuravel_Codigo ,
                                                 AV53TFItemNaoMensuravel_Descricao_Sel ,
                                                 AV52TFItemNaoMensuravel_Descricao ,
                                                 AV57TFItemNaoMensuravel_Referencia_Sel ,
                                                 AV56TFItemNaoMensuravel_Referencia ,
                                                 AV60TFItemNaoMensuravel_Valor ,
                                                 AV61TFItemNaoMensuravel_Valor_To ,
                                                 AV65TFItemNaoMensuravel_Tipo_Sels.Count ,
                                                 AV68TFItemNaoMensuravel_Ativo_Sel ,
                                                 A715ItemNaoMensuravel_Codigo ,
                                                 A714ItemNaoMensuravel_Descricao ,
                                                 A1804ItemNaoMensuravel_Referencia ,
                                                 A719ItemNaoMensuravel_Valor ,
                                                 A716ItemNaoMensuravel_Ativo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A709ReferenciaINM_Codigo ,
                                                 AV7ReferenciaINM_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV32ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
            lV32ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
            lV18ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
            lV18ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
            lV35ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
            lV35ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
            lV22ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
            lV22ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
            lV38ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
            lV38ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
            lV26ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
            lV26ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
            lV48TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
            lV52TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
            lV56TFItemNaoMensuravel_Referencia = StringUtil.PadR( StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
            /* Using cursor H00E52 */
            pr_default.execute(0, new Object[] {AV7ReferenciaINM_Codigo, lV32ItemNaoMensuravel_Codigo1, lV32ItemNaoMensuravel_Codigo1, lV18ItemNaoMensuravel_Descricao1, lV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, lV35ItemNaoMensuravel_Codigo2, lV35ItemNaoMensuravel_Codigo2, lV22ItemNaoMensuravel_Descricao2, lV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, lV38ItemNaoMensuravel_Codigo3, lV38ItemNaoMensuravel_Codigo3, lV26ItemNaoMensuravel_Descricao3, lV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, lV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, lV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, lV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_84_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A718ItemNaoMensuravel_AreaTrabalhoCod = H00E52_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               A709ReferenciaINM_Codigo = H00E52_A709ReferenciaINM_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               A716ItemNaoMensuravel_Ativo = H00E52_A716ItemNaoMensuravel_Ativo[0];
               A717ItemNaoMensuravel_Tipo = H00E52_A717ItemNaoMensuravel_Tipo[0];
               A719ItemNaoMensuravel_Valor = H00E52_A719ItemNaoMensuravel_Valor[0];
               A1804ItemNaoMensuravel_Referencia = H00E52_A1804ItemNaoMensuravel_Referencia[0];
               n1804ItemNaoMensuravel_Referencia = H00E52_n1804ItemNaoMensuravel_Referencia[0];
               A714ItemNaoMensuravel_Descricao = H00E52_A714ItemNaoMensuravel_Descricao[0];
               A715ItemNaoMensuravel_Codigo = H00E52_A715ItemNaoMensuravel_Codigo[0];
               /* Execute user event: E30E52 */
               E30E52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WBE50( ) ;
         }
         nGXsfl_84_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV65TFItemNaoMensuravel_Tipo_Sels ,
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV32ItemNaoMensuravel_Codigo1 ,
                                              AV18ItemNaoMensuravel_Descricao1 ,
                                              AV34ItemNaoMensuravel_Tipo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV35ItemNaoMensuravel_Codigo2 ,
                                              AV22ItemNaoMensuravel_Descricao2 ,
                                              AV37ItemNaoMensuravel_Tipo2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV38ItemNaoMensuravel_Codigo3 ,
                                              AV26ItemNaoMensuravel_Descricao3 ,
                                              AV40ItemNaoMensuravel_Tipo3 ,
                                              AV49TFItemNaoMensuravel_Codigo_Sel ,
                                              AV48TFItemNaoMensuravel_Codigo ,
                                              AV53TFItemNaoMensuravel_Descricao_Sel ,
                                              AV52TFItemNaoMensuravel_Descricao ,
                                              AV57TFItemNaoMensuravel_Referencia_Sel ,
                                              AV56TFItemNaoMensuravel_Referencia ,
                                              AV60TFItemNaoMensuravel_Valor ,
                                              AV61TFItemNaoMensuravel_Valor_To ,
                                              AV65TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV68TFItemNaoMensuravel_Ativo_Sel ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A1804ItemNaoMensuravel_Referencia ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A709ReferenciaINM_Codigo ,
                                              AV7ReferenciaINM_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
         lV32ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
         lV18ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
         lV18ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
         lV35ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
         lV35ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
         lV22ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
         lV22ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
         lV38ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
         lV38ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
         lV26ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
         lV26ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
         lV48TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
         lV52TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
         lV56TFItemNaoMensuravel_Referencia = StringUtil.PadR( StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
         /* Using cursor H00E53 */
         pr_default.execute(1, new Object[] {AV7ReferenciaINM_Codigo, lV32ItemNaoMensuravel_Codigo1, lV32ItemNaoMensuravel_Codigo1, lV18ItemNaoMensuravel_Descricao1, lV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, lV35ItemNaoMensuravel_Codigo2, lV35ItemNaoMensuravel_Codigo2, lV22ItemNaoMensuravel_Descricao2, lV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, lV38ItemNaoMensuravel_Codigo3, lV38ItemNaoMensuravel_Codigo3, lV26ItemNaoMensuravel_Descricao3, lV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, lV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, lV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, lV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To});
         GRID_nRecordCount = H00E53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPE50( )
      {
         /* Before Start, stand alone formulas. */
         AV78Pgmname = "ReferenciaINMItemNaoMensuravelWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28E52 */
         E28E52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV70DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA"), AV47ItemNaoMensuravel_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA"), AV51ItemNaoMensuravel_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_REFERENCIATITLEFILTERDATA"), AV55ItemNaoMensuravel_ReferenciaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA"), AV59ItemNaoMensuravel_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA"), AV63ItemNaoMensuravel_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA"), AV67ItemNaoMensuravel_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV32ItemNaoMensuravel_Codigo1 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_codigo1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
            AV18ItemNaoMensuravel_Descricao1 = cgiGet( edtavItemnaomensuravel_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
            cmbavItemnaomensuravel_tipo1.Name = cmbavItemnaomensuravel_tipo1_Internalname;
            cmbavItemnaomensuravel_tipo1.CurrentValue = cgiGet( cmbavItemnaomensuravel_tipo1_Internalname);
            AV34ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( cgiGet( cmbavItemnaomensuravel_tipo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV35ItemNaoMensuravel_Codigo2 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_codigo2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
            AV22ItemNaoMensuravel_Descricao2 = cgiGet( edtavItemnaomensuravel_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
            cmbavItemnaomensuravel_tipo2.Name = cmbavItemnaomensuravel_tipo2_Internalname;
            cmbavItemnaomensuravel_tipo2.CurrentValue = cgiGet( cmbavItemnaomensuravel_tipo2_Internalname);
            AV37ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( cgiGet( cmbavItemnaomensuravel_tipo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            AV38ItemNaoMensuravel_Codigo3 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_codigo3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
            AV26ItemNaoMensuravel_Descricao3 = cgiGet( edtavItemnaomensuravel_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
            cmbavItemnaomensuravel_tipo3.Name = cmbavItemnaomensuravel_tipo3_Internalname;
            cmbavItemnaomensuravel_tipo3.CurrentValue = cgiGet( cmbavItemnaomensuravel_tipo3_Internalname);
            AV40ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( cgiGet( cmbavItemnaomensuravel_tipo3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
            A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            else
            {
               AV14OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV48TFItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtavTfitemnaomensuravel_codigo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
            AV49TFItemNaoMensuravel_Codigo_Sel = StringUtil.Upper( cgiGet( edtavTfitemnaomensuravel_codigo_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFItemNaoMensuravel_Codigo_Sel", AV49TFItemNaoMensuravel_Codigo_Sel);
            AV52TFItemNaoMensuravel_Descricao = cgiGet( edtavTfitemnaomensuravel_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
            AV53TFItemNaoMensuravel_Descricao_Sel = cgiGet( edtavTfitemnaomensuravel_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFItemNaoMensuravel_Descricao_Sel", AV53TFItemNaoMensuravel_Descricao_Sel);
            AV56TFItemNaoMensuravel_Referencia = cgiGet( edtavTfitemnaomensuravel_referencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
            AV57TFItemNaoMensuravel_Referencia_Sel = cgiGet( edtavTfitemnaomensuravel_referencia_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFItemNaoMensuravel_Referencia_Sel", AV57TFItemNaoMensuravel_Referencia_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_VALOR");
               GX_FocusControl = edtavTfitemnaomensuravel_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFItemNaoMensuravel_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
            }
            else
            {
               AV60TFItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_VALOR_TO");
               GX_FocusControl = edtavTfitemnaomensuravel_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFItemNaoMensuravel_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
            }
            else
            {
               AV61TFItemNaoMensuravel_Valor_To = context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_ATIVO_SEL");
               GX_FocusControl = edtavTfitemnaomensuravel_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFItemNaoMensuravel_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            }
            else
            {
               AV68TFItemNaoMensuravel_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            }
            AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
            AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
            AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace", AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace);
            AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
            AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
            AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_84"), ",", "."));
            AV72GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV73GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ReferenciaINM_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_itemnaomensuravel_codigo_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Caption");
            Ddo_itemnaomensuravel_codigo_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Tooltip");
            Ddo_itemnaomensuravel_codigo_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Cls");
            Ddo_itemnaomensuravel_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_set");
            Ddo_itemnaomensuravel_codigo_Selectedvalue_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_set");
            Ddo_itemnaomensuravel_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortasc"));
            Ddo_itemnaomensuravel_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortdsc"));
            Ddo_itemnaomensuravel_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortedstatus");
            Ddo_itemnaomensuravel_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includefilter"));
            Ddo_itemnaomensuravel_codigo_Filtertype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filtertype");
            Ddo_itemnaomensuravel_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filterisrange"));
            Ddo_itemnaomensuravel_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Includedatalist"));
            Ddo_itemnaomensuravel_codigo_Datalisttype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalisttype");
            Ddo_itemnaomensuravel_codigo_Datalistproc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistproc");
            Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_itemnaomensuravel_codigo_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortasc");
            Ddo_itemnaomensuravel_codigo_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Sortdsc");
            Ddo_itemnaomensuravel_codigo_Loadingdata = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Loadingdata");
            Ddo_itemnaomensuravel_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Cleanfilter");
            Ddo_itemnaomensuravel_codigo_Noresultsfound = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Noresultsfound");
            Ddo_itemnaomensuravel_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Searchbuttontext");
            Ddo_itemnaomensuravel_descricao_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Caption");
            Ddo_itemnaomensuravel_descricao_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Tooltip");
            Ddo_itemnaomensuravel_descricao_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cls");
            Ddo_itemnaomensuravel_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_set");
            Ddo_itemnaomensuravel_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_set");
            Ddo_itemnaomensuravel_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortasc"));
            Ddo_itemnaomensuravel_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortdsc"));
            Ddo_itemnaomensuravel_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortedstatus");
            Ddo_itemnaomensuravel_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includefilter"));
            Ddo_itemnaomensuravel_descricao_Filtertype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filtertype");
            Ddo_itemnaomensuravel_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filterisrange"));
            Ddo_itemnaomensuravel_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includedatalist"));
            Ddo_itemnaomensuravel_descricao_Datalisttype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalisttype");
            Ddo_itemnaomensuravel_descricao_Datalistproc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistproc");
            Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_itemnaomensuravel_descricao_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortasc");
            Ddo_itemnaomensuravel_descricao_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortdsc");
            Ddo_itemnaomensuravel_descricao_Loadingdata = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Loadingdata");
            Ddo_itemnaomensuravel_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cleanfilter");
            Ddo_itemnaomensuravel_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Noresultsfound");
            Ddo_itemnaomensuravel_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Searchbuttontext");
            Ddo_itemnaomensuravel_referencia_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Caption");
            Ddo_itemnaomensuravel_referencia_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Tooltip");
            Ddo_itemnaomensuravel_referencia_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Cls");
            Ddo_itemnaomensuravel_referencia_Filteredtext_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filteredtext_set");
            Ddo_itemnaomensuravel_referencia_Selectedvalue_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Selectedvalue_set");
            Ddo_itemnaomensuravel_referencia_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Dropdownoptionstype");
            Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_referencia_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includesortasc"));
            Ddo_itemnaomensuravel_referencia_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includesortdsc"));
            Ddo_itemnaomensuravel_referencia_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortedstatus");
            Ddo_itemnaomensuravel_referencia_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includefilter"));
            Ddo_itemnaomensuravel_referencia_Filtertype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filtertype");
            Ddo_itemnaomensuravel_referencia_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filterisrange"));
            Ddo_itemnaomensuravel_referencia_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Includedatalist"));
            Ddo_itemnaomensuravel_referencia_Datalisttype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalisttype");
            Ddo_itemnaomensuravel_referencia_Datalistproc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalistproc");
            Ddo_itemnaomensuravel_referencia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_itemnaomensuravel_referencia_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortasc");
            Ddo_itemnaomensuravel_referencia_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Sortdsc");
            Ddo_itemnaomensuravel_referencia_Loadingdata = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Loadingdata");
            Ddo_itemnaomensuravel_referencia_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Cleanfilter");
            Ddo_itemnaomensuravel_referencia_Noresultsfound = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Noresultsfound");
            Ddo_itemnaomensuravel_referencia_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Searchbuttontext");
            Ddo_itemnaomensuravel_valor_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Caption");
            Ddo_itemnaomensuravel_valor_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Tooltip");
            Ddo_itemnaomensuravel_valor_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Cls");
            Ddo_itemnaomensuravel_valor_Filteredtext_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_set");
            Ddo_itemnaomensuravel_valor_Filteredtextto_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_set");
            Ddo_itemnaomensuravel_valor_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Dropdownoptionstype");
            Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_valor_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includesortasc"));
            Ddo_itemnaomensuravel_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includesortdsc"));
            Ddo_itemnaomensuravel_valor_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortedstatus");
            Ddo_itemnaomensuravel_valor_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includefilter"));
            Ddo_itemnaomensuravel_valor_Filtertype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filtertype");
            Ddo_itemnaomensuravel_valor_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filterisrange"));
            Ddo_itemnaomensuravel_valor_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Includedatalist"));
            Ddo_itemnaomensuravel_valor_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortasc");
            Ddo_itemnaomensuravel_valor_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Sortdsc");
            Ddo_itemnaomensuravel_valor_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Cleanfilter");
            Ddo_itemnaomensuravel_valor_Rangefilterfrom = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterfrom");
            Ddo_itemnaomensuravel_valor_Rangefilterto = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterto");
            Ddo_itemnaomensuravel_valor_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Searchbuttontext");
            Ddo_itemnaomensuravel_tipo_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Caption");
            Ddo_itemnaomensuravel_tipo_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Tooltip");
            Ddo_itemnaomensuravel_tipo_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Cls");
            Ddo_itemnaomensuravel_tipo_Selectedvalue_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_set");
            Ddo_itemnaomensuravel_tipo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includesortasc"));
            Ddo_itemnaomensuravel_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includesortdsc"));
            Ddo_itemnaomensuravel_tipo_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortedstatus");
            Ddo_itemnaomensuravel_tipo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includefilter"));
            Ddo_itemnaomensuravel_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Includedatalist"));
            Ddo_itemnaomensuravel_tipo_Datalisttype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Datalisttype");
            Ddo_itemnaomensuravel_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Allowmultipleselection"));
            Ddo_itemnaomensuravel_tipo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Datalistfixedvalues");
            Ddo_itemnaomensuravel_tipo_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortasc");
            Ddo_itemnaomensuravel_tipo_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Sortdsc");
            Ddo_itemnaomensuravel_tipo_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Cleanfilter");
            Ddo_itemnaomensuravel_tipo_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Searchbuttontext");
            Ddo_itemnaomensuravel_ativo_Caption = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Caption");
            Ddo_itemnaomensuravel_ativo_Tooltip = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Tooltip");
            Ddo_itemnaomensuravel_ativo_Cls = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Cls");
            Ddo_itemnaomensuravel_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_set");
            Ddo_itemnaomensuravel_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortasc"));
            Ddo_itemnaomensuravel_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortdsc"));
            Ddo_itemnaomensuravel_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortedstatus");
            Ddo_itemnaomensuravel_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includefilter"));
            Ddo_itemnaomensuravel_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Includedatalist"));
            Ddo_itemnaomensuravel_ativo_Datalisttype = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Datalisttype");
            Ddo_itemnaomensuravel_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Datalistfixedvalues");
            Ddo_itemnaomensuravel_ativo_Sortasc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortasc");
            Ddo_itemnaomensuravel_ativo_Sortdsc = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Sortdsc");
            Ddo_itemnaomensuravel_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Cleanfilter");
            Ddo_itemnaomensuravel_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_itemnaomensuravel_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Activeeventkey");
            Ddo_itemnaomensuravel_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_get");
            Ddo_itemnaomensuravel_codigo_Selectedvalue_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_get");
            Ddo_itemnaomensuravel_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Activeeventkey");
            Ddo_itemnaomensuravel_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_get");
            Ddo_itemnaomensuravel_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_get");
            Ddo_itemnaomensuravel_referencia_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Activeeventkey");
            Ddo_itemnaomensuravel_referencia_Filteredtext_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Filteredtext_get");
            Ddo_itemnaomensuravel_referencia_Selectedvalue_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA_Selectedvalue_get");
            Ddo_itemnaomensuravel_valor_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Activeeventkey");
            Ddo_itemnaomensuravel_valor_Filteredtext_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_get");
            Ddo_itemnaomensuravel_valor_Filteredtextto_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_get");
            Ddo_itemnaomensuravel_tipo_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Activeeventkey");
            Ddo_itemnaomensuravel_tipo_Selectedvalue_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_get");
            Ddo_itemnaomensuravel_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Activeeventkey");
            Ddo_itemnaomensuravel_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO1"), AV32ItemNaoMensuravel_Codigo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO1"), AV18ItemNaoMensuravel_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO1"), ",", ".") != Convert.ToDecimal( AV34ItemNaoMensuravel_Tipo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO2"), AV35ItemNaoMensuravel_Codigo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO2"), AV22ItemNaoMensuravel_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO2"), ",", ".") != Convert.ToDecimal( AV37ItemNaoMensuravel_Tipo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_CODIGO3"), AV38ItemNaoMensuravel_Codigo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_DESCRICAO3"), AV26ItemNaoMensuravel_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vITEMNAOMENSURAVEL_TIPO3"), ",", ".") != Convert.ToDecimal( AV40ItemNaoMensuravel_Tipo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO"), AV48TFItemNaoMensuravel_Codigo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL"), AV49TFItemNaoMensuravel_Codigo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO"), AV52TFItemNaoMensuravel_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL"), AV53TFItemNaoMensuravel_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA"), AV56TFItemNaoMensuravel_Referencia) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_REFERENCIA_SEL"), AV57TFItemNaoMensuravel_Referencia_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR"), ",", ".") != AV60TFItemNaoMensuravel_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_VALOR_TO"), ",", ".") != AV61TFItemNaoMensuravel_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV68TFItemNaoMensuravel_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28E52 */
         E28E52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28E52( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV34ItemNaoMensuravel_Tipo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
         AV16DynamicFiltersSelector1 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37ItemNaoMensuravel_Tipo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
         AV20DynamicFiltersSelector2 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV40ItemNaoMensuravel_Tipo3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
         AV24DynamicFiltersSelector3 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfitemnaomensuravel_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_codigo_Visible), 5, 0)));
         edtavTfitemnaomensuravel_codigo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_codigo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_codigo_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_descricao_Visible), 5, 0)));
         edtavTfitemnaomensuravel_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_descricao_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_referencia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_referencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_referencia_Visible), 5, 0)));
         edtavTfitemnaomensuravel_referencia_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_referencia_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_referencia_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_valor_Visible), 5, 0)));
         edtavTfitemnaomensuravel_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_valor_to_Visible), 5, 0)));
         edtavTfitemnaomensuravel_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfitemnaomensuravel_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_ativo_sel_Visible), 5, 0)));
         Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace);
         AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace);
         AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Referencia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace);
         AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace = Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace", AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace);
         AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace);
         AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace);
         AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtReferenciaINM_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtReferenciaINM_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReferenciaINM_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV70DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV70DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E29E52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV47ItemNaoMensuravel_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ItemNaoMensuravel_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ItemNaoMensuravel_ReferenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ItemNaoMensuravel_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ItemNaoMensuravel_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ItemNaoMensuravel_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV23DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtItemNaoMensuravel_Codigo_Titleformat = 2;
         edtItemNaoMensuravel_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtItemNaoMensuravel_Codigo_Internalname, "Title", edtItemNaoMensuravel_Codigo_Title);
         edtItemNaoMensuravel_Descricao_Titleformat = 2;
         edtItemNaoMensuravel_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtItemNaoMensuravel_Descricao_Internalname, "Title", edtItemNaoMensuravel_Descricao_Title);
         edtItemNaoMensuravel_Referencia_Titleformat = 2;
         edtItemNaoMensuravel_Referencia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Refer�ncia", AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtItemNaoMensuravel_Referencia_Internalname, "Title", edtItemNaoMensuravel_Referencia_Title);
         edtItemNaoMensuravel_Valor_Titleformat = 2;
         edtItemNaoMensuravel_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtItemNaoMensuravel_Valor_Internalname, "Title", edtItemNaoMensuravel_Valor_Title);
         cmbItemNaoMensuravel_Tipo_Titleformat = 2;
         cmbItemNaoMensuravel_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbItemNaoMensuravel_Tipo_Internalname, "Title", cmbItemNaoMensuravel_Tipo.Title.Text);
         chkItemNaoMensuravel_Ativo_Titleformat = 2;
         chkItemNaoMensuravel_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkItemNaoMensuravel_Ativo_Internalname, "Title", chkItemNaoMensuravel_Ativo.Title.Text);
         AV72GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72GridCurrentPage), 10, 0)));
         AV73GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV73GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47ItemNaoMensuravel_CodigoTitleFilterData", AV47ItemNaoMensuravel_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV51ItemNaoMensuravel_DescricaoTitleFilterData", AV51ItemNaoMensuravel_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55ItemNaoMensuravel_ReferenciaTitleFilterData", AV55ItemNaoMensuravel_ReferenciaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV59ItemNaoMensuravel_ValorTitleFilterData", AV59ItemNaoMensuravel_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV63ItemNaoMensuravel_TipoTitleFilterData", AV63ItemNaoMensuravel_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV67ItemNaoMensuravel_AtivoTitleFilterData", AV67ItemNaoMensuravel_AtivoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11E52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV71PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV71PageToGo) ;
         }
      }

      protected void E12E52( )
      {
         /* Ddo_itemnaomensuravel_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFItemNaoMensuravel_Codigo = Ddo_itemnaomensuravel_codigo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
            AV49TFItemNaoMensuravel_Codigo_Sel = Ddo_itemnaomensuravel_codigo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFItemNaoMensuravel_Codigo_Sel", AV49TFItemNaoMensuravel_Codigo_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13E52( )
      {
         /* Ddo_itemnaomensuravel_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFItemNaoMensuravel_Descricao = Ddo_itemnaomensuravel_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
            AV53TFItemNaoMensuravel_Descricao_Sel = Ddo_itemnaomensuravel_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFItemNaoMensuravel_Descricao_Sel", AV53TFItemNaoMensuravel_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14E52( )
      {
         /* Ddo_itemnaomensuravel_referencia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_referencia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_referencia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SortedStatus", Ddo_itemnaomensuravel_referencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_referencia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_referencia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SortedStatus", Ddo_itemnaomensuravel_referencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_referencia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFItemNaoMensuravel_Referencia = Ddo_itemnaomensuravel_referencia_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
            AV57TFItemNaoMensuravel_Referencia_Sel = Ddo_itemnaomensuravel_referencia_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFItemNaoMensuravel_Referencia_Sel", AV57TFItemNaoMensuravel_Referencia_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15E52( )
      {
         /* Ddo_itemnaomensuravel_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFItemNaoMensuravel_Valor = NumberUtil.Val( Ddo_itemnaomensuravel_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
            AV61TFItemNaoMensuravel_Valor_To = NumberUtil.Val( Ddo_itemnaomensuravel_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16E52( )
      {
         /* Ddo_itemnaomensuravel_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFItemNaoMensuravel_Tipo_SelsJson = Ddo_itemnaomensuravel_tipo_Selectedvalue_get;
            AV65TFItemNaoMensuravel_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV64TFItemNaoMensuravel_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65TFItemNaoMensuravel_Tipo_Sels", AV65TFItemNaoMensuravel_Tipo_Sels);
      }

      protected void E17E52( )
      {
         /* Ddo_itemnaomensuravel_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_itemnaomensuravel_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( Ddo_itemnaomensuravel_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E30E52( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV29Update);
         AV76Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         AV30Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV30Delete);
         AV77Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 84;
         }
         sendrow_842( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_84_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(84, GridRow);
         }
      }

      protected void E23E52( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
      }

      protected void E18E52( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Values", cmbavItemnaomensuravel_tipo1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Values", cmbavItemnaomensuravel_tipo2.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Values", cmbavItemnaomensuravel_tipo3.ToJavascriptSource());
      }

      protected void E24E52( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25E52( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
      }

      protected void E19E52( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Values", cmbavItemnaomensuravel_tipo1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Values", cmbavItemnaomensuravel_tipo2.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Values", cmbavItemnaomensuravel_tipo3.ToJavascriptSource());
      }

      protected void E26E52( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E20E52( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV32ItemNaoMensuravel_Codigo1, AV18ItemNaoMensuravel_Descricao1, AV34ItemNaoMensuravel_Tipo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV35ItemNaoMensuravel_Codigo2, AV22ItemNaoMensuravel_Descricao2, AV37ItemNaoMensuravel_Tipo2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV38ItemNaoMensuravel_Codigo3, AV26ItemNaoMensuravel_Descricao3, AV40ItemNaoMensuravel_Tipo3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV14OrderedBy, AV15OrderedDsc, AV48TFItemNaoMensuravel_Codigo, AV49TFItemNaoMensuravel_Codigo_Sel, AV52TFItemNaoMensuravel_Descricao, AV53TFItemNaoMensuravel_Descricao_Sel, AV56TFItemNaoMensuravel_Referencia, AV57TFItemNaoMensuravel_Referencia_Sel, AV60TFItemNaoMensuravel_Valor, AV61TFItemNaoMensuravel_Valor_To, AV68TFItemNaoMensuravel_Ativo_Sel, AV7ReferenciaINM_Codigo, AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace, AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV78Pgmname, AV65TFItemNaoMensuravel_Tipo_Sels, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Values", cmbavItemnaomensuravel_tipo1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Values", cmbavItemnaomensuravel_tipo2.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Values", cmbavItemnaomensuravel_tipo3.ToJavascriptSource());
      }

      protected void E27E52( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E21E52( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65TFItemNaoMensuravel_Tipo_Sels", AV65TFItemNaoMensuravel_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Values", cmbavItemnaomensuravel_tipo1.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Values", cmbavItemnaomensuravel_tipo2.ToJavascriptSource());
         cmbavItemnaomensuravel_tipo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Values", cmbavItemnaomensuravel_tipo3.ToJavascriptSource());
      }

      protected void E22E52( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_itemnaomensuravel_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
         Ddo_itemnaomensuravel_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
         Ddo_itemnaomensuravel_referencia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SortedStatus", Ddo_itemnaomensuravel_referencia_Sortedstatus);
         Ddo_itemnaomensuravel_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
         Ddo_itemnaomensuravel_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
         Ddo_itemnaomensuravel_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_itemnaomensuravel_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_itemnaomensuravel_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_itemnaomensuravel_referencia_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SortedStatus", Ddo_itemnaomensuravel_referencia_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_itemnaomensuravel_valor_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_itemnaomensuravel_tipo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 6 )
         {
            Ddo_itemnaomensuravel_ativo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavItemnaomensuravel_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo1_Visible), 5, 0)));
         edtavItemnaomensuravel_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao1_Visible), 5, 0)));
         cmbavItemnaomensuravel_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            edtavItemnaomensuravel_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            edtavItemnaomensuravel_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 )
         {
            cmbavItemnaomensuravel_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavItemnaomensuravel_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo2_Visible), 5, 0)));
         edtavItemnaomensuravel_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao2_Visible), 5, 0)));
         cmbavItemnaomensuravel_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            edtavItemnaomensuravel_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            edtavItemnaomensuravel_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 )
         {
            cmbavItemnaomensuravel_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavItemnaomensuravel_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo3_Visible), 5, 0)));
         edtavItemnaomensuravel_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao3_Visible), 5, 0)));
         cmbavItemnaomensuravel_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            edtavItemnaomensuravel_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_codigo3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            edtavItemnaomensuravel_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemnaomensuravel_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 )
         {
            cmbavItemnaomensuravel_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavItemnaomensuravel_tipo3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV35ItemNaoMensuravel_Codigo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV38ItemNaoMensuravel_Codigo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV48TFItemNaoMensuravel_Codigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
         Ddo_itemnaomensuravel_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_codigo_Filteredtext_set);
         AV49TFItemNaoMensuravel_Codigo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFItemNaoMensuravel_Codigo_Sel", AV49TFItemNaoMensuravel_Codigo_Sel);
         Ddo_itemnaomensuravel_codigo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_codigo_Selectedvalue_set);
         AV52TFItemNaoMensuravel_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
         Ddo_itemnaomensuravel_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_descricao_Filteredtext_set);
         AV53TFItemNaoMensuravel_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFItemNaoMensuravel_Descricao_Sel", AV53TFItemNaoMensuravel_Descricao_Sel);
         Ddo_itemnaomensuravel_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_descricao_Selectedvalue_set);
         AV56TFItemNaoMensuravel_Referencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
         Ddo_itemnaomensuravel_referencia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_referencia_Filteredtext_set);
         AV57TFItemNaoMensuravel_Referencia_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFItemNaoMensuravel_Referencia_Sel", AV57TFItemNaoMensuravel_Referencia_Sel);
         Ddo_itemnaomensuravel_referencia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_referencia_Selectedvalue_set);
         AV60TFItemNaoMensuravel_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
         Ddo_itemnaomensuravel_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_valor_Filteredtext_set);
         AV61TFItemNaoMensuravel_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
         Ddo_itemnaomensuravel_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredTextTo_set", Ddo_itemnaomensuravel_valor_Filteredtextto_set);
         AV65TFItemNaoMensuravel_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_itemnaomensuravel_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_tipo_Selectedvalue_set);
         AV68TFItemNaoMensuravel_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
         Ddo_itemnaomensuravel_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "ITEMNAOMENSURAVEL_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV32ItemNaoMensuravel_Codigo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV78Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV78Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV78Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV79GXV1 = 1;
         while ( AV79GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV79GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV48TFItemNaoMensuravel_Codigo = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFItemNaoMensuravel_Codigo", AV48TFItemNaoMensuravel_Codigo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo)) )
               {
                  Ddo_itemnaomensuravel_codigo_Filteredtext_set = AV48TFItemNaoMensuravel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_codigo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO_SEL") == 0 )
            {
               AV49TFItemNaoMensuravel_Codigo_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFItemNaoMensuravel_Codigo_Sel", AV49TFItemNaoMensuravel_Codigo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) )
               {
                  Ddo_itemnaomensuravel_codigo_Selectedvalue_set = AV49TFItemNaoMensuravel_Codigo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_codigo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_codigo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV52TFItemNaoMensuravel_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFItemNaoMensuravel_Descricao", AV52TFItemNaoMensuravel_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao)) )
               {
                  Ddo_itemnaomensuravel_descricao_Filteredtext_set = AV52TFItemNaoMensuravel_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO_SEL") == 0 )
            {
               AV53TFItemNaoMensuravel_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFItemNaoMensuravel_Descricao_Sel", AV53TFItemNaoMensuravel_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) )
               {
                  Ddo_itemnaomensuravel_descricao_Selectedvalue_set = AV53TFItemNaoMensuravel_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_descricao_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_REFERENCIA") == 0 )
            {
               AV56TFItemNaoMensuravel_Referencia = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56TFItemNaoMensuravel_Referencia", AV56TFItemNaoMensuravel_Referencia);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia)) )
               {
                  Ddo_itemnaomensuravel_referencia_Filteredtext_set = AV56TFItemNaoMensuravel_Referencia;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_referencia_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_REFERENCIA_SEL") == 0 )
            {
               AV57TFItemNaoMensuravel_Referencia_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFItemNaoMensuravel_Referencia_Sel", AV57TFItemNaoMensuravel_Referencia_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) )
               {
                  Ddo_itemnaomensuravel_referencia_Selectedvalue_set = AV57TFItemNaoMensuravel_Referencia_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_referencia_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_referencia_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_VALOR") == 0 )
            {
               AV60TFItemNaoMensuravel_Valor = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5)));
               AV61TFItemNaoMensuravel_Valor_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV60TFItemNaoMensuravel_Valor) )
               {
                  Ddo_itemnaomensuravel_valor_Filteredtext_set = StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_valor_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV61TFItemNaoMensuravel_Valor_To) )
               {
                  Ddo_itemnaomensuravel_valor_Filteredtextto_set = StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredTextTo_set", Ddo_itemnaomensuravel_valor_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_TIPO_SEL") == 0 )
            {
               AV64TFItemNaoMensuravel_Tipo_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV65TFItemNaoMensuravel_Tipo_Sels.FromJSonString(AV64TFItemNaoMensuravel_Tipo_SelsJson);
               if ( ! ( AV65TFItemNaoMensuravel_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_itemnaomensuravel_tipo_Selectedvalue_set = AV64TFItemNaoMensuravel_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_tipo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_ATIVO_SEL") == 0 )
            {
               AV68TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0));
               if ( ! (0==AV68TFItemNaoMensuravel_Ativo_Sel) )
               {
                  Ddo_itemnaomensuravel_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_itemnaomensuravel_ativo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_ativo_Selectedvalue_set);
               }
            }
            AV79GXV1 = (int)(AV79GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV32ItemNaoMensuravel_Codigo1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ItemNaoMensuravel_Codigo1", AV32ItemNaoMensuravel_Codigo1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ItemNaoMensuravel_Descricao1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ItemNaoMensuravel_Descricao1", AV18ItemNaoMensuravel_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 )
            {
               AV34ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34ItemNaoMensuravel_Tipo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV35ItemNaoMensuravel_Codigo2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ItemNaoMensuravel_Codigo2", AV35ItemNaoMensuravel_Codigo2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ItemNaoMensuravel_Descricao2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ItemNaoMensuravel_Descricao2", AV22ItemNaoMensuravel_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 )
               {
                  AV37ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ItemNaoMensuravel_Tipo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV38ItemNaoMensuravel_Codigo3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ItemNaoMensuravel_Codigo3", AV38ItemNaoMensuravel_Codigo3);
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26ItemNaoMensuravel_Descricao3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ItemNaoMensuravel_Descricao3", AV26ItemNaoMensuravel_Descricao3);
                  }
                  else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 )
                  {
                     AV40ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40ItemNaoMensuravel_Tipo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV78Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = AV48TFItemNaoMensuravel_Codigo;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_CODIGO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV49TFItemNaoMensuravel_Codigo_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV52TFItemNaoMensuravel_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV53TFItemNaoMensuravel_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_REFERENCIA";
            AV12GridStateFilterValue.gxTpr_Value = AV56TFItemNaoMensuravel_Referencia;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_REFERENCIA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV57TFItemNaoMensuravel_Referencia_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV60TFItemNaoMensuravel_Valor) && (Convert.ToDecimal(0)==AV61TFItemNaoMensuravel_Valor_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_VALOR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV60TFItemNaoMensuravel_Valor, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV61TFItemNaoMensuravel_Valor_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV65TFItemNaoMensuravel_Tipo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_TIPO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV65TFItemNaoMensuravel_Tipo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV68TFItemNaoMensuravel_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV68TFItemNaoMensuravel_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ReferenciaINM_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&REFERENCIAINM_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV78Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32ItemNaoMensuravel_Codigo1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18ItemNaoMensuravel_Descricao1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ! (0==AV34ItemNaoMensuravel_Tipo1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV35ItemNaoMensuravel_Codigo2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV22ItemNaoMensuravel_Descricao2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ! (0==AV37ItemNaoMensuravel_Tipo2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV38ItemNaoMensuravel_Codigo3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26ItemNaoMensuravel_Descricao3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ! (0==AV40ItemNaoMensuravel_Tipo3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0);
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV78Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ItemNaoMensuravel";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ReferenciaINM_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_8_E52( true) ;
         }
         else
         {
            wb_table2_8_E52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_E52( true) ;
         }
         else
         {
            wb_table3_81_E52( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_E52e( true) ;
         }
         else
         {
            wb_table1_2_E52e( false) ;
         }
      }

      protected void wb_table3_81_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Referencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Referencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Referencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbItemNaoMensuravel_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbItemNaoMensuravel_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbItemNaoMensuravel_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkItemNaoMensuravel_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkItemNaoMensuravel_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkItemNaoMensuravel_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A714ItemNaoMensuravel_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Referencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Referencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbItemNaoMensuravel_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbItemNaoMensuravel_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkItemNaoMensuravel_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkItemNaoMensuravel_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_E52e( true) ;
         }
         else
         {
            wb_table3_81_E52e( false) ;
         }
      }

      protected void wb_table2_8_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_E52( true) ;
         }
         else
         {
            wb_table4_11_E52( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_16_E52( true) ;
         }
         else
         {
            wb_table5_16_E52( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_E52e( true) ;
         }
         else
         {
            wb_table2_8_E52e( false) ;
         }
      }

      protected void wb_table5_16_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_21_E52( true) ;
         }
         else
         {
            wb_table6_21_E52( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_E52e( true) ;
         }
         else
         {
            wb_table5_16_E52e( false) ;
         }
      }

      protected void wb_table6_21_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_30_E52( true) ;
         }
         else
         {
            wb_table7_30_E52( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_49_E52( true) ;
         }
         else
         {
            wb_table8_49_E52( false) ;
         }
         return  ;
      }

      protected void wb_table8_49_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_68_E52( true) ;
         }
         else
         {
            wb_table9_68_E52( false) ;
         }
         return  ;
      }

      protected void wb_table9_68_E52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_E52e( true) ;
         }
         else
         {
            wb_table6_21_E52e( false) ;
         }
      }

      protected void wb_table9_68_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_codigo3_Internalname, StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3), StringUtil.RTrim( context.localUtil.Format( AV38ItemNaoMensuravel_Codigo3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_codigo3_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavItemnaomensuravel_descricao3_Internalname, AV26ItemNaoMensuravel_Descricao3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavItemnaomensuravel_descricao3_Visible, 1, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavItemnaomensuravel_tipo3, cmbavItemnaomensuravel_tipo3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0)), 1, cmbavItemnaomensuravel_tipo3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavItemnaomensuravel_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavItemnaomensuravel_tipo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40ItemNaoMensuravel_Tipo3), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo3_Internalname, "Values", (String)(cmbavItemnaomensuravel_tipo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_68_E52e( true) ;
         }
         else
         {
            wb_table9_68_E52e( false) ;
         }
      }

      protected void wb_table8_49_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_codigo2_Internalname, StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2), StringUtil.RTrim( context.localUtil.Format( AV35ItemNaoMensuravel_Codigo2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_codigo2_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavItemnaomensuravel_descricao2_Internalname, AV22ItemNaoMensuravel_Descricao2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavItemnaomensuravel_descricao2_Visible, 1, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavItemnaomensuravel_tipo2, cmbavItemnaomensuravel_tipo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0)), 1, cmbavItemnaomensuravel_tipo2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavItemnaomensuravel_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavItemnaomensuravel_tipo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ItemNaoMensuravel_Tipo2), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo2_Internalname, "Values", (String)(cmbavItemnaomensuravel_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_49_E52e( true) ;
         }
         else
         {
            wb_table8_49_E52e( false) ;
         }
      }

      protected void wb_table7_30_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_codigo1_Internalname, StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1), StringUtil.RTrim( context.localUtil.Format( AV32ItemNaoMensuravel_Codigo1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_codigo1_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavItemnaomensuravel_descricao1_Internalname, AV18ItemNaoMensuravel_Descricao1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavItemnaomensuravel_descricao1_Visible, 1, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavItemnaomensuravel_tipo1, cmbavItemnaomensuravel_tipo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0)), 1, cmbavItemnaomensuravel_tipo1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavItemnaomensuravel_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            cmbavItemnaomensuravel_tipo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34ItemNaoMensuravel_Tipo1), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavItemnaomensuravel_tipo1_Internalname, "Values", (String)(cmbavItemnaomensuravel_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_E52e( true) ;
         }
         else
         {
            wb_table7_30_E52e( false) ;
         }
      }

      protected void wb_table4_11_E52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReferenciaINMItemNaoMensuravelWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_E52e( true) ;
         }
         else
         {
            wb_table4_11_E52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAE52( ) ;
         WSE52( ) ;
         WEE52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ReferenciaINM_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAE52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "referenciainmitemnaomensuravelwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAE52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
         }
         wcpOAV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ReferenciaINM_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ReferenciaINM_Codigo != wcpOAV7ReferenciaINM_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ReferenciaINM_Codigo = AV7ReferenciaINM_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ReferenciaINM_Codigo = cgiGet( sPrefix+"AV7ReferenciaINM_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ReferenciaINM_Codigo) > 0 )
         {
            AV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ReferenciaINM_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ReferenciaINM_Codigo), 6, 0)));
         }
         else
         {
            AV7ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ReferenciaINM_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAE52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSE52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSE52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ReferenciaINM_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ReferenciaINM_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ReferenciaINM_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ReferenciaINM_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ReferenciaINM_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEE52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031172396");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("referenciainmitemnaomensuravelwc.js", "?202031172397");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_idx;
         edtItemNaoMensuravel_Codigo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_CODIGO_"+sGXsfl_84_idx;
         edtItemNaoMensuravel_Descricao_Internalname = sPrefix+"ITEMNAOMENSURAVEL_DESCRICAO_"+sGXsfl_84_idx;
         edtItemNaoMensuravel_Referencia_Internalname = sPrefix+"ITEMNAOMENSURAVEL_REFERENCIA_"+sGXsfl_84_idx;
         edtItemNaoMensuravel_Valor_Internalname = sPrefix+"ITEMNAOMENSURAVEL_VALOR_"+sGXsfl_84_idx;
         cmbItemNaoMensuravel_Tipo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_TIPO_"+sGXsfl_84_idx;
         chkItemNaoMensuravel_Ativo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_ATIVO_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_fel_idx;
         edtItemNaoMensuravel_Codigo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_CODIGO_"+sGXsfl_84_fel_idx;
         edtItemNaoMensuravel_Descricao_Internalname = sPrefix+"ITEMNAOMENSURAVEL_DESCRICAO_"+sGXsfl_84_fel_idx;
         edtItemNaoMensuravel_Referencia_Internalname = sPrefix+"ITEMNAOMENSURAVEL_REFERENCIA_"+sGXsfl_84_fel_idx;
         edtItemNaoMensuravel_Valor_Internalname = sPrefix+"ITEMNAOMENSURAVEL_VALOR_"+sGXsfl_84_fel_idx;
         cmbItemNaoMensuravel_Tipo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_TIPO_"+sGXsfl_84_fel_idx;
         chkItemNaoMensuravel_Ativo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_ATIVO_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WBE50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV76Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV77Delete_GXI : context.PathToRelativeUrl( AV30Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Codigo_Internalname,StringUtil.RTrim( A715ItemNaoMensuravel_Codigo),StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"CodigoINM20",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Descricao_Internalname,(String)A714ItemNaoMensuravel_Descricao,(String)A714ItemNaoMensuravel_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)84,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga2M",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Referencia_Internalname,StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Referencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ",", "")),context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_84_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ITEMNAOMENSURAVEL_TIPO_" + sGXsfl_84_idx;
               cmbItemNaoMensuravel_Tipo.Name = GXCCtl;
               cmbItemNaoMensuravel_Tipo.WebTags = "";
               cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
               cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
               cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
               if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
               {
                  A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbItemNaoMensuravel_Tipo,(String)cmbItemNaoMensuravel_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)),(short)1,(String)cmbItemNaoMensuravel_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbItemNaoMensuravel_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbItemNaoMensuravel_Tipo_Internalname, "Values", (String)(cmbItemNaoMensuravel_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkItemNaoMensuravel_Ativo_Internalname,StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_CODIGO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_DESCRICAO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A714ItemNaoMensuravel_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_REFERENCIA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_VALOR"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_TIPO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_ATIVO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A716ItemNaoMensuravel_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavItemnaomensuravel_codigo1_Internalname = sPrefix+"vITEMNAOMENSURAVEL_CODIGO1";
         edtavItemnaomensuravel_descricao1_Internalname = sPrefix+"vITEMNAOMENSURAVEL_DESCRICAO1";
         cmbavItemnaomensuravel_tipo1_Internalname = sPrefix+"vITEMNAOMENSURAVEL_TIPO1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavItemnaomensuravel_codigo2_Internalname = sPrefix+"vITEMNAOMENSURAVEL_CODIGO2";
         edtavItemnaomensuravel_descricao2_Internalname = sPrefix+"vITEMNAOMENSURAVEL_DESCRICAO2";
         cmbavItemnaomensuravel_tipo2_Internalname = sPrefix+"vITEMNAOMENSURAVEL_TIPO2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavItemnaomensuravel_codigo3_Internalname = sPrefix+"vITEMNAOMENSURAVEL_CODIGO3";
         edtavItemnaomensuravel_descricao3_Internalname = sPrefix+"vITEMNAOMENSURAVEL_DESCRICAO3";
         cmbavItemnaomensuravel_tipo3_Internalname = sPrefix+"vITEMNAOMENSURAVEL_TIPO3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtItemNaoMensuravel_Codigo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_CODIGO";
         edtItemNaoMensuravel_Descricao_Internalname = sPrefix+"ITEMNAOMENSURAVEL_DESCRICAO";
         edtItemNaoMensuravel_Referencia_Internalname = sPrefix+"ITEMNAOMENSURAVEL_REFERENCIA";
         edtItemNaoMensuravel_Valor_Internalname = sPrefix+"ITEMNAOMENSURAVEL_VALOR";
         cmbItemNaoMensuravel_Tipo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_TIPO";
         chkItemNaoMensuravel_Ativo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtReferenciaINM_Codigo_Internalname = sPrefix+"REFERENCIAINM_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfitemnaomensuravel_codigo_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_CODIGO";
         edtavTfitemnaomensuravel_codigo_sel_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_CODIGO_SEL";
         edtavTfitemnaomensuravel_descricao_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_DESCRICAO";
         edtavTfitemnaomensuravel_descricao_sel_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_DESCRICAO_SEL";
         edtavTfitemnaomensuravel_referencia_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_REFERENCIA";
         edtavTfitemnaomensuravel_referencia_sel_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_REFERENCIA_SEL";
         edtavTfitemnaomensuravel_valor_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_VALOR";
         edtavTfitemnaomensuravel_valor_to_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_VALOR_TO";
         edtavTfitemnaomensuravel_ativo_sel_Internalname = sPrefix+"vTFITEMNAOMENSURAVEL_ATIVO_SEL";
         Ddo_itemnaomensuravel_codigo_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_CODIGO";
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_descricao_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_DESCRICAO";
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_referencia_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_REFERENCIA";
         edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_valor_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_VALOR";
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_tipo_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_TIPO";
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_ativo_Internalname = sPrefix+"DDO_ITEMNAOMENSURAVEL_ATIVO";
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbItemNaoMensuravel_Tipo_Jsonclick = "";
         edtItemNaoMensuravel_Valor_Jsonclick = "";
         edtItemNaoMensuravel_Referencia_Jsonclick = "";
         edtItemNaoMensuravel_Descricao_Jsonclick = "";
         edtItemNaoMensuravel_Codigo_Jsonclick = "";
         cmbavItemnaomensuravel_tipo1_Jsonclick = "";
         edtavItemnaomensuravel_codigo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavItemnaomensuravel_tipo2_Jsonclick = "";
         edtavItemnaomensuravel_codigo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavItemnaomensuravel_tipo3_Jsonclick = "";
         edtavItemnaomensuravel_codigo3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkItemNaoMensuravel_Ativo_Titleformat = 0;
         cmbItemNaoMensuravel_Tipo_Titleformat = 0;
         edtItemNaoMensuravel_Valor_Titleformat = 0;
         edtItemNaoMensuravel_Referencia_Titleformat = 0;
         edtItemNaoMensuravel_Descricao_Titleformat = 0;
         edtItemNaoMensuravel_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavItemnaomensuravel_tipo3.Visible = 1;
         edtavItemnaomensuravel_descricao3_Visible = 1;
         edtavItemnaomensuravel_codigo3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavItemnaomensuravel_tipo2.Visible = 1;
         edtavItemnaomensuravel_descricao2_Visible = 1;
         edtavItemnaomensuravel_codigo2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavItemnaomensuravel_tipo1.Visible = 1;
         edtavItemnaomensuravel_descricao1_Visible = 1;
         edtavItemnaomensuravel_codigo1_Visible = 1;
         chkItemNaoMensuravel_Ativo.Title.Text = "Ativo";
         cmbItemNaoMensuravel_Tipo.Title.Text = "Tipo";
         edtItemNaoMensuravel_Valor_Title = "Valor";
         edtItemNaoMensuravel_Referencia_Title = "Refer�ncia";
         edtItemNaoMensuravel_Descricao_Title = "Descri��o";
         edtItemNaoMensuravel_Codigo_Title = "C�digo";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkItemNaoMensuravel_Ativo.Caption = "";
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfitemnaomensuravel_ativo_sel_Jsonclick = "";
         edtavTfitemnaomensuravel_ativo_sel_Visible = 1;
         edtavTfitemnaomensuravel_valor_to_Jsonclick = "";
         edtavTfitemnaomensuravel_valor_to_Visible = 1;
         edtavTfitemnaomensuravel_valor_Jsonclick = "";
         edtavTfitemnaomensuravel_valor_Visible = 1;
         edtavTfitemnaomensuravel_referencia_sel_Jsonclick = "";
         edtavTfitemnaomensuravel_referencia_sel_Visible = 1;
         edtavTfitemnaomensuravel_referencia_Jsonclick = "";
         edtavTfitemnaomensuravel_referencia_Visible = 1;
         edtavTfitemnaomensuravel_descricao_sel_Visible = 1;
         edtavTfitemnaomensuravel_descricao_Visible = 1;
         edtavTfitemnaomensuravel_codigo_sel_Jsonclick = "";
         edtavTfitemnaomensuravel_codigo_sel_Visible = 1;
         edtavTfitemnaomensuravel_codigo_Jsonclick = "";
         edtavTfitemnaomensuravel_codigo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtReferenciaINM_Codigo_Jsonclick = "";
         edtReferenciaINM_Codigo_Visible = 1;
         Ddo_itemnaomensuravel_ativo_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_itemnaomensuravel_ativo_Datalisttype = "FixedValues";
         Ddo_itemnaomensuravel_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_ativo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_ativo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_ativo_Caption = "";
         Ddo_itemnaomensuravel_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_itemnaomensuravel_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_tipo_Datalistfixedvalues = "1:PC,2:PF";
         Ddo_itemnaomensuravel_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Datalisttype = "FixedValues";
         Ddo_itemnaomensuravel_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_tipo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_tipo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_tipo_Caption = "";
         Ddo_itemnaomensuravel_valor_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_valor_Rangefilterto = "At�";
         Ddo_itemnaomensuravel_valor_Rangefilterfrom = "Desde";
         Ddo_itemnaomensuravel_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_valor_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Filtertype = "Numeric";
         Ddo_itemnaomensuravel_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_valor_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_valor_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_valor_Caption = "";
         Ddo_itemnaomensuravel_referencia_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_referencia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_itemnaomensuravel_referencia_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_referencia_Loadingdata = "Carregando dados...";
         Ddo_itemnaomensuravel_referencia_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_referencia_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_referencia_Datalistupdateminimumcharacters = 0;
         Ddo_itemnaomensuravel_referencia_Datalistproc = "GetReferenciaINMItemNaoMensuravelWCFilterData";
         Ddo_itemnaomensuravel_referencia_Datalisttype = "Dynamic";
         Ddo_itemnaomensuravel_referencia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_referencia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_referencia_Filtertype = "Character";
         Ddo_itemnaomensuravel_referencia_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_referencia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_referencia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_referencia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_referencia_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_referencia_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_referencia_Caption = "";
         Ddo_itemnaomensuravel_descricao_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_itemnaomensuravel_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_descricao_Loadingdata = "Carregando dados...";
         Ddo_itemnaomensuravel_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_itemnaomensuravel_descricao_Datalistproc = "GetReferenciaINMItemNaoMensuravelWCFilterData";
         Ddo_itemnaomensuravel_descricao_Datalisttype = "Dynamic";
         Ddo_itemnaomensuravel_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_descricao_Filtertype = "Character";
         Ddo_itemnaomensuravel_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_descricao_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_descricao_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_descricao_Caption = "";
         Ddo_itemnaomensuravel_codigo_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_itemnaomensuravel_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_codigo_Loadingdata = "Carregando dados...";
         Ddo_itemnaomensuravel_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_itemnaomensuravel_codigo_Datalistproc = "GetReferenciaINMItemNaoMensuravelWCFilterData";
         Ddo_itemnaomensuravel_codigo_Datalisttype = "Dynamic";
         Ddo_itemnaomensuravel_codigo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_codigo_Filtertype = "Character";
         Ddo_itemnaomensuravel_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_codigo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_codigo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0}],oparms:[{av:'AV47ItemNaoMensuravel_CodigoTitleFilterData',fld:'vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ItemNaoMensuravel_DescricaoTitleFilterData',fld:'vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV55ItemNaoMensuravel_ReferenciaTitleFilterData',fld:'vITEMNAOMENSURAVEL_REFERENCIATITLEFILTERDATA',pic:'',nv:null},{av:'AV59ItemNaoMensuravel_ValorTitleFilterData',fld:'vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV63ItemNaoMensuravel_TipoTitleFilterData',fld:'vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV67ItemNaoMensuravel_AtivoTitleFilterData',fld:'vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtItemNaoMensuravel_Codigo_Titleformat',ctrl:'ITEMNAOMENSURAVEL_CODIGO',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Codigo_Title',ctrl:'ITEMNAOMENSURAVEL_CODIGO',prop:'Title'},{av:'edtItemNaoMensuravel_Descricao_Titleformat',ctrl:'ITEMNAOMENSURAVEL_DESCRICAO',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Descricao_Title',ctrl:'ITEMNAOMENSURAVEL_DESCRICAO',prop:'Title'},{av:'edtItemNaoMensuravel_Referencia_Titleformat',ctrl:'ITEMNAOMENSURAVEL_REFERENCIA',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Referencia_Title',ctrl:'ITEMNAOMENSURAVEL_REFERENCIA',prop:'Title'},{av:'edtItemNaoMensuravel_Valor_Titleformat',ctrl:'ITEMNAOMENSURAVEL_VALOR',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Valor_Title',ctrl:'ITEMNAOMENSURAVEL_VALOR',prop:'Title'},{av:'cmbItemNaoMensuravel_Tipo'},{av:'chkItemNaoMensuravel_Ativo_Titleformat',ctrl:'ITEMNAOMENSURAVEL_ATIVO',prop:'Titleformat'},{av:'chkItemNaoMensuravel_Ativo.Title.Text',ctrl:'ITEMNAOMENSURAVEL_ATIVO',prop:'Title'},{av:'AV72GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV73GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_CODIGO.ONOPTIONCLICKED","{handler:'E12E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_codigo_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_codigo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_DESCRICAO.ONOPTIONCLICKED","{handler:'E13E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_descricao_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_descricao_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_REFERENCIA.ONOPTIONCLICKED","{handler:'E14E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_referencia_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_referencia_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_referencia_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_VALOR.ONOPTIONCLICKED","{handler:'E15E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_valor_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_valor_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_valor_Filteredtextto_get',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_TIPO.ONOPTIONCLICKED","{handler:'E16E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_tipo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_tipo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_ATIVO.ONOPTIONCLICKED","{handler:'E17E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_itemnaomensuravel_ativo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_ativo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_referencia_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30E52',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV30Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'edtavItemnaomensuravel_codigo2_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO2',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao2_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO2',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_codigo3_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO3',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao3_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO3',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_codigo1_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO1',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao1_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO1',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24E52',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_codigo1_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO1',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao1_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO1',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'edtavItemnaomensuravel_codigo2_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO2',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao2_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO2',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_codigo3_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO3',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao3_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO3',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_codigo1_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO1',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao1_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO1',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26E52',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_codigo2_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO2',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao2_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO2',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'edtavItemnaomensuravel_codigo2_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO2',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao2_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO2',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_codigo3_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO3',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao3_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO3',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_codigo1_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO1',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao1_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO1',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27E52',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_codigo3_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO3',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao3_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO3',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21E52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ReferenciaINM_Codigo',fld:'vREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_REFERENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV48TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'FilteredText_set'},{av:'AV49TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SelectedValue_set'},{av:'AV52TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'FilteredText_set'},{av:'AV53TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SelectedValue_set'},{av:'AV56TFItemNaoMensuravel_Referencia',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_referencia_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'FilteredText_set'},{av:'AV57TFItemNaoMensuravel_Referencia_Sel',fld:'vTFITEMNAOMENSURAVEL_REFERENCIA_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_referencia_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_REFERENCIA',prop:'SelectedValue_set'},{av:'AV60TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_valor_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredText_set'},{av:'AV61TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_valor_Filteredtextto_set',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredTextTo_set'},{av:'AV65TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_itemnaomensuravel_tipo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SelectedValue_set'},{av:'AV68TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_itemnaomensuravel_ativo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32ItemNaoMensuravel_Codigo1',fld:'vITEMNAOMENSURAVEL_CODIGO1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavItemnaomensuravel_codigo1_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO1',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao1_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO1',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV35ItemNaoMensuravel_Codigo2',fld:'vITEMNAOMENSURAVEL_CODIGO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_Codigo3',fld:'vITEMNAOMENSURAVEL_CODIGO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18ItemNaoMensuravel_Descricao1',fld:'vITEMNAOMENSURAVEL_DESCRICAO1',pic:'',nv:''},{av:'AV34ItemNaoMensuravel_Tipo1',fld:'vITEMNAOMENSURAVEL_TIPO1',pic:'Z9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22ItemNaoMensuravel_Descricao2',fld:'vITEMNAOMENSURAVEL_DESCRICAO2',pic:'',nv:''},{av:'AV37ItemNaoMensuravel_Tipo2',fld:'vITEMNAOMENSURAVEL_TIPO2',pic:'Z9',nv:0},{av:'AV26ItemNaoMensuravel_Descricao3',fld:'vITEMNAOMENSURAVEL_DESCRICAO3',pic:'',nv:''},{av:'AV40ItemNaoMensuravel_Tipo3',fld:'vITEMNAOMENSURAVEL_TIPO3',pic:'Z9',nv:0},{av:'edtavItemnaomensuravel_codigo2_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO2',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao2_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO2',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_codigo3_Visible',ctrl:'vITEMNAOMENSURAVEL_CODIGO3',prop:'Visible'},{av:'edtavItemnaomensuravel_descricao3_Visible',ctrl:'vITEMNAOMENSURAVEL_DESCRICAO3',prop:'Visible'},{av:'cmbavItemnaomensuravel_tipo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E22E52',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_itemnaomensuravel_codigo_Activeeventkey = "";
         Ddo_itemnaomensuravel_codigo_Filteredtext_get = "";
         Ddo_itemnaomensuravel_codigo_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_descricao_Activeeventkey = "";
         Ddo_itemnaomensuravel_descricao_Filteredtext_get = "";
         Ddo_itemnaomensuravel_descricao_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_referencia_Activeeventkey = "";
         Ddo_itemnaomensuravel_referencia_Filteredtext_get = "";
         Ddo_itemnaomensuravel_referencia_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_valor_Activeeventkey = "";
         Ddo_itemnaomensuravel_valor_Filteredtext_get = "";
         Ddo_itemnaomensuravel_valor_Filteredtextto_get = "";
         Ddo_itemnaomensuravel_tipo_Activeeventkey = "";
         Ddo_itemnaomensuravel_tipo_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_ativo_Activeeventkey = "";
         Ddo_itemnaomensuravel_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV32ItemNaoMensuravel_Codigo1 = "";
         AV18ItemNaoMensuravel_Descricao1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV35ItemNaoMensuravel_Codigo2 = "";
         AV22ItemNaoMensuravel_Descricao2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV38ItemNaoMensuravel_Codigo3 = "";
         AV26ItemNaoMensuravel_Descricao3 = "";
         AV48TFItemNaoMensuravel_Codigo = "";
         AV49TFItemNaoMensuravel_Codigo_Sel = "";
         AV52TFItemNaoMensuravel_Descricao = "";
         AV53TFItemNaoMensuravel_Descricao_Sel = "";
         AV56TFItemNaoMensuravel_Referencia = "";
         AV57TFItemNaoMensuravel_Referencia_Sel = "";
         AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = "";
         AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = "";
         AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace = "";
         AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = "";
         AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = "";
         AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = "";
         AV78Pgmname = "";
         AV65TFItemNaoMensuravel_Tipo_Sels = new GxSimpleCollection();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A715ItemNaoMensuravel_Codigo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV70DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV47ItemNaoMensuravel_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ItemNaoMensuravel_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ItemNaoMensuravel_ReferenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ItemNaoMensuravel_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ItemNaoMensuravel_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67ItemNaoMensuravel_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_itemnaomensuravel_codigo_Filteredtext_set = "";
         Ddo_itemnaomensuravel_codigo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_codigo_Sortedstatus = "";
         Ddo_itemnaomensuravel_descricao_Filteredtext_set = "";
         Ddo_itemnaomensuravel_descricao_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_descricao_Sortedstatus = "";
         Ddo_itemnaomensuravel_referencia_Filteredtext_set = "";
         Ddo_itemnaomensuravel_referencia_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_referencia_Sortedstatus = "";
         Ddo_itemnaomensuravel_valor_Filteredtext_set = "";
         Ddo_itemnaomensuravel_valor_Filteredtextto_set = "";
         Ddo_itemnaomensuravel_valor_Sortedstatus = "";
         Ddo_itemnaomensuravel_tipo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_tipo_Sortedstatus = "";
         Ddo_itemnaomensuravel_ativo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV76Update_GXI = "";
         AV30Delete = "";
         AV77Delete_GXI = "";
         A714ItemNaoMensuravel_Descricao = "";
         A1804ItemNaoMensuravel_Referencia = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV32ItemNaoMensuravel_Codigo1 = "";
         lV18ItemNaoMensuravel_Descricao1 = "";
         lV35ItemNaoMensuravel_Codigo2 = "";
         lV22ItemNaoMensuravel_Descricao2 = "";
         lV38ItemNaoMensuravel_Codigo3 = "";
         lV26ItemNaoMensuravel_Descricao3 = "";
         lV48TFItemNaoMensuravel_Codigo = "";
         lV52TFItemNaoMensuravel_Descricao = "";
         lV56TFItemNaoMensuravel_Referencia = "";
         H00E52_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00E52_A709ReferenciaINM_Codigo = new int[1] ;
         H00E52_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         H00E52_A717ItemNaoMensuravel_Tipo = new short[1] ;
         H00E52_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         H00E52_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         H00E52_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         H00E52_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         H00E52_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00E53_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV64TFItemNaoMensuravel_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ReferenciaINM_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.referenciainmitemnaomensuravelwc__default(),
            new Object[][] {
                new Object[] {
               H00E52_A718ItemNaoMensuravel_AreaTrabalhoCod, H00E52_A709ReferenciaINM_Codigo, H00E52_A716ItemNaoMensuravel_Ativo, H00E52_A717ItemNaoMensuravel_Tipo, H00E52_A719ItemNaoMensuravel_Valor, H00E52_A1804ItemNaoMensuravel_Referencia, H00E52_n1804ItemNaoMensuravel_Referencia, H00E52_A714ItemNaoMensuravel_Descricao, H00E52_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               H00E53_AGRID_nRecordCount
               }
            }
         );
         AV78Pgmname = "ReferenciaINMItemNaoMensuravelWC";
         /* GeneXus formulas. */
         AV78Pgmname = "ReferenciaINMItemNaoMensuravelWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV34ItemNaoMensuravel_Tipo1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV37ItemNaoMensuravel_Tipo2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short AV40ItemNaoMensuravel_Tipo3 ;
      private short AV14OrderedBy ;
      private short AV68TFItemNaoMensuravel_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A717ItemNaoMensuravel_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_84_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtItemNaoMensuravel_Codigo_Titleformat ;
      private short edtItemNaoMensuravel_Descricao_Titleformat ;
      private short edtItemNaoMensuravel_Referencia_Titleformat ;
      private short edtItemNaoMensuravel_Valor_Titleformat ;
      private short cmbItemNaoMensuravel_Tipo_Titleformat ;
      private short chkItemNaoMensuravel_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ReferenciaINM_Codigo ;
      private int wcpOAV7ReferenciaINM_Codigo ;
      private int subGrid_Rows ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_itemnaomensuravel_referencia_Datalistupdateminimumcharacters ;
      private int A709ReferenciaINM_Codigo ;
      private int edtReferenciaINM_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfitemnaomensuravel_codigo_Visible ;
      private int edtavTfitemnaomensuravel_codigo_sel_Visible ;
      private int edtavTfitemnaomensuravel_descricao_Visible ;
      private int edtavTfitemnaomensuravel_descricao_sel_Visible ;
      private int edtavTfitemnaomensuravel_referencia_Visible ;
      private int edtavTfitemnaomensuravel_referencia_sel_Visible ;
      private int edtavTfitemnaomensuravel_valor_Visible ;
      private int edtavTfitemnaomensuravel_valor_to_Visible ;
      private int edtavTfitemnaomensuravel_ativo_sel_Visible ;
      private int edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV65TFItemNaoMensuravel_Tipo_Sels_Count ;
      private int AV71PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavItemnaomensuravel_codigo1_Visible ;
      private int edtavItemnaomensuravel_descricao1_Visible ;
      private int edtavItemnaomensuravel_codigo2_Visible ;
      private int edtavItemnaomensuravel_descricao2_Visible ;
      private int edtavItemnaomensuravel_codigo3_Visible ;
      private int edtavItemnaomensuravel_descricao3_Visible ;
      private int AV79GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV72GridCurrentPage ;
      private long AV73GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV60TFItemNaoMensuravel_Valor ;
      private decimal AV61TFItemNaoMensuravel_Valor_To ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_itemnaomensuravel_codigo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_codigo_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_codigo_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_descricao_Activeeventkey ;
      private String Ddo_itemnaomensuravel_descricao_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_descricao_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_referencia_Activeeventkey ;
      private String Ddo_itemnaomensuravel_referencia_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_referencia_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_valor_Activeeventkey ;
      private String Ddo_itemnaomensuravel_valor_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_valor_Filteredtextto_get ;
      private String Ddo_itemnaomensuravel_tipo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_tipo_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_ativo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_84_idx="0001" ;
      private String AV32ItemNaoMensuravel_Codigo1 ;
      private String AV35ItemNaoMensuravel_Codigo2 ;
      private String AV38ItemNaoMensuravel_Codigo3 ;
      private String AV48TFItemNaoMensuravel_Codigo ;
      private String AV49TFItemNaoMensuravel_Codigo_Sel ;
      private String AV56TFItemNaoMensuravel_Referencia ;
      private String AV57TFItemNaoMensuravel_Referencia_Sel ;
      private String AV78Pgmname ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_itemnaomensuravel_codigo_Caption ;
      private String Ddo_itemnaomensuravel_codigo_Tooltip ;
      private String Ddo_itemnaomensuravel_codigo_Cls ;
      private String Ddo_itemnaomensuravel_codigo_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_codigo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_codigo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_codigo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_codigo_Filtertype ;
      private String Ddo_itemnaomensuravel_codigo_Datalisttype ;
      private String Ddo_itemnaomensuravel_codigo_Datalistproc ;
      private String Ddo_itemnaomensuravel_codigo_Sortasc ;
      private String Ddo_itemnaomensuravel_codigo_Sortdsc ;
      private String Ddo_itemnaomensuravel_codigo_Loadingdata ;
      private String Ddo_itemnaomensuravel_codigo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_codigo_Noresultsfound ;
      private String Ddo_itemnaomensuravel_codigo_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_descricao_Caption ;
      private String Ddo_itemnaomensuravel_descricao_Tooltip ;
      private String Ddo_itemnaomensuravel_descricao_Cls ;
      private String Ddo_itemnaomensuravel_descricao_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_descricao_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_descricao_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_descricao_Sortedstatus ;
      private String Ddo_itemnaomensuravel_descricao_Filtertype ;
      private String Ddo_itemnaomensuravel_descricao_Datalisttype ;
      private String Ddo_itemnaomensuravel_descricao_Datalistproc ;
      private String Ddo_itemnaomensuravel_descricao_Sortasc ;
      private String Ddo_itemnaomensuravel_descricao_Sortdsc ;
      private String Ddo_itemnaomensuravel_descricao_Loadingdata ;
      private String Ddo_itemnaomensuravel_descricao_Cleanfilter ;
      private String Ddo_itemnaomensuravel_descricao_Noresultsfound ;
      private String Ddo_itemnaomensuravel_descricao_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_referencia_Caption ;
      private String Ddo_itemnaomensuravel_referencia_Tooltip ;
      private String Ddo_itemnaomensuravel_referencia_Cls ;
      private String Ddo_itemnaomensuravel_referencia_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_referencia_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_referencia_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_referencia_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_referencia_Sortedstatus ;
      private String Ddo_itemnaomensuravel_referencia_Filtertype ;
      private String Ddo_itemnaomensuravel_referencia_Datalisttype ;
      private String Ddo_itemnaomensuravel_referencia_Datalistproc ;
      private String Ddo_itemnaomensuravel_referencia_Sortasc ;
      private String Ddo_itemnaomensuravel_referencia_Sortdsc ;
      private String Ddo_itemnaomensuravel_referencia_Loadingdata ;
      private String Ddo_itemnaomensuravel_referencia_Cleanfilter ;
      private String Ddo_itemnaomensuravel_referencia_Noresultsfound ;
      private String Ddo_itemnaomensuravel_referencia_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_valor_Caption ;
      private String Ddo_itemnaomensuravel_valor_Tooltip ;
      private String Ddo_itemnaomensuravel_valor_Cls ;
      private String Ddo_itemnaomensuravel_valor_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_valor_Filteredtextto_set ;
      private String Ddo_itemnaomensuravel_valor_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_valor_Sortedstatus ;
      private String Ddo_itemnaomensuravel_valor_Filtertype ;
      private String Ddo_itemnaomensuravel_valor_Sortasc ;
      private String Ddo_itemnaomensuravel_valor_Sortdsc ;
      private String Ddo_itemnaomensuravel_valor_Cleanfilter ;
      private String Ddo_itemnaomensuravel_valor_Rangefilterfrom ;
      private String Ddo_itemnaomensuravel_valor_Rangefilterto ;
      private String Ddo_itemnaomensuravel_valor_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_tipo_Caption ;
      private String Ddo_itemnaomensuravel_tipo_Tooltip ;
      private String Ddo_itemnaomensuravel_tipo_Cls ;
      private String Ddo_itemnaomensuravel_tipo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_tipo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_tipo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_tipo_Datalisttype ;
      private String Ddo_itemnaomensuravel_tipo_Datalistfixedvalues ;
      private String Ddo_itemnaomensuravel_tipo_Sortasc ;
      private String Ddo_itemnaomensuravel_tipo_Sortdsc ;
      private String Ddo_itemnaomensuravel_tipo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_tipo_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_ativo_Caption ;
      private String Ddo_itemnaomensuravel_ativo_Tooltip ;
      private String Ddo_itemnaomensuravel_ativo_Cls ;
      private String Ddo_itemnaomensuravel_ativo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_ativo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_ativo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_ativo_Datalisttype ;
      private String Ddo_itemnaomensuravel_ativo_Datalistfixedvalues ;
      private String Ddo_itemnaomensuravel_ativo_Sortasc ;
      private String Ddo_itemnaomensuravel_ativo_Sortdsc ;
      private String Ddo_itemnaomensuravel_ativo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtReferenciaINM_Codigo_Internalname ;
      private String edtReferenciaINM_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfitemnaomensuravel_codigo_Internalname ;
      private String edtavTfitemnaomensuravel_codigo_Jsonclick ;
      private String edtavTfitemnaomensuravel_codigo_sel_Internalname ;
      private String edtavTfitemnaomensuravel_codigo_sel_Jsonclick ;
      private String edtavTfitemnaomensuravel_descricao_Internalname ;
      private String edtavTfitemnaomensuravel_descricao_sel_Internalname ;
      private String edtavTfitemnaomensuravel_referencia_Internalname ;
      private String edtavTfitemnaomensuravel_referencia_Jsonclick ;
      private String edtavTfitemnaomensuravel_referencia_sel_Internalname ;
      private String edtavTfitemnaomensuravel_referencia_sel_Jsonclick ;
      private String edtavTfitemnaomensuravel_valor_Internalname ;
      private String edtavTfitemnaomensuravel_valor_Jsonclick ;
      private String edtavTfitemnaomensuravel_valor_to_Internalname ;
      private String edtavTfitemnaomensuravel_valor_to_Jsonclick ;
      private String edtavTfitemnaomensuravel_ativo_sel_Internalname ;
      private String edtavTfitemnaomensuravel_ativo_sel_Jsonclick ;
      private String edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_referenciatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtItemNaoMensuravel_Codigo_Internalname ;
      private String edtItemNaoMensuravel_Descricao_Internalname ;
      private String A1804ItemNaoMensuravel_Referencia ;
      private String edtItemNaoMensuravel_Referencia_Internalname ;
      private String edtItemNaoMensuravel_Valor_Internalname ;
      private String cmbItemNaoMensuravel_Tipo_Internalname ;
      private String chkItemNaoMensuravel_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV32ItemNaoMensuravel_Codigo1 ;
      private String lV35ItemNaoMensuravel_Codigo2 ;
      private String lV38ItemNaoMensuravel_Codigo3 ;
      private String lV48TFItemNaoMensuravel_Codigo ;
      private String lV56TFItemNaoMensuravel_Referencia ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavItemnaomensuravel_codigo1_Internalname ;
      private String edtavItemnaomensuravel_descricao1_Internalname ;
      private String cmbavItemnaomensuravel_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavItemnaomensuravel_codigo2_Internalname ;
      private String edtavItemnaomensuravel_descricao2_Internalname ;
      private String cmbavItemnaomensuravel_tipo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavItemnaomensuravel_codigo3_Internalname ;
      private String edtavItemnaomensuravel_descricao3_Internalname ;
      private String cmbavItemnaomensuravel_tipo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_itemnaomensuravel_codigo_Internalname ;
      private String Ddo_itemnaomensuravel_descricao_Internalname ;
      private String Ddo_itemnaomensuravel_referencia_Internalname ;
      private String Ddo_itemnaomensuravel_valor_Internalname ;
      private String Ddo_itemnaomensuravel_tipo_Internalname ;
      private String Ddo_itemnaomensuravel_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtItemNaoMensuravel_Codigo_Title ;
      private String edtItemNaoMensuravel_Descricao_Title ;
      private String edtItemNaoMensuravel_Referencia_Title ;
      private String edtItemNaoMensuravel_Valor_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavItemnaomensuravel_codigo3_Jsonclick ;
      private String cmbavItemnaomensuravel_tipo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavItemnaomensuravel_codigo2_Jsonclick ;
      private String cmbavItemnaomensuravel_tipo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavItemnaomensuravel_codigo1_Jsonclick ;
      private String cmbavItemnaomensuravel_tipo1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ReferenciaINM_Codigo ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String ROClassString ;
      private String edtItemNaoMensuravel_Codigo_Jsonclick ;
      private String edtItemNaoMensuravel_Descricao_Jsonclick ;
      private String edtItemNaoMensuravel_Referencia_Jsonclick ;
      private String edtItemNaoMensuravel_Valor_Jsonclick ;
      private String cmbItemNaoMensuravel_Tipo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV15OrderedDsc ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_itemnaomensuravel_codigo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_codigo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_codigo_Includefilter ;
      private bool Ddo_itemnaomensuravel_codigo_Filterisrange ;
      private bool Ddo_itemnaomensuravel_codigo_Includedatalist ;
      private bool Ddo_itemnaomensuravel_descricao_Includesortasc ;
      private bool Ddo_itemnaomensuravel_descricao_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_descricao_Includefilter ;
      private bool Ddo_itemnaomensuravel_descricao_Filterisrange ;
      private bool Ddo_itemnaomensuravel_descricao_Includedatalist ;
      private bool Ddo_itemnaomensuravel_referencia_Includesortasc ;
      private bool Ddo_itemnaomensuravel_referencia_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_referencia_Includefilter ;
      private bool Ddo_itemnaomensuravel_referencia_Filterisrange ;
      private bool Ddo_itemnaomensuravel_referencia_Includedatalist ;
      private bool Ddo_itemnaomensuravel_valor_Includesortasc ;
      private bool Ddo_itemnaomensuravel_valor_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_valor_Includefilter ;
      private bool Ddo_itemnaomensuravel_valor_Filterisrange ;
      private bool Ddo_itemnaomensuravel_valor_Includedatalist ;
      private bool Ddo_itemnaomensuravel_tipo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_tipo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_tipo_Includefilter ;
      private bool Ddo_itemnaomensuravel_tipo_Includedatalist ;
      private bool Ddo_itemnaomensuravel_tipo_Allowmultipleselection ;
      private bool Ddo_itemnaomensuravel_ativo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_ativo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_ativo_Includefilter ;
      private bool Ddo_itemnaomensuravel_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1804ItemNaoMensuravel_Referencia ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private bool AV30Delete_IsBlob ;
      private String AV18ItemNaoMensuravel_Descricao1 ;
      private String AV22ItemNaoMensuravel_Descricao2 ;
      private String AV26ItemNaoMensuravel_Descricao3 ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String lV18ItemNaoMensuravel_Descricao1 ;
      private String lV22ItemNaoMensuravel_Descricao2 ;
      private String lV26ItemNaoMensuravel_Descricao3 ;
      private String AV64TFItemNaoMensuravel_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV52TFItemNaoMensuravel_Descricao ;
      private String AV53TFItemNaoMensuravel_Descricao_Sel ;
      private String AV50ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace ;
      private String AV54ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace ;
      private String AV58ddo_ItemNaoMensuravel_ReferenciaTitleControlIdToReplace ;
      private String AV62ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace ;
      private String AV66ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace ;
      private String AV69ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace ;
      private String AV76Update_GXI ;
      private String AV77Delete_GXI ;
      private String lV52TFItemNaoMensuravel_Descricao ;
      private String AV29Update ;
      private String AV30Delete ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavItemnaomensuravel_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavItemnaomensuravel_tipo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavItemnaomensuravel_tipo3 ;
      private GXCombobox cmbItemNaoMensuravel_Tipo ;
      private GXCheckbox chkItemNaoMensuravel_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00E52_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int[] H00E52_A709ReferenciaINM_Codigo ;
      private bool[] H00E52_A716ItemNaoMensuravel_Ativo ;
      private short[] H00E52_A717ItemNaoMensuravel_Tipo ;
      private decimal[] H00E52_A719ItemNaoMensuravel_Valor ;
      private String[] H00E52_A1804ItemNaoMensuravel_Referencia ;
      private bool[] H00E52_n1804ItemNaoMensuravel_Referencia ;
      private String[] H00E52_A714ItemNaoMensuravel_Descricao ;
      private String[] H00E52_A715ItemNaoMensuravel_Codigo ;
      private long[] H00E53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV65TFItemNaoMensuravel_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ItemNaoMensuravel_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51ItemNaoMensuravel_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55ItemNaoMensuravel_ReferenciaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59ItemNaoMensuravel_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63ItemNaoMensuravel_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67ItemNaoMensuravel_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV70DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class referenciainmitemnaomensuravelwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00E52( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV65TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV32ItemNaoMensuravel_Codigo1 ,
                                             String AV18ItemNaoMensuravel_Descricao1 ,
                                             short AV34ItemNaoMensuravel_Tipo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV35ItemNaoMensuravel_Codigo2 ,
                                             String AV22ItemNaoMensuravel_Descricao2 ,
                                             short AV37ItemNaoMensuravel_Tipo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV38ItemNaoMensuravel_Codigo3 ,
                                             String AV26ItemNaoMensuravel_Descricao3 ,
                                             short AV40ItemNaoMensuravel_Tipo3 ,
                                             String AV49TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV48TFItemNaoMensuravel_Codigo ,
                                             String AV53TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV52TFItemNaoMensuravel_Descricao ,
                                             String AV57TFItemNaoMensuravel_Referencia_Sel ,
                                             String AV56TFItemNaoMensuravel_Referencia ,
                                             decimal AV60TFItemNaoMensuravel_Valor ,
                                             decimal AV61TFItemNaoMensuravel_Valor_To ,
                                             int AV65TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV68TFItemNaoMensuravel_Ativo_Sel ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A1804ItemNaoMensuravel_Referencia ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A709ReferenciaINM_Codigo ,
                                             int AV7ReferenciaINM_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ItemNaoMensuravel_AreaTrabalhoCod], [ReferenciaINM_Codigo], [ItemNaoMensuravel_Ativo], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Codigo]";
         sFromString = " FROM [ItemNaoMensuravel] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([ReferenciaINM_Codigo] = @AV7ReferenciaINM_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV32ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV32ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV18ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV18ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV34ItemNaoMensuravel_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV34ItemNaoMensuravel_Tipo1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV35ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV35ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV22ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV22ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV37ItemNaoMensuravel_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV37ItemNaoMensuravel_Tipo2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV38ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV38ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV26ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV26ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV40ItemNaoMensuravel_Tipo3) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV40ItemNaoMensuravel_Tipo3)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV48TFItemNaoMensuravel_Codigo)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] = @AV49TFItemNaoMensuravel_Codigo_Sel)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV52TFItemNaoMensuravel_Descricao)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] = @AV53TFItemNaoMensuravel_Descricao_Sel)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] like @lV56TFItemNaoMensuravel_Referencia)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] = @AV57TFItemNaoMensuravel_Referencia_Sel)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV60TFItemNaoMensuravel_Valor) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] >= @AV60TFItemNaoMensuravel_Valor)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFItemNaoMensuravel_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] <= @AV61TFItemNaoMensuravel_Valor_To)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV65TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFItemNaoMensuravel_Tipo_Sels, "[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV68TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV68TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 0)";
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Codigo]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Descricao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Referencia]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Referencia] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Valor]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Valor] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Tipo]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Tipo] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Ativo]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ReferenciaINM_Codigo] DESC, [ItemNaoMensuravel_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00E53( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV65TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV32ItemNaoMensuravel_Codigo1 ,
                                             String AV18ItemNaoMensuravel_Descricao1 ,
                                             short AV34ItemNaoMensuravel_Tipo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV35ItemNaoMensuravel_Codigo2 ,
                                             String AV22ItemNaoMensuravel_Descricao2 ,
                                             short AV37ItemNaoMensuravel_Tipo2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV38ItemNaoMensuravel_Codigo3 ,
                                             String AV26ItemNaoMensuravel_Descricao3 ,
                                             short AV40ItemNaoMensuravel_Tipo3 ,
                                             String AV49TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV48TFItemNaoMensuravel_Codigo ,
                                             String AV53TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV52TFItemNaoMensuravel_Descricao ,
                                             String AV57TFItemNaoMensuravel_Referencia_Sel ,
                                             String AV56TFItemNaoMensuravel_Referencia ,
                                             decimal AV60TFItemNaoMensuravel_Valor ,
                                             decimal AV61TFItemNaoMensuravel_Valor_To ,
                                             int AV65TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV68TFItemNaoMensuravel_Ativo_Sel ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A1804ItemNaoMensuravel_Referencia ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A709ReferenciaINM_Codigo ,
                                             int AV7ReferenciaINM_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ItemNaoMensuravel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ReferenciaINM_Codigo] = @AV7ReferenciaINM_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV32ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV32ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV18ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV18ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV34ItemNaoMensuravel_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV34ItemNaoMensuravel_Tipo1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV35ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV35ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV22ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV22ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV37ItemNaoMensuravel_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV37ItemNaoMensuravel_Tipo2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV38ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV38ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV26ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV26ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV40ItemNaoMensuravel_Tipo3) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV40ItemNaoMensuravel_Tipo3)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFItemNaoMensuravel_Codigo)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV48TFItemNaoMensuravel_Codigo)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] = @AV49TFItemNaoMensuravel_Codigo_Sel)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFItemNaoMensuravel_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV52TFItemNaoMensuravel_Descricao)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] = @AV53TFItemNaoMensuravel_Descricao_Sel)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFItemNaoMensuravel_Referencia)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] like @lV56TFItemNaoMensuravel_Referencia)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFItemNaoMensuravel_Referencia_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] = @AV57TFItemNaoMensuravel_Referencia_Sel)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV60TFItemNaoMensuravel_Valor) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] >= @AV60TFItemNaoMensuravel_Valor)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFItemNaoMensuravel_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] <= @AV61TFItemNaoMensuravel_Valor_To)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV65TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV65TFItemNaoMensuravel_Tipo_Sels, "[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV68TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV68TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00E52(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (short)dynConstraints[34] , (bool)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] );
               case 1 :
                     return conditional_H00E53(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (short)dynConstraints[34] , (bool)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00E52 ;
          prmH00E52 = new Object[] {
          new Object[] {"@AV7ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV32ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV18ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV18ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV34ItemNaoMensuravel_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV35ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV35ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV22ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV22ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV37ItemNaoMensuravel_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV38ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV38ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV26ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV26ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV40ItemNaoMensuravel_Tipo3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV48TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56TFItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@AV57TFItemNaoMensuravel_Referencia_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV60TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV61TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00E53 ;
          prmH00E53 = new Object[] {
          new Object[] {"@AV7ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV32ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV18ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV18ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV34ItemNaoMensuravel_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV35ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV35ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV22ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV22ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV37ItemNaoMensuravel_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV38ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV38ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV26ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV26ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV40ItemNaoMensuravel_Tipo3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV48TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56TFItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@AV57TFItemNaoMensuravel_Referencia_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV60TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV61TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00E52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E52,11,0,true,false )
             ,new CursorDef("H00E53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                return;
       }
    }

 }

}
