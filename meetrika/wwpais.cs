/*
               File: WWPais
        Description:  Pais
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:46.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwpais : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwpais( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwpais( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Pais_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pais_Nome1", AV17Pais_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Pais_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Pais_Nome2", AV21Pais_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Pais_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome3", AV25Pais_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV55TFPais_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
               AV56TFPais_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
               AV59TFPais_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFPais_Nome", AV59TFPais_Nome);
               AV60TFPais_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFPais_Nome_Sel", AV60TFPais_Nome_Sel);
               AV67ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
               AV57ddo_Pais_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Pais_CodigoTitleControlIdToReplace", AV57ddo_Pais_CodigoTitleControlIdToReplace);
               AV61ddo_Pais_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Pais_NomeTitleControlIdToReplace", AV61ddo_Pais_NomeTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV93Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A21Pais_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA122( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START122( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117304665");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwpais.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME1", StringUtil.RTrim( AV17Pais_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME2", StringUtil.RTrim( AV21Pais_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME3", StringUtil.RTrim( AV25Pais_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFPais_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFPais_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_NOME", StringUtil.RTrim( AV59TFPais_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_NOME_SEL", StringUtil.RTrim( AV60TFPais_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV71ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV71ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV62DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPAIS_CODIGOTITLEFILTERDATA", AV54Pais_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPAIS_CODIGOTITLEFILTERDATA", AV54Pais_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPAIS_NOMETITLEFILTERDATA", AV58Pais_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPAIS_NOMETITLEFILTERDATA", AV58Pais_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV93Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Caption", StringUtil.RTrim( Ddo_pais_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Tooltip", StringUtil.RTrim( Ddo_pais_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Cls", StringUtil.RTrim( Ddo_pais_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_pais_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_pais_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pais_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pais_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_pais_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_pais_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_pais_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_pais_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filtertype", StringUtil.RTrim( Ddo_pais_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_pais_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_pais_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Sortasc", StringUtil.RTrim( Ddo_pais_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_pais_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_pais_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_pais_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_pais_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_pais_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Caption", StringUtil.RTrim( Ddo_pais_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Tooltip", StringUtil.RTrim( Ddo_pais_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Cls", StringUtil.RTrim( Ddo_pais_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_pais_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_pais_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_pais_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pais_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_pais_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_pais_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_pais_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_pais_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filtertype", StringUtil.RTrim( Ddo_pais_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_pais_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_pais_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalisttype", StringUtil.RTrim( Ddo_pais_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalistproc", StringUtil.RTrim( Ddo_pais_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pais_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortasc", StringUtil.RTrim( Ddo_pais_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortdsc", StringUtil.RTrim( Ddo_pais_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Loadingdata", StringUtil.RTrim( Ddo_pais_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_pais_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_pais_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_pais_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_pais_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_pais_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_pais_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_pais_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_pais_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_pais_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE122( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT122( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwpais.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWPais" ;
      }

      public override String GetPgmdesc( )
      {
         return " Pais" ;
      }

      protected void WB120( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_122( true) ;
         }
         else
         {
            wb_table1_2_122( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_122e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV67ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPais.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFPais_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFPais_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPais.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFPais_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFPais_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPais.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_nome_Internalname, StringUtil.RTrim( AV59TFPais_Nome), StringUtil.RTrim( context.localUtil.Format( AV59TFPais_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPais.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_nome_sel_Internalname, StringUtil.RTrim( AV60TFPais_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFPais_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPais.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PAIS_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pais_codigotitlecontrolidtoreplace_Internalname, AV57ddo_Pais_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_pais_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPais.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PAIS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pais_nometitlecontrolidtoreplace_Internalname, AV61ddo_Pais_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_pais_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPais.htm");
         }
         wbLoad = true;
      }

      protected void START122( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Pais", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP120( ) ;
      }

      protected void WS122( )
      {
         START122( ) ;
         EVT122( ) ;
      }

      protected void EVT122( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11122 */
                              E11122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12122 */
                              E12122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PAIS_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13122 */
                              E13122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PAIS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14122 */
                              E14122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15122 */
                              E15122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16122 */
                              E16122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17122 */
                              E17122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18122 */
                              E18122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19122 */
                              E19122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20122 */
                              E20122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21122 */
                              E21122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22122 */
                              E22122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23122 */
                              E23122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24122 */
                              E24122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV90Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV91Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV66Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV92Display_GXI : context.convertURL( context.PathToRelativeUrl( AV66Display))));
                              A21Pais_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPais_Codigo_Internalname), ",", "."));
                              A22Pais_Nome = StringUtil.Upper( cgiGet( edtPais_Nome_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25122 */
                                    E25122 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26122 */
                                    E26122 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27122 */
                                    E27122 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME1"), AV17Pais_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME2"), AV21Pais_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME3"), AV25Pais_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPAIS_CODIGO"), ",", ".") != Convert.ToDecimal( AV55TFPais_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPAIS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV56TFPais_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME"), AV59TFPais_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME_SEL"), AV60TFPais_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE122( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA122( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Pais_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Pais_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Pais_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV55TFPais_Codigo ,
                                       int AV56TFPais_Codigo_To ,
                                       String AV59TFPais_Nome ,
                                       String AV60TFPais_Nome_Sel ,
                                       short AV67ManageFiltersExecutionStep ,
                                       String AV57ddo_Pais_CodigoTitleControlIdToReplace ,
                                       String AV61ddo_Pais_NomeTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV93Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A21Pais_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF122( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PAIS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PAIS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PAIS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A22Pais_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PAIS_NOME", StringUtil.RTrim( A22Pais_Nome));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF122( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV93Pgmname = "WWPais";
         context.Gx_err = 0;
      }

      protected void RF122( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E26122 */
         E26122 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV75WWPaisDS_1_Dynamicfiltersselector1 ,
                                                 AV76WWPaisDS_2_Dynamicfiltersoperator1 ,
                                                 AV77WWPaisDS_3_Pais_nome1 ,
                                                 AV78WWPaisDS_4_Dynamicfiltersenabled2 ,
                                                 AV79WWPaisDS_5_Dynamicfiltersselector2 ,
                                                 AV80WWPaisDS_6_Dynamicfiltersoperator2 ,
                                                 AV81WWPaisDS_7_Pais_nome2 ,
                                                 AV82WWPaisDS_8_Dynamicfiltersenabled3 ,
                                                 AV83WWPaisDS_9_Dynamicfiltersselector3 ,
                                                 AV84WWPaisDS_10_Dynamicfiltersoperator3 ,
                                                 AV85WWPaisDS_11_Pais_nome3 ,
                                                 AV86WWPaisDS_12_Tfpais_codigo ,
                                                 AV87WWPaisDS_13_Tfpais_codigo_to ,
                                                 AV89WWPaisDS_15_Tfpais_nome_sel ,
                                                 AV88WWPaisDS_14_Tfpais_nome ,
                                                 A22Pais_Nome ,
                                                 A21Pais_Codigo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV77WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1), 50, "%");
            lV77WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1), 50, "%");
            lV81WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2), 50, "%");
            lV81WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2), 50, "%");
            lV85WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3), 50, "%");
            lV85WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3), 50, "%");
            lV88WWPaisDS_14_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV88WWPaisDS_14_Tfpais_nome), 50, "%");
            /* Using cursor H00122 */
            pr_default.execute(0, new Object[] {lV77WWPaisDS_3_Pais_nome1, lV77WWPaisDS_3_Pais_nome1, lV81WWPaisDS_7_Pais_nome2, lV81WWPaisDS_7_Pais_nome2, lV85WWPaisDS_11_Pais_nome3, lV85WWPaisDS_11_Pais_nome3, AV86WWPaisDS_12_Tfpais_codigo, AV87WWPaisDS_13_Tfpais_codigo_to, lV88WWPaisDS_14_Tfpais_nome, AV89WWPaisDS_15_Tfpais_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A22Pais_Nome = H00122_A22Pais_Nome[0];
               A21Pais_Codigo = H00122_A21Pais_Codigo[0];
               /* Execute user event: E27122 */
               E27122 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WB120( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV75WWPaisDS_1_Dynamicfiltersselector1 ,
                                              AV76WWPaisDS_2_Dynamicfiltersoperator1 ,
                                              AV77WWPaisDS_3_Pais_nome1 ,
                                              AV78WWPaisDS_4_Dynamicfiltersenabled2 ,
                                              AV79WWPaisDS_5_Dynamicfiltersselector2 ,
                                              AV80WWPaisDS_6_Dynamicfiltersoperator2 ,
                                              AV81WWPaisDS_7_Pais_nome2 ,
                                              AV82WWPaisDS_8_Dynamicfiltersenabled3 ,
                                              AV83WWPaisDS_9_Dynamicfiltersselector3 ,
                                              AV84WWPaisDS_10_Dynamicfiltersoperator3 ,
                                              AV85WWPaisDS_11_Pais_nome3 ,
                                              AV86WWPaisDS_12_Tfpais_codigo ,
                                              AV87WWPaisDS_13_Tfpais_codigo_to ,
                                              AV89WWPaisDS_15_Tfpais_nome_sel ,
                                              AV88WWPaisDS_14_Tfpais_nome ,
                                              A22Pais_Nome ,
                                              A21Pais_Codigo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV77WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1), 50, "%");
         lV77WWPaisDS_3_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1), 50, "%");
         lV81WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2), 50, "%");
         lV81WWPaisDS_7_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2), 50, "%");
         lV85WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3), 50, "%");
         lV85WWPaisDS_11_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3), 50, "%");
         lV88WWPaisDS_14_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV88WWPaisDS_14_Tfpais_nome), 50, "%");
         /* Using cursor H00123 */
         pr_default.execute(1, new Object[] {lV77WWPaisDS_3_Pais_nome1, lV77WWPaisDS_3_Pais_nome1, lV81WWPaisDS_7_Pais_nome2, lV81WWPaisDS_7_Pais_nome2, lV85WWPaisDS_11_Pais_nome3, lV85WWPaisDS_11_Pais_nome3, AV86WWPaisDS_12_Tfpais_codigo, AV87WWPaisDS_13_Tfpais_codigo_to, lV88WWPaisDS_14_Tfpais_nome, AV89WWPaisDS_15_Tfpais_nome_sel});
         GRID_nRecordCount = H00123_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP120( )
      {
         /* Before Start, stand alone formulas. */
         AV93Pgmname = "WWPais";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25122 */
         E25122 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV71ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV62DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPAIS_CODIGOTITLEFILTERDATA"), AV54Pais_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPAIS_NOMETITLEFILTERDATA"), AV58Pais_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Pais_Nome1 = StringUtil.Upper( cgiGet( edtavPais_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pais_Nome1", AV17Pais_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Pais_Nome2 = StringUtil.Upper( cgiGet( edtavPais_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Pais_Nome2", AV21Pais_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Pais_Nome3 = StringUtil.Upper( cgiGet( edtavPais_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome3", AV25Pais_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV67ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfpais_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfpais_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPAIS_CODIGO");
               GX_FocusControl = edtavTfpais_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFPais_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
            }
            else
            {
               AV55TFPais_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfpais_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfpais_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfpais_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPAIS_CODIGO_TO");
               GX_FocusControl = edtavTfpais_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFPais_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
            }
            else
            {
               AV56TFPais_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfpais_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
            }
            AV59TFPais_Nome = StringUtil.Upper( cgiGet( edtavTfpais_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFPais_Nome", AV59TFPais_Nome);
            AV60TFPais_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfpais_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFPais_Nome_Sel", AV60TFPais_Nome_Sel);
            AV57ddo_Pais_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_pais_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Pais_CodigoTitleControlIdToReplace", AV57ddo_Pais_CodigoTitleControlIdToReplace);
            AV61ddo_Pais_NomeTitleControlIdToReplace = cgiGet( edtavDdo_pais_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Pais_NomeTitleControlIdToReplace", AV61ddo_Pais_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV64GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV65GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_pais_codigo_Caption = cgiGet( "DDO_PAIS_CODIGO_Caption");
            Ddo_pais_codigo_Tooltip = cgiGet( "DDO_PAIS_CODIGO_Tooltip");
            Ddo_pais_codigo_Cls = cgiGet( "DDO_PAIS_CODIGO_Cls");
            Ddo_pais_codigo_Filteredtext_set = cgiGet( "DDO_PAIS_CODIGO_Filteredtext_set");
            Ddo_pais_codigo_Filteredtextto_set = cgiGet( "DDO_PAIS_CODIGO_Filteredtextto_set");
            Ddo_pais_codigo_Dropdownoptionstype = cgiGet( "DDO_PAIS_CODIGO_Dropdownoptionstype");
            Ddo_pais_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_PAIS_CODIGO_Titlecontrolidtoreplace");
            Ddo_pais_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_CODIGO_Includesortasc"));
            Ddo_pais_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_CODIGO_Includesortdsc"));
            Ddo_pais_codigo_Sortedstatus = cgiGet( "DDO_PAIS_CODIGO_Sortedstatus");
            Ddo_pais_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PAIS_CODIGO_Includefilter"));
            Ddo_pais_codigo_Filtertype = cgiGet( "DDO_PAIS_CODIGO_Filtertype");
            Ddo_pais_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PAIS_CODIGO_Filterisrange"));
            Ddo_pais_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PAIS_CODIGO_Includedatalist"));
            Ddo_pais_codigo_Sortasc = cgiGet( "DDO_PAIS_CODIGO_Sortasc");
            Ddo_pais_codigo_Sortdsc = cgiGet( "DDO_PAIS_CODIGO_Sortdsc");
            Ddo_pais_codigo_Cleanfilter = cgiGet( "DDO_PAIS_CODIGO_Cleanfilter");
            Ddo_pais_codigo_Rangefilterfrom = cgiGet( "DDO_PAIS_CODIGO_Rangefilterfrom");
            Ddo_pais_codigo_Rangefilterto = cgiGet( "DDO_PAIS_CODIGO_Rangefilterto");
            Ddo_pais_codigo_Searchbuttontext = cgiGet( "DDO_PAIS_CODIGO_Searchbuttontext");
            Ddo_pais_nome_Caption = cgiGet( "DDO_PAIS_NOME_Caption");
            Ddo_pais_nome_Tooltip = cgiGet( "DDO_PAIS_NOME_Tooltip");
            Ddo_pais_nome_Cls = cgiGet( "DDO_PAIS_NOME_Cls");
            Ddo_pais_nome_Filteredtext_set = cgiGet( "DDO_PAIS_NOME_Filteredtext_set");
            Ddo_pais_nome_Selectedvalue_set = cgiGet( "DDO_PAIS_NOME_Selectedvalue_set");
            Ddo_pais_nome_Dropdownoptionstype = cgiGet( "DDO_PAIS_NOME_Dropdownoptionstype");
            Ddo_pais_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PAIS_NOME_Titlecontrolidtoreplace");
            Ddo_pais_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includesortasc"));
            Ddo_pais_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includesortdsc"));
            Ddo_pais_nome_Sortedstatus = cgiGet( "DDO_PAIS_NOME_Sortedstatus");
            Ddo_pais_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includefilter"));
            Ddo_pais_nome_Filtertype = cgiGet( "DDO_PAIS_NOME_Filtertype");
            Ddo_pais_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Filterisrange"));
            Ddo_pais_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includedatalist"));
            Ddo_pais_nome_Datalisttype = cgiGet( "DDO_PAIS_NOME_Datalisttype");
            Ddo_pais_nome_Datalistproc = cgiGet( "DDO_PAIS_NOME_Datalistproc");
            Ddo_pais_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PAIS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pais_nome_Sortasc = cgiGet( "DDO_PAIS_NOME_Sortasc");
            Ddo_pais_nome_Sortdsc = cgiGet( "DDO_PAIS_NOME_Sortdsc");
            Ddo_pais_nome_Loadingdata = cgiGet( "DDO_PAIS_NOME_Loadingdata");
            Ddo_pais_nome_Cleanfilter = cgiGet( "DDO_PAIS_NOME_Cleanfilter");
            Ddo_pais_nome_Noresultsfound = cgiGet( "DDO_PAIS_NOME_Noresultsfound");
            Ddo_pais_nome_Searchbuttontext = cgiGet( "DDO_PAIS_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_pais_codigo_Activeeventkey = cgiGet( "DDO_PAIS_CODIGO_Activeeventkey");
            Ddo_pais_codigo_Filteredtext_get = cgiGet( "DDO_PAIS_CODIGO_Filteredtext_get");
            Ddo_pais_codigo_Filteredtextto_get = cgiGet( "DDO_PAIS_CODIGO_Filteredtextto_get");
            Ddo_pais_nome_Activeeventkey = cgiGet( "DDO_PAIS_NOME_Activeeventkey");
            Ddo_pais_nome_Filteredtext_get = cgiGet( "DDO_PAIS_NOME_Filteredtext_get");
            Ddo_pais_nome_Selectedvalue_get = cgiGet( "DDO_PAIS_NOME_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME1"), AV17Pais_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME2"), AV21Pais_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME3"), AV25Pais_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPAIS_CODIGO"), ",", ".") != Convert.ToDecimal( AV55TFPais_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPAIS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV56TFPais_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME"), AV59TFPais_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME_SEL"), AV60TFPais_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25122 */
         E25122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25122( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfpais_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_codigo_Visible), 5, 0)));
         edtavTfpais_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_codigo_to_Visible), 5, 0)));
         edtavTfpais_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_nome_Visible), 5, 0)));
         edtavTfpais_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_nome_sel_Visible), 5, 0)));
         Ddo_pais_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Pais_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "TitleControlIdToReplace", Ddo_pais_codigo_Titlecontrolidtoreplace);
         AV57ddo_Pais_CodigoTitleControlIdToReplace = Ddo_pais_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Pais_CodigoTitleControlIdToReplace", AV57ddo_Pais_CodigoTitleControlIdToReplace);
         edtavDdo_pais_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pais_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pais_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pais_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Pais_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "TitleControlIdToReplace", Ddo_pais_nome_Titlecontrolidtoreplace);
         AV61ddo_Pais_NomeTitleControlIdToReplace = Ddo_pais_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Pais_NomeTitleControlIdToReplace", AV61ddo_Pais_NomeTitleControlIdToReplace);
         edtavDdo_pais_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pais_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pais_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Pais";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pa�s", 0);
         cmbavOrderedby.addItem("2", "C�d. Pa�s", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV62DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV62DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26122( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV54Pais_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Pais_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV67ManageFiltersExecutionStep == 1 )
         {
            AV67ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV67ManageFiltersExecutionStep == 2 )
         {
            AV67ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPais_Codigo_Titleformat = 2;
         edtPais_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�d. Pa�s", AV57ddo_Pais_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPais_Codigo_Internalname, "Title", edtPais_Codigo_Title);
         edtPais_Nome_Titleformat = 2;
         edtPais_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pa�s", AV61ddo_Pais_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPais_Nome_Internalname, "Title", edtPais_Nome_Title);
         AV64GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64GridCurrentPage), 10, 0)));
         AV65GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65GridPageCount), 10, 0)));
         AV75WWPaisDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV76WWPaisDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV77WWPaisDS_3_Pais_nome1 = AV17Pais_Nome1;
         AV78WWPaisDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV79WWPaisDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV80WWPaisDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV81WWPaisDS_7_Pais_nome2 = AV21Pais_Nome2;
         AV82WWPaisDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV83WWPaisDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV84WWPaisDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV85WWPaisDS_11_Pais_nome3 = AV25Pais_Nome3;
         AV86WWPaisDS_12_Tfpais_codigo = AV55TFPais_Codigo;
         AV87WWPaisDS_13_Tfpais_codigo_to = AV56TFPais_Codigo_To;
         AV88WWPaisDS_14_Tfpais_nome = AV59TFPais_Nome;
         AV89WWPaisDS_15_Tfpais_nome_sel = AV60TFPais_Nome_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Pais_CodigoTitleFilterData", AV54Pais_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58Pais_NomeTitleFilterData", AV58Pais_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71ManageFiltersData", AV71ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12122( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV63PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV63PageToGo) ;
         }
      }

      protected void E13122( )
      {
         /* Ddo_pais_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pais_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "SortedStatus", Ddo_pais_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "SortedStatus", Ddo_pais_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFPais_Codigo = (int)(NumberUtil.Val( Ddo_pais_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
            AV56TFPais_Codigo_To = (int)(NumberUtil.Val( Ddo_pais_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14122( )
      {
         /* Ddo_pais_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFPais_Nome = Ddo_pais_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFPais_Nome", AV59TFPais_Nome);
            AV60TFPais_Nome_Sel = Ddo_pais_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFPais_Nome_Sel", AV60TFPais_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27122( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("pais.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A21Pais_Codigo);
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV90Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV90Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("pais.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A21Pais_Codigo);
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV91Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV91Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewpais.aspx") + "?" + UrlEncode("" +A21Pais_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV66Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV66Display);
            AV92Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV66Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV66Display);
            AV92Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtPais_Nome_Link = formatLink("viewpais.aspx") + "?" + UrlEncode("" +A21Pais_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E15122( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20122( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16122( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21122( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22122( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17122( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23122( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18122( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Pais_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Pais_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Pais_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV55TFPais_Codigo, AV56TFPais_Codigo_To, AV59TFPais_Nome, AV60TFPais_Nome_Sel, AV67ManageFiltersExecutionStep, AV57ddo_Pais_CodigoTitleControlIdToReplace, AV61ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV93Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24122( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11122( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPaisFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV67ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPaisFilters")), new Object[] {});
            AV67ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV67ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV68ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWPaisFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV68ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV93Pgmname+"GridState",  AV68ManageFiltersXml) ;
               AV10GridState.FromXml(AV68ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E19122( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("pais.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_pais_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "SortedStatus", Ddo_pais_codigo_Sortedstatus);
         Ddo_pais_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_pais_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "SortedStatus", Ddo_pais_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_pais_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPais_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 )
         {
            edtavPais_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavPais_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PAIS_NOME") == 0 )
         {
            edtavPais_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavPais_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PAIS_NOME") == 0 )
         {
            edtavPais_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Pais_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Pais_Nome2", AV21Pais_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Pais_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome3", AV25Pais_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV71ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV72ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV72ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV72ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV72ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV72ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
         AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV72ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV72ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV72ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV72ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
         AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV72ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
         AV69ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWPaisFilters"), "");
         AV94GXV1 = 1;
         while ( AV94GXV1 <= AV69ManageFiltersItems.Count )
         {
            AV70ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV69ManageFiltersItems.Item(AV94GXV1));
            AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV72ManageFiltersDataItem.gxTpr_Title = AV70ManageFiltersItem.gxTpr_Title;
            AV72ManageFiltersDataItem.gxTpr_Eventkey = AV70ManageFiltersItem.gxTpr_Title;
            AV72ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV72ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
            if ( AV71ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV94GXV1 = (int)(AV94GXV1+1);
         }
         if ( AV71ManageFiltersData.Count > 3 )
         {
            AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV72ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
            AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV72ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV72ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV72ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV72ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV72ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV71ManageFiltersData.Add(AV72ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV55TFPais_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
         Ddo_pais_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "FilteredText_set", Ddo_pais_codigo_Filteredtext_set);
         AV56TFPais_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
         Ddo_pais_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "FilteredTextTo_set", Ddo_pais_codigo_Filteredtextto_set);
         AV59TFPais_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFPais_Nome", AV59TFPais_Nome);
         Ddo_pais_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "FilteredText_set", Ddo_pais_nome_Filteredtext_set);
         AV60TFPais_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFPais_Nome_Sel", AV60TFPais_Nome_Sel);
         Ddo_pais_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SelectedValue_set", Ddo_pais_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "PAIS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Pais_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pais_Nome1", AV17Pais_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV93Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV93Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV32Session.Get(AV93Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV95GXV2 = 1;
         while ( AV95GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV95GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPAIS_CODIGO") == 0 )
            {
               AV55TFPais_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPais_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0)));
               AV56TFPais_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFPais_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0)));
               if ( ! (0==AV55TFPais_Codigo) )
               {
                  Ddo_pais_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "FilteredText_set", Ddo_pais_codigo_Filteredtext_set);
               }
               if ( ! (0==AV56TFPais_Codigo_To) )
               {
                  Ddo_pais_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_codigo_Internalname, "FilteredTextTo_set", Ddo_pais_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME") == 0 )
            {
               AV59TFPais_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFPais_Nome", AV59TFPais_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFPais_Nome)) )
               {
                  Ddo_pais_nome_Filteredtext_set = AV59TFPais_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "FilteredText_set", Ddo_pais_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME_SEL") == 0 )
            {
               AV60TFPais_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFPais_Nome_Sel", AV60TFPais_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFPais_Nome_Sel)) )
               {
                  Ddo_pais_nome_Selectedvalue_set = AV60TFPais_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SelectedValue_set", Ddo_pais_nome_Selectedvalue_set);
               }
            }
            AV95GXV2 = (int)(AV95GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Pais_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pais_Nome1", AV17Pais_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PAIS_NOME") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Pais_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Pais_Nome2", AV21Pais_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PAIS_NOME") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Pais_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome3", AV25Pais_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV32Session.Get(AV93Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV55TFPais_Codigo) && (0==AV56TFPais_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPAIS_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFPais_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFPais_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFPais_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPAIS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFPais_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFPais_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPAIS_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFPais_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV93Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Pais_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Pais_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Pais_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Pais_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Pais_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Pais_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV93Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Pais";
         AV32Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_122( true) ;
         }
         else
         {
            wb_table2_8_122( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_122( true) ;
         }
         else
         {
            wb_table3_82_122( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_122e( true) ;
         }
         else
         {
            wb_table1_2_122e( false) ;
         }
      }

      protected void wb_table3_82_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_122( true) ;
         }
         else
         {
            wb_table4_85_122( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_122e( true) ;
         }
         else
         {
            wb_table3_82_122e( false) ;
         }
      }

      protected void wb_table4_85_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPais_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtPais_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPais_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPais_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPais_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPais_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV66Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPais_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPais_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A22Pais_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPais_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPais_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtPais_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_122e( true) ;
         }
         else
         {
            wb_table4_85_122e( false) ;
         }
      }

      protected void wb_table2_8_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_122( true) ;
         }
         else
         {
            wb_table5_11_122( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_122( true) ;
         }
         else
         {
            wb_table6_23_122( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_122e( true) ;
         }
         else
         {
            wb_table2_8_122e( false) ;
         }
      }

      protected void wb_table6_23_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_122( true) ;
         }
         else
         {
            wb_table7_28_122( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_122e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_122e( true) ;
         }
         else
         {
            wb_table6_23_122e( false) ;
         }
      }

      protected void wb_table7_28_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_122( true) ;
         }
         else
         {
            wb_table8_37_122( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_122( true) ;
         }
         else
         {
            wb_table9_54_122( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_122( true) ;
         }
         else
         {
            wb_table10_71_122( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_122e( true) ;
         }
         else
         {
            wb_table7_28_122e( false) ;
         }
      }

      protected void wb_table10_71_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome3_Internalname, StringUtil.RTrim( AV25Pais_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25Pais_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_122e( true) ;
         }
         else
         {
            wb_table10_71_122e( false) ;
         }
      }

      protected void wb_table9_54_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome2_Internalname, StringUtil.RTrim( AV21Pais_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21Pais_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_122e( true) ;
         }
         else
         {
            wb_table9_54_122e( false) ;
         }
      }

      protected void wb_table8_37_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWPais.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome1_Internalname, StringUtil.RTrim( AV17Pais_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Pais_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_122e( true) ;
         }
         else
         {
            wb_table8_37_122e( false) ;
         }
      }

      protected void wb_table5_11_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaistitle_Internalname, "Paises", "", "", lblPaistitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWPais.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWPais.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_122e( true) ;
         }
         else
         {
            wb_table5_11_122e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA122( ) ;
         WS122( ) ;
         WE122( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117305130");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwpais.js", "?20203117305130");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_idx;
         edtPais_Codigo_Internalname = "PAIS_CODIGO_"+sGXsfl_88_idx;
         edtPais_Nome_Internalname = "PAIS_NOME_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_fel_idx;
         edtPais_Codigo_Internalname = "PAIS_CODIGO_"+sGXsfl_88_fel_idx;
         edtPais_Nome_Internalname = "PAIS_NOME_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WB120( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV90Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV90Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV91Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV66Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV66Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV92Display_GXI : context.PathToRelativeUrl( AV66Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV66Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPais_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPais_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPais_Nome_Internalname,StringUtil.RTrim( A22Pais_Nome),StringUtil.RTrim( context.localUtil.Format( A22Pais_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtPais_Nome_Link,(String)"",(String)"",(String)"",(String)edtPais_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_PAIS_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PAIS_NOME"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A22Pais_Nome, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblPaistitle_Internalname = "PAISTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavPais_nome1_Internalname = "vPAIS_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavPais_nome2_Internalname = "vPAIS_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavPais_nome3_Internalname = "vPAIS_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtPais_Codigo_Internalname = "PAIS_CODIGO";
         edtPais_Nome_Internalname = "PAIS_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfpais_codigo_Internalname = "vTFPAIS_CODIGO";
         edtavTfpais_codigo_to_Internalname = "vTFPAIS_CODIGO_TO";
         edtavTfpais_nome_Internalname = "vTFPAIS_NOME";
         edtavTfpais_nome_sel_Internalname = "vTFPAIS_NOME_SEL";
         Ddo_pais_codigo_Internalname = "DDO_PAIS_CODIGO";
         edtavDdo_pais_codigotitlecontrolidtoreplace_Internalname = "vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_pais_nome_Internalname = "DDO_PAIS_NOME";
         edtavDdo_pais_nometitlecontrolidtoreplace_Internalname = "vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtPais_Nome_Jsonclick = "";
         edtPais_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavPais_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavPais_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavPais_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtPais_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtPais_Nome_Titleformat = 0;
         edtPais_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavPais_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavPais_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavPais_nome1_Visible = 1;
         edtPais_Nome_Title = "Pa�s";
         edtPais_Codigo_Title = "C�d. Pa�s";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_pais_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pais_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfpais_nome_sel_Jsonclick = "";
         edtavTfpais_nome_sel_Visible = 1;
         edtavTfpais_nome_Jsonclick = "";
         edtavTfpais_nome_Visible = 1;
         edtavTfpais_codigo_to_Jsonclick = "";
         edtavTfpais_codigo_to_Visible = 1;
         edtavTfpais_codigo_Jsonclick = "";
         edtavTfpais_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_pais_nome_Searchbuttontext = "Pesquisar";
         Ddo_pais_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pais_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_pais_nome_Loadingdata = "Carregando dados...";
         Ddo_pais_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_pais_nome_Sortasc = "Ordenar de A � Z";
         Ddo_pais_nome_Datalistupdateminimumcharacters = 0;
         Ddo_pais_nome_Datalistproc = "GetWWPaisFilterData";
         Ddo_pais_nome_Datalisttype = "Dynamic";
         Ddo_pais_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pais_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pais_nome_Filtertype = "Character";
         Ddo_pais_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_pais_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pais_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pais_nome_Titlecontrolidtoreplace = "";
         Ddo_pais_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pais_nome_Cls = "ColumnSettings";
         Ddo_pais_nome_Tooltip = "Op��es";
         Ddo_pais_nome_Caption = "";
         Ddo_pais_codigo_Searchbuttontext = "Pesquisar";
         Ddo_pais_codigo_Rangefilterto = "At�";
         Ddo_pais_codigo_Rangefilterfrom = "Desde";
         Ddo_pais_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_pais_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_pais_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_pais_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pais_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pais_codigo_Filtertype = "Numeric";
         Ddo_pais_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_pais_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pais_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pais_codigo_Titlecontrolidtoreplace = "";
         Ddo_pais_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pais_codigo_Cls = "ColumnSettings";
         Ddo_pais_codigo_Tooltip = "Op��es";
         Ddo_pais_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Pais";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV54Pais_CodigoTitleFilterData',fld:'vPAIS_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58Pais_NomeTitleFilterData',fld:'vPAIS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtPais_Codigo_Titleformat',ctrl:'PAIS_CODIGO',prop:'Titleformat'},{av:'edtPais_Codigo_Title',ctrl:'PAIS_CODIGO',prop:'Title'},{av:'edtPais_Nome_Titleformat',ctrl:'PAIS_NOME',prop:'Titleformat'},{av:'edtPais_Nome_Title',ctrl:'PAIS_NOME',prop:'Title'},{av:'AV64GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV65GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV71ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PAIS_CODIGO.ONOPTIONCLICKED","{handler:'E13122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pais_codigo_Activeeventkey',ctrl:'DDO_PAIS_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_pais_codigo_Filteredtext_get',ctrl:'DDO_PAIS_CODIGO',prop:'FilteredText_get'},{av:'Ddo_pais_codigo_Filteredtextto_get',ctrl:'DDO_PAIS_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pais_codigo_Sortedstatus',ctrl:'DDO_PAIS_CODIGO',prop:'SortedStatus'},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PAIS_NOME.ONOPTIONCLICKED","{handler:'E14122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pais_nome_Activeeventkey',ctrl:'DDO_PAIS_NOME',prop:'ActiveEventKey'},{av:'Ddo_pais_nome_Filteredtext_get',ctrl:'DDO_PAIS_NOME',prop:'FilteredText_get'},{av:'Ddo_pais_nome_Selectedvalue_get',ctrl:'DDO_PAIS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pais_codigo_Sortedstatus',ctrl:'DDO_PAIS_CODIGO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27122',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV66Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtPais_Nome_Link',ctrl:'PAIS_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20122',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21122',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22122',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23122',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24122',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11122',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV57ddo_Pais_CodigoTitleControlIdToReplace',fld:'vDDO_PAIS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV93Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV67ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV55TFPais_Codigo',fld:'vTFPAIS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pais_codigo_Filteredtext_set',ctrl:'DDO_PAIS_CODIGO',prop:'FilteredText_set'},{av:'AV56TFPais_Codigo_To',fld:'vTFPAIS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_pais_codigo_Filteredtextto_set',ctrl:'DDO_PAIS_CODIGO',prop:'FilteredTextTo_set'},{av:'AV59TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'Ddo_pais_nome_Filteredtext_set',ctrl:'DDO_PAIS_NOME',prop:'FilteredText_set'},{av:'AV60TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pais_nome_Selectedvalue_set',ctrl:'DDO_PAIS_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'Ddo_pais_codigo_Sortedstatus',ctrl:'DDO_PAIS_CODIGO',prop:'SortedStatus'},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19122',iparms:[{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_pais_codigo_Activeeventkey = "";
         Ddo_pais_codigo_Filteredtext_get = "";
         Ddo_pais_codigo_Filteredtextto_get = "";
         Ddo_pais_nome_Activeeventkey = "";
         Ddo_pais_nome_Filteredtext_get = "";
         Ddo_pais_nome_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Pais_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Pais_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Pais_Nome3 = "";
         AV59TFPais_Nome = "";
         AV60TFPais_Nome_Sel = "";
         AV57ddo_Pais_CodigoTitleControlIdToReplace = "";
         AV61ddo_Pais_NomeTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV93Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV71ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV54Pais_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Pais_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_pais_codigo_Filteredtext_set = "";
         Ddo_pais_codigo_Filteredtextto_set = "";
         Ddo_pais_codigo_Sortedstatus = "";
         Ddo_pais_nome_Filteredtext_set = "";
         Ddo_pais_nome_Selectedvalue_set = "";
         Ddo_pais_nome_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV90Update_GXI = "";
         AV29Delete = "";
         AV91Delete_GXI = "";
         AV66Display = "";
         AV92Display_GXI = "";
         A22Pais_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV77WWPaisDS_3_Pais_nome1 = "";
         lV81WWPaisDS_7_Pais_nome2 = "";
         lV85WWPaisDS_11_Pais_nome3 = "";
         lV88WWPaisDS_14_Tfpais_nome = "";
         AV75WWPaisDS_1_Dynamicfiltersselector1 = "";
         AV77WWPaisDS_3_Pais_nome1 = "";
         AV79WWPaisDS_5_Dynamicfiltersselector2 = "";
         AV81WWPaisDS_7_Pais_nome2 = "";
         AV83WWPaisDS_9_Dynamicfiltersselector3 = "";
         AV85WWPaisDS_11_Pais_nome3 = "";
         AV89WWPaisDS_15_Tfpais_nome_sel = "";
         AV88WWPaisDS_14_Tfpais_nome = "";
         H00122_A22Pais_Nome = new String[] {""} ;
         H00122_A21Pais_Codigo = new int[1] ;
         H00123_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV68ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV72ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV69ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV70ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV32Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblPaistitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwpais__default(),
            new Object[][] {
                new Object[] {
               H00122_A22Pais_Nome, H00122_A21Pais_Codigo
               }
               , new Object[] {
               H00123_AGRID_nRecordCount
               }
            }
         );
         AV93Pgmname = "WWPais";
         /* GeneXus formulas. */
         AV93Pgmname = "WWPais";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV67ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV76WWPaisDS_2_Dynamicfiltersoperator1 ;
      private short AV80WWPaisDS_6_Dynamicfiltersoperator2 ;
      private short AV84WWPaisDS_10_Dynamicfiltersoperator3 ;
      private short edtPais_Codigo_Titleformat ;
      private short edtPais_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV55TFPais_Codigo ;
      private int AV56TFPais_Codigo_To ;
      private int A21Pais_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_pais_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfpais_codigo_Visible ;
      private int edtavTfpais_codigo_to_Visible ;
      private int edtavTfpais_nome_Visible ;
      private int edtavTfpais_nome_sel_Visible ;
      private int edtavDdo_pais_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pais_nometitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV86WWPaisDS_12_Tfpais_codigo ;
      private int AV87WWPaisDS_13_Tfpais_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV63PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavPais_nome1_Visible ;
      private int edtavPais_nome2_Visible ;
      private int edtavPais_nome3_Visible ;
      private int AV94GXV1 ;
      private int AV95GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV64GridCurrentPage ;
      private long AV65GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_pais_codigo_Activeeventkey ;
      private String Ddo_pais_codigo_Filteredtext_get ;
      private String Ddo_pais_codigo_Filteredtextto_get ;
      private String Ddo_pais_nome_Activeeventkey ;
      private String Ddo_pais_nome_Filteredtext_get ;
      private String Ddo_pais_nome_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV17Pais_Nome1 ;
      private String AV21Pais_Nome2 ;
      private String AV25Pais_Nome3 ;
      private String AV59TFPais_Nome ;
      private String AV60TFPais_Nome_Sel ;
      private String AV93Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_pais_codigo_Caption ;
      private String Ddo_pais_codigo_Tooltip ;
      private String Ddo_pais_codigo_Cls ;
      private String Ddo_pais_codigo_Filteredtext_set ;
      private String Ddo_pais_codigo_Filteredtextto_set ;
      private String Ddo_pais_codigo_Dropdownoptionstype ;
      private String Ddo_pais_codigo_Titlecontrolidtoreplace ;
      private String Ddo_pais_codigo_Sortedstatus ;
      private String Ddo_pais_codigo_Filtertype ;
      private String Ddo_pais_codigo_Sortasc ;
      private String Ddo_pais_codigo_Sortdsc ;
      private String Ddo_pais_codigo_Cleanfilter ;
      private String Ddo_pais_codigo_Rangefilterfrom ;
      private String Ddo_pais_codigo_Rangefilterto ;
      private String Ddo_pais_codigo_Searchbuttontext ;
      private String Ddo_pais_nome_Caption ;
      private String Ddo_pais_nome_Tooltip ;
      private String Ddo_pais_nome_Cls ;
      private String Ddo_pais_nome_Filteredtext_set ;
      private String Ddo_pais_nome_Selectedvalue_set ;
      private String Ddo_pais_nome_Dropdownoptionstype ;
      private String Ddo_pais_nome_Titlecontrolidtoreplace ;
      private String Ddo_pais_nome_Sortedstatus ;
      private String Ddo_pais_nome_Filtertype ;
      private String Ddo_pais_nome_Datalisttype ;
      private String Ddo_pais_nome_Datalistproc ;
      private String Ddo_pais_nome_Sortasc ;
      private String Ddo_pais_nome_Sortdsc ;
      private String Ddo_pais_nome_Loadingdata ;
      private String Ddo_pais_nome_Cleanfilter ;
      private String Ddo_pais_nome_Noresultsfound ;
      private String Ddo_pais_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfpais_codigo_Internalname ;
      private String edtavTfpais_codigo_Jsonclick ;
      private String edtavTfpais_codigo_to_Internalname ;
      private String edtavTfpais_codigo_to_Jsonclick ;
      private String edtavTfpais_nome_Internalname ;
      private String edtavTfpais_nome_Jsonclick ;
      private String edtavTfpais_nome_sel_Internalname ;
      private String edtavTfpais_nome_sel_Jsonclick ;
      private String edtavDdo_pais_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pais_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtPais_Codigo_Internalname ;
      private String A22Pais_Nome ;
      private String edtPais_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV77WWPaisDS_3_Pais_nome1 ;
      private String lV81WWPaisDS_7_Pais_nome2 ;
      private String lV85WWPaisDS_11_Pais_nome3 ;
      private String lV88WWPaisDS_14_Tfpais_nome ;
      private String AV77WWPaisDS_3_Pais_nome1 ;
      private String AV81WWPaisDS_7_Pais_nome2 ;
      private String AV85WWPaisDS_11_Pais_nome3 ;
      private String AV89WWPaisDS_15_Tfpais_nome_sel ;
      private String AV88WWPaisDS_14_Tfpais_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavPais_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavPais_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavPais_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_pais_codigo_Internalname ;
      private String Ddo_pais_nome_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtPais_Codigo_Title ;
      private String edtPais_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtPais_Nome_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavPais_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavPais_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavPais_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblPaistitle_Internalname ;
      private String lblPaistitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtPais_Codigo_Jsonclick ;
      private String edtPais_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_pais_codigo_Includesortasc ;
      private bool Ddo_pais_codigo_Includesortdsc ;
      private bool Ddo_pais_codigo_Includefilter ;
      private bool Ddo_pais_codigo_Filterisrange ;
      private bool Ddo_pais_codigo_Includedatalist ;
      private bool Ddo_pais_nome_Includesortasc ;
      private bool Ddo_pais_nome_Includesortdsc ;
      private bool Ddo_pais_nome_Includefilter ;
      private bool Ddo_pais_nome_Filterisrange ;
      private bool Ddo_pais_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV78WWPaisDS_4_Dynamicfiltersenabled2 ;
      private bool AV82WWPaisDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV66Display_IsBlob ;
      private String AV68ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV57ddo_Pais_CodigoTitleControlIdToReplace ;
      private String AV61ddo_Pais_NomeTitleControlIdToReplace ;
      private String AV90Update_GXI ;
      private String AV91Delete_GXI ;
      private String AV92Display_GXI ;
      private String AV75WWPaisDS_1_Dynamicfiltersselector1 ;
      private String AV79WWPaisDS_5_Dynamicfiltersselector2 ;
      private String AV83WWPaisDS_9_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV66Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV32Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00122_A22Pais_Nome ;
      private int[] H00122_A21Pais_Codigo ;
      private long[] H00123_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV71ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Pais_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Pais_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV69ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV72ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV62DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV70ManageFiltersItem ;
   }

   public class wwpais__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00122( IGxContext context ,
                                             String AV75WWPaisDS_1_Dynamicfiltersselector1 ,
                                             short AV76WWPaisDS_2_Dynamicfiltersoperator1 ,
                                             String AV77WWPaisDS_3_Pais_nome1 ,
                                             bool AV78WWPaisDS_4_Dynamicfiltersenabled2 ,
                                             String AV79WWPaisDS_5_Dynamicfiltersselector2 ,
                                             short AV80WWPaisDS_6_Dynamicfiltersoperator2 ,
                                             String AV81WWPaisDS_7_Pais_nome2 ,
                                             bool AV82WWPaisDS_8_Dynamicfiltersenabled3 ,
                                             String AV83WWPaisDS_9_Dynamicfiltersselector3 ,
                                             short AV84WWPaisDS_10_Dynamicfiltersoperator3 ,
                                             String AV85WWPaisDS_11_Pais_nome3 ,
                                             int AV86WWPaisDS_12_Tfpais_codigo ,
                                             int AV87WWPaisDS_13_Tfpais_codigo_to ,
                                             String AV89WWPaisDS_15_Tfpais_nome_sel ,
                                             String AV88WWPaisDS_14_Tfpais_nome ,
                                             String A22Pais_Nome ,
                                             int A21Pais_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Pais_Nome], [Pais_Codigo]";
         sFromString = " FROM [Pais] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV75WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV76WWPaisDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV77WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV77WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV75WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV76WWPaisDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV77WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV77WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV78WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV79WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV80WWPaisDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV81WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV81WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV78WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV79WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV80WWPaisDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV81WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV81WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV82WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV84WWPaisDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV85WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV85WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV82WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV84WWPaisDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV85WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV85WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV86WWPaisDS_12_Tfpais_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] >= @AV86WWPaisDS_12_Tfpais_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] >= @AV86WWPaisDS_12_Tfpais_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV87WWPaisDS_13_Tfpais_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] <= @AV87WWPaisDS_13_Tfpais_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] <= @AV87WWPaisDS_13_Tfpais_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV89WWPaisDS_15_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPaisDS_14_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV88WWPaisDS_14_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV88WWPaisDS_14_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWPaisDS_15_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] = @AV89WWPaisDS_15_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] = @AV89WWPaisDS_15_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Pais_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Pais_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Pais_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Pais_Codigo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Pais_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00123( IGxContext context ,
                                             String AV75WWPaisDS_1_Dynamicfiltersselector1 ,
                                             short AV76WWPaisDS_2_Dynamicfiltersoperator1 ,
                                             String AV77WWPaisDS_3_Pais_nome1 ,
                                             bool AV78WWPaisDS_4_Dynamicfiltersenabled2 ,
                                             String AV79WWPaisDS_5_Dynamicfiltersselector2 ,
                                             short AV80WWPaisDS_6_Dynamicfiltersoperator2 ,
                                             String AV81WWPaisDS_7_Pais_nome2 ,
                                             bool AV82WWPaisDS_8_Dynamicfiltersenabled3 ,
                                             String AV83WWPaisDS_9_Dynamicfiltersselector3 ,
                                             short AV84WWPaisDS_10_Dynamicfiltersoperator3 ,
                                             String AV85WWPaisDS_11_Pais_nome3 ,
                                             int AV86WWPaisDS_12_Tfpais_codigo ,
                                             int AV87WWPaisDS_13_Tfpais_codigo_to ,
                                             String AV89WWPaisDS_15_Tfpais_nome_sel ,
                                             String AV88WWPaisDS_14_Tfpais_nome ,
                                             String A22Pais_Nome ,
                                             int A21Pais_Codigo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [10] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Pais] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV75WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV76WWPaisDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV77WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV77WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV75WWPaisDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV76WWPaisDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWPaisDS_3_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV77WWPaisDS_3_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV77WWPaisDS_3_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV78WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV79WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV80WWPaisDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV81WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV81WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV78WWPaisDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV79WWPaisDS_5_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV80WWPaisDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWPaisDS_7_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV81WWPaisDS_7_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV81WWPaisDS_7_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV82WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV84WWPaisDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV85WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV85WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV82WWPaisDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWPaisDS_9_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV84WWPaisDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPaisDS_11_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like '%' + @lV85WWPaisDS_11_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like '%' + @lV85WWPaisDS_11_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV86WWPaisDS_12_Tfpais_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] >= @AV86WWPaisDS_12_Tfpais_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] >= @AV86WWPaisDS_12_Tfpais_codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV87WWPaisDS_13_Tfpais_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Codigo] <= @AV87WWPaisDS_13_Tfpais_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Codigo] <= @AV87WWPaisDS_13_Tfpais_codigo_to)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV89WWPaisDS_15_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPaisDS_14_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] like @lV88WWPaisDS_14_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] like @lV88WWPaisDS_14_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWPaisDS_15_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Pais_Nome] = @AV89WWPaisDS_15_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Pais_Nome] = @AV89WWPaisDS_15_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00122(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
               case 1 :
                     return conditional_H00123(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00122 ;
          prmH00122 = new Object[] {
          new Object[] {"@lV77WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV77WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV81WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV81WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV85WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV85WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV86WWPaisDS_12_Tfpais_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWPaisDS_13_Tfpais_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV88WWPaisDS_14_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWPaisDS_15_Tfpais_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00123 ;
          prmH00123 = new Object[] {
          new Object[] {"@lV77WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV77WWPaisDS_3_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV81WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV81WWPaisDS_7_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV85WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV85WWPaisDS_11_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV86WWPaisDS_12_Tfpais_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWPaisDS_13_Tfpais_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV88WWPaisDS_14_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWPaisDS_15_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00122", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00122,11,0,true,false )
             ,new CursorDef("H00123", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00123,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
