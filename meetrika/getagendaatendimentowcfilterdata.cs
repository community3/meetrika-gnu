/*
               File: GetAgendaAtendimentoWCFilterData
        Description: Get Agenda Atendimento WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:37.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getagendaatendimentowcfilterdata : GXProcedure
   {
      public getagendaatendimentowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getagendaatendimentowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getagendaatendimentowcfilterdata objgetagendaatendimentowcfilterdata;
         objgetagendaatendimentowcfilterdata = new getagendaatendimentowcfilterdata();
         objgetagendaatendimentowcfilterdata.AV18DDOName = aP0_DDOName;
         objgetagendaatendimentowcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetagendaatendimentowcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetagendaatendimentowcfilterdata.AV22OptionsJson = "" ;
         objgetagendaatendimentowcfilterdata.AV25OptionsDescJson = "" ;
         objgetagendaatendimentowcfilterdata.AV27OptionIndexesJson = "" ;
         objgetagendaatendimentowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetagendaatendimentowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetagendaatendimentowcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getagendaatendimentowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("AgendaAtendimentoWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "AgendaAtendimentoWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("AgendaAtendimentoWCGridState"), "");
         }
         AV38GXV1 = 1;
         while ( AV38GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV38GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV10TFContagemResultado_Demanda = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV11TFContagemResultado_Demanda_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV12TFContagemResultado_DemandaFM = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV13TFContagemResultado_DemandaFM_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_QTDUND") == 0 )
            {
               AV14TFAgendaAtendimento_QtdUnd = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&AGENDAATENDIMENTO_CNTSRCCOD") == 0 )
            {
               AV34AgendaAtendimento_CntSrcCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&AGENDAATENDIMENTO_DATA") == 0 )
            {
               AV35AgendaAtendimento_Data = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
            }
            AV38GXV1 = (int)(AV38GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV10TFContagemResultado_Demanda = AV16SearchTxt;
         AV11TFContagemResultado_Demanda_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContagemResultado_Demanda_Sel ,
                                              AV10TFContagemResultado_Demanda ,
                                              AV13TFContagemResultado_DemandaFM_Sel ,
                                              AV12TFContagemResultado_DemandaFM ,
                                              AV14TFAgendaAtendimento_QtdUnd ,
                                              AV15TFAgendaAtendimento_QtdUnd_To ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              AV34AgendaAtendimento_CntSrcCod ,
                                              AV35AgendaAtendimento_Data ,
                                              A1183AgendaAtendimento_CntSrcCod ,
                                              A1184AgendaAtendimento_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV10TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_Demanda), "%", "");
         lV12TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_DemandaFM), "%", "");
         /* Using cursor P00Q02 */
         pr_default.execute(0, new Object[] {AV34AgendaAtendimento_CntSrcCod, AV35AgendaAtendimento_Data, lV10TFContagemResultado_Demanda, AV11TFContagemResultado_Demanda_Sel, lV12TFContagemResultado_DemandaFM, AV13TFContagemResultado_DemandaFM_Sel, AV14TFAgendaAtendimento_QtdUnd, AV15TFAgendaAtendimento_QtdUnd_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQ02 = false;
            A1209AgendaAtendimento_CodDmn = P00Q02_A1209AgendaAtendimento_CodDmn[0];
            A1183AgendaAtendimento_CntSrcCod = P00Q02_A1183AgendaAtendimento_CntSrcCod[0];
            A1184AgendaAtendimento_Data = P00Q02_A1184AgendaAtendimento_Data[0];
            A457ContagemResultado_Demanda = P00Q02_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00Q02_n457ContagemResultado_Demanda[0];
            A1186AgendaAtendimento_QtdUnd = P00Q02_A1186AgendaAtendimento_QtdUnd[0];
            A493ContagemResultado_DemandaFM = P00Q02_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00Q02_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00Q02_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00Q02_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00Q02_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00Q02_n493ContagemResultado_DemandaFM[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00Q02_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( P00Q02_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( StringUtil.StrCmp(P00Q02_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKQ02 = false;
               A1209AgendaAtendimento_CodDmn = P00Q02_A1209AgendaAtendimento_CodDmn[0];
               AV28count = (long)(AV28count+1);
               BRKQ02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV20Option = A457ContagemResultado_Demanda;
               AV23OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV21Options.Add(AV20Option, 0);
               AV24OptionsDesc.Add(AV23OptionDesc, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ02 )
            {
               BRKQ02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV12TFContagemResultado_DemandaFM = AV16SearchTxt;
         AV13TFContagemResultado_DemandaFM_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFContagemResultado_Demanda_Sel ,
                                              AV10TFContagemResultado_Demanda ,
                                              AV13TFContagemResultado_DemandaFM_Sel ,
                                              AV12TFContagemResultado_DemandaFM ,
                                              AV14TFAgendaAtendimento_QtdUnd ,
                                              AV15TFAgendaAtendimento_QtdUnd_To ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              AV34AgendaAtendimento_CntSrcCod ,
                                              AV35AgendaAtendimento_Data ,
                                              A1183AgendaAtendimento_CntSrcCod ,
                                              A1184AgendaAtendimento_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV10TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_Demanda), "%", "");
         lV12TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_DemandaFM), "%", "");
         /* Using cursor P00Q03 */
         pr_default.execute(1, new Object[] {AV34AgendaAtendimento_CntSrcCod, AV35AgendaAtendimento_Data, lV10TFContagemResultado_Demanda, AV11TFContagemResultado_Demanda_Sel, lV12TFContagemResultado_DemandaFM, AV13TFContagemResultado_DemandaFM_Sel, AV14TFAgendaAtendimento_QtdUnd, AV15TFAgendaAtendimento_QtdUnd_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQ04 = false;
            A1209AgendaAtendimento_CodDmn = P00Q03_A1209AgendaAtendimento_CodDmn[0];
            A1183AgendaAtendimento_CntSrcCod = P00Q03_A1183AgendaAtendimento_CntSrcCod[0];
            A1184AgendaAtendimento_Data = P00Q03_A1184AgendaAtendimento_Data[0];
            A493ContagemResultado_DemandaFM = P00Q03_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00Q03_n493ContagemResultado_DemandaFM[0];
            A1186AgendaAtendimento_QtdUnd = P00Q03_A1186AgendaAtendimento_QtdUnd[0];
            A457ContagemResultado_Demanda = P00Q03_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00Q03_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00Q03_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00Q03_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00Q03_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00Q03_n457ContagemResultado_Demanda[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00Q03_A1183AgendaAtendimento_CntSrcCod[0] == A1183AgendaAtendimento_CntSrcCod ) && ( P00Q03_A1184AgendaAtendimento_Data[0] == A1184AgendaAtendimento_Data ) && ( StringUtil.StrCmp(P00Q03_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKQ04 = false;
               A1209AgendaAtendimento_CodDmn = P00Q03_A1209AgendaAtendimento_CodDmn[0];
               AV28count = (long)(AV28count+1);
               BRKQ04 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV20Option = A493ContagemResultado_DemandaFM;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ04 )
            {
               BRKQ04 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_Demanda = "";
         AV11TFContagemResultado_Demanda_Sel = "";
         AV12TFContagemResultado_DemandaFM = "";
         AV13TFContagemResultado_DemandaFM_Sel = "";
         AV35AgendaAtendimento_Data = DateTime.MinValue;
         scmdbuf = "";
         lV10TFContagemResultado_Demanda = "";
         lV12TFContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         P00Q02_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P00Q02_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         P00Q02_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P00Q02_A457ContagemResultado_Demanda = new String[] {""} ;
         P00Q02_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00Q02_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         P00Q02_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00Q02_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         AV20Option = "";
         AV23OptionDesc = "";
         P00Q03_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P00Q03_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         P00Q03_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P00Q03_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00Q03_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00Q03_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         P00Q03_A457ContagemResultado_Demanda = new String[] {""} ;
         P00Q03_n457ContagemResultado_Demanda = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getagendaatendimentowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00Q02_A1209AgendaAtendimento_CodDmn, P00Q02_A1183AgendaAtendimento_CntSrcCod, P00Q02_A1184AgendaAtendimento_Data, P00Q02_A457ContagemResultado_Demanda, P00Q02_n457ContagemResultado_Demanda, P00Q02_A1186AgendaAtendimento_QtdUnd, P00Q02_A493ContagemResultado_DemandaFM, P00Q02_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               P00Q03_A1209AgendaAtendimento_CodDmn, P00Q03_A1183AgendaAtendimento_CntSrcCod, P00Q03_A1184AgendaAtendimento_Data, P00Q03_A493ContagemResultado_DemandaFM, P00Q03_n493ContagemResultado_DemandaFM, P00Q03_A1186AgendaAtendimento_QtdUnd, P00Q03_A457ContagemResultado_Demanda, P00Q03_n457ContagemResultado_Demanda
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV38GXV1 ;
      private int AV34AgendaAtendimento_CntSrcCod ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private int A1209AgendaAtendimento_CodDmn ;
      private long AV28count ;
      private decimal AV14TFAgendaAtendimento_QtdUnd ;
      private decimal AV15TFAgendaAtendimento_QtdUnd_To ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private String scmdbuf ;
      private DateTime AV35AgendaAtendimento_Data ;
      private DateTime A1184AgendaAtendimento_Data ;
      private bool returnInSub ;
      private bool BRKQ02 ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool BRKQ04 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV10TFContagemResultado_Demanda ;
      private String AV11TFContagemResultado_Demanda_Sel ;
      private String AV12TFContagemResultado_DemandaFM ;
      private String AV13TFContagemResultado_DemandaFM_Sel ;
      private String lV10TFContagemResultado_Demanda ;
      private String lV12TFContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV20Option ;
      private String AV23OptionDesc ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Q02_A1209AgendaAtendimento_CodDmn ;
      private int[] P00Q02_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] P00Q02_A1184AgendaAtendimento_Data ;
      private String[] P00Q02_A457ContagemResultado_Demanda ;
      private bool[] P00Q02_n457ContagemResultado_Demanda ;
      private decimal[] P00Q02_A1186AgendaAtendimento_QtdUnd ;
      private String[] P00Q02_A493ContagemResultado_DemandaFM ;
      private bool[] P00Q02_n493ContagemResultado_DemandaFM ;
      private int[] P00Q03_A1209AgendaAtendimento_CodDmn ;
      private int[] P00Q03_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] P00Q03_A1184AgendaAtendimento_Data ;
      private String[] P00Q03_A493ContagemResultado_DemandaFM ;
      private bool[] P00Q03_n493ContagemResultado_DemandaFM ;
      private decimal[] P00Q03_A1186AgendaAtendimento_QtdUnd ;
      private String[] P00Q03_A457ContagemResultado_Demanda ;
      private bool[] P00Q03_n457ContagemResultado_Demanda ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getagendaatendimentowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Q02( IGxContext context ,
                                             String AV11TFContagemResultado_Demanda_Sel ,
                                             String AV10TFContagemResultado_Demanda ,
                                             String AV13TFContagemResultado_DemandaFM_Sel ,
                                             String AV12TFContagemResultado_DemandaFM ,
                                             decimal AV14TFAgendaAtendimento_QtdUnd ,
                                             decimal AV15TFAgendaAtendimento_QtdUnd_To ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             int AV34AgendaAtendimento_CntSrcCod ,
                                             DateTime AV35AgendaAtendimento_Data ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             DateTime A1184AgendaAtendimento_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_Demanda], T1.[AgendaAtendimento_QtdUnd], T2.[ContagemResultado_DemandaFM] FROM ([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn])";
         scmdbuf = scmdbuf + " WHERE (T1.[AgendaAtendimento_CntSrcCod] = @AV34AgendaAtendimento_CntSrcCod and T1.[AgendaAtendimento_Data] = @AV35AgendaAtendimento_Data)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV10TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV11TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV12TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV13TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV14TFAgendaAtendimento_QtdUnd) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV14TFAgendaAtendimento_QtdUnd)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV15TFAgendaAtendimento_QtdUnd_To) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV15TFAgendaAtendimento_QtdUnd_To)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_Demanda]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00Q03( IGxContext context ,
                                             String AV11TFContagemResultado_Demanda_Sel ,
                                             String AV10TFContagemResultado_Demanda ,
                                             String AV13TFContagemResultado_DemandaFM_Sel ,
                                             String AV12TFContagemResultado_DemandaFM ,
                                             decimal AV14TFAgendaAtendimento_QtdUnd ,
                                             decimal AV15TFAgendaAtendimento_QtdUnd_To ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             int AV34AgendaAtendimento_CntSrcCod ,
                                             DateTime AV35AgendaAtendimento_Data ,
                                             int A1183AgendaAtendimento_CntSrcCod ,
                                             DateTime A1184AgendaAtendimento_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_DemandaFM], T1.[AgendaAtendimento_QtdUnd], T2.[ContagemResultado_Demanda] FROM ([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn])";
         scmdbuf = scmdbuf + " WHERE (T1.[AgendaAtendimento_CntSrcCod] = @AV34AgendaAtendimento_CntSrcCod and T1.[AgendaAtendimento_Data] = @AV35AgendaAtendimento_Data)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV10TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV11TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV12TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV13TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV14TFAgendaAtendimento_QtdUnd) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV14TFAgendaAtendimento_QtdUnd)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV15TFAgendaAtendimento_QtdUnd_To) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV15TFAgendaAtendimento_QtdUnd_To)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], T2.[ContagemResultado_DemandaFM]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Q02(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (int)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] );
               case 1 :
                     return conditional_P00Q03(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (int)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Q02 ;
          prmP00Q02 = new Object[] {
          new Object[] {"@AV34AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV10TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV12TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFAgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV15TFAgendaAtendimento_QtdUnd_To",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00Q03 ;
          prmP00Q03 = new Object[] {
          new Object[] {"@AV34AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35AgendaAtendimento_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV10TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV11TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV12TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFAgendaAtendimento_QtdUnd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV15TFAgendaAtendimento_QtdUnd_To",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Q02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q02,100,0,true,false )
             ,new CursorDef("P00Q03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q03,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getagendaatendimentowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getagendaatendimentowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getagendaatendimentowcfilterdata") )
          {
             return  ;
          }
          getagendaatendimentowcfilterdata worker = new getagendaatendimentowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
