/*
               File: type_SdtAudit
        Description: Audit
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:19.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Audit" )]
   [XmlType(TypeName =  "Audit" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtAudit : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAudit( )
      {
         /* Constructor for serialization */
         gxTv_SdtAudit_Auditdate = (DateTime)(DateTime.MinValue);
         gxTv_SdtAudit_Audittablename = "";
         gxTv_SdtAudit_Auditdescription = "";
         gxTv_SdtAudit_Auditshortdescription = "";
         gxTv_SdtAudit_Usuario_pessoanom = "";
         gxTv_SdtAudit_Usuario_nome = "";
         gxTv_SdtAudit_Auditaction = "";
         gxTv_SdtAudit_Mode = "";
         gxTv_SdtAudit_Auditdate_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtAudit_Audittablename_Z = "";
         gxTv_SdtAudit_Auditshortdescription_Z = "";
         gxTv_SdtAudit_Usuario_pessoanom_Z = "";
         gxTv_SdtAudit_Usuario_nome_Z = "";
         gxTv_SdtAudit_Auditaction_Z = "";
      }

      public SdtAudit( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( long AV1430AuditId )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(long)AV1430AuditId});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"AuditId", typeof(long)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Audit");
         metadata.Set("BT", "Audit");
         metadata.Set("PK", "[ \"AuditId\" ]");
         metadata.Set("PKAssigned", "[ \"AuditId\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditdate_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Audittablename_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditshortdescription_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditaction_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditdate_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Audittablename_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditdescription_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditshortdescription_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_pessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Auditaction_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAudit deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAudit)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAudit obj ;
         obj = this;
         obj.gxTpr_Auditid = deserialized.gxTpr_Auditid;
         obj.gxTpr_Auditdate = deserialized.gxTpr_Auditdate;
         obj.gxTpr_Audittablename = deserialized.gxTpr_Audittablename;
         obj.gxTpr_Auditdescription = deserialized.gxTpr_Auditdescription;
         obj.gxTpr_Auditshortdescription = deserialized.gxTpr_Auditshortdescription;
         obj.gxTpr_Usuario_codigo = deserialized.gxTpr_Usuario_codigo;
         obj.gxTpr_Usuario_pessoanom = deserialized.gxTpr_Usuario_pessoanom;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Auditaction = deserialized.gxTpr_Auditaction;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Auditid_Z = deserialized.gxTpr_Auditid_Z;
         obj.gxTpr_Auditdate_Z = deserialized.gxTpr_Auditdate_Z;
         obj.gxTpr_Audittablename_Z = deserialized.gxTpr_Audittablename_Z;
         obj.gxTpr_Auditshortdescription_Z = deserialized.gxTpr_Auditshortdescription_Z;
         obj.gxTpr_Usuario_codigo_Z = deserialized.gxTpr_Usuario_codigo_Z;
         obj.gxTpr_Usuario_pessoanom_Z = deserialized.gxTpr_Usuario_pessoanom_Z;
         obj.gxTpr_Usuario_nome_Z = deserialized.gxTpr_Usuario_nome_Z;
         obj.gxTpr_Auditaction_Z = deserialized.gxTpr_Auditaction_Z;
         obj.gxTpr_Auditdate_N = deserialized.gxTpr_Auditdate_N;
         obj.gxTpr_Audittablename_N = deserialized.gxTpr_Audittablename_N;
         obj.gxTpr_Auditdescription_N = deserialized.gxTpr_Auditdescription_N;
         obj.gxTpr_Auditshortdescription_N = deserialized.gxTpr_Auditshortdescription_N;
         obj.gxTpr_Usuario_codigo_N = deserialized.gxTpr_Usuario_codigo_N;
         obj.gxTpr_Usuario_pessoanom_N = deserialized.gxTpr_Usuario_pessoanom_N;
         obj.gxTpr_Usuario_nome_N = deserialized.gxTpr_Usuario_nome_N;
         obj.gxTpr_Auditaction_N = deserialized.gxTpr_Auditaction_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditId") )
               {
                  gxTv_SdtAudit_Auditid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditDate") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtAudit_Auditdate = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtAudit_Auditdate = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditTableName") )
               {
                  gxTv_SdtAudit_Audittablename = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditDescription") )
               {
                  gxTv_SdtAudit_Auditdescription = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditShortDescription") )
               {
                  gxTv_SdtAudit_Auditshortdescription = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo") )
               {
                  gxTv_SdtAudit_Usuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom") )
               {
                  gxTv_SdtAudit_Usuario_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtAudit_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditAction") )
               {
                  gxTv_SdtAudit_Auditaction = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAudit_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAudit_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditId_Z") )
               {
                  gxTv_SdtAudit_Auditid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditDate_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtAudit_Auditdate_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtAudit_Auditdate_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditTableName_Z") )
               {
                  gxTv_SdtAudit_Audittablename_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditShortDescription_Z") )
               {
                  gxTv_SdtAudit_Auditshortdescription_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo_Z") )
               {
                  gxTv_SdtAudit_Usuario_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_Z") )
               {
                  gxTv_SdtAudit_Usuario_pessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_Z") )
               {
                  gxTv_SdtAudit_Usuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditAction_Z") )
               {
                  gxTv_SdtAudit_Auditaction_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditDate_N") )
               {
                  gxTv_SdtAudit_Auditdate_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditTableName_N") )
               {
                  gxTv_SdtAudit_Audittablename_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditDescription_N") )
               {
                  gxTv_SdtAudit_Auditdescription_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditShortDescription_N") )
               {
                  gxTv_SdtAudit_Auditshortdescription_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo_N") )
               {
                  gxTv_SdtAudit_Usuario_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom_N") )
               {
                  gxTv_SdtAudit_Usuario_pessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_N") )
               {
                  gxTv_SdtAudit_Usuario_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AuditAction_N") )
               {
                  gxTv_SdtAudit_Auditaction_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Audit";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AuditId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditid), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtAudit_Auditdate) )
         {
            oWriter.WriteStartElement("AuditDate");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtAudit_Auditdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("AuditDate", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("AuditTableName", StringUtil.RTrim( gxTv_SdtAudit_Audittablename));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AuditDescription", StringUtil.RTrim( gxTv_SdtAudit_Auditdescription));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AuditShortDescription", StringUtil.RTrim( gxTv_SdtAudit_Auditshortdescription));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Usuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_PessoaNom", StringUtil.RTrim( gxTv_SdtAudit_Usuario_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtAudit_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AuditAction", StringUtil.RTrim( gxTv_SdtAudit_Auditaction));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAudit_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditid_Z), 18, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtAudit_Auditdate_Z) )
            {
               oWriter.WriteStartElement("AuditDate_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtAudit_Auditdate_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("AuditDate_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("AuditTableName_Z", StringUtil.RTrim( gxTv_SdtAudit_Audittablename_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditShortDescription_Z", StringUtil.RTrim( gxTv_SdtAudit_Auditshortdescription_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Usuario_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_PessoaNom_Z", StringUtil.RTrim( gxTv_SdtAudit_Usuario_pessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Nome_Z", StringUtil.RTrim( gxTv_SdtAudit_Usuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditAction_Z", StringUtil.RTrim( gxTv_SdtAudit_Auditaction_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditDate_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditdate_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditTableName_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Audittablename_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditDescription_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditdescription_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditShortDescription_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditshortdescription_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Usuario_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_PessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Usuario_pessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Usuario_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AuditAction_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditaction_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditid), 18, 0)), false);
         datetime_STZ = gxTv_SdtAudit_Auditdate;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("AuditDate", sDateCnv, false);
         AddObjectProperty("AuditTableName", gxTv_SdtAudit_Audittablename, false);
         AddObjectProperty("AuditDescription", gxTv_SdtAudit_Auditdescription, false);
         AddObjectProperty("AuditShortDescription", gxTv_SdtAudit_Auditshortdescription, false);
         AddObjectProperty("Usuario_Codigo", gxTv_SdtAudit_Usuario_codigo, false);
         AddObjectProperty("Usuario_PessoaNom", gxTv_SdtAudit_Usuario_pessoanom, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtAudit_Usuario_nome, false);
         AddObjectProperty("AuditAction", gxTv_SdtAudit_Auditaction, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAudit_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAudit_Initialized, false);
            AddObjectProperty("AuditId_Z", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtAudit_Auditid_Z), 18, 0)), false);
            datetime_STZ = gxTv_SdtAudit_Auditdate_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("AuditDate_Z", sDateCnv, false);
            AddObjectProperty("AuditTableName_Z", gxTv_SdtAudit_Audittablename_Z, false);
            AddObjectProperty("AuditShortDescription_Z", gxTv_SdtAudit_Auditshortdescription_Z, false);
            AddObjectProperty("Usuario_Codigo_Z", gxTv_SdtAudit_Usuario_codigo_Z, false);
            AddObjectProperty("Usuario_PessoaNom_Z", gxTv_SdtAudit_Usuario_pessoanom_Z, false);
            AddObjectProperty("Usuario_Nome_Z", gxTv_SdtAudit_Usuario_nome_Z, false);
            AddObjectProperty("AuditAction_Z", gxTv_SdtAudit_Auditaction_Z, false);
            AddObjectProperty("AuditDate_N", gxTv_SdtAudit_Auditdate_N, false);
            AddObjectProperty("AuditTableName_N", gxTv_SdtAudit_Audittablename_N, false);
            AddObjectProperty("AuditDescription_N", gxTv_SdtAudit_Auditdescription_N, false);
            AddObjectProperty("AuditShortDescription_N", gxTv_SdtAudit_Auditshortdescription_N, false);
            AddObjectProperty("Usuario_Codigo_N", gxTv_SdtAudit_Usuario_codigo_N, false);
            AddObjectProperty("Usuario_PessoaNom_N", gxTv_SdtAudit_Usuario_pessoanom_N, false);
            AddObjectProperty("Usuario_Nome_N", gxTv_SdtAudit_Usuario_nome_N, false);
            AddObjectProperty("AuditAction_N", gxTv_SdtAudit_Auditaction_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AuditId" )]
      [  XmlElement( ElementName = "AuditId"   )]
      public long gxTpr_Auditid
      {
         get {
            return gxTv_SdtAudit_Auditid ;
         }

         set {
            if ( gxTv_SdtAudit_Auditid != value )
            {
               gxTv_SdtAudit_Mode = "INS";
               this.gxTv_SdtAudit_Auditid_Z_SetNull( );
               this.gxTv_SdtAudit_Auditdate_Z_SetNull( );
               this.gxTv_SdtAudit_Audittablename_Z_SetNull( );
               this.gxTv_SdtAudit_Auditshortdescription_Z_SetNull( );
               this.gxTv_SdtAudit_Usuario_codigo_Z_SetNull( );
               this.gxTv_SdtAudit_Usuario_pessoanom_Z_SetNull( );
               this.gxTv_SdtAudit_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtAudit_Auditaction_Z_SetNull( );
            }
            gxTv_SdtAudit_Auditid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "AuditDate" )]
      [  XmlElement( ElementName = "AuditDate"  , IsNullable=true )]
      public string gxTpr_Auditdate_Nullable
      {
         get {
            if ( gxTv_SdtAudit_Auditdate == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtAudit_Auditdate).value ;
         }

         set {
            gxTv_SdtAudit_Auditdate_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtAudit_Auditdate = DateTime.MinValue;
            else
               gxTv_SdtAudit_Auditdate = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Auditdate
      {
         get {
            return gxTv_SdtAudit_Auditdate ;
         }

         set {
            gxTv_SdtAudit_Auditdate_N = 0;
            gxTv_SdtAudit_Auditdate = (DateTime)(value);
         }

      }

      public void gxTv_SdtAudit_Auditdate_SetNull( )
      {
         gxTv_SdtAudit_Auditdate_N = 1;
         gxTv_SdtAudit_Auditdate = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtAudit_Auditdate_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditTableName" )]
      [  XmlElement( ElementName = "AuditTableName"   )]
      public String gxTpr_Audittablename
      {
         get {
            return gxTv_SdtAudit_Audittablename ;
         }

         set {
            gxTv_SdtAudit_Audittablename_N = 0;
            gxTv_SdtAudit_Audittablename = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Audittablename_SetNull( )
      {
         gxTv_SdtAudit_Audittablename_N = 1;
         gxTv_SdtAudit_Audittablename = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Audittablename_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditDescription" )]
      [  XmlElement( ElementName = "AuditDescription"   )]
      public String gxTpr_Auditdescription
      {
         get {
            return gxTv_SdtAudit_Auditdescription ;
         }

         set {
            gxTv_SdtAudit_Auditdescription_N = 0;
            gxTv_SdtAudit_Auditdescription = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Auditdescription_SetNull( )
      {
         gxTv_SdtAudit_Auditdescription_N = 1;
         gxTv_SdtAudit_Auditdescription = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Auditdescription_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditShortDescription" )]
      [  XmlElement( ElementName = "AuditShortDescription"   )]
      public String gxTpr_Auditshortdescription
      {
         get {
            return gxTv_SdtAudit_Auditshortdescription ;
         }

         set {
            gxTv_SdtAudit_Auditshortdescription_N = 0;
            gxTv_SdtAudit_Auditshortdescription = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Auditshortdescription_SetNull( )
      {
         gxTv_SdtAudit_Auditshortdescription_N = 1;
         gxTv_SdtAudit_Auditshortdescription = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Auditshortdescription_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo" )]
      [  XmlElement( ElementName = "Usuario_Codigo"   )]
      public int gxTpr_Usuario_codigo
      {
         get {
            return gxTv_SdtAudit_Usuario_codigo ;
         }

         set {
            gxTv_SdtAudit_Usuario_codigo_N = 0;
            gxTv_SdtAudit_Usuario_codigo = (int)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_codigo_SetNull( )
      {
         gxTv_SdtAudit_Usuario_codigo_N = 1;
         gxTv_SdtAudit_Usuario_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom"   )]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return gxTv_SdtAudit_Usuario_pessoanom ;
         }

         set {
            gxTv_SdtAudit_Usuario_pessoanom_N = 0;
            gxTv_SdtAudit_Usuario_pessoanom = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_pessoanom_SetNull( )
      {
         gxTv_SdtAudit_Usuario_pessoanom_N = 1;
         gxTv_SdtAudit_Usuario_pessoanom = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_pessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtAudit_Usuario_nome ;
         }

         set {
            gxTv_SdtAudit_Usuario_nome_N = 0;
            gxTv_SdtAudit_Usuario_nome = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_nome_SetNull( )
      {
         gxTv_SdtAudit_Usuario_nome_N = 1;
         gxTv_SdtAudit_Usuario_nome = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditAction" )]
      [  XmlElement( ElementName = "AuditAction"   )]
      public String gxTpr_Auditaction
      {
         get {
            return gxTv_SdtAudit_Auditaction ;
         }

         set {
            gxTv_SdtAudit_Auditaction_N = 0;
            gxTv_SdtAudit_Auditaction = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Auditaction_SetNull( )
      {
         gxTv_SdtAudit_Auditaction_N = 1;
         gxTv_SdtAudit_Auditaction = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Auditaction_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAudit_Mode ;
         }

         set {
            gxTv_SdtAudit_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Mode_SetNull( )
      {
         gxTv_SdtAudit_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAudit_Initialized ;
         }

         set {
            gxTv_SdtAudit_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Initialized_SetNull( )
      {
         gxTv_SdtAudit_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditId_Z" )]
      [  XmlElement( ElementName = "AuditId_Z"   )]
      public long gxTpr_Auditid_Z
      {
         get {
            return gxTv_SdtAudit_Auditid_Z ;
         }

         set {
            gxTv_SdtAudit_Auditid_Z = (long)(value);
         }

      }

      public void gxTv_SdtAudit_Auditid_Z_SetNull( )
      {
         gxTv_SdtAudit_Auditid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Auditid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditDate_Z" )]
      [  XmlElement( ElementName = "AuditDate_Z"  , IsNullable=true )]
      public string gxTpr_Auditdate_Z_Nullable
      {
         get {
            if ( gxTv_SdtAudit_Auditdate_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtAudit_Auditdate_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtAudit_Auditdate_Z = DateTime.MinValue;
            else
               gxTv_SdtAudit_Auditdate_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Auditdate_Z
      {
         get {
            return gxTv_SdtAudit_Auditdate_Z ;
         }

         set {
            gxTv_SdtAudit_Auditdate_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtAudit_Auditdate_Z_SetNull( )
      {
         gxTv_SdtAudit_Auditdate_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtAudit_Auditdate_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditTableName_Z" )]
      [  XmlElement( ElementName = "AuditTableName_Z"   )]
      public String gxTpr_Audittablename_Z
      {
         get {
            return gxTv_SdtAudit_Audittablename_Z ;
         }

         set {
            gxTv_SdtAudit_Audittablename_Z = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Audittablename_Z_SetNull( )
      {
         gxTv_SdtAudit_Audittablename_Z = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Audittablename_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditShortDescription_Z" )]
      [  XmlElement( ElementName = "AuditShortDescription_Z"   )]
      public String gxTpr_Auditshortdescription_Z
      {
         get {
            return gxTv_SdtAudit_Auditshortdescription_Z ;
         }

         set {
            gxTv_SdtAudit_Auditshortdescription_Z = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Auditshortdescription_Z_SetNull( )
      {
         gxTv_SdtAudit_Auditshortdescription_Z = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Auditshortdescription_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo_Z" )]
      [  XmlElement( ElementName = "Usuario_Codigo_Z"   )]
      public int gxTpr_Usuario_codigo_Z
      {
         get {
            return gxTv_SdtAudit_Usuario_codigo_Z ;
         }

         set {
            gxTv_SdtAudit_Usuario_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_codigo_Z_SetNull( )
      {
         gxTv_SdtAudit_Usuario_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_Z" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_Z"   )]
      public String gxTpr_Usuario_pessoanom_Z
      {
         get {
            return gxTv_SdtAudit_Usuario_pessoanom_Z ;
         }

         set {
            gxTv_SdtAudit_Usuario_pessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_pessoanom_Z_SetNull( )
      {
         gxTv_SdtAudit_Usuario_pessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_pessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_Z" )]
      [  XmlElement( ElementName = "Usuario_Nome_Z"   )]
      public String gxTpr_Usuario_nome_Z
      {
         get {
            return gxTv_SdtAudit_Usuario_nome_Z ;
         }

         set {
            gxTv_SdtAudit_Usuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_nome_Z_SetNull( )
      {
         gxTv_SdtAudit_Usuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditAction_Z" )]
      [  XmlElement( ElementName = "AuditAction_Z"   )]
      public String gxTpr_Auditaction_Z
      {
         get {
            return gxTv_SdtAudit_Auditaction_Z ;
         }

         set {
            gxTv_SdtAudit_Auditaction_Z = (String)(value);
         }

      }

      public void gxTv_SdtAudit_Auditaction_Z_SetNull( )
      {
         gxTv_SdtAudit_Auditaction_Z = "";
         return  ;
      }

      public bool gxTv_SdtAudit_Auditaction_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditDate_N" )]
      [  XmlElement( ElementName = "AuditDate_N"   )]
      public short gxTpr_Auditdate_N
      {
         get {
            return gxTv_SdtAudit_Auditdate_N ;
         }

         set {
            gxTv_SdtAudit_Auditdate_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Auditdate_N_SetNull( )
      {
         gxTv_SdtAudit_Auditdate_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Auditdate_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditTableName_N" )]
      [  XmlElement( ElementName = "AuditTableName_N"   )]
      public short gxTpr_Audittablename_N
      {
         get {
            return gxTv_SdtAudit_Audittablename_N ;
         }

         set {
            gxTv_SdtAudit_Audittablename_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Audittablename_N_SetNull( )
      {
         gxTv_SdtAudit_Audittablename_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Audittablename_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditDescription_N" )]
      [  XmlElement( ElementName = "AuditDescription_N"   )]
      public short gxTpr_Auditdescription_N
      {
         get {
            return gxTv_SdtAudit_Auditdescription_N ;
         }

         set {
            gxTv_SdtAudit_Auditdescription_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Auditdescription_N_SetNull( )
      {
         gxTv_SdtAudit_Auditdescription_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Auditdescription_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditShortDescription_N" )]
      [  XmlElement( ElementName = "AuditShortDescription_N"   )]
      public short gxTpr_Auditshortdescription_N
      {
         get {
            return gxTv_SdtAudit_Auditshortdescription_N ;
         }

         set {
            gxTv_SdtAudit_Auditshortdescription_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Auditshortdescription_N_SetNull( )
      {
         gxTv_SdtAudit_Auditshortdescription_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Auditshortdescription_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo_N" )]
      [  XmlElement( ElementName = "Usuario_Codigo_N"   )]
      public short gxTpr_Usuario_codigo_N
      {
         get {
            return gxTv_SdtAudit_Usuario_codigo_N ;
         }

         set {
            gxTv_SdtAudit_Usuario_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_codigo_N_SetNull( )
      {
         gxTv_SdtAudit_Usuario_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_PessoaNom_N" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom_N"   )]
      public short gxTpr_Usuario_pessoanom_N
      {
         get {
            return gxTv_SdtAudit_Usuario_pessoanom_N ;
         }

         set {
            gxTv_SdtAudit_Usuario_pessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_pessoanom_N_SetNull( )
      {
         gxTv_SdtAudit_Usuario_pessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_pessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_N" )]
      [  XmlElement( ElementName = "Usuario_Nome_N"   )]
      public short gxTpr_Usuario_nome_N
      {
         get {
            return gxTv_SdtAudit_Usuario_nome_N ;
         }

         set {
            gxTv_SdtAudit_Usuario_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Usuario_nome_N_SetNull( )
      {
         gxTv_SdtAudit_Usuario_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Usuario_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AuditAction_N" )]
      [  XmlElement( ElementName = "AuditAction_N"   )]
      public short gxTpr_Auditaction_N
      {
         get {
            return gxTv_SdtAudit_Auditaction_N ;
         }

         set {
            gxTv_SdtAudit_Auditaction_N = (short)(value);
         }

      }

      public void gxTv_SdtAudit_Auditaction_N_SetNull( )
      {
         gxTv_SdtAudit_Auditaction_N = 0;
         return  ;
      }

      public bool gxTv_SdtAudit_Auditaction_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAudit_Auditdate = (DateTime)(DateTime.MinValue);
         gxTv_SdtAudit_Audittablename = "";
         gxTv_SdtAudit_Auditdescription = "";
         gxTv_SdtAudit_Auditshortdescription = "";
         gxTv_SdtAudit_Usuario_pessoanom = "";
         gxTv_SdtAudit_Usuario_nome = "";
         gxTv_SdtAudit_Auditaction = "";
         gxTv_SdtAudit_Mode = "";
         gxTv_SdtAudit_Auditdate_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtAudit_Audittablename_Z = "";
         gxTv_SdtAudit_Auditshortdescription_Z = "";
         gxTv_SdtAudit_Usuario_pessoanom_Z = "";
         gxTv_SdtAudit_Usuario_nome_Z = "";
         gxTv_SdtAudit_Auditaction_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "audit", "GeneXus.Programs.audit_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAudit_Initialized ;
      private short gxTv_SdtAudit_Auditdate_N ;
      private short gxTv_SdtAudit_Audittablename_N ;
      private short gxTv_SdtAudit_Auditdescription_N ;
      private short gxTv_SdtAudit_Auditshortdescription_N ;
      private short gxTv_SdtAudit_Usuario_codigo_N ;
      private short gxTv_SdtAudit_Usuario_pessoanom_N ;
      private short gxTv_SdtAudit_Usuario_nome_N ;
      private short gxTv_SdtAudit_Auditaction_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAudit_Usuario_codigo ;
      private int gxTv_SdtAudit_Usuario_codigo_Z ;
      private long gxTv_SdtAudit_Auditid ;
      private long gxTv_SdtAudit_Auditid_Z ;
      private String gxTv_SdtAudit_Audittablename ;
      private String gxTv_SdtAudit_Usuario_pessoanom ;
      private String gxTv_SdtAudit_Usuario_nome ;
      private String gxTv_SdtAudit_Auditaction ;
      private String gxTv_SdtAudit_Mode ;
      private String gxTv_SdtAudit_Audittablename_Z ;
      private String gxTv_SdtAudit_Usuario_pessoanom_Z ;
      private String gxTv_SdtAudit_Usuario_nome_Z ;
      private String gxTv_SdtAudit_Auditaction_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtAudit_Auditdate ;
      private DateTime gxTv_SdtAudit_Auditdate_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtAudit_Auditdescription ;
      private String gxTv_SdtAudit_Auditshortdescription ;
      private String gxTv_SdtAudit_Auditshortdescription_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Audit", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAudit_RESTInterface : GxGenericCollectionItem<SdtAudit>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAudit_RESTInterface( ) : base()
      {
      }

      public SdtAudit_RESTInterface( SdtAudit psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AuditId" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Auditid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Auditid), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Auditid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "AuditDate" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Auditdate
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Auditdate) ;
         }

         set {
            sdt.gxTpr_Auditdate = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "AuditTableName" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Audittablename
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Audittablename) ;
         }

         set {
            sdt.gxTpr_Audittablename = (String)(value);
         }

      }

      [DataMember( Name = "AuditDescription" , Order = 3 )]
      public String gxTpr_Auditdescription
      {
         get {
            return sdt.gxTpr_Auditdescription ;
         }

         set {
            sdt.gxTpr_Auditdescription = (String)(value);
         }

      }

      [DataMember( Name = "AuditShortDescription" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Auditshortdescription
      {
         get {
            return sdt.gxTpr_Auditshortdescription ;
         }

         set {
            sdt.gxTpr_Auditshortdescription = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Codigo" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Usuario_codigo
      {
         get {
            return sdt.gxTpr_Usuario_codigo ;
         }

         set {
            sdt.gxTpr_Usuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_PessoaNom" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoanom) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "AuditAction" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Auditaction
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Auditaction) ;
         }

         set {
            sdt.gxTpr_Auditaction = (String)(value);
         }

      }

      public SdtAudit sdt
      {
         get {
            return (SdtAudit)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAudit() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 27 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
