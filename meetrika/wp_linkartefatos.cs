/*
               File: WP_LinkArtefatos
        Description: Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:50:13.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_linkartefatos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_linkartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_linkartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_User_Id )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12User_Id = aP1_User_Id;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavCtlartefatos_obrigatorio = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_6 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_6_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_6_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               A1772ContagemResultadoArtefato_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1772ContagemResultadoArtefato_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1772ContagemResultadoArtefato_OSCod), 6, 0)));
               AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
               A1771ContagemResultadoArtefato_ArtefatoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1771ContagemResultadoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10sdt_Artefato);
               A1751Artefatos_Descricao = GetNextPar( );
               n1751Artefatos_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
               AV15InMemory = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15InMemory", AV15InMemory);
               A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1553ContagemResultado_CntSrvCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
               A1749Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               A1768ContratoServicosArtefato_Obrigatorio = (bool)(BooleanUtil.Val(GetNextPar( )));
               n1768ContratoServicosArtefato_Obrigatorio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
               A1770ContagemResultadoArtefato_EvdCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1770ContagemResultadoArtefato_EvdCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1770ContagemResultadoArtefato_EvdCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1770ContagemResultadoArtefato_EvdCod), 6, 0)));
               A586ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               A1449ContagemResultadoEvidencia_Link = GetNextPar( );
               n1449ContagemResultadoEvidencia_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1449ContagemResultadoEvidencia_Link", A1449ContagemResultadoEvidencia_Link);
               A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               A1773ContagemResultadoArtefato_AnxCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1773ContagemResultadoArtefato_AnxCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1773ContagemResultadoArtefato_AnxCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1773ContagemResultadoArtefato_AnxCod), 6, 0)));
               A1450Anexo_Link = GetNextPar( );
               n1450Anexo_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1450Anexo_Link", A1450Anexo_Link);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV18Sdt_ContagemResultadoEvidencias);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11sdt_Artefatos);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( A1772ContagemResultadoArtefato_OSCod, AV7ContagemResultado_Codigo, A1771ContagemResultadoArtefato_ArtefatoCod, AV10sdt_Artefato, A1751Artefatos_Descricao, AV15InMemory, A160ContratoServicos_Codigo, A1553ContagemResultado_CntSrvCod, A1749Artefatos_Codigo, A1768ContratoServicosArtefato_Obrigatorio, A1770ContagemResultadoArtefato_EvdCod, A586ContagemResultadoEvidencia_Codigo, A1449ContagemResultadoEvidencia_Link, A1106Anexo_Codigo, A1773ContagemResultadoArtefato_AnxCod, A1450Anexo_Link, AV18Sdt_ContagemResultadoEvidencias, AV11sdt_Artefatos) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12User_Id = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12User_Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12User_Id), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSER_ID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12User_Id), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PANF2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTNF2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423501327");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_linkartefatos.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV12User_Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_artefatos", AV11sdt_Artefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_artefatos", AV11sdt_Artefatos);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_6", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_6), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOARTEFATO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1772ContagemResultadoArtefato_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARTEFATO", AV10sdt_Artefato);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARTEFATO", AV10sdt_Artefato);
         }
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "vINMEMORY", AV15InMemory);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOSARTEFATO_OBRIGATORIO", A1768ContratoServicosArtefato_Obrigatorio);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOARTEFATO_EVDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1770ContagemResultadoArtefato_EvdCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_LINK", A1449ContagemResultadoEvidencia_Link);
         GxWebStd.gx_hidden_field( context, "ANEXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1106Anexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOARTEFATO_ANXCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1773ContagemResultadoArtefato_AnxCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXO_LINK", A1450Anexo_Link);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV18Sdt_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV18Sdt_ContagemResultadoEvidencias);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARTEFATOS", AV11sdt_Artefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARTEFATOS", AV11sdt_Artefatos);
         }
         GxWebStd.gx_hidden_field( context, "vCALLER", StringUtil.RTrim( AV14Caller));
         GxWebStd.gx_hidden_field( context, "vUSER_ID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12User_Id), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vANEXOSDE", AV6AnexosDe);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vANEXOSDE", AV6AnexosDe);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARQUIVOEVIDENCIA", AV17Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARQUIVOEVIDENCIA", AV17Sdt_ArquivoEvidencia);
         }
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSER_ID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12User_Id), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSER_ID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12User_Id), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WENF2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTNF2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_linkartefatos.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV12User_Id) ;
      }

      public override String GetPgmname( )
      {
         return "WP_LinkArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Artefatos" ;
      }

      protected void WBNF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_NF2( true) ;
         }
         else
         {
            wb_table1_3_NF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_NF2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_LinkArtefatos.htm");
         }
         wbLoad = true;
      }

      protected void STARTNF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPNF0( ) ;
      }

      protected void WSNF2( )
      {
         STARTNF2( ) ;
         EVTNF2( ) ;
      }

      protected void EVTNF2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11NF2 */
                                    E11NF2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) )
                           {
                              nGXsfl_6_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
                              SubsflControlProps_62( ) ;
                              AV23GXV1 = nGXsfl_6_idx;
                              if ( ( AV11sdt_Artefatos.Count >= AV23GXV1 ) && ( AV23GXV1 > 0 ) )
                              {
                                 AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12NF2 */
                                    E12NF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13NF2 */
                                    E13NF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14NF2 */
                                    E14NF2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WENF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PANF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CTLARTEFATOS_OBRIGATORIO_" + sGXsfl_6_idx;
            chkavCtlartefatos_obrigatorio.Name = GXCCtl;
            chkavCtlartefatos_obrigatorio.WebTags = "";
            chkavCtlartefatos_obrigatorio.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCtlartefatos_obrigatorio_Internalname, "TitleCaption", chkavCtlartefatos_obrigatorio.Caption);
            chkavCtlartefatos_obrigatorio.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_62( ) ;
         while ( nGXsfl_6_idx <= nRC_GXsfl_6 )
         {
            sendrow_62( ) ;
            nGXsfl_6_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_6_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_6_idx+1));
            sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
            SubsflControlProps_62( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A1772ContagemResultadoArtefato_OSCod ,
                                       int AV7ContagemResultado_Codigo ,
                                       int A1771ContagemResultadoArtefato_ArtefatoCod ,
                                       SdtSDT_Artefatos AV10sdt_Artefato ,
                                       String A1751Artefatos_Descricao ,
                                       bool AV15InMemory ,
                                       int A160ContratoServicos_Codigo ,
                                       int A1553ContagemResultado_CntSrvCod ,
                                       int A1749Artefatos_Codigo ,
                                       bool A1768ContratoServicosArtefato_Obrigatorio ,
                                       int A1770ContagemResultadoArtefato_EvdCod ,
                                       int A586ContagemResultadoEvidencia_Codigo ,
                                       String A1449ContagemResultadoEvidencia_Link ,
                                       int A1106Anexo_Codigo ,
                                       int A1773ContagemResultadoArtefato_AnxCod ,
                                       String A1450Anexo_Link ,
                                       IGxCollection AV18Sdt_ContagemResultadoEvidencias ,
                                       IGxCollection AV11sdt_Artefatos )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFNF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0)));
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCtlartefatos_obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0)));
         edtavCtlartefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0)));
         edtavCtlartefatos_evdcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_evdcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_evdcod_Enabled), 5, 0)));
         edtavCtlartefatos_anxcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_anxcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_anxcod_Enabled), 5, 0)));
      }

      protected void RFNF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 6;
         /* Execute user event: E13NF2 */
         E13NF2 ();
         nGXsfl_6_idx = 1;
         sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
         SubsflControlProps_62( ) ;
         nGXsfl_6_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_62( ) ;
            /* Execute user event: E14NF2 */
            E14NF2 ();
            wbEnd = 6;
            WBNF0( ) ;
         }
         nGXsfl_6_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPNF0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0)));
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCtlartefatos_obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0)));
         edtavCtlartefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0)));
         edtavCtlartefatos_evdcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_evdcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_evdcod_Enabled), 5, 0)));
         edtavCtlartefatos_anxcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_anxcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_anxcod_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12NF2 */
         E12NF2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_artefatos"), AV11sdt_Artefatos);
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_6 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_6"), ",", "."));
            nRC_GXsfl_6 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_6"), ",", "."));
            nGXsfl_6_fel_idx = 0;
            while ( nGXsfl_6_fel_idx < nRC_GXsfl_6 )
            {
               nGXsfl_6_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_6_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_6_fel_idx+1));
               sGXsfl_6_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_62( ) ;
               AV23GXV1 = nGXsfl_6_fel_idx;
               if ( ( AV11sdt_Artefatos.Count >= AV23GXV1 ) && ( AV23GXV1 > 0 ) )
               {
                  AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
               }
            }
            if ( nGXsfl_6_fel_idx == 0 )
            {
               nGXsfl_6_idx = 1;
               sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
               SubsflControlProps_62( ) ;
            }
            nGXsfl_6_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12NF2 */
         E12NF2 ();
         if (returnInSub) return;
      }

      protected void E12NF2( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         AV14Caller = AV20WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Caller", AV14Caller);
         AV18Sdt_ContagemResultadoEvidencias.FromXml(AV20WebSession.Get("ArquivosEvd"), "");
      }

      protected void E13NF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV11sdt_Artefatos.Clear();
         gx_BV6 = true;
         /* Using cursor H00NF2 */
         pr_default.execute(0, new Object[] {AV7ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1773ContagemResultadoArtefato_AnxCod = H00NF2_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = H00NF2_n1773ContagemResultadoArtefato_AnxCod[0];
            A1450Anexo_Link = H00NF2_A1450Anexo_Link[0];
            n1450Anexo_Link = H00NF2_n1450Anexo_Link[0];
            A1770ContagemResultadoArtefato_EvdCod = H00NF2_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = H00NF2_n1770ContagemResultadoArtefato_EvdCod[0];
            A1449ContagemResultadoEvidencia_Link = H00NF2_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = H00NF2_n1449ContagemResultadoEvidencia_Link[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = H00NF2_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1553ContagemResultado_CntSrvCod = H00NF2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00NF2_n1553ContagemResultado_CntSrvCod[0];
            A1772ContagemResultadoArtefato_OSCod = H00NF2_A1772ContagemResultadoArtefato_OSCod[0];
            A1751Artefatos_Descricao = H00NF2_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00NF2_n1751Artefatos_Descricao[0];
            A1450Anexo_Link = H00NF2_A1450Anexo_Link[0];
            n1450Anexo_Link = H00NF2_n1450Anexo_Link[0];
            A1449ContagemResultadoEvidencia_Link = H00NF2_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = H00NF2_n1449ContagemResultadoEvidencia_Link[0];
            A1751Artefatos_Descricao = H00NF2_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00NF2_n1751Artefatos_Descricao[0];
            A1553ContagemResultado_CntSrvCod = H00NF2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00NF2_n1553ContagemResultado_CntSrvCod[0];
            AV10sdt_Artefato.gxTpr_Artefatos_codigo = A1771ContagemResultadoArtefato_ArtefatoCod;
            AV10sdt_Artefato.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
            /* Execute user subroutine: 'MEMORYFIND' */
            S113 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            if ( ! AV15InMemory )
            {
               /* Using cursor H00NF3 */
               pr_default.execute(1, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1771ContagemResultadoArtefato_ArtefatoCod});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A160ContratoServicos_Codigo = H00NF3_A160ContratoServicos_Codigo[0];
                  A1749Artefatos_Codigo = H00NF3_A1749Artefatos_Codigo[0];
                  A1768ContratoServicosArtefato_Obrigatorio = H00NF3_A1768ContratoServicosArtefato_Obrigatorio[0];
                  n1768ContratoServicosArtefato_Obrigatorio = H00NF3_n1768ContratoServicosArtefato_Obrigatorio[0];
                  AV10sdt_Artefato.gxTpr_Artefatos_obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
               if ( A1770ContagemResultadoArtefato_EvdCod > 0 )
               {
                  /* Using cursor H00NF4 */
                  pr_default.execute(2, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A586ContagemResultadoEvidencia_Codigo = H00NF4_A586ContagemResultadoEvidencia_Codigo[0];
                     AV10sdt_Artefato.gxTpr_Artefatos_evdcod = A586ContagemResultadoEvidencia_Codigo;
                     AV10sdt_Artefato.gxTpr_Artefatos_link = A1449ContagemResultadoEvidencia_Link;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(2);
               }
               else
               {
                  /* Using cursor H00NF5 */
                  pr_default.execute(3, new Object[] {n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A1106Anexo_Codigo = H00NF5_A1106Anexo_Codigo[0];
                     AV10sdt_Artefato.gxTpr_Artefatos_anxcod = A1106Anexo_Codigo;
                     AV10sdt_Artefato.gxTpr_Artefatos_link = A1450Anexo_Link;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(3);
               }
            }
            AV11sdt_Artefatos.Add(AV10sdt_Artefato, 0);
            gx_BV6 = true;
            AV10sdt_Artefato = new SdtSDT_Artefatos(context);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11sdt_Artefatos", AV11sdt_Artefatos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10sdt_Artefato", AV10sdt_Artefato);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17Sdt_ArquivoEvidencia", AV17Sdt_ArquivoEvidencia);
      }

      private void E14NF2( )
      {
         /* Load Routine */
         AV23GXV1 = 1;
         while ( AV23GXV1 <= AV11sdt_Artefatos.Count )
         {
            AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
            if ( ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_obrigatorio )
            {
               edtavCtlartefatos_link_Backcolor = GXUtil.RGB( 250, 255, 225);
            }
            else
            {
               edtavCtlartefatos_link_Backcolor = GXUtil.RGB( 255, 255, 255);
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 6;
            }
            sendrow_62( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_6_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(6, GridRow);
            }
            AV23GXV1 = (short)(AV23GXV1+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11NF2 */
         E11NF2 ();
         if (returnInSub) return;
      }

      protected void E11NF2( )
      {
         AV23GXV1 = nGXsfl_6_idx;
         if ( AV11sdt_Artefatos.Count >= AV23GXV1 )
         {
            AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
         }
         /* Enter Routine */
         AV13Pendencias = false;
         /* Start For Each Line */
         nRC_GXsfl_6 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_6"), ",", "."));
         nGXsfl_6_fel_idx = 0;
         while ( nGXsfl_6_fel_idx < nRC_GXsfl_6 )
         {
            nGXsfl_6_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_6_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_6_fel_idx+1));
            sGXsfl_6_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_62( ) ;
            AV23GXV1 = nGXsfl_6_fel_idx;
            if ( ( AV11sdt_Artefatos.Count >= AV23GXV1 ) && ( AV23GXV1 > 0 ) )
            {
               AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_link)) && ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_obrigatorio )
            {
               edtavCtlartefatos_link_Backcolor = GXUtil.RGB( 250, 255, 225);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_link_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_link_Backcolor), 9, 0)));
               AV13Pendencias = true;
            }
            else
            {
               edtavCtlartefatos_link_Backcolor = GXUtil.RGB( 255, 255, 255);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlartefatos_link_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_link_Backcolor), 9, 0)));
            }
            /* End For Each Line */
         }
         if ( nGXsfl_6_fel_idx == 0 )
         {
            nGXsfl_6_idx = 1;
            sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
            SubsflControlProps_62( ) ;
         }
         nGXsfl_6_fel_idx = 1;
         if ( AV13Pendencias )
         {
            GX_msglist.addItem("Existe(m) artefato(s) obrigat�rio(s) (fundo amarelo)!");
         }
         else
         {
            if ( StringUtil.StrCmp(AV14Caller, "Evidencias") == 0 )
            {
               /* Execute user subroutine: 'GRAVADO' */
               S122 ();
               if (returnInSub) return;
            }
            else
            {
               /* Execute user subroutine: 'EMMEMORIA' */
               S132 ();
               if (returnInSub) return;
            }
            /* Execute user subroutine: 'CLOSEREFRESH' */
            S142 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10sdt_Artefato", AV10sdt_Artefato);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6AnexosDe", AV6AnexosDe);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV18Sdt_ContagemResultadoEvidencias", AV18Sdt_ContagemResultadoEvidencias);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17Sdt_ArquivoEvidencia", AV17Sdt_ArquivoEvidencia);
      }

      protected void S122( )
      {
         /* 'GRAVADO' Routine */
         AV35GXV2 = 1;
         while ( AV35GXV2 <= AV11sdt_Artefatos.Count )
         {
            AV10sdt_Artefato = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV35GXV2));
            if ( (0==AV10sdt_Artefato.gxTpr_Artefatos_evdcod) && (0==AV10sdt_Artefato.gxTpr_Artefatos_anxcod) )
            {
               AV5Anexos = new SdtAnexos(context);
               AV5Anexos.gxTpr_Anexo_data = DateTimeUtil.ServerNow( context, "DEFAULT");
               AV5Anexos.gxTpr_Anexo_link = AV10sdt_Artefato.gxTpr_Artefatos_link;
               AV5Anexos.gxTpr_Anexo_userid = AV12User_Id;
               AV5Anexos.gxTpr_Anexo_owner = AV12User_Id;
               AV5Anexos.gxTv_SdtAnexos_Tipodocumento_codigo_SetNull();
               AV6AnexosDe.gxTpr_Anexode_id = AV7ContagemResultado_Codigo;
               AV6AnexosDe.gxTpr_Anexode_tabela = 1;
               AV5Anexos.gxTpr_De.Add(AV6AnexosDe, 0);
               AV5Anexos.Save();
               AV8ContagemResultadoArtefato = new SdtContagemResultadoArtefato(context);
               AV8ContagemResultadoArtefato.gxTpr_Contagemresultadoartefato_oscod = AV7ContagemResultado_Codigo;
               AV8ContagemResultadoArtefato.gxTpr_Contagemresultadoartefato_artefatocod = AV10sdt_Artefato.gxTpr_Artefatos_codigo;
               AV8ContagemResultadoArtefato.gxTpr_Contagemresultadoartefato_anxcod = AV5Anexos.gxTpr_Anexo_codigo;
               AV8ContagemResultadoArtefato.gxTv_SdtContagemResultadoArtefato_Contagemresultadoartefato_evdcod_SetNull();
               AV8ContagemResultadoArtefato.Save();
            }
            else if ( AV10sdt_Artefato.gxTpr_Artefatos_evdcod > 0 )
            {
               AV9ContagemResultadoEvidencia.Load((int)(AV10sdt_Artefato.gxTpr_Artefatos_evdcod));
               AV9ContagemResultadoEvidencia.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_SetNull();
               AV9ContagemResultadoEvidencia.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_SetNull();
               AV9ContagemResultadoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = "";
               AV9ContagemResultadoEvidencia.gxTpr_Contagemresultadoevidencia_link = AV10sdt_Artefato.gxTpr_Artefatos_link;
               AV9ContagemResultadoEvidencia.Save();
            }
            else if ( AV10sdt_Artefato.gxTpr_Artefatos_anxcod > 0 )
            {
               AV5Anexos.Load((int)(AV10sdt_Artefato.gxTpr_Artefatos_anxcod));
               AV5Anexos.gxTv_SdtAnexos_Anexo_arquivo_SetNull();
               AV5Anexos.gxTv_SdtAnexos_Anexo_nomearq_SetNull();
               AV5Anexos.gxTv_SdtAnexos_Anexo_tipoarq_SetNull();
               AV5Anexos.gxTpr_Anexo_link = AV10sdt_Artefato.gxTpr_Artefatos_link;
               AV5Anexos.Save();
            }
            AV35GXV2 = (int)(AV35GXV2+1);
         }
         context.CommitDataStores( "WP_LinkArtefatos");
      }

      protected void S132( )
      {
         /* 'EMMEMORIA' Routine */
         AV18Sdt_ContagemResultadoEvidencias.Clear();
         /* Start For Each Line */
         nRC_GXsfl_6 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_6"), ",", "."));
         nGXsfl_6_fel_idx = 0;
         while ( nGXsfl_6_fel_idx < nRC_GXsfl_6 )
         {
            nGXsfl_6_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_6_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_6_fel_idx+1));
            sGXsfl_6_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_62( ) ;
            AV23GXV1 = nGXsfl_6_fel_idx;
            if ( ( AV11sdt_Artefatos.Count >= AV23GXV1 ) && ( AV23GXV1 > 0 ) )
            {
               AV11sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1));
            }
            AV17Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_link;
            AV17Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_link;
            AV17Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner = AV19UserId;
            AV17Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo = ((SdtSDT_Artefatos)(AV11sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_codigo;
            AV18Sdt_ContagemResultadoEvidencias.Add(AV17Sdt_ArquivoEvidencia, 0);
            AV17Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
            /* End For Each Line */
         }
         if ( nGXsfl_6_fel_idx == 0 )
         {
            nGXsfl_6_idx = 1;
            sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
            SubsflControlProps_62( ) ;
         }
         nGXsfl_6_fel_idx = 1;
         AV20WebSession.Set("ArquivosEvd", AV18Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
      }

      protected void S113( )
      {
         /* 'MEMORYFIND' Routine */
         AV15InMemory = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15InMemory", AV15InMemory);
         AV37GXV3 = 1;
         while ( AV37GXV3 <= AV18Sdt_ContagemResultadoEvidencias.Count )
         {
            AV17Sdt_ArquivoEvidencia = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV18Sdt_ContagemResultadoEvidencias.Item(AV37GXV3));
            if ( AV17Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo == AV10sdt_Artefato.gxTpr_Artefatos_codigo )
            {
               AV10sdt_Artefato.gxTpr_Artefatos_link = AV17Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link;
               AV15InMemory = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15InMemory", AV15InMemory);
               if (true) break;
            }
            AV37GXV3 = (int)(AV37GXV3+1);
         }
      }

      protected void S142( )
      {
         /* 'CLOSEREFRESH' Routine */
         if ( StringUtil.StrCmp(AV14Caller, "Evidencias") == 0 )
         {
            lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\">parent.location.replace(parent.location.href); </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
         else
         {
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void wb_table1_3_NF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"6\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Obrigatorio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Link") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Artefatos_Evd Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Artefatos_Anx Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Backcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_link_Backcolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_evdcod_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_anxcod_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 6 )
         {
            wbEnd = 0;
            nRC_GXsfl_6 = (short)(nGXsfl_6_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV23GXV1 = nGXsfl_6_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirmar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(6), 1, 0)+","+"null"+");", "Confirmar", bttConfirmar_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_LinkArtefatos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancelar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(6), 1, 0)+","+"null"+");", "Fechar", bttCancelar_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_LinkArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_NF2e( true) ;
         }
         else
         {
            wb_table1_3_NF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemResultado_Codigo), "ZZZZZ9")));
         AV12User_Id = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12User_Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12User_Id), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSER_ID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12User_Id), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANF2( ) ;
         WSNF2( ) ;
         WENF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423501386");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_linkartefatos.js", "?202032423501387");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_62( )
      {
         edtavCtlartefatos_codigo_Internalname = "CTLARTEFATOS_CODIGO_"+sGXsfl_6_idx;
         chkavCtlartefatos_obrigatorio_Internalname = "CTLARTEFATOS_OBRIGATORIO_"+sGXsfl_6_idx;
         edtavCtlartefatos_descricao_Internalname = "CTLARTEFATOS_DESCRICAO_"+sGXsfl_6_idx;
         edtavCtlartefatos_link_Internalname = "CTLARTEFATOS_LINK_"+sGXsfl_6_idx;
         edtavCtlartefatos_evdcod_Internalname = "CTLARTEFATOS_EVDCOD_"+sGXsfl_6_idx;
         edtavCtlartefatos_anxcod_Internalname = "CTLARTEFATOS_ANXCOD_"+sGXsfl_6_idx;
      }

      protected void SubsflControlProps_fel_62( )
      {
         edtavCtlartefatos_codigo_Internalname = "CTLARTEFATOS_CODIGO_"+sGXsfl_6_fel_idx;
         chkavCtlartefatos_obrigatorio_Internalname = "CTLARTEFATOS_OBRIGATORIO_"+sGXsfl_6_fel_idx;
         edtavCtlartefatos_descricao_Internalname = "CTLARTEFATOS_DESCRICAO_"+sGXsfl_6_fel_idx;
         edtavCtlartefatos_link_Internalname = "CTLARTEFATOS_LINK_"+sGXsfl_6_fel_idx;
         edtavCtlartefatos_evdcod_Internalname = "CTLARTEFATOS_EVDCOD_"+sGXsfl_6_fel_idx;
         edtavCtlartefatos_anxcod_Internalname = "CTLARTEFATOS_ANXCOD_"+sGXsfl_6_fel_idx;
      }

      protected void sendrow_62( )
      {
         SubsflControlProps_62( ) ;
         WBNF0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_6_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_6_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_codigo), 6, 0, ",", "")),((edtavCtlartefatos_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlartefatos_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)6,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Check box */
         ClassString = "Attribute";
         StyleString = "";
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavCtlartefatos_obrigatorio_Internalname,StringUtil.BoolToStr( ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_obrigatorio),(String)"",(String)"",(short)0,chkavCtlartefatos_obrigatorio.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_descricao_Internalname,((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_descricao,StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_descricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlartefatos_descricao_Enabled,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)6,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\""+" bgcolor="+context.BuildHTMLColor( edtavCtlartefatos_link_Backcolor)+">") ;
         }
         /* Multiple line edit */
         TempTags = " " + ((edtavCtlartefatos_link_Enabled!=0)&&(edtavCtlartefatos_link_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 10,'',false,'"+sGXsfl_6_idx+"',6)\"" : " ");
         ClassString = "Attribute";
         StyleString = ((edtavCtlartefatos_link_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( edtavCtlartefatos_link_Backcolor)+";");
         ClassString = "Attribute";
         StyleString = ((edtavCtlartefatos_link_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( edtavCtlartefatos_link_Backcolor)+";");
         GridRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_link_Internalname,((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_link,(String)"",TempTags+((edtavCtlartefatos_link_Enabled!=0)&&(edtavCtlartefatos_link_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlartefatos_link_Enabled!=0)&&(edtavCtlartefatos_link_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,10);\"" : " "),(short)0,(short)-1,(short)1,(short)0,(short)410,(String)"px",(short)51,(String)"px",(String)StyleString,(String)ClassString,(String)"",(String)"1000",(short)1,(String)"",(String)"",(short)0,(bool)true,(String)""});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_evdcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_evdcod), 10, 0, ",", "")),((edtavCtlartefatos_evdcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_evdcod), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_evdcod), "ZZZZZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_evdcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlartefatos_evdcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)6,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_anxcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_anxcod), 10, 0, ",", "")),((edtavCtlartefatos_anxcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_anxcod), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_anxcod), "ZZZZZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_anxcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlartefatos_anxcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)6,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, "gxhash_CTLARTEFATOS_CODIGO"+"_"+sGXsfl_6_idx, GetSecureSignedToken( sGXsfl_6_idx, context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLARTEFATOS_OBRIGATORIO"+"_"+sGXsfl_6_idx, GetSecureSignedToken( sGXsfl_6_idx, ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_obrigatorio));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLARTEFATOS_DESCRICAO"+"_"+sGXsfl_6_idx, GetSecureSignedToken( sGXsfl_6_idx, StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLARTEFATOS_EVDCOD"+"_"+sGXsfl_6_idx, GetSecureSignedToken( sGXsfl_6_idx, context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_evdcod), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLARTEFATOS_ANXCOD"+"_"+sGXsfl_6_idx, GetSecureSignedToken( sGXsfl_6_idx, context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV11sdt_Artefatos.Item(AV23GXV1)).gxTpr_Artefatos_anxcod), "ZZZZZZZZZ9")));
         GridContainer.AddRow(GridRow);
         nGXsfl_6_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_6_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_6_idx+1));
         sGXsfl_6_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_6_idx), 4, 0)), 4, "0");
         SubsflControlProps_62( ) ;
         /* End function sendrow_62 */
      }

      protected void init_default_properties( )
      {
         edtavCtlartefatos_codigo_Internalname = "CTLARTEFATOS_CODIGO";
         chkavCtlartefatos_obrigatorio_Internalname = "CTLARTEFATOS_OBRIGATORIO";
         edtavCtlartefatos_descricao_Internalname = "CTLARTEFATOS_DESCRICAO";
         edtavCtlartefatos_link_Internalname = "CTLARTEFATOS_LINK";
         edtavCtlartefatos_evdcod_Internalname = "CTLARTEFATOS_EVDCOD";
         edtavCtlartefatos_anxcod_Internalname = "CTLARTEFATOS_ANXCOD";
         bttConfirmar_Internalname = "CONFIRMAR";
         bttCancelar_Internalname = "CANCELAR";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlartefatos_anxcod_Jsonclick = "";
         edtavCtlartefatos_evdcod_Jsonclick = "";
         edtavCtlartefatos_link_Visible = -1;
         edtavCtlartefatos_link_Enabled = 1;
         edtavCtlartefatos_descricao_Jsonclick = "";
         edtavCtlartefatos_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCtlartefatos_anxcod_Enabled = 0;
         edtavCtlartefatos_evdcod_Enabled = 0;
         edtavCtlartefatos_link_Backcolor = -1;
         edtavCtlartefatos_descricao_Enabled = 0;
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         subGrid_Class = "Grid";
         subGrid_Backcolorstyle = 3;
         edtavCtlartefatos_anxcod_Enabled = -1;
         edtavCtlartefatos_evdcod_Enabled = -1;
         edtavCtlartefatos_descricao_Enabled = -1;
         chkavCtlartefatos_obrigatorio.Enabled = -1;
         edtavCtlartefatos_codigo_Enabled = -1;
         chkavCtlartefatos_obrigatorio.Caption = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Artefatos";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV11sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:6,pic:'',nv:null},{av:'A1772ContagemResultadoArtefato_OSCod',fld:'CONTAGEMRESULTADOARTEFATO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1771ContagemResultadoArtefato_ArtefatoCod',fld:'CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV15InMemory',fld:'vINMEMORY',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1768ContratoServicosArtefato_Obrigatorio',fld:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',pic:'',nv:false},{av:'A1770ContagemResultadoArtefato_EvdCod',fld:'CONTAGEMRESULTADOARTEFATO_EVDCOD',pic:'ZZZZZ9',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1773ContagemResultadoArtefato_AnxCod',fld:'CONTAGEMRESULTADOARTEFATO_ANXCOD',pic:'ZZZZZ9',nv:0},{av:'A1450Anexo_Link',fld:'ANEXO_LINK',pic:'',nv:''},{av:'AV18Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}],oparms:[{av:'AV11sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:6,pic:'',nv:null},{av:'AV10sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'AV15InMemory',fld:'vINMEMORY',pic:'',nv:false},{av:'AV17Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E11NF2',iparms:[{av:'AV11sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:6,pic:'',nv:null},{av:'AV14Caller',fld:'vCALLER',pic:'@!',nv:''},{av:'AV12User_Id',fld:'vUSER_ID',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6AnexosDe',fld:'vANEXOSDE',pic:'',nv:null},{av:'AV17Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV19UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'CTLARTEFATOS_LINK',prop:'Backcolor'},{av:'AV10sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'AV6AnexosDe',fld:'vANEXOSDE',pic:'',nv:null},{av:'AV18Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null},{av:'AV17Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV10sdt_Artefato = new SdtSDT_Artefatos(context);
         A1751Artefatos_Descricao = "";
         A1449ContagemResultadoEvidencia_Link = "";
         A1450Anexo_Link = "";
         AV18Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV11sdt_Artefatos = new GxObjectCollection( context, "SDT_Artefatos", "GxEv3Up14_Meetrika", "SdtSDT_Artefatos", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV14Caller = "";
         AV6AnexosDe = new SdtAnexos_De(context);
         AV17Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV20WebSession = context.GetSession();
         scmdbuf = "";
         H00NF2_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00NF2_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         H00NF2_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         H00NF2_A1450Anexo_Link = new String[] {""} ;
         H00NF2_n1450Anexo_Link = new bool[] {false} ;
         H00NF2_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         H00NF2_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         H00NF2_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         H00NF2_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         H00NF2_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         H00NF2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00NF2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00NF2_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         H00NF2_A1751Artefatos_Descricao = new String[] {""} ;
         H00NF2_n1751Artefatos_Descricao = new bool[] {false} ;
         H00NF3_A160ContratoServicos_Codigo = new int[1] ;
         H00NF3_A1749Artefatos_Codigo = new int[1] ;
         H00NF3_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00NF3_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00NF4_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00NF5_A1106Anexo_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         AV5Anexos = new SdtAnexos(context);
         AV8ContagemResultadoArtefato = new SdtContagemResultadoArtefato(context);
         AV9ContagemResultadoEvidencia = new SdtContagemResultadoEvidencia(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         bttConfirmar_Jsonclick = "";
         bttCancelar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_linkartefatos__default(),
            new Object[][] {
                new Object[] {
               H00NF2_A1769ContagemResultadoArtefato_Codigo, H00NF2_A1773ContagemResultadoArtefato_AnxCod, H00NF2_n1773ContagemResultadoArtefato_AnxCod, H00NF2_A1450Anexo_Link, H00NF2_n1450Anexo_Link, H00NF2_A1770ContagemResultadoArtefato_EvdCod, H00NF2_n1770ContagemResultadoArtefato_EvdCod, H00NF2_A1449ContagemResultadoEvidencia_Link, H00NF2_n1449ContagemResultadoEvidencia_Link, H00NF2_A1771ContagemResultadoArtefato_ArtefatoCod,
               H00NF2_A1553ContagemResultado_CntSrvCod, H00NF2_n1553ContagemResultado_CntSrvCod, H00NF2_A1772ContagemResultadoArtefato_OSCod, H00NF2_A1751Artefatos_Descricao, H00NF2_n1751Artefatos_Descricao
               }
               , new Object[] {
               H00NF3_A160ContratoServicos_Codigo, H00NF3_A1749Artefatos_Codigo, H00NF3_A1768ContratoServicosArtefato_Obrigatorio, H00NF3_n1768ContratoServicosArtefato_Obrigatorio
               }
               , new Object[] {
               H00NF4_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               H00NF5_A1106Anexo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         edtavCtlartefatos_descricao_Enabled = 0;
         edtavCtlartefatos_evdcod_Enabled = 0;
         edtavCtlartefatos_anxcod_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_6 ;
      private short nGXsfl_6_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV23GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_6_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short nGXsfl_6_fel_idx=1 ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int AV12User_Id ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int wcpOAV12User_Id ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1749Artefatos_Codigo ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1106Anexo_Codigo ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int AV19UserId ;
      private int lblTbjava_Visible ;
      private int subGrid_Islastpage ;
      private int edtavCtlartefatos_codigo_Enabled ;
      private int edtavCtlartefatos_descricao_Enabled ;
      private int edtavCtlartefatos_evdcod_Enabled ;
      private int edtavCtlartefatos_anxcod_Enabled ;
      private int edtavCtlartefatos_link_Backcolor ;
      private int AV35GXV2 ;
      private int AV37GXV3 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavCtlartefatos_link_Enabled ;
      private int edtavCtlartefatos_link_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_6_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV14Caller ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String chkavCtlartefatos_obrigatorio_Internalname ;
      private String edtavCtlartefatos_codigo_Internalname ;
      private String edtavCtlartefatos_descricao_Internalname ;
      private String edtavCtlartefatos_evdcod_Internalname ;
      private String edtavCtlartefatos_anxcod_Internalname ;
      private String sGXsfl_6_fel_idx="0001" ;
      private String scmdbuf ;
      private String edtavCtlartefatos_link_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String bttConfirmar_Internalname ;
      private String bttConfirmar_Jsonclick ;
      private String bttCancelar_Internalname ;
      private String bttCancelar_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlartefatos_codigo_Jsonclick ;
      private String edtavCtlartefatos_descricao_Jsonclick ;
      private String edtavCtlartefatos_evdcod_Jsonclick ;
      private String edtavCtlartefatos_anxcod_Jsonclick ;
      private bool entryPointCalled ;
      private bool n1751Artefatos_Descricao ;
      private bool AV15InMemory ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private bool n1450Anexo_Link ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool gx_BV6 ;
      private bool AV13Pendencias ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String A1450Anexo_Link ;
      private String A1751Artefatos_Descricao ;
      private IGxSession AV20WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavCtlartefatos_obrigatorio ;
      private IDataStoreProvider pr_default ;
      private int[] H00NF2_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00NF2_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] H00NF2_n1773ContagemResultadoArtefato_AnxCod ;
      private String[] H00NF2_A1450Anexo_Link ;
      private bool[] H00NF2_n1450Anexo_Link ;
      private int[] H00NF2_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] H00NF2_n1770ContagemResultadoArtefato_EvdCod ;
      private String[] H00NF2_A1449ContagemResultadoEvidencia_Link ;
      private bool[] H00NF2_n1449ContagemResultadoEvidencia_Link ;
      private int[] H00NF2_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] H00NF2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00NF2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00NF2_A1772ContagemResultadoArtefato_OSCod ;
      private String[] H00NF2_A1751Artefatos_Descricao ;
      private bool[] H00NF2_n1751Artefatos_Descricao ;
      private int[] H00NF3_A160ContratoServicos_Codigo ;
      private int[] H00NF3_A1749Artefatos_Codigo ;
      private bool[] H00NF3_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] H00NF3_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] H00NF4_A586ContagemResultadoEvidencia_Codigo ;
      private int[] H00NF5_A1106Anexo_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV18Sdt_ContagemResultadoEvidencias ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Artefatos ))]
      private IGxCollection AV11sdt_Artefatos ;
      private GXWebForm Form ;
      private SdtAnexos AV5Anexos ;
      private SdtAnexos_De AV6AnexosDe ;
      private SdtContagemResultadoArtefato AV8ContagemResultadoArtefato ;
      private SdtContagemResultadoEvidencia AV9ContagemResultadoEvidencia ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV17Sdt_ArquivoEvidencia ;
      private SdtSDT_Artefatos AV10sdt_Artefato ;
   }

   public class wp_linkartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NF2 ;
          prmH00NF2 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NF3 ;
          prmH00NF3 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NF4 ;
          prmH00NF4 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NF5 ;
          prmH00NF5 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NF2", "SELECT T1.[ContagemResultadoArtefato_Codigo], T1.[ContagemResultadoArtefato_AnxCod] AS ContagemResultadoArtefato_AnxCod, T2.[Anexo_Link], T1.[ContagemResultadoArtefato_EvdCod] AS ContagemResultadoArtefato_EvdCod, T3.[ContagemResultadoEvidencia_Link], T1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, T5.[ContagemResultado_CntSrvCod], T1.[ContagemResultadoArtefato_OSCod] AS ContagemResultadoArtefato_OSCod, T4.[Artefatos_Descricao] FROM (((([ContagemResultadoArtefato] T1 WITH (NOLOCK) LEFT JOIN [Anexos] T2 WITH (NOLOCK) ON T2.[Anexo_Codigo] = T1.[ContagemResultadoArtefato_AnxCod]) LEFT JOIN [ContagemResultadoEvidencia] T3 WITH (NOLOCK) ON T3.[ContagemResultadoEvidencia_Codigo] = T1.[ContagemResultadoArtefato_EvdCod]) INNER JOIN [Artefatos] T4 WITH (NOLOCK) ON T4.[Artefatos_Codigo] = T1.[ContagemResultadoArtefato_ArtefatoCod]) INNER JOIN [ContagemResultado] T5 WITH (NOLOCK) ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultadoArtefato_OSCod]) WHERE T1.[ContagemResultadoArtefato_OSCod] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NF2,100,0,true,false )
             ,new CursorDef("H00NF3", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo], [ContratoServicosArtefato_Obrigatorio] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod and [Artefatos_Codigo] = @ContagemResultadoArtefato_ArtefatoCod ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NF3,1,0,false,true )
             ,new CursorDef("H00NF4", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoArtefato_EvdCod ORDER BY [ContagemResultadoEvidencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NF4,1,0,false,true )
             ,new CursorDef("H00NF5", "SELECT TOP 1 [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @ContagemResultadoArtefato_AnxCod ORDER BY [Anexo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NF5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
