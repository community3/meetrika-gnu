/*
               File: WP_NovoPerfil
        Description: Novo Perfil - GAM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:7.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_novoperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_novoperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_novoperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           long aP1_Id ,
                           int aP2_Perfil_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV11Id = aP1_Id;
         this.AV16Perfil_Codigo = aP2_Perfil_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavPerfil_ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11Id = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Id), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Id), "ZZZZZZZZZZZ9")));
                  AV16Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Perfil_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Perfil_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8N2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8N2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117381125");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_novoperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV11Id) + "," + UrlEncode("" +AV16Perfil_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL", AV15Perfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL", AV15Perfil);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV22WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV22WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Id), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Id), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Id), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Perfil_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8N2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8N2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_novoperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV11Id) + "," + UrlEncode("" +AV16Perfil_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_NovoPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Novo Perfil - GAM " ;
      }

      protected void WB8N0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_8N2( true) ;
         }
         else
         {
            wb_table1_3_8N2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_8N2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START8N2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Novo Perfil - GAM ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8N0( ) ;
      }

      protected void WS8N2( )
      {
         START8N2( ) ;
         EVT8N2( ) ;
      }

      protected void EVT8N2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118N2 */
                              E118N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E128N2 */
                                    E128N2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E138N2 */
                              E138N2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8N2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8N2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavPerfil_ativo.Name = "vPERFIL_ATIVO";
            chkavPerfil_ativo.WebTags = "";
            chkavPerfil_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPerfil_ativo_Internalname, "TitleCaption", chkavPerfil_ativo.Caption);
            chkavPerfil_ativo.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavName_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8N2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF8N2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E138N2 */
            E138N2 ();
            WB8N0( ) ;
         }
      }

      protected void STRUP8N0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118N2 */
         E118N2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV14Name = StringUtil.Upper( cgiGet( edtavName_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Name", AV14Name);
            AV23Perfil_Ativo = StringUtil.StrToBool( cgiGet( chkavPerfil_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Ativo", AV23Perfil_Ativo);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118N2 */
         E118N2 ();
         if (returnInSub) return;
      }

      protected void E118N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV22WWPContext) ;
         AV20SecurityPolicies = new SdtGAMRepository(context).getsecuritypolicies(AV9Filter, out  AV7Errors);
         AV28GXV1 = 1;
         while ( AV28GXV1 <= AV20SecurityPolicies.Count )
         {
            AV21SecurityPolicy = ((SdtGAMSecurityPolicy)AV20SecurityPolicies.Item(AV28GXV1));
            AV19SecPolId = AV21SecurityPolicy.gxTpr_Id;
            if (true) break;
            AV28GXV1 = (int)(AV28GXV1+1);
         }
         AV17Perfil_Tipo = 0;
         AV23Perfil_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Ativo", AV23Perfil_Ativo);
         AV18Role.load( 1);
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV15Perfil.Load(AV16Perfil_Codigo);
            AV17Perfil_Tipo = AV15Perfil.gxTpr_Perfil_tipo;
            AV24Perfil_GamId = AV15Perfil.gxTpr_Perfil_gamid;
            AV23Perfil_Ativo = AV15Perfil.gxTpr_Perfil_ativo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Ativo", AV23Perfil_Ativo);
            AV10GUID = AV18Role.gxTpr_Guid;
            AV14Name = AV15Perfil.gxTpr_Perfil_nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Name", AV14Name);
            AV5Dsc = AV18Role.gxTpr_Description;
            AV8ExtId = AV18Role.gxTpr_Externalid;
            AV19SecPolId = AV18Role.gxTpr_Securitypolicyid;
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtnconfirm_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
            {
               edtavName_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
               chkavPerfil_ativo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPerfil_ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavPerfil_ativo.Enabled), 5, 0)));
               bttBtnconfirm_Caption = "Eliminar";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E128N2 */
         E128N2 ();
         if (returnInSub) return;
      }

      protected void E128N2( )
      {
         /* Enter Routine */
         AV18Role.load( 1);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV14Name)) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
            {
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  AV15Perfil = new SdtPerfil(context);
               }
               AV15Perfil.gxTpr_Perfil_nome = AV14Name;
               AV15Perfil.gxTpr_Perfil_areatrabalhocod = AV22WWPContext.gxTpr_Areatrabalho_codigo;
               AV15Perfil.gxTpr_Perfil_tipo = 0;
               AV15Perfil.gxTpr_Perfil_gamguid = AV18Role.gxTpr_Guid;
               AV15Perfil.gxTpr_Perfil_gamid = AV18Role.gxTpr_Id;
               AV15Perfil.gxTpr_Perfil_ativo = AV23Perfil_Ativo;
               AV15Perfil.Save();
               if ( AV15Perfil.Fail() )
               {
                  context.RollbackDataStores( "WP_NovoPerfil");
                  /* Execute user subroutine: 'MOSTRA-ERROS-BC' */
                  S112 ();
                  if (returnInSub) return;
               }
               else
               {
                  context.CommitDataStores( "WP_NovoPerfil");
               }
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               AV15Perfil.Delete();
               if ( AV15Perfil.Fail() )
               {
                  context.RollbackDataStores( "WP_NovoPerfil");
                  /* Execute user subroutine: 'MOSTRA-ERROS-BC' */
                  S112 ();
                  if (returnInSub) return;
               }
               else
               {
                  context.CommitDataStores( "WP_NovoPerfil");
               }
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            GX_msglist.addItem("O nome do Perfil � obrigat�rio!");
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Perfil", AV15Perfil);
      }

      protected void S112( )
      {
         /* 'MOSTRA-ERROS-BC' Routine */
         AV13Messages = AV15Perfil.GetMessages();
         AV29GXV2 = 1;
         while ( AV29GXV2 <= AV13Messages.Count )
         {
            AV12Message = ((SdtMessages_Message)AV13Messages.Item(AV29GXV2));
            GX_msglist.addItem(StringUtil.Format( "%1 (BC %2)", AV12Message.gxTpr_Description, AV12Message.gxTpr_Id, "", "", "", "", "", "", ""));
            AV29GXV2 = (int)(AV29GXV2+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E138N2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_8N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr bgcolor=\"#F5F5F5\" bordercolor=\"#000000\"  class='Table'>") ;
            context.WriteHtmlText( "<td bgcolor=\"#F5F5F5\" bordercolor=\"#000000\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:40px;width:150px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, "Perfil", "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal; color:#000000; background-color:#F5F5F5;", "Title", 0, "", 1, 1, 0, "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='DataDescription'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Nome", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV14Name), StringUtil.RTrim( context.localUtil.Format( AV14Name, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "DataContentCell", "", "", "", 1, edtavName_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname2_Internalname, "Ativo", "", "", lblTbname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavPerfil_ativo_Internalname, StringUtil.BoolToStr( AV23Perfil_Ativo), "", "", 1, chkavPerfil_ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(22, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_8N2e( true) ;
         }
         else
         {
            wb_table1_3_8N2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         AV11Id = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Id), "ZZZZZZZZZZZ9")));
         AV16Perfil_Codigo = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Perfil_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8N2( ) ;
         WS8N2( ) ;
         WE8N2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117381282");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_novoperfil.js", "?20203117381283");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle_Internalname = "TBFORMTITLE";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbname2_Internalname = "TBNAME2";
         chkavPerfil_ativo_Internalname = "vPERFIL_ATIVO";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         bttBtnconfirm_Visible = 1;
         edtavName_Jsonclick = "";
         bttBtnconfirm_Caption = "Confirmar";
         chkavPerfil_ativo.Enabled = 1;
         edtavName_Enabled = 1;
         chkavPerfil_ativo.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Novo Perfil - GAM ";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E128N2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14Name',fld:'vNAME',pic:'@!',nv:''},{av:'AV15Perfil',fld:'vPERFIL',pic:'',nv:null},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Perfil_Ativo',fld:'vPERFIL_ATIVO',pic:'',nv:false}],oparms:[{av:'AV15Perfil',fld:'vPERFIL',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15Perfil = new SdtPerfil(context);
         AV22WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Name = "";
         AV23Perfil_Ativo = true;
         AV20SecurityPolicies = new GxExternalCollection( context, "SdtGAMSecurityPolicy", "GeneXus.Programs");
         AV9Filter = new SdtGAMSecurityPolicyFilter(context);
         AV7Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV21SecurityPolicy = new SdtGAMSecurityPolicy(context);
         AV18Role = new SdtGAMRole(context);
         AV10GUID = "";
         AV5Dsc = "";
         AV8ExtId = "";
         AV13Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV12Message = new SdtMessages_Message(context);
         sStyleString = "";
         lblTbformtitle_Jsonclick = "";
         lblTbname_Jsonclick = "";
         TempTags = "";
         lblTbname2_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_novoperfil__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV17Perfil_Tipo ;
      private short nGXWrapped ;
      private int AV16Perfil_Codigo ;
      private int wcpOAV16Perfil_Codigo ;
      private int AV28GXV1 ;
      private int AV19SecPolId ;
      private int bttBtnconfirm_Visible ;
      private int edtavName_Enabled ;
      private int AV29GXV2 ;
      private int idxLst ;
      private long AV11Id ;
      private long wcpOAV11Id ;
      private long AV24Perfil_GamId ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavPerfil_ativo_Internalname ;
      private String edtavName_Internalname ;
      private String AV14Name ;
      private String AV10GUID ;
      private String AV5Dsc ;
      private String AV8ExtId ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String sStyleString ;
      private String tblTblmain_Internalname ;
      private String lblTbformtitle_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String TempTags ;
      private String edtavName_Jsonclick ;
      private String lblTbname2_Internalname ;
      private String lblTbname2_Jsonclick ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV23Perfil_Ativo ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavPerfil_ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV7Errors ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV13Messages ;
      [ObjectCollection(ItemType=typeof( SdtGAMSecurityPolicy ))]
      private IGxCollection AV20SecurityPolicies ;
      private GXWebForm Form ;
      private SdtGAMSecurityPolicyFilter AV9Filter ;
      private SdtMessages_Message AV12Message ;
      private SdtPerfil AV15Perfil ;
      private SdtGAMRole AV18Role ;
      private SdtGAMSecurityPolicy AV21SecurityPolicy ;
      private wwpbaseobjects.SdtWWPContext AV22WWPContext ;
   }

   public class wp_novoperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
