/*
               File: SolicitacaoMudancaItemGeneral
        Description: Solicitacao Mudanca Item General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:6.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaomudancaitemgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public solicitacaomudancaitemgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public solicitacaomudancaitemgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SolicitacaoMudanca_Codigo ,
                           int aP1_SolicitacaoMudancaItem_FuncaoAPF )
      {
         this.A996SolicitacaoMudanca_Codigo = aP0_SolicitacaoMudanca_Codigo;
         this.A995SolicitacaoMudancaItem_FuncaoAPF = aP1_SolicitacaoMudancaItem_FuncaoAPF;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A996SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  A995SolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A996SolicitacaoMudanca_Codigo,(int)A995SolicitacaoMudancaItem_FuncaoAPF});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAH52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "SolicitacaoMudancaItemGeneral";
               context.Gx_err = 0;
               /* Using cursor H00H52 */
               pr_default.execute(0, new Object[] {A996SolicitacaoMudanca_Codigo});
               A993SolicitacaoMudanca_SistemaCod = H00H52_A993SolicitacaoMudanca_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
               n993SolicitacaoMudanca_SistemaCod = H00H52_n993SolicitacaoMudanca_SistemaCod[0];
               pr_default.close(0);
               WSH52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Solicitacao Mudanca Item General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311726696");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaomudancaitemgeneral.aspx") + "?" + UrlEncode("" +A996SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +A995SolicitacaoMudancaItem_FuncaoAPF)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO", A998SolicitacaoMudancaItem_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA996SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled", StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormH52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("solicitacaomudancaitemgeneral.js", "?2020311726697");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoMudancaItemGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacao Mudanca Item General" ;
      }

      protected void WBH50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "solicitacaomudancaitemgeneral.aspx");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            }
            wb_table1_2_H52( true) ;
         }
         else
         {
            wb_table1_2_H52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSolicitacaoMudanca_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaItemGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTH52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Solicitacao Mudanca Item General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPH50( ) ;
            }
         }
      }

      protected void WSH52( )
      {
         STARTH52( ) ;
         EVTH52( ) ;
      }

      protected void EVTH52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11H52 */
                                    E11H52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12H52 */
                                    E12H52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13H52 */
                                    E13H52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14H52 */
                                    E14H52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPH50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEH52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormH52( ) ;
            }
         }
      }

      protected void PAH52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "SolicitacaoMudancaItemGeneral";
         context.Gx_err = 0;
      }

      protected void RFH52( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00H53 */
            pr_default.execute(1, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A998SolicitacaoMudancaItem_Descricao = H00H53_A998SolicitacaoMudancaItem_Descricao[0];
               /* Execute user event: E12H52 */
               E12H52 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBH50( ) ;
         }
      }

      protected void STRUPH50( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "SolicitacaoMudancaItemGeneral";
         context.Gx_err = 0;
         /* Using cursor H00H54 */
         pr_default.execute(2, new Object[] {A996SolicitacaoMudanca_Codigo});
         A993SolicitacaoMudanca_SistemaCod = H00H54_A993SolicitacaoMudanca_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         n993SolicitacaoMudanca_SistemaCod = H00H54_n993SolicitacaoMudanca_SistemaCod[0];
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11H52 */
         E11H52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", "."));
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            /* Read saved values. */
            A998SolicitacaoMudancaItem_Descricao = cgiGet( sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO");
            wcpOA996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA996SolicitacaoMudanca_Codigo"), ",", "."));
            wcpOA995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA995SolicitacaoMudancaItem_FuncaoAPF"), ",", "."));
            Solicitacaomudancaitem_descricao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11H52 */
         E11H52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11H52( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12H52( )
      {
         /* Load Routine */
         edtSolicitacaoMudanca_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSolicitacaoMudanca_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Visible), 5, 0)));
      }

      protected void E13H52( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A996SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +A995SolicitacaoMudancaItem_FuncaoAPF);
         context.wjLocDisableFrm = 1;
      }

      protected void E14H52( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A996SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +A995SolicitacaoMudancaItem_FuncaoAPF);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "SolicitacaoMudancaItem";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "SolicitacaoMudanca_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "SolicitacaoMudancaItem_FuncaoAPF";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8SolicitacaoMudancaItem_FuncaoAPF), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_H52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_H52( true) ;
         }
         else
         {
            wb_table2_8_H52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_H52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_H52( true) ;
         }
         else
         {
            wb_table3_26_H52( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_H52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H52e( true) ;
         }
         else
         {
            wb_table1_2_H52e( false) ;
         }
      }

      protected void wb_table3_26_H52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_H52e( true) ;
         }
         else
         {
            wb_table3_26_H52e( false) ;
         }
      }

      protected void wb_table2_8_H52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudanca_sistemacod_Internalname, "Sistema", "", "", lblTextblocksolicitacaomudanca_sistemacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_SistemaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname, "Item", "", "", lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudancaitem_descricao_Internalname, "", "", "", lblTextblocksolicitacaomudancaitem_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_H52e( true) ;
         }
         else
         {
            wb_table2_8_H52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A996SolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         A995SolicitacaoMudancaItem_FuncaoAPF = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH52( ) ;
         WSH52( ) ;
         WEH52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA996SolicitacaoMudanca_Codigo = (String)((String)getParm(obj,0));
         sCtrlA995SolicitacaoMudancaItem_FuncaoAPF = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAH52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "solicitacaomudancaitemgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAH52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A996SolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            A995SolicitacaoMudancaItem_FuncaoAPF = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         }
         wcpOA996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA996SolicitacaoMudanca_Codigo"), ",", "."));
         wcpOA995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA995SolicitacaoMudancaItem_FuncaoAPF"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A996SolicitacaoMudanca_Codigo != wcpOA996SolicitacaoMudanca_Codigo ) || ( A995SolicitacaoMudancaItem_FuncaoAPF != wcpOA995SolicitacaoMudancaItem_FuncaoAPF ) ) )
         {
            setjustcreated();
         }
         wcpOA996SolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
         wcpOA995SolicitacaoMudancaItem_FuncaoAPF = A995SolicitacaoMudancaItem_FuncaoAPF;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA996SolicitacaoMudanca_Codigo = cgiGet( sPrefix+"A996SolicitacaoMudanca_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA996SolicitacaoMudanca_Codigo) > 0 )
         {
            A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA996SolicitacaoMudanca_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         }
         else
         {
            A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A996SolicitacaoMudanca_Codigo_PARM"), ",", "."));
         }
         sCtrlA995SolicitacaoMudancaItem_FuncaoAPF = cgiGet( sPrefix+"A995SolicitacaoMudancaItem_FuncaoAPF_CTRL");
         if ( StringUtil.Len( sCtrlA995SolicitacaoMudancaItem_FuncaoAPF) > 0 )
         {
            A995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( sCtrlA995SolicitacaoMudancaItem_FuncaoAPF), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         }
         else
         {
            A995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A995SolicitacaoMudancaItem_FuncaoAPF_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAH52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSH52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSH52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A996SolicitacaoMudanca_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA996SolicitacaoMudanca_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A996SolicitacaoMudanca_Codigo_CTRL", StringUtil.RTrim( sCtrlA996SolicitacaoMudanca_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A995SolicitacaoMudancaItem_FuncaoAPF_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA995SolicitacaoMudancaItem_FuncaoAPF)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A995SolicitacaoMudancaItem_FuncaoAPF_CTRL", StringUtil.RTrim( sCtrlA995SolicitacaoMudancaItem_FuncaoAPF));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEH52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311726720");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("solicitacaomudancaitemgeneral.js", "?2020311726720");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicitacaomudanca_sistemacod_Internalname = sPrefix+"TEXTBLOCKSOLICITACAOMUDANCA_SISTEMACOD";
         edtSolicitacaoMudanca_SistemaCod_Internalname = sPrefix+"SOLICITACAOMUDANCA_SISTEMACOD";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname = sPrefix+"TEXTBLOCKSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         lblTextblocksolicitacaomudancaitem_descricao_Internalname = sPrefix+"TEXTBLOCKSOLICITACAOMUDANCAITEM_DESCRICAO";
         Solicitacaomudancaitem_descricao_Internalname = sPrefix+"SOLICITACAOMUDANCAITEM_DESCRICAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtSolicitacaoMudanca_Codigo_Internalname = sPrefix+"SOLICITACAOMUDANCA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         Solicitacaomudancaitem_descricao_Enabled = Convert.ToBoolean( 0);
         edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick = "";
         edtSolicitacaoMudanca_SistemaCod_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13H52',iparms:[{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14H52',iparms:[{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H00H52_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         H00H52_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A998SolicitacaoMudancaItem_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00H53_A996SolicitacaoMudanca_Codigo = new int[1] ;
         H00H53_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         H00H53_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         H00H53_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         H00H53_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         H00H54_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         H00H54_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocksolicitacaomudanca_sistemacod_Jsonclick = "";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         lblTextblocksolicitacaomudancaitem_descricao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA996SolicitacaoMudanca_Codigo = "";
         sCtrlA995SolicitacaoMudancaItem_FuncaoAPF = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaomudancaitemgeneral__default(),
            new Object[][] {
                new Object[] {
               H00H52_A993SolicitacaoMudanca_SistemaCod, H00H52_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               H00H53_A996SolicitacaoMudanca_Codigo, H00H53_A995SolicitacaoMudancaItem_FuncaoAPF, H00H53_A998SolicitacaoMudancaItem_Descricao, H00H53_A993SolicitacaoMudanca_SistemaCod, H00H53_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               H00H54_A993SolicitacaoMudanca_SistemaCod, H00H54_n993SolicitacaoMudanca_SistemaCod
               }
            }
         );
         AV15Pgmname = "SolicitacaoMudancaItemGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "SolicitacaoMudancaItemGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int wcpOA996SolicitacaoMudanca_Codigo ;
      private int wcpOA995SolicitacaoMudancaItem_FuncaoAPF ;
      private int A993SolicitacaoMudanca_SistemaCod ;
      private int edtSolicitacaoMudanca_Codigo_Visible ;
      private int AV7SolicitacaoMudanca_Codigo ;
      private int AV8SolicitacaoMudancaItem_FuncaoAPF ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtSolicitacaoMudanca_Codigo_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtSolicitacaoMudanca_SistemaCod_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Internalname ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Jsonclick ;
      private String edtSolicitacaoMudanca_SistemaCod_Jsonclick ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Internalname ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick ;
      private String lblTextblocksolicitacaomudancaitem_descricao_Internalname ;
      private String lblTextblocksolicitacaomudancaitem_descricao_Jsonclick ;
      private String sCtrlA996SolicitacaoMudanca_Codigo ;
      private String sCtrlA995SolicitacaoMudancaItem_FuncaoAPF ;
      private String Solicitacaomudancaitem_descricao_Internalname ;
      private bool entryPointCalled ;
      private bool n993SolicitacaoMudanca_SistemaCod ;
      private bool toggleJsOutput ;
      private bool Solicitacaomudancaitem_descricao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A998SolicitacaoMudancaItem_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00H52_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] H00H52_n993SolicitacaoMudanca_SistemaCod ;
      private int[] H00H53_A996SolicitacaoMudanca_Codigo ;
      private int[] H00H53_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private String[] H00H53_A998SolicitacaoMudancaItem_Descricao ;
      private int[] H00H53_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] H00H53_n993SolicitacaoMudanca_SistemaCod ;
      private int[] H00H54_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] H00H54_n993SolicitacaoMudanca_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class solicitacaomudancaitemgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H52 ;
          prmH00H52 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H53 ;
          prmH00H53 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00H54 ;
          prmH00H54 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H52", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H52,1,0,true,true )
             ,new CursorDef("H00H53", "SELECT T1.[SolicitacaoMudanca_Codigo], T1.[SolicitacaoMudancaItem_FuncaoAPF], T1.[SolicitacaoMudancaItem_Descricao], T2.[SolicitacaoMudanca_SistemaCod] FROM ([SolicitacaoMudancaItem] T1 WITH (NOLOCK) INNER JOIN [SolicitacaoMudanca] T2 WITH (NOLOCK) ON T2.[SolicitacaoMudanca_Codigo] = T1.[SolicitacaoMudanca_Codigo]) WHERE T1.[SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo and T1.[SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF ORDER BY T1.[SolicitacaoMudanca_Codigo], T1.[SolicitacaoMudancaItem_FuncaoAPF] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H53,1,0,true,true )
             ,new CursorDef("H00H54", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H54,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
