/*
               File: PromptTipoProjeto
        Description: Selecione Tipos de Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:54:46.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prompttipoprojeto : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public prompttipoprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prompttipoprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutTipoProjeto_Codigo ,
                           ref String aP1_InOutTipoProjeto_Nome )
      {
         this.AV7InOutTipoProjeto_Codigo = aP0_InOutTipoProjeto_Codigo;
         this.AV8InOutTipoProjeto_Nome = aP1_InOutTipoProjeto_Nome;
         executePrivate();
         aP0_InOutTipoProjeto_Codigo=this.AV7InOutTipoProjeto_Codigo;
         aP1_InOutTipoProjeto_Nome=this.AV8InOutTipoProjeto_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17TipoProjeto_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21TipoProjeto_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25TipoProjeto_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFTipoProjeto_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
               AV32TFTipoProjeto_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFTipoProjeto_Nome_Sel", AV32TFTipoProjeto_Nome_Sel);
               AV35TFTipoProjeto_Prazo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2)));
               AV36TFTipoProjeto_Prazo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2)));
               AV39TFTipoProjeto_CnstExp = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFTipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2)));
               AV40TFTipoProjeto_CnstExp_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoProjeto_CnstExp_To", StringUtil.LTrim( StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2)));
               AV33ddo_TipoProjeto_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_TipoProjeto_NomeTitleControlIdToReplace", AV33ddo_TipoProjeto_NomeTitleControlIdToReplace);
               AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace", AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace);
               AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace", AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace);
               AV49Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutTipoProjeto_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTipoProjeto_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutTipoProjeto_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTipoProjeto_Nome", AV8InOutTipoProjeto_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAGQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV49Pgmname = "PromptTipoProjeto";
               context.Gx_err = 0;
               WSGQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEGQ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118544630");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("prompttipoprojeto.aspx") + "?" + UrlEncode("" +AV7InOutTipoProjeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutTipoProjeto_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPOPROJETO_NOME1", StringUtil.RTrim( AV17TipoProjeto_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPOPROJETO_NOME2", StringUtil.RTrim( AV21TipoProjeto_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPOPROJETO_NOME3", StringUtil.RTrim( AV25TipoProjeto_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_NOME", StringUtil.RTrim( AV31TFTipoProjeto_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_NOME_SEL", StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_PRAZO", StringUtil.LTrim( StringUtil.NToC( AV35TFTipoProjeto_Prazo, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( AV36TFTipoProjeto_Prazo_To, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_CNSTEXP", StringUtil.LTrim( StringUtil.NToC( AV39TFTipoProjeto_CnstExp, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPOPROJETO_CNSTEXP_TO", StringUtil.LTrim( StringUtil.NToC( AV40TFTipoProjeto_CnstExp_To, 5, 2, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOPROJETO_NOMETITLEFILTERDATA", AV30TipoProjeto_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOPROJETO_NOMETITLEFILTERDATA", AV30TipoProjeto_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOPROJETO_PRAZOTITLEFILTERDATA", AV34TipoProjeto_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOPROJETO_PRAZOTITLEFILTERDATA", AV34TipoProjeto_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOPROJETO_CNSTEXPTITLEFILTERDATA", AV38TipoProjeto_CnstExpTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOPROJETO_CNSTEXPTITLEFILTERDATA", AV38TipoProjeto_CnstExpTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV49Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTTIPOPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutTipoProjeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTTIPOPROJETO_NOME", StringUtil.RTrim( AV8InOutTipoProjeto_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Caption", StringUtil.RTrim( Ddo_tipoprojeto_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Tooltip", StringUtil.RTrim( Ddo_tipoprojeto_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Cls", StringUtil.RTrim( Ddo_tipoprojeto_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tipoprojeto_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tipoprojeto_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipoprojeto_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipoprojeto_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tipoprojeto_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tipoprojeto_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tipoprojeto_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tipoprojeto_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Filtertype", StringUtil.RTrim( Ddo_tipoprojeto_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tipoprojeto_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tipoprojeto_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Datalisttype", StringUtil.RTrim( Ddo_tipoprojeto_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Datalistproc", StringUtil.RTrim( Ddo_tipoprojeto_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipoprojeto_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Sortasc", StringUtil.RTrim( Ddo_tipoprojeto_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Sortdsc", StringUtil.RTrim( Ddo_tipoprojeto_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Loadingdata", StringUtil.RTrim( Ddo_tipoprojeto_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tipoprojeto_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tipoprojeto_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tipoprojeto_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Caption", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Tooltip", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Cls", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_tipoprojeto_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_tipoprojeto_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Sortedstatus", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_tipoprojeto_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filtertype", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_tipoprojeto_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_tipoprojeto_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Sortasc", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Sortdsc", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Caption", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Tooltip", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Cls", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filteredtext_set", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filteredtextto_set", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Includesortasc", StringUtil.BoolToStr( Ddo_tipoprojeto_cnstexp_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Includesortdsc", StringUtil.BoolToStr( Ddo_tipoprojeto_cnstexp_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Sortedstatus", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Includefilter", StringUtil.BoolToStr( Ddo_tipoprojeto_cnstexp_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filtertype", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filterisrange", StringUtil.BoolToStr( Ddo_tipoprojeto_cnstexp_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Includedatalist", StringUtil.BoolToStr( Ddo_tipoprojeto_cnstexp_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Sortasc", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Sortdsc", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Cleanfilter", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Rangefilterfrom", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Rangefilterto", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Searchbuttontext", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tipoprojeto_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tipoprojeto_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tipoprojeto_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_tipoprojeto_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Activeeventkey", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filteredtext_get", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPOPROJETO_CNSTEXP_Filteredtextto_get", StringUtil.RTrim( Ddo_tipoprojeto_cnstexp_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGQ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptTipoProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Tipos de Projeto" ;
      }

      protected void WBGQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GQ2( true) ;
         }
         else
         {
            wb_table1_2_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(90, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_nome_Internalname, StringUtil.RTrim( AV31TFTipoProjeto_Nome), StringUtil.RTrim( context.localUtil.Format( AV31TFTipoProjeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTipoProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_nome_sel_Internalname, StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV32TFTipoProjeto_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTipoProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV35TFTipoProjeto_Prazo, 5, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV35TFTipoProjeto_Prazo, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_prazo_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptTipoProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV36TFTipoProjeto_Prazo_To, 5, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV36TFTipoProjeto_Prazo_To, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_prazo_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptTipoProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_cnstexp_Internalname, StringUtil.LTrim( StringUtil.NToC( AV39TFTipoProjeto_CnstExp, 5, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV39TFTipoProjeto_CnstExp, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_cnstexp_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_cnstexp_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptTipoProjeto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipoprojeto_cnstexp_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV40TFTipoProjeto_CnstExp_To, 5, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV40TFTipoProjeto_CnstExp_To, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipoprojeto_cnstexp_to_Jsonclick, 0, "Attribute", "", "", "", edtavTftipoprojeto_cnstexp_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptTipoProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOPROJETO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Internalname, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptTipoProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOPROJETO_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Internalname, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptTipoProjeto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPOPROJETO_CNSTEXPContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Internalname, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptTipoProjeto.htm");
         }
         wbLoad = true;
      }

      protected void STARTGQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Tipos de Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGQ0( ) ;
      }

      protected void WSGQ2( )
      {
         STARTGQ2( ) ;
         EVTGQ2( ) ;
      }

      protected void EVTGQ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11GQ2 */
                           E11GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOPROJETO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12GQ2 */
                           E12GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOPROJETO_PRAZO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13GQ2 */
                           E13GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TIPOPROJETO_CNSTEXP.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14GQ2 */
                           E14GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15GQ2 */
                           E15GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16GQ2 */
                           E16GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17GQ2 */
                           E17GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18GQ2 */
                           E18GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19GQ2 */
                           E19GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20GQ2 */
                           E20GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21GQ2 */
                           E21GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22GQ2 */
                           E22GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23GQ2 */
                           E23GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24GQ2 */
                           E24GQ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV48Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A979TipoProjeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoProjeto_Codigo_Internalname), ",", "."));
                           A980TipoProjeto_Nome = StringUtil.Upper( cgiGet( edtTipoProjeto_Nome_Internalname));
                           A981TipoProjeto_Prazo = context.localUtil.CToN( cgiGet( edtTipoProjeto_Prazo_Internalname), ",", ".");
                           A982TipoProjeto_CnstExp = context.localUtil.CToN( cgiGet( edtTipoProjeto_CnstExp_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25GQ2 */
                                 E25GQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26GQ2 */
                                 E26GQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27GQ2 */
                                 E27GQ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipoprojeto_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME1"), AV17TipoProjeto_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipoprojeto_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME2"), AV21TipoProjeto_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipoprojeto_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME3"), AV25TipoProjeto_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOPROJETO_NOME"), AV31TFTipoProjeto_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOPROJETO_NOME_SEL"), AV32TFTipoProjeto_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_prazo Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_PRAZO"), ",", ".") != AV35TFTipoProjeto_Prazo )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_prazo_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_PRAZO_TO"), ",", ".") != AV36TFTipoProjeto_Prazo_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_cnstexp Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_CNSTEXP"), ",", ".") != AV39TFTipoProjeto_CnstExp )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipoprojeto_cnstexp_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_CNSTEXP_TO"), ",", ".") != AV40TFTipoProjeto_CnstExp_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E28GQ2 */
                                       E28GQ2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEGQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGQ2( ) ;
            }
         }
      }

      protected void PAGQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TIPOPROJETO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("TIPOPROJETO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("TIPOPROJETO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17TipoProjeto_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21TipoProjeto_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25TipoProjeto_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV31TFTipoProjeto_Nome ,
                                       String AV32TFTipoProjeto_Nome_Sel ,
                                       decimal AV35TFTipoProjeto_Prazo ,
                                       decimal AV36TFTipoProjeto_Prazo_To ,
                                       decimal AV39TFTipoProjeto_CnstExp ,
                                       decimal AV40TFTipoProjeto_CnstExp_To ,
                                       String AV33ddo_TipoProjeto_NomeTitleControlIdToReplace ,
                                       String AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace ,
                                       String AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace ,
                                       String AV49Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TIPOPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A979TipoProjeto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A980TipoProjeto_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TIPOPROJETO_NOME", StringUtil.RTrim( A980TipoProjeto_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_PRAZO", GetSecureSignedToken( "", context.localUtil.Format( A981TipoProjeto_Prazo, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "TIPOPROJETO_PRAZO", StringUtil.LTrim( StringUtil.NToC( A981TipoProjeto_Prazo, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_CNSTEXP", GetSecureSignedToken( "", context.localUtil.Format( A982TipoProjeto_CnstExp, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "TIPOPROJETO_CNSTEXP", StringUtil.LTrim( StringUtil.NToC( A982TipoProjeto_CnstExp, 5, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV49Pgmname = "PromptTipoProjeto";
         context.Gx_err = 0;
      }

      protected void RFGQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E26GQ2 */
         E26GQ2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17TipoProjeto_Nome1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21TipoProjeto_Nome2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25TipoProjeto_Nome3 ,
                                                 AV32TFTipoProjeto_Nome_Sel ,
                                                 AV31TFTipoProjeto_Nome ,
                                                 AV35TFTipoProjeto_Prazo ,
                                                 AV36TFTipoProjeto_Prazo_To ,
                                                 AV39TFTipoProjeto_CnstExp ,
                                                 AV40TFTipoProjeto_CnstExp_To ,
                                                 A980TipoProjeto_Nome ,
                                                 A981TipoProjeto_Prazo ,
                                                 A982TipoProjeto_CnstExp ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17TipoProjeto_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
            lV17TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17TipoProjeto_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
            lV21TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21TipoProjeto_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
            lV21TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21TipoProjeto_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
            lV25TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV25TipoProjeto_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
            lV25TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV25TipoProjeto_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
            lV31TFTipoProjeto_Nome = StringUtil.PadR( StringUtil.RTrim( AV31TFTipoProjeto_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
            /* Using cursor H00GQ2 */
            pr_default.execute(0, new Object[] {lV17TipoProjeto_Nome1, lV17TipoProjeto_Nome1, lV21TipoProjeto_Nome2, lV21TipoProjeto_Nome2, lV25TipoProjeto_Nome3, lV25TipoProjeto_Nome3, lV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A982TipoProjeto_CnstExp = H00GQ2_A982TipoProjeto_CnstExp[0];
               A981TipoProjeto_Prazo = H00GQ2_A981TipoProjeto_Prazo[0];
               A980TipoProjeto_Nome = H00GQ2_A980TipoProjeto_Nome[0];
               A979TipoProjeto_Codigo = H00GQ2_A979TipoProjeto_Codigo[0];
               /* Execute user event: E27GQ2 */
               E27GQ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBGQ0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17TipoProjeto_Nome1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21TipoProjeto_Nome2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25TipoProjeto_Nome3 ,
                                              AV32TFTipoProjeto_Nome_Sel ,
                                              AV31TFTipoProjeto_Nome ,
                                              AV35TFTipoProjeto_Prazo ,
                                              AV36TFTipoProjeto_Prazo_To ,
                                              AV39TFTipoProjeto_CnstExp ,
                                              AV40TFTipoProjeto_CnstExp_To ,
                                              A980TipoProjeto_Nome ,
                                              A981TipoProjeto_Prazo ,
                                              A982TipoProjeto_CnstExp ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17TipoProjeto_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
         lV17TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17TipoProjeto_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
         lV21TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21TipoProjeto_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
         lV21TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV21TipoProjeto_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
         lV25TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV25TipoProjeto_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
         lV25TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV25TipoProjeto_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
         lV31TFTipoProjeto_Nome = StringUtil.PadR( StringUtil.RTrim( AV31TFTipoProjeto_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
         /* Using cursor H00GQ3 */
         pr_default.execute(1, new Object[] {lV17TipoProjeto_Nome1, lV17TipoProjeto_Nome1, lV21TipoProjeto_Nome2, lV21TipoProjeto_Nome2, lV25TipoProjeto_Nome3, lV25TipoProjeto_Nome3, lV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To});
         GRID_nRecordCount = H00GQ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV49Pgmname = "PromptTipoProjeto";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25GQ2 */
         E25GQ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV42DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOPROJETO_NOMETITLEFILTERDATA"), AV30TipoProjeto_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOPROJETO_PRAZOTITLEFILTERDATA"), AV34TipoProjeto_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPOPROJETO_CNSTEXPTITLEFILTERDATA"), AV38TipoProjeto_CnstExpTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17TipoProjeto_Nome1 = StringUtil.Upper( cgiGet( edtavTipoprojeto_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21TipoProjeto_Nome2 = StringUtil.Upper( cgiGet( edtavTipoprojeto_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25TipoProjeto_Nome3 = StringUtil.Upper( cgiGet( edtavTipoprojeto_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV31TFTipoProjeto_Nome = StringUtil.Upper( cgiGet( edtavTftipoprojeto_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
            AV32TFTipoProjeto_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftipoprojeto_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFTipoProjeto_Nome_Sel", AV32TFTipoProjeto_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_Internalname), ",", ".") > 99.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOPROJETO_PRAZO");
               GX_FocusControl = edtavTftipoprojeto_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFTipoProjeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2)));
            }
            else
            {
               AV35TFTipoProjeto_Prazo = context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_to_Internalname), ",", ".") > 99.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOPROJETO_PRAZO_TO");
               GX_FocusControl = edtavTftipoprojeto_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFTipoProjeto_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2)));
            }
            else
            {
               AV36TFTipoProjeto_Prazo_To = context.localUtil.CToN( cgiGet( edtavTftipoprojeto_prazo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_Internalname), ",", ".") > 99.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOPROJETO_CNSTEXP");
               GX_FocusControl = edtavTftipoprojeto_cnstexp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFTipoProjeto_CnstExp = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFTipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2)));
            }
            else
            {
               AV39TFTipoProjeto_CnstExp = context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFTipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_to_Internalname), ",", ".") > 99.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPOPROJETO_CNSTEXP_TO");
               GX_FocusControl = edtavTftipoprojeto_cnstexp_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFTipoProjeto_CnstExp_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoProjeto_CnstExp_To", StringUtil.LTrim( StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2)));
            }
            else
            {
               AV40TFTipoProjeto_CnstExp_To = context.localUtil.CToN( cgiGet( edtavTftipoprojeto_cnstexp_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoProjeto_CnstExp_To", StringUtil.LTrim( StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2)));
            }
            AV33ddo_TipoProjeto_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_TipoProjeto_NomeTitleControlIdToReplace", AV33ddo_TipoProjeto_NomeTitleControlIdToReplace);
            AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace", AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace);
            AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace = cgiGet( edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace", AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV44GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV45GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tipoprojeto_nome_Caption = cgiGet( "DDO_TIPOPROJETO_NOME_Caption");
            Ddo_tipoprojeto_nome_Tooltip = cgiGet( "DDO_TIPOPROJETO_NOME_Tooltip");
            Ddo_tipoprojeto_nome_Cls = cgiGet( "DDO_TIPOPROJETO_NOME_Cls");
            Ddo_tipoprojeto_nome_Filteredtext_set = cgiGet( "DDO_TIPOPROJETO_NOME_Filteredtext_set");
            Ddo_tipoprojeto_nome_Selectedvalue_set = cgiGet( "DDO_TIPOPROJETO_NOME_Selectedvalue_set");
            Ddo_tipoprojeto_nome_Dropdownoptionstype = cgiGet( "DDO_TIPOPROJETO_NOME_Dropdownoptionstype");
            Ddo_tipoprojeto_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOPROJETO_NOME_Titlecontrolidtoreplace");
            Ddo_tipoprojeto_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_NOME_Includesortasc"));
            Ddo_tipoprojeto_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_NOME_Includesortdsc"));
            Ddo_tipoprojeto_nome_Sortedstatus = cgiGet( "DDO_TIPOPROJETO_NOME_Sortedstatus");
            Ddo_tipoprojeto_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_NOME_Includefilter"));
            Ddo_tipoprojeto_nome_Filtertype = cgiGet( "DDO_TIPOPROJETO_NOME_Filtertype");
            Ddo_tipoprojeto_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_NOME_Filterisrange"));
            Ddo_tipoprojeto_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_NOME_Includedatalist"));
            Ddo_tipoprojeto_nome_Datalisttype = cgiGet( "DDO_TIPOPROJETO_NOME_Datalisttype");
            Ddo_tipoprojeto_nome_Datalistproc = cgiGet( "DDO_TIPOPROJETO_NOME_Datalistproc");
            Ddo_tipoprojeto_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPOPROJETO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipoprojeto_nome_Sortasc = cgiGet( "DDO_TIPOPROJETO_NOME_Sortasc");
            Ddo_tipoprojeto_nome_Sortdsc = cgiGet( "DDO_TIPOPROJETO_NOME_Sortdsc");
            Ddo_tipoprojeto_nome_Loadingdata = cgiGet( "DDO_TIPOPROJETO_NOME_Loadingdata");
            Ddo_tipoprojeto_nome_Cleanfilter = cgiGet( "DDO_TIPOPROJETO_NOME_Cleanfilter");
            Ddo_tipoprojeto_nome_Noresultsfound = cgiGet( "DDO_TIPOPROJETO_NOME_Noresultsfound");
            Ddo_tipoprojeto_nome_Searchbuttontext = cgiGet( "DDO_TIPOPROJETO_NOME_Searchbuttontext");
            Ddo_tipoprojeto_prazo_Caption = cgiGet( "DDO_TIPOPROJETO_PRAZO_Caption");
            Ddo_tipoprojeto_prazo_Tooltip = cgiGet( "DDO_TIPOPROJETO_PRAZO_Tooltip");
            Ddo_tipoprojeto_prazo_Cls = cgiGet( "DDO_TIPOPROJETO_PRAZO_Cls");
            Ddo_tipoprojeto_prazo_Filteredtext_set = cgiGet( "DDO_TIPOPROJETO_PRAZO_Filteredtext_set");
            Ddo_tipoprojeto_prazo_Filteredtextto_set = cgiGet( "DDO_TIPOPROJETO_PRAZO_Filteredtextto_set");
            Ddo_tipoprojeto_prazo_Dropdownoptionstype = cgiGet( "DDO_TIPOPROJETO_PRAZO_Dropdownoptionstype");
            Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOPROJETO_PRAZO_Titlecontrolidtoreplace");
            Ddo_tipoprojeto_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_PRAZO_Includesortasc"));
            Ddo_tipoprojeto_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_PRAZO_Includesortdsc"));
            Ddo_tipoprojeto_prazo_Sortedstatus = cgiGet( "DDO_TIPOPROJETO_PRAZO_Sortedstatus");
            Ddo_tipoprojeto_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_PRAZO_Includefilter"));
            Ddo_tipoprojeto_prazo_Filtertype = cgiGet( "DDO_TIPOPROJETO_PRAZO_Filtertype");
            Ddo_tipoprojeto_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_PRAZO_Filterisrange"));
            Ddo_tipoprojeto_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_PRAZO_Includedatalist"));
            Ddo_tipoprojeto_prazo_Sortasc = cgiGet( "DDO_TIPOPROJETO_PRAZO_Sortasc");
            Ddo_tipoprojeto_prazo_Sortdsc = cgiGet( "DDO_TIPOPROJETO_PRAZO_Sortdsc");
            Ddo_tipoprojeto_prazo_Cleanfilter = cgiGet( "DDO_TIPOPROJETO_PRAZO_Cleanfilter");
            Ddo_tipoprojeto_prazo_Rangefilterfrom = cgiGet( "DDO_TIPOPROJETO_PRAZO_Rangefilterfrom");
            Ddo_tipoprojeto_prazo_Rangefilterto = cgiGet( "DDO_TIPOPROJETO_PRAZO_Rangefilterto");
            Ddo_tipoprojeto_prazo_Searchbuttontext = cgiGet( "DDO_TIPOPROJETO_PRAZO_Searchbuttontext");
            Ddo_tipoprojeto_cnstexp_Caption = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Caption");
            Ddo_tipoprojeto_cnstexp_Tooltip = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Tooltip");
            Ddo_tipoprojeto_cnstexp_Cls = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Cls");
            Ddo_tipoprojeto_cnstexp_Filteredtext_set = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filteredtext_set");
            Ddo_tipoprojeto_cnstexp_Filteredtextto_set = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filteredtextto_set");
            Ddo_tipoprojeto_cnstexp_Dropdownoptionstype = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Dropdownoptionstype");
            Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Titlecontrolidtoreplace");
            Ddo_tipoprojeto_cnstexp_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Includesortasc"));
            Ddo_tipoprojeto_cnstexp_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Includesortdsc"));
            Ddo_tipoprojeto_cnstexp_Sortedstatus = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Sortedstatus");
            Ddo_tipoprojeto_cnstexp_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Includefilter"));
            Ddo_tipoprojeto_cnstexp_Filtertype = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filtertype");
            Ddo_tipoprojeto_cnstexp_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filterisrange"));
            Ddo_tipoprojeto_cnstexp_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Includedatalist"));
            Ddo_tipoprojeto_cnstexp_Sortasc = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Sortasc");
            Ddo_tipoprojeto_cnstexp_Sortdsc = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Sortdsc");
            Ddo_tipoprojeto_cnstexp_Cleanfilter = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Cleanfilter");
            Ddo_tipoprojeto_cnstexp_Rangefilterfrom = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Rangefilterfrom");
            Ddo_tipoprojeto_cnstexp_Rangefilterto = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Rangefilterto");
            Ddo_tipoprojeto_cnstexp_Searchbuttontext = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tipoprojeto_nome_Activeeventkey = cgiGet( "DDO_TIPOPROJETO_NOME_Activeeventkey");
            Ddo_tipoprojeto_nome_Filteredtext_get = cgiGet( "DDO_TIPOPROJETO_NOME_Filteredtext_get");
            Ddo_tipoprojeto_nome_Selectedvalue_get = cgiGet( "DDO_TIPOPROJETO_NOME_Selectedvalue_get");
            Ddo_tipoprojeto_prazo_Activeeventkey = cgiGet( "DDO_TIPOPROJETO_PRAZO_Activeeventkey");
            Ddo_tipoprojeto_prazo_Filteredtext_get = cgiGet( "DDO_TIPOPROJETO_PRAZO_Filteredtext_get");
            Ddo_tipoprojeto_prazo_Filteredtextto_get = cgiGet( "DDO_TIPOPROJETO_PRAZO_Filteredtextto_get");
            Ddo_tipoprojeto_cnstexp_Activeeventkey = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Activeeventkey");
            Ddo_tipoprojeto_cnstexp_Filteredtext_get = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filteredtext_get");
            Ddo_tipoprojeto_cnstexp_Filteredtextto_get = cgiGet( "DDO_TIPOPROJETO_CNSTEXP_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME1"), AV17TipoProjeto_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME2"), AV21TipoProjeto_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPOPROJETO_NOME3"), AV25TipoProjeto_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOPROJETO_NOME"), AV31TFTipoProjeto_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPOPROJETO_NOME_SEL"), AV32TFTipoProjeto_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_PRAZO"), ",", ".") != AV35TFTipoProjeto_Prazo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_PRAZO_TO"), ",", ".") != AV36TFTipoProjeto_Prazo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_CNSTEXP"), ",", ".") != AV39TFTipoProjeto_CnstExp )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPOPROJETO_CNSTEXP_TO"), ",", ".") != AV40TFTipoProjeto_CnstExp_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25GQ2 */
         E25GQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25GQ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTftipoprojeto_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_nome_Visible), 5, 0)));
         edtavTftipoprojeto_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_nome_sel_Visible), 5, 0)));
         edtavTftipoprojeto_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_prazo_Visible), 5, 0)));
         edtavTftipoprojeto_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_prazo_to_Visible), 5, 0)));
         edtavTftipoprojeto_cnstexp_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_cnstexp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_cnstexp_Visible), 5, 0)));
         edtavTftipoprojeto_cnstexp_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipoprojeto_cnstexp_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipoprojeto_cnstexp_to_Visible), 5, 0)));
         Ddo_tipoprojeto_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoProjeto_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "TitleControlIdToReplace", Ddo_tipoprojeto_nome_Titlecontrolidtoreplace);
         AV33ddo_TipoProjeto_NomeTitleControlIdToReplace = Ddo_tipoprojeto_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_TipoProjeto_NomeTitleControlIdToReplace", AV33ddo_TipoProjeto_NomeTitleControlIdToReplace);
         edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoProjeto_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "TitleControlIdToReplace", Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace);
         AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace = Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace", AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace);
         edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoProjeto_CnstExp";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "TitleControlIdToReplace", Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace);
         AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace = Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace", AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace);
         edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Tipos de Projeto";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "�timo", 0);
         cmbavOrderedby.addItem("3", "exponencial", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV42DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV42DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26GQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30TipoProjeto_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34TipoProjeto_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38TipoProjeto_CnstExpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTipoProjeto_Nome_Titleformat = 2;
         edtTipoProjeto_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Nome_Internalname, "Title", edtTipoProjeto_Nome_Title);
         edtTipoProjeto_Prazo_Titleformat = 2;
         edtTipoProjeto_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�timo", AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Prazo_Internalname, "Title", edtTipoProjeto_Prazo_Title);
         edtTipoProjeto_CnstExp_Titleformat = 2;
         edtTipoProjeto_CnstExp_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "exponencial", AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_CnstExp_Internalname, "Title", edtTipoProjeto_CnstExp_Title);
         AV44GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridCurrentPage), 10, 0)));
         AV45GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30TipoProjeto_NomeTitleFilterData", AV30TipoProjeto_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34TipoProjeto_PrazoTitleFilterData", AV34TipoProjeto_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38TipoProjeto_CnstExpTitleFilterData", AV38TipoProjeto_CnstExpTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11GQ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV43PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV43PageToGo) ;
         }
      }

      protected void E12GQ2( )
      {
         /* Ddo_tipoprojeto_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipoprojeto_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "SortedStatus", Ddo_tipoprojeto_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "SortedStatus", Ddo_tipoprojeto_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFTipoProjeto_Nome = Ddo_tipoprojeto_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
            AV32TFTipoProjeto_Nome_Sel = Ddo_tipoprojeto_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFTipoProjeto_Nome_Sel", AV32TFTipoProjeto_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GQ2( )
      {
         /* Ddo_tipoprojeto_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipoprojeto_prazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_prazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "SortedStatus", Ddo_tipoprojeto_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_prazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_prazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "SortedStatus", Ddo_tipoprojeto_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFTipoProjeto_Prazo = NumberUtil.Val( Ddo_tipoprojeto_prazo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2)));
            AV36TFTipoProjeto_Prazo_To = NumberUtil.Val( Ddo_tipoprojeto_prazo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14GQ2( )
      {
         /* Ddo_tipoprojeto_cnstexp_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipoprojeto_cnstexp_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_cnstexp_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "SortedStatus", Ddo_tipoprojeto_cnstexp_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_cnstexp_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipoprojeto_cnstexp_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "SortedStatus", Ddo_tipoprojeto_cnstexp_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipoprojeto_cnstexp_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFTipoProjeto_CnstExp = NumberUtil.Val( Ddo_tipoprojeto_cnstexp_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFTipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2)));
            AV40TFTipoProjeto_CnstExp_To = NumberUtil.Val( Ddo_tipoprojeto_cnstexp_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoProjeto_CnstExp_To", StringUtil.LTrim( StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27GQ2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV48Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E28GQ2 */
         E28GQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28GQ2( )
      {
         /* Enter Routine */
         AV7InOutTipoProjeto_Codigo = A979TipoProjeto_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTipoProjeto_Codigo), 6, 0)));
         AV8InOutTipoProjeto_Nome = A980TipoProjeto_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTipoProjeto_Nome", AV8InOutTipoProjeto_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutTipoProjeto_Codigo,(String)AV8InOutTipoProjeto_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15GQ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20GQ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16GQ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21GQ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22GQ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17GQ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23GQ2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18GQ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17TipoProjeto_Nome1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21TipoProjeto_Nome2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25TipoProjeto_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFTipoProjeto_Nome, AV32TFTipoProjeto_Nome_Sel, AV35TFTipoProjeto_Prazo, AV36TFTipoProjeto_Prazo_To, AV39TFTipoProjeto_CnstExp, AV40TFTipoProjeto_CnstExp_To, AV33ddo_TipoProjeto_NomeTitleControlIdToReplace, AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace, AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace, AV49Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24GQ2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19GQ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tipoprojeto_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "SortedStatus", Ddo_tipoprojeto_nome_Sortedstatus);
         Ddo_tipoprojeto_prazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "SortedStatus", Ddo_tipoprojeto_prazo_Sortedstatus);
         Ddo_tipoprojeto_cnstexp_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "SortedStatus", Ddo_tipoprojeto_cnstexp_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tipoprojeto_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "SortedStatus", Ddo_tipoprojeto_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tipoprojeto_prazo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "SortedStatus", Ddo_tipoprojeto_prazo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_tipoprojeto_cnstexp_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "SortedStatus", Ddo_tipoprojeto_cnstexp_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTipoprojeto_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 )
         {
            edtavTipoprojeto_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavTipoprojeto_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 )
         {
            edtavTipoprojeto_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavTipoprojeto_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 )
         {
            edtavTipoprojeto_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipoprojeto_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipoprojeto_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21TipoProjeto_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25TipoProjeto_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFTipoProjeto_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFTipoProjeto_Nome", AV31TFTipoProjeto_Nome);
         Ddo_tipoprojeto_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "FilteredText_set", Ddo_tipoprojeto_nome_Filteredtext_set);
         AV32TFTipoProjeto_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFTipoProjeto_Nome_Sel", AV32TFTipoProjeto_Nome_Sel);
         Ddo_tipoprojeto_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_nome_Internalname, "SelectedValue_set", Ddo_tipoprojeto_nome_Selectedvalue_set);
         AV35TFTipoProjeto_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2)));
         Ddo_tipoprojeto_prazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "FilteredText_set", Ddo_tipoprojeto_prazo_Filteredtext_set);
         AV36TFTipoProjeto_Prazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFTipoProjeto_Prazo_To", StringUtil.LTrim( StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2)));
         Ddo_tipoprojeto_prazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_prazo_Internalname, "FilteredTextTo_set", Ddo_tipoprojeto_prazo_Filteredtextto_set);
         AV39TFTipoProjeto_CnstExp = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFTipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2)));
         Ddo_tipoprojeto_cnstexp_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "FilteredText_set", Ddo_tipoprojeto_cnstexp_Filteredtext_set);
         AV40TFTipoProjeto_CnstExp_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFTipoProjeto_CnstExp_To", StringUtil.LTrim( StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2)));
         Ddo_tipoprojeto_cnstexp_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipoprojeto_cnstexp_Internalname, "FilteredTextTo_set", Ddo_tipoprojeto_cnstexp_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "TIPOPROJETO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17TipoProjeto_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17TipoProjeto_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoProjeto_Nome1", AV17TipoProjeto_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21TipoProjeto_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoProjeto_Nome2", AV21TipoProjeto_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25TipoProjeto_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoProjeto_Nome3", AV25TipoProjeto_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFTipoProjeto_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOPROJETO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV31TFTipoProjeto_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOPROJETO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV32TFTipoProjeto_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV35TFTipoProjeto_Prazo) && (Convert.ToDecimal(0)==AV36TFTipoProjeto_Prazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOPROJETO_PRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV35TFTipoProjeto_Prazo, 5, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV36TFTipoProjeto_Prazo_To, 5, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV39TFTipoProjeto_CnstExp) && (Convert.ToDecimal(0)==AV40TFTipoProjeto_CnstExp_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPOPROJETO_CNSTEXP";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV39TFTipoProjeto_CnstExp, 5, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV40TFTipoProjeto_CnstExp_To, 5, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV49Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoProjeto_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17TipoProjeto_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoProjeto_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21TipoProjeto_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoProjeto_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25TipoProjeto_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_GQ2( true) ;
         }
         else
         {
            wb_table2_5_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_GQ2( true) ;
         }
         else
         {
            wb_table3_74_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GQ2e( true) ;
         }
         else
         {
            wb_table1_2_GQ2e( false) ;
         }
      }

      protected void wb_table3_74_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_GQ2( true) ;
         }
         else
         {
            wb_table4_77_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_GQ2e( true) ;
         }
         else
         {
            wb_table3_74_GQ2e( false) ;
         }
      }

      protected void wb_table4_77_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoProjeto_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoProjeto_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoProjeto_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(40), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoProjeto_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoProjeto_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoProjeto_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(76), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoProjeto_CnstExp_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoProjeto_CnstExp_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoProjeto_CnstExp_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A979TipoProjeto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A980TipoProjeto_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoProjeto_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoProjeto_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A981TipoProjeto_Prazo, 5, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoProjeto_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoProjeto_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A982TipoProjeto_CnstExp, 5, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoProjeto_CnstExp_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoProjeto_CnstExp_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_GQ2e( true) ;
         }
         else
         {
            wb_table4_77_GQ2e( false) ;
         }
      }

      protected void wb_table2_5_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_GQ2( true) ;
         }
         else
         {
            wb_table5_14_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GQ2e( true) ;
         }
         else
         {
            wb_table2_5_GQ2e( false) ;
         }
      }

      protected void wb_table5_14_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_GQ2( true) ;
         }
         else
         {
            wb_table6_19_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_GQ2e( true) ;
         }
         else
         {
            wb_table5_14_GQ2e( false) ;
         }
      }

      protected void wb_table6_19_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_GQ2( true) ;
         }
         else
         {
            wb_table7_28_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_GQ2( true) ;
         }
         else
         {
            wb_table8_45_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_GQ2( true) ;
         }
         else
         {
            wb_table9_62_GQ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_GQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_GQ2e( true) ;
         }
         else
         {
            wb_table6_19_GQ2e( false) ;
         }
      }

      protected void wb_table9_62_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipoprojeto_nome3_Internalname, StringUtil.RTrim( AV25TipoProjeto_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25TipoProjeto_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipoprojeto_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipoprojeto_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_GQ2e( true) ;
         }
         else
         {
            wb_table9_62_GQ2e( false) ;
         }
      }

      protected void wb_table8_45_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipoprojeto_nome2_Internalname, StringUtil.RTrim( AV21TipoProjeto_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21TipoProjeto_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipoprojeto_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipoprojeto_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_GQ2e( true) ;
         }
         else
         {
            wb_table8_45_GQ2e( false) ;
         }
      }

      protected void wb_table7_28_GQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptTipoProjeto.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipoprojeto_nome1_Internalname, StringUtil.RTrim( AV17TipoProjeto_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17TipoProjeto_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipoprojeto_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipoprojeto_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptTipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_GQ2e( true) ;
         }
         else
         {
            wb_table7_28_GQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutTipoProjeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutTipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutTipoProjeto_Codigo), 6, 0)));
         AV8InOutTipoProjeto_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutTipoProjeto_Nome", AV8InOutTipoProjeto_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGQ2( ) ;
         WSGQ2( ) ;
         WEGQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118545115");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("prompttipoprojeto.js", "?20203118545115");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtTipoProjeto_Codigo_Internalname = "TIPOPROJETO_CODIGO_"+sGXsfl_80_idx;
         edtTipoProjeto_Nome_Internalname = "TIPOPROJETO_NOME_"+sGXsfl_80_idx;
         edtTipoProjeto_Prazo_Internalname = "TIPOPROJETO_PRAZO_"+sGXsfl_80_idx;
         edtTipoProjeto_CnstExp_Internalname = "TIPOPROJETO_CNSTEXP_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtTipoProjeto_Codigo_Internalname = "TIPOPROJETO_CODIGO_"+sGXsfl_80_fel_idx;
         edtTipoProjeto_Nome_Internalname = "TIPOPROJETO_NOME_"+sGXsfl_80_fel_idx;
         edtTipoProjeto_Prazo_Internalname = "TIPOPROJETO_PRAZO_"+sGXsfl_80_fel_idx;
         edtTipoProjeto_CnstExp_Internalname = "TIPOPROJETO_CNSTEXP_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBGQ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV48Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV48Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoProjeto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A979TipoProjeto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoProjeto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoProjeto_Nome_Internalname,StringUtil.RTrim( A980TipoProjeto_Nome),StringUtil.RTrim( context.localUtil.Format( A980TipoProjeto_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoProjeto_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoProjeto_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( A981TipoProjeto_Prazo, 5, 2, ",", "")),context.localUtil.Format( A981TipoProjeto_Prazo, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoProjeto_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)40,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoProjeto_CnstExp_Internalname,StringUtil.LTrim( StringUtil.NToC( A982TipoProjeto_CnstExp, 5, 2, ",", "")),context.localUtil.Format( A982TipoProjeto_CnstExp, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoProjeto_CnstExp_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)76,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_NOME"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A980TipoProjeto_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_PRAZO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A981TipoProjeto_Prazo, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPOPROJETO_CNSTEXP"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A982TipoProjeto_CnstExp, "Z9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavTipoprojeto_nome1_Internalname = "vTIPOPROJETO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavTipoprojeto_nome2_Internalname = "vTIPOPROJETO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavTipoprojeto_nome3_Internalname = "vTIPOPROJETO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtTipoProjeto_Codigo_Internalname = "TIPOPROJETO_CODIGO";
         edtTipoProjeto_Nome_Internalname = "TIPOPROJETO_NOME";
         edtTipoProjeto_Prazo_Internalname = "TIPOPROJETO_PRAZO";
         edtTipoProjeto_CnstExp_Internalname = "TIPOPROJETO_CNSTEXP";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTftipoprojeto_nome_Internalname = "vTFTIPOPROJETO_NOME";
         edtavTftipoprojeto_nome_sel_Internalname = "vTFTIPOPROJETO_NOME_SEL";
         edtavTftipoprojeto_prazo_Internalname = "vTFTIPOPROJETO_PRAZO";
         edtavTftipoprojeto_prazo_to_Internalname = "vTFTIPOPROJETO_PRAZO_TO";
         edtavTftipoprojeto_cnstexp_Internalname = "vTFTIPOPROJETO_CNSTEXP";
         edtavTftipoprojeto_cnstexp_to_Internalname = "vTFTIPOPROJETO_CNSTEXP_TO";
         Ddo_tipoprojeto_nome_Internalname = "DDO_TIPOPROJETO_NOME";
         edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Internalname = "vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tipoprojeto_prazo_Internalname = "DDO_TIPOPROJETO_PRAZO";
         edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Internalname = "vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_tipoprojeto_cnstexp_Internalname = "DDO_TIPOPROJETO_CNSTEXP";
         edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Internalname = "vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTipoProjeto_CnstExp_Jsonclick = "";
         edtTipoProjeto_Prazo_Jsonclick = "";
         edtTipoProjeto_Nome_Jsonclick = "";
         edtTipoProjeto_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavTipoprojeto_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavTipoprojeto_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavTipoprojeto_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtTipoProjeto_CnstExp_Titleformat = 0;
         edtTipoProjeto_Prazo_Titleformat = 0;
         edtTipoProjeto_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavTipoprojeto_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavTipoprojeto_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavTipoprojeto_nome1_Visible = 1;
         edtTipoProjeto_CnstExp_Title = "exponencial";
         edtTipoProjeto_Prazo_Title = "�timo";
         edtTipoProjeto_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftipoprojeto_cnstexp_to_Jsonclick = "";
         edtavTftipoprojeto_cnstexp_to_Visible = 1;
         edtavTftipoprojeto_cnstexp_Jsonclick = "";
         edtavTftipoprojeto_cnstexp_Visible = 1;
         edtavTftipoprojeto_prazo_to_Jsonclick = "";
         edtavTftipoprojeto_prazo_to_Visible = 1;
         edtavTftipoprojeto_prazo_Jsonclick = "";
         edtavTftipoprojeto_prazo_Visible = 1;
         edtavTftipoprojeto_nome_sel_Jsonclick = "";
         edtavTftipoprojeto_nome_sel_Visible = 1;
         edtavTftipoprojeto_nome_Jsonclick = "";
         edtavTftipoprojeto_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_tipoprojeto_cnstexp_Searchbuttontext = "Pesquisar";
         Ddo_tipoprojeto_cnstexp_Rangefilterto = "At�";
         Ddo_tipoprojeto_cnstexp_Rangefilterfrom = "Desde";
         Ddo_tipoprojeto_cnstexp_Cleanfilter = "Limpar pesquisa";
         Ddo_tipoprojeto_cnstexp_Sortdsc = "Ordenar de Z � A";
         Ddo_tipoprojeto_cnstexp_Sortasc = "Ordenar de A � Z";
         Ddo_tipoprojeto_cnstexp_Includedatalist = Convert.ToBoolean( 0);
         Ddo_tipoprojeto_cnstexp_Filterisrange = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_cnstexp_Filtertype = "Numeric";
         Ddo_tipoprojeto_cnstexp_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_cnstexp_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_cnstexp_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace = "";
         Ddo_tipoprojeto_cnstexp_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipoprojeto_cnstexp_Cls = "ColumnSettings";
         Ddo_tipoprojeto_cnstexp_Tooltip = "Op��es";
         Ddo_tipoprojeto_cnstexp_Caption = "";
         Ddo_tipoprojeto_prazo_Searchbuttontext = "Pesquisar";
         Ddo_tipoprojeto_prazo_Rangefilterto = "At�";
         Ddo_tipoprojeto_prazo_Rangefilterfrom = "Desde";
         Ddo_tipoprojeto_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_tipoprojeto_prazo_Sortdsc = "Ordenar de Z � A";
         Ddo_tipoprojeto_prazo_Sortasc = "Ordenar de A � Z";
         Ddo_tipoprojeto_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_tipoprojeto_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_prazo_Filtertype = "Numeric";
         Ddo_tipoprojeto_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_prazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_prazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace = "";
         Ddo_tipoprojeto_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipoprojeto_prazo_Cls = "ColumnSettings";
         Ddo_tipoprojeto_prazo_Tooltip = "Op��es";
         Ddo_tipoprojeto_prazo_Caption = "";
         Ddo_tipoprojeto_nome_Searchbuttontext = "Pesquisar";
         Ddo_tipoprojeto_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipoprojeto_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tipoprojeto_nome_Loadingdata = "Carregando dados...";
         Ddo_tipoprojeto_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tipoprojeto_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tipoprojeto_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tipoprojeto_nome_Datalistproc = "GetPromptTipoProjetoFilterData";
         Ddo_tipoprojeto_nome_Datalisttype = "Dynamic";
         Ddo_tipoprojeto_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipoprojeto_nome_Filtertype = "Character";
         Ddo_tipoprojeto_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipoprojeto_nome_Titlecontrolidtoreplace = "";
         Ddo_tipoprojeto_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipoprojeto_nome_Cls = "ColumnSettings";
         Ddo_tipoprojeto_nome_Tooltip = "Op��es";
         Ddo_tipoprojeto_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Tipos de Projeto";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV30TipoProjeto_NomeTitleFilterData',fld:'vTIPOPROJETO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV34TipoProjeto_PrazoTitleFilterData',fld:'vTIPOPROJETO_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38TipoProjeto_CnstExpTitleFilterData',fld:'vTIPOPROJETO_CNSTEXPTITLEFILTERDATA',pic:'',nv:null},{av:'edtTipoProjeto_Nome_Titleformat',ctrl:'TIPOPROJETO_NOME',prop:'Titleformat'},{av:'edtTipoProjeto_Nome_Title',ctrl:'TIPOPROJETO_NOME',prop:'Title'},{av:'edtTipoProjeto_Prazo_Titleformat',ctrl:'TIPOPROJETO_PRAZO',prop:'Titleformat'},{av:'edtTipoProjeto_Prazo_Title',ctrl:'TIPOPROJETO_PRAZO',prop:'Title'},{av:'edtTipoProjeto_CnstExp_Titleformat',ctrl:'TIPOPROJETO_CNSTEXP',prop:'Titleformat'},{av:'edtTipoProjeto_CnstExp_Title',ctrl:'TIPOPROJETO_CNSTEXP',prop:'Title'},{av:'AV44GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TIPOPROJETO_NOME.ONOPTIONCLICKED","{handler:'E12GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tipoprojeto_nome_Activeeventkey',ctrl:'DDO_TIPOPROJETO_NOME',prop:'ActiveEventKey'},{av:'Ddo_tipoprojeto_nome_Filteredtext_get',ctrl:'DDO_TIPOPROJETO_NOME',prop:'FilteredText_get'},{av:'Ddo_tipoprojeto_nome_Selectedvalue_get',ctrl:'DDO_TIPOPROJETO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipoprojeto_nome_Sortedstatus',ctrl:'DDO_TIPOPROJETO_NOME',prop:'SortedStatus'},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipoprojeto_prazo_Sortedstatus',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'SortedStatus'},{av:'Ddo_tipoprojeto_cnstexp_Sortedstatus',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPOPROJETO_PRAZO.ONOPTIONCLICKED","{handler:'E13GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tipoprojeto_prazo_Activeeventkey',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_tipoprojeto_prazo_Filteredtext_get',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'FilteredText_get'},{av:'Ddo_tipoprojeto_prazo_Filteredtextto_get',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipoprojeto_prazo_Sortedstatus',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'SortedStatus'},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_nome_Sortedstatus',ctrl:'DDO_TIPOPROJETO_NOME',prop:'SortedStatus'},{av:'Ddo_tipoprojeto_cnstexp_Sortedstatus',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPOPROJETO_CNSTEXP.ONOPTIONCLICKED","{handler:'E14GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tipoprojeto_cnstexp_Activeeventkey',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'ActiveEventKey'},{av:'Ddo_tipoprojeto_cnstexp_Filteredtext_get',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'FilteredText_get'},{av:'Ddo_tipoprojeto_cnstexp_Filteredtextto_get',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipoprojeto_cnstexp_Sortedstatus',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'SortedStatus'},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_nome_Sortedstatus',ctrl:'DDO_TIPOPROJETO_NOME',prop:'SortedStatus'},{av:'Ddo_tipoprojeto_prazo_Sortedstatus',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27GQ2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E28GQ2',iparms:[{av:'A979TipoProjeto_Codigo',fld:'TIPOPROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A980TipoProjeto_Nome',fld:'TIPOPROJETO_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutTipoProjeto_Codigo',fld:'vINOUTTIPOPROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutTipoProjeto_Nome',fld:'vINOUTTIPOPROJETO_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20GQ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipoprojeto_nome2_Visible',ctrl:'vTIPOPROJETO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTipoprojeto_nome3_Visible',ctrl:'vTIPOPROJETO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTipoprojeto_nome1_Visible',ctrl:'vTIPOPROJETO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21GQ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavTipoprojeto_nome1_Visible',ctrl:'vTIPOPROJETO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22GQ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipoprojeto_nome2_Visible',ctrl:'vTIPOPROJETO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTipoprojeto_nome3_Visible',ctrl:'vTIPOPROJETO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTipoprojeto_nome1_Visible',ctrl:'vTIPOPROJETO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23GQ2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavTipoprojeto_nome2_Visible',ctrl:'vTIPOPROJETO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipoprojeto_nome2_Visible',ctrl:'vTIPOPROJETO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTipoprojeto_nome3_Visible',ctrl:'vTIPOPROJETO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavTipoprojeto_nome1_Visible',ctrl:'vTIPOPROJETO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24GQ2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavTipoprojeto_nome3_Visible',ctrl:'vTIPOPROJETO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19GQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'AV33ddo_TipoProjeto_NomeTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace',fld:'vDDO_TIPOPROJETO_CNSTEXPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFTipoProjeto_Nome',fld:'vTFTIPOPROJETO_NOME',pic:'@!',nv:''},{av:'Ddo_tipoprojeto_nome_Filteredtext_set',ctrl:'DDO_TIPOPROJETO_NOME',prop:'FilteredText_set'},{av:'AV32TFTipoProjeto_Nome_Sel',fld:'vTFTIPOPROJETO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipoprojeto_nome_Selectedvalue_set',ctrl:'DDO_TIPOPROJETO_NOME',prop:'SelectedValue_set'},{av:'AV35TFTipoProjeto_Prazo',fld:'vTFTIPOPROJETO_PRAZO',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_prazo_Filteredtext_set',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'FilteredText_set'},{av:'AV36TFTipoProjeto_Prazo_To',fld:'vTFTIPOPROJETO_PRAZO_TO',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_prazo_Filteredtextto_set',ctrl:'DDO_TIPOPROJETO_PRAZO',prop:'FilteredTextTo_set'},{av:'AV39TFTipoProjeto_CnstExp',fld:'vTFTIPOPROJETO_CNSTEXP',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_cnstexp_Filteredtext_set',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'FilteredText_set'},{av:'AV40TFTipoProjeto_CnstExp_To',fld:'vTFTIPOPROJETO_CNSTEXP_TO',pic:'Z9.99',nv:0.0},{av:'Ddo_tipoprojeto_cnstexp_Filteredtextto_set',ctrl:'DDO_TIPOPROJETO_CNSTEXP',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17TipoProjeto_Nome1',fld:'vTIPOPROJETO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTipoprojeto_nome1_Visible',ctrl:'vTIPOPROJETO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21TipoProjeto_Nome2',fld:'vTIPOPROJETO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25TipoProjeto_Nome3',fld:'vTIPOPROJETO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipoprojeto_nome2_Visible',ctrl:'vTIPOPROJETO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavTipoprojeto_nome3_Visible',ctrl:'vTIPOPROJETO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutTipoProjeto_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_tipoprojeto_nome_Activeeventkey = "";
         Ddo_tipoprojeto_nome_Filteredtext_get = "";
         Ddo_tipoprojeto_nome_Selectedvalue_get = "";
         Ddo_tipoprojeto_prazo_Activeeventkey = "";
         Ddo_tipoprojeto_prazo_Filteredtext_get = "";
         Ddo_tipoprojeto_prazo_Filteredtextto_get = "";
         Ddo_tipoprojeto_cnstexp_Activeeventkey = "";
         Ddo_tipoprojeto_cnstexp_Filteredtext_get = "";
         Ddo_tipoprojeto_cnstexp_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17TipoProjeto_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21TipoProjeto_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25TipoProjeto_Nome3 = "";
         AV31TFTipoProjeto_Nome = "";
         AV32TFTipoProjeto_Nome_Sel = "";
         AV33ddo_TipoProjeto_NomeTitleControlIdToReplace = "";
         AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace = "";
         AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace = "";
         AV49Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV42DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30TipoProjeto_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34TipoProjeto_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38TipoProjeto_CnstExpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tipoprojeto_nome_Filteredtext_set = "";
         Ddo_tipoprojeto_nome_Selectedvalue_set = "";
         Ddo_tipoprojeto_nome_Sortedstatus = "";
         Ddo_tipoprojeto_prazo_Filteredtext_set = "";
         Ddo_tipoprojeto_prazo_Filteredtextto_set = "";
         Ddo_tipoprojeto_prazo_Sortedstatus = "";
         Ddo_tipoprojeto_cnstexp_Filteredtext_set = "";
         Ddo_tipoprojeto_cnstexp_Filteredtextto_set = "";
         Ddo_tipoprojeto_cnstexp_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV48Select_GXI = "";
         A980TipoProjeto_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17TipoProjeto_Nome1 = "";
         lV21TipoProjeto_Nome2 = "";
         lV25TipoProjeto_Nome3 = "";
         lV31TFTipoProjeto_Nome = "";
         H00GQ2_A982TipoProjeto_CnstExp = new decimal[1] ;
         H00GQ2_A981TipoProjeto_Prazo = new decimal[1] ;
         H00GQ2_A980TipoProjeto_Nome = new String[] {""} ;
         H00GQ2_A979TipoProjeto_Codigo = new int[1] ;
         H00GQ3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prompttipoprojeto__default(),
            new Object[][] {
                new Object[] {
               H00GQ2_A982TipoProjeto_CnstExp, H00GQ2_A981TipoProjeto_Prazo, H00GQ2_A980TipoProjeto_Nome, H00GQ2_A979TipoProjeto_Codigo
               }
               , new Object[] {
               H00GQ3_AGRID_nRecordCount
               }
            }
         );
         AV49Pgmname = "PromptTipoProjeto";
         /* GeneXus formulas. */
         AV49Pgmname = "PromptTipoProjeto";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTipoProjeto_Nome_Titleformat ;
      private short edtTipoProjeto_Prazo_Titleformat ;
      private short edtTipoProjeto_CnstExp_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutTipoProjeto_Codigo ;
      private int wcpOAV7InOutTipoProjeto_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tipoprojeto_nome_Datalistupdateminimumcharacters ;
      private int edtavTftipoprojeto_nome_Visible ;
      private int edtavTftipoprojeto_nome_sel_Visible ;
      private int edtavTftipoprojeto_prazo_Visible ;
      private int edtavTftipoprojeto_prazo_to_Visible ;
      private int edtavTftipoprojeto_cnstexp_Visible ;
      private int edtavTftipoprojeto_cnstexp_to_Visible ;
      private int edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Visible ;
      private int A979TipoProjeto_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV43PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavTipoprojeto_nome1_Visible ;
      private int edtavTipoprojeto_nome2_Visible ;
      private int edtavTipoprojeto_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV44GridCurrentPage ;
      private long AV45GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV35TFTipoProjeto_Prazo ;
      private decimal AV36TFTipoProjeto_Prazo_To ;
      private decimal AV39TFTipoProjeto_CnstExp ;
      private decimal AV40TFTipoProjeto_CnstExp_To ;
      private decimal A981TipoProjeto_Prazo ;
      private decimal A982TipoProjeto_CnstExp ;
      private String AV8InOutTipoProjeto_Nome ;
      private String wcpOAV8InOutTipoProjeto_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tipoprojeto_nome_Activeeventkey ;
      private String Ddo_tipoprojeto_nome_Filteredtext_get ;
      private String Ddo_tipoprojeto_nome_Selectedvalue_get ;
      private String Ddo_tipoprojeto_prazo_Activeeventkey ;
      private String Ddo_tipoprojeto_prazo_Filteredtext_get ;
      private String Ddo_tipoprojeto_prazo_Filteredtextto_get ;
      private String Ddo_tipoprojeto_cnstexp_Activeeventkey ;
      private String Ddo_tipoprojeto_cnstexp_Filteredtext_get ;
      private String Ddo_tipoprojeto_cnstexp_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV17TipoProjeto_Nome1 ;
      private String AV21TipoProjeto_Nome2 ;
      private String AV25TipoProjeto_Nome3 ;
      private String AV31TFTipoProjeto_Nome ;
      private String AV32TFTipoProjeto_Nome_Sel ;
      private String AV49Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tipoprojeto_nome_Caption ;
      private String Ddo_tipoprojeto_nome_Tooltip ;
      private String Ddo_tipoprojeto_nome_Cls ;
      private String Ddo_tipoprojeto_nome_Filteredtext_set ;
      private String Ddo_tipoprojeto_nome_Selectedvalue_set ;
      private String Ddo_tipoprojeto_nome_Dropdownoptionstype ;
      private String Ddo_tipoprojeto_nome_Titlecontrolidtoreplace ;
      private String Ddo_tipoprojeto_nome_Sortedstatus ;
      private String Ddo_tipoprojeto_nome_Filtertype ;
      private String Ddo_tipoprojeto_nome_Datalisttype ;
      private String Ddo_tipoprojeto_nome_Datalistproc ;
      private String Ddo_tipoprojeto_nome_Sortasc ;
      private String Ddo_tipoprojeto_nome_Sortdsc ;
      private String Ddo_tipoprojeto_nome_Loadingdata ;
      private String Ddo_tipoprojeto_nome_Cleanfilter ;
      private String Ddo_tipoprojeto_nome_Noresultsfound ;
      private String Ddo_tipoprojeto_nome_Searchbuttontext ;
      private String Ddo_tipoprojeto_prazo_Caption ;
      private String Ddo_tipoprojeto_prazo_Tooltip ;
      private String Ddo_tipoprojeto_prazo_Cls ;
      private String Ddo_tipoprojeto_prazo_Filteredtext_set ;
      private String Ddo_tipoprojeto_prazo_Filteredtextto_set ;
      private String Ddo_tipoprojeto_prazo_Dropdownoptionstype ;
      private String Ddo_tipoprojeto_prazo_Titlecontrolidtoreplace ;
      private String Ddo_tipoprojeto_prazo_Sortedstatus ;
      private String Ddo_tipoprojeto_prazo_Filtertype ;
      private String Ddo_tipoprojeto_prazo_Sortasc ;
      private String Ddo_tipoprojeto_prazo_Sortdsc ;
      private String Ddo_tipoprojeto_prazo_Cleanfilter ;
      private String Ddo_tipoprojeto_prazo_Rangefilterfrom ;
      private String Ddo_tipoprojeto_prazo_Rangefilterto ;
      private String Ddo_tipoprojeto_prazo_Searchbuttontext ;
      private String Ddo_tipoprojeto_cnstexp_Caption ;
      private String Ddo_tipoprojeto_cnstexp_Tooltip ;
      private String Ddo_tipoprojeto_cnstexp_Cls ;
      private String Ddo_tipoprojeto_cnstexp_Filteredtext_set ;
      private String Ddo_tipoprojeto_cnstexp_Filteredtextto_set ;
      private String Ddo_tipoprojeto_cnstexp_Dropdownoptionstype ;
      private String Ddo_tipoprojeto_cnstexp_Titlecontrolidtoreplace ;
      private String Ddo_tipoprojeto_cnstexp_Sortedstatus ;
      private String Ddo_tipoprojeto_cnstexp_Filtertype ;
      private String Ddo_tipoprojeto_cnstexp_Sortasc ;
      private String Ddo_tipoprojeto_cnstexp_Sortdsc ;
      private String Ddo_tipoprojeto_cnstexp_Cleanfilter ;
      private String Ddo_tipoprojeto_cnstexp_Rangefilterfrom ;
      private String Ddo_tipoprojeto_cnstexp_Rangefilterto ;
      private String Ddo_tipoprojeto_cnstexp_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTftipoprojeto_nome_Internalname ;
      private String edtavTftipoprojeto_nome_Jsonclick ;
      private String edtavTftipoprojeto_nome_sel_Internalname ;
      private String edtavTftipoprojeto_nome_sel_Jsonclick ;
      private String edtavTftipoprojeto_prazo_Internalname ;
      private String edtavTftipoprojeto_prazo_Jsonclick ;
      private String edtavTftipoprojeto_prazo_to_Internalname ;
      private String edtavTftipoprojeto_prazo_to_Jsonclick ;
      private String edtavTftipoprojeto_cnstexp_Internalname ;
      private String edtavTftipoprojeto_cnstexp_Jsonclick ;
      private String edtavTftipoprojeto_cnstexp_to_Internalname ;
      private String edtavTftipoprojeto_cnstexp_to_Jsonclick ;
      private String edtavDdo_tipoprojeto_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipoprojeto_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipoprojeto_cnstexptitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtTipoProjeto_Codigo_Internalname ;
      private String A980TipoProjeto_Nome ;
      private String edtTipoProjeto_Nome_Internalname ;
      private String edtTipoProjeto_Prazo_Internalname ;
      private String edtTipoProjeto_CnstExp_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17TipoProjeto_Nome1 ;
      private String lV21TipoProjeto_Nome2 ;
      private String lV25TipoProjeto_Nome3 ;
      private String lV31TFTipoProjeto_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavTipoprojeto_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavTipoprojeto_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavTipoprojeto_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tipoprojeto_nome_Internalname ;
      private String Ddo_tipoprojeto_prazo_Internalname ;
      private String Ddo_tipoprojeto_cnstexp_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtTipoProjeto_Nome_Title ;
      private String edtTipoProjeto_Prazo_Title ;
      private String edtTipoProjeto_CnstExp_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavTipoprojeto_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavTipoprojeto_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavTipoprojeto_nome1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtTipoProjeto_Codigo_Jsonclick ;
      private String edtTipoProjeto_Nome_Jsonclick ;
      private String edtTipoProjeto_Prazo_Jsonclick ;
      private String edtTipoProjeto_CnstExp_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tipoprojeto_nome_Includesortasc ;
      private bool Ddo_tipoprojeto_nome_Includesortdsc ;
      private bool Ddo_tipoprojeto_nome_Includefilter ;
      private bool Ddo_tipoprojeto_nome_Filterisrange ;
      private bool Ddo_tipoprojeto_nome_Includedatalist ;
      private bool Ddo_tipoprojeto_prazo_Includesortasc ;
      private bool Ddo_tipoprojeto_prazo_Includesortdsc ;
      private bool Ddo_tipoprojeto_prazo_Includefilter ;
      private bool Ddo_tipoprojeto_prazo_Filterisrange ;
      private bool Ddo_tipoprojeto_prazo_Includedatalist ;
      private bool Ddo_tipoprojeto_cnstexp_Includesortasc ;
      private bool Ddo_tipoprojeto_cnstexp_Includesortdsc ;
      private bool Ddo_tipoprojeto_cnstexp_Includefilter ;
      private bool Ddo_tipoprojeto_cnstexp_Filterisrange ;
      private bool Ddo_tipoprojeto_cnstexp_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33ddo_TipoProjeto_NomeTitleControlIdToReplace ;
      private String AV37ddo_TipoProjeto_PrazoTitleControlIdToReplace ;
      private String AV41ddo_TipoProjeto_CnstExpTitleControlIdToReplace ;
      private String AV48Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutTipoProjeto_Codigo ;
      private String aP1_InOutTipoProjeto_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00GQ2_A982TipoProjeto_CnstExp ;
      private decimal[] H00GQ2_A981TipoProjeto_Prazo ;
      private String[] H00GQ2_A980TipoProjeto_Nome ;
      private int[] H00GQ2_A979TipoProjeto_Codigo ;
      private long[] H00GQ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30TipoProjeto_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34TipoProjeto_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38TipoProjeto_CnstExpTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV42DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class prompttipoprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GQ2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17TipoProjeto_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21TipoProjeto_Nome2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25TipoProjeto_Nome3 ,
                                             String AV32TFTipoProjeto_Nome_Sel ,
                                             String AV31TFTipoProjeto_Nome ,
                                             decimal AV35TFTipoProjeto_Prazo ,
                                             decimal AV36TFTipoProjeto_Prazo_To ,
                                             decimal AV39TFTipoProjeto_CnstExp ,
                                             decimal AV40TFTipoProjeto_CnstExp_To ,
                                             String A980TipoProjeto_Nome ,
                                             decimal A981TipoProjeto_Prazo ,
                                             decimal A982TipoProjeto_CnstExp ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [TipoProjeto_CnstExp], [TipoProjeto_Prazo], [TipoProjeto_Nome], [TipoProjeto_Codigo]";
         sFromString = " FROM [TipoProjeto] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV17TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV17TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV17TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV17TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV21TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV21TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV21TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV21TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV25TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV25TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV25TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV25TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFTipoProjeto_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV31TFTipoProjeto_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV31TFTipoProjeto_Nome)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] = @AV32TFTipoProjeto_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] = @AV32TFTipoProjeto_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFTipoProjeto_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] >= @AV35TFTipoProjeto_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] >= @AV35TFTipoProjeto_Prazo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFTipoProjeto_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] <= @AV36TFTipoProjeto_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] <= @AV36TFTipoProjeto_Prazo_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV39TFTipoProjeto_CnstExp) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] >= @AV39TFTipoProjeto_CnstExp)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] >= @AV39TFTipoProjeto_CnstExp)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFTipoProjeto_CnstExp_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] <= @AV40TFTipoProjeto_CnstExp_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] <= @AV40TFTipoProjeto_CnstExp_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_Prazo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_Prazo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_CnstExp]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_CnstExp] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [TipoProjeto_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GQ3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17TipoProjeto_Nome1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21TipoProjeto_Nome2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25TipoProjeto_Nome3 ,
                                             String AV32TFTipoProjeto_Nome_Sel ,
                                             String AV31TFTipoProjeto_Nome ,
                                             decimal AV35TFTipoProjeto_Prazo ,
                                             decimal AV36TFTipoProjeto_Prazo_To ,
                                             decimal AV39TFTipoProjeto_CnstExp ,
                                             decimal AV40TFTipoProjeto_CnstExp_To ,
                                             String A980TipoProjeto_Nome ,
                                             decimal A981TipoProjeto_Prazo ,
                                             decimal A982TipoProjeto_CnstExp ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [TipoProjeto] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV17TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV17TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV17TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV17TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV21TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV21TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV21TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV21TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV25TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV25TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV25TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV25TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFTipoProjeto_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV31TFTipoProjeto_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV31TFTipoProjeto_Nome)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFTipoProjeto_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] = @AV32TFTipoProjeto_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] = @AV32TFTipoProjeto_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFTipoProjeto_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] >= @AV35TFTipoProjeto_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] >= @AV35TFTipoProjeto_Prazo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFTipoProjeto_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] <= @AV36TFTipoProjeto_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] <= @AV36TFTipoProjeto_Prazo_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV39TFTipoProjeto_CnstExp) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] >= @AV39TFTipoProjeto_CnstExp)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] >= @AV39TFTipoProjeto_CnstExp)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFTipoProjeto_CnstExp_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] <= @AV40TFTipoProjeto_CnstExp_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] <= @AV40TFTipoProjeto_CnstExp_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GQ2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (String)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00GQ3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (String)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GQ2 ;
          prmH00GQ2 = new Object[] {
          new Object[] {"@lV17TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TFTipoProjeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV32TFTipoProjeto_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFTipoProjeto_Prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV36TFTipoProjeto_Prazo_To",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV39TFTipoProjeto_CnstExp",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV40TFTipoProjeto_CnstExp_To",SqlDbType.Decimal,5,2} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GQ3 ;
          prmH00GQ3 = new Object[] {
          new Object[] {"@lV17TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV21TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TFTipoProjeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV32TFTipoProjeto_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFTipoProjeto_Prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV36TFTipoProjeto_Prazo_To",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV39TFTipoProjeto_CnstExp",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV40TFTipoProjeto_CnstExp_To",SqlDbType.Decimal,5,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GQ2,11,0,true,false )
             ,new CursorDef("H00GQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GQ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                return;
       }
    }

 }

}
