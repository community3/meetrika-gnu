/*
               File: PRC_DLTMenuPerfil
        Description: Delete Perfis associados ao Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:32.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltmenuperfil : GXProcedure
   {
      public prc_dltmenuperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltmenuperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Menu_Codigo )
      {
         this.A277Menu_Codigo = aP0_Menu_Codigo;
         initialize();
         executePrivate();
         aP0_Menu_Codigo=this.A277Menu_Codigo;
      }

      public int executeUdp( )
      {
         this.A277Menu_Codigo = aP0_Menu_Codigo;
         initialize();
         executePrivate();
         aP0_Menu_Codigo=this.A277Menu_Codigo;
         return A277Menu_Codigo ;
      }

      public void executeSubmit( ref int aP0_Menu_Codigo )
      {
         prc_dltmenuperfil objprc_dltmenuperfil;
         objprc_dltmenuperfil = new prc_dltmenuperfil();
         objprc_dltmenuperfil.A277Menu_Codigo = aP0_Menu_Codigo;
         objprc_dltmenuperfil.context.SetSubmitInitialConfig(context);
         objprc_dltmenuperfil.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltmenuperfil);
         aP0_Menu_Codigo=this.A277Menu_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltmenuperfil)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P006L2 */
         pr_default.execute(0, new Object[] {A277Menu_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
         /* End optimized DELETE. */
         /* Optimized DELETE. */
         /* Using cursor P006L3 */
         pr_default.execute(1, new Object[] {A277Menu_Codigo});
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("Menu") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DLTMenuPerfil");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltmenuperfil__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A277Menu_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Menu_Codigo ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_dltmenuperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006L2 ;
          prmP006L2 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006L3 ;
          prmP006L3 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006L2", "DELETE FROM [MenuPerfil]  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006L2)
             ,new CursorDef("P006L3", "DELETE FROM [Menu]  WHERE [Menu_Codigo] = @Menu_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006L3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
