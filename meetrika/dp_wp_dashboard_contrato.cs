/*
               File: DP_WP_Dashboard_Contrato
        Description: Lista os Contratos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:39.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_wp_dashboard_contrato : GXProcedure
   {
      public dp_wp_dashboard_contrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_wp_dashboard_contrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoGestor_ContratadaAreaCod ,
                           int aP1_ContratoGestor_UsuarioCod ,
                           out IGxCollection aP2_Gxm2rootcol )
      {
         this.AV5ContratoGestor_ContratadaAreaCod = aP0_ContratoGestor_ContratadaAreaCod;
         this.AV7ContratoGestor_UsuarioCod = aP1_ContratoGestor_UsuarioCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_ContratoGestor_ContratadaAreaCod ,
                                       int aP1_ContratoGestor_UsuarioCod )
      {
         this.AV5ContratoGestor_ContratadaAreaCod = aP0_ContratoGestor_ContratadaAreaCod;
         this.AV7ContratoGestor_UsuarioCod = aP1_ContratoGestor_UsuarioCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_ContratoGestor_ContratadaAreaCod ,
                                 int aP1_ContratoGestor_UsuarioCod ,
                                 out IGxCollection aP2_Gxm2rootcol )
      {
         dp_wp_dashboard_contrato objdp_wp_dashboard_contrato;
         objdp_wp_dashboard_contrato = new dp_wp_dashboard_contrato();
         objdp_wp_dashboard_contrato.AV5ContratoGestor_ContratadaAreaCod = aP0_ContratoGestor_ContratadaAreaCod;
         objdp_wp_dashboard_contrato.AV7ContratoGestor_UsuarioCod = aP1_ContratoGestor_UsuarioCod;
         objdp_wp_dashboard_contrato.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs") ;
         objdp_wp_dashboard_contrato.context.SetSubmitInitialConfig(context);
         objdp_wp_dashboard_contrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_wp_dashboard_contrato);
         aP2_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_wp_dashboard_contrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000P2 */
         pr_default.execute(0, new Object[] {AV7ContratoGestor_UsuarioCod, AV5ContratoGestor_ContratadaAreaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = P000P2_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P000P2_n1136ContratoGestor_ContratadaCod[0];
            A40Contratada_PessoaCod = P000P2_A40Contratada_PessoaCod[0];
            n40Contratada_PessoaCod = P000P2_n40Contratada_PessoaCod[0];
            A1079ContratoGestor_UsuarioCod = P000P2_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P000P2_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P000P2_n1446ContratoGestor_ContratadaAreaCod[0];
            A1078ContratoGestor_ContratoCod = P000P2_A1078ContratoGestor_ContratoCod[0];
            A92Contrato_Ativo = P000P2_A92Contrato_Ativo[0];
            n92Contrato_Ativo = P000P2_n92Contrato_Ativo[0];
            A41Contratada_PessoaNom = P000P2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P000P2_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P000P2_A77Contrato_Numero[0];
            n77Contrato_Numero = P000P2_n77Contrato_Numero[0];
            A1136ContratoGestor_ContratadaCod = P000P2_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = P000P2_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P000P2_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P000P2_n1446ContratoGestor_ContratadaAreaCod[0];
            A92Contrato_Ativo = P000P2_A92Contrato_Ativo[0];
            n92Contrato_Ativo = P000P2_n92Contrato_Ativo[0];
            A77Contrato_Numero = P000P2_A77Contrato_Numero[0];
            n77Contrato_Numero = P000P2_n77Contrato_Numero[0];
            A40Contratada_PessoaCod = P000P2_A40Contratada_PessoaCod[0];
            n40Contratada_PessoaCod = P000P2_n40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P000P2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P000P2_n41Contratada_PessoaNom[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A1078ContratoGestor_ContratoCod;
            Gxm1sdt_codigos.gxTpr_Descricao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000P2_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P000P2_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P000P2_A40Contratada_PessoaCod = new int[1] ;
         P000P2_n40Contratada_PessoaCod = new bool[] {false} ;
         P000P2_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P000P2_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P000P2_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P000P2_A1078ContratoGestor_ContratoCod = new int[1] ;
         P000P2_A92Contrato_Ativo = new bool[] {false} ;
         P000P2_n92Contrato_Ativo = new bool[] {false} ;
         P000P2_A41Contratada_PessoaNom = new String[] {""} ;
         P000P2_n41Contratada_PessoaNom = new bool[] {false} ;
         P000P2_A77Contrato_Numero = new String[] {""} ;
         P000P2_n77Contrato_Numero = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         Gxm1sdt_codigos = new SdtSDT_Codigos(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_wp_dashboard_contrato__default(),
            new Object[][] {
                new Object[] {
               P000P2_A1136ContratoGestor_ContratadaCod, P000P2_n1136ContratoGestor_ContratadaCod, P000P2_A40Contratada_PessoaCod, P000P2_n40Contratada_PessoaCod, P000P2_A1079ContratoGestor_UsuarioCod, P000P2_A1446ContratoGestor_ContratadaAreaCod, P000P2_n1446ContratoGestor_ContratadaAreaCod, P000P2_A1078ContratoGestor_ContratoCod, P000P2_A92Contrato_Ativo, P000P2_n92Contrato_Ativo,
               P000P2_A41Contratada_PessoaNom, P000P2_n41Contratada_PessoaNom, P000P2_A77Contrato_Numero, P000P2_n77Contrato_Numero
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5ContratoGestor_ContratadaAreaCod ;
      private int AV7ContratoGestor_UsuarioCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A40Contratada_PessoaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n40Contratada_PessoaCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A92Contrato_Ativo ;
      private bool n92Contrato_Ativo ;
      private bool n41Contratada_PessoaNom ;
      private bool n77Contrato_Numero ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000P2_A1136ContratoGestor_ContratadaCod ;
      private bool[] P000P2_n1136ContratoGestor_ContratadaCod ;
      private int[] P000P2_A40Contratada_PessoaCod ;
      private bool[] P000P2_n40Contratada_PessoaCod ;
      private int[] P000P2_A1079ContratoGestor_UsuarioCod ;
      private int[] P000P2_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P000P2_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P000P2_A1078ContratoGestor_ContratoCod ;
      private bool[] P000P2_A92Contrato_Ativo ;
      private bool[] P000P2_n92Contrato_Ativo ;
      private String[] P000P2_A41Contratada_PessoaNom ;
      private bool[] P000P2_n41Contratada_PessoaNom ;
      private String[] P000P2_A77Contrato_Numero ;
      private bool[] P000P2_n77Contrato_Numero ;
      private IGxCollection aP2_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Codigos Gxm1sdt_codigos ;
   }

   public class dp_wp_dashboard_contrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000P2 ;
          prmP000P2 = new Object[] {
          new Object[] {"@AV7ContratoGestor_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000P2", "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contrato_Ativo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero] FROM ((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV7ContratoGestor_UsuarioCod) AND (T2.[Contrato_AreaTrabalhoCod] = @AV5ContratoGestor_ContratadaAreaCod) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000P2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
