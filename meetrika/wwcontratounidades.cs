/*
               File: WWContratoUnidades
        Description: WWContrato Unidades
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 1:2:52.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratounidades : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratounidades( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratounidades( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_34 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_34_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_34_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV20TFContratoUnidades_UndMedNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedNom", AV20TFContratoUnidades_UndMedNom);
               AV21TFContratoUnidades_UndMedNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedNom_Sel", AV21TFContratoUnidades_UndMedNom_Sel);
               AV24TFContratoUnidades_UndMedSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedSigla", AV24TFContratoUnidades_UndMedSigla);
               AV25TFContratoUnidades_UndMedSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedSigla_Sel", AV25TFContratoUnidades_UndMedSigla_Sel);
               AV28TFContratoUnidades_Produtividade = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5)));
               AV29TFContratoUnidades_Produtividade_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5)));
               AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
               AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
               AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
               AV45Pgmname = GetNextPar( );
               A1207ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAIL2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTIL2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312125259");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratounidades.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM", StringUtil.RTrim( AV20TFContratoUnidades_UndMedNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL", StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA", StringUtil.RTrim( AV24TFContratoUnidades_UndMedSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL", StringUtil.RTrim( AV25TFContratoUnidades_UndMedSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( AV28TFContratoUnidades_Produtividade, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO", StringUtil.LTrim( StringUtil.NToC( AV29TFContratoUnidades_Produtividade_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_34", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_34), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV19ContratoUnidades_UndMedNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV19ContratoUnidades_UndMedNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV23ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV23ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV27ContratoUnidades_ProdutividadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV27ContratoUnidades_ProdutividadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption", StringUtil.RTrim( Ddo_contratounidades_undmednom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmednom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratounidades_undmednom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmednom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmednom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmednom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmednom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption", StringUtil.RTrim( Ddo_contratounidades_produtividade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip", StringUtil.RTrim( Ddo_contratounidades_produtividade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_produtividade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_produtividade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmednom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_produtividade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEIL2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTIL2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratounidades.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoUnidades" ;
      }

      public override String GetPgmdesc( )
      {
         return "WWContrato Unidades" ;
      }

      protected void WBIL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_IL2( true) ;
         }
         else
         {
            wb_table1_2_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_Internalname, StringUtil.RTrim( AV20TFContratoUnidades_UndMedNom), StringUtil.RTrim( context.localUtil.Format( AV20TFContratoUnidades_UndMedNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_sel_Internalname, StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV21TFContratoUnidades_UndMedNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_Internalname, StringUtil.RTrim( AV24TFContratoUnidades_UndMedSigla), StringUtil.RTrim( context.localUtil.Format( AV24TFContratoUnidades_UndMedSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_sel_Internalname, StringUtil.RTrim( AV25TFContratoUnidades_UndMedSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV25TFContratoUnidades_UndMedSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( AV28TFContratoUnidades_Produtividade, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV28TFContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoUnidades.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV29TFContratoUnidades_Produtividade_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV29TFContratoUnidades_Produtividade_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_UNDMEDNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_UNDMEDSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoUnidades.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOUNIDADES_PRODUTIVIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_34_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoUnidades.htm");
         }
         wbLoad = true;
      }

      protected void STARTIL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WWContrato Unidades", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPIL0( ) ;
      }

      protected void WSIL2( )
      {
         STARTIL2( ) ;
         EVTIL2( ) ;
      }

      protected void EVTIL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11IL2 */
                              E11IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12IL2 */
                              E12IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13IL2 */
                              E13IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14IL2 */
                              E14IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15IL2 */
                              E15IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16IL2 */
                              E16IL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_34_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
                              SubsflControlProps_342( ) ;
                              AV14Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV43Update_GXI : context.convertURL( context.PathToRelativeUrl( AV14Update))));
                              AV15Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV44Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV15Delete))));
                              A1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_ContratoCod_Internalname), ",", "."));
                              A1204ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( edtContratoUnidades_UndMedCod_Internalname), ",", "."));
                              A1205ContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedNom_Internalname));
                              n1205ContratoUnidades_UndMedNom = false;
                              A1206ContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedSigla_Internalname));
                              n1206ContratoUnidades_UndMedSigla = false;
                              A1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".");
                              n1208ContratoUnidades_Produtividade = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17IL2 */
                                    E17IL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18IL2 */
                                    E18IL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19IL2 */
                                    E19IL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_undmednom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV20TFContratoUnidades_UndMedNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_undmednom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV21TFContratoUnidades_UndMedNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_undmedsigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV24TFContratoUnidades_UndMedSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_undmedsigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV25TFContratoUnidades_UndMedSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_produtividade Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV28TFContratoUnidades_Produtividade )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratounidades_produtividade_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV29TFContratoUnidades_Produtividade_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAIL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_342( ) ;
         while ( nGXsfl_34_idx <= nRC_GXsfl_34 )
         {
            sendrow_342( ) ;
            nGXsfl_34_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_34_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_34_idx+1));
            sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
            SubsflControlProps_342( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV12OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV20TFContratoUnidades_UndMedNom ,
                                       String AV21TFContratoUnidades_UndMedNom_Sel ,
                                       String AV24TFContratoUnidades_UndMedSigla ,
                                       String AV25TFContratoUnidades_UndMedSigla_Sel ,
                                       decimal AV28TFContratoUnidades_Produtividade ,
                                       decimal AV29TFContratoUnidades_Produtividade_To ,
                                       String AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ,
                                       String AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ,
                                       String AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ,
                                       String AV45Pgmname ,
                                       int A1207ContratoUnidades_ContratoCod ,
                                       int A1204ContratoUnidades_UndMedCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIL2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( "", context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV45Pgmname = "WWContratoUnidades";
         context.Gx_err = 0;
      }

      protected void RFIL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 34;
         /* Execute user event: E18IL2 */
         E18IL2 ();
         nGXsfl_34_idx = 1;
         sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
         SubsflControlProps_342( ) ;
         nGXsfl_34_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_342( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                                 AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                                 AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                                 AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                                 AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                                 AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                                 A1205ContratoUnidades_UndMedNom ,
                                                 A1206ContratoUnidades_UndMedSigla ,
                                                 A1208ContratoUnidades_Produtividade ,
                                                 AV12OrderedBy ,
                                                 AV13OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = StringUtil.PadR( StringUtil.RTrim( AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom), 50, "%");
            lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = StringUtil.PadR( StringUtil.RTrim( AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla), 15, "%");
            /* Using cursor H00IL2 */
            pr_default.execute(0, new Object[] {lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom, AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel, lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla, AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel, AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade, AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_34_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1208ContratoUnidades_Produtividade = H00IL2_A1208ContratoUnidades_Produtividade[0];
               n1208ContratoUnidades_Produtividade = H00IL2_n1208ContratoUnidades_Produtividade[0];
               A1206ContratoUnidades_UndMedSigla = H00IL2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IL2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IL2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IL2_n1205ContratoUnidades_UndMedNom[0];
               A1204ContratoUnidades_UndMedCod = H00IL2_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = H00IL2_A1207ContratoUnidades_ContratoCod[0];
               A1206ContratoUnidades_UndMedSigla = H00IL2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IL2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IL2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IL2_n1205ContratoUnidades_UndMedNom[0];
               /* Execute user event: E19IL2 */
               E19IL2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 34;
            WBIL0( ) ;
         }
         nGXsfl_34_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                              AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                              AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                              AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                              AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                              AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1208ContratoUnidades_Produtividade ,
                                              AV12OrderedBy ,
                                              AV13OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = StringUtil.PadR( StringUtil.RTrim( AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom), 50, "%");
         lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = StringUtil.PadR( StringUtil.RTrim( AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla), 15, "%");
         /* Using cursor H00IL3 */
         pr_default.execute(1, new Object[] {lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom, AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel, lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla, AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel, AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade, AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to});
         GRID_nRecordCount = H00IL3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV20TFContratoUnidades_UndMedNom, AV21TFContratoUnidades_UndMedNom_Sel, AV24TFContratoUnidades_UndMedSigla, AV25TFContratoUnidades_UndMedSigla_Sel, AV28TFContratoUnidades_Produtividade, AV29TFContratoUnidades_Produtividade_To, AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV45Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIL0( )
      {
         /* Before Start, stand alone formulas. */
         AV45Pgmname = "WWContratoUnidades";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17IL2 */
         E17IL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV31DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA"), AV19ContratoUnidades_UndMedNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA"), AV23ContratoUnidades_UndMedSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA"), AV27ContratoUnidades_ProdutividadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV12OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            AV20TFContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedNom", AV20TFContratoUnidades_UndMedNom);
            AV21TFContratoUnidades_UndMedNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedNom_Sel", AV21TFContratoUnidades_UndMedNom_Sel);
            AV24TFContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedSigla", AV24TFContratoUnidades_UndMedSigla);
            AV25TFContratoUnidades_UndMedSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedSigla_Sel", AV25TFContratoUnidades_UndMedSigla_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE");
               GX_FocusControl = edtavTfcontratounidades_produtividade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFContratoUnidades_Produtividade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5)));
            }
            else
            {
               AV28TFContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO");
               GX_FocusControl = edtavTfcontratounidades_produtividade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFContratoUnidades_Produtividade_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            else
            {
               AV29TFContratoUnidades_Produtividade_To = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
            AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
            AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_34 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_34"), ",", "."));
            AV33GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV34GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratounidades_undmednom_Caption = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption");
            Ddo_contratounidades_undmednom_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip");
            Ddo_contratounidades_undmednom_Cls = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls");
            Ddo_contratounidades_undmednom_Filteredtext_set = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_set");
            Ddo_contratounidades_undmednom_Selectedvalue_set = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_set");
            Ddo_contratounidades_undmednom_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype");
            Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmednom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc"));
            Ddo_contratounidades_undmednom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc"));
            Ddo_contratounidades_undmednom_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus");
            Ddo_contratounidades_undmednom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter"));
            Ddo_contratounidades_undmednom_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype");
            Ddo_contratounidades_undmednom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange"));
            Ddo_contratounidades_undmednom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist"));
            Ddo_contratounidades_undmednom_Datalisttype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype");
            Ddo_contratounidades_undmednom_Datalistproc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc");
            Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmednom_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc");
            Ddo_contratounidades_undmednom_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc");
            Ddo_contratounidades_undmednom_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata");
            Ddo_contratounidades_undmednom_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter");
            Ddo_contratounidades_undmednom_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound");
            Ddo_contratounidades_undmednom_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext");
            Ddo_contratounidades_undmedsigla_Caption = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption");
            Ddo_contratounidades_undmedsigla_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip");
            Ddo_contratounidades_undmedsigla_Cls = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls");
            Ddo_contratounidades_undmedsigla_Filteredtext_set = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_set");
            Ddo_contratounidades_undmedsigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_set");
            Ddo_contratounidades_undmedsigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype");
            Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmedsigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc"));
            Ddo_contratounidades_undmedsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc"));
            Ddo_contratounidades_undmedsigla_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus");
            Ddo_contratounidades_undmedsigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter"));
            Ddo_contratounidades_undmedsigla_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype");
            Ddo_contratounidades_undmedsigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange"));
            Ddo_contratounidades_undmedsigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist"));
            Ddo_contratounidades_undmedsigla_Datalisttype = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype");
            Ddo_contratounidades_undmedsigla_Datalistproc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc");
            Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmedsigla_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc");
            Ddo_contratounidades_undmedsigla_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc");
            Ddo_contratounidades_undmedsigla_Loadingdata = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata");
            Ddo_contratounidades_undmedsigla_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter");
            Ddo_contratounidades_undmedsigla_Noresultsfound = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound");
            Ddo_contratounidades_undmedsigla_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext");
            Ddo_contratounidades_produtividade_Caption = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption");
            Ddo_contratounidades_produtividade_Tooltip = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip");
            Ddo_contratounidades_produtividade_Cls = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls");
            Ddo_contratounidades_produtividade_Filteredtext_set = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_set");
            Ddo_contratounidades_produtividade_Filteredtextto_set = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_set");
            Ddo_contratounidades_produtividade_Dropdownoptionstype = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype");
            Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace");
            Ddo_contratounidades_produtividade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc"));
            Ddo_contratounidades_produtividade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc"));
            Ddo_contratounidades_produtividade_Sortedstatus = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus");
            Ddo_contratounidades_produtividade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter"));
            Ddo_contratounidades_produtividade_Filtertype = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype");
            Ddo_contratounidades_produtividade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange"));
            Ddo_contratounidades_produtividade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist"));
            Ddo_contratounidades_produtividade_Sortasc = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc");
            Ddo_contratounidades_produtividade_Sortdsc = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc");
            Ddo_contratounidades_produtividade_Cleanfilter = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter");
            Ddo_contratounidades_produtividade_Rangefilterfrom = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom");
            Ddo_contratounidades_produtividade_Rangefilterto = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto");
            Ddo_contratounidades_produtividade_Searchbuttontext = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratounidades_undmednom_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey");
            Ddo_contratounidades_undmednom_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get");
            Ddo_contratounidades_undmednom_Selectedvalue_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get");
            Ddo_contratounidades_undmedsigla_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey");
            Ddo_contratounidades_undmedsigla_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get");
            Ddo_contratounidades_undmedsigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get");
            Ddo_contratounidades_produtividade_Activeeventkey = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey");
            Ddo_contratounidades_produtividade_Filteredtext_get = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get");
            Ddo_contratounidades_produtividade_Filteredtextto_get = cgiGet( "DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV20TFContratoUnidades_UndMedNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV21TFContratoUnidades_UndMedNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV24TFContratoUnidades_UndMedSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV25TFContratoUnidades_UndMedSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV28TFContratoUnidades_Produtividade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV29TFContratoUnidades_Produtividade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17IL2 */
         E17IL2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17IL2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratounidades_undmednom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmednom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_Visible), 5, 0)));
         edtavTfcontratounidades_undmednom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmednom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_sel_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_undmedsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_sel_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_produtividade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratounidades_produtividade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_to_Visible), 5, 0)));
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmednom_Titlecontrolidtoreplace);
         AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = Ddo_contratounidades_undmednom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace);
         AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_Produtividade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_produtividade_Titlecontrolidtoreplace);
         AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = Ddo_contratounidades_produtividade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Unidades Contratadas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Contrata��o", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "Prod. Dia", 0);
         if ( AV12OrderedBy < 1 )
         {
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV31DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV31DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18IL2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV19ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoUnidades_UndMedNom_Titleformat = 2;
         edtContratoUnidades_UndMedNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedNom_Internalname, "Title", edtContratoUnidades_UndMedNom_Title);
         edtContratoUnidades_UndMedSigla_Titleformat = 2;
         edtContratoUnidades_UndMedSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedSigla_Internalname, "Title", edtContratoUnidades_UndMedSigla_Title);
         edtContratoUnidades_Produtividade_Titleformat = 2;
         edtContratoUnidades_Produtividade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prod. Dia", AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_Produtividade_Internalname, "Title", edtContratoUnidades_Produtividade_Title);
         AV33GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridCurrentPage), 10, 0)));
         AV34GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridPageCount), 10, 0)));
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = AV20TFContratoUnidades_UndMedNom;
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = AV21TFContratoUnidades_UndMedNom_Sel;
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = AV24TFContratoUnidades_UndMedSigla;
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = AV25TFContratoUnidades_UndMedSigla_Sel;
         AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade = AV28TFContratoUnidades_Produtividade;
         AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to = AV29TFContratoUnidades_Produtividade_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19ContratoUnidades_UndMedNomTitleFilterData", AV19ContratoUnidades_UndMedNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23ContratoUnidades_UndMedSiglaTitleFilterData", AV23ContratoUnidades_UndMedSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27ContratoUnidades_ProdutividadeTitleFilterData", AV27ContratoUnidades_ProdutividadeTitleFilterData);
      }

      protected void E11IL2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV32PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV32PageToGo) ;
         }
      }

      protected void E12IL2( )
      {
         /* Ddo_contratounidades_undmednom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV20TFContratoUnidades_UndMedNom = Ddo_contratounidades_undmednom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedNom", AV20TFContratoUnidades_UndMedNom);
            AV21TFContratoUnidades_UndMedNom_Sel = Ddo_contratounidades_undmednom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedNom_Sel", AV21TFContratoUnidades_UndMedNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13IL2( )
      {
         /* Ddo_contratounidades_undmedsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV24TFContratoUnidades_UndMedSigla = Ddo_contratounidades_undmedsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedSigla", AV24TFContratoUnidades_UndMedSigla);
            AV25TFContratoUnidades_UndMedSigla_Sel = Ddo_contratounidades_undmedsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedSigla_Sel", AV25TFContratoUnidades_UndMedSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14IL2( )
      {
         /* Ddo_contratounidades_produtividade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFContratoUnidades_Produtividade = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5)));
            AV29TFContratoUnidades_Produtividade_To = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E19IL2( )
      {
         /* Grid_Load Routine */
         AV14Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV14Update);
         AV43Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
         AV15Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV15Delete);
         AV44Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
         edtContratoUnidades_UndMedNom_Link = formatLink("viewcontratounidades.aspx") + "?" + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 34;
         }
         sendrow_342( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_34_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(34, GridRow);
         }
      }

      protected void E15IL2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E16IL2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV12OrderedBy == 1 )
         {
            Ddo_contratounidades_undmednom_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         }
         else if ( AV12OrderedBy == 2 )
         {
            Ddo_contratounidades_undmedsigla_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
         }
         else if ( AV12OrderedBy == 3 )
         {
            Ddo_contratounidades_produtividade_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV45Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV45Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV16Session.Get(AV45Pgmname+"GridState"), "");
         }
         AV12OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM") == 0 )
            {
               AV20TFContratoUnidades_UndMedNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFContratoUnidades_UndMedNom", AV20TFContratoUnidades_UndMedNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoUnidades_UndMedNom)) )
               {
                  Ddo_contratounidades_undmednom_Filteredtext_set = AV20TFContratoUnidades_UndMedNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "FilteredText_set", Ddo_contratounidades_undmednom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM_SEL") == 0 )
            {
               AV21TFContratoUnidades_UndMedNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoUnidades_UndMedNom_Sel", AV21TFContratoUnidades_UndMedNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom_Sel)) )
               {
                  Ddo_contratounidades_undmednom_Selectedvalue_set = AV21TFContratoUnidades_UndMedNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmednom_Internalname, "SelectedValue_set", Ddo_contratounidades_undmednom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
            {
               AV24TFContratoUnidades_UndMedSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TFContratoUnidades_UndMedSigla", AV24TFContratoUnidades_UndMedSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoUnidades_UndMedSigla)) )
               {
                  Ddo_contratounidades_undmedsigla_Filteredtext_set = AV24TFContratoUnidades_UndMedSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "FilteredText_set", Ddo_contratounidades_undmedsigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL") == 0 )
            {
               AV25TFContratoUnidades_UndMedSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFContratoUnidades_UndMedSigla_Sel", AV25TFContratoUnidades_UndMedSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedSigla_Sel)) )
               {
                  Ddo_contratounidades_undmedsigla_Selectedvalue_set = AV25TFContratoUnidades_UndMedSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_undmedsigla_Internalname, "SelectedValue_set", Ddo_contratounidades_undmedsigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_PRODUTIVIDADE") == 0 )
            {
               AV28TFContratoUnidades_Produtividade = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5)));
               AV29TFContratoUnidades_Produtividade_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV28TFContratoUnidades_Produtividade) )
               {
                  Ddo_contratounidades_produtividade_Filteredtext_set = StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "FilteredText_set", Ddo_contratounidades_produtividade_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV29TFContratoUnidades_Produtividade_To) )
               {
                  Ddo_contratounidades_produtividade_Filteredtextto_set = StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratounidades_produtividade_Internalname, "FilteredTextTo_set", Ddo_contratounidades_produtividade_Filteredtextto_set);
               }
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV16Session.Get(AV45Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoUnidades_UndMedNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM";
            AV11GridStateFilterValue.gxTpr_Value = AV20TFContratoUnidades_UndMedNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV21TFContratoUnidades_UndMedNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoUnidades_UndMedSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV24TFContratoUnidades_UndMedSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoUnidades_UndMedSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV25TFContratoUnidades_UndMedSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV28TFContratoUnidades_Produtividade) && (Convert.ToDecimal(0)==AV29TFContratoUnidades_Produtividade_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_PRODUTIVIDADE";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV28TFContratoUnidades_Produtividade, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV29TFContratoUnidades_Produtividade_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV45Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV45Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoUnidades";
         AV16Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_IL2( true) ;
         }
         else
         {
            wb_table2_8_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_28_IL2( true) ;
         }
         else
         {
            wb_table3_28_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table3_28_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IL2e( true) ;
         }
         else
         {
            wb_table1_2_IL2e( false) ;
         }
      }

      protected void wb_table3_28_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_31_IL2( true) ;
         }
         else
         {
            wb_table4_31_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table4_31_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_IL2e( true) ;
         }
         else
         {
            wb_table3_28_IL2e( false) ;
         }
      }

      protected void wb_table4_31_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"34\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Unidades_Contrato Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Contrata��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_Produtividade_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_Produtividade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_Produtividade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV14Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1205ContratoUnidades_UndMedNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoUnidades_UndMedNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_Produtividade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_Produtividade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 34 )
         {
            wbEnd = 0;
            nRC_GXsfl_34 = (short)(nGXsfl_34_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_31_IL2e( true) ;
         }
         else
         {
            wb_table4_31_IL2e( false) ;
         }
      }

      protected void wb_table2_8_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratounidadestitle_Internalname, "Unidades Contratadas", "", "", lblContratounidadestitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_IL2( true) ;
         }
         else
         {
            wb_table5_13_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_34_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoUnidades.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_34_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_IL2( true) ;
         }
         else
         {
            wb_table6_23_IL2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_IL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_IL2e( true) ;
         }
         else
         {
            wb_table2_8_IL2e( false) ;
         }
      }

      protected void wb_table6_23_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_IL2e( true) ;
         }
         else
         {
            wb_table6_23_IL2e( false) ;
         }
      }

      protected void wb_table5_13_IL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_IL2e( true) ;
         }
         else
         {
            wb_table5_13_IL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIL2( ) ;
         WSIL2( ) ;
         WEIL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312125534");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratounidades.js", "?2020312125535");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_342( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_34_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_34_idx;
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD_"+sGXsfl_34_idx;
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD_"+sGXsfl_34_idx;
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_34_idx;
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_34_idx;
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_34_idx;
      }

      protected void SubsflControlProps_fel_342( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_34_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_34_fel_idx;
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD_"+sGXsfl_34_fel_idx;
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD_"+sGXsfl_34_fel_idx;
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_34_fel_idx;
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_34_fel_idx;
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_34_fel_idx;
      }

      protected void sendrow_342( )
      {
         SubsflControlProps_342( ) ;
         WBIL0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_34_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_34_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_34_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV14Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV43Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV43Update_GXI : context.PathToRelativeUrl( AV14Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV14Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV44Delete_GXI : context.PathToRelativeUrl( AV15Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedNom_Internalname,StringUtil.RTrim( A1205ContratoUnidades_UndMedNom),StringUtil.RTrim( context.localUtil.Format( A1205ContratoUnidades_UndMedNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoUnidades_UndMedNom_Link,(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedSigla_Internalname,StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla),StringUtil.RTrim( context.localUtil.Format( A1206ContratoUnidades_UndMedSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_Produtividade_Internalname,StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ",", "")),context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_Produtividade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)34,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_CONTRATOCOD"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_UNDMEDCOD"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, context.localUtil.Format( (decimal)(A1204ContratoUnidades_UndMedCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOUNIDADES_PRODUTIVIDADE"+"_"+sGXsfl_34_idx, GetSecureSignedToken( sGXsfl_34_idx, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_34_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_34_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_34_idx+1));
            sGXsfl_34_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_34_idx), 4, 0)), 4, "0");
            SubsflControlProps_342( ) ;
         }
         /* End function sendrow_342 */
      }

      protected void init_default_properties( )
      {
         lblContratounidadestitle_Internalname = "CONTRATOUNIDADESTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoUnidades_ContratoCod_Internalname = "CONTRATOUNIDADES_CONTRATOCOD";
         edtContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD";
         edtContratoUnidades_UndMedNom_Internalname = "CONTRATOUNIDADES_UNDMEDNOM";
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA";
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfcontratounidades_undmednom_Internalname = "vTFCONTRATOUNIDADES_UNDMEDNOM";
         edtavTfcontratounidades_undmednom_sel_Internalname = "vTFCONTRATOUNIDADES_UNDMEDNOM_SEL";
         edtavTfcontratounidades_undmedsigla_Internalname = "vTFCONTRATOUNIDADES_UNDMEDSIGLA";
         edtavTfcontratounidades_undmedsigla_sel_Internalname = "vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
         edtavTfcontratounidades_produtividade_Internalname = "vTFCONTRATOUNIDADES_PRODUTIVIDADE";
         edtavTfcontratounidades_produtividade_to_Internalname = "vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO";
         Ddo_contratounidades_undmednom_Internalname = "DDO_CONTRATOUNIDADES_UNDMEDNOM";
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_undmedsigla_Internalname = "DDO_CONTRATOUNIDADES_UNDMEDSIGLA";
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_produtividade_Internalname = "DDO_CONTRATOUNIDADES_PRODUTIVIDADE";
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoUnidades_Produtividade_Jsonclick = "";
         edtContratoUnidades_UndMedSigla_Jsonclick = "";
         edtContratoUnidades_UndMedNom_Jsonclick = "";
         edtContratoUnidades_UndMedCod_Jsonclick = "";
         edtContratoUnidades_ContratoCod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoUnidades_UndMedNom_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoUnidades_Produtividade_Titleformat = 0;
         edtContratoUnidades_UndMedSigla_Titleformat = 0;
         edtContratoUnidades_UndMedNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoUnidades_Produtividade_Title = "Prod. Dia";
         edtContratoUnidades_UndMedSigla_Title = "Sigla";
         edtContratoUnidades_UndMedNom_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratounidades_produtividade_to_Jsonclick = "";
         edtavTfcontratounidades_produtividade_to_Visible = 1;
         edtavTfcontratounidades_produtividade_Jsonclick = "";
         edtavTfcontratounidades_produtividade_Visible = 1;
         edtavTfcontratounidades_undmedsigla_sel_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_sel_Visible = 1;
         edtavTfcontratounidades_undmedsigla_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_Visible = 1;
         edtavTfcontratounidades_undmednom_sel_Jsonclick = "";
         edtavTfcontratounidades_undmednom_sel_Visible = 1;
         edtavTfcontratounidades_undmednom_Jsonclick = "";
         edtavTfcontratounidades_undmednom_Visible = 1;
         Ddo_contratounidades_produtividade_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_produtividade_Rangefilterto = "At�";
         Ddo_contratounidades_produtividade_Rangefilterfrom = "Desde";
         Ddo_contratounidades_produtividade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_produtividade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_produtividade_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_produtividade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratounidades_produtividade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Filtertype = "Numeric";
         Ddo_contratounidades_produtividade_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_produtividade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_produtividade_Cls = "ColumnSettings";
         Ddo_contratounidades_produtividade_Tooltip = "Op��es";
         Ddo_contratounidades_produtividade_Caption = "";
         Ddo_contratounidades_undmedsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmedsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmedsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmedsigla_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmedsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmedsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmedsigla_Datalistproc = "GetWWContratoUnidadesFilterData";
         Ddo_contratounidades_undmedsigla_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmedsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmedsigla_Filtertype = "Character";
         Ddo_contratounidades_undmedsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmedsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmedsigla_Cls = "ColumnSettings";
         Ddo_contratounidades_undmedsigla_Tooltip = "Op��es";
         Ddo_contratounidades_undmedsigla_Caption = "";
         Ddo_contratounidades_undmednom_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmednom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmednom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmednom_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmednom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmednom_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmednom_Datalistproc = "GetWWContratoUnidadesFilterData";
         Ddo_contratounidades_undmednom_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmednom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmednom_Filtertype = "Character";
         Ddo_contratounidades_undmednom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmednom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmednom_Cls = "ColumnSettings";
         Ddo_contratounidades_undmednom_Tooltip = "Op��es";
         Ddo_contratounidades_undmednom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "WWContrato Unidades";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV19ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED","{handler:'E12IL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratounidades_undmednom_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmednom_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmednom_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED","{handler:'E13IL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratounidades_undmedsigla_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmedsigla_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmedsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED","{handler:'E14IL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratounidades_produtividade_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_produtividade_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredText_get'},{av:'Ddo_contratounidades_produtividade_Filteredtextto_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19IL2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV14Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV15Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoUnidades_UndMedNom_Link',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15IL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV21TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV28TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E16IL2',iparms:[{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratounidades_undmednom_Activeeventkey = "";
         Ddo_contratounidades_undmednom_Filteredtext_get = "";
         Ddo_contratounidades_undmednom_Selectedvalue_get = "";
         Ddo_contratounidades_undmedsigla_Activeeventkey = "";
         Ddo_contratounidades_undmedsigla_Filteredtext_get = "";
         Ddo_contratounidades_undmedsigla_Selectedvalue_get = "";
         Ddo_contratounidades_produtividade_Activeeventkey = "";
         Ddo_contratounidades_produtividade_Filteredtext_get = "";
         Ddo_contratounidades_produtividade_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV20TFContratoUnidades_UndMedNom = "";
         AV21TFContratoUnidades_UndMedNom_Sel = "";
         AV24TFContratoUnidades_UndMedSigla = "";
         AV25TFContratoUnidades_UndMedSigla_Sel = "";
         AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = "";
         AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = "";
         AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = "";
         AV45Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV19ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratounidades_undmednom_Filteredtext_set = "";
         Ddo_contratounidades_undmednom_Selectedvalue_set = "";
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         Ddo_contratounidades_undmedsigla_Filteredtext_set = "";
         Ddo_contratounidades_undmedsigla_Selectedvalue_set = "";
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         Ddo_contratounidades_produtividade_Filteredtext_set = "";
         Ddo_contratounidades_produtividade_Filteredtextto_set = "";
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Update = "";
         AV43Update_GXI = "";
         AV15Delete = "";
         AV44Delete_GXI = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = "";
         lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = "";
         AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel = "";
         AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom = "";
         AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel = "";
         AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla = "";
         H00IL2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         H00IL2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         H00IL2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         H00IL2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         H00IL2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         H00IL2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         H00IL2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00IL2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H00IL3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratounidadestitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratounidades__default(),
            new Object[][] {
                new Object[] {
               H00IL2_A1208ContratoUnidades_Produtividade, H00IL2_n1208ContratoUnidades_Produtividade, H00IL2_A1206ContratoUnidades_UndMedSigla, H00IL2_n1206ContratoUnidades_UndMedSigla, H00IL2_A1205ContratoUnidades_UndMedNom, H00IL2_n1205ContratoUnidades_UndMedNom, H00IL2_A1204ContratoUnidades_UndMedCod, H00IL2_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               H00IL3_AGRID_nRecordCount
               }
            }
         );
         AV45Pgmname = "WWContratoUnidades";
         /* GeneXus formulas. */
         AV45Pgmname = "WWContratoUnidades";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_34 ;
      private short nGXsfl_34_idx=1 ;
      private short AV12OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_34_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoUnidades_UndMedNom_Titleformat ;
      private short edtContratoUnidades_UndMedSigla_Titleformat ;
      private short edtContratoUnidades_Produtividade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters ;
      private int edtavTfcontratounidades_undmednom_Visible ;
      private int edtavTfcontratounidades_undmednom_sel_Visible ;
      private int edtavTfcontratounidades_undmedsigla_Visible ;
      private int edtavTfcontratounidades_undmedsigla_sel_Visible ;
      private int edtavTfcontratounidades_produtividade_Visible ;
      private int edtavTfcontratounidades_produtividade_to_Visible ;
      private int edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV32PageToGo ;
      private int AV46GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV33GridCurrentPage ;
      private long AV34GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV28TFContratoUnidades_Produtividade ;
      private decimal AV29TFContratoUnidades_Produtividade_To ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private decimal AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ;
      private decimal AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratounidades_undmednom_Activeeventkey ;
      private String Ddo_contratounidades_undmednom_Filteredtext_get ;
      private String Ddo_contratounidades_undmednom_Selectedvalue_get ;
      private String Ddo_contratounidades_undmedsigla_Activeeventkey ;
      private String Ddo_contratounidades_undmedsigla_Filteredtext_get ;
      private String Ddo_contratounidades_undmedsigla_Selectedvalue_get ;
      private String Ddo_contratounidades_produtividade_Activeeventkey ;
      private String Ddo_contratounidades_produtividade_Filteredtext_get ;
      private String Ddo_contratounidades_produtividade_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_34_idx="0001" ;
      private String AV20TFContratoUnidades_UndMedNom ;
      private String AV21TFContratoUnidades_UndMedNom_Sel ;
      private String AV24TFContratoUnidades_UndMedSigla ;
      private String AV25TFContratoUnidades_UndMedSigla_Sel ;
      private String AV45Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratounidades_undmednom_Caption ;
      private String Ddo_contratounidades_undmednom_Tooltip ;
      private String Ddo_contratounidades_undmednom_Cls ;
      private String Ddo_contratounidades_undmednom_Filteredtext_set ;
      private String Ddo_contratounidades_undmednom_Selectedvalue_set ;
      private String Ddo_contratounidades_undmednom_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmednom_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmednom_Sortedstatus ;
      private String Ddo_contratounidades_undmednom_Filtertype ;
      private String Ddo_contratounidades_undmednom_Datalisttype ;
      private String Ddo_contratounidades_undmednom_Datalistproc ;
      private String Ddo_contratounidades_undmednom_Sortasc ;
      private String Ddo_contratounidades_undmednom_Sortdsc ;
      private String Ddo_contratounidades_undmednom_Loadingdata ;
      private String Ddo_contratounidades_undmednom_Cleanfilter ;
      private String Ddo_contratounidades_undmednom_Noresultsfound ;
      private String Ddo_contratounidades_undmednom_Searchbuttontext ;
      private String Ddo_contratounidades_undmedsigla_Caption ;
      private String Ddo_contratounidades_undmedsigla_Tooltip ;
      private String Ddo_contratounidades_undmedsigla_Cls ;
      private String Ddo_contratounidades_undmedsigla_Filteredtext_set ;
      private String Ddo_contratounidades_undmedsigla_Selectedvalue_set ;
      private String Ddo_contratounidades_undmedsigla_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmedsigla_Sortedstatus ;
      private String Ddo_contratounidades_undmedsigla_Filtertype ;
      private String Ddo_contratounidades_undmedsigla_Datalisttype ;
      private String Ddo_contratounidades_undmedsigla_Datalistproc ;
      private String Ddo_contratounidades_undmedsigla_Sortasc ;
      private String Ddo_contratounidades_undmedsigla_Sortdsc ;
      private String Ddo_contratounidades_undmedsigla_Loadingdata ;
      private String Ddo_contratounidades_undmedsigla_Cleanfilter ;
      private String Ddo_contratounidades_undmedsigla_Noresultsfound ;
      private String Ddo_contratounidades_undmedsigla_Searchbuttontext ;
      private String Ddo_contratounidades_produtividade_Caption ;
      private String Ddo_contratounidades_produtividade_Tooltip ;
      private String Ddo_contratounidades_produtividade_Cls ;
      private String Ddo_contratounidades_produtividade_Filteredtext_set ;
      private String Ddo_contratounidades_produtividade_Filteredtextto_set ;
      private String Ddo_contratounidades_produtividade_Dropdownoptionstype ;
      private String Ddo_contratounidades_produtividade_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_produtividade_Sortedstatus ;
      private String Ddo_contratounidades_produtividade_Filtertype ;
      private String Ddo_contratounidades_produtividade_Sortasc ;
      private String Ddo_contratounidades_produtividade_Sortdsc ;
      private String Ddo_contratounidades_produtividade_Cleanfilter ;
      private String Ddo_contratounidades_produtividade_Rangefilterfrom ;
      private String Ddo_contratounidades_produtividade_Rangefilterto ;
      private String Ddo_contratounidades_produtividade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontratounidades_undmednom_Internalname ;
      private String edtavTfcontratounidades_undmednom_Jsonclick ;
      private String edtavTfcontratounidades_undmednom_sel_Internalname ;
      private String edtavTfcontratounidades_undmednom_sel_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_sel_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_sel_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_Internalname ;
      private String edtavTfcontratounidades_produtividade_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_to_Internalname ;
      private String edtavTfcontratounidades_produtividade_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoUnidades_ContratoCod_Internalname ;
      private String edtContratoUnidades_UndMedCod_Internalname ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String edtContratoUnidades_UndMedNom_Internalname ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private String edtContratoUnidades_UndMedSigla_Internalname ;
      private String edtContratoUnidades_Produtividade_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ;
      private String lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ;
      private String AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ;
      private String AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ;
      private String AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ;
      private String AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratounidades_undmednom_Internalname ;
      private String Ddo_contratounidades_undmedsigla_Internalname ;
      private String Ddo_contratounidades_produtividade_Internalname ;
      private String edtContratoUnidades_UndMedNom_Title ;
      private String edtContratoUnidades_UndMedSigla_Title ;
      private String edtContratoUnidades_Produtividade_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoUnidades_UndMedNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratounidadestitle_Internalname ;
      private String lblContratounidadestitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_34_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoUnidades_ContratoCod_Jsonclick ;
      private String edtContratoUnidades_UndMedCod_Jsonclick ;
      private String edtContratoUnidades_UndMedNom_Jsonclick ;
      private String edtContratoUnidades_UndMedSigla_Jsonclick ;
      private String edtContratoUnidades_Produtividade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratounidades_undmednom_Includesortasc ;
      private bool Ddo_contratounidades_undmednom_Includesortdsc ;
      private bool Ddo_contratounidades_undmednom_Includefilter ;
      private bool Ddo_contratounidades_undmednom_Filterisrange ;
      private bool Ddo_contratounidades_undmednom_Includedatalist ;
      private bool Ddo_contratounidades_undmedsigla_Includesortasc ;
      private bool Ddo_contratounidades_undmedsigla_Includesortdsc ;
      private bool Ddo_contratounidades_undmedsigla_Includefilter ;
      private bool Ddo_contratounidades_undmedsigla_Filterisrange ;
      private bool Ddo_contratounidades_undmedsigla_Includedatalist ;
      private bool Ddo_contratounidades_produtividade_Includesortasc ;
      private bool Ddo_contratounidades_produtividade_Includesortdsc ;
      private bool Ddo_contratounidades_produtividade_Includefilter ;
      private bool Ddo_contratounidades_produtividade_Filterisrange ;
      private bool Ddo_contratounidades_produtividade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14Update_IsBlob ;
      private bool AV15Delete_IsBlob ;
      private String AV22ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ;
      private String AV26ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ;
      private String AV30ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ;
      private String AV43Update_GXI ;
      private String AV44Delete_GXI ;
      private String AV14Update ;
      private String AV15Delete ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00IL2_A1208ContratoUnidades_Produtividade ;
      private bool[] H00IL2_n1208ContratoUnidades_Produtividade ;
      private String[] H00IL2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] H00IL2_n1206ContratoUnidades_UndMedSigla ;
      private String[] H00IL2_A1205ContratoUnidades_UndMedNom ;
      private bool[] H00IL2_n1205ContratoUnidades_UndMedNom ;
      private int[] H00IL2_A1204ContratoUnidades_UndMedCod ;
      private int[] H00IL2_A1207ContratoUnidades_ContratoCod ;
      private long[] H00IL3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV19ContratoUnidades_UndMedNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV23ContratoUnidades_UndMedSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27ContratoUnidades_ProdutividadeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV31DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratounidades__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IL2( IGxContext context ,
                                             String AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                             String AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                             String AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                             String AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                             decimal AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                             decimal AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_ContratoCod]";
         sFromString = " FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         sOrderString = "";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla] DESC";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade]";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IL3( IGxContext context ,
                                             String AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel ,
                                             String AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom ,
                                             String AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel ,
                                             String AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla ,
                                             decimal AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade ,
                                             decimal AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] like @lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Sigla] = @AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] >= @AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoUnidades_Produtividade] <= @AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IL2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H00IL3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IL2 ;
          prmH00IL2 = new Object[] {
          new Object[] {"@lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IL3 ;
          prmH00IL3 = new Object[] {
          new Object[] {"@lV37WWContratoUnidadesDS_1_Tfcontratounidades_undmednom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38WWContratoUnidadesDS_2_Tfcontratounidades_undmednom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39WWContratoUnidadesDS_3_Tfcontratounidades_undmedsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40WWContratoUnidadesDS_4_Tfcontratounidades_undmedsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41WWContratoUnidadesDS_5_Tfcontratounidades_produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV42WWContratoUnidadesDS_6_Tfcontratounidades_produtividade_to",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IL2,11,0,true,false )
             ,new CursorDef("H00IL3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IL3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                return;
       }
    }

 }

}
