/*
               File: ws_EnviaEmail
        Description: Stub for ws_EnviaEmail
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:44:25.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ws_enviaemail : GXProcedure
   {
      public ws_enviaemail( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public ws_enviaemail( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           IGxCollection aP1_Usuarios ,
                           String aP2_Subject ,
                           String aP3_EmailText ,
                           IGxCollection aP4_Attachments ,
                           ref String aP5_Resultado )
      {
         this.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV3Usuarios = aP1_Usuarios;
         this.AV4Subject = aP2_Subject;
         this.AV5EmailText = aP3_EmailText;
         this.AV6Attachments = aP4_Attachments;
         this.AV7Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV7Resultado;
      }

      public String executeUdp( int aP0_AreaTrabalho_Codigo ,
                                IGxCollection aP1_Usuarios ,
                                String aP2_Subject ,
                                String aP3_EmailText ,
                                IGxCollection aP4_Attachments )
      {
         this.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV3Usuarios = aP1_Usuarios;
         this.AV4Subject = aP2_Subject;
         this.AV5EmailText = aP3_EmailText;
         this.AV6Attachments = aP4_Attachments;
         this.AV7Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV7Resultado;
         return AV7Resultado ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 IGxCollection aP1_Usuarios ,
                                 String aP2_Subject ,
                                 String aP3_EmailText ,
                                 IGxCollection aP4_Attachments ,
                                 ref String aP5_Resultado )
      {
         ws_enviaemail objws_enviaemail;
         objws_enviaemail = new ws_enviaemail();
         objws_enviaemail.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objws_enviaemail.AV3Usuarios = aP1_Usuarios;
         objws_enviaemail.AV4Subject = aP2_Subject;
         objws_enviaemail.AV5EmailText = aP3_EmailText;
         objws_enviaemail.AV6Attachments = aP4_Attachments;
         objws_enviaemail.AV7Resultado = aP5_Resultado;
         objws_enviaemail.context.SetSubmitInitialConfig(context);
         objws_enviaemail.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objws_enviaemail);
         aP5_Resultado=this.AV7Resultado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((ws_enviaemail)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         context.nSOAPErr = 0;
         context.sSOAPErrMsg = "";
         GXSoapHTTPClient.Host = context.GetServerName( );
         GXSoapHTTPClient.Port = context.GetServerPort( );
         GXSoapHTTPClient.BaseURL = context.GetScriptPath( );
         SoapParm.AssignLocationProperties( context, "Aws_EnviaEmail", GXSoapHTTPClient);
         GXSoapHTTPClient.AddHeader("Content-type", "text/xml;charset=utf-8");
         GXSoapHTTPClient.AddHeader("SOAPAction", "GxEv3Up14_Meetrikaaction/"+"AWS_ENVIAEMAIL.Execute");
         GXSoapXMLWriter.OpenRequest(GXSoapHTTPClient);
         GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
         GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Envelope");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
         GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
         GXSoapXMLWriter.WriteStartElement("Execute");
         GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         GXSoapXMLWriter.WriteElement("Areatrabalho_codigo", StringUtil.Trim( StringUtil.Str( (decimal)(AV2AreaTrabalho_Codigo), 6, 0)));
         GXSoapXMLWriter.WriteAttribute("xmlns", "http://tempuri.org/");
         if ( AV3Usuarios != null )
         {
            AV3Usuarios.writexmlcollection(GXSoapXMLWriter, "Usuarios", "http://tempuri.org/", "item", "http://tempuri.org/");
         }
         GXSoapXMLWriter.WriteElement("Subject", StringUtil.RTrim( AV4Subject));
         GXSoapXMLWriter.WriteAttribute("xmlns", "http://tempuri.org/");
         GXSoapXMLWriter.WriteElement("Emailtext", StringUtil.RTrim( AV5EmailText));
         GXSoapXMLWriter.WriteAttribute("xmlns", "http://tempuri.org/");
         if ( AV6Attachments != null )
         {
            AV6Attachments.writexmlcollection(GXSoapXMLWriter, "Attachments", "http://tempuri.org/", "item", "http://tempuri.org/");
         }
         GXSoapXMLWriter.WriteElement("Resultado", StringUtil.RTrim( AV7Resultado));
         GXSoapXMLWriter.WriteAttribute("xmlns", "http://tempuri.org/");
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.Close();
         GXSoapHTTPClient.Execute("POST", "aws_enviaemail.aspx");
         if ( GXSoapHTTPClient.ErrCode != 0 )
         {
            currSoapErr = (short)(GXSoapHTTPClient.ErrCode*-1-10000);
            currSoapErrmsg = GXSoapHTTPClient.ErrDescription;
         }
         GXSoapXMLReader.OpenResponse(GXSoapHTTPClient);
         GXSoapError = GXSoapXMLReader.Read();
         while ( GXSoapError > 0 )
         {
            if ( StringUtil.StringSearch( GXSoapXMLReader.Name, "Body", 1) > 0 )
            {
               if (true) break;
            }
            GXSoapError = GXSoapXMLReader.Read();
         }
         if ( GXSoapError > 0 )
         {
            GXSoapError = GXSoapXMLReader.Read();
            if ( ( GXSoapError > 0 ) && ( StringUtil.StrCmp(GXSoapXMLReader.LocalName, "Fault") == 0 ) )
            {
               soapFault = 1;
            }
         }
         sTagName = GXSoapXMLReader.Name;
         if ( GXSoapXMLReader.IsSimple == 0 )
         {
            if ( soapFault == 0 )
            {
               GXSoapError = GXSoapXMLReader.Read();
               if ( ( GXSoapError > 0 ) && ( StringUtil.StrCmp(GXSoapXMLReader.LocalName, "Fault") == 0 ) )
               {
                  soapFault = 1;
               }
            }
            if ( soapFault == 1 )
            {
               GXSoapXMLReader.Read();
               while ( ! ( ( StringUtil.StrCmp(GXSoapXMLReader.LocalName, "Fault") == 0 ) && ( GXSoapXMLReader.NodeType == 2 ) ) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapXMLReader.Name), "faultcode") == 0 )
                  {
                     sFaultCode = GXSoapXMLReader.Value;
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapXMLReader.Name), "faultstring") == 0 )
                  {
                     sFaultString = GXSoapXMLReader.Value;
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapXMLReader.Name), "detail") == 0 )
                  {
                     if ( GXSoapXMLReader.IsSimple != 0 )
                     {
                        GXSoapXMLReader.Read();
                        sDetail = GXSoapXMLReader.ReadRawXML();
                     }
                     else
                     {
                        sDetail = GXSoapXMLReader.Value;
                     }
                  }
                  if ( ! ( ( StringUtil.StrCmp(GXSoapXMLReader.LocalName, "Fault") == 0 ) && ( GXSoapXMLReader.NodeType == 2 ) ) )
                  {
                     GXSoapXMLReader.Read();
                  }
               }
               if ( StringUtil.StringSearch( StringUtil.Lower( sFaultCode), "client", 1) > 0 )
               {
                  currSoapErr = (short)(-20004);
                  currSoapErrmsg = currSoapErrmsg + "SOAP Fault: Error in client request." + StringUtil.NewLine( ) + "Message: " + sFaultString + StringUtil.NewLine( ) + "Detail: " + sDetail;
               }
               else if ( StringUtil.StringSearch( StringUtil.Lower( sFaultCode), "server", 1) > 0 )
               {
                  currSoapErr = (short)(-20005);
                  currSoapErrmsg = currSoapErrmsg + "SOAP Fault: Error in server execution." + StringUtil.NewLine( ) + "Message: " + sFaultString + StringUtil.NewLine( ) + "Detail: " + sDetail;
               }
               else
               {
                  currSoapErr = (short)(-20006);
                  currSoapErrmsg = currSoapErrmsg + "Unknown error: " + sFaultCode + StringUtil.NewLine( ) + "Message: " + sFaultString + StringUtil.NewLine( ) + "Detail: " + sDetail;
               }
            }
            else
            {
               nOutParmCount = 0;
               while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
               {
                  readOk = 0;
                  if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Resultado") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "http://tempuri.org/") == 0 ) )
                  {
                     AV7Resultado = GXSoapXMLReader.Value;
                     if ( GXSoapError > 0 )
                     {
                        readOk = 1;
                     }
                     GXSoapError = GXSoapXMLReader.Read();
                  }
                  nOutParmCount = (short)(nOutParmCount+1);
                  if ( readOk == 0 )
                  {
                     context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                     context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                     GXSoapError = (short)(nOutParmCount*-1);
                  }
               }
            }
         }
         GXSoapXMLReader.Close();
         if ( currSoapErr == 0 )
         {
            if ( GXSoapError < 0 )
            {
               currSoapErr = (short)(GXSoapError*-1);
               currSoapErrmsg = context.sSOAPErrMsg;
            }
            else
            {
               if ( GXSoapXMLReader.ErrCode > 0 )
               {
                  currSoapErr = (short)(GXSoapXMLReader.ErrCode*-1);
                  currSoapErrmsg = GXSoapXMLReader.ErrDescription;
               }
               else
               {
                  if ( GXSoapError == 0 )
                  {
                     currSoapErr = (short)(-20001);
                     currSoapErrmsg = "Malformed SOAP message.";
                  }
                  else
                  {
                     currSoapErr = 0;
                     currSoapErrmsg = "No error.";
                  }
               }
            }
         }
         if ( currSoapErr != 0 )
         {
            oLocation = SoapParm.getlocation( context, "Aws_EnviaEmail");
            if ( oLocation.CancelOnError == 1 )
            {
               throw new Exception( currSoapErrmsg+"("+StringUtil.LTrim( StringUtil.NToC( (decimal)(currSoapErr), 6, 0, ".", ""))+")") ;
            }
         }
         context.nSOAPErr = currSoapErr;
         context.sSOAPErrMsg = currSoapErrmsg;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXSoapHTTPClient = new GxHttpClient( context);
         sReturn = "";
         GXSoapXMLReader = new GXXMLReader(context.GetPhysicalPath());
         GXSoapXMLWriter = new GXXMLWriter(context.GetPhysicalPath());
         currSoapErrmsg = "";
         sTagName = "";
         sFaultCode = "";
         sFaultString = "";
         sDetail = "";
         oLocation = new GxLocation();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXSoapError ;
      private short currSoapErr ;
      private short soapFault ;
      private short readOk ;
      private short nOutParmCount ;
      private int AV2AreaTrabalho_Codigo ;
      private String AV4Subject ;
      private String AV5EmailText ;
      private String AV7Resultado ;
      private String sReturn ;
      private String currSoapErrmsg ;
      private String sTagName ;
      private String sFaultCode ;
      private String sFaultString ;
      private String sDetail ;
      private GXXMLReader GXSoapXMLReader ;
      private GXXMLWriter GXSoapXMLWriter ;
      private GxHttpClient GXSoapHTTPClient ;
      private GxLocation oLocation ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP5_Resultado ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV3Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV6Attachments ;
   }

}
