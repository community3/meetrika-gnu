/*
               File: ContagemResultadoChckLst
        Description: Contagem Resultado Chck Lst
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:49.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadochcklst : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1868ContagemResultadoChckLst_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1868ContagemResultadoChckLst_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A758CheckList_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContagemResultadoChckLst_Cumpre.Name = "CONTAGEMRESULTADOCHCKLST_CUMPRE";
         cmbContagemResultadoChckLst_Cumpre.WebTags = "";
         cmbContagemResultadoChckLst_Cumpre.addItem("S", "Sim", 0);
         cmbContagemResultadoChckLst_Cumpre.addItem("N", "N�o", 0);
         cmbContagemResultadoChckLst_Cumpre.addItem("X", "N/A", 0);
         if ( cmbContagemResultadoChckLst_Cumpre.ItemCount > 0 )
         {
            A762ContagemResultadoChckLst_Cumpre = cmbContagemResultadoChckLst_Cumpre.getValidValue(A762ContagemResultadoChckLst_Cumpre);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Chck Lst", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadochcklst( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadochcklst( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagemResultadoChckLst_Cumpre = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemResultadoChckLst_Cumpre.ItemCount > 0 )
         {
            A762ContagemResultadoChckLst_Cumpre = cmbContagemResultadoChckLst_Cumpre.getValidValue(A762ContagemResultadoChckLst_Cumpre);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2I206( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2I206e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2I206( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2I206( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2I206e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Chck Lst", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoChckLst.htm");
            wb_table3_28_2I206( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2I206e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2I206e( true) ;
         }
         else
         {
            wb_table1_2_2I206e( false) ;
         }
      }

      protected void wb_table3_28_2I206( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2I206( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2I206e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLst.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLst.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2I206e( true) ;
         }
         else
         {
            wb_table3_28_2I206e( false) ;
         }
      }

      protected void wb_table4_34_2I206( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklst_codigo_Internalname, "OS", "", "", lblTextblockcontagemresultadochcklst_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLst_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0, ",", "")), ((edtContagemResultadoChckLst_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A761ContagemResultadoChckLst_Codigo), "ZZZ9")) : context.localUtil.Format( (decimal)(A761ContagemResultadoChckLst_Codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLst_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLst_Codigo_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklst_oscod_Internalname, "Chck Lst_OSCod", "", "", lblTextblockcontagemresultadochcklst_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLst_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0, ",", "")), ((edtContagemResultadoChckLst_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1868ContagemResultadoChckLst_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1868ContagemResultadoChckLst_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLst_OSCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLst_OSCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_codigo_Internalname, "Check List Item", "", "", lblTextblockchecklist_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCheckList_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")), ((edtCheckList_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheckList_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtCheckList_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklst_cumpre_Internalname, "Adequa��o", "", "", lblTextblockcontagemresultadochcklst_cumpre_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLst.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoChckLst_Cumpre, cmbContagemResultadoChckLst_Cumpre_Internalname, StringUtil.RTrim( A762ContagemResultadoChckLst_Cumpre), 1, cmbContagemResultadoChckLst_Cumpre_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagemResultadoChckLst_Cumpre.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_ContagemResultadoChckLst.htm");
            cmbContagemResultadoChckLst_Cumpre.CurrentValue = StringUtil.RTrim( A762ContagemResultadoChckLst_Cumpre);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoChckLst_Cumpre_Internalname, "Values", (String)(cmbContagemResultadoChckLst_Cumpre.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2I206e( true) ;
         }
         else
         {
            wb_table4_34_2I206e( false) ;
         }
      }

      protected void wb_table2_5_2I206( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLst.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2I206e( true) ;
         }
         else
         {
            wb_table2_5_2I206e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLST_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A761ContagemResultadoChckLst_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
               }
               else
               {
                  A761ContagemResultadoChckLst_Codigo = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLST_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1868ContagemResultadoChckLst_OSCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
               }
               else
               {
                  A1868ContagemResultadoChckLst_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLst_OSCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CHECKLIST_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCheckList_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A758CheckList_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               }
               else
               {
                  A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               }
               cmbContagemResultadoChckLst_Cumpre.CurrentValue = cgiGet( cmbContagemResultadoChckLst_Cumpre_Internalname);
               A762ContagemResultadoChckLst_Cumpre = cgiGet( cmbContagemResultadoChckLst_Cumpre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
               /* Read saved values. */
               Z761ContagemResultadoChckLst_Codigo = (short)(context.localUtil.CToN( cgiGet( "Z761ContagemResultadoChckLst_Codigo"), ",", "."));
               Z762ContagemResultadoChckLst_Cumpre = cgiGet( "Z762ContagemResultadoChckLst_Cumpre");
               Z758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z758CheckList_Codigo"), ",", "."));
               Z1868ContagemResultadoChckLst_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z1868ContagemResultadoChckLst_OSCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A761ContagemResultadoChckLst_Codigo = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2I206( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2I206( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2I0( )
      {
      }

      protected void ZM2I206( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z762ContagemResultadoChckLst_Cumpre = T002I3_A762ContagemResultadoChckLst_Cumpre[0];
               Z758CheckList_Codigo = T002I3_A758CheckList_Codigo[0];
               Z1868ContagemResultadoChckLst_OSCod = T002I3_A1868ContagemResultadoChckLst_OSCod[0];
            }
            else
            {
               Z762ContagemResultadoChckLst_Cumpre = A762ContagemResultadoChckLst_Cumpre;
               Z758CheckList_Codigo = A758CheckList_Codigo;
               Z1868ContagemResultadoChckLst_OSCod = A1868ContagemResultadoChckLst_OSCod;
            }
         }
         if ( GX_JID == -1 )
         {
            Z761ContagemResultadoChckLst_Codigo = A761ContagemResultadoChckLst_Codigo;
            Z762ContagemResultadoChckLst_Cumpre = A762ContagemResultadoChckLst_Cumpre;
            Z758CheckList_Codigo = A758CheckList_Codigo;
            Z1868ContagemResultadoChckLst_OSCod = A1868ContagemResultadoChckLst_OSCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2I206( )
      {
         /* Using cursor T002I6 */
         pr_default.execute(4, new Object[] {A761ContagemResultadoChckLst_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound206 = 1;
            A762ContagemResultadoChckLst_Cumpre = T002I6_A762ContagemResultadoChckLst_Cumpre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
            A758CheckList_Codigo = T002I6_A758CheckList_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
            A1868ContagemResultadoChckLst_OSCod = T002I6_A1868ContagemResultadoChckLst_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
            ZM2I206( -1) ;
         }
         pr_default.close(4);
         OnLoadActions2I206( ) ;
      }

      protected void OnLoadActions2I206( )
      {
      }

      protected void CheckExtendedTable2I206( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002I5 */
         pr_default.execute(3, new Object[] {A1868ContagemResultadoChckLst_OSCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLST_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T002I4 */
         pr_default.execute(2, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Check List'.", "ForeignKeyNotFound", 1, "CHECKLIST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheckList_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2I206( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1868ContagemResultadoChckLst_OSCod )
      {
         /* Using cursor T002I7 */
         pr_default.execute(5, new Object[] {A1868ContagemResultadoChckLst_OSCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLST_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_2( int A758CheckList_Codigo )
      {
         /* Using cursor T002I8 */
         pr_default.execute(6, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Check List'.", "ForeignKeyNotFound", 1, "CHECKLIST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheckList_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2I206( )
      {
         /* Using cursor T002I9 */
         pr_default.execute(7, new Object[] {A761ContagemResultadoChckLst_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound206 = 1;
         }
         else
         {
            RcdFound206 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002I3 */
         pr_default.execute(1, new Object[] {A761ContagemResultadoChckLst_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2I206( 1) ;
            RcdFound206 = 1;
            A761ContagemResultadoChckLst_Codigo = T002I3_A761ContagemResultadoChckLst_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
            A762ContagemResultadoChckLst_Cumpre = T002I3_A762ContagemResultadoChckLst_Cumpre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
            A758CheckList_Codigo = T002I3_A758CheckList_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
            A1868ContagemResultadoChckLst_OSCod = T002I3_A1868ContagemResultadoChckLst_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
            Z761ContagemResultadoChckLst_Codigo = A761ContagemResultadoChckLst_Codigo;
            sMode206 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2I206( ) ;
            if ( AnyError == 1 )
            {
               RcdFound206 = 0;
               InitializeNonKey2I206( ) ;
            }
            Gx_mode = sMode206;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound206 = 0;
            InitializeNonKey2I206( ) ;
            sMode206 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode206;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2I206( ) ;
         if ( RcdFound206 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound206 = 0;
         /* Using cursor T002I10 */
         pr_default.execute(8, new Object[] {A761ContagemResultadoChckLst_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002I10_A761ContagemResultadoChckLst_Codigo[0] < A761ContagemResultadoChckLst_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002I10_A761ContagemResultadoChckLst_Codigo[0] > A761ContagemResultadoChckLst_Codigo ) ) )
            {
               A761ContagemResultadoChckLst_Codigo = T002I10_A761ContagemResultadoChckLst_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
               RcdFound206 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound206 = 0;
         /* Using cursor T002I11 */
         pr_default.execute(9, new Object[] {A761ContagemResultadoChckLst_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002I11_A761ContagemResultadoChckLst_Codigo[0] > A761ContagemResultadoChckLst_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002I11_A761ContagemResultadoChckLst_Codigo[0] < A761ContagemResultadoChckLst_Codigo ) ) )
            {
               A761ContagemResultadoChckLst_Codigo = T002I11_A761ContagemResultadoChckLst_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
               RcdFound206 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2I206( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2I206( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound206 == 1 )
            {
               if ( A761ContagemResultadoChckLst_Codigo != Z761ContagemResultadoChckLst_Codigo )
               {
                  A761ContagemResultadoChckLst_Codigo = Z761ContagemResultadoChckLst_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLST_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2I206( ) ;
                  GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A761ContagemResultadoChckLst_Codigo != Z761ContagemResultadoChckLst_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2I206( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOCHCKLST_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2I206( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A761ContagemResultadoChckLst_Codigo != Z761ContagemResultadoChckLst_Codigo )
         {
            A761ContagemResultadoChckLst_Codigo = Z761ContagemResultadoChckLst_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOCHCKLST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound206 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLst_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2I206( ) ;
         if ( RcdFound206 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2I206( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound206 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound206 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2I206( ) ;
         if ( RcdFound206 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound206 != 0 )
            {
               ScanNext2I206( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2I206( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2I206( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002I2 */
            pr_default.execute(0, new Object[] {A761ContagemResultadoChckLst_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLst"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z762ContagemResultadoChckLst_Cumpre, T002I2_A762ContagemResultadoChckLst_Cumpre[0]) != 0 ) || ( Z758CheckList_Codigo != T002I2_A758CheckList_Codigo[0] ) || ( Z1868ContagemResultadoChckLst_OSCod != T002I2_A1868ContagemResultadoChckLst_OSCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z762ContagemResultadoChckLst_Cumpre, T002I2_A762ContagemResultadoChckLst_Cumpre[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadochcklst:[seudo value changed for attri]"+"ContagemResultadoChckLst_Cumpre");
                  GXUtil.WriteLogRaw("Old: ",Z762ContagemResultadoChckLst_Cumpre);
                  GXUtil.WriteLogRaw("Current: ",T002I2_A762ContagemResultadoChckLst_Cumpre[0]);
               }
               if ( Z758CheckList_Codigo != T002I2_A758CheckList_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklst:[seudo value changed for attri]"+"CheckList_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z758CheckList_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002I2_A758CheckList_Codigo[0]);
               }
               if ( Z1868ContagemResultadoChckLst_OSCod != T002I2_A1868ContagemResultadoChckLst_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklst:[seudo value changed for attri]"+"ContagemResultadoChckLst_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z1868ContagemResultadoChckLst_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T002I2_A1868ContagemResultadoChckLst_OSCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoChckLst"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2I206( )
      {
         BeforeValidate2I206( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2I206( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2I206( 0) ;
            CheckOptimisticConcurrency2I206( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2I206( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2I206( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002I12 */
                     pr_default.execute(10, new Object[] {A762ContagemResultadoChckLst_Cumpre, A758CheckList_Codigo, A1868ContagemResultadoChckLst_OSCod});
                     A761ContagemResultadoChckLst_Codigo = T002I12_A761ContagemResultadoChckLst_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLst") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2I0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2I206( ) ;
            }
            EndLevel2I206( ) ;
         }
         CloseExtendedTableCursors2I206( ) ;
      }

      protected void Update2I206( )
      {
         BeforeValidate2I206( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2I206( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2I206( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2I206( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2I206( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002I13 */
                     pr_default.execute(11, new Object[] {A762ContagemResultadoChckLst_Cumpre, A758CheckList_Codigo, A1868ContagemResultadoChckLst_OSCod, A761ContagemResultadoChckLst_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLst") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLst"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2I206( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2I0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2I206( ) ;
         }
         CloseExtendedTableCursors2I206( ) ;
      }

      protected void DeferredUpdate2I206( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2I206( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2I206( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2I206( ) ;
            AfterConfirm2I206( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2I206( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002I14 */
                  pr_default.execute(12, new Object[] {A761ContagemResultadoChckLst_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLst") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound206 == 0 )
                        {
                           InitAll2I206( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2I0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode206 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2I206( ) ;
         Gx_mode = sMode206;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2I206( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2I206( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2I206( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContagemResultadoChckLst");
            if ( AnyError == 0 )
            {
               ConfirmValues2I0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContagemResultadoChckLst");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2I206( )
      {
         /* Using cursor T002I15 */
         pr_default.execute(13);
         RcdFound206 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound206 = 1;
            A761ContagemResultadoChckLst_Codigo = T002I15_A761ContagemResultadoChckLst_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2I206( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound206 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound206 = 1;
            A761ContagemResultadoChckLst_Codigo = T002I15_A761ContagemResultadoChckLst_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
         }
      }

      protected void ScanEnd2I206( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm2I206( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2I206( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2I206( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2I206( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2I206( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2I206( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2I206( )
      {
         edtContagemResultadoChckLst_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLst_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLst_Codigo_Enabled), 5, 0)));
         edtContagemResultadoChckLst_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLst_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLst_OSCod_Enabled), 5, 0)));
         edtCheckList_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheckList_Codigo_Enabled), 5, 0)));
         cmbContagemResultadoChckLst_Cumpre.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoChckLst_Cumpre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoChckLst_Cumpre.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2I0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122195045");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadochcklst.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z761ContagemResultadoChckLst_Codigo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z762ContagemResultadoChckLst_Cumpre", StringUtil.RTrim( Z762ContagemResultadoChckLst_Cumpre));
         GxWebStd.gx_hidden_field( context, "Z758CheckList_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z758CheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1868ContagemResultadoChckLst_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadochcklst.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoChckLst" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Chck Lst" ;
      }

      protected void InitializeNonKey2I206( )
      {
         A1868ContagemResultadoChckLst_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
         A758CheckList_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         A762ContagemResultadoChckLst_Cumpre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
         Z762ContagemResultadoChckLst_Cumpre = "";
         Z758CheckList_Codigo = 0;
         Z1868ContagemResultadoChckLst_OSCod = 0;
      }

      protected void InitAll2I206( )
      {
         A761ContagemResultadoChckLst_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A761ContagemResultadoChckLst_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A761ContagemResultadoChckLst_Codigo), 4, 0)));
         InitializeNonKey2I206( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122195049");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadochcklst.js", "?20203122195049");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadochcklst_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLST_CODIGO";
         edtContagemResultadoChckLst_Codigo_Internalname = "CONTAGEMRESULTADOCHCKLST_CODIGO";
         lblTextblockcontagemresultadochcklst_oscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLST_OSCOD";
         edtContagemResultadoChckLst_OSCod_Internalname = "CONTAGEMRESULTADOCHCKLST_OSCOD";
         lblTextblockchecklist_codigo_Internalname = "TEXTBLOCKCHECKLIST_CODIGO";
         edtCheckList_Codigo_Internalname = "CHECKLIST_CODIGO";
         lblTextblockcontagemresultadochcklst_cumpre_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLST_CUMPRE";
         cmbContagemResultadoChckLst_Cumpre_Internalname = "CONTAGEMRESULTADOCHCKLST_CUMPRE";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Chck Lst";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         cmbContagemResultadoChckLst_Cumpre_Jsonclick = "";
         cmbContagemResultadoChckLst_Cumpre.Enabled = 1;
         edtCheckList_Codigo_Jsonclick = "";
         edtCheckList_Codigo_Enabled = 1;
         edtContagemResultadoChckLst_OSCod_Jsonclick = "";
         edtContagemResultadoChckLst_OSCod_Enabled = 1;
         edtContagemResultadoChckLst_Codigo_Jsonclick = "";
         edtContagemResultadoChckLst_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadochcklst_codigo( short GX_Parm1 ,
                                                         GXCombobox cmbGX_Parm2 ,
                                                         int GX_Parm3 ,
                                                         int GX_Parm4 )
      {
         A761ContagemResultadoChckLst_Codigo = GX_Parm1;
         cmbContagemResultadoChckLst_Cumpre = cmbGX_Parm2;
         A762ContagemResultadoChckLst_Cumpre = cmbContagemResultadoChckLst_Cumpre.CurrentValue;
         cmbContagemResultadoChckLst_Cumpre.CurrentValue = A762ContagemResultadoChckLst_Cumpre;
         A758CheckList_Codigo = GX_Parm3;
         A1868ContagemResultadoChckLst_OSCod = GX_Parm4;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ".", "")));
         cmbContagemResultadoChckLst_Cumpre.CurrentValue = A762ContagemResultadoChckLst_Cumpre;
         isValidOutput.Add(cmbContagemResultadoChckLst_Cumpre);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z761ContagemResultadoChckLst_Codigo), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1868ContagemResultadoChckLst_OSCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z758CheckList_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z762ContagemResultadoChckLst_Cumpre));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadochcklst_oscod( int GX_Parm1 )
      {
         A1868ContagemResultadoChckLst_OSCod = GX_Parm1;
         /* Using cursor T002I16 */
         pr_default.execute(14, new Object[] {A1868ContagemResultadoChckLst_OSCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLST_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLst_OSCod_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Checklist_codigo( int GX_Parm1 )
      {
         A758CheckList_Codigo = GX_Parm1;
         /* Using cursor T002I17 */
         pr_default.execute(15, new Object[] {A758CheckList_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Check List'.", "ForeignKeyNotFound", 1, "CHECKLIST_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheckList_Codigo_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z762ContagemResultadoChckLst_Cumpre = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A762ContagemResultadoChckLst_Cumpre = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadochcklst_codigo_Jsonclick = "";
         lblTextblockcontagemresultadochcklst_oscod_Jsonclick = "";
         lblTextblockchecklist_codigo_Jsonclick = "";
         lblTextblockcontagemresultadochcklst_cumpre_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T002I6_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I6_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         T002I6_A758CheckList_Codigo = new int[1] ;
         T002I6_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         T002I5_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         T002I4_A758CheckList_Codigo = new int[1] ;
         T002I7_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         T002I8_A758CheckList_Codigo = new int[1] ;
         T002I9_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I3_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I3_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         T002I3_A758CheckList_Codigo = new int[1] ;
         T002I3_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         sMode206 = "";
         T002I10_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I11_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I2_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I2_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         T002I2_A758CheckList_Codigo = new int[1] ;
         T002I2_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         T002I12_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T002I15_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T002I16_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         T002I17_A758CheckList_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadochcklst__default(),
            new Object[][] {
                new Object[] {
               T002I2_A761ContagemResultadoChckLst_Codigo, T002I2_A762ContagemResultadoChckLst_Cumpre, T002I2_A758CheckList_Codigo, T002I2_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I3_A761ContagemResultadoChckLst_Codigo, T002I3_A762ContagemResultadoChckLst_Cumpre, T002I3_A758CheckList_Codigo, T002I3_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I4_A758CheckList_Codigo
               }
               , new Object[] {
               T002I5_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I6_A761ContagemResultadoChckLst_Codigo, T002I6_A762ContagemResultadoChckLst_Cumpre, T002I6_A758CheckList_Codigo, T002I6_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I7_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I8_A758CheckList_Codigo
               }
               , new Object[] {
               T002I9_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T002I10_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T002I11_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T002I12_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002I15_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T002I16_A1868ContagemResultadoChckLst_OSCod
               }
               , new Object[] {
               T002I17_A758CheckList_Codigo
               }
            }
         );
      }

      private short Z761ContagemResultadoChckLst_Codigo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A761ContagemResultadoChckLst_Codigo ;
      private short GX_JID ;
      private short RcdFound206 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z758CheckList_Codigo ;
      private int Z1868ContagemResultadoChckLst_OSCod ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int A758CheckList_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemResultadoChckLst_Codigo_Enabled ;
      private int edtContagemResultadoChckLst_OSCod_Enabled ;
      private int edtCheckList_Codigo_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String Z762ContagemResultadoChckLst_Cumpre ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoChckLst_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadochcklst_codigo_Internalname ;
      private String lblTextblockcontagemresultadochcklst_codigo_Jsonclick ;
      private String edtContagemResultadoChckLst_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadochcklst_oscod_Internalname ;
      private String lblTextblockcontagemresultadochcklst_oscod_Jsonclick ;
      private String edtContagemResultadoChckLst_OSCod_Internalname ;
      private String edtContagemResultadoChckLst_OSCod_Jsonclick ;
      private String lblTextblockchecklist_codigo_Internalname ;
      private String lblTextblockchecklist_codigo_Jsonclick ;
      private String edtCheckList_Codigo_Internalname ;
      private String edtCheckList_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadochcklst_cumpre_Internalname ;
      private String lblTextblockcontagemresultadochcklst_cumpre_Jsonclick ;
      private String cmbContagemResultadoChckLst_Cumpre_Internalname ;
      private String cmbContagemResultadoChckLst_Cumpre_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode206 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultadoChckLst_Cumpre ;
      private IDataStoreProvider pr_default ;
      private short[] T002I6_A761ContagemResultadoChckLst_Codigo ;
      private String[] T002I6_A762ContagemResultadoChckLst_Cumpre ;
      private int[] T002I6_A758CheckList_Codigo ;
      private int[] T002I6_A1868ContagemResultadoChckLst_OSCod ;
      private int[] T002I5_A1868ContagemResultadoChckLst_OSCod ;
      private int[] T002I4_A758CheckList_Codigo ;
      private int[] T002I7_A1868ContagemResultadoChckLst_OSCod ;
      private int[] T002I8_A758CheckList_Codigo ;
      private short[] T002I9_A761ContagemResultadoChckLst_Codigo ;
      private short[] T002I3_A761ContagemResultadoChckLst_Codigo ;
      private String[] T002I3_A762ContagemResultadoChckLst_Cumpre ;
      private int[] T002I3_A758CheckList_Codigo ;
      private int[] T002I3_A1868ContagemResultadoChckLst_OSCod ;
      private short[] T002I10_A761ContagemResultadoChckLst_Codigo ;
      private short[] T002I11_A761ContagemResultadoChckLst_Codigo ;
      private short[] T002I2_A761ContagemResultadoChckLst_Codigo ;
      private String[] T002I2_A762ContagemResultadoChckLst_Cumpre ;
      private int[] T002I2_A758CheckList_Codigo ;
      private int[] T002I2_A1868ContagemResultadoChckLst_OSCod ;
      private short[] T002I12_A761ContagemResultadoChckLst_Codigo ;
      private short[] T002I15_A761ContagemResultadoChckLst_Codigo ;
      private int[] T002I16_A1868ContagemResultadoChckLst_OSCod ;
      private int[] T002I17_A758CheckList_Codigo ;
      private GXWebForm Form ;
   }

   public class contagemresultadochcklst__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002I6 ;
          prmT002I6 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I5 ;
          prmT002I5 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I4 ;
          prmT002I4 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I7 ;
          prmT002I7 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I8 ;
          prmT002I8 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I9 ;
          prmT002I9 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I3 ;
          prmT002I3 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I10 ;
          prmT002I10 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I11 ;
          prmT002I11 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I2 ;
          prmT002I2 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I12 ;
          prmT002I12 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Cumpre",SqlDbType.Char,1,0} ,
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLst_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I13 ;
          prmT002I13 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Cumpre",SqlDbType.Char,1,0} ,
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLst_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I14 ;
          prmT002I14 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002I15 ;
          prmT002I15 = new Object[] {
          } ;
          Object[] prmT002I16 ;
          prmT002I16 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002I17 ;
          prmT002I17 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002I2", "SELECT [ContagemResultadoChckLst_Codigo], [ContagemResultadoChckLst_Cumpre], [CheckList_Codigo], [ContagemResultadoChckLst_OSCod] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultadoChckLst] WITH (UPDLOCK) WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I2,1,0,true,false )
             ,new CursorDef("T002I3", "SELECT [ContagemResultadoChckLst_Codigo], [ContagemResultadoChckLst_Cumpre], [CheckList_Codigo], [ContagemResultadoChckLst_OSCod] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I3,1,0,true,false )
             ,new CursorDef("T002I4", "SELECT [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I4,1,0,true,false )
             ,new CursorDef("T002I5", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLst_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I5,1,0,true,false )
             ,new CursorDef("T002I6", "SELECT TM1.[ContagemResultadoChckLst_Codigo], TM1.[ContagemResultadoChckLst_Cumpre], TM1.[CheckList_Codigo], TM1.[ContagemResultadoChckLst_OSCod] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultadoChckLst] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo ORDER BY TM1.[ContagemResultadoChckLst_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002I6,100,0,true,false )
             ,new CursorDef("T002I7", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLst_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I7,1,0,true,false )
             ,new CursorDef("T002I8", "SELECT [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I8,1,0,true,false )
             ,new CursorDef("T002I9", "SELECT [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002I9,1,0,true,false )
             ,new CursorDef("T002I10", "SELECT TOP 1 [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE ( [ContagemResultadoChckLst_Codigo] > @ContagemResultadoChckLst_Codigo) ORDER BY [ContagemResultadoChckLst_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002I10,1,0,true,true )
             ,new CursorDef("T002I11", "SELECT TOP 1 [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE ( [ContagemResultadoChckLst_Codigo] < @ContagemResultadoChckLst_Codigo) ORDER BY [ContagemResultadoChckLst_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002I11,1,0,true,true )
             ,new CursorDef("T002I12", "INSERT INTO [ContagemResultadoChckLst]([ContagemResultadoChckLst_Cumpre], [CheckList_Codigo], [ContagemResultadoChckLst_OSCod]) VALUES(@ContagemResultadoChckLst_Cumpre, @CheckList_Codigo, @ContagemResultadoChckLst_OSCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002I12)
             ,new CursorDef("T002I13", "UPDATE [ContagemResultadoChckLst] SET [ContagemResultadoChckLst_Cumpre]=@ContagemResultadoChckLst_Cumpre, [CheckList_Codigo]=@CheckList_Codigo, [ContagemResultadoChckLst_OSCod]=@ContagemResultadoChckLst_OSCod  WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo", GxErrorMask.GX_NOMASK,prmT002I13)
             ,new CursorDef("T002I14", "DELETE FROM [ContagemResultadoChckLst]  WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo", GxErrorMask.GX_NOMASK,prmT002I14)
             ,new CursorDef("T002I15", "SELECT [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) ORDER BY [ContagemResultadoChckLst_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002I15,100,0,true,false )
             ,new CursorDef("T002I16", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLst_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLst_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I16,1,0,true,false )
             ,new CursorDef("T002I17", "SELECT [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002I17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
