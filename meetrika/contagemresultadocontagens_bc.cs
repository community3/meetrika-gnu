/*
               File: ContagemResultadoContagens_BC
        Description: Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:20:22.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadocontagens_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadocontagens_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadocontagens_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1V72( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1V72( ) ;
         standaloneModal( ) ;
         AddRow1V72( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E111V2 */
            E111V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
               Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1V0( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1V72( ) ;
            }
            else
            {
               CheckExtendedTable1V72( ) ;
               if ( AnyError == 0 )
               {
                  ZM1V72( 20) ;
                  ZM1V72( 21) ;
                  ZM1V72( 22) ;
                  ZM1V72( 23) ;
                  ZM1V72( 24) ;
                  ZM1V72( 25) ;
                  ZM1V72( 26) ;
                  ZM1V72( 27) ;
               }
               CloseExtendedTableCursors1V72( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E121V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV15WWPContext) ;
         AV12TrnContext.FromXml(AV14WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV12TrnContext.gxTpr_Transactionname, AV37Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV38GXV1 = 1;
            while ( AV38GXV1 <= AV12TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV12TrnContext.gxTpr_Attributes.Item(AV38GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContadorFMCod") == 0 )
               {
                  AV10Insert_ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_NaoCnfCntCod") == 0 )
               {
                  AV11Insert_ContagemResultado_NaoCnfCntCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV38GXV1 = (int)(AV38GXV1+1);
            }
         }
         AV35CalculoPFinal = "";
         AV19IndiceDivergencia = 0;
         AV27ContagemResultado_Divergencia = 0;
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            new prc_getparmindicedivergenciadaos(context ).execute(  AV7ContagemResultado_Codigo, out  AV19IndiceDivergencia, out  AV20CalculoDivergencia, out  AV29ContagemResultado_ValorPF) ;
            /* Execute user subroutine: 'CALCULADIVERGENCIA' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E111V2( )
      {
         /* After Trn Routine */
      }

      protected void E131V2( )
      {
         /* ContagemResultado_PFBFS_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E141V2( )
      {
         /* ContagemResultado_PFLFS_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E151V2( )
      {
         /* ContagemResultado_PFBFM_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E161V2( )
      {
         /* ContagemResultado_PFLFM_Isvalid Routine */
         /* Execute user subroutine: 'CALCULADIVERGENCIA' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         GXt_decimal1 = AV27ContagemResultado_Divergencia;
         new prc_calculardivergencia(context ).execute(  AV20CalculoDivergencia,  A458ContagemResultado_PFBFS,  A460ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
         AV27ContagemResultado_Divergencia = GXt_decimal1;
      }

      protected void ZM1V72( short GX_JID )
      {
         if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
         {
            Z482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            Z462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            Z483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            Z481ContagemResultado_TimeCnt = A481ContagemResultado_TimeCnt;
            Z901ContagemResultadoContagens_Prazo = A901ContagemResultadoContagens_Prazo;
            Z458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            Z459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            Z460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            Z461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            Z517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            Z800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            Z833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            Z1756ContagemResultado_NvlCnt = A1756ContagemResultado_NvlCnt;
            Z470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            Z469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z1389ContagemResultado_RdmnIssueId = A1389ContagemResultado_RdmnIssueId;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
            Z999ContagemResultado_CrFMEhContratada = A999ContagemResultado_CrFMEhContratada;
            Z1000ContagemResultado_CrFMEhContratante = A1000ContagemResultado_CrFMEhContratante;
            Z479ContagemResultado_CrFMPessoaCod = A479ContagemResultado_CrFMPessoaCod;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
            Z478ContagemResultado_NaoCnfCntNom = A478ContagemResultado_NaoCnfCntNom;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
         {
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 26 ) || ( GX_JID == 0 ) )
         {
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( ( GX_JID == 27 ) || ( GX_JID == 0 ) )
         {
            Z474ContagemResultado_ContadorFMNom = A474ContagemResultado_ContadorFMNom;
            Z501ContagemResultado_OsFsOsFm = A501ContagemResultado_OsFsOsFm;
         }
         if ( GX_JID == -19 )
         {
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            Z482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            Z462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            Z483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            Z481ContagemResultado_TimeCnt = A481ContagemResultado_TimeCnt;
            Z901ContagemResultadoContagens_Prazo = A901ContagemResultadoContagens_Prazo;
            Z458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            Z459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            Z460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            Z461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            Z463ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
            Z517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            Z800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            Z833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            Z852ContagemResultado_Planilha = A852ContagemResultado_Planilha;
            Z1756ContagemResultado_NvlCnt = A1756ContagemResultado_NvlCnt;
            Z854ContagemResultado_TipoPla = A854ContagemResultado_TipoPla;
            Z853ContagemResultado_NomePla = A853ContagemResultado_NomePla;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            Z469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z1389ContagemResultado_RdmnIssueId = A1389ContagemResultado_RdmnIssueId;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z513Sistema_Coordenacao = A513Sistema_Coordenacao;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z601ContagemResultado_Servico = A601ContagemResultado_Servico;
            Z999ContagemResultado_CrFMEhContratada = A999ContagemResultado_CrFMEhContratada;
            Z1000ContagemResultado_CrFMEhContratante = A1000ContagemResultado_CrFMEhContratante;
            Z479ContagemResultado_CrFMPessoaCod = A479ContagemResultado_CrFMPessoaCod;
            Z474ContagemResultado_ContadorFMNom = A474ContagemResultado_ContadorFMNom;
            Z478ContagemResultado_NaoCnfCntNom = A478ContagemResultado_NaoCnfCntNom;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV37Pgmname = "ContagemResultadoContagens_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A482ContagemResultadoContagens_Esforco) && ( Gx_BScreen == 0 ) )
         {
            A482ContagemResultadoContagens_Esforco = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A517ContagemResultado_Ultima) && ( Gx_BScreen == 0 ) )
         {
            A517ContagemResultado_Ultima = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A511ContagemResultado_HoraCnt)) && ( Gx_BScreen == 0 ) )
         {
            A511ContagemResultado_HoraCnt = context.localUtil.Time( );
         }
      }

      protected void Load1V72( )
      {
         /* Using cursor BC001V12 */
         pr_default.execute(10, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound72 = 1;
            A146Modulo_Codigo = BC001V12_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V12_n146Modulo_Codigo[0];
            A127Sistema_Codigo = BC001V12_A127Sistema_Codigo[0];
            A482ContagemResultadoContagens_Esforco = BC001V12_A482ContagemResultadoContagens_Esforco[0];
            A462ContagemResultado_Divergencia = BC001V12_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = BC001V12_A483ContagemResultado_StatusCnt[0];
            A485ContagemResultado_EhValidacao = BC001V12_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V12_n485ContagemResultado_EhValidacao[0];
            A513Sistema_Coordenacao = BC001V12_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V12_n513Sistema_Coordenacao[0];
            A457ContagemResultado_Demanda = BC001V12_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V12_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V12_n484ContagemResultado_StatusDmn[0];
            A481ContagemResultado_TimeCnt = BC001V12_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = BC001V12_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = BC001V12_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = BC001V12_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = BC001V12_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = BC001V12_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = BC001V12_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = BC001V12_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = BC001V12_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = BC001V12_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = BC001V12_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = BC001V12_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = BC001V12_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = BC001V12_n463ContagemResultado_ParecerTcn[0];
            A474ContagemResultado_ContadorFMNom = BC001V12_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = BC001V12_n474ContagemResultado_ContadorFMNom[0];
            A999ContagemResultado_CrFMEhContratada = BC001V12_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = BC001V12_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = BC001V12_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = BC001V12_n1000ContagemResultado_CrFMEhContratante[0];
            A478ContagemResultado_NaoCnfCntNom = BC001V12_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = BC001V12_n478ContagemResultado_NaoCnfCntNom[0];
            A517ContagemResultado_Ultima = BC001V12_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = BC001V12_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = BC001V12_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = BC001V12_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = BC001V12_n833ContagemResultado_CstUntPrd[0];
            A1389ContagemResultado_RdmnIssueId = BC001V12_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V12_n1389ContagemResultado_RdmnIssueId[0];
            A1756ContagemResultado_NvlCnt = BC001V12_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = BC001V12_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = BC001V12_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = BC001V12_n854ContagemResultado_TipoPla[0];
            A852ContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            A853ContagemResultado_NomePla = BC001V12_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = BC001V12_n853ContagemResultado_NomePla[0];
            A852ContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A493ContagemResultado_DemandaFM = BC001V12_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V12_n493ContagemResultado_DemandaFM[0];
            A470ContagemResultado_ContadorFMCod = BC001V12_A470ContagemResultado_ContadorFMCod[0];
            A469ContagemResultado_NaoCnfCntCod = BC001V12_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = BC001V12_n469ContagemResultado_NaoCnfCntCod[0];
            A890ContagemResultado_Responsavel = BC001V12_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V12_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V12_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V12_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V12_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V12_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = BC001V12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V12_n52Contratada_AreaTrabalhoCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V12_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V12_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V12_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V12_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = BC001V12_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V12_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = BC001V12_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V12_n602ContagemResultado_OSVinculada[0];
            A479ContagemResultado_CrFMPessoaCod = BC001V12_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = BC001V12_n479ContagemResultado_CrFMPessoaCod[0];
            A852ContagemResultado_Planilha = BC001V12_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = BC001V12_n852ContagemResultado_Planilha[0];
            ZM1V72( -19) ;
         }
         pr_default.close(10);
         OnLoadActions1V72( ) ;
      }

      protected void OnLoadActions1V72( )
      {
         if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
         {
            AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
         }
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         AV35CalculoPFinal = GXt_char2;
      }

      protected void CheckExtendedTable1V72( )
      {
         standaloneModal( ) ;
         if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
         {
            AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
         }
         /* Using cursor BC001V4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
         }
         A146Modulo_Codigo = BC001V4_A146Modulo_Codigo[0];
         n146Modulo_Codigo = BC001V4_n146Modulo_Codigo[0];
         A485ContagemResultado_EhValidacao = BC001V4_A485ContagemResultado_EhValidacao[0];
         n485ContagemResultado_EhValidacao = BC001V4_n485ContagemResultado_EhValidacao[0];
         A457ContagemResultado_Demanda = BC001V4_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = BC001V4_n457ContagemResultado_Demanda[0];
         A484ContagemResultado_StatusDmn = BC001V4_A484ContagemResultado_StatusDmn[0];
         n484ContagemResultado_StatusDmn = BC001V4_n484ContagemResultado_StatusDmn[0];
         A1389ContagemResultado_RdmnIssueId = BC001V4_A1389ContagemResultado_RdmnIssueId[0];
         n1389ContagemResultado_RdmnIssueId = BC001V4_n1389ContagemResultado_RdmnIssueId[0];
         A493ContagemResultado_DemandaFM = BC001V4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = BC001V4_n493ContagemResultado_DemandaFM[0];
         A890ContagemResultado_Responsavel = BC001V4_A890ContagemResultado_Responsavel[0];
         n890ContagemResultado_Responsavel = BC001V4_n890ContagemResultado_Responsavel[0];
         A468ContagemResultado_NaoCnfDmnCod = BC001V4_A468ContagemResultado_NaoCnfDmnCod[0];
         n468ContagemResultado_NaoCnfDmnCod = BC001V4_n468ContagemResultado_NaoCnfDmnCod[0];
         A490ContagemResultado_ContratadaCod = BC001V4_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = BC001V4_n490ContagemResultado_ContratadaCod[0];
         A805ContagemResultado_ContratadaOrigemCod = BC001V4_A805ContagemResultado_ContratadaOrigemCod[0];
         n805ContagemResultado_ContratadaOrigemCod = BC001V4_n805ContagemResultado_ContratadaOrigemCod[0];
         A1553ContagemResultado_CntSrvCod = BC001V4_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = BC001V4_n1553ContagemResultado_CntSrvCod[0];
         A602ContagemResultado_OSVinculada = BC001V4_A602ContagemResultado_OSVinculada[0];
         n602ContagemResultado_OSVinculada = BC001V4_n602ContagemResultado_OSVinculada[0];
         pr_default.close(2);
         /* Using cursor BC001V7 */
         pr_default.execute(5, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A127Sistema_Codigo = BC001V7_A127Sistema_Codigo[0];
         pr_default.close(5);
         /* Using cursor BC001V8 */
         pr_default.execute(6, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A127Sistema_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A513Sistema_Coordenacao = BC001V8_A513Sistema_Coordenacao[0];
         n513Sistema_Coordenacao = BC001V8_n513Sistema_Coordenacao[0];
         pr_default.close(6);
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         /* Using cursor BC001V9 */
         pr_default.execute(7, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = BC001V9_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = BC001V9_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(7);
         /* Using cursor BC001V10 */
         pr_default.execute(8, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A601ContagemResultado_Servico = BC001V10_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = BC001V10_n601ContagemResultado_Servico[0];
         pr_default.close(8);
         GXt_char2 = AV35CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
         AV35CalculoPFinal = GXt_char2;
         if ( ! ( (DateTime.MinValue==A473ContagemResultado_DataCnt) || ( A473ContagemResultado_DataCnt >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A901ContagemResultadoContagens_Prazo) || ( A901ContagemResultadoContagens_Prazo >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Prazo fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC001V5 */
         pr_default.execute(3, new Object[] {A470ContagemResultado_ContadorFMCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de M�t.'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFMCOD");
            AnyError = 1;
         }
         A999ContagemResultado_CrFMEhContratada = BC001V5_A999ContagemResultado_CrFMEhContratada[0];
         n999ContagemResultado_CrFMEhContratada = BC001V5_n999ContagemResultado_CrFMEhContratada[0];
         A1000ContagemResultado_CrFMEhContratante = BC001V5_A1000ContagemResultado_CrFMEhContratante[0];
         n1000ContagemResultado_CrFMEhContratante = BC001V5_n1000ContagemResultado_CrFMEhContratante[0];
         A479ContagemResultado_CrFMPessoaCod = BC001V5_A479ContagemResultado_CrFMPessoaCod[0];
         n479ContagemResultado_CrFMPessoaCod = BC001V5_n479ContagemResultado_CrFMPessoaCod[0];
         pr_default.close(3);
         /* Using cursor BC001V11 */
         pr_default.execute(9, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A474ContagemResultado_ContadorFMNom = BC001V11_A474ContagemResultado_ContadorFMNom[0];
         n474ContagemResultado_ContadorFMNom = BC001V11_n474ContagemResultado_ContadorFMNom[0];
         pr_default.close(9);
         /* Using cursor BC001V6 */
         pr_default.execute(4, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A469ContagemResultado_NaoCnfCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Nao Conformidade da Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFCNTCOD");
               AnyError = 1;
            }
         }
         A478ContagemResultado_NaoCnfCntNom = BC001V6_A478ContagemResultado_NaoCnfCntNom[0];
         n478ContagemResultado_NaoCnfCntNom = BC001V6_n478ContagemResultado_NaoCnfCntNom[0];
         pr_default.close(4);
         if ( ! ( ( A483ContagemResultado_StatusCnt == 1 ) || ( A483ContagemResultado_StatusCnt == 2 ) || ( A483ContagemResultado_StatusCnt == 3 ) || ( A483ContagemResultado_StatusCnt == 4 ) || ( A483ContagemResultado_StatusCnt == 5 ) || ( A483ContagemResultado_StatusCnt == 6 ) || ( A483ContagemResultado_StatusCnt == 7 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1756ContagemResultado_NvlCnt == 0 ) || ( A1756ContagemResultado_NvlCnt == 1 ) || ( A1756ContagemResultado_NvlCnt == 2 ) || (0==A1756ContagemResultado_NvlCnt) ) )
         {
            GX_msglist.addItem("Campo Nivel fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors1V72( )
      {
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(3);
         pr_default.close(9);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1V72( )
      {
         /* Using cursor BC001V13 */
         pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound72 = 1;
         }
         else
         {
            RcdFound72 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001V3 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1V72( 19) ;
            RcdFound72 = 1;
            A473ContagemResultado_DataCnt = BC001V3_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = BC001V3_A511ContagemResultado_HoraCnt[0];
            A482ContagemResultadoContagens_Esforco = BC001V3_A482ContagemResultadoContagens_Esforco[0];
            A462ContagemResultado_Divergencia = BC001V3_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = BC001V3_A483ContagemResultado_StatusCnt[0];
            A481ContagemResultado_TimeCnt = BC001V3_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = BC001V3_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = BC001V3_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = BC001V3_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = BC001V3_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = BC001V3_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = BC001V3_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = BC001V3_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = BC001V3_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = BC001V3_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = BC001V3_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = BC001V3_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = BC001V3_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = BC001V3_n463ContagemResultado_ParecerTcn[0];
            A517ContagemResultado_Ultima = BC001V3_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = BC001V3_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = BC001V3_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = BC001V3_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = BC001V3_n833ContagemResultado_CstUntPrd[0];
            A1756ContagemResultado_NvlCnt = BC001V3_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = BC001V3_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = BC001V3_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = BC001V3_n854ContagemResultado_TipoPla[0];
            A852ContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            A853ContagemResultado_NomePla = BC001V3_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = BC001V3_n853ContagemResultado_NomePla[0];
            A852ContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A456ContagemResultado_Codigo = BC001V3_A456ContagemResultado_Codigo[0];
            A470ContagemResultado_ContadorFMCod = BC001V3_A470ContagemResultado_ContadorFMCod[0];
            A469ContagemResultado_NaoCnfCntCod = BC001V3_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = BC001V3_n469ContagemResultado_NaoCnfCntCod[0];
            A852ContagemResultado_Planilha = BC001V3_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = BC001V3_n852ContagemResultado_Planilha[0];
            O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            sMode72 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1V72( ) ;
            if ( AnyError == 1 )
            {
               RcdFound72 = 0;
               InitializeNonKey1V72( ) ;
            }
            Gx_mode = sMode72;
         }
         else
         {
            RcdFound72 = 0;
            InitializeNonKey1V72( ) ;
            sMode72 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode72;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1V72( ) ;
         if ( RcdFound72 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1V0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1V72( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001V2 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoContagens"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z482ContagemResultadoContagens_Esforco != BC001V2_A482ContagemResultadoContagens_Esforco[0] ) || ( Z462ContagemResultado_Divergencia != BC001V2_A462ContagemResultado_Divergencia[0] ) || ( Z483ContagemResultado_StatusCnt != BC001V2_A483ContagemResultado_StatusCnt[0] ) || ( Z481ContagemResultado_TimeCnt != BC001V2_A481ContagemResultado_TimeCnt[0] ) || ( Z901ContagemResultadoContagens_Prazo != BC001V2_A901ContagemResultadoContagens_Prazo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z458ContagemResultado_PFBFS != BC001V2_A458ContagemResultado_PFBFS[0] ) || ( Z459ContagemResultado_PFLFS != BC001V2_A459ContagemResultado_PFLFS[0] ) || ( Z460ContagemResultado_PFBFM != BC001V2_A460ContagemResultado_PFBFM[0] ) || ( Z461ContagemResultado_PFLFM != BC001V2_A461ContagemResultado_PFLFM[0] ) || ( Z517ContagemResultado_Ultima != BC001V2_A517ContagemResultado_Ultima[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z800ContagemResultado_Deflator != BC001V2_A800ContagemResultado_Deflator[0] ) || ( Z833ContagemResultado_CstUntPrd != BC001V2_A833ContagemResultado_CstUntPrd[0] ) || ( Z1756ContagemResultado_NvlCnt != BC001V2_A1756ContagemResultado_NvlCnt[0] ) || ( Z470ContagemResultado_ContadorFMCod != BC001V2_A470ContagemResultado_ContadorFMCod[0] ) || ( Z469ContagemResultado_NaoCnfCntCod != BC001V2_A469ContagemResultado_NaoCnfCntCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoContagens"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1V72( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1V72( 0) ;
            CheckOptimisticConcurrency1V72( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1V72( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1V72( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001V14 */
                     pr_default.execute(12, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A482ContagemResultadoContagens_Esforco, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, A456ContagemResultado_Codigo, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1V72( ) ;
            }
            EndLevel1V72( ) ;
         }
         CloseExtendedTableCursors1V72( ) ;
      }

      protected void Update1V72( )
      {
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1V72( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1V72( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1V72( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001V15 */
                     pr_default.execute(13, new Object[] {A482ContagemResultadoContagens_Esforco, A462ContagemResultado_Divergencia, A483ContagemResultado_StatusCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoContagens"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1V72( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1V72( ) ;
         }
         CloseExtendedTableCursors1V72( ) ;
      }

      protected void DeferredUpdate1V72( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC001V16 */
            pr_default.execute(14, new Object[] {n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1V72( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1V72( ) ;
            AfterConfirm1V72( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1V72( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001V17 */
                  pr_default.execute(15, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     new prc_contagemresultado_ultima(context ).execute( ref  A456ContagemResultado_Codigo) ;
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode72 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1V72( ) ;
         Gx_mode = sMode72;
      }

      protected void OnDeleteControls1V72( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001V18 */
            pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
            A146Modulo_Codigo = BC001V18_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V18_n146Modulo_Codigo[0];
            A485ContagemResultado_EhValidacao = BC001V18_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V18_n485ContagemResultado_EhValidacao[0];
            A457ContagemResultado_Demanda = BC001V18_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V18_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V18_n484ContagemResultado_StatusDmn[0];
            A1389ContagemResultado_RdmnIssueId = BC001V18_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V18_n1389ContagemResultado_RdmnIssueId[0];
            A493ContagemResultado_DemandaFM = BC001V18_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V18_n493ContagemResultado_DemandaFM[0];
            A890ContagemResultado_Responsavel = BC001V18_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V18_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V18_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V18_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V18_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V18_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V18_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V18_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V18_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC001V18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V18_n602ContagemResultado_OSVinculada[0];
            pr_default.close(16);
            /* Using cursor BC001V19 */
            pr_default.execute(17, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            A127Sistema_Codigo = BC001V19_A127Sistema_Codigo[0];
            pr_default.close(17);
            /* Using cursor BC001V20 */
            pr_default.execute(18, new Object[] {A127Sistema_Codigo});
            A513Sistema_Coordenacao = BC001V20_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V20_n513Sistema_Coordenacao[0];
            pr_default.close(18);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            /* Using cursor BC001V21 */
            pr_default.execute(19, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = BC001V21_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V21_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(19);
            /* Using cursor BC001V22 */
            pr_default.execute(20, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = BC001V22_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V22_n601ContagemResultado_Servico[0];
            pr_default.close(20);
            if ( (Convert.ToDecimal(0)==AV27ContagemResultado_Divergencia) )
            {
               AV27ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            }
            /* Using cursor BC001V23 */
            pr_default.execute(21, new Object[] {A470ContagemResultado_ContadorFMCod});
            A999ContagemResultado_CrFMEhContratada = BC001V23_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = BC001V23_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = BC001V23_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = BC001V23_n1000ContagemResultado_CrFMEhContratante[0];
            A479ContagemResultado_CrFMPessoaCod = BC001V23_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = BC001V23_n479ContagemResultado_CrFMPessoaCod[0];
            pr_default.close(21);
            /* Using cursor BC001V24 */
            pr_default.execute(22, new Object[] {n479ContagemResultado_CrFMPessoaCod, A479ContagemResultado_CrFMPessoaCod});
            A474ContagemResultado_ContadorFMNom = BC001V24_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = BC001V24_n474ContagemResultado_ContadorFMNom[0];
            pr_default.close(22);
            /* Using cursor BC001V25 */
            pr_default.execute(23, new Object[] {n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod});
            A478ContagemResultado_NaoCnfCntNom = BC001V25_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = BC001V25_n478ContagemResultado_NaoCnfCntNom[0];
            pr_default.close(23);
            GXt_char2 = AV35CalculoPFinal;
            new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A470ContagemResultado_ContadorFMCod, ref  A833ContagemResultado_CstUntPrd, out  GXt_char2) ;
            AV35CalculoPFinal = GXt_char2;
         }
      }

      protected void EndLevel1V72( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1V72( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1V72( )
      {
         /* Scan By routine */
         /* Using cursor BC001V26 */
         pr_default.execute(24, new Object[] {A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A456ContagemResultado_Codigo});
         RcdFound72 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound72 = 1;
            A146Modulo_Codigo = BC001V26_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V26_n146Modulo_Codigo[0];
            A127Sistema_Codigo = BC001V26_A127Sistema_Codigo[0];
            A473ContagemResultado_DataCnt = BC001V26_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = BC001V26_A511ContagemResultado_HoraCnt[0];
            A482ContagemResultadoContagens_Esforco = BC001V26_A482ContagemResultadoContagens_Esforco[0];
            A462ContagemResultado_Divergencia = BC001V26_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = BC001V26_A483ContagemResultado_StatusCnt[0];
            A485ContagemResultado_EhValidacao = BC001V26_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V26_n485ContagemResultado_EhValidacao[0];
            A513Sistema_Coordenacao = BC001V26_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V26_n513Sistema_Coordenacao[0];
            A457ContagemResultado_Demanda = BC001V26_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V26_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V26_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V26_n484ContagemResultado_StatusDmn[0];
            A481ContagemResultado_TimeCnt = BC001V26_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = BC001V26_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = BC001V26_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = BC001V26_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = BC001V26_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = BC001V26_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = BC001V26_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = BC001V26_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = BC001V26_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = BC001V26_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = BC001V26_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = BC001V26_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = BC001V26_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = BC001V26_n463ContagemResultado_ParecerTcn[0];
            A474ContagemResultado_ContadorFMNom = BC001V26_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = BC001V26_n474ContagemResultado_ContadorFMNom[0];
            A999ContagemResultado_CrFMEhContratada = BC001V26_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = BC001V26_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = BC001V26_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = BC001V26_n1000ContagemResultado_CrFMEhContratante[0];
            A478ContagemResultado_NaoCnfCntNom = BC001V26_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = BC001V26_n478ContagemResultado_NaoCnfCntNom[0];
            A517ContagemResultado_Ultima = BC001V26_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = BC001V26_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = BC001V26_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = BC001V26_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = BC001V26_n833ContagemResultado_CstUntPrd[0];
            A1389ContagemResultado_RdmnIssueId = BC001V26_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V26_n1389ContagemResultado_RdmnIssueId[0];
            A1756ContagemResultado_NvlCnt = BC001V26_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = BC001V26_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = BC001V26_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = BC001V26_n854ContagemResultado_TipoPla[0];
            A852ContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            A853ContagemResultado_NomePla = BC001V26_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = BC001V26_n853ContagemResultado_NomePla[0];
            A852ContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A493ContagemResultado_DemandaFM = BC001V26_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V26_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = BC001V26_A456ContagemResultado_Codigo[0];
            A470ContagemResultado_ContadorFMCod = BC001V26_A470ContagemResultado_ContadorFMCod[0];
            A469ContagemResultado_NaoCnfCntCod = BC001V26_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = BC001V26_n469ContagemResultado_NaoCnfCntCod[0];
            A890ContagemResultado_Responsavel = BC001V26_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V26_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V26_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V26_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V26_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V26_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = BC001V26_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V26_n52Contratada_AreaTrabalhoCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V26_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V26_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V26_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V26_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = BC001V26_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V26_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = BC001V26_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V26_n602ContagemResultado_OSVinculada[0];
            A479ContagemResultado_CrFMPessoaCod = BC001V26_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = BC001V26_n479ContagemResultado_CrFMPessoaCod[0];
            A852ContagemResultado_Planilha = BC001V26_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = BC001V26_n852ContagemResultado_Planilha[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1V72( )
      {
         /* Scan next routine */
         pr_default.readNext(24);
         RcdFound72 = 0;
         ScanKeyLoad1V72( ) ;
      }

      protected void ScanKeyLoad1V72( )
      {
         sMode72 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound72 = 1;
            A146Modulo_Codigo = BC001V26_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V26_n146Modulo_Codigo[0];
            A127Sistema_Codigo = BC001V26_A127Sistema_Codigo[0];
            A473ContagemResultado_DataCnt = BC001V26_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = BC001V26_A511ContagemResultado_HoraCnt[0];
            A482ContagemResultadoContagens_Esforco = BC001V26_A482ContagemResultadoContagens_Esforco[0];
            A462ContagemResultado_Divergencia = BC001V26_A462ContagemResultado_Divergencia[0];
            A483ContagemResultado_StatusCnt = BC001V26_A483ContagemResultado_StatusCnt[0];
            A485ContagemResultado_EhValidacao = BC001V26_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V26_n485ContagemResultado_EhValidacao[0];
            A513Sistema_Coordenacao = BC001V26_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V26_n513Sistema_Coordenacao[0];
            A457ContagemResultado_Demanda = BC001V26_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V26_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V26_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V26_n484ContagemResultado_StatusDmn[0];
            A481ContagemResultado_TimeCnt = BC001V26_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = BC001V26_n481ContagemResultado_TimeCnt[0];
            A901ContagemResultadoContagens_Prazo = BC001V26_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = BC001V26_n901ContagemResultadoContagens_Prazo[0];
            A458ContagemResultado_PFBFS = BC001V26_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = BC001V26_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = BC001V26_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = BC001V26_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = BC001V26_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = BC001V26_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = BC001V26_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = BC001V26_n461ContagemResultado_PFLFM[0];
            A463ContagemResultado_ParecerTcn = BC001V26_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = BC001V26_n463ContagemResultado_ParecerTcn[0];
            A474ContagemResultado_ContadorFMNom = BC001V26_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = BC001V26_n474ContagemResultado_ContadorFMNom[0];
            A999ContagemResultado_CrFMEhContratada = BC001V26_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = BC001V26_n999ContagemResultado_CrFMEhContratada[0];
            A1000ContagemResultado_CrFMEhContratante = BC001V26_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = BC001V26_n1000ContagemResultado_CrFMEhContratante[0];
            A478ContagemResultado_NaoCnfCntNom = BC001V26_A478ContagemResultado_NaoCnfCntNom[0];
            n478ContagemResultado_NaoCnfCntNom = BC001V26_n478ContagemResultado_NaoCnfCntNom[0];
            A517ContagemResultado_Ultima = BC001V26_A517ContagemResultado_Ultima[0];
            A800ContagemResultado_Deflator = BC001V26_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = BC001V26_n800ContagemResultado_Deflator[0];
            A833ContagemResultado_CstUntPrd = BC001V26_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = BC001V26_n833ContagemResultado_CstUntPrd[0];
            A1389ContagemResultado_RdmnIssueId = BC001V26_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V26_n1389ContagemResultado_RdmnIssueId[0];
            A1756ContagemResultado_NvlCnt = BC001V26_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = BC001V26_n1756ContagemResultado_NvlCnt[0];
            A854ContagemResultado_TipoPla = BC001V26_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = BC001V26_n854ContagemResultado_TipoPla[0];
            A852ContagemResultado_Planilha_Filetype = A854ContagemResultado_TipoPla;
            A853ContagemResultado_NomePla = BC001V26_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = BC001V26_n853ContagemResultado_NomePla[0];
            A852ContagemResultado_Planilha_Filename = A853ContagemResultado_NomePla;
            A493ContagemResultado_DemandaFM = BC001V26_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V26_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = BC001V26_A456ContagemResultado_Codigo[0];
            A470ContagemResultado_ContadorFMCod = BC001V26_A470ContagemResultado_ContadorFMCod[0];
            A469ContagemResultado_NaoCnfCntCod = BC001V26_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = BC001V26_n469ContagemResultado_NaoCnfCntCod[0];
            A890ContagemResultado_Responsavel = BC001V26_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V26_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V26_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V26_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V26_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V26_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = BC001V26_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V26_n52Contratada_AreaTrabalhoCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V26_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V26_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V26_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V26_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = BC001V26_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V26_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = BC001V26_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V26_n602ContagemResultado_OSVinculada[0];
            A479ContagemResultado_CrFMPessoaCod = BC001V26_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = BC001V26_n479ContagemResultado_CrFMPessoaCod[0];
            A852ContagemResultado_Planilha = BC001V26_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = BC001V26_n852ContagemResultado_Planilha[0];
         }
         Gx_mode = sMode72;
      }

      protected void ScanKeyEnd1V72( )
      {
         pr_default.close(24);
      }

      protected void AfterConfirm1V72( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1V72( )
      {
         /* Before Insert Rules */
         if ( (0==A469ContagemResultado_NaoCnfCntCod) )
         {
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
         }
      }

      protected void BeforeUpdate1V72( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1V72( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1V72( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1V72( )
      {
         /* Before Validate Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            GXt_decimal1 = AV27ContagemResultado_Divergencia;
            new prc_calculardivergencia(context ).execute(  AV20CalculoDivergencia,  A458ContagemResultado_PFBFS,  A460ContagemResultado_PFBFM,  A459ContagemResultado_PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
            AV27ContagemResultado_Divergencia = GXt_decimal1;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            A462ContagemResultado_Divergencia = AV27ContagemResultado_Divergencia;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && (0==A482ContagemResultadoContagens_Esforco) )
         {
            A482ContagemResultadoContagens_Esforco = 0;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) && ( A462ContagemResultado_Divergencia > AV19IndiceDivergencia ) )
         {
            A483ContagemResultado_StatusCnt = 7;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (0==A469ContagemResultado_NaoCnfCntCod) )
            {
               A483ContagemResultado_StatusCnt = 6;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( A483ContagemResultado_StatusCnt == O483ContagemResultado_StatusCnt ) && ! (Convert.ToDecimal(0)==A458ContagemResultado_PFBFS) && ( A462ContagemResultado_Divergencia <= AV19IndiceDivergencia ) )
               {
                  A483ContagemResultado_StatusCnt = 5;
               }
            }
         }
      }

      protected void DisableAttributes1V72( )
      {
      }

      protected void AddRow1V72( )
      {
         VarsToRow72( bcContagemResultadoContagens) ;
      }

      protected void ReadRow1V72( )
      {
         RowToVars72( bcContagemResultadoContagens, 1) ;
      }

      protected void InitializeNonKey1V72( )
      {
         A127Sistema_Codigo = 0;
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         A462ContagemResultado_Divergencia = 0;
         A483ContagemResultado_StatusCnt = 0;
         A501ContagemResultado_OsFsOsFm = "";
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A513Sistema_Coordenacao = "";
         n513Sistema_Coordenacao = false;
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         A601ContagemResultado_Servico = 0;
         n601ContagemResultado_Servico = false;
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         A805ContagemResultado_ContratadaOrigemCod = 0;
         n805ContagemResultado_ContratadaOrigemCod = false;
         A52Contratada_AreaTrabalhoCod = 0;
         n52Contratada_AreaTrabalhoCod = false;
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         A602ContagemResultado_OSVinculada = 0;
         n602ContagemResultado_OSVinculada = false;
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         A484ContagemResultado_StatusDmn = "";
         n484ContagemResultado_StatusDmn = false;
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         n481ContagemResultado_TimeCnt = false;
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         n901ContagemResultadoContagens_Prazo = false;
         A458ContagemResultado_PFBFS = 0;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = 0;
         n459ContagemResultado_PFLFS = false;
         A460ContagemResultado_PFBFM = 0;
         n460ContagemResultado_PFBFM = false;
         A461ContagemResultado_PFLFM = 0;
         n461ContagemResultado_PFLFM = false;
         A463ContagemResultado_ParecerTcn = "";
         n463ContagemResultado_ParecerTcn = false;
         A470ContagemResultado_ContadorFMCod = 0;
         A479ContagemResultado_CrFMPessoaCod = 0;
         n479ContagemResultado_CrFMPessoaCod = false;
         A474ContagemResultado_ContadorFMNom = "";
         n474ContagemResultado_ContadorFMNom = false;
         A999ContagemResultado_CrFMEhContratada = false;
         n999ContagemResultado_CrFMEhContratada = false;
         A1000ContagemResultado_CrFMEhContratante = false;
         n1000ContagemResultado_CrFMEhContratante = false;
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         A478ContagemResultado_NaoCnfCntNom = "";
         n478ContagemResultado_NaoCnfCntNom = false;
         A800ContagemResultado_Deflator = 0;
         n800ContagemResultado_Deflator = false;
         A833ContagemResultado_CstUntPrd = 0;
         n833ContagemResultado_CstUntPrd = false;
         A852ContagemResultado_Planilha = "";
         n852ContagemResultado_Planilha = false;
         A1389ContagemResultado_RdmnIssueId = 0;
         n1389ContagemResultado_RdmnIssueId = false;
         A1756ContagemResultado_NvlCnt = 0;
         n1756ContagemResultado_NvlCnt = false;
         A854ContagemResultado_TipoPla = "";
         n854ContagemResultado_TipoPla = false;
         A853ContagemResultado_NomePla = "";
         n853ContagemResultado_NomePla = false;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A482ContagemResultadoContagens_Esforco = 0;
         A517ContagemResultado_Ultima = true;
         O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
         Z482ContagemResultadoContagens_Esforco = 0;
         Z462ContagemResultado_Divergencia = 0;
         Z483ContagemResultado_StatusCnt = 0;
         Z481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         Z901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         Z458ContagemResultado_PFBFS = 0;
         Z459ContagemResultado_PFLFS = 0;
         Z460ContagemResultado_PFBFM = 0;
         Z461ContagemResultado_PFLFM = 0;
         Z517ContagemResultado_Ultima = false;
         Z800ContagemResultado_Deflator = 0;
         Z833ContagemResultado_CstUntPrd = 0;
         Z1756ContagemResultado_NvlCnt = 0;
         Z470ContagemResultado_ContadorFMCod = 0;
         Z469ContagemResultado_NaoCnfCntCod = 0;
      }

      protected void InitAll1V72( )
      {
         A456ContagemResultado_Codigo = 0;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = context.localUtil.Time( );
         InitializeNonKey1V72( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A482ContagemResultadoContagens_Esforco = i482ContagemResultadoContagens_Esforco;
         A517ContagemResultado_Ultima = i517ContagemResultado_Ultima;
      }

      public void VarsToRow72( SdtContagemResultadoContagens obj72 )
      {
         obj72.gxTpr_Mode = Gx_mode;
         obj72.gxTpr_Contagemresultado_divergencia = A462ContagemResultado_Divergencia;
         obj72.gxTpr_Contagemresultado_statuscnt = A483ContagemResultado_StatusCnt;
         obj72.gxTpr_Contagemresultado_osfsosfm = A501ContagemResultado_OsFsOsFm;
         obj72.gxTpr_Contagemresultado_ehvalidacao = A485ContagemResultado_EhValidacao;
         obj72.gxTpr_Sistema_coordenacao = A513Sistema_Coordenacao;
         obj72.gxTpr_Contagemresultado_demanda = A457ContagemResultado_Demanda;
         obj72.gxTpr_Contagemresultado_cntsrvcod = A1553ContagemResultado_CntSrvCod;
         obj72.gxTpr_Contagemresultado_servico = A601ContagemResultado_Servico;
         obj72.gxTpr_Contagemresultado_contratadacod = A490ContagemResultado_ContratadaCod;
         obj72.gxTpr_Contagemresultado_contratadaorigemcod = A805ContagemResultado_ContratadaOrigemCod;
         obj72.gxTpr_Contratada_areatrabalhocod = A52Contratada_AreaTrabalhoCod;
         obj72.gxTpr_Contagemresultado_responsavel = A890ContagemResultado_Responsavel;
         obj72.gxTpr_Contagemresultado_osvinculada = A602ContagemResultado_OSVinculada;
         obj72.gxTpr_Contagemresultado_naocnfdmncod = A468ContagemResultado_NaoCnfDmnCod;
         obj72.gxTpr_Contagemresultado_statusdmn = A484ContagemResultado_StatusDmn;
         obj72.gxTpr_Contagemresultado_timecnt = A481ContagemResultado_TimeCnt;
         obj72.gxTpr_Contagemresultadocontagens_prazo = A901ContagemResultadoContagens_Prazo;
         obj72.gxTpr_Contagemresultado_pfbfs = A458ContagemResultado_PFBFS;
         obj72.gxTpr_Contagemresultado_pflfs = A459ContagemResultado_PFLFS;
         obj72.gxTpr_Contagemresultado_pfbfm = A460ContagemResultado_PFBFM;
         obj72.gxTpr_Contagemresultado_pflfm = A461ContagemResultado_PFLFM;
         obj72.gxTpr_Contagemresultado_parecertcn = A463ContagemResultado_ParecerTcn;
         obj72.gxTpr_Contagemresultado_contadorfmcod = A470ContagemResultado_ContadorFMCod;
         obj72.gxTpr_Contagemresultado_crfmpessoacod = A479ContagemResultado_CrFMPessoaCod;
         obj72.gxTpr_Contagemresultado_contadorfmnom = A474ContagemResultado_ContadorFMNom;
         obj72.gxTpr_Contagemresultado_crfmehcontratada = A999ContagemResultado_CrFMEhContratada;
         obj72.gxTpr_Contagemresultado_crfmehcontratante = A1000ContagemResultado_CrFMEhContratante;
         obj72.gxTpr_Contagemresultado_naocnfcntcod = A469ContagemResultado_NaoCnfCntCod;
         obj72.gxTpr_Contagemresultado_naocnfcntnom = A478ContagemResultado_NaoCnfCntNom;
         obj72.gxTpr_Contagemresultado_deflator = A800ContagemResultado_Deflator;
         obj72.gxTpr_Contagemresultado_cstuntprd = A833ContagemResultado_CstUntPrd;
         obj72.gxTpr_Contagemresultado_planilha = A852ContagemResultado_Planilha;
         obj72.gxTpr_Contagemresultado_rdmnissueid = A1389ContagemResultado_RdmnIssueId;
         obj72.gxTpr_Contagemresultado_nvlcnt = A1756ContagemResultado_NvlCnt;
         obj72.gxTpr_Contagemresultado_tipopla = A854ContagemResultado_TipoPla;
         obj72.gxTpr_Contagemresultado_nomepla = A853ContagemResultado_NomePla;
         obj72.gxTpr_Contagemresultadocontagens_esforco = A482ContagemResultadoContagens_Esforco;
         obj72.gxTpr_Contagemresultado_ultima = A517ContagemResultado_Ultima;
         obj72.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj72.gxTpr_Contagemresultado_datacnt = A473ContagemResultado_DataCnt;
         obj72.gxTpr_Contagemresultado_horacnt = A511ContagemResultado_HoraCnt;
         obj72.gxTpr_Contagemresultado_codigo_Z = Z456ContagemResultado_Codigo;
         obj72.gxTpr_Contagemresultado_datacnt_Z = Z473ContagemResultado_DataCnt;
         obj72.gxTpr_Contagemresultado_horacnt_Z = Z511ContagemResultado_HoraCnt;
         obj72.gxTpr_Contagemresultado_osfsosfm_Z = Z501ContagemResultado_OsFsOsFm;
         obj72.gxTpr_Contagemresultado_ehvalidacao_Z = Z485ContagemResultado_EhValidacao;
         obj72.gxTpr_Sistema_coordenacao_Z = Z513Sistema_Coordenacao;
         obj72.gxTpr_Contagemresultado_demanda_Z = Z457ContagemResultado_Demanda;
         obj72.gxTpr_Contagemresultado_cntsrvcod_Z = Z1553ContagemResultado_CntSrvCod;
         obj72.gxTpr_Contagemresultado_servico_Z = Z601ContagemResultado_Servico;
         obj72.gxTpr_Contagemresultado_contratadacod_Z = Z490ContagemResultado_ContratadaCod;
         obj72.gxTpr_Contagemresultado_contratadaorigemcod_Z = Z805ContagemResultado_ContratadaOrigemCod;
         obj72.gxTpr_Contratada_areatrabalhocod_Z = Z52Contratada_AreaTrabalhoCod;
         obj72.gxTpr_Contagemresultado_responsavel_Z = Z890ContagemResultado_Responsavel;
         obj72.gxTpr_Contagemresultado_osvinculada_Z = Z602ContagemResultado_OSVinculada;
         obj72.gxTpr_Contagemresultado_naocnfdmncod_Z = Z468ContagemResultado_NaoCnfDmnCod;
         obj72.gxTpr_Contagemresultado_statusdmn_Z = Z484ContagemResultado_StatusDmn;
         obj72.gxTpr_Contagemresultado_timecnt_Z = Z481ContagemResultado_TimeCnt;
         obj72.gxTpr_Contagemresultadocontagens_prazo_Z = Z901ContagemResultadoContagens_Prazo;
         obj72.gxTpr_Contagemresultado_pfbfs_Z = Z458ContagemResultado_PFBFS;
         obj72.gxTpr_Contagemresultado_pflfs_Z = Z459ContagemResultado_PFLFS;
         obj72.gxTpr_Contagemresultado_pfbfm_Z = Z460ContagemResultado_PFBFM;
         obj72.gxTpr_Contagemresultado_pflfm_Z = Z461ContagemResultado_PFLFM;
         obj72.gxTpr_Contagemresultado_divergencia_Z = Z462ContagemResultado_Divergencia;
         obj72.gxTpr_Contagemresultado_contadorfmcod_Z = Z470ContagemResultado_ContadorFMCod;
         obj72.gxTpr_Contagemresultado_crfmpessoacod_Z = Z479ContagemResultado_CrFMPessoaCod;
         obj72.gxTpr_Contagemresultado_contadorfmnom_Z = Z474ContagemResultado_ContadorFMNom;
         obj72.gxTpr_Contagemresultado_crfmehcontratada_Z = Z999ContagemResultado_CrFMEhContratada;
         obj72.gxTpr_Contagemresultado_crfmehcontratante_Z = Z1000ContagemResultado_CrFMEhContratante;
         obj72.gxTpr_Contagemresultado_naocnfcntcod_Z = Z469ContagemResultado_NaoCnfCntCod;
         obj72.gxTpr_Contagemresultado_naocnfcntnom_Z = Z478ContagemResultado_NaoCnfCntNom;
         obj72.gxTpr_Contagemresultado_statuscnt_Z = Z483ContagemResultado_StatusCnt;
         obj72.gxTpr_Contagemresultadocontagens_esforco_Z = Z482ContagemResultadoContagens_Esforco;
         obj72.gxTpr_Contagemresultado_ultima_Z = Z517ContagemResultado_Ultima;
         obj72.gxTpr_Contagemresultado_deflator_Z = Z800ContagemResultado_Deflator;
         obj72.gxTpr_Contagemresultado_cstuntprd_Z = Z833ContagemResultado_CstUntPrd;
         obj72.gxTpr_Contagemresultado_nomepla_Z = Z853ContagemResultado_NomePla;
         obj72.gxTpr_Contagemresultado_tipopla_Z = Z854ContagemResultado_TipoPla;
         obj72.gxTpr_Contagemresultado_rdmnissueid_Z = Z1389ContagemResultado_RdmnIssueId;
         obj72.gxTpr_Contagemresultado_nvlcnt_Z = Z1756ContagemResultado_NvlCnt;
         obj72.gxTpr_Contagemresultado_ehvalidacao_N = (short)(Convert.ToInt16(n485ContagemResultado_EhValidacao));
         obj72.gxTpr_Sistema_coordenacao_N = (short)(Convert.ToInt16(n513Sistema_Coordenacao));
         obj72.gxTpr_Contagemresultado_demanda_N = (short)(Convert.ToInt16(n457ContagemResultado_Demanda));
         obj72.gxTpr_Contagemresultado_cntsrvcod_N = (short)(Convert.ToInt16(n1553ContagemResultado_CntSrvCod));
         obj72.gxTpr_Contagemresultado_servico_N = (short)(Convert.ToInt16(n601ContagemResultado_Servico));
         obj72.gxTpr_Contagemresultado_contratadacod_N = (short)(Convert.ToInt16(n490ContagemResultado_ContratadaCod));
         obj72.gxTpr_Contagemresultado_contratadaorigemcod_N = (short)(Convert.ToInt16(n805ContagemResultado_ContratadaOrigemCod));
         obj72.gxTpr_Contratada_areatrabalhocod_N = (short)(Convert.ToInt16(n52Contratada_AreaTrabalhoCod));
         obj72.gxTpr_Contagemresultado_responsavel_N = (short)(Convert.ToInt16(n890ContagemResultado_Responsavel));
         obj72.gxTpr_Contagemresultado_osvinculada_N = (short)(Convert.ToInt16(n602ContagemResultado_OSVinculada));
         obj72.gxTpr_Contagemresultado_naocnfdmncod_N = (short)(Convert.ToInt16(n468ContagemResultado_NaoCnfDmnCod));
         obj72.gxTpr_Contagemresultado_statusdmn_N = (short)(Convert.ToInt16(n484ContagemResultado_StatusDmn));
         obj72.gxTpr_Contagemresultado_timecnt_N = (short)(Convert.ToInt16(n481ContagemResultado_TimeCnt));
         obj72.gxTpr_Contagemresultadocontagens_prazo_N = (short)(Convert.ToInt16(n901ContagemResultadoContagens_Prazo));
         obj72.gxTpr_Contagemresultado_pfbfs_N = (short)(Convert.ToInt16(n458ContagemResultado_PFBFS));
         obj72.gxTpr_Contagemresultado_pflfs_N = (short)(Convert.ToInt16(n459ContagemResultado_PFLFS));
         obj72.gxTpr_Contagemresultado_pfbfm_N = (short)(Convert.ToInt16(n460ContagemResultado_PFBFM));
         obj72.gxTpr_Contagemresultado_pflfm_N = (short)(Convert.ToInt16(n461ContagemResultado_PFLFM));
         obj72.gxTpr_Contagemresultado_parecertcn_N = (short)(Convert.ToInt16(n463ContagemResultado_ParecerTcn));
         obj72.gxTpr_Contagemresultado_crfmpessoacod_N = (short)(Convert.ToInt16(n479ContagemResultado_CrFMPessoaCod));
         obj72.gxTpr_Contagemresultado_contadorfmnom_N = (short)(Convert.ToInt16(n474ContagemResultado_ContadorFMNom));
         obj72.gxTpr_Contagemresultado_crfmehcontratada_N = (short)(Convert.ToInt16(n999ContagemResultado_CrFMEhContratada));
         obj72.gxTpr_Contagemresultado_crfmehcontratante_N = (short)(Convert.ToInt16(n1000ContagemResultado_CrFMEhContratante));
         obj72.gxTpr_Contagemresultado_naocnfcntcod_N = (short)(Convert.ToInt16(n469ContagemResultado_NaoCnfCntCod));
         obj72.gxTpr_Contagemresultado_naocnfcntnom_N = (short)(Convert.ToInt16(n478ContagemResultado_NaoCnfCntNom));
         obj72.gxTpr_Contagemresultado_deflator_N = (short)(Convert.ToInt16(n800ContagemResultado_Deflator));
         obj72.gxTpr_Contagemresultado_cstuntprd_N = (short)(Convert.ToInt16(n833ContagemResultado_CstUntPrd));
         obj72.gxTpr_Contagemresultado_planilha_N = (short)(Convert.ToInt16(n852ContagemResultado_Planilha));
         obj72.gxTpr_Contagemresultado_nomepla_N = (short)(Convert.ToInt16(n853ContagemResultado_NomePla));
         obj72.gxTpr_Contagemresultado_tipopla_N = (short)(Convert.ToInt16(n854ContagemResultado_TipoPla));
         obj72.gxTpr_Contagemresultado_rdmnissueid_N = (short)(Convert.ToInt16(n1389ContagemResultado_RdmnIssueId));
         obj72.gxTpr_Contagemresultado_nvlcnt_N = (short)(Convert.ToInt16(n1756ContagemResultado_NvlCnt));
         obj72.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow72( SdtContagemResultadoContagens obj72 )
      {
         obj72.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj72.gxTpr_Contagemresultado_datacnt = A473ContagemResultado_DataCnt;
         obj72.gxTpr_Contagemresultado_horacnt = A511ContagemResultado_HoraCnt;
         return  ;
      }

      public void RowToVars72( SdtContagemResultadoContagens obj72 ,
                               int forceLoad )
      {
         Gx_mode = obj72.gxTpr_Mode;
         A462ContagemResultado_Divergencia = obj72.gxTpr_Contagemresultado_divergencia;
         A483ContagemResultado_StatusCnt = obj72.gxTpr_Contagemresultado_statuscnt;
         A501ContagemResultado_OsFsOsFm = obj72.gxTpr_Contagemresultado_osfsosfm;
         A485ContagemResultado_EhValidacao = obj72.gxTpr_Contagemresultado_ehvalidacao;
         n485ContagemResultado_EhValidacao = false;
         A513Sistema_Coordenacao = obj72.gxTpr_Sistema_coordenacao;
         n513Sistema_Coordenacao = false;
         A457ContagemResultado_Demanda = obj72.gxTpr_Contagemresultado_demanda;
         n457ContagemResultado_Demanda = false;
         A1553ContagemResultado_CntSrvCod = obj72.gxTpr_Contagemresultado_cntsrvcod;
         n1553ContagemResultado_CntSrvCod = false;
         A601ContagemResultado_Servico = obj72.gxTpr_Contagemresultado_servico;
         n601ContagemResultado_Servico = false;
         A490ContagemResultado_ContratadaCod = obj72.gxTpr_Contagemresultado_contratadacod;
         n490ContagemResultado_ContratadaCod = false;
         A805ContagemResultado_ContratadaOrigemCod = obj72.gxTpr_Contagemresultado_contratadaorigemcod;
         n805ContagemResultado_ContratadaOrigemCod = false;
         A52Contratada_AreaTrabalhoCod = obj72.gxTpr_Contratada_areatrabalhocod;
         n52Contratada_AreaTrabalhoCod = false;
         A890ContagemResultado_Responsavel = obj72.gxTpr_Contagemresultado_responsavel;
         n890ContagemResultado_Responsavel = false;
         A602ContagemResultado_OSVinculada = obj72.gxTpr_Contagemresultado_osvinculada;
         n602ContagemResultado_OSVinculada = false;
         A468ContagemResultado_NaoCnfDmnCod = obj72.gxTpr_Contagemresultado_naocnfdmncod;
         n468ContagemResultado_NaoCnfDmnCod = false;
         A484ContagemResultado_StatusDmn = obj72.gxTpr_Contagemresultado_statusdmn;
         n484ContagemResultado_StatusDmn = false;
         A481ContagemResultado_TimeCnt = obj72.gxTpr_Contagemresultado_timecnt;
         n481ContagemResultado_TimeCnt = false;
         A901ContagemResultadoContagens_Prazo = obj72.gxTpr_Contagemresultadocontagens_prazo;
         n901ContagemResultadoContagens_Prazo = false;
         A458ContagemResultado_PFBFS = obj72.gxTpr_Contagemresultado_pfbfs;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = obj72.gxTpr_Contagemresultado_pflfs;
         n459ContagemResultado_PFLFS = false;
         A460ContagemResultado_PFBFM = obj72.gxTpr_Contagemresultado_pfbfm;
         n460ContagemResultado_PFBFM = false;
         A461ContagemResultado_PFLFM = obj72.gxTpr_Contagemresultado_pflfm;
         n461ContagemResultado_PFLFM = false;
         A463ContagemResultado_ParecerTcn = obj72.gxTpr_Contagemresultado_parecertcn;
         n463ContagemResultado_ParecerTcn = false;
         A470ContagemResultado_ContadorFMCod = obj72.gxTpr_Contagemresultado_contadorfmcod;
         A479ContagemResultado_CrFMPessoaCod = obj72.gxTpr_Contagemresultado_crfmpessoacod;
         n479ContagemResultado_CrFMPessoaCod = false;
         A474ContagemResultado_ContadorFMNom = obj72.gxTpr_Contagemresultado_contadorfmnom;
         n474ContagemResultado_ContadorFMNom = false;
         A999ContagemResultado_CrFMEhContratada = obj72.gxTpr_Contagemresultado_crfmehcontratada;
         n999ContagemResultado_CrFMEhContratada = false;
         A1000ContagemResultado_CrFMEhContratante = obj72.gxTpr_Contagemresultado_crfmehcontratante;
         n1000ContagemResultado_CrFMEhContratante = false;
         A469ContagemResultado_NaoCnfCntCod = obj72.gxTpr_Contagemresultado_naocnfcntcod;
         n469ContagemResultado_NaoCnfCntCod = false;
         A478ContagemResultado_NaoCnfCntNom = obj72.gxTpr_Contagemresultado_naocnfcntnom;
         n478ContagemResultado_NaoCnfCntNom = false;
         A800ContagemResultado_Deflator = obj72.gxTpr_Contagemresultado_deflator;
         n800ContagemResultado_Deflator = false;
         A833ContagemResultado_CstUntPrd = obj72.gxTpr_Contagemresultado_cstuntprd;
         n833ContagemResultado_CstUntPrd = false;
         A852ContagemResultado_Planilha = obj72.gxTpr_Contagemresultado_planilha;
         n852ContagemResultado_Planilha = false;
         A1389ContagemResultado_RdmnIssueId = obj72.gxTpr_Contagemresultado_rdmnissueid;
         n1389ContagemResultado_RdmnIssueId = false;
         A1756ContagemResultado_NvlCnt = obj72.gxTpr_Contagemresultado_nvlcnt;
         n1756ContagemResultado_NvlCnt = false;
         A854ContagemResultado_TipoPla = (String.IsNullOrEmpty(StringUtil.RTrim( obj72.gxTpr_Contagemresultado_tipopla)) ? FileUtil.GetFileType( A852ContagemResultado_Planilha) : obj72.gxTpr_Contagemresultado_tipopla);
         n854ContagemResultado_TipoPla = false;
         A853ContagemResultado_NomePla = (String.IsNullOrEmpty(StringUtil.RTrim( obj72.gxTpr_Contagemresultado_nomepla)) ? FileUtil.GetFileName( A852ContagemResultado_Planilha) : obj72.gxTpr_Contagemresultado_nomepla);
         n853ContagemResultado_NomePla = false;
         A482ContagemResultadoContagens_Esforco = obj72.gxTpr_Contagemresultadocontagens_esforco;
         A517ContagemResultado_Ultima = obj72.gxTpr_Contagemresultado_ultima;
         A456ContagemResultado_Codigo = obj72.gxTpr_Contagemresultado_codigo;
         A473ContagemResultado_DataCnt = obj72.gxTpr_Contagemresultado_datacnt;
         A511ContagemResultado_HoraCnt = obj72.gxTpr_Contagemresultado_horacnt;
         Z456ContagemResultado_Codigo = obj72.gxTpr_Contagemresultado_codigo_Z;
         Z473ContagemResultado_DataCnt = obj72.gxTpr_Contagemresultado_datacnt_Z;
         Z511ContagemResultado_HoraCnt = obj72.gxTpr_Contagemresultado_horacnt_Z;
         Z501ContagemResultado_OsFsOsFm = obj72.gxTpr_Contagemresultado_osfsosfm_Z;
         Z485ContagemResultado_EhValidacao = obj72.gxTpr_Contagemresultado_ehvalidacao_Z;
         Z513Sistema_Coordenacao = obj72.gxTpr_Sistema_coordenacao_Z;
         Z457ContagemResultado_Demanda = obj72.gxTpr_Contagemresultado_demanda_Z;
         Z1553ContagemResultado_CntSrvCod = obj72.gxTpr_Contagemresultado_cntsrvcod_Z;
         Z601ContagemResultado_Servico = obj72.gxTpr_Contagemresultado_servico_Z;
         Z490ContagemResultado_ContratadaCod = obj72.gxTpr_Contagemresultado_contratadacod_Z;
         Z805ContagemResultado_ContratadaOrigemCod = obj72.gxTpr_Contagemresultado_contratadaorigemcod_Z;
         Z52Contratada_AreaTrabalhoCod = obj72.gxTpr_Contratada_areatrabalhocod_Z;
         Z890ContagemResultado_Responsavel = obj72.gxTpr_Contagemresultado_responsavel_Z;
         Z602ContagemResultado_OSVinculada = obj72.gxTpr_Contagemresultado_osvinculada_Z;
         Z468ContagemResultado_NaoCnfDmnCod = obj72.gxTpr_Contagemresultado_naocnfdmncod_Z;
         Z484ContagemResultado_StatusDmn = obj72.gxTpr_Contagemresultado_statusdmn_Z;
         Z481ContagemResultado_TimeCnt = obj72.gxTpr_Contagemresultado_timecnt_Z;
         Z901ContagemResultadoContagens_Prazo = obj72.gxTpr_Contagemresultadocontagens_prazo_Z;
         Z458ContagemResultado_PFBFS = obj72.gxTpr_Contagemresultado_pfbfs_Z;
         Z459ContagemResultado_PFLFS = obj72.gxTpr_Contagemresultado_pflfs_Z;
         Z460ContagemResultado_PFBFM = obj72.gxTpr_Contagemresultado_pfbfm_Z;
         Z461ContagemResultado_PFLFM = obj72.gxTpr_Contagemresultado_pflfm_Z;
         Z462ContagemResultado_Divergencia = obj72.gxTpr_Contagemresultado_divergencia_Z;
         Z470ContagemResultado_ContadorFMCod = obj72.gxTpr_Contagemresultado_contadorfmcod_Z;
         Z479ContagemResultado_CrFMPessoaCod = obj72.gxTpr_Contagemresultado_crfmpessoacod_Z;
         Z474ContagemResultado_ContadorFMNom = obj72.gxTpr_Contagemresultado_contadorfmnom_Z;
         Z999ContagemResultado_CrFMEhContratada = obj72.gxTpr_Contagemresultado_crfmehcontratada_Z;
         Z1000ContagemResultado_CrFMEhContratante = obj72.gxTpr_Contagemresultado_crfmehcontratante_Z;
         Z469ContagemResultado_NaoCnfCntCod = obj72.gxTpr_Contagemresultado_naocnfcntcod_Z;
         Z478ContagemResultado_NaoCnfCntNom = obj72.gxTpr_Contagemresultado_naocnfcntnom_Z;
         Z483ContagemResultado_StatusCnt = obj72.gxTpr_Contagemresultado_statuscnt_Z;
         O483ContagemResultado_StatusCnt = obj72.gxTpr_Contagemresultado_statuscnt_Z;
         Z482ContagemResultadoContagens_Esforco = obj72.gxTpr_Contagemresultadocontagens_esforco_Z;
         Z517ContagemResultado_Ultima = obj72.gxTpr_Contagemresultado_ultima_Z;
         Z800ContagemResultado_Deflator = obj72.gxTpr_Contagemresultado_deflator_Z;
         Z833ContagemResultado_CstUntPrd = obj72.gxTpr_Contagemresultado_cstuntprd_Z;
         Z853ContagemResultado_NomePla = obj72.gxTpr_Contagemresultado_nomepla_Z;
         Z854ContagemResultado_TipoPla = obj72.gxTpr_Contagemresultado_tipopla_Z;
         Z1389ContagemResultado_RdmnIssueId = obj72.gxTpr_Contagemresultado_rdmnissueid_Z;
         Z1756ContagemResultado_NvlCnt = obj72.gxTpr_Contagemresultado_nvlcnt_Z;
         n485ContagemResultado_EhValidacao = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_ehvalidacao_N));
         n513Sistema_Coordenacao = (bool)(Convert.ToBoolean(obj72.gxTpr_Sistema_coordenacao_N));
         n457ContagemResultado_Demanda = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_demanda_N));
         n1553ContagemResultado_CntSrvCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_cntsrvcod_N));
         n601ContagemResultado_Servico = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_servico_N));
         n490ContagemResultado_ContratadaCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_contratadacod_N));
         n805ContagemResultado_ContratadaOrigemCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_contratadaorigemcod_N));
         n52Contratada_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contratada_areatrabalhocod_N));
         n890ContagemResultado_Responsavel = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_responsavel_N));
         n602ContagemResultado_OSVinculada = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_osvinculada_N));
         n468ContagemResultado_NaoCnfDmnCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_naocnfdmncod_N));
         n484ContagemResultado_StatusDmn = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_statusdmn_N));
         n481ContagemResultado_TimeCnt = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_timecnt_N));
         n901ContagemResultadoContagens_Prazo = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultadocontagens_prazo_N));
         n458ContagemResultado_PFBFS = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_pfbfs_N));
         n459ContagemResultado_PFLFS = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_pflfs_N));
         n460ContagemResultado_PFBFM = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_pfbfm_N));
         n461ContagemResultado_PFLFM = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_pflfm_N));
         n463ContagemResultado_ParecerTcn = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_parecertcn_N));
         n479ContagemResultado_CrFMPessoaCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_crfmpessoacod_N));
         n474ContagemResultado_ContadorFMNom = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_contadorfmnom_N));
         n999ContagemResultado_CrFMEhContratada = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_crfmehcontratada_N));
         n1000ContagemResultado_CrFMEhContratante = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_crfmehcontratante_N));
         n469ContagemResultado_NaoCnfCntCod = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_naocnfcntcod_N));
         n478ContagemResultado_NaoCnfCntNom = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_naocnfcntnom_N));
         n800ContagemResultado_Deflator = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_deflator_N));
         n833ContagemResultado_CstUntPrd = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_cstuntprd_N));
         n852ContagemResultado_Planilha = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_planilha_N));
         n853ContagemResultado_NomePla = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_nomepla_N));
         n854ContagemResultado_TipoPla = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_tipopla_N));
         n1389ContagemResultado_RdmnIssueId = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_rdmnissueid_N));
         n1756ContagemResultado_NvlCnt = (bool)(Convert.ToBoolean(obj72.gxTpr_Contagemresultado_nvlcnt_N));
         Gx_mode = obj72.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A456ContagemResultado_Codigo = (int)getParm(obj,0);
         A473ContagemResultado_DataCnt = (DateTime)getParm(obj,1);
         A511ContagemResultado_HoraCnt = (String)getParm(obj,2);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1V72( ) ;
         ScanKeyStart1V72( ) ;
         if ( RcdFound72 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001V18 */
            pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
               AnyError = 1;
            }
            A146Modulo_Codigo = BC001V18_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V18_n146Modulo_Codigo[0];
            A485ContagemResultado_EhValidacao = BC001V18_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V18_n485ContagemResultado_EhValidacao[0];
            A457ContagemResultado_Demanda = BC001V18_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V18_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V18_n484ContagemResultado_StatusDmn[0];
            A1389ContagemResultado_RdmnIssueId = BC001V18_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V18_n1389ContagemResultado_RdmnIssueId[0];
            A493ContagemResultado_DemandaFM = BC001V18_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V18_n493ContagemResultado_DemandaFM[0];
            A890ContagemResultado_Responsavel = BC001V18_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V18_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V18_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V18_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V18_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V18_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V18_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V18_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V18_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC001V18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V18_n602ContagemResultado_OSVinculada[0];
            pr_default.close(16);
            /* Using cursor BC001V19 */
            pr_default.execute(17, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(17) == 101) )
            {
               if ( ! ( (0==A146Modulo_Codigo) ) )
               {
                  GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A127Sistema_Codigo = BC001V19_A127Sistema_Codigo[0];
            pr_default.close(17);
            /* Using cursor BC001V20 */
            pr_default.execute(18, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(18) == 101) )
            {
               if ( ! ( (0==A127Sistema_Codigo) ) )
               {
                  GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A513Sistema_Coordenacao = BC001V20_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V20_n513Sistema_Coordenacao[0];
            pr_default.close(18);
            /* Using cursor BC001V21 */
            pr_default.execute(19, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            if ( (pr_default.getStatus(19) == 101) )
            {
               if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
               {
                  GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A52Contratada_AreaTrabalhoCod = BC001V21_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V21_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(19);
            /* Using cursor BC001V22 */
            pr_default.execute(20, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            if ( (pr_default.getStatus(20) == 101) )
            {
               if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
               {
                  GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A601ContagemResultado_Servico = BC001V22_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V22_n601ContagemResultado_Servico[0];
            pr_default.close(20);
         }
         else
         {
            Gx_mode = "UPD";
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
         }
         ZM1V72( -19) ;
         OnLoadActions1V72( ) ;
         AddRow1V72( ) ;
         ScanKeyEnd1V72( ) ;
         if ( RcdFound72 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars72( bcContagemResultadoContagens, 0) ;
         ScanKeyStart1V72( ) ;
         if ( RcdFound72 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001V18 */
            pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
               AnyError = 1;
            }
            A146Modulo_Codigo = BC001V18_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC001V18_n146Modulo_Codigo[0];
            A485ContagemResultado_EhValidacao = BC001V18_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC001V18_n485ContagemResultado_EhValidacao[0];
            A457ContagemResultado_Demanda = BC001V18_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = BC001V18_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = BC001V18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC001V18_n484ContagemResultado_StatusDmn[0];
            A1389ContagemResultado_RdmnIssueId = BC001V18_A1389ContagemResultado_RdmnIssueId[0];
            n1389ContagemResultado_RdmnIssueId = BC001V18_n1389ContagemResultado_RdmnIssueId[0];
            A493ContagemResultado_DemandaFM = BC001V18_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC001V18_n493ContagemResultado_DemandaFM[0];
            A890ContagemResultado_Responsavel = BC001V18_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC001V18_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC001V18_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC001V18_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC001V18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC001V18_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC001V18_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC001V18_n805ContagemResultado_ContratadaOrigemCod[0];
            A1553ContagemResultado_CntSrvCod = BC001V18_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC001V18_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC001V18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC001V18_n602ContagemResultado_OSVinculada[0];
            pr_default.close(16);
            /* Using cursor BC001V19 */
            pr_default.execute(17, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(17) == 101) )
            {
               if ( ! ( (0==A146Modulo_Codigo) ) )
               {
                  GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A127Sistema_Codigo = BC001V19_A127Sistema_Codigo[0];
            pr_default.close(17);
            /* Using cursor BC001V20 */
            pr_default.execute(18, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(18) == 101) )
            {
               if ( ! ( (0==A127Sistema_Codigo) ) )
               {
                  GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A513Sistema_Coordenacao = BC001V20_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = BC001V20_n513Sistema_Coordenacao[0];
            pr_default.close(18);
            /* Using cursor BC001V21 */
            pr_default.execute(19, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            if ( (pr_default.getStatus(19) == 101) )
            {
               if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
               {
                  GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A52Contratada_AreaTrabalhoCod = BC001V21_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = BC001V21_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(19);
            /* Using cursor BC001V22 */
            pr_default.execute(20, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            if ( (pr_default.getStatus(20) == 101) )
            {
               if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
               {
                  GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
                  AnyError = 1;
               }
            }
            A601ContagemResultado_Servico = BC001V22_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = BC001V22_n601ContagemResultado_Servico[0];
            pr_default.close(20);
         }
         else
         {
            Gx_mode = "UPD";
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            Z511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            O483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
         }
         ZM1V72( -19) ;
         OnLoadActions1V72( ) ;
         AddRow1V72( ) ;
         ScanKeyEnd1V72( ) ;
         if ( RcdFound72 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars72( bcContagemResultadoContagens, 0) ;
         nKeyPressed = 1;
         GetKey1V72( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1V72( ) ;
         }
         else
         {
            if ( RcdFound72 == 1 )
            {
               if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
               {
                  A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
                  A473ContagemResultado_DataCnt = Z473ContagemResultado_DataCnt;
                  A511ContagemResultado_HoraCnt = Z511ContagemResultado_HoraCnt;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1V72( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1V72( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1V72( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow72( bcContagemResultadoContagens) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars72( bcContagemResultadoContagens, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1V72( ) ;
         if ( RcdFound72 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
            {
               A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
               A473ContagemResultado_DataCnt = Z473ContagemResultado_DataCnt;
               A511ContagemResultado_HoraCnt = Z511ContagemResultado_HoraCnt;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != Z473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, Z511ContagemResultado_HoraCnt) != 0 ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(21);
         pr_default.close(23);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(22);
         context.RollbackDataStores( "ContagemResultadoContagens_BC");
         VarsToRow72( bcContagemResultadoContagens) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoContagens.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoContagens.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoContagens )
         {
            bcContagemResultadoContagens = (SdtContagemResultadoContagens)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoContagens.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoContagens.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow72( bcContagemResultadoContagens) ;
            }
            else
            {
               RowToVars72( bcContagemResultadoContagens, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoContagens.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoContagens.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars72( bcContagemResultadoContagens, 1) ;
         return  ;
      }

      public SdtContagemResultadoContagens ContagemResultadoContagens_BC
      {
         get {
            return bcContagemResultadoContagens ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(21);
         pr_default.close(23);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z473ContagemResultado_DataCnt = DateTime.MinValue;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         Z511ContagemResultado_HoraCnt = "";
         A511ContagemResultado_HoraCnt = "";
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV14WebSession = context.GetSession();
         AV37Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV35CalculoPFinal = "";
         AV20CalculoDivergencia = "";
         Z481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         Z901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         Z501ContagemResultado_OsFsOsFm = "";
         A501ContagemResultado_OsFsOsFm = "";
         Z457ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         Z484ContagemResultado_StatusDmn = "";
         A484ContagemResultado_StatusDmn = "";
         Z493ContagemResultado_DemandaFM = "";
         A493ContagemResultado_DemandaFM = "";
         Z478ContagemResultado_NaoCnfCntNom = "";
         A478ContagemResultado_NaoCnfCntNom = "";
         Z513Sistema_Coordenacao = "";
         A513Sistema_Coordenacao = "";
         Z474ContagemResultado_ContadorFMNom = "";
         A474ContagemResultado_ContadorFMNom = "";
         Z463ContagemResultado_ParecerTcn = "";
         A463ContagemResultado_ParecerTcn = "";
         Z852ContagemResultado_Planilha = "";
         A852ContagemResultado_Planilha = "";
         Z854ContagemResultado_TipoPla = "";
         A854ContagemResultado_TipoPla = "";
         Z853ContagemResultado_NomePla = "";
         A853ContagemResultado_NomePla = "";
         BC001V12_A146Modulo_Codigo = new int[1] ;
         BC001V12_n146Modulo_Codigo = new bool[] {false} ;
         BC001V12_A127Sistema_Codigo = new int[1] ;
         BC001V12_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V12_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC001V12_A482ContagemResultadoContagens_Esforco = new short[1] ;
         BC001V12_A462ContagemResultado_Divergencia = new decimal[1] ;
         BC001V12_A483ContagemResultado_StatusCnt = new short[1] ;
         BC001V12_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V12_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V12_A513Sistema_Coordenacao = new String[] {""} ;
         BC001V12_n513Sistema_Coordenacao = new bool[] {false} ;
         BC001V12_A457ContagemResultado_Demanda = new String[] {""} ;
         BC001V12_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC001V12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC001V12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC001V12_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V12_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         BC001V12_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC001V12_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         BC001V12_A458ContagemResultado_PFBFS = new decimal[1] ;
         BC001V12_n458ContagemResultado_PFBFS = new bool[] {false} ;
         BC001V12_A459ContagemResultado_PFLFS = new decimal[1] ;
         BC001V12_n459ContagemResultado_PFLFS = new bool[] {false} ;
         BC001V12_A460ContagemResultado_PFBFM = new decimal[1] ;
         BC001V12_n460ContagemResultado_PFBFM = new bool[] {false} ;
         BC001V12_A461ContagemResultado_PFLFM = new decimal[1] ;
         BC001V12_n461ContagemResultado_PFLFM = new bool[] {false} ;
         BC001V12_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         BC001V12_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         BC001V12_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         BC001V12_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         BC001V12_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V12_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V12_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V12_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V12_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         BC001V12_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         BC001V12_A517ContagemResultado_Ultima = new bool[] {false} ;
         BC001V12_A800ContagemResultado_Deflator = new decimal[1] ;
         BC001V12_n800ContagemResultado_Deflator = new bool[] {false} ;
         BC001V12_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         BC001V12_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         BC001V12_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         BC001V12_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         BC001V12_A1756ContagemResultado_NvlCnt = new short[1] ;
         BC001V12_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         BC001V12_A854ContagemResultado_TipoPla = new String[] {""} ;
         BC001V12_n854ContagemResultado_TipoPla = new bool[] {false} ;
         BC001V12_A853ContagemResultado_NomePla = new String[] {""} ;
         BC001V12_n853ContagemResultado_NomePla = new bool[] {false} ;
         BC001V12_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC001V12_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC001V12_A456ContagemResultado_Codigo = new int[1] ;
         BC001V12_A470ContagemResultado_ContadorFMCod = new int[1] ;
         BC001V12_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         BC001V12_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         BC001V12_A890ContagemResultado_Responsavel = new int[1] ;
         BC001V12_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC001V12_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC001V12_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC001V12_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC001V12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC001V12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC001V12_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC001V12_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC001V12_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC001V12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC001V12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC001V12_A601ContagemResultado_Servico = new int[1] ;
         BC001V12_n601ContagemResultado_Servico = new bool[] {false} ;
         BC001V12_A602ContagemResultado_OSVinculada = new int[1] ;
         BC001V12_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC001V12_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         BC001V12_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         BC001V12_A852ContagemResultado_Planilha = new String[] {""} ;
         BC001V12_n852ContagemResultado_Planilha = new bool[] {false} ;
         A852ContagemResultado_Planilha_Filetype = "";
         A852ContagemResultado_Planilha_Filename = "";
         BC001V4_A146Modulo_Codigo = new int[1] ;
         BC001V4_n146Modulo_Codigo = new bool[] {false} ;
         BC001V4_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V4_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V4_A457ContagemResultado_Demanda = new String[] {""} ;
         BC001V4_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC001V4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC001V4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC001V4_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         BC001V4_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         BC001V4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC001V4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC001V4_A890ContagemResultado_Responsavel = new int[1] ;
         BC001V4_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC001V4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC001V4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC001V4_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC001V4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC001V4_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC001V4_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC001V4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC001V4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC001V4_A602ContagemResultado_OSVinculada = new int[1] ;
         BC001V4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC001V7_A127Sistema_Codigo = new int[1] ;
         BC001V8_A513Sistema_Coordenacao = new String[] {""} ;
         BC001V8_n513Sistema_Coordenacao = new bool[] {false} ;
         BC001V9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC001V9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC001V10_A601ContagemResultado_Servico = new int[1] ;
         BC001V10_n601ContagemResultado_Servico = new bool[] {false} ;
         BC001V5_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V5_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V5_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V5_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V5_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         BC001V5_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         BC001V11_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         BC001V11_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         BC001V6_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         BC001V6_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         BC001V13_A456ContagemResultado_Codigo = new int[1] ;
         BC001V13_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V13_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC001V3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC001V3_A482ContagemResultadoContagens_Esforco = new short[1] ;
         BC001V3_A462ContagemResultado_Divergencia = new decimal[1] ;
         BC001V3_A483ContagemResultado_StatusCnt = new short[1] ;
         BC001V3_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V3_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         BC001V3_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC001V3_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         BC001V3_A458ContagemResultado_PFBFS = new decimal[1] ;
         BC001V3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         BC001V3_A459ContagemResultado_PFLFS = new decimal[1] ;
         BC001V3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         BC001V3_A460ContagemResultado_PFBFM = new decimal[1] ;
         BC001V3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         BC001V3_A461ContagemResultado_PFLFM = new decimal[1] ;
         BC001V3_n461ContagemResultado_PFLFM = new bool[] {false} ;
         BC001V3_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         BC001V3_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         BC001V3_A517ContagemResultado_Ultima = new bool[] {false} ;
         BC001V3_A800ContagemResultado_Deflator = new decimal[1] ;
         BC001V3_n800ContagemResultado_Deflator = new bool[] {false} ;
         BC001V3_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         BC001V3_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         BC001V3_A1756ContagemResultado_NvlCnt = new short[1] ;
         BC001V3_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         BC001V3_A854ContagemResultado_TipoPla = new String[] {""} ;
         BC001V3_n854ContagemResultado_TipoPla = new bool[] {false} ;
         BC001V3_A853ContagemResultado_NomePla = new String[] {""} ;
         BC001V3_n853ContagemResultado_NomePla = new bool[] {false} ;
         BC001V3_A456ContagemResultado_Codigo = new int[1] ;
         BC001V3_A470ContagemResultado_ContadorFMCod = new int[1] ;
         BC001V3_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         BC001V3_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         BC001V3_A852ContagemResultado_Planilha = new String[] {""} ;
         BC001V3_n852ContagemResultado_Planilha = new bool[] {false} ;
         sMode72 = "";
         BC001V2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC001V2_A482ContagemResultadoContagens_Esforco = new short[1] ;
         BC001V2_A462ContagemResultado_Divergencia = new decimal[1] ;
         BC001V2_A483ContagemResultado_StatusCnt = new short[1] ;
         BC001V2_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V2_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         BC001V2_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC001V2_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         BC001V2_A458ContagemResultado_PFBFS = new decimal[1] ;
         BC001V2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         BC001V2_A459ContagemResultado_PFLFS = new decimal[1] ;
         BC001V2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         BC001V2_A460ContagemResultado_PFBFM = new decimal[1] ;
         BC001V2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         BC001V2_A461ContagemResultado_PFLFM = new decimal[1] ;
         BC001V2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         BC001V2_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         BC001V2_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         BC001V2_A517ContagemResultado_Ultima = new bool[] {false} ;
         BC001V2_A800ContagemResultado_Deflator = new decimal[1] ;
         BC001V2_n800ContagemResultado_Deflator = new bool[] {false} ;
         BC001V2_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         BC001V2_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         BC001V2_A1756ContagemResultado_NvlCnt = new short[1] ;
         BC001V2_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         BC001V2_A854ContagemResultado_TipoPla = new String[] {""} ;
         BC001V2_n854ContagemResultado_TipoPla = new bool[] {false} ;
         BC001V2_A853ContagemResultado_NomePla = new String[] {""} ;
         BC001V2_n853ContagemResultado_NomePla = new bool[] {false} ;
         BC001V2_A456ContagemResultado_Codigo = new int[1] ;
         BC001V2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         BC001V2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         BC001V2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         BC001V2_A852ContagemResultado_Planilha = new String[] {""} ;
         BC001V2_n852ContagemResultado_Planilha = new bool[] {false} ;
         BC001V18_A146Modulo_Codigo = new int[1] ;
         BC001V18_n146Modulo_Codigo = new bool[] {false} ;
         BC001V18_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V18_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V18_A457ContagemResultado_Demanda = new String[] {""} ;
         BC001V18_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC001V18_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC001V18_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC001V18_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         BC001V18_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         BC001V18_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC001V18_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC001V18_A890ContagemResultado_Responsavel = new int[1] ;
         BC001V18_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC001V18_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC001V18_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC001V18_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC001V18_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC001V18_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC001V18_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC001V18_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC001V18_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC001V18_A602ContagemResultado_OSVinculada = new int[1] ;
         BC001V18_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC001V19_A127Sistema_Codigo = new int[1] ;
         BC001V20_A513Sistema_Coordenacao = new String[] {""} ;
         BC001V20_n513Sistema_Coordenacao = new bool[] {false} ;
         BC001V21_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC001V21_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC001V22_A601ContagemResultado_Servico = new int[1] ;
         BC001V22_n601ContagemResultado_Servico = new bool[] {false} ;
         BC001V23_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V23_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V23_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V23_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V23_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         BC001V23_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         BC001V24_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         BC001V24_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         BC001V25_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         BC001V25_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         GXt_char2 = "";
         BC001V26_A146Modulo_Codigo = new int[1] ;
         BC001V26_n146Modulo_Codigo = new bool[] {false} ;
         BC001V26_A127Sistema_Codigo = new int[1] ;
         BC001V26_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V26_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC001V26_A482ContagemResultadoContagens_Esforco = new short[1] ;
         BC001V26_A462ContagemResultado_Divergencia = new decimal[1] ;
         BC001V26_A483ContagemResultado_StatusCnt = new short[1] ;
         BC001V26_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V26_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC001V26_A513Sistema_Coordenacao = new String[] {""} ;
         BC001V26_n513Sistema_Coordenacao = new bool[] {false} ;
         BC001V26_A457ContagemResultado_Demanda = new String[] {""} ;
         BC001V26_n457ContagemResultado_Demanda = new bool[] {false} ;
         BC001V26_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC001V26_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC001V26_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         BC001V26_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         BC001V26_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC001V26_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         BC001V26_A458ContagemResultado_PFBFS = new decimal[1] ;
         BC001V26_n458ContagemResultado_PFBFS = new bool[] {false} ;
         BC001V26_A459ContagemResultado_PFLFS = new decimal[1] ;
         BC001V26_n459ContagemResultado_PFLFS = new bool[] {false} ;
         BC001V26_A460ContagemResultado_PFBFM = new decimal[1] ;
         BC001V26_n460ContagemResultado_PFBFM = new bool[] {false} ;
         BC001V26_A461ContagemResultado_PFLFM = new decimal[1] ;
         BC001V26_n461ContagemResultado_PFLFM = new bool[] {false} ;
         BC001V26_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         BC001V26_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         BC001V26_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         BC001V26_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         BC001V26_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V26_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         BC001V26_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V26_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         BC001V26_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         BC001V26_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         BC001V26_A517ContagemResultado_Ultima = new bool[] {false} ;
         BC001V26_A800ContagemResultado_Deflator = new decimal[1] ;
         BC001V26_n800ContagemResultado_Deflator = new bool[] {false} ;
         BC001V26_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         BC001V26_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         BC001V26_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         BC001V26_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         BC001V26_A1756ContagemResultado_NvlCnt = new short[1] ;
         BC001V26_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         BC001V26_A854ContagemResultado_TipoPla = new String[] {""} ;
         BC001V26_n854ContagemResultado_TipoPla = new bool[] {false} ;
         BC001V26_A853ContagemResultado_NomePla = new String[] {""} ;
         BC001V26_n853ContagemResultado_NomePla = new bool[] {false} ;
         BC001V26_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC001V26_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC001V26_A456ContagemResultado_Codigo = new int[1] ;
         BC001V26_A470ContagemResultado_ContadorFMCod = new int[1] ;
         BC001V26_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         BC001V26_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         BC001V26_A890ContagemResultado_Responsavel = new int[1] ;
         BC001V26_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC001V26_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC001V26_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC001V26_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC001V26_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC001V26_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC001V26_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         BC001V26_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC001V26_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC001V26_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC001V26_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC001V26_A601ContagemResultado_Servico = new int[1] ;
         BC001V26_n601ContagemResultado_Servico = new bool[] {false} ;
         BC001V26_A602ContagemResultado_OSVinculada = new int[1] ;
         BC001V26_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC001V26_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         BC001V26_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         BC001V26_A852ContagemResultado_Planilha = new String[] {""} ;
         BC001V26_n852ContagemResultado_Planilha = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadocontagens_bc__default(),
            new Object[][] {
                new Object[] {
               BC001V2_A473ContagemResultado_DataCnt, BC001V2_A511ContagemResultado_HoraCnt, BC001V2_A482ContagemResultadoContagens_Esforco, BC001V2_A462ContagemResultado_Divergencia, BC001V2_A483ContagemResultado_StatusCnt, BC001V2_A481ContagemResultado_TimeCnt, BC001V2_n481ContagemResultado_TimeCnt, BC001V2_A901ContagemResultadoContagens_Prazo, BC001V2_n901ContagemResultadoContagens_Prazo, BC001V2_A458ContagemResultado_PFBFS,
               BC001V2_n458ContagemResultado_PFBFS, BC001V2_A459ContagemResultado_PFLFS, BC001V2_n459ContagemResultado_PFLFS, BC001V2_A460ContagemResultado_PFBFM, BC001V2_n460ContagemResultado_PFBFM, BC001V2_A461ContagemResultado_PFLFM, BC001V2_n461ContagemResultado_PFLFM, BC001V2_A463ContagemResultado_ParecerTcn, BC001V2_n463ContagemResultado_ParecerTcn, BC001V2_A517ContagemResultado_Ultima,
               BC001V2_A800ContagemResultado_Deflator, BC001V2_n800ContagemResultado_Deflator, BC001V2_A833ContagemResultado_CstUntPrd, BC001V2_n833ContagemResultado_CstUntPrd, BC001V2_A1756ContagemResultado_NvlCnt, BC001V2_n1756ContagemResultado_NvlCnt, BC001V2_A854ContagemResultado_TipoPla, BC001V2_n854ContagemResultado_TipoPla, BC001V2_A853ContagemResultado_NomePla, BC001V2_n853ContagemResultado_NomePla,
               BC001V2_A456ContagemResultado_Codigo, BC001V2_A470ContagemResultado_ContadorFMCod, BC001V2_A469ContagemResultado_NaoCnfCntCod, BC001V2_n469ContagemResultado_NaoCnfCntCod, BC001V2_A852ContagemResultado_Planilha, BC001V2_n852ContagemResultado_Planilha
               }
               , new Object[] {
               BC001V3_A473ContagemResultado_DataCnt, BC001V3_A511ContagemResultado_HoraCnt, BC001V3_A482ContagemResultadoContagens_Esforco, BC001V3_A462ContagemResultado_Divergencia, BC001V3_A483ContagemResultado_StatusCnt, BC001V3_A481ContagemResultado_TimeCnt, BC001V3_n481ContagemResultado_TimeCnt, BC001V3_A901ContagemResultadoContagens_Prazo, BC001V3_n901ContagemResultadoContagens_Prazo, BC001V3_A458ContagemResultado_PFBFS,
               BC001V3_n458ContagemResultado_PFBFS, BC001V3_A459ContagemResultado_PFLFS, BC001V3_n459ContagemResultado_PFLFS, BC001V3_A460ContagemResultado_PFBFM, BC001V3_n460ContagemResultado_PFBFM, BC001V3_A461ContagemResultado_PFLFM, BC001V3_n461ContagemResultado_PFLFM, BC001V3_A463ContagemResultado_ParecerTcn, BC001V3_n463ContagemResultado_ParecerTcn, BC001V3_A517ContagemResultado_Ultima,
               BC001V3_A800ContagemResultado_Deflator, BC001V3_n800ContagemResultado_Deflator, BC001V3_A833ContagemResultado_CstUntPrd, BC001V3_n833ContagemResultado_CstUntPrd, BC001V3_A1756ContagemResultado_NvlCnt, BC001V3_n1756ContagemResultado_NvlCnt, BC001V3_A854ContagemResultado_TipoPla, BC001V3_n854ContagemResultado_TipoPla, BC001V3_A853ContagemResultado_NomePla, BC001V3_n853ContagemResultado_NomePla,
               BC001V3_A456ContagemResultado_Codigo, BC001V3_A470ContagemResultado_ContadorFMCod, BC001V3_A469ContagemResultado_NaoCnfCntCod, BC001V3_n469ContagemResultado_NaoCnfCntCod, BC001V3_A852ContagemResultado_Planilha, BC001V3_n852ContagemResultado_Planilha
               }
               , new Object[] {
               BC001V4_A146Modulo_Codigo, BC001V4_n146Modulo_Codigo, BC001V4_A485ContagemResultado_EhValidacao, BC001V4_n485ContagemResultado_EhValidacao, BC001V4_A457ContagemResultado_Demanda, BC001V4_n457ContagemResultado_Demanda, BC001V4_A484ContagemResultado_StatusDmn, BC001V4_n484ContagemResultado_StatusDmn, BC001V4_A1389ContagemResultado_RdmnIssueId, BC001V4_n1389ContagemResultado_RdmnIssueId,
               BC001V4_A493ContagemResultado_DemandaFM, BC001V4_n493ContagemResultado_DemandaFM, BC001V4_A890ContagemResultado_Responsavel, BC001V4_n890ContagemResultado_Responsavel, BC001V4_A468ContagemResultado_NaoCnfDmnCod, BC001V4_n468ContagemResultado_NaoCnfDmnCod, BC001V4_A490ContagemResultado_ContratadaCod, BC001V4_n490ContagemResultado_ContratadaCod, BC001V4_A805ContagemResultado_ContratadaOrigemCod, BC001V4_n805ContagemResultado_ContratadaOrigemCod,
               BC001V4_A1553ContagemResultado_CntSrvCod, BC001V4_n1553ContagemResultado_CntSrvCod, BC001V4_A602ContagemResultado_OSVinculada, BC001V4_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC001V5_A999ContagemResultado_CrFMEhContratada, BC001V5_n999ContagemResultado_CrFMEhContratada, BC001V5_A1000ContagemResultado_CrFMEhContratante, BC001V5_n1000ContagemResultado_CrFMEhContratante, BC001V5_A479ContagemResultado_CrFMPessoaCod, BC001V5_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               BC001V6_A478ContagemResultado_NaoCnfCntNom, BC001V6_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               BC001V7_A127Sistema_Codigo
               }
               , new Object[] {
               BC001V8_A513Sistema_Coordenacao, BC001V8_n513Sistema_Coordenacao
               }
               , new Object[] {
               BC001V9_A52Contratada_AreaTrabalhoCod, BC001V9_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               BC001V10_A601ContagemResultado_Servico, BC001V10_n601ContagemResultado_Servico
               }
               , new Object[] {
               BC001V11_A474ContagemResultado_ContadorFMNom, BC001V11_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               BC001V12_A146Modulo_Codigo, BC001V12_n146Modulo_Codigo, BC001V12_A127Sistema_Codigo, BC001V12_A473ContagemResultado_DataCnt, BC001V12_A511ContagemResultado_HoraCnt, BC001V12_A482ContagemResultadoContagens_Esforco, BC001V12_A462ContagemResultado_Divergencia, BC001V12_A483ContagemResultado_StatusCnt, BC001V12_A485ContagemResultado_EhValidacao, BC001V12_n485ContagemResultado_EhValidacao,
               BC001V12_A513Sistema_Coordenacao, BC001V12_n513Sistema_Coordenacao, BC001V12_A457ContagemResultado_Demanda, BC001V12_n457ContagemResultado_Demanda, BC001V12_A484ContagemResultado_StatusDmn, BC001V12_n484ContagemResultado_StatusDmn, BC001V12_A481ContagemResultado_TimeCnt, BC001V12_n481ContagemResultado_TimeCnt, BC001V12_A901ContagemResultadoContagens_Prazo, BC001V12_n901ContagemResultadoContagens_Prazo,
               BC001V12_A458ContagemResultado_PFBFS, BC001V12_n458ContagemResultado_PFBFS, BC001V12_A459ContagemResultado_PFLFS, BC001V12_n459ContagemResultado_PFLFS, BC001V12_A460ContagemResultado_PFBFM, BC001V12_n460ContagemResultado_PFBFM, BC001V12_A461ContagemResultado_PFLFM, BC001V12_n461ContagemResultado_PFLFM, BC001V12_A463ContagemResultado_ParecerTcn, BC001V12_n463ContagemResultado_ParecerTcn,
               BC001V12_A474ContagemResultado_ContadorFMNom, BC001V12_n474ContagemResultado_ContadorFMNom, BC001V12_A999ContagemResultado_CrFMEhContratada, BC001V12_n999ContagemResultado_CrFMEhContratada, BC001V12_A1000ContagemResultado_CrFMEhContratante, BC001V12_n1000ContagemResultado_CrFMEhContratante, BC001V12_A478ContagemResultado_NaoCnfCntNom, BC001V12_n478ContagemResultado_NaoCnfCntNom, BC001V12_A517ContagemResultado_Ultima, BC001V12_A800ContagemResultado_Deflator,
               BC001V12_n800ContagemResultado_Deflator, BC001V12_A833ContagemResultado_CstUntPrd, BC001V12_n833ContagemResultado_CstUntPrd, BC001V12_A1389ContagemResultado_RdmnIssueId, BC001V12_n1389ContagemResultado_RdmnIssueId, BC001V12_A1756ContagemResultado_NvlCnt, BC001V12_n1756ContagemResultado_NvlCnt, BC001V12_A854ContagemResultado_TipoPla, BC001V12_n854ContagemResultado_TipoPla, BC001V12_A853ContagemResultado_NomePla,
               BC001V12_n853ContagemResultado_NomePla, BC001V12_A493ContagemResultado_DemandaFM, BC001V12_n493ContagemResultado_DemandaFM, BC001V12_A456ContagemResultado_Codigo, BC001V12_A470ContagemResultado_ContadorFMCod, BC001V12_A469ContagemResultado_NaoCnfCntCod, BC001V12_n469ContagemResultado_NaoCnfCntCod, BC001V12_A890ContagemResultado_Responsavel, BC001V12_n890ContagemResultado_Responsavel, BC001V12_A468ContagemResultado_NaoCnfDmnCod,
               BC001V12_n468ContagemResultado_NaoCnfDmnCod, BC001V12_A490ContagemResultado_ContratadaCod, BC001V12_n490ContagemResultado_ContratadaCod, BC001V12_A52Contratada_AreaTrabalhoCod, BC001V12_n52Contratada_AreaTrabalhoCod, BC001V12_A805ContagemResultado_ContratadaOrigemCod, BC001V12_n805ContagemResultado_ContratadaOrigemCod, BC001V12_A1553ContagemResultado_CntSrvCod, BC001V12_n1553ContagemResultado_CntSrvCod, BC001V12_A601ContagemResultado_Servico,
               BC001V12_n601ContagemResultado_Servico, BC001V12_A602ContagemResultado_OSVinculada, BC001V12_n602ContagemResultado_OSVinculada, BC001V12_A479ContagemResultado_CrFMPessoaCod, BC001V12_n479ContagemResultado_CrFMPessoaCod, BC001V12_A852ContagemResultado_Planilha, BC001V12_n852ContagemResultado_Planilha
               }
               , new Object[] {
               BC001V13_A456ContagemResultado_Codigo, BC001V13_A473ContagemResultado_DataCnt, BC001V13_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001V18_A146Modulo_Codigo, BC001V18_n146Modulo_Codigo, BC001V18_A485ContagemResultado_EhValidacao, BC001V18_n485ContagemResultado_EhValidacao, BC001V18_A457ContagemResultado_Demanda, BC001V18_n457ContagemResultado_Demanda, BC001V18_A484ContagemResultado_StatusDmn, BC001V18_n484ContagemResultado_StatusDmn, BC001V18_A1389ContagemResultado_RdmnIssueId, BC001V18_n1389ContagemResultado_RdmnIssueId,
               BC001V18_A493ContagemResultado_DemandaFM, BC001V18_n493ContagemResultado_DemandaFM, BC001V18_A890ContagemResultado_Responsavel, BC001V18_n890ContagemResultado_Responsavel, BC001V18_A468ContagemResultado_NaoCnfDmnCod, BC001V18_n468ContagemResultado_NaoCnfDmnCod, BC001V18_A490ContagemResultado_ContratadaCod, BC001V18_n490ContagemResultado_ContratadaCod, BC001V18_A805ContagemResultado_ContratadaOrigemCod, BC001V18_n805ContagemResultado_ContratadaOrigemCod,
               BC001V18_A1553ContagemResultado_CntSrvCod, BC001V18_n1553ContagemResultado_CntSrvCod, BC001V18_A602ContagemResultado_OSVinculada, BC001V18_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC001V19_A127Sistema_Codigo
               }
               , new Object[] {
               BC001V20_A513Sistema_Coordenacao, BC001V20_n513Sistema_Coordenacao
               }
               , new Object[] {
               BC001V21_A52Contratada_AreaTrabalhoCod, BC001V21_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               BC001V22_A601ContagemResultado_Servico, BC001V22_n601ContagemResultado_Servico
               }
               , new Object[] {
               BC001V23_A999ContagemResultado_CrFMEhContratada, BC001V23_n999ContagemResultado_CrFMEhContratada, BC001V23_A1000ContagemResultado_CrFMEhContratante, BC001V23_n1000ContagemResultado_CrFMEhContratante, BC001V23_A479ContagemResultado_CrFMPessoaCod, BC001V23_n479ContagemResultado_CrFMPessoaCod
               }
               , new Object[] {
               BC001V24_A474ContagemResultado_ContadorFMNom, BC001V24_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               BC001V25_A478ContagemResultado_NaoCnfCntNom, BC001V25_n478ContagemResultado_NaoCnfCntNom
               }
               , new Object[] {
               BC001V26_A146Modulo_Codigo, BC001V26_n146Modulo_Codigo, BC001V26_A127Sistema_Codigo, BC001V26_A473ContagemResultado_DataCnt, BC001V26_A511ContagemResultado_HoraCnt, BC001V26_A482ContagemResultadoContagens_Esforco, BC001V26_A462ContagemResultado_Divergencia, BC001V26_A483ContagemResultado_StatusCnt, BC001V26_A485ContagemResultado_EhValidacao, BC001V26_n485ContagemResultado_EhValidacao,
               BC001V26_A513Sistema_Coordenacao, BC001V26_n513Sistema_Coordenacao, BC001V26_A457ContagemResultado_Demanda, BC001V26_n457ContagemResultado_Demanda, BC001V26_A484ContagemResultado_StatusDmn, BC001V26_n484ContagemResultado_StatusDmn, BC001V26_A481ContagemResultado_TimeCnt, BC001V26_n481ContagemResultado_TimeCnt, BC001V26_A901ContagemResultadoContagens_Prazo, BC001V26_n901ContagemResultadoContagens_Prazo,
               BC001V26_A458ContagemResultado_PFBFS, BC001V26_n458ContagemResultado_PFBFS, BC001V26_A459ContagemResultado_PFLFS, BC001V26_n459ContagemResultado_PFLFS, BC001V26_A460ContagemResultado_PFBFM, BC001V26_n460ContagemResultado_PFBFM, BC001V26_A461ContagemResultado_PFLFM, BC001V26_n461ContagemResultado_PFLFM, BC001V26_A463ContagemResultado_ParecerTcn, BC001V26_n463ContagemResultado_ParecerTcn,
               BC001V26_A474ContagemResultado_ContadorFMNom, BC001V26_n474ContagemResultado_ContadorFMNom, BC001V26_A999ContagemResultado_CrFMEhContratada, BC001V26_n999ContagemResultado_CrFMEhContratada, BC001V26_A1000ContagemResultado_CrFMEhContratante, BC001V26_n1000ContagemResultado_CrFMEhContratante, BC001V26_A478ContagemResultado_NaoCnfCntNom, BC001V26_n478ContagemResultado_NaoCnfCntNom, BC001V26_A517ContagemResultado_Ultima, BC001V26_A800ContagemResultado_Deflator,
               BC001V26_n800ContagemResultado_Deflator, BC001V26_A833ContagemResultado_CstUntPrd, BC001V26_n833ContagemResultado_CstUntPrd, BC001V26_A1389ContagemResultado_RdmnIssueId, BC001V26_n1389ContagemResultado_RdmnIssueId, BC001V26_A1756ContagemResultado_NvlCnt, BC001V26_n1756ContagemResultado_NvlCnt, BC001V26_A854ContagemResultado_TipoPla, BC001V26_n854ContagemResultado_TipoPla, BC001V26_A853ContagemResultado_NomePla,
               BC001V26_n853ContagemResultado_NomePla, BC001V26_A493ContagemResultado_DemandaFM, BC001V26_n493ContagemResultado_DemandaFM, BC001V26_A456ContagemResultado_Codigo, BC001V26_A470ContagemResultado_ContadorFMCod, BC001V26_A469ContagemResultado_NaoCnfCntCod, BC001V26_n469ContagemResultado_NaoCnfCntCod, BC001V26_A890ContagemResultado_Responsavel, BC001V26_n890ContagemResultado_Responsavel, BC001V26_A468ContagemResultado_NaoCnfDmnCod,
               BC001V26_n468ContagemResultado_NaoCnfDmnCod, BC001V26_A490ContagemResultado_ContratadaCod, BC001V26_n490ContagemResultado_ContratadaCod, BC001V26_A52Contratada_AreaTrabalhoCod, BC001V26_n52Contratada_AreaTrabalhoCod, BC001V26_A805ContagemResultado_ContratadaOrigemCod, BC001V26_n805ContagemResultado_ContratadaOrigemCod, BC001V26_A1553ContagemResultado_CntSrvCod, BC001V26_n1553ContagemResultado_CntSrvCod, BC001V26_A601ContagemResultado_Servico,
               BC001V26_n601ContagemResultado_Servico, BC001V26_A602ContagemResultado_OSVinculada, BC001V26_n602ContagemResultado_OSVinculada, BC001V26_A479ContagemResultado_CrFMPessoaCod, BC001V26_n479ContagemResultado_CrFMPessoaCod, BC001V26_A852ContagemResultado_Planilha, BC001V26_n852ContagemResultado_Planilha
               }
            }
         );
         Z517ContagemResultado_Ultima = true;
         A517ContagemResultado_Ultima = true;
         i517ContagemResultado_Ultima = true;
         Z482ContagemResultadoContagens_Esforco = 0;
         A482ContagemResultadoContagens_Esforco = 0;
         i482ContagemResultadoContagens_Esforco = 0;
         Z511ContagemResultado_HoraCnt = context.localUtil.Time( );
         A511ContagemResultado_HoraCnt = context.localUtil.Time( );
         AV37Pgmname = "ContagemResultadoContagens_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E121V2 */
         E121V2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z482ContagemResultadoContagens_Esforco ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short Z483ContagemResultado_StatusCnt ;
      private short A483ContagemResultado_StatusCnt ;
      private short Z1756ContagemResultado_NvlCnt ;
      private short A1756ContagemResultado_NvlCnt ;
      private short Gx_BScreen ;
      private short RcdFound72 ;
      private short O483ContagemResultado_StatusCnt ;
      private short i482ContagemResultadoContagens_Esforco ;
      private int trnEnded ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int AV38GXV1 ;
      private int AV10Insert_ContagemResultado_ContadorFMCod ;
      private int AV11Insert_ContagemResultado_NaoCnfCntCod ;
      private int AV7ContagemResultado_Codigo ;
      private int Z470ContagemResultado_ContadorFMCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int Z469ContagemResultado_NaoCnfCntCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int Z146Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int Z1389ContagemResultado_RdmnIssueId ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int Z890ContagemResultado_Responsavel ;
      private int A890ContagemResultado_Responsavel ;
      private int Z468ContagemResultado_NaoCnfDmnCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int Z805ContagemResultado_ContratadaOrigemCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int Z602ContagemResultado_OSVinculada ;
      private int A602ContagemResultado_OSVinculada ;
      private int Z479ContagemResultado_CrFMPessoaCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int Z601ContagemResultado_Servico ;
      private int A601ContagemResultado_Servico ;
      private decimal AV19IndiceDivergencia ;
      private decimal AV27ContagemResultado_Divergencia ;
      private decimal AV29ContagemResultado_ValorPF ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal Z462ContagemResultado_Divergencia ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal Z458ContagemResultado_PFBFS ;
      private decimal Z459ContagemResultado_PFLFS ;
      private decimal Z460ContagemResultado_PFBFM ;
      private decimal Z461ContagemResultado_PFLFM ;
      private decimal Z800ContagemResultado_Deflator ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal Z833ContagemResultado_CstUntPrd ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal GXt_decimal1 ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z511ContagemResultado_HoraCnt ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV37Pgmname ;
      private String AV35CalculoPFinal ;
      private String AV20CalculoDivergencia ;
      private String Z484ContagemResultado_StatusDmn ;
      private String A484ContagemResultado_StatusDmn ;
      private String Z478ContagemResultado_NaoCnfCntNom ;
      private String A478ContagemResultado_NaoCnfCntNom ;
      private String Z474ContagemResultado_ContadorFMNom ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String Z854ContagemResultado_TipoPla ;
      private String A854ContagemResultado_TipoPla ;
      private String Z853ContagemResultado_NomePla ;
      private String A853ContagemResultado_NomePla ;
      private String A852ContagemResultado_Planilha_Filetype ;
      private String A852ContagemResultado_Planilha_Filename ;
      private String sMode72 ;
      private String GXt_char2 ;
      private DateTime Z481ContagemResultado_TimeCnt ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime Z901ContagemResultadoContagens_Prazo ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime Z473ContagemResultado_DataCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool Z517ContagemResultado_Ultima ;
      private bool A517ContagemResultado_Ultima ;
      private bool Z485ContagemResultado_EhValidacao ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool Z999ContagemResultado_CrFMEhContratada ;
      private bool A999ContagemResultado_CrFMEhContratada ;
      private bool Z1000ContagemResultado_CrFMEhContratante ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool n146Modulo_Codigo ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n513Sistema_Coordenacao ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n999ContagemResultado_CrFMEhContratada ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool n478ContagemResultado_NaoCnfCntNom ;
      private bool n800ContagemResultado_Deflator ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n852ContagemResultado_Planilha ;
      private bool Gx_longc ;
      private bool i517ContagemResultado_Ultima ;
      private String Z463ContagemResultado_ParecerTcn ;
      private String A463ContagemResultado_ParecerTcn ;
      private String Z501ContagemResultado_OsFsOsFm ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String Z457ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private String Z513Sistema_Coordenacao ;
      private String A513Sistema_Coordenacao ;
      private String Z852ContagemResultado_Planilha ;
      private String A852ContagemResultado_Planilha ;
      private IGxSession AV14WebSession ;
      private SdtContagemResultadoContagens bcContagemResultadoContagens ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001V12_A146Modulo_Codigo ;
      private bool[] BC001V12_n146Modulo_Codigo ;
      private int[] BC001V12_A127Sistema_Codigo ;
      private DateTime[] BC001V12_A473ContagemResultado_DataCnt ;
      private String[] BC001V12_A511ContagemResultado_HoraCnt ;
      private short[] BC001V12_A482ContagemResultadoContagens_Esforco ;
      private decimal[] BC001V12_A462ContagemResultado_Divergencia ;
      private short[] BC001V12_A483ContagemResultado_StatusCnt ;
      private bool[] BC001V12_A485ContagemResultado_EhValidacao ;
      private bool[] BC001V12_n485ContagemResultado_EhValidacao ;
      private String[] BC001V12_A513Sistema_Coordenacao ;
      private bool[] BC001V12_n513Sistema_Coordenacao ;
      private String[] BC001V12_A457ContagemResultado_Demanda ;
      private bool[] BC001V12_n457ContagemResultado_Demanda ;
      private String[] BC001V12_A484ContagemResultado_StatusDmn ;
      private bool[] BC001V12_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC001V12_A481ContagemResultado_TimeCnt ;
      private bool[] BC001V12_n481ContagemResultado_TimeCnt ;
      private DateTime[] BC001V12_A901ContagemResultadoContagens_Prazo ;
      private bool[] BC001V12_n901ContagemResultadoContagens_Prazo ;
      private decimal[] BC001V12_A458ContagemResultado_PFBFS ;
      private bool[] BC001V12_n458ContagemResultado_PFBFS ;
      private decimal[] BC001V12_A459ContagemResultado_PFLFS ;
      private bool[] BC001V12_n459ContagemResultado_PFLFS ;
      private decimal[] BC001V12_A460ContagemResultado_PFBFM ;
      private bool[] BC001V12_n460ContagemResultado_PFBFM ;
      private decimal[] BC001V12_A461ContagemResultado_PFLFM ;
      private bool[] BC001V12_n461ContagemResultado_PFLFM ;
      private String[] BC001V12_A463ContagemResultado_ParecerTcn ;
      private bool[] BC001V12_n463ContagemResultado_ParecerTcn ;
      private String[] BC001V12_A474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V12_n474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V12_A999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V12_n999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V12_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] BC001V12_n1000ContagemResultado_CrFMEhContratante ;
      private String[] BC001V12_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V12_n478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V12_A517ContagemResultado_Ultima ;
      private decimal[] BC001V12_A800ContagemResultado_Deflator ;
      private bool[] BC001V12_n800ContagemResultado_Deflator ;
      private decimal[] BC001V12_A833ContagemResultado_CstUntPrd ;
      private bool[] BC001V12_n833ContagemResultado_CstUntPrd ;
      private int[] BC001V12_A1389ContagemResultado_RdmnIssueId ;
      private bool[] BC001V12_n1389ContagemResultado_RdmnIssueId ;
      private short[] BC001V12_A1756ContagemResultado_NvlCnt ;
      private bool[] BC001V12_n1756ContagemResultado_NvlCnt ;
      private String[] BC001V12_A854ContagemResultado_TipoPla ;
      private bool[] BC001V12_n854ContagemResultado_TipoPla ;
      private String[] BC001V12_A853ContagemResultado_NomePla ;
      private bool[] BC001V12_n853ContagemResultado_NomePla ;
      private String[] BC001V12_A493ContagemResultado_DemandaFM ;
      private bool[] BC001V12_n493ContagemResultado_DemandaFM ;
      private int[] BC001V12_A456ContagemResultado_Codigo ;
      private int[] BC001V12_A470ContagemResultado_ContadorFMCod ;
      private int[] BC001V12_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] BC001V12_n469ContagemResultado_NaoCnfCntCod ;
      private int[] BC001V12_A890ContagemResultado_Responsavel ;
      private bool[] BC001V12_n890ContagemResultado_Responsavel ;
      private int[] BC001V12_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC001V12_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC001V12_A490ContagemResultado_ContratadaCod ;
      private bool[] BC001V12_n490ContagemResultado_ContratadaCod ;
      private int[] BC001V12_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC001V12_n52Contratada_AreaTrabalhoCod ;
      private int[] BC001V12_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC001V12_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC001V12_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC001V12_n1553ContagemResultado_CntSrvCod ;
      private int[] BC001V12_A601ContagemResultado_Servico ;
      private bool[] BC001V12_n601ContagemResultado_Servico ;
      private int[] BC001V12_A602ContagemResultado_OSVinculada ;
      private bool[] BC001V12_n602ContagemResultado_OSVinculada ;
      private int[] BC001V12_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] BC001V12_n479ContagemResultado_CrFMPessoaCod ;
      private String[] BC001V12_A852ContagemResultado_Planilha ;
      private bool[] BC001V12_n852ContagemResultado_Planilha ;
      private int[] BC001V4_A146Modulo_Codigo ;
      private bool[] BC001V4_n146Modulo_Codigo ;
      private bool[] BC001V4_A485ContagemResultado_EhValidacao ;
      private bool[] BC001V4_n485ContagemResultado_EhValidacao ;
      private String[] BC001V4_A457ContagemResultado_Demanda ;
      private bool[] BC001V4_n457ContagemResultado_Demanda ;
      private String[] BC001V4_A484ContagemResultado_StatusDmn ;
      private bool[] BC001V4_n484ContagemResultado_StatusDmn ;
      private int[] BC001V4_A1389ContagemResultado_RdmnIssueId ;
      private bool[] BC001V4_n1389ContagemResultado_RdmnIssueId ;
      private String[] BC001V4_A493ContagemResultado_DemandaFM ;
      private bool[] BC001V4_n493ContagemResultado_DemandaFM ;
      private int[] BC001V4_A890ContagemResultado_Responsavel ;
      private bool[] BC001V4_n890ContagemResultado_Responsavel ;
      private int[] BC001V4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC001V4_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC001V4_A490ContagemResultado_ContratadaCod ;
      private bool[] BC001V4_n490ContagemResultado_ContratadaCod ;
      private int[] BC001V4_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC001V4_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC001V4_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC001V4_n1553ContagemResultado_CntSrvCod ;
      private int[] BC001V4_A602ContagemResultado_OSVinculada ;
      private bool[] BC001V4_n602ContagemResultado_OSVinculada ;
      private int[] BC001V7_A127Sistema_Codigo ;
      private String[] BC001V8_A513Sistema_Coordenacao ;
      private bool[] BC001V8_n513Sistema_Coordenacao ;
      private int[] BC001V9_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC001V9_n52Contratada_AreaTrabalhoCod ;
      private int[] BC001V10_A601ContagemResultado_Servico ;
      private bool[] BC001V10_n601ContagemResultado_Servico ;
      private bool[] BC001V5_A999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V5_n999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V5_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] BC001V5_n1000ContagemResultado_CrFMEhContratante ;
      private int[] BC001V5_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] BC001V5_n479ContagemResultado_CrFMPessoaCod ;
      private String[] BC001V11_A474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V11_n474ContagemResultado_ContadorFMNom ;
      private String[] BC001V6_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V6_n478ContagemResultado_NaoCnfCntNom ;
      private int[] BC001V13_A456ContagemResultado_Codigo ;
      private DateTime[] BC001V13_A473ContagemResultado_DataCnt ;
      private String[] BC001V13_A511ContagemResultado_HoraCnt ;
      private DateTime[] BC001V3_A473ContagemResultado_DataCnt ;
      private String[] BC001V3_A511ContagemResultado_HoraCnt ;
      private short[] BC001V3_A482ContagemResultadoContagens_Esforco ;
      private decimal[] BC001V3_A462ContagemResultado_Divergencia ;
      private short[] BC001V3_A483ContagemResultado_StatusCnt ;
      private DateTime[] BC001V3_A481ContagemResultado_TimeCnt ;
      private bool[] BC001V3_n481ContagemResultado_TimeCnt ;
      private DateTime[] BC001V3_A901ContagemResultadoContagens_Prazo ;
      private bool[] BC001V3_n901ContagemResultadoContagens_Prazo ;
      private decimal[] BC001V3_A458ContagemResultado_PFBFS ;
      private bool[] BC001V3_n458ContagemResultado_PFBFS ;
      private decimal[] BC001V3_A459ContagemResultado_PFLFS ;
      private bool[] BC001V3_n459ContagemResultado_PFLFS ;
      private decimal[] BC001V3_A460ContagemResultado_PFBFM ;
      private bool[] BC001V3_n460ContagemResultado_PFBFM ;
      private decimal[] BC001V3_A461ContagemResultado_PFLFM ;
      private bool[] BC001V3_n461ContagemResultado_PFLFM ;
      private String[] BC001V3_A463ContagemResultado_ParecerTcn ;
      private bool[] BC001V3_n463ContagemResultado_ParecerTcn ;
      private bool[] BC001V3_A517ContagemResultado_Ultima ;
      private decimal[] BC001V3_A800ContagemResultado_Deflator ;
      private bool[] BC001V3_n800ContagemResultado_Deflator ;
      private decimal[] BC001V3_A833ContagemResultado_CstUntPrd ;
      private bool[] BC001V3_n833ContagemResultado_CstUntPrd ;
      private short[] BC001V3_A1756ContagemResultado_NvlCnt ;
      private bool[] BC001V3_n1756ContagemResultado_NvlCnt ;
      private String[] BC001V3_A854ContagemResultado_TipoPla ;
      private bool[] BC001V3_n854ContagemResultado_TipoPla ;
      private String[] BC001V3_A853ContagemResultado_NomePla ;
      private bool[] BC001V3_n853ContagemResultado_NomePla ;
      private int[] BC001V3_A456ContagemResultado_Codigo ;
      private int[] BC001V3_A470ContagemResultado_ContadorFMCod ;
      private int[] BC001V3_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] BC001V3_n469ContagemResultado_NaoCnfCntCod ;
      private String[] BC001V3_A852ContagemResultado_Planilha ;
      private bool[] BC001V3_n852ContagemResultado_Planilha ;
      private DateTime[] BC001V2_A473ContagemResultado_DataCnt ;
      private String[] BC001V2_A511ContagemResultado_HoraCnt ;
      private short[] BC001V2_A482ContagemResultadoContagens_Esforco ;
      private decimal[] BC001V2_A462ContagemResultado_Divergencia ;
      private short[] BC001V2_A483ContagemResultado_StatusCnt ;
      private DateTime[] BC001V2_A481ContagemResultado_TimeCnt ;
      private bool[] BC001V2_n481ContagemResultado_TimeCnt ;
      private DateTime[] BC001V2_A901ContagemResultadoContagens_Prazo ;
      private bool[] BC001V2_n901ContagemResultadoContagens_Prazo ;
      private decimal[] BC001V2_A458ContagemResultado_PFBFS ;
      private bool[] BC001V2_n458ContagemResultado_PFBFS ;
      private decimal[] BC001V2_A459ContagemResultado_PFLFS ;
      private bool[] BC001V2_n459ContagemResultado_PFLFS ;
      private decimal[] BC001V2_A460ContagemResultado_PFBFM ;
      private bool[] BC001V2_n460ContagemResultado_PFBFM ;
      private decimal[] BC001V2_A461ContagemResultado_PFLFM ;
      private bool[] BC001V2_n461ContagemResultado_PFLFM ;
      private String[] BC001V2_A463ContagemResultado_ParecerTcn ;
      private bool[] BC001V2_n463ContagemResultado_ParecerTcn ;
      private bool[] BC001V2_A517ContagemResultado_Ultima ;
      private decimal[] BC001V2_A800ContagemResultado_Deflator ;
      private bool[] BC001V2_n800ContagemResultado_Deflator ;
      private decimal[] BC001V2_A833ContagemResultado_CstUntPrd ;
      private bool[] BC001V2_n833ContagemResultado_CstUntPrd ;
      private short[] BC001V2_A1756ContagemResultado_NvlCnt ;
      private bool[] BC001V2_n1756ContagemResultado_NvlCnt ;
      private String[] BC001V2_A854ContagemResultado_TipoPla ;
      private bool[] BC001V2_n854ContagemResultado_TipoPla ;
      private String[] BC001V2_A853ContagemResultado_NomePla ;
      private bool[] BC001V2_n853ContagemResultado_NomePla ;
      private int[] BC001V2_A456ContagemResultado_Codigo ;
      private int[] BC001V2_A470ContagemResultado_ContadorFMCod ;
      private int[] BC001V2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] BC001V2_n469ContagemResultado_NaoCnfCntCod ;
      private String[] BC001V2_A852ContagemResultado_Planilha ;
      private bool[] BC001V2_n852ContagemResultado_Planilha ;
      private int[] BC001V18_A146Modulo_Codigo ;
      private bool[] BC001V18_n146Modulo_Codigo ;
      private bool[] BC001V18_A485ContagemResultado_EhValidacao ;
      private bool[] BC001V18_n485ContagemResultado_EhValidacao ;
      private String[] BC001V18_A457ContagemResultado_Demanda ;
      private bool[] BC001V18_n457ContagemResultado_Demanda ;
      private String[] BC001V18_A484ContagemResultado_StatusDmn ;
      private bool[] BC001V18_n484ContagemResultado_StatusDmn ;
      private int[] BC001V18_A1389ContagemResultado_RdmnIssueId ;
      private bool[] BC001V18_n1389ContagemResultado_RdmnIssueId ;
      private String[] BC001V18_A493ContagemResultado_DemandaFM ;
      private bool[] BC001V18_n493ContagemResultado_DemandaFM ;
      private int[] BC001V18_A890ContagemResultado_Responsavel ;
      private bool[] BC001V18_n890ContagemResultado_Responsavel ;
      private int[] BC001V18_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC001V18_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC001V18_A490ContagemResultado_ContratadaCod ;
      private bool[] BC001V18_n490ContagemResultado_ContratadaCod ;
      private int[] BC001V18_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC001V18_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC001V18_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC001V18_n1553ContagemResultado_CntSrvCod ;
      private int[] BC001V18_A602ContagemResultado_OSVinculada ;
      private bool[] BC001V18_n602ContagemResultado_OSVinculada ;
      private int[] BC001V19_A127Sistema_Codigo ;
      private String[] BC001V20_A513Sistema_Coordenacao ;
      private bool[] BC001V20_n513Sistema_Coordenacao ;
      private int[] BC001V21_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC001V21_n52Contratada_AreaTrabalhoCod ;
      private int[] BC001V22_A601ContagemResultado_Servico ;
      private bool[] BC001V22_n601ContagemResultado_Servico ;
      private bool[] BC001V23_A999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V23_n999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V23_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] BC001V23_n1000ContagemResultado_CrFMEhContratante ;
      private int[] BC001V23_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] BC001V23_n479ContagemResultado_CrFMPessoaCod ;
      private String[] BC001V24_A474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V24_n474ContagemResultado_ContadorFMNom ;
      private String[] BC001V25_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V25_n478ContagemResultado_NaoCnfCntNom ;
      private int[] BC001V26_A146Modulo_Codigo ;
      private bool[] BC001V26_n146Modulo_Codigo ;
      private int[] BC001V26_A127Sistema_Codigo ;
      private DateTime[] BC001V26_A473ContagemResultado_DataCnt ;
      private String[] BC001V26_A511ContagemResultado_HoraCnt ;
      private short[] BC001V26_A482ContagemResultadoContagens_Esforco ;
      private decimal[] BC001V26_A462ContagemResultado_Divergencia ;
      private short[] BC001V26_A483ContagemResultado_StatusCnt ;
      private bool[] BC001V26_A485ContagemResultado_EhValidacao ;
      private bool[] BC001V26_n485ContagemResultado_EhValidacao ;
      private String[] BC001V26_A513Sistema_Coordenacao ;
      private bool[] BC001V26_n513Sistema_Coordenacao ;
      private String[] BC001V26_A457ContagemResultado_Demanda ;
      private bool[] BC001V26_n457ContagemResultado_Demanda ;
      private String[] BC001V26_A484ContagemResultado_StatusDmn ;
      private bool[] BC001V26_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC001V26_A481ContagemResultado_TimeCnt ;
      private bool[] BC001V26_n481ContagemResultado_TimeCnt ;
      private DateTime[] BC001V26_A901ContagemResultadoContagens_Prazo ;
      private bool[] BC001V26_n901ContagemResultadoContagens_Prazo ;
      private decimal[] BC001V26_A458ContagemResultado_PFBFS ;
      private bool[] BC001V26_n458ContagemResultado_PFBFS ;
      private decimal[] BC001V26_A459ContagemResultado_PFLFS ;
      private bool[] BC001V26_n459ContagemResultado_PFLFS ;
      private decimal[] BC001V26_A460ContagemResultado_PFBFM ;
      private bool[] BC001V26_n460ContagemResultado_PFBFM ;
      private decimal[] BC001V26_A461ContagemResultado_PFLFM ;
      private bool[] BC001V26_n461ContagemResultado_PFLFM ;
      private String[] BC001V26_A463ContagemResultado_ParecerTcn ;
      private bool[] BC001V26_n463ContagemResultado_ParecerTcn ;
      private String[] BC001V26_A474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V26_n474ContagemResultado_ContadorFMNom ;
      private bool[] BC001V26_A999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V26_n999ContagemResultado_CrFMEhContratada ;
      private bool[] BC001V26_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] BC001V26_n1000ContagemResultado_CrFMEhContratante ;
      private String[] BC001V26_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V26_n478ContagemResultado_NaoCnfCntNom ;
      private bool[] BC001V26_A517ContagemResultado_Ultima ;
      private decimal[] BC001V26_A800ContagemResultado_Deflator ;
      private bool[] BC001V26_n800ContagemResultado_Deflator ;
      private decimal[] BC001V26_A833ContagemResultado_CstUntPrd ;
      private bool[] BC001V26_n833ContagemResultado_CstUntPrd ;
      private int[] BC001V26_A1389ContagemResultado_RdmnIssueId ;
      private bool[] BC001V26_n1389ContagemResultado_RdmnIssueId ;
      private short[] BC001V26_A1756ContagemResultado_NvlCnt ;
      private bool[] BC001V26_n1756ContagemResultado_NvlCnt ;
      private String[] BC001V26_A854ContagemResultado_TipoPla ;
      private bool[] BC001V26_n854ContagemResultado_TipoPla ;
      private String[] BC001V26_A853ContagemResultado_NomePla ;
      private bool[] BC001V26_n853ContagemResultado_NomePla ;
      private String[] BC001V26_A493ContagemResultado_DemandaFM ;
      private bool[] BC001V26_n493ContagemResultado_DemandaFM ;
      private int[] BC001V26_A456ContagemResultado_Codigo ;
      private int[] BC001V26_A470ContagemResultado_ContadorFMCod ;
      private int[] BC001V26_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] BC001V26_n469ContagemResultado_NaoCnfCntCod ;
      private int[] BC001V26_A890ContagemResultado_Responsavel ;
      private bool[] BC001V26_n890ContagemResultado_Responsavel ;
      private int[] BC001V26_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC001V26_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC001V26_A490ContagemResultado_ContratadaCod ;
      private bool[] BC001V26_n490ContagemResultado_ContratadaCod ;
      private int[] BC001V26_A52Contratada_AreaTrabalhoCod ;
      private bool[] BC001V26_n52Contratada_AreaTrabalhoCod ;
      private int[] BC001V26_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC001V26_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC001V26_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC001V26_n1553ContagemResultado_CntSrvCod ;
      private int[] BC001V26_A601ContagemResultado_Servico ;
      private bool[] BC001V26_n601ContagemResultado_Servico ;
      private int[] BC001V26_A602ContagemResultado_OSVinculada ;
      private bool[] BC001V26_n602ContagemResultado_OSVinculada ;
      private int[] BC001V26_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] BC001V26_n479ContagemResultado_CrFMPessoaCod ;
      private String[] BC001V26_A852ContagemResultado_Planilha ;
      private bool[] BC001V26_n852ContagemResultado_Planilha ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV12TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class contagemresultadocontagens_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001V12 ;
          prmBC001V12 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC001V12 ;
          cmdBufferBC001V12=" SELECT T2.[Modulo_Codigo], T3.[Sistema_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt], TM1.[ContagemResultadoContagens_Esforco], TM1.[ContagemResultado_Divergencia], TM1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_EhValidacao], T4.[Sistema_Coordenacao], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_TimeCnt], TM1.[ContagemResultadoContagens_Prazo], TM1.[ContagemResultado_PFBFS], TM1.[ContagemResultado_PFLFS], TM1.[ContagemResultado_PFBFM], TM1.[ContagemResultado_PFLFM], TM1.[ContagemResultado_ParecerTcn], T8.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T7.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, T7.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T9.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom, TM1.[ContagemResultado_Ultima], TM1.[ContagemResultado_Deflator], TM1.[ContagemResultado_CstUntPrd], T2.[ContagemResultado_RdmnIssueId], TM1.[ContagemResultado_NvlCnt], TM1.[ContagemResultado_TipoPla], TM1.[ContagemResultado_NomePla], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, TM1.[ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaOrigemCod], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_OSVinculada], T7.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, TM1.[ContagemResultado_Planilha] FROM (((((((([ContagemResultadoContagens] TM1 WITH (NOLOCK) "
          + " INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Modulo_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[Sistema_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) INNER JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) LEFT JOIN [NaoConformidade] T9 WITH (NOLOCK) ON T9.[NaoConformidade_Codigo] = TM1.[ContagemResultado_NaoCnfCntCod]) WHERE TM1.[ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and TM1.[ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt and TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt]  OPTION (FAST 100)" ;
          Object[] prmBC001V4 ;
          prmBC001V4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V7 ;
          prmBC001V7 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V8 ;
          prmBC001V8 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V9 ;
          prmBC001V9 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V10 ;
          prmBC001V10 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V5 ;
          prmBC001V5 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V11 ;
          prmBC001V11 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V6 ;
          prmBC001V6 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V13 ;
          prmBC001V13 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V3 ;
          prmBC001V3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V2 ;
          prmBC001V2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V14 ;
          prmBC001V14 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V15 ;
          prmBC001V15 = new Object[] {
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V16 ;
          prmBC001V16 = new Object[] {
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V17 ;
          prmBC001V17 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmBC001V23 ;
          prmBC001V23 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V24 ;
          prmBC001V24 = new Object[] {
          new Object[] {"@ContagemResultado_CrFMPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V25 ;
          prmBC001V25 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V26 ;
          prmBC001V26 = new Object[] {
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC001V26 ;
          cmdBufferBC001V26=" SELECT T2.[Modulo_Codigo], T3.[Sistema_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt], TM1.[ContagemResultadoContagens_Esforco], TM1.[ContagemResultado_Divergencia], TM1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_EhValidacao], T4.[Sistema_Coordenacao], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_TimeCnt], TM1.[ContagemResultadoContagens_Prazo], TM1.[ContagemResultado_PFBFS], TM1.[ContagemResultado_PFLFS], TM1.[ContagemResultado_PFBFM], TM1.[ContagemResultado_PFLFM], TM1.[ContagemResultado_ParecerTcn], T8.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T7.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, T7.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T9.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom, TM1.[ContagemResultado_Ultima], TM1.[ContagemResultado_Deflator], TM1.[ContagemResultado_CstUntPrd], T2.[ContagemResultado_RdmnIssueId], TM1.[ContagemResultado_NvlCnt], TM1.[ContagemResultado_TipoPla], TM1.[ContagemResultado_NomePla], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, TM1.[ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaOrigemCod], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_OSVinculada], T7.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, TM1.[ContagemResultado_Planilha] FROM (((((((([ContagemResultadoContagens] TM1 WITH (NOLOCK) "
          + " INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Modulo_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[Sistema_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) INNER JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) LEFT JOIN [NaoConformidade] T9 WITH (NOLOCK) ON T9.[NaoConformidade_Codigo] = TM1.[ContagemResultado_NaoCnfCntCod]) WHERE TM1.[ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and TM1.[ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt and TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_DataCnt], TM1.[ContagemResultado_HoraCnt]  OPTION (FAST 100)" ;
          Object[] prmBC001V18 ;
          prmBC001V18 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V19 ;
          prmBC001V19 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V20 ;
          prmBC001V20 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V21 ;
          prmBC001V21 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001V22 ;
          prmBC001V22 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001V2", "SELECT [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V2,1,0,true,false )
             ,new CursorDef("BC001V3", "SELECT [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V3,1,0,true,false )
             ,new CursorDef("BC001V4", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V4,1,0,true,false )
             ,new CursorDef("BC001V5", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V5,1,0,true,false )
             ,new CursorDef("BC001V6", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V6,1,0,true,false )
             ,new CursorDef("BC001V7", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V7,1,0,true,false )
             ,new CursorDef("BC001V8", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V8,1,0,true,false )
             ,new CursorDef("BC001V9", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V9,1,0,true,false )
             ,new CursorDef("BC001V10", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V10,1,0,true,false )
             ,new CursorDef("BC001V11", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V11,1,0,true,false )
             ,new CursorDef("BC001V12", cmdBufferBC001V12,true, GxErrorMask.GX_NOMASK, false, this,prmBC001V12,100,0,true,false )
             ,new CursorDef("BC001V13", "SELECT [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V13,1,0,true,false )
             ,new CursorDef("BC001V14", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Divergencia], [ContagemResultado_StatusCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ParecerTcn], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NvlCnt], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_Codigo], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod]) VALUES(@ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Divergencia, @ContagemResultado_StatusCnt, @ContagemResultado_TimeCnt, @ContagemResultadoContagens_Prazo, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_ParecerTcn, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NvlCnt, @ContagemResultado_TipoPla, @ContagemResultado_NomePla, @ContagemResultado_Codigo, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod)", GxErrorMask.GX_NOMASK,prmBC001V14)
             ,new CursorDef("BC001V15", "UPDATE [ContagemResultadoContagens] SET [ContagemResultadoContagens_Esforco]=@ContagemResultadoContagens_Esforco, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_TimeCnt]=@ContagemResultado_TimeCnt, [ContagemResultadoContagens_Prazo]=@ContagemResultadoContagens_Prazo, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_ParecerTcn]=@ContagemResultado_ParecerTcn, [ContagemResultado_Ultima]=@ContagemResultado_Ultima, [ContagemResultado_Deflator]=@ContagemResultado_Deflator, [ContagemResultado_CstUntPrd]=@ContagemResultado_CstUntPrd, [ContagemResultado_NvlCnt]=@ContagemResultado_NvlCnt, [ContagemResultado_TipoPla]=@ContagemResultado_TipoPla, [ContagemResultado_NomePla]=@ContagemResultado_NomePla, [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod, [ContagemResultado_NaoCnfCntCod]=@ContagemResultado_NaoCnfCntCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmBC001V15)
             ,new CursorDef("BC001V16", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Planilha]=@ContagemResultado_Planilha  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmBC001V16)
             ,new CursorDef("BC001V17", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK,prmBC001V17)
             ,new CursorDef("BC001V18", "SELECT [Modulo_Codigo], [ContagemResultado_EhValidacao], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_RdmnIssueId], [ContagemResultado_DemandaFM], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V18,1,0,true,false )
             ,new CursorDef("BC001V19", "SELECT [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V19,1,0,true,false )
             ,new CursorDef("BC001V20", "SELECT [Sistema_Coordenacao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V20,1,0,true,false )
             ,new CursorDef("BC001V21", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V21,1,0,true,false )
             ,new CursorDef("BC001V22", "SELECT [Servico_Codigo] AS ContagemResultado_Servico FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V22,1,0,true,false )
             ,new CursorDef("BC001V23", "SELECT [Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, [Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, [Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V23,1,0,true,false )
             ,new CursorDef("BC001V24", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFMPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V24,1,0,true,false )
             ,new CursorDef("BC001V25", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001V25,1,0,true,false )
             ,new CursorDef("BC001V26", cmdBufferBC001V26,true, GxErrorMask.GX_NOMASK, false, this,prmBC001V26,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((short[]) buf[24])[0] = rslt.getShort(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((short[]) buf[24])[0] = rslt.getShort(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((String[]) buf[30])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((bool[]) buf[32])[0] = rslt.getBool(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((decimal[]) buf[39])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(24);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((int[]) buf[43])[0] = rslt.getInt(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((short[]) buf[45])[0] = rslt.getShort(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((String[]) buf[47])[0] = rslt.getString(28, 10) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 50) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((String[]) buf[51])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((int[]) buf[53])[0] = rslt.getInt(31) ;
                ((int[]) buf[54])[0] = rslt.getInt(32) ;
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((int[]) buf[59])[0] = rslt.getInt(35) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(35);
                ((int[]) buf[61])[0] = rslt.getInt(36) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(36);
                ((int[]) buf[63])[0] = rslt.getInt(37) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(37);
                ((int[]) buf[65])[0] = rslt.getInt(38) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(38);
                ((int[]) buf[67])[0] = rslt.getInt(39) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(39);
                ((int[]) buf[69])[0] = rslt.getInt(40) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(40);
                ((int[]) buf[71])[0] = rslt.getInt(41) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(41);
                ((int[]) buf[73])[0] = rslt.getInt(42) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(42);
                ((String[]) buf[75])[0] = rslt.getBLOBFile(43, rslt.getString(28, 10), rslt.getString(29, 50)) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(43);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((String[]) buf[28])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((String[]) buf[30])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((bool[]) buf[32])[0] = rslt.getBool(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((decimal[]) buf[39])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(24);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((int[]) buf[43])[0] = rslt.getInt(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((short[]) buf[45])[0] = rslt.getShort(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((String[]) buf[47])[0] = rslt.getString(28, 10) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 50) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((String[]) buf[51])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((int[]) buf[53])[0] = rslt.getInt(31) ;
                ((int[]) buf[54])[0] = rslt.getInt(32) ;
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((int[]) buf[59])[0] = rslt.getInt(35) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(35);
                ((int[]) buf[61])[0] = rslt.getInt(36) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(36);
                ((int[]) buf[63])[0] = rslt.getInt(37) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(37);
                ((int[]) buf[65])[0] = rslt.getInt(38) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(38);
                ((int[]) buf[67])[0] = rslt.getInt(39) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(39);
                ((int[]) buf[69])[0] = rslt.getInt(40) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(40);
                ((int[]) buf[71])[0] = rslt.getInt(41) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(41);
                ((int[]) buf[73])[0] = rslt.getInt(42) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(42);
                ((String[]) buf[75])[0] = rslt.getBLOBFile(43, rslt.getString(28, 10), rslt.getString(29, 50)) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(43);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[18]);
                }
                stmt.SetParameter(13, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 17 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(17, (short)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[31]);
                }
                stmt.SetParameter(20, (int)parms[32]);
                stmt.SetParameter(21, (int)parms[33]);
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                stmt.SetParameter(11, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                stmt.SetParameter(17, (int)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[30]);
                }
                stmt.SetParameter(19, (int)parms[31]);
                stmt.SetParameter(20, (DateTime)parms[32]);
                stmt.SetParameter(21, (String)parms[33]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
