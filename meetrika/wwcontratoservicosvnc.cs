/*
               File: WWContratoServicosVnc
        Description:  Regra de Vinculo entre Servi�os
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 1:2:39.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoservicosvnc : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoservicosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoservicosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbContratoSrvVnc_StatusDmn = new GXCombobox();
         cmbContratoServicosVnc_ClonarLink = new GXCombobox();
         cmbContratoSrvVnc_SemCusto = new GXCombobox();
         chkContratoServicosVnc_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_35 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_35_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_35_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV21TFContratoSrvVnc_ServicoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoSrvVnc_ServicoSigla", AV21TFContratoSrvVnc_ServicoSigla);
               AV22TFContratoSrvVnc_ServicoSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoSigla_Sel", AV22TFContratoSrvVnc_ServicoSigla_Sel);
               AV54TFContratoServicosVnc_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosVnc_Descricao", AV54TFContratoServicosVnc_Descricao);
               AV55TFContratoServicosVnc_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosVnc_Descricao_Sel", AV55TFContratoServicosVnc_Descricao_Sel);
               AV29TFContratoSrvVnc_ServicoVncSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoSrvVnc_ServicoVncSigla", AV29TFContratoSrvVnc_ServicoVncSigla);
               AV30TFContratoSrvVnc_ServicoVncSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30TFContratoSrvVnc_ServicoVncSigla_Sel", AV30TFContratoSrvVnc_ServicoVncSigla_Sel);
               AV44TFContratoSrvVnc_SrvVncCntNum = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoSrvVnc_SrvVncCntNum", AV44TFContratoSrvVnc_SrvVncCntNum);
               AV45TFContratoSrvVnc_SrvVncCntNum_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoSrvVnc_SrvVncCntNum_Sel", AV45TFContratoSrvVnc_SrvVncCntNum_Sel);
               AV48TFContratoServicosVnc_ClonarLink_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
               AV37TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
               AV51TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
               AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
               AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
               AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
               AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
               AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace", AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace);
               AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
               AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace", AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace);
               AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
               AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV26TFContratoSrvVnc_StatusDmn_Sels);
               AV33TFContratoSrvVnc_PrestadoraPesNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoSrvVnc_PrestadoraPesNom", AV33TFContratoSrvVnc_PrestadoraPesNom);
               AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoSrvVnc_PrestadoraPesNom_Sel", AV34TFContratoSrvVnc_PrestadoraPesNom_Sel);
               AV75Pgmname = GetNextPar( );
               A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV17ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoSrvVnc_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoSrvVnc_CntSrvCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAGC2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTGC2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312123929");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoservicosvnc.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA", StringUtil.RTrim( AV21TFContratoSrvVnc_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL", StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO", AV54TFContratoServicosVnc_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL", AV55TFContratoServicosVnc_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA", StringUtil.RTrim( AV29TFContratoSrvVnc_ServicoVncSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL", StringUtil.RTrim( AV30TFContratoSrvVnc_ServicoVncSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM", StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL", StringUtil.RTrim( AV45TFContratoSrvVnc_SrvVncCntNum_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_35", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_35), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV39DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA", AV20ContratoSrvVnc_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA", AV20ContratoSrvVnc_ServicoSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA", AV53ContratoServicosVnc_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA", AV53ContratoServicosVnc_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV24ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV24ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_PRESTADORAPESNOMTITLEFILTERDATA", AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_PRESTADORAPESNOMTITLEFILTERDATA", AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA", AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA", AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSVNC_CLONARLINKTITLEFILTERDATA", AV47ContratoServicosVnc_ClonarLinkTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSVNC_CLONARLINKTITLEFILTERDATA", AV47ContratoServicosVnc_ClonarLinkTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA", AV36ContratoSrvVnc_SemCustoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA", AV36ContratoSrvVnc_SemCustoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA", AV50ContratoServicosVnc_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA", AV50ContratoServicosVnc_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV26TFContratoSrvVnc_StatusDmn_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV26TFContratoSrvVnc_StatusDmn_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV75Pgmname));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosvnc_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_prestadorapesnom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_prestadorapesnom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_prestadorapesnom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_prestadorapesnom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_prestadorapesnom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_prestadorapesnom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_srvvnccntnum_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Caption", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Tooltip", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Cls", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_clonarlink_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_clonarlink_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosvnc_clonarlink_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosvnc_clonarlink_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortasc", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_semcusto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Caption", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Cls", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosvnc_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosvnc_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_semcusto_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosvnc_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEGC2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTGC2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoservicosvnc.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoServicosVnc" ;
      }

      public override String GetPgmdesc( )
      {
         return " Regra de Vinculo entre Servi�os" ;
      }

      protected void WBGC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_GC2( true) ;
         }
         else
         {
            wb_table1_2_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicosigla_Internalname, StringUtil.RTrim( AV21TFContratoSrvVnc_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV21TFContratoSrvVnc_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicosigla_sel_Internalname, StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV22TFContratoSrvVnc_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_descricao_Internalname, AV54TFContratoServicosVnc_Descricao, StringUtil.RTrim( context.localUtil.Format( AV54TFContratoServicosVnc_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_descricao_sel_Internalname, AV55TFContratoServicosVnc_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV55TFContratoServicosVnc_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_Internalname, StringUtil.RTrim( AV29TFContratoSrvVnc_ServicoVncSigla), StringUtil.RTrim( context.localUtil.Format( AV29TFContratoSrvVnc_ServicoVncSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, StringUtil.RTrim( AV30TFContratoSrvVnc_ServicoVncSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV30TFContratoSrvVnc_ServicoVncSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_prestadorapesnom_Internalname, StringUtil.RTrim( AV33TFContratoSrvVnc_PrestadoraPesNom), StringUtil.RTrim( context.localUtil.Format( AV33TFContratoSrvVnc_PrestadoraPesNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_prestadorapesnom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_prestadorapesnom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_prestadorapesnom_sel_Internalname, StringUtil.RTrim( AV34TFContratoSrvVnc_PrestadoraPesNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_prestadorapesnom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_prestadorapesnom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_srvvnccntnum_Internalname, StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum), StringUtil.RTrim( context.localUtil.Format( AV44TFContratoSrvVnc_SrvVncCntNum, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname, StringUtil.RTrim( AV45TFContratoSrvVnc_SrvVncCntNum_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFContratoSrvVnc_SrvVncCntNum_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_clonarlink_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_clonarlink_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_clonarlink_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_semcusto_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_semcusto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_semcusto_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosvnc_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosvnc_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosvnc_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSVNC_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", 0, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_STATUSDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_PRESTADORAPESNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Internalname, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SRVVNCCNTNUMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSVNC_CLONARLINKContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Internalname, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SEMCUSTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSVNC_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosVnc.htm");
         }
         wbLoad = true;
      }

      protected void STARTGC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Regra de Vinculo entre Servi�os", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGC0( ) ;
      }

      protected void WSGC2( )
      {
         STARTGC2( ) ;
         EVTGC2( ) ;
      }

      protected void EVTGC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11GC2 */
                              E11GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12GC2 */
                              E12GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSVNC_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13GC2 */
                              E13GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14GC2 */
                              E14GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15GC2 */
                              E15GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16GC2 */
                              E16GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17GC2 */
                              E17GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSVNC_CLONARLINK.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18GC2 */
                              E18GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SEMCUSTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19GC2 */
                              E19GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSVNC_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20GC2 */
                              E20GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21GC2 */
                              E21GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22GC2 */
                              E22GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23GC2 */
                              E23GC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_35_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
                              SubsflControlProps_352( ) ;
                              AV14Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV73Update_GXI : context.convertURL( context.PathToRelativeUrl( AV14Update))));
                              AV15Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV74Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV15Delete))));
                              A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_Codigo_Internalname), ",", "."));
                              A922ContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoSigla_Internalname));
                              n922ContratoSrvVnc_ServicoSigla = false;
                              A2108ContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtContratoServicosVnc_Descricao_Internalname));
                              n2108ContratoServicosVnc_Descricao = false;
                              cmbContratoSrvVnc_StatusDmn.Name = cmbContratoSrvVnc_StatusDmn_Internalname;
                              cmbContratoSrvVnc_StatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                              A1084ContratoSrvVnc_StatusDmn = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                              n1084ContratoSrvVnc_StatusDmn = false;
                              A924ContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoVncSigla_Internalname));
                              n924ContratoSrvVnc_ServicoVncSigla = false;
                              A1092ContratoSrvVnc_PrestadoraPesNom = StringUtil.Upper( cgiGet( edtContratoSrvVnc_PrestadoraPesNom_Internalname));
                              n1092ContratoSrvVnc_PrestadoraPesNom = false;
                              A1631ContratoSrvVnc_SrvVncCntNum = cgiGet( edtContratoSrvVnc_SrvVncCntNum_Internalname);
                              n1631ContratoSrvVnc_SrvVncCntNum = false;
                              cmbContratoServicosVnc_ClonarLink.Name = cmbContratoServicosVnc_ClonarLink_Internalname;
                              cmbContratoServicosVnc_ClonarLink.CurrentValue = cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname);
                              A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname));
                              n1818ContratoServicosVnc_ClonarLink = false;
                              cmbContratoSrvVnc_SemCusto.Name = cmbContratoSrvVnc_SemCusto_Internalname;
                              cmbContratoSrvVnc_SemCusto.CurrentValue = cgiGet( cmbContratoSrvVnc_SemCusto_Internalname);
                              A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cgiGet( cmbContratoSrvVnc_SemCusto_Internalname));
                              n1090ContratoSrvVnc_SemCusto = false;
                              A1453ContratoServicosVnc_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_Ativo_Internalname));
                              n1453ContratoServicosVnc_Ativo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24GC2 */
                                    E24GC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25GC2 */
                                    E25GC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26GC2 */
                                    E26GC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_servicosigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA"), AV21TFContratoSrvVnc_ServicoSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_servicosigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL"), AV22TFContratoSrvVnc_ServicoSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosvnc_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO"), AV54TFContratoServicosVnc_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosvnc_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL"), AV55TFContratoServicosVnc_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_servicovncsigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV29TFContratoSrvVnc_ServicoVncSigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_servicovncsigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV30TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_srvvnccntnum Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM"), AV44TFContratoSrvVnc_SrvVncCntNum) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_srvvnccntnum_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL"), AV45TFContratoSrvVnc_SrvVncCntNum_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosvnc_clonarlink_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicosVnc_ClonarLink_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratosrvvnc_semcusto_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL"), ",", ".") != Convert.ToDecimal( AV37TFContratoSrvVnc_SemCusto_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosvnc_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV51TFContratoServicosVnc_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAGC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_35_idx;
            cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
            cmbContratoSrvVnc_StatusDmn.WebTags = "";
            cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
            {
               A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
               n1084ContratoSrvVnc_StatusDmn = false;
            }
            GXCCtl = "CONTRATOSERVICOSVNC_CLONARLINK_" + sGXsfl_35_idx;
            cmbContratoServicosVnc_ClonarLink.Name = GXCCtl;
            cmbContratoServicosVnc_ClonarLink.WebTags = "";
            cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
            {
               A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
               n1818ContratoServicosVnc_ClonarLink = false;
            }
            GXCCtl = "CONTRATOSRVVNC_SEMCUSTO_" + sGXsfl_35_idx;
            cmbContratoSrvVnc_SemCusto.Name = GXCCtl;
            cmbContratoSrvVnc_SemCusto.WebTags = "";
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
            {
               A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
               n1090ContratoSrvVnc_SemCusto = false;
            }
            GXCCtl = "CONTRATOSERVICOSVNC_ATIVO_" + sGXsfl_35_idx;
            chkContratoServicosVnc_Ativo.Name = GXCCtl;
            chkContratoServicosVnc_Ativo.WebTags = "";
            chkContratoServicosVnc_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_Ativo_Internalname, "TitleCaption", chkContratoServicosVnc_Ativo.Caption);
            chkContratoServicosVnc_Ativo.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_352( ) ;
         while ( nGXsfl_35_idx <= nRC_GXsfl_35 )
         {
            sendrow_352( ) ;
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV12OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV21TFContratoSrvVnc_ServicoSigla ,
                                       String AV22TFContratoSrvVnc_ServicoSigla_Sel ,
                                       String AV54TFContratoServicosVnc_Descricao ,
                                       String AV55TFContratoServicosVnc_Descricao_Sel ,
                                       String AV29TFContratoSrvVnc_ServicoVncSigla ,
                                       String AV30TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                       String AV44TFContratoSrvVnc_SrvVncCntNum ,
                                       String AV45TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                       short AV48TFContratoServicosVnc_ClonarLink_Sel ,
                                       short AV37TFContratoSrvVnc_SemCusto_Sel ,
                                       short AV51TFContratoServicosVnc_Ativo_Sel ,
                                       String AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace ,
                                       String AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace ,
                                       String AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ,
                                       String AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ,
                                       String AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace ,
                                       String AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace ,
                                       String AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace ,
                                       String AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace ,
                                       String AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV26TFContratoSrvVnc_StatusDmn_Sels ,
                                       String AV33TFContratoSrvVnc_PrestadoraPesNom ,
                                       String AV34TFContratoSrvVnc_PrestadoraPesNom_Sel ,
                                       String AV75Pgmname ,
                                       int A917ContratoSrvVnc_Codigo ,
                                       int AV17ContratoSrvVnc_CntSrvCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSVNC_DESCRICAO", A2108ContratoServicosVnc_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_STATUSDMN", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( "", A1818ContratoServicosVnc_ClonarLink));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSVNC_CLONARLINK", StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( "", A1090ContratoSrvVnc_SemCusto));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_SEMCUSTO", StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_ATIVO", GetSecureSignedToken( "", A1453ContratoServicosVnc_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSVNC_ATIVO", StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV75Pgmname = "WWContratoServicosVnc";
         context.Gx_err = 0;
      }

      protected void RFGC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 35;
         /* Execute user event: E25GC2 */
         E25GC2 ();
         nGXsfl_35_idx = 1;
         sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
         SubsflControlProps_352( ) ;
         nGXsfl_35_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_352( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1084ContratoSrvVnc_StatusDmn ,
                                                 AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                                 AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                                 AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                                 AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                                 AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                                 AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                                 AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                                 AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                                 AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                                 AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                                 AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                                 AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                                 AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                                 A922ContratoSrvVnc_ServicoSigla ,
                                                 A2108ContratoServicosVnc_Descricao ,
                                                 A924ContratoSrvVnc_ServicoVncSigla ,
                                                 A1631ContratoSrvVnc_SrvVncCntNum ,
                                                 A1818ContratoServicosVnc_ClonarLink ,
                                                 A1090ContratoSrvVnc_SemCusto ,
                                                 A1453ContratoServicosVnc_Ativo ,
                                                 AV12OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                                 AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                                 A1092ContratoSrvVnc_PrestadoraPesNom },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
            lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
            lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
            lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
            lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
            /* Using cursor H00GC4 */
            pr_default.execute(0, new Object[] {AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_35_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A915ContratoSrvVnc_CntSrvCod = H00GC4_A915ContratoSrvVnc_CntSrvCod[0];
               A921ContratoSrvVnc_ServicoCod = H00GC4_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GC4_n921ContratoSrvVnc_ServicoCod[0];
               A1589ContratoSrvVnc_SrvVncCntSrvCod = H00GC4_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = H00GC4_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A1628ContratoSrvVnc_SrvVncCntCod = H00GC4_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = H00GC4_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GC4_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GC4_n923ContratoSrvVnc_ServicoVncCod[0];
               A1453ContratoServicosVnc_Ativo = H00GC4_A1453ContratoServicosVnc_Ativo[0];
               n1453ContratoServicosVnc_Ativo = H00GC4_n1453ContratoServicosVnc_Ativo[0];
               A1090ContratoSrvVnc_SemCusto = H00GC4_A1090ContratoSrvVnc_SemCusto[0];
               n1090ContratoSrvVnc_SemCusto = H00GC4_n1090ContratoSrvVnc_SemCusto[0];
               A1818ContratoServicosVnc_ClonarLink = H00GC4_A1818ContratoServicosVnc_ClonarLink[0];
               n1818ContratoServicosVnc_ClonarLink = H00GC4_n1818ContratoServicosVnc_ClonarLink[0];
               A1631ContratoSrvVnc_SrvVncCntNum = H00GC4_A1631ContratoSrvVnc_SrvVncCntNum[0];
               n1631ContratoSrvVnc_SrvVncCntNum = H00GC4_n1631ContratoSrvVnc_SrvVncCntNum[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GC4_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GC4_n924ContratoSrvVnc_ServicoVncSigla[0];
               A1084ContratoSrvVnc_StatusDmn = H00GC4_A1084ContratoSrvVnc_StatusDmn[0];
               n1084ContratoSrvVnc_StatusDmn = H00GC4_n1084ContratoSrvVnc_StatusDmn[0];
               A2108ContratoServicosVnc_Descricao = H00GC4_A2108ContratoServicosVnc_Descricao[0];
               n2108ContratoServicosVnc_Descricao = H00GC4_n2108ContratoServicosVnc_Descricao[0];
               A922ContratoSrvVnc_ServicoSigla = H00GC4_A922ContratoSrvVnc_ServicoSigla[0];
               n922ContratoSrvVnc_ServicoSigla = H00GC4_n922ContratoSrvVnc_ServicoSigla[0];
               A917ContratoSrvVnc_Codigo = H00GC4_A917ContratoSrvVnc_Codigo[0];
               A1092ContratoSrvVnc_PrestadoraPesNom = H00GC4_A1092ContratoSrvVnc_PrestadoraPesNom[0];
               n1092ContratoSrvVnc_PrestadoraPesNom = H00GC4_n1092ContratoSrvVnc_PrestadoraPesNom[0];
               A921ContratoSrvVnc_ServicoCod = H00GC4_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GC4_n921ContratoSrvVnc_ServicoCod[0];
               A922ContratoSrvVnc_ServicoSigla = H00GC4_A922ContratoSrvVnc_ServicoSigla[0];
               n922ContratoSrvVnc_ServicoSigla = H00GC4_n922ContratoSrvVnc_ServicoSigla[0];
               A1628ContratoSrvVnc_SrvVncCntCod = H00GC4_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = H00GC4_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GC4_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GC4_n923ContratoSrvVnc_ServicoVncCod[0];
               A1631ContratoSrvVnc_SrvVncCntNum = H00GC4_A1631ContratoSrvVnc_SrvVncCntNum[0];
               n1631ContratoSrvVnc_SrvVncCntNum = H00GC4_n1631ContratoSrvVnc_SrvVncCntNum[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GC4_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GC4_n924ContratoSrvVnc_ServicoVncSigla[0];
               A1092ContratoSrvVnc_PrestadoraPesNom = H00GC4_A1092ContratoSrvVnc_PrestadoraPesNom[0];
               n1092ContratoSrvVnc_PrestadoraPesNom = H00GC4_n1092ContratoSrvVnc_PrestadoraPesNom[0];
               /* Execute user event: E26GC2 */
               E26GC2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 35;
            WBGC0( ) ;
         }
         nGXsfl_35_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                              AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                              AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                              AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                              AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                              AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels.Count ,
                                              AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                              AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                              AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                              AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                              AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                              AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                              AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1818ContratoServicosVnc_ClonarLink ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV12OrderedBy ,
                                              AV13OrderedDsc ,
                                              AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                              AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                              A1092ContratoSrvVnc_PrestadoraPesNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom), 50, "%");
         lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla), 15, "%");
         lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = StringUtil.Concat( StringUtil.RTrim( AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao), "%", "");
         lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = StringUtil.PadR( StringUtil.RTrim( AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla), 15, "%");
         lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum), 20, "%");
         /* Using cursor H00GC7 */
         pr_default.execute(1, new Object[] {AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom, AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel, lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla, AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel, lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao, AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel, lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla, AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel, lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum, AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel});
         GRID_nRecordCount = H00GC7_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFContratoSrvVnc_ServicoSigla, AV22TFContratoSrvVnc_ServicoSigla_Sel, AV54TFContratoServicosVnc_Descricao, AV55TFContratoServicosVnc_Descricao_Sel, AV29TFContratoSrvVnc_ServicoVncSigla, AV30TFContratoSrvVnc_ServicoVncSigla_Sel, AV44TFContratoSrvVnc_SrvVncCntNum, AV45TFContratoSrvVnc_SrvVncCntNum_Sel, AV48TFContratoServicosVnc_ClonarLink_Sel, AV37TFContratoSrvVnc_SemCusto_Sel, AV51TFContratoServicosVnc_Ativo_Sel, AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, AV26TFContratoSrvVnc_StatusDmn_Sels, AV33TFContratoSrvVnc_PrestadoraPesNom, AV34TFContratoSrvVnc_PrestadoraPesNom_Sel, AV75Pgmname, A917ContratoSrvVnc_Codigo, AV17ContratoSrvVnc_CntSrvCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGC0( )
      {
         /* Before Start, stand alone formulas. */
         AV75Pgmname = "WWContratoServicosVnc";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24GC2 */
         E24GC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV39DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA"), AV20ContratoSrvVnc_ServicoSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA"), AV53ContratoServicosVnc_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA"), AV24ContratoSrvVnc_StatusDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA"), AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_PRESTADORAPESNOMTITLEFILTERDATA"), AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA"), AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSVNC_CLONARLINKTITLEFILTERDATA"), AV47ContratoServicosVnc_ClonarLinkTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA"), AV36ContratoSrvVnc_SemCustoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA"), AV50ContratoServicosVnc_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV12OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            AV21TFContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoSrvVnc_ServicoSigla", AV21TFContratoSrvVnc_ServicoSigla);
            AV22TFContratoSrvVnc_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoSigla_Sel", AV22TFContratoSrvVnc_ServicoSigla_Sel);
            AV54TFContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoservicosvnc_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosVnc_Descricao", AV54TFContratoServicosVnc_Descricao);
            AV55TFContratoServicosVnc_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicosvnc_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosVnc_Descricao_Sel", AV55TFContratoServicosVnc_Descricao_Sel);
            AV29TFContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoSrvVnc_ServicoVncSigla", AV29TFContratoSrvVnc_ServicoVncSigla);
            AV30TFContratoSrvVnc_ServicoVncSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30TFContratoSrvVnc_ServicoVncSigla_Sel", AV30TFContratoSrvVnc_ServicoVncSigla_Sel);
            AV33TFContratoSrvVnc_PrestadoraPesNom = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_prestadorapesnom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoSrvVnc_PrestadoraPesNom", AV33TFContratoSrvVnc_PrestadoraPesNom);
            AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_prestadorapesnom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoSrvVnc_PrestadoraPesNom_Sel", AV34TFContratoSrvVnc_PrestadoraPesNom_Sel);
            AV44TFContratoSrvVnc_SrvVncCntNum = cgiGet( edtavTfcontratosrvvnc_srvvnccntnum_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoSrvVnc_SrvVncCntNum", AV44TFContratoSrvVnc_SrvVncCntNum);
            AV45TFContratoSrvVnc_SrvVncCntNum_Sel = cgiGet( edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoSrvVnc_SrvVncCntNum_Sel", AV45TFContratoSrvVnc_SrvVncCntNum_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_clonarlink_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_clonarlink_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL");
               GX_FocusControl = edtavTfcontratoservicosvnc_clonarlink_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratoServicosVnc_ClonarLink_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
            }
            else
            {
               AV48TFContratoServicosVnc_ClonarLink_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_clonarlink_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSRVVNC_SEMCUSTO_SEL");
               GX_FocusControl = edtavTfcontratosrvvnc_semcusto_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContratoSrvVnc_SemCusto_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            }
            else
            {
               AV37TFContratoSrvVnc_SemCusto_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratosrvvnc_semcusto_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSVNC_ATIVO_SEL");
               GX_FocusControl = edtavTfcontratoservicosvnc_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFContratoServicosVnc_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
            }
            else
            {
               AV51TFContratoServicosVnc_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosvnc_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
            }
            AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
            AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
            AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
            AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
            AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace", AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace);
            AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
            AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace", AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace);
            AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
            AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_35 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_35"), ",", "."));
            AV41GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV42GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratosrvvnc_servicosigla_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Caption");
            Ddo_contratosrvvnc_servicosigla_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Tooltip");
            Ddo_contratosrvvnc_servicosigla_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cls");
            Ddo_contratosrvvnc_servicosigla_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_set");
            Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_set");
            Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortasc"));
            Ddo_contratosrvvnc_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortdsc"));
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortedstatus");
            Ddo_contratosrvvnc_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includefilter"));
            Ddo_contratosrvvnc_servicosigla_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filtertype");
            Ddo_contratosrvvnc_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filterisrange"));
            Ddo_contratosrvvnc_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includedatalist"));
            Ddo_contratosrvvnc_servicosigla_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalisttype");
            Ddo_contratosrvvnc_servicosigla_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistproc");
            Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_servicosigla_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortasc");
            Ddo_contratosrvvnc_servicosigla_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortdsc");
            Ddo_contratosrvvnc_servicosigla_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Loadingdata");
            Ddo_contratosrvvnc_servicosigla_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cleanfilter");
            Ddo_contratosrvvnc_servicosigla_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Noresultsfound");
            Ddo_contratosrvvnc_servicosigla_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Searchbuttontext");
            Ddo_contratoservicosvnc_descricao_Caption = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Caption");
            Ddo_contratoservicosvnc_descricao_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Tooltip");
            Ddo_contratoservicosvnc_descricao_Cls = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cls");
            Ddo_contratoservicosvnc_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_set");
            Ddo_contratoservicosvnc_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_set");
            Ddo_contratoservicosvnc_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoservicosvnc_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortasc"));
            Ddo_contratoservicosvnc_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includesortdsc"));
            Ddo_contratoservicosvnc_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortedstatus");
            Ddo_contratoservicosvnc_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includefilter"));
            Ddo_contratoservicosvnc_descricao_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filtertype");
            Ddo_contratoservicosvnc_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filterisrange"));
            Ddo_contratoservicosvnc_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Includedatalist"));
            Ddo_contratoservicosvnc_descricao_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalisttype");
            Ddo_contratoservicosvnc_descricao_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistproc");
            Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosvnc_descricao_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortasc");
            Ddo_contratoservicosvnc_descricao_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Sortdsc");
            Ddo_contratoservicosvnc_descricao_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Loadingdata");
            Ddo_contratoservicosvnc_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Cleanfilter");
            Ddo_contratoservicosvnc_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Noresultsfound");
            Ddo_contratoservicosvnc_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Searchbuttontext");
            Ddo_contratosrvvnc_statusdmn_Caption = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Caption");
            Ddo_contratosrvvnc_statusdmn_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip");
            Ddo_contratosrvvnc_statusdmn_Cls = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Cls");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set");
            Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype");
            Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_statusdmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc"));
            Ddo_contratosrvvnc_statusdmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc"));
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus");
            Ddo_contratosrvvnc_statusdmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter"));
            Ddo_contratosrvvnc_statusdmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist"));
            Ddo_contratosrvvnc_statusdmn_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype");
            Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection"));
            Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues");
            Ddo_contratosrvvnc_statusdmn_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc");
            Ddo_contratosrvvnc_statusdmn_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc");
            Ddo_contratosrvvnc_statusdmn_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter");
            Ddo_contratosrvvnc_statusdmn_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext");
            Ddo_contratosrvvnc_servicovncsigla_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption");
            Ddo_contratosrvvnc_servicovncsigla_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip");
            Ddo_contratosrvvnc_servicovncsigla_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set");
            Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype");
            Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_servicovncsigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc"));
            Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc"));
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus");
            Ddo_contratosrvvnc_servicovncsigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter"));
            Ddo_contratosrvvnc_servicovncsigla_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype");
            Ddo_contratosrvvnc_servicovncsigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange"));
            Ddo_contratosrvvnc_servicovncsigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist"));
            Ddo_contratosrvvnc_servicovncsigla_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype");
            Ddo_contratosrvvnc_servicovncsigla_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc");
            Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_servicovncsigla_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc");
            Ddo_contratosrvvnc_servicovncsigla_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc");
            Ddo_contratosrvvnc_servicovncsigla_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata");
            Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter");
            Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound");
            Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext");
            Ddo_contratosrvvnc_prestadorapesnom_Caption = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Caption");
            Ddo_contratosrvvnc_prestadorapesnom_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Tooltip");
            Ddo_contratosrvvnc_prestadorapesnom_Cls = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Cls");
            Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filteredtext_set");
            Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Selectedvalue_set");
            Ddo_contratosrvvnc_prestadorapesnom_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Dropdownoptionstype");
            Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_prestadorapesnom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includesortasc"));
            Ddo_contratosrvvnc_prestadorapesnom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includesortdsc"));
            Ddo_contratosrvvnc_prestadorapesnom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includefilter"));
            Ddo_contratosrvvnc_prestadorapesnom_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filtertype");
            Ddo_contratosrvvnc_prestadorapesnom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filterisrange"));
            Ddo_contratosrvvnc_prestadorapesnom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Includedatalist"));
            Ddo_contratosrvvnc_prestadorapesnom_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalisttype");
            Ddo_contratosrvvnc_prestadorapesnom_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalistproc");
            Ddo_contratosrvvnc_prestadorapesnom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_prestadorapesnom_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Loadingdata");
            Ddo_contratosrvvnc_prestadorapesnom_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Cleanfilter");
            Ddo_contratosrvvnc_prestadorapesnom_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Noresultsfound");
            Ddo_contratosrvvnc_prestadorapesnom_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Searchbuttontext");
            Ddo_contratosrvvnc_srvvnccntnum_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Caption");
            Ddo_contratosrvvnc_srvvnccntnum_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Tooltip");
            Ddo_contratosrvvnc_srvvnccntnum_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cls");
            Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_set");
            Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_set");
            Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Dropdownoptionstype");
            Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_srvvnccntnum_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortasc"));
            Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includesortdsc"));
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortedstatus");
            Ddo_contratosrvvnc_srvvnccntnum_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includefilter"));
            Ddo_contratosrvvnc_srvvnccntnum_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filtertype");
            Ddo_contratosrvvnc_srvvnccntnum_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filterisrange"));
            Ddo_contratosrvvnc_srvvnccntnum_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Includedatalist"));
            Ddo_contratosrvvnc_srvvnccntnum_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalisttype");
            Ddo_contratosrvvnc_srvvnccntnum_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistproc");
            Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_srvvnccntnum_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortasc");
            Ddo_contratosrvvnc_srvvnccntnum_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Sortdsc");
            Ddo_contratosrvvnc_srvvnccntnum_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Loadingdata");
            Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Cleanfilter");
            Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Noresultsfound");
            Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Searchbuttontext");
            Ddo_contratoservicosvnc_clonarlink_Caption = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Caption");
            Ddo_contratoservicosvnc_clonarlink_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Tooltip");
            Ddo_contratoservicosvnc_clonarlink_Cls = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Cls");
            Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Selectedvalue_set");
            Ddo_contratoservicosvnc_clonarlink_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Dropdownoptionstype");
            Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Titlecontrolidtoreplace");
            Ddo_contratoservicosvnc_clonarlink_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includesortasc"));
            Ddo_contratoservicosvnc_clonarlink_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includesortdsc"));
            Ddo_contratoservicosvnc_clonarlink_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortedstatus");
            Ddo_contratoservicosvnc_clonarlink_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includefilter"));
            Ddo_contratoservicosvnc_clonarlink_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Includedatalist"));
            Ddo_contratoservicosvnc_clonarlink_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Datalisttype");
            Ddo_contratoservicosvnc_clonarlink_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Datalistfixedvalues");
            Ddo_contratoservicosvnc_clonarlink_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortasc");
            Ddo_contratoservicosvnc_clonarlink_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Sortdsc");
            Ddo_contratoservicosvnc_clonarlink_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Cleanfilter");
            Ddo_contratoservicosvnc_clonarlink_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Searchbuttontext");
            Ddo_contratosrvvnc_semcusto_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Caption");
            Ddo_contratosrvvnc_semcusto_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Tooltip");
            Ddo_contratosrvvnc_semcusto_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Cls");
            Ddo_contratosrvvnc_semcusto_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_set");
            Ddo_contratosrvvnc_semcusto_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Dropdownoptionstype");
            Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_semcusto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortasc"));
            Ddo_contratosrvvnc_semcusto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Includesortdsc"));
            Ddo_contratosrvvnc_semcusto_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortedstatus");
            Ddo_contratosrvvnc_semcusto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Includefilter"));
            Ddo_contratosrvvnc_semcusto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Includedatalist"));
            Ddo_contratosrvvnc_semcusto_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Datalisttype");
            Ddo_contratosrvvnc_semcusto_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Datalistfixedvalues");
            Ddo_contratosrvvnc_semcusto_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortasc");
            Ddo_contratosrvvnc_semcusto_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Sortdsc");
            Ddo_contratosrvvnc_semcusto_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Cleanfilter");
            Ddo_contratosrvvnc_semcusto_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Searchbuttontext");
            Ddo_contratoservicosvnc_ativo_Caption = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Caption");
            Ddo_contratoservicosvnc_ativo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Tooltip");
            Ddo_contratoservicosvnc_ativo_Cls = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Cls");
            Ddo_contratoservicosvnc_ativo_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_set");
            Ddo_contratoservicosvnc_ativo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Dropdownoptionstype");
            Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Titlecontrolidtoreplace");
            Ddo_contratoservicosvnc_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortasc"));
            Ddo_contratoservicosvnc_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Includesortdsc"));
            Ddo_contratoservicosvnc_ativo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortedstatus");
            Ddo_contratoservicosvnc_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Includefilter"));
            Ddo_contratoservicosvnc_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Includedatalist"));
            Ddo_contratoservicosvnc_ativo_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Datalisttype");
            Ddo_contratoservicosvnc_ativo_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Datalistfixedvalues");
            Ddo_contratoservicosvnc_ativo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortasc");
            Ddo_contratoservicosvnc_ativo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Sortdsc");
            Ddo_contratoservicosvnc_ativo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Cleanfilter");
            Ddo_contratoservicosvnc_ativo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratosrvvnc_servicosigla_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Activeeventkey");
            Ddo_contratosrvvnc_servicosigla_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_get");
            Ddo_contratosrvvnc_servicosigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_get");
            Ddo_contratoservicosvnc_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Activeeventkey");
            Ddo_contratoservicosvnc_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Filteredtext_get");
            Ddo_contratoservicosvnc_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSVNC_DESCRICAO_Selectedvalue_get");
            Ddo_contratosrvvnc_statusdmn_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get");
            Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get");
            Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Activeeventkey");
            Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Filteredtext_get");
            Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM_Selectedvalue_get");
            Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Activeeventkey");
            Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Filteredtext_get");
            Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM_Selectedvalue_get");
            Ddo_contratoservicosvnc_clonarlink_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Activeeventkey");
            Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSVNC_CLONARLINK_Selectedvalue_get");
            Ddo_contratosrvvnc_semcusto_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Activeeventkey");
            Ddo_contratosrvvnc_semcusto_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SEMCUSTO_Selectedvalue_get");
            Ddo_contratoservicosvnc_ativo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Activeeventkey");
            Ddo_contratoservicosvnc_ativo_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSVNC_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA"), AV21TFContratoSrvVnc_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL"), AV22TFContratoSrvVnc_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO"), AV54TFContratoServicosVnc_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL"), AV55TFContratoServicosVnc_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV29TFContratoSrvVnc_ServicoVncSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV30TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM"), AV44TFContratoSrvVnc_SrvVncCntNum) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL"), AV45TFContratoSrvVnc_SrvVncCntNum_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL"), ",", ".") != Convert.ToDecimal( AV48TFContratoServicosVnc_ClonarLink_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSRVVNC_SEMCUSTO_SEL"), ",", ".") != Convert.ToDecimal( AV37TFContratoSrvVnc_SemCusto_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSVNC_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV51TFContratoServicosVnc_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24GC2 */
         E24GC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24GC2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratosrvvnc_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicosigla_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicosigla_sel_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosvnc_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_descricao_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosvnc_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_descricao_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicovncsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_prestadorapesnom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_prestadorapesnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_prestadorapesnom_Visible), 5, 0)));
         edtavTfcontratosrvvnc_prestadorapesnom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_prestadorapesnom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_prestadorapesnom_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_srvvnccntnum_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_srvvnccntnum_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_srvvnccntnum_Visible), 5, 0)));
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_clonarlink_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosvnc_clonarlink_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_clonarlink_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_semcusto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_semcusto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_semcusto_sel_Visible), 5, 0)));
         edtavTfcontratoservicosvnc_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosvnc_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosvnc_ativo_sel_Visible), 5, 0)));
         Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace);
         AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosVnc_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace);
         AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace", AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_StatusDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace);
         AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_ServicoVncSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace);
         AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_PrestadoraPesNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_prestadorapesnom_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace);
         AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace = Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace", AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_SrvVncCntNum";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace);
         AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace", AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosVnc_ClonarLink";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace);
         AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace = Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace", AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace);
         edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_SemCusto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace);
         AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace", AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosVnc_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace);
         AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace", AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace);
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Regra de Vinculo entre Servi�os";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Servi�o", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "No status", 0);
         cmbavOrderedby.addItem("4", "Novo servi�o", 0);
         cmbavOrderedby.addItem("5", "Contrato", 0);
         cmbavOrderedby.addItem("6", "Clonar link", 0);
         cmbavOrderedby.addItem("7", "Sem custo", 0);
         cmbavOrderedby.addItem("8", "Ativa?", 0);
         if ( AV12OrderedBy < 1 )
         {
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV39DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV39DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25GC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20ContratoSrvVnc_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContratoServicosVnc_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoServicosVnc_ClonarLinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoSrvVnc_SemCustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50ContratoServicosVnc_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoSrvVnc_ServicoSigla_Titleformat = 2;
         edtContratoSrvVnc_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_ServicoSigla_Internalname, "Title", edtContratoSrvVnc_ServicoSigla_Title);
         edtContratoServicosVnc_Descricao_Titleformat = 2;
         edtContratoServicosVnc_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosVnc_Descricao_Internalname, "Title", edtContratoServicosVnc_Descricao_Title);
         cmbContratoSrvVnc_StatusDmn_Titleformat = 2;
         cmbContratoSrvVnc_StatusDmn.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "No status", AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Title", cmbContratoSrvVnc_StatusDmn.Title.Text);
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 2;
         edtContratoSrvVnc_ServicoVncSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Novo servi�o", AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_ServicoVncSigla_Internalname, "Title", edtContratoSrvVnc_ServicoVncSigla_Title);
         edtContratoSrvVnc_PrestadoraPesNom_Titleformat = 2;
         edtContratoSrvVnc_PrestadoraPesNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Requisitado", AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_PrestadoraPesNom_Internalname, "Title", edtContratoSrvVnc_PrestadoraPesNom_Title);
         edtContratoSrvVnc_SrvVncCntNum_Titleformat = 2;
         edtContratoSrvVnc_SrvVncCntNum_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_SrvVncCntNum_Internalname, "Title", edtContratoSrvVnc_SrvVncCntNum_Title);
         cmbContratoServicosVnc_ClonarLink_Titleformat = 2;
         cmbContratoServicosVnc_ClonarLink.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Clonar link", AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonarLink_Internalname, "Title", cmbContratoServicosVnc_ClonarLink.Title.Text);
         cmbContratoSrvVnc_SemCusto_Titleformat = 2;
         cmbContratoSrvVnc_SemCusto.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sem custo", AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SemCusto_Internalname, "Title", cmbContratoSrvVnc_SemCusto.Title.Text);
         chkContratoServicosVnc_Ativo_Titleformat = 2;
         chkContratoServicosVnc_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativa?", AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosVnc_Ativo_Internalname, "Title", chkContratoServicosVnc_Ativo.Title.Text);
         AV41GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridCurrentPage), 10, 0)));
         AV42GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridPageCount), 10, 0)));
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = AV21TFContratoSrvVnc_ServicoSigla;
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = AV22TFContratoSrvVnc_ServicoSigla_Sel;
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = AV54TFContratoServicosVnc_Descricao;
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = AV55TFContratoServicosVnc_Descricao_Sel;
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = AV26TFContratoSrvVnc_StatusDmn_Sels;
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = AV29TFContratoSrvVnc_ServicoVncSigla;
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = AV33TFContratoSrvVnc_PrestadoraPesNom;
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = AV44TFContratoSrvVnc_SrvVncCntNum;
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
         AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel = AV48TFContratoServicosVnc_ClonarLink_Sel;
         AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel = AV37TFContratoSrvVnc_SemCusto_Sel;
         AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel = AV51TFContratoServicosVnc_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20ContratoSrvVnc_ServicoSiglaTitleFilterData", AV20ContratoSrvVnc_ServicoSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53ContratoServicosVnc_DescricaoTitleFilterData", AV53ContratoServicosVnc_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24ContratoSrvVnc_StatusDmnTitleFilterData", AV24ContratoSrvVnc_StatusDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData", AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData", AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData", AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratoServicosVnc_ClonarLinkTitleFilterData", AV47ContratoServicosVnc_ClonarLinkTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ContratoSrvVnc_SemCustoTitleFilterData", AV36ContratoSrvVnc_SemCustoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50ContratoServicosVnc_AtivoTitleFilterData", AV50ContratoServicosVnc_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11GC2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV40PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV40PageToGo) ;
         }
      }

      protected void E12GC2( )
      {
         /* Ddo_contratosrvvnc_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFContratoSrvVnc_ServicoSigla = Ddo_contratosrvvnc_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoSrvVnc_ServicoSigla", AV21TFContratoSrvVnc_ServicoSigla);
            AV22TFContratoSrvVnc_ServicoSigla_Sel = Ddo_contratosrvvnc_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoSigla_Sel", AV22TFContratoSrvVnc_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GC2( )
      {
         /* Ddo_contratoservicosvnc_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFContratoServicosVnc_Descricao = Ddo_contratoservicosvnc_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosVnc_Descricao", AV54TFContratoServicosVnc_Descricao);
            AV55TFContratoServicosVnc_Descricao_Sel = Ddo_contratoservicosvnc_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosVnc_Descricao_Sel", AV55TFContratoServicosVnc_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14GC2( )
      {
         /* Ddo_contratosrvvnc_statusdmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFContratoSrvVnc_StatusDmn_SelsJson = Ddo_contratosrvvnc_statusdmn_Selectedvalue_get;
            AV26TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV25TFContratoSrvVnc_StatusDmn_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26TFContratoSrvVnc_StatusDmn_Sels", AV26TFContratoSrvVnc_StatusDmn_Sels);
      }

      protected void E15GC2( )
      {
         /* Ddo_contratosrvvnc_servicovncsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFContratoSrvVnc_ServicoVncSigla = Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoSrvVnc_ServicoVncSigla", AV29TFContratoSrvVnc_ServicoVncSigla);
            AV30TFContratoSrvVnc_ServicoVncSigla_Sel = Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30TFContratoSrvVnc_ServicoVncSigla_Sel", AV30TFContratoSrvVnc_ServicoVncSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16GC2( )
      {
         /* Ddo_contratosrvvnc_prestadorapesnom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFContratoSrvVnc_PrestadoraPesNom = Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoSrvVnc_PrestadoraPesNom", AV33TFContratoSrvVnc_PrestadoraPesNom);
            AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoSrvVnc_PrestadoraPesNom_Sel", AV34TFContratoSrvVnc_PrestadoraPesNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E17GC2( )
      {
         /* Ddo_contratosrvvnc_srvvnccntnum_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFContratoSrvVnc_SrvVncCntNum = Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoSrvVnc_SrvVncCntNum", AV44TFContratoSrvVnc_SrvVncCntNum);
            AV45TFContratoSrvVnc_SrvVncCntNum_Sel = Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoSrvVnc_SrvVncCntNum_Sel", AV45TFContratoSrvVnc_SrvVncCntNum_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18GC2( )
      {
         /* Ddo_contratoservicosvnc_clonarlink_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_clonarlink_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_clonarlink_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SortedStatus", Ddo_contratoservicosvnc_clonarlink_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_clonarlink_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_clonarlink_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SortedStatus", Ddo_contratoservicosvnc_clonarlink_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_clonarlink_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFContratoServicosVnc_ClonarLink_Sel = (short)(NumberUtil.Val( Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19GC2( )
      {
         /* Ddo_contratosrvvnc_semcusto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_semcusto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratosrvvnc_semcusto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_semcusto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( Ddo_contratosrvvnc_semcusto_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20GC2( )
      {
         /* Ddo_contratoservicosvnc_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosvnc_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosvnc_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contratoservicosvnc_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26GC2( )
      {
         /* Grid_Load Routine */
         AV14Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV14Update);
         AV73Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV17ContratoSrvVnc_CntSrvCod);
         AV15Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV15Delete);
         AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV17ContratoSrvVnc_CntSrvCod);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 35;
         }
         sendrow_352( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(35, GridRow);
         }
      }

      protected void E21GC2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22GC2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV26TFContratoSrvVnc_StatusDmn_Sels", AV26TFContratoSrvVnc_StatusDmn_Sels);
      }

      protected void E23GC2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV17ContratoSrvVnc_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratosrvvnc_servicosigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
         Ddo_contratoservicosvnc_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
         Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
         Ddo_contratoservicosvnc_clonarlink_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SortedStatus", Ddo_contratoservicosvnc_clonarlink_Sortedstatus);
         Ddo_contratosrvvnc_semcusto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
         Ddo_contratoservicosvnc_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV12OrderedBy == 1 )
         {
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
         }
         else if ( AV12OrderedBy == 2 )
         {
            Ddo_contratoservicosvnc_descricao_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SortedStatus", Ddo_contratoservicosvnc_descricao_Sortedstatus);
         }
         else if ( AV12OrderedBy == 3 )
         {
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         }
         else if ( AV12OrderedBy == 4 )
         {
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
         }
         else if ( AV12OrderedBy == 5 )
         {
            Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SortedStatus", Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus);
         }
         else if ( AV12OrderedBy == 6 )
         {
            Ddo_contratoservicosvnc_clonarlink_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SortedStatus", Ddo_contratoservicosvnc_clonarlink_Sortedstatus);
         }
         else if ( AV12OrderedBy == 7 )
         {
            Ddo_contratosrvvnc_semcusto_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SortedStatus", Ddo_contratosrvvnc_semcusto_Sortedstatus);
         }
         else if ( AV12OrderedBy == 8 )
         {
            Ddo_contratoservicosvnc_ativo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SortedStatus", Ddo_contratoservicosvnc_ativo_Sortedstatus);
         }
      }

      protected void S162( )
      {
         /* 'CLEANFILTERS' Routine */
         AV21TFContratoSrvVnc_ServicoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoSrvVnc_ServicoSigla", AV21TFContratoSrvVnc_ServicoSigla);
         Ddo_contratosrvvnc_servicosigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicosigla_Filteredtext_set);
         AV22TFContratoSrvVnc_ServicoSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoSigla_Sel", AV22TFContratoSrvVnc_ServicoSigla_Sel);
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicosigla_Selectedvalue_set);
         AV54TFContratoServicosVnc_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosVnc_Descricao", AV54TFContratoServicosVnc_Descricao);
         Ddo_contratoservicosvnc_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "FilteredText_set", Ddo_contratoservicosvnc_descricao_Filteredtext_set);
         AV55TFContratoServicosVnc_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosVnc_Descricao_Sel", AV55TFContratoServicosVnc_Descricao_Sel);
         Ddo_contratoservicosvnc_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_descricao_Selectedvalue_set);
         AV26TFContratoSrvVnc_StatusDmn_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_statusdmn_Selectedvalue_set);
         AV29TFContratoSrvVnc_ServicoVncSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoSrvVnc_ServicoVncSigla", AV29TFContratoSrvVnc_ServicoVncSigla);
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set);
         AV30TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30TFContratoSrvVnc_ServicoVncSigla_Sel", AV30TFContratoSrvVnc_ServicoVncSigla_Sel);
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set);
         AV33TFContratoSrvVnc_PrestadoraPesNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoSrvVnc_PrestadoraPesNom", AV33TFContratoSrvVnc_PrestadoraPesNom);
         Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_prestadorapesnom_Internalname, "FilteredText_set", Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set);
         AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoSrvVnc_PrestadoraPesNom_Sel", AV34TFContratoSrvVnc_PrestadoraPesNom_Sel);
         Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_prestadorapesnom_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set);
         AV44TFContratoSrvVnc_SrvVncCntNum = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoSrvVnc_SrvVncCntNum", AV44TFContratoSrvVnc_SrvVncCntNum);
         Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "FilteredText_set", Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set);
         AV45TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoSrvVnc_SrvVncCntNum_Sel", AV45TFContratoSrvVnc_SrvVncCntNum_Sel);
         Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set);
         AV48TFContratoServicosVnc_ClonarLink_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
         Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set);
         AV37TFContratoSrvVnc_SemCusto_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
         Ddo_contratosrvvnc_semcusto_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_semcusto_Selectedvalue_set);
         AV51TFContratoServicosVnc_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
         Ddo_contratoservicosvnc_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_ativo_Selectedvalue_set);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV75Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV75Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV16Session.Get(AV75Pgmname+"GridState"), "");
         }
         AV12OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S172( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV76GXV1 = 1;
         while ( AV76GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV76GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA") == 0 )
            {
               AV21TFContratoSrvVnc_ServicoSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFContratoSrvVnc_ServicoSigla", AV21TFContratoSrvVnc_ServicoSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoSrvVnc_ServicoSigla)) )
               {
                  Ddo_contratosrvvnc_servicosigla_Filteredtext_set = AV21TFContratoSrvVnc_ServicoSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicosigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOSIGLA_SEL") == 0 )
            {
               AV22TFContratoSrvVnc_ServicoSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoSigla_Sel", AV22TFContratoSrvVnc_ServicoSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoSigla_Sel)) )
               {
                  Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = AV22TFContratoSrvVnc_ServicoSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicosigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO") == 0 )
            {
               AV54TFContratoServicosVnc_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosVnc_Descricao", AV54TFContratoServicosVnc_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFContratoServicosVnc_Descricao)) )
               {
                  Ddo_contratoservicosvnc_descricao_Filteredtext_set = AV54TFContratoServicosVnc_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "FilteredText_set", Ddo_contratoservicosvnc_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL") == 0 )
            {
               AV55TFContratoServicosVnc_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosVnc_Descricao_Sel", AV55TFContratoServicosVnc_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratoServicosVnc_Descricao_Sel)) )
               {
                  Ddo_contratoservicosvnc_descricao_Selectedvalue_set = AV55TFContratoServicosVnc_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_descricao_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_STATUSDMN_SEL") == 0 )
            {
               AV25TFContratoSrvVnc_StatusDmn_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV26TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV25TFContratoSrvVnc_StatusDmn_SelsJson);
               if ( ! ( AV26TFContratoSrvVnc_StatusDmn_Sels.Count == 0 ) )
               {
                  Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = AV25TFContratoSrvVnc_StatusDmn_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_statusdmn_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
            {
               AV29TFContratoSrvVnc_ServicoVncSigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TFContratoSrvVnc_ServicoVncSigla", AV29TFContratoSrvVnc_ServicoVncSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoSrvVnc_ServicoVncSigla)) )
               {
                  Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = AV29TFContratoSrvVnc_ServicoVncSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL") == 0 )
            {
               AV30TFContratoSrvVnc_ServicoVncSigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30TFContratoSrvVnc_ServicoVncSigla_Sel", AV30TFContratoSrvVnc_ServicoVncSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoSrvVnc_ServicoVncSigla_Sel)) )
               {
                  Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_PRESTADORAPESNOM") == 0 )
            {
               AV33TFContratoSrvVnc_PrestadoraPesNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoSrvVnc_PrestadoraPesNom", AV33TFContratoSrvVnc_PrestadoraPesNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoSrvVnc_PrestadoraPesNom)) )
               {
                  Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set = AV33TFContratoSrvVnc_PrestadoraPesNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_prestadorapesnom_Internalname, "FilteredText_set", Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL") == 0 )
            {
               AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoSrvVnc_PrestadoraPesNom_Sel", AV34TFContratoSrvVnc_PrestadoraPesNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoSrvVnc_PrestadoraPesNom_Sel)) )
               {
                  Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_prestadorapesnom_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
            {
               AV44TFContratoSrvVnc_SrvVncCntNum = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoSrvVnc_SrvVncCntNum", AV44TFContratoSrvVnc_SrvVncCntNum);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum)) )
               {
                  Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = AV44TFContratoSrvVnc_SrvVncCntNum;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "FilteredText_set", Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL") == 0 )
            {
               AV45TFContratoSrvVnc_SrvVncCntNum_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoSrvVnc_SrvVncCntNum_Sel", AV45TFContratoSrvVnc_SrvVncCntNum_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoSrvVnc_SrvVncCntNum_Sel)) )
               {
                  Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_srvvnccntnum_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_CLONARLINK_SEL") == 0 )
            {
               AV48TFContratoServicosVnc_ClonarLink_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosVnc_ClonarLink_Sel", StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0));
               if ( ! (0==AV48TFContratoServicosVnc_ClonarLink_Sel) )
               {
                  Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set = StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_clonarlink_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SEMCUSTO_SEL") == 0 )
            {
               AV37TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoSrvVnc_SemCusto_Sel", StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0));
               if ( ! (0==AV37TFContratoSrvVnc_SemCusto_Sel) )
               {
                  Ddo_contratosrvvnc_semcusto_Selectedvalue_set = StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_semcusto_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_semcusto_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_ATIVO_SEL") == 0 )
            {
               AV51TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosVnc_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0));
               if ( ! (0==AV51TFContratoServicosVnc_Ativo_Sel) )
               {
                  Ddo_contratoservicosvnc_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosvnc_ativo_Internalname, "SelectedValue_set", Ddo_contratoservicosvnc_ativo_Selectedvalue_set);
               }
            }
            AV76GXV1 = (int)(AV76GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV16Session.Get(AV75Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoSrvVnc_ServicoSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV21TFContratoSrvVnc_ServicoSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV22TFContratoSrvVnc_ServicoSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFContratoServicosVnc_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV54TFContratoServicosVnc_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratoServicosVnc_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFContratoServicosVnc_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV26TFContratoSrvVnc_StatusDmn_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_STATUSDMN_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV26TFContratoSrvVnc_StatusDmn_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoSrvVnc_ServicoVncSigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV29TFContratoSrvVnc_ServicoVncSigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV30TFContratoSrvVnc_ServicoVncSigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoSrvVnc_PrestadoraPesNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_PRESTADORAPESNOM";
            AV11GridStateFilterValue.gxTpr_Value = AV33TFContratoSrvVnc_PrestadoraPesNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoSrvVnc_PrestadoraPesNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFContratoSrvVnc_PrestadoraPesNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoSrvVnc_SrvVncCntNum)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SRVVNCCNTNUM";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFContratoSrvVnc_SrvVncCntNum;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContratoSrvVnc_SrvVncCntNum_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV48TFContratoServicosVnc_ClonarLink_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_CLONARLINK_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV48TFContratoServicosVnc_ClonarLink_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV37TFContratoSrvVnc_SemCusto_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SEMCUSTO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37TFContratoSrvVnc_SemCusto_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV51TFContratoServicosVnc_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSVNC_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFContratoServicosVnc_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV75Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV75Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosVnc";
         AV16Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_GC2( true) ;
         }
         else
         {
            wb_table2_8_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_GC2( true) ;
         }
         else
         {
            wb_table3_29_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GC2e( true) ;
         }
         else
         {
            wb_table1_2_GC2e( false) ;
         }
      }

      protected void wb_table3_29_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_32_GC2( true) ;
         }
         else
         {
            wb_table4_32_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_32_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_GC2e( true) ;
         }
         else
         {
            wb_table3_29_GC2e( false) ;
         }
      }

      protected void wb_table4_32_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"35\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosVnc_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosVnc_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosVnc_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_StatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_ServicoVncSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_PrestadoraPesNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_PrestadoraPesNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_PrestadoraPesNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_SrvVncCntNum_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_SrvVncCntNum_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_SrvVncCntNum_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosVnc_ClonarLink_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosVnc_ClonarLink.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosVnc_ClonarLink.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_SemCusto_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_SemCusto.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_SemCusto.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratoServicosVnc_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratoServicosVnc_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratoServicosVnc_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV14Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2108ContratoServicosVnc_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosVnc_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosVnc_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_StatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_StatusDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_ServicoVncSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoVncSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_PrestadoraPesNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_PrestadoraPesNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_SrvVncCntNum_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_SrvVncCntNum_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosVnc_ClonarLink.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosVnc_ClonarLink_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_SemCusto.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_SemCusto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratoServicosVnc_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratoServicosVnc_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 35 )
         {
            wbEnd = 0;
            nRC_GXsfl_35 = (short)(nGXsfl_35_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_32_GC2e( true) ;
         }
         else
         {
            wb_table4_32_GC2e( false) ;
         }
      }

      protected void wb_table2_8_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosvnctitle_Internalname, "Regra de Vinculo entre Servi�os", "", "", lblContratoservicosvnctitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_GC2( true) ;
         }
         else
         {
            wb_table5_13_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_35_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoServicosVnc.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_GC2( true) ;
         }
         else
         {
            wb_table6_23_GC2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_GC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_GC2e( true) ;
         }
         else
         {
            wb_table2_8_GC2e( false) ;
         }
      }

      protected void wb_table6_23_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_GC2e( true) ;
         }
         else
         {
            wb_table6_23_GC2e( false) ;
         }
      }

      protected void wb_table5_13_GC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_GC2e( true) ;
         }
         else
         {
            wb_table5_13_GC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGC2( ) ;
         WSGC2( ) ;
         WEGC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312124722");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoservicosvnc.js", "?2020312124722");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_352( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_35_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_35_idx;
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO_"+sGXsfl_35_idx;
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA_"+sGXsfl_35_idx;
         edtContratoServicosVnc_Descricao_Internalname = "CONTRATOSERVICOSVNC_DESCRICAO_"+sGXsfl_35_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_35_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_35_idx;
         edtContratoSrvVnc_PrestadoraPesNom_Internalname = "CONTRATOSRVVNC_PRESTADORAPESNOM_"+sGXsfl_35_idx;
         edtContratoSrvVnc_SrvVncCntNum_Internalname = "CONTRATOSRVVNC_SRVVNCCNTNUM_"+sGXsfl_35_idx;
         cmbContratoServicosVnc_ClonarLink_Internalname = "CONTRATOSERVICOSVNC_CLONARLINK_"+sGXsfl_35_idx;
         cmbContratoSrvVnc_SemCusto_Internalname = "CONTRATOSRVVNC_SEMCUSTO_"+sGXsfl_35_idx;
         chkContratoServicosVnc_Ativo_Internalname = "CONTRATOSERVICOSVNC_ATIVO_"+sGXsfl_35_idx;
      }

      protected void SubsflControlProps_fel_352( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_35_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_35_fel_idx;
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO_"+sGXsfl_35_fel_idx;
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA_"+sGXsfl_35_fel_idx;
         edtContratoServicosVnc_Descricao_Internalname = "CONTRATOSERVICOSVNC_DESCRICAO_"+sGXsfl_35_fel_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_35_fel_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_35_fel_idx;
         edtContratoSrvVnc_PrestadoraPesNom_Internalname = "CONTRATOSRVVNC_PRESTADORAPESNOM_"+sGXsfl_35_fel_idx;
         edtContratoSrvVnc_SrvVncCntNum_Internalname = "CONTRATOSRVVNC_SRVVNCCNTNUM_"+sGXsfl_35_fel_idx;
         cmbContratoServicosVnc_ClonarLink_Internalname = "CONTRATOSERVICOSVNC_CLONARLINK_"+sGXsfl_35_fel_idx;
         cmbContratoSrvVnc_SemCusto_Internalname = "CONTRATOSRVVNC_SEMCUSTO_"+sGXsfl_35_fel_idx;
         chkContratoServicosVnc_Ativo_Internalname = "CONTRATOSERVICOSVNC_ATIVO_"+sGXsfl_35_fel_idx;
      }

      protected void sendrow_352( )
      {
         SubsflControlProps_352( ) ;
         WBGC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_35_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_35_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_35_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV14Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV73Update_GXI : context.PathToRelativeUrl( AV14Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV14Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV74Delete_GXI : context.PathToRelativeUrl( AV15Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoSigla_Internalname,StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A922ContratoSrvVnc_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosVnc_Descricao_Internalname,(String)A2108ContratoServicosVnc_Descricao,StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosVnc_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_35_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_35_idx;
               cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
               cmbContratoSrvVnc_StatusDmn.WebTags = "";
               cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
               {
                  A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
                  n1084ContratoSrvVnc_StatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_StatusDmn,(String)cmbContratoSrvVnc_StatusDmn_Internalname,StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn),(short)1,(String)cmbContratoSrvVnc_StatusDmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_StatusDmn.CurrentValue = StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_StatusDmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoVncSigla_Internalname,StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla),StringUtil.RTrim( context.localUtil.Format( A924ContratoSrvVnc_ServicoVncSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoVncSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_PrestadoraPesNom_Internalname,StringUtil.RTrim( A1092ContratoSrvVnc_PrestadoraPesNom),StringUtil.RTrim( context.localUtil.Format( A1092ContratoSrvVnc_PrestadoraPesNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_PrestadoraPesNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_SrvVncCntNum_Internalname,StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_SrvVncCntNum_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_35_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSVNC_CLONARLINK_" + sGXsfl_35_idx;
               cmbContratoServicosVnc_ClonarLink.Name = GXCCtl;
               cmbContratoServicosVnc_ClonarLink.WebTags = "";
               cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
               {
                  A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
                  n1818ContratoServicosVnc_ClonarLink = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosVnc_ClonarLink,(String)cmbContratoServicosVnc_ClonarLink_Internalname,StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink),(short)1,(String)cmbContratoServicosVnc_ClonarLink_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosVnc_ClonarLink.CurrentValue = StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosVnc_ClonarLink_Internalname, "Values", (String)(cmbContratoServicosVnc_ClonarLink.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_35_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_SEMCUSTO_" + sGXsfl_35_idx;
               cmbContratoSrvVnc_SemCusto.Name = GXCCtl;
               cmbContratoSrvVnc_SemCusto.WebTags = "";
               cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
               {
                  A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
                  n1090ContratoSrvVnc_SemCusto = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_SemCusto,(String)cmbContratoSrvVnc_SemCusto_Internalname,StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto),(short)1,(String)cmbContratoSrvVnc_SemCusto_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_SemCusto.CurrentValue = StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_SemCusto_Internalname, "Values", (String)(cmbContratoSrvVnc_SemCusto.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratoServicosVnc_Ativo_Internalname,StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_CODIGO"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_DESCRICAO"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_STATUSDMN"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, A1818ContratoServicosVnc_ClonarLink));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_SEMCUSTO"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, A1090ContratoSrvVnc_SemCusto));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSVNC_ATIVO"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, A1453ContratoServicosVnc_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         /* End function sendrow_352 */
      }

      protected void init_default_properties( )
      {
         lblContratoservicosvnctitle_Internalname = "CONTRATOSERVICOSVNCTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO";
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA";
         edtContratoServicosVnc_Descricao_Internalname = "CONTRATOSERVICOSVNC_DESCRICAO";
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN";
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtContratoSrvVnc_PrestadoraPesNom_Internalname = "CONTRATOSRVVNC_PRESTADORAPESNOM";
         edtContratoSrvVnc_SrvVncCntNum_Internalname = "CONTRATOSRVVNC_SRVVNCCNTNUM";
         cmbContratoServicosVnc_ClonarLink_Internalname = "CONTRATOSERVICOSVNC_CLONARLINK";
         cmbContratoSrvVnc_SemCusto_Internalname = "CONTRATOSRVVNC_SEMCUSTO";
         chkContratoServicosVnc_Ativo_Internalname = "CONTRATOSERVICOSVNC_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfcontratosrvvnc_servicosigla_Internalname = "vTFCONTRATOSRVVNC_SERVICOSIGLA";
         edtavTfcontratosrvvnc_servicosigla_sel_Internalname = "vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL";
         edtavTfcontratoservicosvnc_descricao_Internalname = "vTFCONTRATOSERVICOSVNC_DESCRICAO";
         edtavTfcontratoservicosvnc_descricao_sel_Internalname = "vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL";
         edtavTfcontratosrvvnc_servicovncsigla_Internalname = "vTFCONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname = "vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
         edtavTfcontratosrvvnc_prestadorapesnom_Internalname = "vTFCONTRATOSRVVNC_PRESTADORAPESNOM";
         edtavTfcontratosrvvnc_prestadorapesnom_sel_Internalname = "vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL";
         edtavTfcontratosrvvnc_srvvnccntnum_Internalname = "vTFCONTRATOSRVVNC_SRVVNCCNTNUM";
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname = "vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL";
         edtavTfcontratoservicosvnc_clonarlink_sel_Internalname = "vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL";
         edtavTfcontratosrvvnc_semcusto_sel_Internalname = "vTFCONTRATOSRVVNC_SEMCUSTO_SEL";
         edtavTfcontratoservicosvnc_ativo_sel_Internalname = "vTFCONTRATOSERVICOSVNC_ATIVO_SEL";
         Ddo_contratosrvvnc_servicosigla_Internalname = "DDO_CONTRATOSRVVNC_SERVICOSIGLA";
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosvnc_descricao_Internalname = "DDO_CONTRATOSERVICOSVNC_DESCRICAO";
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_statusdmn_Internalname = "DDO_CONTRATOSRVVNC_STATUSDMN";
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_servicovncsigla_Internalname = "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_prestadorapesnom_Internalname = "DDO_CONTRATOSRVVNC_PRESTADORAPESNOM";
         edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_srvvnccntnum_Internalname = "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM";
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosvnc_clonarlink_Internalname = "DDO_CONTRATOSERVICOSVNC_CLONARLINK";
         edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_semcusto_Internalname = "DDO_CONTRATOSRVVNC_SEMCUSTO";
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosvnc_ativo_Internalname = "DDO_CONTRATOSERVICOSVNC_ATIVO";
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContratoSrvVnc_SemCusto_Jsonclick = "";
         cmbContratoServicosVnc_ClonarLink_Jsonclick = "";
         edtContratoSrvVnc_SrvVncCntNum_Jsonclick = "";
         edtContratoSrvVnc_PrestadoraPesNom_Jsonclick = "";
         edtContratoSrvVnc_ServicoVncSigla_Jsonclick = "";
         cmbContratoSrvVnc_StatusDmn_Jsonclick = "";
         edtContratoServicosVnc_Descricao_Jsonclick = "";
         edtContratoSrvVnc_ServicoSigla_Jsonclick = "";
         edtContratoSrvVnc_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkContratoServicosVnc_Ativo_Titleformat = 0;
         cmbContratoSrvVnc_SemCusto_Titleformat = 0;
         cmbContratoServicosVnc_ClonarLink_Titleformat = 0;
         edtContratoSrvVnc_SrvVncCntNum_Titleformat = 0;
         edtContratoSrvVnc_PrestadoraPesNom_Titleformat = 0;
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 0;
         cmbContratoSrvVnc_StatusDmn_Titleformat = 0;
         edtContratoServicosVnc_Descricao_Titleformat = 0;
         edtContratoSrvVnc_ServicoSigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         chkContratoServicosVnc_Ativo.Title.Text = "Ativa?";
         cmbContratoSrvVnc_SemCusto.Title.Text = "Sem custo";
         cmbContratoServicosVnc_ClonarLink.Title.Text = "Clonar link";
         edtContratoSrvVnc_SrvVncCntNum_Title = "Contrato";
         edtContratoSrvVnc_PrestadoraPesNom_Title = "Requisitado";
         edtContratoSrvVnc_ServicoVncSigla_Title = "Novo servi�o";
         cmbContratoSrvVnc_StatusDmn.Title.Text = "No status";
         edtContratoServicosVnc_Descricao_Title = "Descri��o";
         edtContratoSrvVnc_ServicoSigla_Title = "Servi�o";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratoServicosVnc_Ativo.Caption = "";
         edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosvnc_ativo_sel_Jsonclick = "";
         edtavTfcontratoservicosvnc_ativo_sel_Visible = 1;
         edtavTfcontratosrvvnc_semcusto_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_semcusto_sel_Visible = 1;
         edtavTfcontratoservicosvnc_clonarlink_sel_Jsonclick = "";
         edtavTfcontratoservicosvnc_clonarlink_sel_Visible = 1;
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible = 1;
         edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick = "";
         edtavTfcontratosrvvnc_srvvnccntnum_Visible = 1;
         edtavTfcontratosrvvnc_prestadorapesnom_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_prestadorapesnom_sel_Visible = 1;
         edtavTfcontratosrvvnc_prestadorapesnom_Jsonclick = "";
         edtavTfcontratosrvvnc_prestadorapesnom_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 1;
         edtavTfcontratoservicosvnc_descricao_sel_Jsonclick = "";
         edtavTfcontratoservicosvnc_descricao_sel_Visible = 1;
         edtavTfcontratoservicosvnc_descricao_Jsonclick = "";
         edtavTfcontratoservicosvnc_descricao_Visible = 1;
         edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_servicosigla_sel_Visible = 1;
         edtavTfcontratosrvvnc_servicosigla_Jsonclick = "";
         edtavTfcontratosrvvnc_servicosigla_Visible = 1;
         Ddo_contratoservicosvnc_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosvnc_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosvnc_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosvnc_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosvnc_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratoservicosvnc_ativo_Datalisttype = "FixedValues";
         Ddo_contratoservicosvnc_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosvnc_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosvnc_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosvnc_ativo_Cls = "ColumnSettings";
         Ddo_contratoservicosvnc_ativo_Tooltip = "Op��es";
         Ddo_contratoservicosvnc_ativo_Caption = "";
         Ddo_contratosrvvnc_semcusto_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_semcusto_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_semcusto_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_semcusto_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_semcusto_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratosrvvnc_semcusto_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_semcusto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_semcusto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_semcusto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_semcusto_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_semcusto_Tooltip = "Op��es";
         Ddo_contratosrvvnc_semcusto_Caption = "";
         Ddo_contratoservicosvnc_clonarlink_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosvnc_clonarlink_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosvnc_clonarlink_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosvnc_clonarlink_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosvnc_clonarlink_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratoservicosvnc_clonarlink_Datalisttype = "FixedValues";
         Ddo_contratoservicosvnc_clonarlink_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_clonarlink_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosvnc_clonarlink_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_clonarlink_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosvnc_clonarlink_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosvnc_clonarlink_Cls = "ColumnSettings";
         Ddo_contratoservicosvnc_clonarlink_Tooltip = "Op��es";
         Ddo_contratoservicosvnc_clonarlink_Caption = "";
         Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_srvvnccntnum_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_srvvnccntnum_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_srvvnccntnum_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_srvvnccntnum_Datalistproc = "GetWWContratoServicosVncFilterData";
         Ddo_contratosrvvnc_srvvnccntnum_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_srvvnccntnum_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_srvvnccntnum_Filtertype = "Character";
         Ddo_contratosrvvnc_srvvnccntnum_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_srvvnccntnum_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_srvvnccntnum_Tooltip = "Op��es";
         Ddo_contratosrvvnc_srvvnccntnum_Caption = "";
         Ddo_contratosrvvnc_prestadorapesnom_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_prestadorapesnom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_prestadorapesnom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_prestadorapesnom_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_prestadorapesnom_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_prestadorapesnom_Datalistproc = "GetWWContratoServicosVncFilterData";
         Ddo_contratosrvvnc_prestadorapesnom_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_prestadorapesnom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_prestadorapesnom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_prestadorapesnom_Filtertype = "Character";
         Ddo_contratosrvvnc_prestadorapesnom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_prestadorapesnom_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_prestadorapesnom_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_prestadorapesnom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_prestadorapesnom_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_prestadorapesnom_Tooltip = "Op��es";
         Ddo_contratosrvvnc_prestadorapesnom_Caption = "";
         Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_servicovncsigla_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_servicovncsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_servicovncsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_servicovncsigla_Datalistproc = "GetWWContratoServicosVncFilterData";
         Ddo_contratosrvvnc_servicovncsigla_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_servicovncsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_servicovncsigla_Filtertype = "Character";
         Ddo_contratosrvvnc_servicovncsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_servicovncsigla_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_servicovncsigla_Tooltip = "Op��es";
         Ddo_contratosrvvnc_servicovncsigla_Caption = "";
         Ddo_contratosrvvnc_statusdmn_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratosrvvnc_statusdmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_statusdmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_statusdmn_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_statusdmn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_statusdmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_statusdmn_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_statusdmn_Tooltip = "Op��es";
         Ddo_contratosrvvnc_statusdmn_Caption = "";
         Ddo_contratoservicosvnc_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosvnc_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosvnc_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosvnc_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosvnc_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosvnc_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosvnc_descricao_Datalistproc = "GetWWContratoServicosVncFilterData";
         Ddo_contratoservicosvnc_descricao_Datalisttype = "Dynamic";
         Ddo_contratoservicosvnc_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosvnc_descricao_Filtertype = "Character";
         Ddo_contratoservicosvnc_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosvnc_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosvnc_descricao_Cls = "ColumnSettings";
         Ddo_contratoservicosvnc_descricao_Tooltip = "Op��es";
         Ddo_contratoservicosvnc_descricao_Caption = "";
         Ddo_contratosrvvnc_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_servicosigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_servicosigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_servicosigla_Datalistproc = "GetWWContratoServicosVncFilterData";
         Ddo_contratosrvvnc_servicosigla_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_servicosigla_Filtertype = "Character";
         Ddo_contratosrvvnc_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_servicosigla_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_servicosigla_Tooltip = "Op��es";
         Ddo_contratosrvvnc_servicosigla_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Regra de Vinculo entre Servi�os";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV20ContratoSrvVnc_ServicoSiglaTitleFilterData',fld:'vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV53ContratoServicosVnc_DescricaoTitleFilterData',fld:'vCONTRATOSERVICOSVNC_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoSrvVnc_StatusDmnTitleFilterData',fld:'vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData',fld:'vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData',fld:'vCONTRATOSRVVNC_PRESTADORAPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData',fld:'vCONTRATOSRVVNC_SRVVNCCNTNUMTITLEFILTERDATA',pic:'',nv:null},{av:'AV47ContratoServicosVnc_ClonarLinkTitleFilterData',fld:'vCONTRATOSERVICOSVNC_CLONARLINKTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ContratoSrvVnc_SemCustoTitleFilterData',fld:'vCONTRATOSRVVNC_SEMCUSTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV50ContratoServicosVnc_AtivoTitleFilterData',fld:'vCONTRATOSERVICOSVNC_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoSrvVnc_ServicoSigla_Titleformat',ctrl:'CONTRATOSRVVNC_SERVICOSIGLA',prop:'Titleformat'},{av:'edtContratoSrvVnc_ServicoSigla_Title',ctrl:'CONTRATOSRVVNC_SERVICOSIGLA',prop:'Title'},{av:'edtContratoServicosVnc_Descricao_Titleformat',ctrl:'CONTRATOSERVICOSVNC_DESCRICAO',prop:'Titleformat'},{av:'edtContratoServicosVnc_Descricao_Title',ctrl:'CONTRATOSERVICOSVNC_DESCRICAO',prop:'Title'},{av:'cmbContratoSrvVnc_StatusDmn'},{av:'edtContratoSrvVnc_ServicoVncSigla_Titleformat',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Titleformat'},{av:'edtContratoSrvVnc_ServicoVncSigla_Title',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Title'},{av:'edtContratoSrvVnc_PrestadoraPesNom_Titleformat',ctrl:'CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'Titleformat'},{av:'edtContratoSrvVnc_PrestadoraPesNom_Title',ctrl:'CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'Title'},{av:'edtContratoSrvVnc_SrvVncCntNum_Titleformat',ctrl:'CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'Titleformat'},{av:'edtContratoSrvVnc_SrvVncCntNum_Title',ctrl:'CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'Title'},{av:'cmbContratoServicosVnc_ClonarLink'},{av:'cmbContratoSrvVnc_SemCusto'},{av:'chkContratoServicosVnc_Ativo_Titleformat',ctrl:'CONTRATOSERVICOSVNC_ATIVO',prop:'Titleformat'},{av:'chkContratoServicosVnc_Ativo.Title.Text',ctrl:'CONTRATOSERVICOSVNC_ATIVO',prop:'Title'},{av:'AV41GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV42GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E12GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_servicosigla_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_servicosigla_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_servicosigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSVNC_DESCRICAO.ONOPTIONCLICKED","{handler:'E13GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosvnc_descricao_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosvnc_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosvnc_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED","{handler:'E14GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_statusdmn_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_statusdmn_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED","{handler:'E15GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_servicovncsigla_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_PRESTADORAPESNOM.ONOPTIONCLICKED","{handler:'E16GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SRVVNCCNTNUM.ONOPTIONCLICKED","{handler:'E17GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSVNC_CLONARLINK.ONOPTIONCLICKED","{handler:'E18GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosvnc_clonarlink_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SEMCUSTO.ONOPTIONCLICKED","{handler:'E19GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratosrvvnc_semcusto_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_semcusto_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSVNC_ATIVO.ONOPTIONCLICKED","{handler:'E20GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosvnc_ativo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosvnc_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosvnc_ativo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SortedStatus'},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_descricao_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SortedStatus'},{av:'Ddo_contratoservicosvnc_clonarlink_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_semcusto_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26GC2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV15Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E21GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E22GC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_PRESTADORAPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SRVVNCCNTNUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_CLONARLINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SEMCUSTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSVNC_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'FilteredText_set'},{av:'AV22TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SelectedValue_set'},{av:'AV54TFContratoServicosVnc_Descricao',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'FilteredText_set'},{av:'AV55TFContratoServicosVnc_Descricao_Sel',fld:'vTFCONTRATOSERVICOSVNC_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosvnc_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSVNC_DESCRICAO',prop:'SelectedValue_set'},{av:'AV26TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratosrvvnc_statusdmn_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SelectedValue_set'},{av:'AV29TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'FilteredText_set'},{av:'AV30TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SelectedValue_set'},{av:'AV33TFContratoSrvVnc_PrestadoraPesNom',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'FilteredText_set'},{av:'AV34TFContratoSrvVnc_PrestadoraPesNom_Sel',fld:'vTFCONTRATOSRVVNC_PRESTADORAPESNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_PRESTADORAPESNOM',prop:'SelectedValue_set'},{av:'AV44TFContratoSrvVnc_SrvVncCntNum',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM',pic:'',nv:''},{av:'Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'FilteredText_set'},{av:'AV45TFContratoSrvVnc_SrvVncCntNum_Sel',fld:'vTFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL',pic:'',nv:''},{av:'Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SRVVNCCNTNUM',prop:'SelectedValue_set'},{av:'AV48TFContratoServicosVnc_ClonarLink_Sel',fld:'vTFCONTRATOSERVICOSVNC_CLONARLINK_SEL',pic:'9',nv:0},{av:'Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSVNC_CLONARLINK',prop:'SelectedValue_set'},{av:'AV37TFContratoSrvVnc_SemCusto_Sel',fld:'vTFCONTRATOSRVVNC_SEMCUSTO_SEL',pic:'9',nv:0},{av:'Ddo_contratosrvvnc_semcusto_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SEMCUSTO',prop:'SelectedValue_set'},{av:'AV51TFContratoServicosVnc_Ativo_Sel',fld:'vTFCONTRATOSERVICOSVNC_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratoservicosvnc_ativo_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSVNC_ATIVO',prop:'SelectedValue_set'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E23GC2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratosrvvnc_servicosigla_Activeeventkey = "";
         Ddo_contratosrvvnc_servicosigla_Filteredtext_get = "";
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_get = "";
         Ddo_contratoservicosvnc_descricao_Activeeventkey = "";
         Ddo_contratoservicosvnc_descricao_Filteredtext_get = "";
         Ddo_contratoservicosvnc_descricao_Selectedvalue_get = "";
         Ddo_contratosrvvnc_statusdmn_Activeeventkey = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = "";
         Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey = "";
         Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get = "";
         Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get = "";
         Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey = "";
         Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get = "";
         Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get = "";
         Ddo_contratoservicosvnc_clonarlink_Activeeventkey = "";
         Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get = "";
         Ddo_contratosrvvnc_semcusto_Activeeventkey = "";
         Ddo_contratosrvvnc_semcusto_Selectedvalue_get = "";
         Ddo_contratoservicosvnc_ativo_Activeeventkey = "";
         Ddo_contratoservicosvnc_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV21TFContratoSrvVnc_ServicoSigla = "";
         AV22TFContratoSrvVnc_ServicoSigla_Sel = "";
         AV54TFContratoServicosVnc_Descricao = "";
         AV55TFContratoServicosVnc_Descricao_Sel = "";
         AV29TFContratoSrvVnc_ServicoVncSigla = "";
         AV30TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV44TFContratoSrvVnc_SrvVncCntNum = "";
         AV45TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = "";
         AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace = "";
         AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = "";
         AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = "";
         AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace = "";
         AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace = "";
         AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace = "";
         AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace = "";
         AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace = "";
         AV26TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV33TFContratoSrvVnc_PrestadoraPesNom = "";
         AV34TFContratoSrvVnc_PrestadoraPesNom_Sel = "";
         AV75Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV39DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20ContratoSrvVnc_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContratoServicosVnc_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoServicosVnc_ClonarLinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoSrvVnc_SemCustoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50ContratoServicosVnc_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratosrvvnc_servicosigla_Filteredtext_set = "";
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = "";
         Ddo_contratosrvvnc_servicosigla_Sortedstatus = "";
         Ddo_contratoservicosvnc_descricao_Filteredtext_set = "";
         Ddo_contratoservicosvnc_descricao_Selectedvalue_set = "";
         Ddo_contratoservicosvnc_descricao_Sortedstatus = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = "";
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set = "";
         Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set = "";
         Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set = "";
         Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set = "";
         Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus = "";
         Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set = "";
         Ddo_contratoservicosvnc_clonarlink_Sortedstatus = "";
         Ddo_contratosrvvnc_semcusto_Selectedvalue_set = "";
         Ddo_contratosrvvnc_semcusto_Sortedstatus = "";
         Ddo_contratoservicosvnc_ativo_Selectedvalue_set = "";
         Ddo_contratoservicosvnc_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Update = "";
         AV73Update_GXI = "";
         AV15Delete = "";
         AV74Delete_GXI = "";
         A922ContratoSrvVnc_ServicoSigla = "";
         A2108ContratoServicosVnc_Descricao = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1092ContratoSrvVnc_PrestadoraPesNom = "";
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = "";
         lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = "";
         lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = "";
         lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = "";
         lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = "";
         AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel = "";
         AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla = "";
         AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel = "";
         AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao = "";
         AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel = "";
         AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla = "";
         AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel = "";
         AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum = "";
         AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = "";
         AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = "";
         H00GC4_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00GC4_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         H00GC4_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         H00GC4_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         H00GC4_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         H00GC4_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         H00GC4_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         H00GC4_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         H00GC4_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         H00GC4_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GC4_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GC4_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GC4_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GC4_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         H00GC4_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         H00GC4_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         H00GC4_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         H00GC4_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         H00GC4_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         H00GC4_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00GC4_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00GC4_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         H00GC4_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         H00GC4_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         H00GC4_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         H00GC4_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00GC4_A1092ContratoSrvVnc_PrestadoraPesNom = new String[] {""} ;
         H00GC4_n1092ContratoSrvVnc_PrestadoraPesNom = new bool[] {false} ;
         H00GC7_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV25TFContratoSrvVnc_StatusDmn_SelsJson = "";
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoservicosvnctitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoservicosvnc__default(),
            new Object[][] {
                new Object[] {
               H00GC4_A915ContratoSrvVnc_CntSrvCod, H00GC4_A921ContratoSrvVnc_ServicoCod, H00GC4_n921ContratoSrvVnc_ServicoCod, H00GC4_A1589ContratoSrvVnc_SrvVncCntSrvCod, H00GC4_n1589ContratoSrvVnc_SrvVncCntSrvCod, H00GC4_A1628ContratoSrvVnc_SrvVncCntCod, H00GC4_n1628ContratoSrvVnc_SrvVncCntCod, H00GC4_A923ContratoSrvVnc_ServicoVncCod, H00GC4_n923ContratoSrvVnc_ServicoVncCod, H00GC4_A1453ContratoServicosVnc_Ativo,
               H00GC4_n1453ContratoServicosVnc_Ativo, H00GC4_A1090ContratoSrvVnc_SemCusto, H00GC4_n1090ContratoSrvVnc_SemCusto, H00GC4_A1818ContratoServicosVnc_ClonarLink, H00GC4_n1818ContratoServicosVnc_ClonarLink, H00GC4_A1631ContratoSrvVnc_SrvVncCntNum, H00GC4_n1631ContratoSrvVnc_SrvVncCntNum, H00GC4_A924ContratoSrvVnc_ServicoVncSigla, H00GC4_n924ContratoSrvVnc_ServicoVncSigla, H00GC4_A1084ContratoSrvVnc_StatusDmn,
               H00GC4_n1084ContratoSrvVnc_StatusDmn, H00GC4_A2108ContratoServicosVnc_Descricao, H00GC4_n2108ContratoServicosVnc_Descricao, H00GC4_A922ContratoSrvVnc_ServicoSigla, H00GC4_n922ContratoSrvVnc_ServicoSigla, H00GC4_A917ContratoSrvVnc_Codigo, H00GC4_A1092ContratoSrvVnc_PrestadoraPesNom, H00GC4_n1092ContratoSrvVnc_PrestadoraPesNom
               }
               , new Object[] {
               H00GC7_AGRID_nRecordCount
               }
            }
         );
         AV75Pgmname = "WWContratoServicosVnc";
         /* GeneXus formulas. */
         AV75Pgmname = "WWContratoServicosVnc";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_35 ;
      private short nGXsfl_35_idx=1 ;
      private short AV12OrderedBy ;
      private short AV48TFContratoServicosVnc_ClonarLink_Sel ;
      private short AV37TFContratoSrvVnc_SemCusto_Sel ;
      private short AV51TFContratoServicosVnc_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_35_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ;
      private short AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ;
      private short AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ;
      private short edtContratoSrvVnc_ServicoSigla_Titleformat ;
      private short edtContratoServicosVnc_Descricao_Titleformat ;
      private short cmbContratoSrvVnc_StatusDmn_Titleformat ;
      private short edtContratoSrvVnc_ServicoVncSigla_Titleformat ;
      private short edtContratoSrvVnc_PrestadoraPesNom_Titleformat ;
      private short edtContratoSrvVnc_SrvVncCntNum_Titleformat ;
      private short cmbContratoServicosVnc_ClonarLink_Titleformat ;
      private short cmbContratoSrvVnc_SemCusto_Titleformat ;
      private short chkContratoServicosVnc_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A917ContratoSrvVnc_Codigo ;
      private int AV17ContratoSrvVnc_CntSrvCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicosvnc_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_prestadorapesnom_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_srvvnccntnum_Datalistupdateminimumcharacters ;
      private int edtavTfcontratosrvvnc_servicosigla_Visible ;
      private int edtavTfcontratosrvvnc_servicosigla_sel_Visible ;
      private int edtavTfcontratoservicosvnc_descricao_Visible ;
      private int edtavTfcontratoservicosvnc_descricao_sel_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_sel_Visible ;
      private int edtavTfcontratosrvvnc_prestadorapesnom_Visible ;
      private int edtavTfcontratosrvvnc_prestadorapesnom_sel_Visible ;
      private int edtavTfcontratosrvvnc_srvvnccntnum_Visible ;
      private int edtavTfcontratosrvvnc_srvvnccntnum_sel_Visible ;
      private int edtavTfcontratoservicosvnc_clonarlink_sel_Visible ;
      private int edtavTfcontratosrvvnc_semcusto_sel_Visible ;
      private int edtavTfcontratoservicosvnc_ativo_sel_Visible ;
      private int edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV40PageToGo ;
      private int AV76GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV41GridCurrentPage ;
      private long AV42GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratosrvvnc_servicosigla_Activeeventkey ;
      private String Ddo_contratosrvvnc_servicosigla_Filteredtext_get ;
      private String Ddo_contratosrvvnc_servicosigla_Selectedvalue_get ;
      private String Ddo_contratoservicosvnc_descricao_Activeeventkey ;
      private String Ddo_contratoservicosvnc_descricao_Filteredtext_get ;
      private String Ddo_contratoservicosvnc_descricao_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_statusdmn_Activeeventkey ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Activeeventkey ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Activeeventkey ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_get ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Activeeventkey ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_get ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_get ;
      private String Ddo_contratoservicosvnc_clonarlink_Activeeventkey ;
      private String Ddo_contratoservicosvnc_clonarlink_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_semcusto_Activeeventkey ;
      private String Ddo_contratosrvvnc_semcusto_Selectedvalue_get ;
      private String Ddo_contratoservicosvnc_ativo_Activeeventkey ;
      private String Ddo_contratoservicosvnc_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_35_idx="0001" ;
      private String AV21TFContratoSrvVnc_ServicoSigla ;
      private String AV22TFContratoSrvVnc_ServicoSigla_Sel ;
      private String AV29TFContratoSrvVnc_ServicoVncSigla ;
      private String AV30TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String AV44TFContratoSrvVnc_SrvVncCntNum ;
      private String AV45TFContratoSrvVnc_SrvVncCntNum_Sel ;
      private String AV33TFContratoSrvVnc_PrestadoraPesNom ;
      private String AV34TFContratoSrvVnc_PrestadoraPesNom_Sel ;
      private String AV75Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratosrvvnc_servicosigla_Caption ;
      private String Ddo_contratosrvvnc_servicosigla_Tooltip ;
      private String Ddo_contratosrvvnc_servicosigla_Cls ;
      private String Ddo_contratosrvvnc_servicosigla_Filteredtext_set ;
      private String Ddo_contratosrvvnc_servicosigla_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_servicosigla_Sortedstatus ;
      private String Ddo_contratosrvvnc_servicosigla_Filtertype ;
      private String Ddo_contratosrvvnc_servicosigla_Datalisttype ;
      private String Ddo_contratosrvvnc_servicosigla_Datalistproc ;
      private String Ddo_contratosrvvnc_servicosigla_Sortasc ;
      private String Ddo_contratosrvvnc_servicosigla_Sortdsc ;
      private String Ddo_contratosrvvnc_servicosigla_Loadingdata ;
      private String Ddo_contratosrvvnc_servicosigla_Cleanfilter ;
      private String Ddo_contratosrvvnc_servicosigla_Noresultsfound ;
      private String Ddo_contratosrvvnc_servicosigla_Searchbuttontext ;
      private String Ddo_contratoservicosvnc_descricao_Caption ;
      private String Ddo_contratoservicosvnc_descricao_Tooltip ;
      private String Ddo_contratoservicosvnc_descricao_Cls ;
      private String Ddo_contratoservicosvnc_descricao_Filteredtext_set ;
      private String Ddo_contratoservicosvnc_descricao_Selectedvalue_set ;
      private String Ddo_contratoservicosvnc_descricao_Dropdownoptionstype ;
      private String Ddo_contratoservicosvnc_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosvnc_descricao_Sortedstatus ;
      private String Ddo_contratoservicosvnc_descricao_Filtertype ;
      private String Ddo_contratoservicosvnc_descricao_Datalisttype ;
      private String Ddo_contratoservicosvnc_descricao_Datalistproc ;
      private String Ddo_contratoservicosvnc_descricao_Sortasc ;
      private String Ddo_contratoservicosvnc_descricao_Sortdsc ;
      private String Ddo_contratoservicosvnc_descricao_Loadingdata ;
      private String Ddo_contratoservicosvnc_descricao_Cleanfilter ;
      private String Ddo_contratoservicosvnc_descricao_Noresultsfound ;
      private String Ddo_contratoservicosvnc_descricao_Searchbuttontext ;
      private String Ddo_contratosrvvnc_statusdmn_Caption ;
      private String Ddo_contratosrvvnc_statusdmn_Tooltip ;
      private String Ddo_contratosrvvnc_statusdmn_Cls ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_statusdmn_Sortedstatus ;
      private String Ddo_contratosrvvnc_statusdmn_Datalisttype ;
      private String Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_statusdmn_Sortasc ;
      private String Ddo_contratosrvvnc_statusdmn_Sortdsc ;
      private String Ddo_contratosrvvnc_statusdmn_Cleanfilter ;
      private String Ddo_contratosrvvnc_statusdmn_Searchbuttontext ;
      private String Ddo_contratosrvvnc_servicovncsigla_Caption ;
      private String Ddo_contratosrvvnc_servicovncsigla_Tooltip ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cls ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortedstatus ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filtertype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalisttype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalistproc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortasc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortdsc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Loadingdata ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cleanfilter ;
      private String Ddo_contratosrvvnc_servicovncsigla_Noresultsfound ;
      private String Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Caption ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Tooltip ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Cls ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Filteredtext_set ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Filtertype ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Datalisttype ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Datalistproc ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Loadingdata ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Cleanfilter ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Noresultsfound ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Searchbuttontext ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Caption ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Tooltip ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Cls ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filteredtext_set ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortedstatus ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Filtertype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Datalisttype ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Datalistproc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortasc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Sortdsc ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Loadingdata ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Cleanfilter ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Noresultsfound ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Searchbuttontext ;
      private String Ddo_contratoservicosvnc_clonarlink_Caption ;
      private String Ddo_contratoservicosvnc_clonarlink_Tooltip ;
      private String Ddo_contratoservicosvnc_clonarlink_Cls ;
      private String Ddo_contratoservicosvnc_clonarlink_Selectedvalue_set ;
      private String Ddo_contratoservicosvnc_clonarlink_Dropdownoptionstype ;
      private String Ddo_contratoservicosvnc_clonarlink_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosvnc_clonarlink_Sortedstatus ;
      private String Ddo_contratoservicosvnc_clonarlink_Datalisttype ;
      private String Ddo_contratoservicosvnc_clonarlink_Datalistfixedvalues ;
      private String Ddo_contratoservicosvnc_clonarlink_Sortasc ;
      private String Ddo_contratoservicosvnc_clonarlink_Sortdsc ;
      private String Ddo_contratoservicosvnc_clonarlink_Cleanfilter ;
      private String Ddo_contratoservicosvnc_clonarlink_Searchbuttontext ;
      private String Ddo_contratosrvvnc_semcusto_Caption ;
      private String Ddo_contratosrvvnc_semcusto_Tooltip ;
      private String Ddo_contratosrvvnc_semcusto_Cls ;
      private String Ddo_contratosrvvnc_semcusto_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_semcusto_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_semcusto_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_semcusto_Sortedstatus ;
      private String Ddo_contratosrvvnc_semcusto_Datalisttype ;
      private String Ddo_contratosrvvnc_semcusto_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_semcusto_Sortasc ;
      private String Ddo_contratosrvvnc_semcusto_Sortdsc ;
      private String Ddo_contratosrvvnc_semcusto_Cleanfilter ;
      private String Ddo_contratosrvvnc_semcusto_Searchbuttontext ;
      private String Ddo_contratoservicosvnc_ativo_Caption ;
      private String Ddo_contratoservicosvnc_ativo_Tooltip ;
      private String Ddo_contratoservicosvnc_ativo_Cls ;
      private String Ddo_contratoservicosvnc_ativo_Selectedvalue_set ;
      private String Ddo_contratoservicosvnc_ativo_Dropdownoptionstype ;
      private String Ddo_contratoservicosvnc_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosvnc_ativo_Sortedstatus ;
      private String Ddo_contratoservicosvnc_ativo_Datalisttype ;
      private String Ddo_contratoservicosvnc_ativo_Datalistfixedvalues ;
      private String Ddo_contratoservicosvnc_ativo_Sortasc ;
      private String Ddo_contratoservicosvnc_ativo_Sortdsc ;
      private String Ddo_contratoservicosvnc_ativo_Cleanfilter ;
      private String Ddo_contratoservicosvnc_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontratosrvvnc_servicosigla_Internalname ;
      private String edtavTfcontratosrvvnc_servicosigla_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicosigla_sel_Internalname ;
      private String edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick ;
      private String edtavTfcontratoservicosvnc_descricao_Internalname ;
      private String edtavTfcontratoservicosvnc_descricao_Jsonclick ;
      private String edtavTfcontratoservicosvnc_descricao_sel_Internalname ;
      private String edtavTfcontratoservicosvnc_descricao_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_prestadorapesnom_Internalname ;
      private String edtavTfcontratosrvvnc_prestadorapesnom_Jsonclick ;
      private String edtavTfcontratosrvvnc_prestadorapesnom_sel_Internalname ;
      private String edtavTfcontratosrvvnc_prestadorapesnom_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_Internalname ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_Jsonclick ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_sel_Internalname ;
      private String edtavTfcontratosrvvnc_srvvnccntnum_sel_Jsonclick ;
      private String edtavTfcontratoservicosvnc_clonarlink_sel_Internalname ;
      private String edtavTfcontratoservicosvnc_clonarlink_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_semcusto_sel_Internalname ;
      private String edtavTfcontratosrvvnc_semcusto_sel_Jsonclick ;
      private String edtavTfcontratoservicosvnc_ativo_sel_Internalname ;
      private String edtavTfcontratoservicosvnc_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosvnc_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_prestadorapesnomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_srvvnccntnumtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosvnc_clonarlinktitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_semcustotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosvnc_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoSrvVnc_Codigo_Internalname ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String edtContratoSrvVnc_ServicoSigla_Internalname ;
      private String edtContratoServicosVnc_Descricao_Internalname ;
      private String cmbContratoSrvVnc_StatusDmn_Internalname ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String edtContratoSrvVnc_ServicoVncSigla_Internalname ;
      private String A1092ContratoSrvVnc_PrestadoraPesNom ;
      private String edtContratoSrvVnc_PrestadoraPesNom_Internalname ;
      private String A1631ContratoSrvVnc_SrvVncCntNum ;
      private String edtContratoSrvVnc_SrvVncCntNum_Internalname ;
      private String cmbContratoServicosVnc_ClonarLink_Internalname ;
      private String cmbContratoSrvVnc_SemCusto_Internalname ;
      private String chkContratoServicosVnc_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ;
      private String lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ;
      private String lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ;
      private String lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ;
      private String AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ;
      private String AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ;
      private String AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ;
      private String AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ;
      private String AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ;
      private String AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ;
      private String AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ;
      private String AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratosrvvnc_servicosigla_Internalname ;
      private String Ddo_contratoservicosvnc_descricao_Internalname ;
      private String Ddo_contratosrvvnc_statusdmn_Internalname ;
      private String Ddo_contratosrvvnc_servicovncsigla_Internalname ;
      private String Ddo_contratosrvvnc_prestadorapesnom_Internalname ;
      private String Ddo_contratosrvvnc_srvvnccntnum_Internalname ;
      private String Ddo_contratoservicosvnc_clonarlink_Internalname ;
      private String Ddo_contratosrvvnc_semcusto_Internalname ;
      private String Ddo_contratoservicosvnc_ativo_Internalname ;
      private String edtContratoSrvVnc_ServicoSigla_Title ;
      private String edtContratoServicosVnc_Descricao_Title ;
      private String edtContratoSrvVnc_ServicoVncSigla_Title ;
      private String edtContratoSrvVnc_PrestadoraPesNom_Title ;
      private String edtContratoSrvVnc_SrvVncCntNum_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoservicosvnctitle_Internalname ;
      private String lblContratoservicosvnctitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_35_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoSrvVnc_Codigo_Jsonclick ;
      private String edtContratoSrvVnc_ServicoSigla_Jsonclick ;
      private String edtContratoServicosVnc_Descricao_Jsonclick ;
      private String cmbContratoSrvVnc_StatusDmn_Jsonclick ;
      private String edtContratoSrvVnc_ServicoVncSigla_Jsonclick ;
      private String edtContratoSrvVnc_PrestadoraPesNom_Jsonclick ;
      private String edtContratoSrvVnc_SrvVncCntNum_Jsonclick ;
      private String cmbContratoServicosVnc_ClonarLink_Jsonclick ;
      private String cmbContratoSrvVnc_SemCusto_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratosrvvnc_servicosigla_Includesortasc ;
      private bool Ddo_contratosrvvnc_servicosigla_Includesortdsc ;
      private bool Ddo_contratosrvvnc_servicosigla_Includefilter ;
      private bool Ddo_contratosrvvnc_servicosigla_Filterisrange ;
      private bool Ddo_contratosrvvnc_servicosigla_Includedatalist ;
      private bool Ddo_contratoservicosvnc_descricao_Includesortasc ;
      private bool Ddo_contratoservicosvnc_descricao_Includesortdsc ;
      private bool Ddo_contratoservicosvnc_descricao_Includefilter ;
      private bool Ddo_contratoservicosvnc_descricao_Filterisrange ;
      private bool Ddo_contratoservicosvnc_descricao_Includedatalist ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortasc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortdsc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includefilter ;
      private bool Ddo_contratosrvvnc_statusdmn_Includedatalist ;
      private bool Ddo_contratosrvvnc_statusdmn_Allowmultipleselection ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortasc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortdsc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includefilter ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Filterisrange ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includedatalist ;
      private bool Ddo_contratosrvvnc_prestadorapesnom_Includesortasc ;
      private bool Ddo_contratosrvvnc_prestadorapesnom_Includesortdsc ;
      private bool Ddo_contratosrvvnc_prestadorapesnom_Includefilter ;
      private bool Ddo_contratosrvvnc_prestadorapesnom_Filterisrange ;
      private bool Ddo_contratosrvvnc_prestadorapesnom_Includedatalist ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includesortasc ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includesortdsc ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includefilter ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Filterisrange ;
      private bool Ddo_contratosrvvnc_srvvnccntnum_Includedatalist ;
      private bool Ddo_contratoservicosvnc_clonarlink_Includesortasc ;
      private bool Ddo_contratoservicosvnc_clonarlink_Includesortdsc ;
      private bool Ddo_contratoservicosvnc_clonarlink_Includefilter ;
      private bool Ddo_contratoservicosvnc_clonarlink_Includedatalist ;
      private bool Ddo_contratosrvvnc_semcusto_Includesortasc ;
      private bool Ddo_contratosrvvnc_semcusto_Includesortdsc ;
      private bool Ddo_contratosrvvnc_semcusto_Includefilter ;
      private bool Ddo_contratosrvvnc_semcusto_Includedatalist ;
      private bool Ddo_contratoservicosvnc_ativo_Includesortasc ;
      private bool Ddo_contratoservicosvnc_ativo_Includesortdsc ;
      private bool Ddo_contratoservicosvnc_ativo_Includefilter ;
      private bool Ddo_contratoservicosvnc_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14Update_IsBlob ;
      private bool AV15Delete_IsBlob ;
      private String AV25TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV54TFContratoServicosVnc_Descricao ;
      private String AV55TFContratoServicosVnc_Descricao_Sel ;
      private String AV23ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace ;
      private String AV56ddo_ContratoServicosVnc_DescricaoTitleControlIdToReplace ;
      private String AV27ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ;
      private String AV31ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ;
      private String AV35ddo_ContratoSrvVnc_PrestadoraPesNomTitleControlIdToReplace ;
      private String AV46ddo_ContratoSrvVnc_SrvVncCntNumTitleControlIdToReplace ;
      private String AV49ddo_ContratoServicosVnc_ClonarLinkTitleControlIdToReplace ;
      private String AV38ddo_ContratoSrvVnc_SemCustoTitleControlIdToReplace ;
      private String AV52ddo_ContratoServicosVnc_AtivoTitleControlIdToReplace ;
      private String AV73Update_GXI ;
      private String AV74Delete_GXI ;
      private String A2108ContratoServicosVnc_Descricao ;
      private String lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ;
      private String AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ;
      private String AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ;
      private String AV14Update ;
      private String AV15Delete ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbContratoSrvVnc_StatusDmn ;
      private GXCombobox cmbContratoServicosVnc_ClonarLink ;
      private GXCombobox cmbContratoSrvVnc_SemCusto ;
      private GXCheckbox chkContratoServicosVnc_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00GC4_A915ContratoSrvVnc_CntSrvCod ;
      private int[] H00GC4_A921ContratoSrvVnc_ServicoCod ;
      private bool[] H00GC4_n921ContratoSrvVnc_ServicoCod ;
      private int[] H00GC4_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] H00GC4_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] H00GC4_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] H00GC4_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] H00GC4_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00GC4_n923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00GC4_A1453ContratoServicosVnc_Ativo ;
      private bool[] H00GC4_n1453ContratoServicosVnc_Ativo ;
      private bool[] H00GC4_A1090ContratoSrvVnc_SemCusto ;
      private bool[] H00GC4_n1090ContratoSrvVnc_SemCusto ;
      private bool[] H00GC4_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] H00GC4_n1818ContratoServicosVnc_ClonarLink ;
      private String[] H00GC4_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] H00GC4_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] H00GC4_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] H00GC4_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] H00GC4_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00GC4_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00GC4_A2108ContratoServicosVnc_Descricao ;
      private bool[] H00GC4_n2108ContratoServicosVnc_Descricao ;
      private String[] H00GC4_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] H00GC4_n922ContratoSrvVnc_ServicoSigla ;
      private int[] H00GC4_A917ContratoSrvVnc_Codigo ;
      private String[] H00GC4_A1092ContratoSrvVnc_PrestadoraPesNom ;
      private bool[] H00GC4_n1092ContratoSrvVnc_PrestadoraPesNom ;
      private long[] H00GC7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ContratoSrvVnc_ServicoSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53ContratoServicosVnc_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ContratoSrvVnc_StatusDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28ContratoSrvVnc_ServicoVncSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32ContratoSrvVnc_PrestadoraPesNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43ContratoSrvVnc_SrvVncCntNumTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ContratoServicosVnc_ClonarLinkTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ContratoSrvVnc_SemCustoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50ContratoServicosVnc_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV39DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoservicosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GC4( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                             String AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                             String AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                             String AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                             String AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                             int AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                             String AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                             String AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                             String AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                             String AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                             short AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                             short AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                             short AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1818ContratoServicosVnc_ClonarLink ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             String AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                             String AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                             String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_ClonarLink], T5.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T6.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoServicosVnc_Descricao], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T1.[ContratoSrvVnc_Codigo], COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') AS ContratoSrvVnc_PrestadoraPesNom";
         sFromString = " FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo] = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (Not ( (@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         sWhereString = sWhereString + " and ((@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla]";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla] DESC";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_Descricao]";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_Descricao] DESC";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_StatusDmn]";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_StatusDmn] DESC";
         }
         else if ( ( AV12OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Servico_Sigla]";
         }
         else if ( ( AV12OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Servico_Sigla] DESC";
         }
         else if ( ( AV12OrderedBy == 5 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contrato_Numero]";
         }
         else if ( ( AV12OrderedBy == 5 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contrato_Numero] DESC";
         }
         else if ( ( AV12OrderedBy == 6 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_ClonarLink]";
         }
         else if ( ( AV12OrderedBy == 6 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_ClonarLink] DESC";
         }
         else if ( ( AV12OrderedBy == 7 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_SemCusto]";
         }
         else if ( ( AV12OrderedBy == 7 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_SemCusto] DESC";
         }
         else if ( ( AV12OrderedBy == 8 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_Ativo]";
         }
         else if ( ( AV12OrderedBy == 8 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosVnc_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GC7( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels ,
                                             String AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel ,
                                             String AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla ,
                                             String AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel ,
                                             String AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao ,
                                             int AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count ,
                                             String AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel ,
                                             String AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla ,
                                             String AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel ,
                                             String AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum ,
                                             short AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel ,
                                             short AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel ,
                                             short AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1818ContratoServicosVnc_ClonarLink ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             String AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel ,
                                             String AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom ,
                                             String A1092ContratoSrvVnc_PrestadoraPesNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) < 910000 THEN COALESCE( T9.[Contratada_Sigla], '') WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 910000 THEN 'Criador da SS' WHEN COALESCE( T8.[ContratoSrvVnc_PrestadoraCod], 0) = 920000 THEN 'Criador da OS' END AS ContratoSrvVnc_PrestadoraPesNom, T8.[ContratoSrvVnc_Codigo] FROM ([ContratoServicosVnc] T8 WITH (NOLOCK) LEFT JOIN (SELECT [Contratada_Sigla] AS Contratada_Sigla, [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ) T9 ON T9.[Contratada_Codigo] = T8.[ContratoSrvVnc_PrestadoraCod]) ) T7 ON T7.[ContratoSrvVnc_Codigo] = T1.[ContratoSrvVnc_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( (@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') and ( Not (@AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom = ''))) or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') like @lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom))";
         scmdbuf = scmdbuf + " and ((@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel = '') or ( COALESCE( T7.[ContratoSrvVnc_PrestadoraPesNom], '') = @AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel))";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV63WWContratoServicosVncDS_5_Tfcontratosrvvnc_statusdmn_sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] like @lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contrato_Numero] = @AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 1)";
         }
         if ( AV70WWContratoServicosVncDS_12_Tfcontratoservicosvnc_clonarlink_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_ClonarLink] = 0)";
         }
         if ( AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV71WWContratoServicosVncDS_13_Tfcontratosrvvnc_semcusto_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV72WWContratoServicosVncDS_14_Tfcontratoservicosvnc_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 4 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 4 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 5 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 5 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 6 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 6 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 7 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 7 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 8 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 8 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GC4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_H00GC7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (bool)dynConstraints[19] , (bool)dynConstraints[20] , (short)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GC4 ;
          prmH00GC4 = new Object[] {
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GC7 ;
          prmH00GC7 = new Object[] {
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWContratoServicosVncDS_8_Tfcontratosrvvnc_prestadorapesnom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosVncDS_9_Tfcontratosrvvnc_prestadorapesnom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWContratoServicosVncDS_1_Tfcontratosrvvnc_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV60WWContratoServicosVncDS_2_Tfcontratosrvvnc_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV61WWContratoServicosVncDS_3_Tfcontratoservicosvnc_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWContratoServicosVncDS_4_Tfcontratoservicosvnc_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV64WWContratoServicosVncDS_6_Tfcontratosrvvnc_servicovncsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV65WWContratoServicosVncDS_7_Tfcontratosrvvnc_servicovncsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV68WWContratoServicosVncDS_10_Tfcontratosrvvnc_srvvnccntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWContratoServicosVncDS_11_Tfcontratosrvvnc_srvvnccntnum_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GC4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GC4,11,0,true,false )
             ,new CursorDef("H00GC7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GC7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 20) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
