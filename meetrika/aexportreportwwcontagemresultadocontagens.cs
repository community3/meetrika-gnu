/*
               File: ExportReportWWContagemResultadoContagens
        Description: Export Report WWContagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:0.54
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportreportwwcontagemresultadocontagens : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV94TFContratada_AreaTrabalhoDes = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV95TFContratada_AreaTrabalhoDes_Sel = GetNextPar( );
                  AV96TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV97TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV98TFContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV99TFContagemResultado_DataCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV100TFContagemResultado_HoraCnt = GetNextPar( );
                  AV101TFContagemResultado_HoraCnt_Sel = GetNextPar( );
                  AV102TFContagemResultado_OsFsOsFm = GetNextPar( );
                  AV103TFContagemResultado_OsFsOsFm_Sel = GetNextPar( );
                  AV104TFContagemrResultado_SistemaSigla = GetNextPar( );
                  AV105TFContagemrResultado_SistemaSigla_Sel = GetNextPar( );
                  AV106TFContagemResultado_ContadorFMNom = GetNextPar( );
                  AV107TFContagemResultado_ContadorFMNom_Sel = GetNextPar( );
                  AV108TFContagemResultado_StatusCnt_SelsJson = GetNextPar( );
                  AV112TFContagemResultadoContagens_Esforco = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV113TFContagemResultadoContagens_Esforco_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV114TFContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
                  AV115TFContagemResultado_PFBFS_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV116TFContagemResultado_PFLFS = NumberUtil.Val( GetNextPar( ), ".");
                  AV117TFContagemResultado_PFLFS_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV118TFContagemResultado_PFBFM = NumberUtil.Val( GetNextPar( ), ".");
                  AV119TFContagemResultado_PFBFM_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV120TFContagemResultado_PFLFM = NumberUtil.Val( GetNextPar( ), ".");
                  AV121TFContagemResultado_PFLFM_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV43GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aexportreportwwcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aexportreportwwcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TFContratada_AreaTrabalhoDes ,
                           String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP2_TFContagemResultado_DataDmn ,
                           DateTime aP3_TFContagemResultado_DataDmn_To ,
                           DateTime aP4_TFContagemResultado_DataCnt ,
                           DateTime aP5_TFContagemResultado_DataCnt_To ,
                           String aP6_TFContagemResultado_HoraCnt ,
                           String aP7_TFContagemResultado_HoraCnt_Sel ,
                           String aP8_TFContagemResultado_OsFsOsFm ,
                           String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP10_TFContagemrResultado_SistemaSigla ,
                           String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP12_TFContagemResultado_ContadorFMNom ,
                           String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                           String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                           short aP15_TFContagemResultadoContagens_Esforco ,
                           short aP16_TFContagemResultadoContagens_Esforco_To ,
                           decimal aP17_TFContagemResultado_PFBFS ,
                           decimal aP18_TFContagemResultado_PFBFS_To ,
                           decimal aP19_TFContagemResultado_PFLFS ,
                           decimal aP20_TFContagemResultado_PFLFS_To ,
                           decimal aP21_TFContagemResultado_PFBFM ,
                           decimal aP22_TFContagemResultado_PFBFM_To ,
                           decimal aP23_TFContagemResultado_PFLFM ,
                           decimal aP24_TFContagemResultado_PFLFM_To ,
                           short aP25_OrderedBy ,
                           bool aP26_OrderedDsc ,
                           String aP27_GridStateXML )
      {
         this.AV94TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         this.AV95TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         this.AV96TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         this.AV97TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         this.AV98TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         this.AV99TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         this.AV100TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         this.AV101TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         this.AV102TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV103TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV104TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         this.AV105TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         this.AV106TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         this.AV107TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         this.AV108TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         this.AV112TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         this.AV113TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         this.AV114TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         this.AV115TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         this.AV116TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         this.AV117TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         this.AV118TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         this.AV119TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         this.AV120TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         this.AV121TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         this.AV10OrderedBy = aP25_OrderedBy;
         this.AV11OrderedDsc = aP26_OrderedDsc;
         this.AV43GridStateXML = aP27_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_TFContratada_AreaTrabalhoDes ,
                                 String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP2_TFContagemResultado_DataDmn ,
                                 DateTime aP3_TFContagemResultado_DataDmn_To ,
                                 DateTime aP4_TFContagemResultado_DataCnt ,
                                 DateTime aP5_TFContagemResultado_DataCnt_To ,
                                 String aP6_TFContagemResultado_HoraCnt ,
                                 String aP7_TFContagemResultado_HoraCnt_Sel ,
                                 String aP8_TFContagemResultado_OsFsOsFm ,
                                 String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP10_TFContagemrResultado_SistemaSigla ,
                                 String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP12_TFContagemResultado_ContadorFMNom ,
                                 String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                                 String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                                 short aP15_TFContagemResultadoContagens_Esforco ,
                                 short aP16_TFContagemResultadoContagens_Esforco_To ,
                                 decimal aP17_TFContagemResultado_PFBFS ,
                                 decimal aP18_TFContagemResultado_PFBFS_To ,
                                 decimal aP19_TFContagemResultado_PFLFS ,
                                 decimal aP20_TFContagemResultado_PFLFS_To ,
                                 decimal aP21_TFContagemResultado_PFBFM ,
                                 decimal aP22_TFContagemResultado_PFBFM_To ,
                                 decimal aP23_TFContagemResultado_PFLFM ,
                                 decimal aP24_TFContagemResultado_PFLFM_To ,
                                 short aP25_OrderedBy ,
                                 bool aP26_OrderedDsc ,
                                 String aP27_GridStateXML )
      {
         aexportreportwwcontagemresultadocontagens objaexportreportwwcontagemresultadocontagens;
         objaexportreportwwcontagemresultadocontagens = new aexportreportwwcontagemresultadocontagens();
         objaexportreportwwcontagemresultadocontagens.AV94TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         objaexportreportwwcontagemresultadocontagens.AV95TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         objaexportreportwwcontagemresultadocontagens.AV96TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         objaexportreportwwcontagemresultadocontagens.AV97TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         objaexportreportwwcontagemresultadocontagens.AV98TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         objaexportreportwwcontagemresultadocontagens.AV99TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         objaexportreportwwcontagemresultadocontagens.AV100TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         objaexportreportwwcontagemresultadocontagens.AV101TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         objaexportreportwwcontagemresultadocontagens.AV102TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         objaexportreportwwcontagemresultadocontagens.AV103TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         objaexportreportwwcontagemresultadocontagens.AV104TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         objaexportreportwwcontagemresultadocontagens.AV105TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         objaexportreportwwcontagemresultadocontagens.AV106TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         objaexportreportwwcontagemresultadocontagens.AV107TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         objaexportreportwwcontagemresultadocontagens.AV108TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         objaexportreportwwcontagemresultadocontagens.AV112TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         objaexportreportwwcontagemresultadocontagens.AV113TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         objaexportreportwwcontagemresultadocontagens.AV114TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         objaexportreportwwcontagemresultadocontagens.AV115TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         objaexportreportwwcontagemresultadocontagens.AV116TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         objaexportreportwwcontagemresultadocontagens.AV117TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         objaexportreportwwcontagemresultadocontagens.AV118TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         objaexportreportwwcontagemresultadocontagens.AV119TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         objaexportreportwwcontagemresultadocontagens.AV120TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         objaexportreportwwcontagemresultadocontagens.AV121TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         objaexportreportwwcontagemresultadocontagens.AV10OrderedBy = aP25_OrderedBy;
         objaexportreportwwcontagemresultadocontagens.AV11OrderedDsc = aP26_OrderedDsc;
         objaexportreportwwcontagemresultadocontagens.AV43GridStateXML = aP27_GridStateXML;
         objaexportreportwwcontagemresultadocontagens.context.SetSubmitInitialConfig(context);
         objaexportreportwwcontagemresultadocontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportreportwwcontagemresultadocontagens);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportreportwwcontagemresultadocontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV59Flag = (short)(NumberUtil.Val( AV60Websession.Get("TodasAsAreas"), "."));
            AV82Contratadas.FromXml(AV60Websession.Get("Contratadas"), "Collection");
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H340( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         AV83Usuario_EhGestor = AV9WWPContext.gxTpr_Userehgestor;
         H340( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Resumo de Esfor�o", 5, Gx_line+5, 1035, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         AV44GridState.gxTpr_Dynamicfilters.FromXml(AV43GridStateXML, "");
         if ( AV44GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV44GridState.gxTpr_Dynamicfilters.Item(1));
            AV13DynamicFiltersSelector1 = AV45GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV88DynamicFiltersOperator1 = AV45GridStateDynamicFilter.gxTpr_Operator;
               AV14ContagemResultado_OsFsOsFm1 = AV45GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14ContagemResultado_OsFsOsFm1)) )
               {
                  if ( AV88DynamicFiltersOperator1 == 0 )
                  {
                     AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV88DynamicFiltersOperator1 == 1 )
                  {
                     AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV88DynamicFiltersOperator1 == 2 )
                  {
                     AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV46ContagemResultado_OsFsOsFm = AV14ContagemResultado_OsFsOsFm1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV15ContagemResultado_DataCnt1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV16ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV15ContagemResultado_DataCnt1) )
               {
                  AV47FilterContagemResultado_DataCntDescription = "Data Contagem";
                  AV48ContagemResultado_DataCnt = AV15ContagemResultado_DataCnt1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV16ContagemResultado_DataCnt_To1) )
               {
                  AV47FilterContagemResultado_DataCntDescription = "Data Contagem (at�)";
                  AV48ContagemResultado_DataCnt = AV16ContagemResultado_DataCnt_To1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV17ContagemResultado_DataDmn1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
               AV18ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV17ContagemResultado_DataDmn1) )
               {
                  AV49FilterContagemResultado_DataDmnDescription = "Data Demanda";
                  AV50ContagemResultado_DataDmn = AV17ContagemResultado_DataDmn1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV18ContagemResultado_DataDmn_To1) )
               {
                  AV49FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                  AV50ContagemResultado_DataDmn = AV18ContagemResultado_DataDmn_To1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV84ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV84ContagemResultado_ContadorFM1) )
               {
                  AV85ContagemResultado_ContadorFM = AV84ContagemResultado_ContadorFM1;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV20ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV20ContagemResultado_StatusCnt1) )
               {
                  AV22FilterContagemResultado_StatusCnt1ValueDescription = gxdomainstatuscontagem.getDescription(context,AV20ContagemResultado_StatusCnt1);
                  AV21FilterContagemResultado_StatusCntValueDescription = AV22FilterContagemResultado_StatusCnt1ValueDescription;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV21FilterContagemResultado_StatusCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV61ContagemResultado_Baseline1 = AV45GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Baseline1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV61ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV66FilterContagemResultado_Baseline1ValueDescription = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV61ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV66FilterContagemResultado_Baseline1ValueDescription = "N�o";
                  }
                  AV65FilterContagemResultado_BaselineValueDescription = AV66FilterContagemResultado_Baseline1ValueDescription;
                  H340( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV65FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV44GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV23DynamicFiltersEnabled2 = true;
               AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV44GridState.gxTpr_Dynamicfilters.Item(2));
               AV24DynamicFiltersSelector2 = AV45GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV90DynamicFiltersOperator2 = AV45GridStateDynamicFilter.gxTpr_Operator;
                  AV25ContagemResultado_OsFsOsFm2 = AV45GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultado_OsFsOsFm2)) )
                  {
                     if ( AV90DynamicFiltersOperator2 == 0 )
                     {
                        AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV90DynamicFiltersOperator2 == 1 )
                     {
                        AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV90DynamicFiltersOperator2 == 2 )
                     {
                        AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV46ContagemResultado_OsFsOsFm = AV25ContagemResultado_OsFsOsFm2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV26ContagemResultado_DataCnt2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV27ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV26ContagemResultado_DataCnt2) )
                  {
                     AV47FilterContagemResultado_DataCntDescription = "Data Contagem";
                     AV48ContagemResultado_DataCnt = AV26ContagemResultado_DataCnt2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV27ContagemResultado_DataCnt_To2) )
                  {
                     AV47FilterContagemResultado_DataCntDescription = "Data Contagem (at�)";
                     AV48ContagemResultado_DataCnt = AV27ContagemResultado_DataCnt_To2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV28ContagemResultado_DataDmn2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                  AV29ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV28ContagemResultado_DataDmn2) )
                  {
                     AV49FilterContagemResultado_DataDmnDescription = "Data Demanda";
                     AV50ContagemResultado_DataDmn = AV28ContagemResultado_DataDmn2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV29ContagemResultado_DataDmn_To2) )
                  {
                     AV49FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                     AV50ContagemResultado_DataDmn = AV29ContagemResultado_DataDmn_To2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV86ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV86ContagemResultado_ContadorFM2) )
                  {
                     AV85ContagemResultado_ContadorFM = AV86ContagemResultado_ContadorFM2;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV31ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV31ContagemResultado_StatusCnt2) )
                  {
                     AV32FilterContagemResultado_StatusCnt2ValueDescription = gxdomainstatuscontagem.getDescription(context,AV31ContagemResultado_StatusCnt2);
                     AV21FilterContagemResultado_StatusCntValueDescription = AV32FilterContagemResultado_StatusCnt2ValueDescription;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV21FilterContagemResultado_StatusCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV62ContagemResultado_Baseline2 = AV45GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContagemResultado_Baseline2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV62ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV67FilterContagemResultado_Baseline2ValueDescription = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV62ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV67FilterContagemResultado_Baseline2ValueDescription = "N�o";
                     }
                     AV65FilterContagemResultado_BaselineValueDescription = AV67FilterContagemResultado_Baseline2ValueDescription;
                     H340( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV65FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV44GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV33DynamicFiltersEnabled3 = true;
                  AV45GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV44GridState.gxTpr_Dynamicfilters.Item(3));
                  AV34DynamicFiltersSelector3 = AV45GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV91DynamicFiltersOperator3 = AV45GridStateDynamicFilter.gxTpr_Operator;
                     AV35ContagemResultado_OsFsOsFm3 = AV45GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ContagemResultado_OsFsOsFm3)) )
                     {
                        if ( AV91DynamicFiltersOperator3 == 0 )
                        {
                           AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV91DynamicFiltersOperator3 == 1 )
                        {
                           AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV91DynamicFiltersOperator3 == 2 )
                        {
                           AV89FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV46ContagemResultado_OsFsOsFm = AV35ContagemResultado_OsFsOsFm3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV36ContagemResultado_DataCnt3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV37ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV36ContagemResultado_DataCnt3) )
                     {
                        AV47FilterContagemResultado_DataCntDescription = "Data Contagem";
                        AV48ContagemResultado_DataCnt = AV36ContagemResultado_DataCnt3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV37ContagemResultado_DataCnt_To3) )
                     {
                        AV47FilterContagemResultado_DataCntDescription = "Data Contagem (at�)";
                        AV48ContagemResultado_DataCnt = AV37ContagemResultado_DataCnt_To3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47FilterContagemResultado_DataCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV48ContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV38ContagemResultado_DataDmn3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Value, 2);
                     AV39ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV45GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV38ContagemResultado_DataDmn3) )
                     {
                        AV49FilterContagemResultado_DataDmnDescription = "Data Demanda";
                        AV50ContagemResultado_DataDmn = AV38ContagemResultado_DataDmn3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV39ContagemResultado_DataDmn_To3) )
                     {
                        AV49FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                        AV50ContagemResultado_DataDmn = AV39ContagemResultado_DataDmn_To3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV50ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV87ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV87ContagemResultado_ContadorFM3) )
                     {
                        AV85ContagemResultado_ContadorFM = AV87ContagemResultado_ContadorFM3;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV41ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV45GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV41ContagemResultado_StatusCnt3) )
                     {
                        AV42FilterContagemResultado_StatusCnt3ValueDescription = gxdomainstatuscontagem.getDescription(context,AV41ContagemResultado_StatusCnt3);
                        AV21FilterContagemResultado_StatusCntValueDescription = AV42FilterContagemResultado_StatusCnt3ValueDescription;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV21FilterContagemResultado_StatusCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV63ContagemResultado_Baseline3 = AV45GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Baseline3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV63ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV68FilterContagemResultado_Baseline3ValueDescription = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV63ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV68FilterContagemResultado_Baseline3ValueDescription = "N�o";
                        }
                        AV65FilterContagemResultado_BaselineValueDescription = AV68FilterContagemResultado_Baseline3ValueDescription;
                        H340( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV65FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95TFContratada_AreaTrabalhoDes_Sel)) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95TFContratada_AreaTrabalhoDes_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94TFContratada_AreaTrabalhoDes)) )
            {
               H340( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("�rea", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94TFContratada_AreaTrabalhoDes, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (DateTime.MinValue==AV96TFContagemResultado_DataDmn) && (DateTime.MinValue==AV97TFContagemResultado_DataDmn_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV96TFContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV97TFContagemResultado_DataDmn_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV98TFContagemResultado_DataCnt) && (DateTime.MinValue==AV99TFContagemResultado_DataCnt_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV98TFContagemResultado_DataCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV99TFContagemResultado_DataCnt_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101TFContagemResultado_HoraCnt_Sel)) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("", 5, Gx_line+2, 186, Gx_line+19, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV101TFContagemResultado_HoraCnt_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100TFContagemResultado_HoraCnt)) )
            {
               H340( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("", 5, Gx_line+2, 186, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV100TFContagemResultado_HoraCnt, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103TFContagemResultado_OsFsOsFm_Sel)) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103TFContagemResultado_OsFsOsFm_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102TFContagemResultado_OsFsOsFm)) )
            {
               H340( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102TFContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105TFContagemrResultado_SistemaSigla_Sel)) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105TFContagemrResultado_SistemaSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFContagemrResultado_SistemaSigla)) )
            {
               H340( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV104TFContagemrResultado_SistemaSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107TFContagemResultado_ContadorFMNom_Sel)) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contador FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV107TFContagemResultado_ContadorFMNom_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106TFContagemResultado_ContadorFMNom)) )
            {
               H340( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contador FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106TFContagemResultado_ContadorFMNom, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         AV110TFContagemResultado_StatusCnt_Sels.FromJSonString(AV108TFContagemResultado_StatusCnt_SelsJson);
         if ( ! ( AV110TFContagemResultado_StatusCnt_Sels.Count == 0 ) )
         {
            AV123i = 1;
            AV126GXV1 = 1;
            while ( AV126GXV1 <= AV110TFContagemResultado_StatusCnt_Sels.Count )
            {
               AV111TFContagemResultado_StatusCnt_Sel = (short)(AV110TFContagemResultado_StatusCnt_Sels.GetNumeric(AV126GXV1));
               if ( AV123i == 1 )
               {
                  AV109TFContagemResultado_StatusCnt_SelDscs = "";
               }
               else
               {
                  AV109TFContagemResultado_StatusCnt_SelDscs = AV109TFContagemResultado_StatusCnt_SelDscs + ", ";
               }
               AV122FilterTFContagemResultado_StatusCnt_SelValueDescription = gxdomainstatuscontagem.getDescription(context,AV111TFContagemResultado_StatusCnt_Sel);
               AV109TFContagemResultado_StatusCnt_SelDscs = AV109TFContagemResultado_StatusCnt_SelDscs + AV122FilterTFContagemResultado_StatusCnt_SelValueDescription;
               AV123i = (long)(AV123i+1);
               AV126GXV1 = (int)(AV126GXV1+1);
            }
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV109TFContagemResultado_StatusCnt_SelDscs, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (0==AV112TFContagemResultadoContagens_Esforco) && (0==AV113TFContagemResultadoContagens_Esforco_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Esfor�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV112TFContagemResultadoContagens_Esforco), "ZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Esfor�o (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113TFContagemResultadoContagens_Esforco_To), "ZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV114TFContagemResultado_PFBFS) && (Convert.ToDecimal(0)==AV115TFContagemResultado_PFBFS_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV114TFContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV115TFContagemResultado_PFBFS_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV116TFContagemResultado_PFLFS) && (Convert.ToDecimal(0)==AV117TFContagemResultado_PFLFS_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV116TFContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV117TFContagemResultado_PFLFS_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV118TFContagemResultado_PFBFM) && (Convert.ToDecimal(0)==AV119TFContagemResultado_PFBFM_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV118TFContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV119TFContagemResultado_PFBFM_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV120TFContagemResultado_PFLFM) && (Convert.ToDecimal(0)==AV121TFContagemResultado_PFLFM_To) ) )
         {
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV120TFContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV121TFContagemResultado_PFLFM_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H340( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 111, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(116, Gx_line+30, 169, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(174, Gx_line+30, 227, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(232, Gx_line+30, 285, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(290, Gx_line+30, 396, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(401, Gx_line+30, 507, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(512, Gx_line+30, 618, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(623, Gx_line+30, 729, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(734, Gx_line+30, 787, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(792, Gx_line+30, 845, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(850, Gx_line+30, 903, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(908, Gx_line+30, 961, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(966, Gx_line+30, 1019, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("�rea", 5, Gx_line+15, 111, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Demanda", 116, Gx_line+15, 169, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contagem", 174, Gx_line+15, 227, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("", 232, Gx_line+15, 285, Gx_line+32, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("OS Ref|OS", 290, Gx_line+15, 396, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Sistema", 401, Gx_line+15, 507, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contador FM", 512, Gx_line+15, 618, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Status", 623, Gx_line+15, 729, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Esfor�o", 734, Gx_line+15, 787, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FS", 792, Gx_line+15, 845, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FS", 850, Gx_line+15, 903, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FM", 908, Gx_line+15, 961, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FM", 966, Gx_line+15, 1019, Gx_line+29, 2, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = AV13DynamicFiltersSelector1;
         AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 = AV88DynamicFiltersOperator1;
         AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = AV14ContagemResultado_OsFsOsFm1;
         AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = AV15ContagemResultado_DataCnt1;
         AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = AV16ContagemResultado_DataCnt_To1;
         AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = AV84ContagemResultado_ContadorFM1;
         AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 = AV20ContagemResultado_StatusCnt1;
         AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = AV61ContagemResultado_Baseline1;
         AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = AV23DynamicFiltersEnabled2;
         AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = AV24DynamicFiltersSelector2;
         AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = AV25ContagemResultado_OsFsOsFm2;
         AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = AV26ContagemResultado_DataCnt2;
         AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = AV27ContagemResultado_DataCnt_To2;
         AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = AV28ContagemResultado_DataDmn2;
         AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = AV29ContagemResultado_DataDmn_To2;
         AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = AV86ContagemResultado_ContadorFM2;
         AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 = AV31ContagemResultado_StatusCnt2;
         AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = AV62ContagemResultado_Baseline2;
         AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = AV33DynamicFiltersEnabled3;
         AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = AV34DynamicFiltersSelector3;
         AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = AV35ContagemResultado_OsFsOsFm3;
         AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = AV36ContagemResultado_DataCnt3;
         AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = AV37ContagemResultado_DataCnt_To3;
         AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = AV38ContagemResultado_DataDmn3;
         AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = AV39ContagemResultado_DataDmn_To3;
         AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = AV87ContagemResultado_ContadorFM3;
         AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 = AV41ContagemResultado_StatusCnt3;
         AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = AV63ContagemResultado_Baseline3;
         AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = AV94TFContratada_AreaTrabalhoDes;
         AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = AV95TFContratada_AreaTrabalhoDes_Sel;
         AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = AV96TFContagemResultado_DataDmn;
         AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = AV97TFContagemResultado_DataDmn_To;
         AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = AV98TFContagemResultado_DataCnt;
         AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = AV99TFContagemResultado_DataCnt_To;
         AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = AV100TFContagemResultado_HoraCnt;
         AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = AV101TFContagemResultado_HoraCnt_Sel;
         AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = AV102TFContagemResultado_OsFsOsFm;
         AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = AV103TFContagemResultado_OsFsOsFm_Sel;
         AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = AV104TFContagemrResultado_SistemaSigla;
         AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = AV105TFContagemrResultado_SistemaSigla_Sel;
         AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = AV106TFContagemResultado_ContadorFMNom;
         AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = AV107TFContagemResultado_ContadorFMNom_Sel;
         AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = AV110TFContagemResultado_StatusCnt_Sels;
         AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco = AV112TFContagemResultadoContagens_Esforco;
         AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to = AV113TFContagemResultadoContagens_Esforco_To;
         AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs = AV114TFContagemResultado_PFBFS;
         AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to = AV115TFContagemResultado_PFBFS_To;
         AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs = AV116TFContagemResultado_PFLFS;
         AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to = AV117TFContagemResultado_PFLFS_To;
         AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm = AV118TFContagemResultado_PFBFM;
         AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to = AV119TFContagemResultado_PFBFM_To;
         AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm = AV120TFContagemResultado_PFLFM;
         AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to = AV121TFContagemResultado_PFLFM_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV82Contratadas ,
                                              AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                              AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                              AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                              AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                              AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                              AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                              AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                              AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                              AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                              AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                              AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                              AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                              AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                              AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                              AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                              AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                              AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                              AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                              AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                              AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                              AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                              AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                              AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                              AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                              AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                              AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                              AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                              AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                              AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                              AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                              AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                              AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                              AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                              AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                              AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                              AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                              AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                              AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                              AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                              AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                              AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                              AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                              AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels.Count ,
                                              AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                              AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                              AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                              AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                              AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                              AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                              AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                              AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                              AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                              AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                              AV83Usuario_EhGestor ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              AV59Flag ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV44GridState.gxTpr_Dynamicfilters.Count ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A473ContagemResultado_DataCnt ,
                                              A471ContagemResultado_DataDmn ,
                                              A598ContagemResultado_Baseline ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A511ContagemResultado_HoraCnt ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A474ContagemResultado_ContadorFMNom ,
                                              A482ContagemResultadoContagens_Esforco ,
                                              A458ContagemResultado_PFBFS ,
                                              A459ContagemResultado_PFLFS ,
                                              A460ContagemResultado_PFBFM ,
                                              A461ContagemResultado_PFLFM ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              AV9WWPContext.gxTpr_Userid ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              Gx_date ,
                                              AV10OrderedBy ,
                                              AV11OrderedDsc ,
                                              AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ,
                                              AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes), "%", "");
         lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = StringUtil.PadR( StringUtil.RTrim( AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt), 5, "%");
         lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm), "%", "");
         lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = StringUtil.PadR( StringUtil.RTrim( AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom), 100, "%");
         /* Using cursor P00343 */
         pr_default.execute(0, new Object[] {AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2, AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3, AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1, AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1, AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1, AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1, AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1, AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2, AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2, AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2, AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2, AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2, AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3, AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3, AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3, AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3, AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3, lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes, AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel, AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn, AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to, AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt, AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to, lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt, AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel, lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm, AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel, lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla, AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel, lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom, AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel, AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco, AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to, AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs, AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to, AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs, AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to, AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm, AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to, AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm, AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to, AV9WWPContext.gxTpr_Userid, AV9WWPContext.gxTpr_Areatrabalho_codigo, Gx_date});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00343_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00343_n489ContagemResultado_SistemaCod[0];
            A479ContagemResultado_CrFMPessoaCod = P00343_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = P00343_n479ContagemResultado_CrFMPessoaCod[0];
            A490ContagemResultado_ContratadaCod = P00343_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00343_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00343_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00343_n52Contratada_AreaTrabalhoCod[0];
            A470ContagemResultado_ContadorFMCod = P00343_A470ContagemResultado_ContadorFMCod[0];
            A461ContagemResultado_PFLFM = P00343_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P00343_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P00343_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P00343_n460ContagemResultado_PFBFM[0];
            A459ContagemResultado_PFLFS = P00343_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P00343_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P00343_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P00343_n458ContagemResultado_PFBFS[0];
            A482ContagemResultadoContagens_Esforco = P00343_A482ContagemResultadoContagens_Esforco[0];
            A474ContagemResultado_ContadorFMNom = P00343_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = P00343_n474ContagemResultado_ContadorFMNom[0];
            A509ContagemrResultado_SistemaSigla = P00343_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00343_n509ContagemrResultado_SistemaSigla[0];
            A511ContagemResultado_HoraCnt = P00343_A511ContagemResultado_HoraCnt[0];
            A53Contratada_AreaTrabalhoDes = P00343_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00343_n53Contratada_AreaTrabalhoDes[0];
            A598ContagemResultado_Baseline = P00343_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00343_n598ContagemResultado_Baseline[0];
            A483ContagemResultado_StatusCnt = P00343_A483ContagemResultado_StatusCnt[0];
            A508ContagemResultado_Owner = P00343_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00343_A471ContagemResultado_DataDmn[0];
            A473ContagemResultado_DataCnt = P00343_A473ContagemResultado_DataCnt[0];
            A456ContagemResultado_Codigo = P00343_A456ContagemResultado_Codigo[0];
            A584ContagemResultado_ContadorFM = P00343_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00343_n584ContagemResultado_ContadorFM[0];
            A493ContagemResultado_DemandaFM = P00343_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00343_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00343_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00343_n457ContagemResultado_Demanda[0];
            A479ContagemResultado_CrFMPessoaCod = P00343_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = P00343_n479ContagemResultado_CrFMPessoaCod[0];
            A474ContagemResultado_ContadorFMNom = P00343_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = P00343_n474ContagemResultado_ContadorFMNom[0];
            A489ContagemResultado_SistemaCod = P00343_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00343_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = P00343_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00343_n490ContagemResultado_ContratadaCod[0];
            A598ContagemResultado_Baseline = P00343_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00343_n598ContagemResultado_Baseline[0];
            A508ContagemResultado_Owner = P00343_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00343_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00343_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00343_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00343_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00343_n457ContagemResultado_Demanda[0];
            A509ContagemrResultado_SistemaSigla = P00343_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00343_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00343_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00343_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00343_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00343_n53Contratada_AreaTrabalhoDes[0];
            A584ContagemResultado_ContadorFM = P00343_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P00343_n584ContagemResultado_ContadorFM[0];
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            AV12ContagemResultado_StatusCntDescription = gxdomainstatuscontagem.getDescription(context,A483ContagemResultado_StatusCnt);
            /* Execute user subroutine: 'BEFOREPRINTLINE' */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            H340( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A53Contratada_AreaTrabalhoDes, "@!")), 5, Gx_line+2, 111, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 116, Gx_line+2, 169, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"), 174, Gx_line+2, 227, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")), 232, Gx_line+2, 285, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 290, Gx_line+2, 396, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 401, Gx_line+2, 507, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A474ContagemResultado_ContadorFMNom, "@!")), 512, Gx_line+2, 618, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_StatusCntDescription, "")), 623, Gx_line+2, 729, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")), 734, Gx_line+2, 787, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), 792, Gx_line+2, 845, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), 850, Gx_line+2, 903, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), 908, Gx_line+2, 961, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), 966, Gx_line+2, 1019, Gx_line+17, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            /* Execute user subroutine: 'AFTERPRINTLINE' */
            S162 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = AV13DynamicFiltersSelector1;
         AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 = AV88DynamicFiltersOperator1;
         AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = AV14ContagemResultado_OsFsOsFm1;
         AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = AV15ContagemResultado_DataCnt1;
         AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = AV16ContagemResultado_DataCnt_To1;
         AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = AV84ContagemResultado_ContadorFM1;
         AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 = AV20ContagemResultado_StatusCnt1;
         AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = AV61ContagemResultado_Baseline1;
         AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = AV23DynamicFiltersEnabled2;
         AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = AV24DynamicFiltersSelector2;
         AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = AV25ContagemResultado_OsFsOsFm2;
         AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = AV26ContagemResultado_DataCnt2;
         AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = AV27ContagemResultado_DataCnt_To2;
         AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = AV28ContagemResultado_DataDmn2;
         AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = AV29ContagemResultado_DataDmn_To2;
         AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = AV86ContagemResultado_ContadorFM2;
         AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 = AV31ContagemResultado_StatusCnt2;
         AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = AV62ContagemResultado_Baseline2;
         AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = AV33DynamicFiltersEnabled3;
         AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = AV34DynamicFiltersSelector3;
         AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = AV35ContagemResultado_OsFsOsFm3;
         AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = AV36ContagemResultado_DataCnt3;
         AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = AV37ContagemResultado_DataCnt_To3;
         AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = AV38ContagemResultado_DataDmn3;
         AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = AV39ContagemResultado_DataDmn_To3;
         AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = AV87ContagemResultado_ContadorFM3;
         AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 = AV41ContagemResultado_StatusCnt3;
         AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = AV63ContagemResultado_Baseline3;
         AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = AV94TFContratada_AreaTrabalhoDes;
         AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = AV95TFContratada_AreaTrabalhoDes_Sel;
         AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = AV96TFContagemResultado_DataDmn;
         AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = AV97TFContagemResultado_DataDmn_To;
         AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = AV98TFContagemResultado_DataCnt;
         AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = AV99TFContagemResultado_DataCnt_To;
         AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = AV100TFContagemResultado_HoraCnt;
         AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = AV101TFContagemResultado_HoraCnt_Sel;
         AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = AV102TFContagemResultado_OsFsOsFm;
         AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = AV103TFContagemResultado_OsFsOsFm_Sel;
         AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = AV104TFContagemrResultado_SistemaSigla;
         AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = AV105TFContagemrResultado_SistemaSigla_Sel;
         AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = AV106TFContagemResultado_ContadorFMNom;
         AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = AV107TFContagemResultado_ContadorFMNom_Sel;
         AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = AV110TFContagemResultado_StatusCnt_Sels;
         AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco = AV112TFContagemResultadoContagens_Esforco;
         AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to = AV113TFContagemResultadoContagens_Esforco_To;
         AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs = AV114TFContagemResultado_PFBFS;
         AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to = AV115TFContagemResultado_PFBFS_To;
         AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs = AV116TFContagemResultado_PFLFS;
         AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to = AV117TFContagemResultado_PFLFS_To;
         AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm = AV118TFContagemResultado_PFBFM;
         AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to = AV119TFContagemResultado_PFBFM_To;
         AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm = AV120TFContagemResultado_PFLFM;
         AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to = AV121TFContagemResultado_PFLFM_To;
         /* Optimized group. */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                              AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                              AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                              AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                              AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                              AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                              AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                              AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                              AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                              AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                              AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                              AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                              AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                              AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                              AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                              AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                              AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                              AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                              AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                              AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                              AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                              AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                              AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                              AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                              AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                              AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                              AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                              AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                              AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                              AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                              AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                              AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                              AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                              AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                              AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                              AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                              AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                              AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                              AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                              AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                              AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                              AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                              AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                              AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels.Count ,
                                              AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                              AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                              AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                              AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                              AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                              AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                              AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                              AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                              AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                              AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                              AV9WWPContext.gxTpr_Userehadministradorgam ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV59Flag ,
                                              AV44GridState.gxTpr_Dynamicfilters.Count ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A473ContagemResultado_DataCnt ,
                                              A471ContagemResultado_DataDmn ,
                                              A598ContagemResultado_Baseline ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A511ContagemResultado_HoraCnt ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A474ContagemResultado_ContadorFMNom ,
                                              c482ContagemResultadoContagens_Esforco ,
                                              c458ContagemResultado_PFBFS ,
                                              c459ContagemResultado_PFLFS ,
                                              c460ContagemResultado_PFBFM ,
                                              c461ContagemResultado_PFLFM ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              AV9WWPContext.gxTpr_Userid ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              Gx_date },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes), "%", "");
         lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = StringUtil.PadR( StringUtil.RTrim( AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt), 5, "%");
         lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm), "%", "");
         lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = StringUtil.PadR( StringUtil.RTrim( AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom), 100, "%");
         /* Using cursor P00345 */
         pr_default.execute(1, new Object[] {AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2, AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3, AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1, AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1, AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1, AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1, AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1, AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2, AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2, AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2, AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2, AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2, AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3, AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3, AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3, AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3, AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3, lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes, AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel, AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn, AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to, AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt, AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to, lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt, AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel, lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm, AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel, lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla, AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel, lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom, AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel, AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco, AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to, AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs, AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to, AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs, AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to, AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm, AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to, AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm, AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to, AV9WWPContext.gxTpr_Userid, AV9WWPContext.gxTpr_Areatrabalho_codigo, Gx_date});
         cV58Qtde = P00345_AV58Qtde[0];
         c482ContagemResultadoContagens_Esforco = P00345_A482ContagemResultadoContagens_Esforco[0];
         c458ContagemResultado_PFBFS = P00345_A458ContagemResultado_PFBFS[0];
         n458ContagemResultado_PFBFS = P00345_n458ContagemResultado_PFBFS[0];
         c459ContagemResultado_PFLFS = P00345_A459ContagemResultado_PFLFS[0];
         n459ContagemResultado_PFLFS = P00345_n459ContagemResultado_PFLFS[0];
         c460ContagemResultado_PFBFM = P00345_A460ContagemResultado_PFBFM[0];
         n460ContagemResultado_PFBFM = P00345_n460ContagemResultado_PFBFM[0];
         c461ContagemResultado_PFLFM = P00345_A461ContagemResultado_PFLFM[0];
         n461ContagemResultado_PFLFM = P00345_n461ContagemResultado_PFLFM[0];
         pr_default.close(1);
         AV58Qtde = (short)(AV58Qtde+cV58Qtde*1);
         AV53ContagemResultadoContagens_Esforco = (int)(AV53ContagemResultadoContagens_Esforco+c482ContagemResultadoContagens_Esforco);
         AV54ContagemResultado_PFBFS = (decimal)(AV54ContagemResultado_PFBFS+c458ContagemResultado_PFBFS);
         AV55ContagemResultado_PFLFS = (decimal)(AV55ContagemResultado_PFLFS+c459ContagemResultado_PFLFS);
         AV56ContagemResultado_PFBFM = (decimal)(AV56ContagemResultado_PFBFM+c460ContagemResultado_PFBFM);
         AV57ContagemResultado_PFLFM = (decimal)(AV57ContagemResultado_PFLFM+c461ContagemResultado_PFLFM);
         /* End optimized group. */
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H340( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV60Websession = context.GetSession();
         AV82Contratadas = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV83Usuario_EhGestor = false;
         AV44GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV45GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13DynamicFiltersSelector1 = "";
         AV14ContagemResultado_OsFsOsFm1 = "";
         AV89FilterContagemResultado_OsFsOsFmDescription = "";
         AV46ContagemResultado_OsFsOsFm = "";
         AV15ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV16ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV47FilterContagemResultado_DataCntDescription = "";
         AV48ContagemResultado_DataCnt = DateTime.MinValue;
         AV17ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV18ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV49FilterContagemResultado_DataDmnDescription = "";
         AV50ContagemResultado_DataDmn = DateTime.MinValue;
         AV22FilterContagemResultado_StatusCnt1ValueDescription = "";
         AV21FilterContagemResultado_StatusCntValueDescription = "";
         AV61ContagemResultado_Baseline1 = "";
         AV66FilterContagemResultado_Baseline1ValueDescription = "";
         AV65FilterContagemResultado_BaselineValueDescription = "";
         AV24DynamicFiltersSelector2 = "";
         AV25ContagemResultado_OsFsOsFm2 = "";
         AV26ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV27ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV28ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV29ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV32FilterContagemResultado_StatusCnt2ValueDescription = "";
         AV62ContagemResultado_Baseline2 = "";
         AV67FilterContagemResultado_Baseline2ValueDescription = "";
         AV34DynamicFiltersSelector3 = "";
         AV35ContagemResultado_OsFsOsFm3 = "";
         AV36ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV37ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV38ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV39ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV42FilterContagemResultado_StatusCnt3ValueDescription = "";
         AV63ContagemResultado_Baseline3 = "";
         AV68FilterContagemResultado_Baseline3ValueDescription = "";
         AV110TFContagemResultado_StatusCnt_Sels = new GxSimpleCollection();
         AV109TFContagemResultado_StatusCnt_SelDscs = "";
         AV122FilterTFContagemResultado_StatusCnt_SelValueDescription = "";
         AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = "";
         AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = "";
         AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = "";
         AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = "";
         AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = "";
         AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = "";
         AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = "";
         AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = "";
         AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = "";
         AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = "";
         AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = "";
         AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = DateTime.MinValue;
         AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = DateTime.MinValue;
         AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = "";
         AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = "";
         AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = "";
         AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = "";
         AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = "";
         AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = "";
         AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = "";
         AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = "";
         AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = "";
         lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = "";
         lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = "";
         lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = "";
         lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = "";
         lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = "";
         lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = "";
         lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A53Contratada_AreaTrabalhoDes = "";
         A511ContagemResultado_HoraCnt = "";
         A509ContagemrResultado_SistemaSigla = "";
         A474ContagemResultado_ContadorFMNom = "";
         Gx_date = DateTime.MinValue;
         P00343_A489ContagemResultado_SistemaCod = new int[1] ;
         P00343_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00343_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         P00343_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         P00343_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00343_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00343_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00343_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00343_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P00343_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00343_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00343_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00343_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00343_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00343_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00343_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00343_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00343_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P00343_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         P00343_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         P00343_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00343_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00343_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00343_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00343_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00343_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00343_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00343_A483ContagemResultado_StatusCnt = new short[1] ;
         P00343_A508ContagemResultado_Owner = new int[1] ;
         P00343_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00343_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00343_A456ContagemResultado_Codigo = new int[1] ;
         P00343_A584ContagemResultado_ContadorFM = new int[1] ;
         P00343_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         P00343_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00343_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00343_A457ContagemResultado_Demanda = new String[] {""} ;
         P00343_n457ContagemResultado_Demanda = new bool[] {false} ;
         A501ContagemResultado_OsFsOsFm = "";
         AV12ContagemResultado_StatusCntDescription = "";
         P00345_AV58Qtde = new short[1] ;
         P00345_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P00345_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00345_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00345_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00345_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00345_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00345_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00345_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00345_n461ContagemResultado_PFLFM = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportreportwwcontagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               P00343_A489ContagemResultado_SistemaCod, P00343_n489ContagemResultado_SistemaCod, P00343_A479ContagemResultado_CrFMPessoaCod, P00343_n479ContagemResultado_CrFMPessoaCod, P00343_A490ContagemResultado_ContratadaCod, P00343_n490ContagemResultado_ContratadaCod, P00343_A52Contratada_AreaTrabalhoCod, P00343_n52Contratada_AreaTrabalhoCod, P00343_A470ContagemResultado_ContadorFMCod, P00343_A461ContagemResultado_PFLFM,
               P00343_n461ContagemResultado_PFLFM, P00343_A460ContagemResultado_PFBFM, P00343_n460ContagemResultado_PFBFM, P00343_A459ContagemResultado_PFLFS, P00343_n459ContagemResultado_PFLFS, P00343_A458ContagemResultado_PFBFS, P00343_n458ContagemResultado_PFBFS, P00343_A482ContagemResultadoContagens_Esforco, P00343_A474ContagemResultado_ContadorFMNom, P00343_n474ContagemResultado_ContadorFMNom,
               P00343_A509ContagemrResultado_SistemaSigla, P00343_n509ContagemrResultado_SistemaSigla, P00343_A511ContagemResultado_HoraCnt, P00343_A53Contratada_AreaTrabalhoDes, P00343_n53Contratada_AreaTrabalhoDes, P00343_A598ContagemResultado_Baseline, P00343_n598ContagemResultado_Baseline, P00343_A483ContagemResultado_StatusCnt, P00343_A508ContagemResultado_Owner, P00343_A471ContagemResultado_DataDmn,
               P00343_A473ContagemResultado_DataCnt, P00343_A456ContagemResultado_Codigo, P00343_A584ContagemResultado_ContadorFM, P00343_n584ContagemResultado_ContadorFM, P00343_A493ContagemResultado_DemandaFM, P00343_n493ContagemResultado_DemandaFM, P00343_A457ContagemResultado_Demanda, P00343_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P00345_AV58Qtde, P00345_A482ContagemResultadoContagens_Esforco, P00345_A458ContagemResultado_PFBFS, P00345_n458ContagemResultado_PFBFS, P00345_A459ContagemResultado_PFLFS, P00345_n459ContagemResultado_PFLFS, P00345_A460ContagemResultado_PFBFM, P00345_n460ContagemResultado_PFBFM, P00345_A461ContagemResultado_PFLFM, P00345_n461ContagemResultado_PFLFM
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV112TFContagemResultadoContagens_Esforco ;
      private short AV113TFContagemResultadoContagens_Esforco_To ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV59Flag ;
      private short AV88DynamicFiltersOperator1 ;
      private short AV20ContagemResultado_StatusCnt1 ;
      private short AV90DynamicFiltersOperator2 ;
      private short AV31ContagemResultado_StatusCnt2 ;
      private short AV91DynamicFiltersOperator3 ;
      private short AV41ContagemResultado_StatusCnt3 ;
      private short AV111TFContagemResultado_StatusCnt_Sel ;
      private short AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ;
      private short AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ;
      private short AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ;
      private short AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ;
      private short AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ;
      private short AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ;
      private short AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ;
      private short AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ;
      private short AV9WWPContext_gxTpr_Userid ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short cV58Qtde ;
      private short AV58Qtde ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int AV84ContagemResultado_ContadorFM1 ;
      private int AV85ContagemResultado_ContadorFM ;
      private int AV86ContagemResultado_ContadorFM2 ;
      private int AV87ContagemResultado_ContadorFM3 ;
      private int AV126GXV1 ;
      private int AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ;
      private int AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ;
      private int AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 ;
      private int AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV44GridState_gxTpr_Dynamicfilters_Count ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A489ContagemResultado_SistemaCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A456ContagemResultado_Codigo ;
      private int c482ContagemResultadoContagens_Esforco ;
      private int AV53ContagemResultadoContagens_Esforco ;
      private long AV123i ;
      private decimal AV114TFContagemResultado_PFBFS ;
      private decimal AV115TFContagemResultado_PFBFS_To ;
      private decimal AV116TFContagemResultado_PFLFS ;
      private decimal AV117TFContagemResultado_PFLFS_To ;
      private decimal AV118TFContagemResultado_PFBFM ;
      private decimal AV119TFContagemResultado_PFBFM_To ;
      private decimal AV120TFContagemResultado_PFLFM ;
      private decimal AV121TFContagemResultado_PFLFM_To ;
      private decimal AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ;
      private decimal AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ;
      private decimal AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ;
      private decimal AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ;
      private decimal AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ;
      private decimal AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ;
      private decimal AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ;
      private decimal AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal c458ContagemResultado_PFBFS ;
      private decimal c459ContagemResultado_PFLFS ;
      private decimal c460ContagemResultado_PFBFM ;
      private decimal c461ContagemResultado_PFLFM ;
      private decimal AV54ContagemResultado_PFBFS ;
      private decimal AV55ContagemResultado_PFLFS ;
      private decimal AV56ContagemResultado_PFBFM ;
      private decimal AV57ContagemResultado_PFLFM ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV100TFContagemResultado_HoraCnt ;
      private String AV101TFContagemResultado_HoraCnt_Sel ;
      private String AV104TFContagemrResultado_SistemaSigla ;
      private String AV105TFContagemrResultado_SistemaSigla_Sel ;
      private String AV106TFContagemResultado_ContadorFMNom ;
      private String AV107TFContagemResultado_ContadorFMNom_Sel ;
      private String AV61ContagemResultado_Baseline1 ;
      private String AV62ContagemResultado_Baseline2 ;
      private String AV63ContagemResultado_Baseline3 ;
      private String AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ;
      private String AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ;
      private String AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ;
      private String AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ;
      private String AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ;
      private String AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ;
      private String AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ;
      private String AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ;
      private String scmdbuf ;
      private String lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ;
      private String lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ;
      private String lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ;
      private String A511ContagemResultado_HoraCnt ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A474ContagemResultado_ContadorFMNom ;
      private DateTime AV96TFContagemResultado_DataDmn ;
      private DateTime AV97TFContagemResultado_DataDmn_To ;
      private DateTime AV98TFContagemResultado_DataCnt ;
      private DateTime AV99TFContagemResultado_DataCnt_To ;
      private DateTime AV15ContagemResultado_DataCnt1 ;
      private DateTime AV16ContagemResultado_DataCnt_To1 ;
      private DateTime AV48ContagemResultado_DataCnt ;
      private DateTime AV17ContagemResultado_DataDmn1 ;
      private DateTime AV18ContagemResultado_DataDmn_To1 ;
      private DateTime AV50ContagemResultado_DataDmn ;
      private DateTime AV26ContagemResultado_DataCnt2 ;
      private DateTime AV27ContagemResultado_DataCnt_To2 ;
      private DateTime AV28ContagemResultado_DataDmn2 ;
      private DateTime AV29ContagemResultado_DataDmn_To2 ;
      private DateTime AV36ContagemResultado_DataCnt3 ;
      private DateTime AV37ContagemResultado_DataCnt_To3 ;
      private DateTime AV38ContagemResultado_DataDmn3 ;
      private DateTime AV39ContagemResultado_DataDmn_To3 ;
      private DateTime AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ;
      private DateTime AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ;
      private DateTime AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ;
      private DateTime AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ;
      private DateTime AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ;
      private DateTime AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ;
      private DateTime AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ;
      private DateTime AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ;
      private DateTime AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ;
      private DateTime AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ;
      private DateTime AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ;
      private DateTime AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ;
      private DateTime AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ;
      private DateTime AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV11OrderedDsc ;
      private bool returnInSub ;
      private bool AV83Usuario_EhGestor ;
      private bool AV23DynamicFiltersEnabled2 ;
      private bool AV33DynamicFiltersEnabled3 ;
      private bool AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ;
      private bool AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ;
      private bool AV9WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool A598ContagemResultado_Baseline ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n598ContagemResultado_Baseline ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool AV9WWPContext_gxTpr_Userehadministradorgam ;
      private String AV108TFContagemResultado_StatusCnt_SelsJson ;
      private String AV43GridStateXML ;
      private String AV94TFContratada_AreaTrabalhoDes ;
      private String AV95TFContratada_AreaTrabalhoDes_Sel ;
      private String AV102TFContagemResultado_OsFsOsFm ;
      private String AV103TFContagemResultado_OsFsOsFm_Sel ;
      private String AV13DynamicFiltersSelector1 ;
      private String AV14ContagemResultado_OsFsOsFm1 ;
      private String AV89FilterContagemResultado_OsFsOsFmDescription ;
      private String AV46ContagemResultado_OsFsOsFm ;
      private String AV47FilterContagemResultado_DataCntDescription ;
      private String AV49FilterContagemResultado_DataDmnDescription ;
      private String AV22FilterContagemResultado_StatusCnt1ValueDescription ;
      private String AV21FilterContagemResultado_StatusCntValueDescription ;
      private String AV66FilterContagemResultado_Baseline1ValueDescription ;
      private String AV65FilterContagemResultado_BaselineValueDescription ;
      private String AV24DynamicFiltersSelector2 ;
      private String AV25ContagemResultado_OsFsOsFm2 ;
      private String AV32FilterContagemResultado_StatusCnt2ValueDescription ;
      private String AV67FilterContagemResultado_Baseline2ValueDescription ;
      private String AV34DynamicFiltersSelector3 ;
      private String AV35ContagemResultado_OsFsOsFm3 ;
      private String AV42FilterContagemResultado_StatusCnt3ValueDescription ;
      private String AV68FilterContagemResultado_Baseline3ValueDescription ;
      private String AV109TFContagemResultado_StatusCnt_SelDscs ;
      private String AV122FilterTFContagemResultado_StatusCnt_SelValueDescription ;
      private String AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ;
      private String AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ;
      private String AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ;
      private String AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ;
      private String AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ;
      private String AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ;
      private String AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ;
      private String AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ;
      private String AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ;
      private String AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ;
      private String lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ;
      private String lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ;
      private String lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ;
      private String lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ;
      private String lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV12ContagemResultado_StatusCntDescription ;
      private IGxSession AV60Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00343_A489ContagemResultado_SistemaCod ;
      private bool[] P00343_n489ContagemResultado_SistemaCod ;
      private int[] P00343_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] P00343_n479ContagemResultado_CrFMPessoaCod ;
      private int[] P00343_A490ContagemResultado_ContratadaCod ;
      private bool[] P00343_n490ContagemResultado_ContratadaCod ;
      private int[] P00343_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00343_n52Contratada_AreaTrabalhoCod ;
      private int[] P00343_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P00343_A461ContagemResultado_PFLFM ;
      private bool[] P00343_n461ContagemResultado_PFLFM ;
      private decimal[] P00343_A460ContagemResultado_PFBFM ;
      private bool[] P00343_n460ContagemResultado_PFBFM ;
      private decimal[] P00343_A459ContagemResultado_PFLFS ;
      private bool[] P00343_n459ContagemResultado_PFLFS ;
      private decimal[] P00343_A458ContagemResultado_PFBFS ;
      private bool[] P00343_n458ContagemResultado_PFBFS ;
      private short[] P00343_A482ContagemResultadoContagens_Esforco ;
      private String[] P00343_A474ContagemResultado_ContadorFMNom ;
      private bool[] P00343_n474ContagemResultado_ContadorFMNom ;
      private String[] P00343_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00343_n509ContagemrResultado_SistemaSigla ;
      private String[] P00343_A511ContagemResultado_HoraCnt ;
      private String[] P00343_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00343_n53Contratada_AreaTrabalhoDes ;
      private bool[] P00343_A598ContagemResultado_Baseline ;
      private bool[] P00343_n598ContagemResultado_Baseline ;
      private short[] P00343_A483ContagemResultado_StatusCnt ;
      private int[] P00343_A508ContagemResultado_Owner ;
      private DateTime[] P00343_A471ContagemResultado_DataDmn ;
      private DateTime[] P00343_A473ContagemResultado_DataCnt ;
      private int[] P00343_A456ContagemResultado_Codigo ;
      private int[] P00343_A584ContagemResultado_ContadorFM ;
      private bool[] P00343_n584ContagemResultado_ContadorFM ;
      private String[] P00343_A493ContagemResultado_DemandaFM ;
      private bool[] P00343_n493ContagemResultado_DemandaFM ;
      private String[] P00343_A457ContagemResultado_Demanda ;
      private bool[] P00343_n457ContagemResultado_Demanda ;
      private short[] P00345_AV58Qtde ;
      private short[] P00345_A482ContagemResultadoContagens_Esforco ;
      private decimal[] P00345_A458ContagemResultado_PFBFS ;
      private bool[] P00345_n458ContagemResultado_PFBFS ;
      private decimal[] P00345_A459ContagemResultado_PFLFS ;
      private bool[] P00345_n459ContagemResultado_PFLFS ;
      private decimal[] P00345_A460ContagemResultado_PFBFM ;
      private bool[] P00345_n460ContagemResultado_PFBFM ;
      private decimal[] P00345_A461ContagemResultado_PFLFM ;
      private bool[] P00345_n461ContagemResultado_PFLFM ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV82Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV110TFContagemResultado_StatusCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV44GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV45GridStateDynamicFilter ;
   }

   public class aexportreportwwcontagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00343( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV82Contratadas ,
                                             String AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                             short AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                             String AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                             DateTime AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                             DateTime AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                             DateTime AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                             short AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                             String AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                             bool AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                             String AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                             short AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                             String AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                             DateTime AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                             DateTime AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                             DateTime AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                             DateTime AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                             short AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                             String AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                             bool AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                             String AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                             short AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                             String AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                             DateTime AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                             DateTime AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                             DateTime AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                             DateTime AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                             short AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                             String AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                             String AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                             String AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                             DateTime AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                             DateTime AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                             DateTime AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                             String AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                             String AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                             String AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                             String AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                             String AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                             String AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                             int AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ,
                                             short AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                             short AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                             decimal AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                             decimal AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                             decimal AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                             decimal AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                             decimal AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                             decimal AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                             decimal AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                             decimal AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                             bool AV83Usuario_EhGestor ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             short AV59Flag ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             int AV44GridState_gxTpr_Dynamicfilters_Count ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             bool A598ContagemResultado_Baseline ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A511ContagemResultado_HoraCnt ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A474ContagemResultado_ContadorFMNom ,
                                             short A482ContagemResultadoContagens_Esforco ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             short AV9WWPContext_gxTpr_Userid ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime Gx_date ,
                                             short AV10OrderedBy ,
                                             bool AV11OrderedDsc ,
                                             int AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             int AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ,
                                             int AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [74] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T4.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultadoContagens_Esforco], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_HoraCnt], T7.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T4.[ContagemResultado_Baseline], T1.[ContagemResultado_StatusCnt], T4.[ContagemResultado_Owner], T4.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo], COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, T4.[ContagemResultado_DemandaFM], T4.[ContagemResultado_Demanda] FROM ((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T4.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T7 WITH (NOLOCK) ON T7.[AreaTrabalho_Codigo] = T6.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo]";
         scmdbuf = scmdbuf + " FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = 1 and @AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = 1 and @AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3)))";
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] = @AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[14] = 1;
            GXv_int1[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] like @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[16] = 1;
            GXv_int1[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] like '%' + @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[18] = 1;
            GXv_int1[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] = @AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[25] = 1;
            GXv_int1[26] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] like @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[27] = 1;
            GXv_int1[28] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] like '%' + @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[29] = 1;
            GXv_int1[30] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] = @AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[36] = 1;
            GXv_int1[37] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] like @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[38] = 1;
            GXv_int1[39] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] like '%' + @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[40] = 1;
            GXv_int1[41] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3)";
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3)";
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T7.[AreaTrabalho_Descricao] like @lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[AreaTrabalho_Descricao] = @AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( ! (DateTime.MinValue==AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( ! (DateTime.MinValue==AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( ! (DateTime.MinValue==AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt)";
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( ! (DateTime.MinValue==AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to)";
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] like @lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)";
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] = @AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)";
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T4.[ContagemResultado_Demanda])) + CASE  WHEN (T4.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T4.[ContagemResultado_DemandaFM])) END) like @lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T4.[ContagemResultado_Demanda])) + CASE  WHEN (T4.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T4.[ContagemResultado_DemandaFM])) END) = @AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int1[57] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int1[58] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)";
         }
         else
         {
            GXv_int1[59] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)";
         }
         else
         {
            GXv_int1[60] = 1;
         }
         if ( AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels, "T1.[ContagemResultado_StatusCnt] IN (", ")") + ")";
         }
         if ( ! (0==AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] >= @AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco)";
         }
         else
         {
            GXv_int1[61] = 1;
         }
         if ( ! (0==AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] <= @AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to)";
         }
         else
         {
            GXv_int1[62] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] >= @AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs)";
         }
         else
         {
            GXv_int1[63] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] <= @AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to)";
         }
         else
         {
            GXv_int1[64] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] >= @AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs)";
         }
         else
         {
            GXv_int1[65] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] <= @AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to)";
         }
         else
         {
            GXv_int1[66] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] >= @AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm)";
         }
         else
         {
            GXv_int1[67] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] <= @AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to)";
         }
         else
         {
            GXv_int1[68] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] >= @AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm)";
         }
         else
         {
            GXv_int1[69] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] <= @AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to)";
         }
         else
         {
            GXv_int1[70] = 1;
         }
         if ( ! ( AV83Usuario_EhGestor || AV9WWPContext_gxTpr_Userehfinanceiro || AV9WWPContext_gxTpr_Userehcontratante ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV9WWPContext__Userid)";
         }
         else
         {
            GXv_int1[71] = 1;
         }
         if ( AV59Flag != 1 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         }
         else
         {
            GXv_int1[72] = 1;
         }
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) && ( AV59Flag == 1 ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV82Contratadas, "T4.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( AV44GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @Gx_date)";
         }
         else
         {
            GXv_int1[73] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV10OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_DataCnt] DESC, T1.[ContagemResultado_HoraCnt] DESC";
         }
         else if ( AV10OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataCnt] DESC";
         }
         else if ( ( AV10OrderedBy == 3 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV10OrderedBy == 3 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV10OrderedBy == 4 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultado_DataDmn]";
         }
         else if ( ( AV10OrderedBy == 4 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultado_DataDmn] DESC";
         }
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00345( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                             String AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                             short AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                             String AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                             DateTime AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                             DateTime AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                             DateTime AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                             short AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                             String AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                             bool AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                             String AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                             short AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                             String AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                             DateTime AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                             DateTime AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                             DateTime AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                             DateTime AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                             short AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                             String AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                             bool AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                             String AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                             short AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                             String AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                             DateTime AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                             DateTime AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                             DateTime AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                             DateTime AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                             short AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                             String AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                             String AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                             String AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                             DateTime AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                             DateTime AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                             DateTime AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                             String AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                             String AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                             String AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                             String AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                             String AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                             String AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                             int AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ,
                                             short AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                             short AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                             decimal AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                             decimal AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                             decimal AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                             decimal AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                             decimal AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                             decimal AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                             decimal AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                             decimal AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                             bool AV9WWPContext_gxTpr_Userehadministradorgam ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             short AV59Flag ,
                                             int AV44GridState_gxTpr_Dynamicfilters_Count ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             bool A598ContagemResultado_Baseline ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A511ContagemResultado_HoraCnt ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A474ContagemResultado_ContadorFMNom ,
                                             short A482ContagemResultadoContagens_Esforco ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             short AV9WWPContext_gxTpr_Userid ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime Gx_date )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [74] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*), SUM(T1.[ContagemResultadoContagens_Esforco]), SUM(T1.[ContagemResultado_PFBFS]), SUM(T1.[ContagemResultado_PFLFS]), SUM(T1.[ContagemResultado_PFBFM]), SUM(T1.[ContagemResultado_PFLFM]) FROM ((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) INNER JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = 1 and @AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = 1 and @AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3)))";
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[14] = 1;
            GXv_int3[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[16] = 1;
            GXv_int3[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV129WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[18] = 1;
            GXv_int3[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV137WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[25] = 1;
            GXv_int3[26] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[27] = 1;
            GXv_int3[28] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV140WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[29] = 1;
            GXv_int3[30] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV148WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[36] = 1;
            GXv_int3[37] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[38] = 1;
            GXv_int3[39] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV151WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[40] = 1;
            GXv_int3[41] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3)";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV159WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T4.[AreaTrabalho_Descricao] like @lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[AreaTrabalho_Descricao] = @AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( ! (DateTime.MinValue==AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( ! (DateTime.MinValue==AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( ! (DateTime.MinValue==AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt)";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( ! (DateTime.MinValue==AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] like @lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] = @AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)) ) )
         {
            sWhereString = sWhereString + " and (T8.[Pessoa_Nome] like @lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) )
         {
            sWhereString = sWhereString + " and (T8.[Pessoa_Nome] = @AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV174WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels, "T1.[ContagemResultado_StatusCnt] IN (", ")") + ")";
         }
         if ( ! (0==AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] >= @AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco)";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         if ( ! (0==AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] <= @AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to)";
         }
         else
         {
            GXv_int3[62] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] >= @AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs)";
         }
         else
         {
            GXv_int3[63] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] <= @AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to)";
         }
         else
         {
            GXv_int3[64] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] >= @AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs)";
         }
         else
         {
            GXv_int3[65] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] <= @AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to)";
         }
         else
         {
            GXv_int3[66] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] >= @AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm)";
         }
         else
         {
            GXv_int3[67] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] <= @AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to)";
         }
         else
         {
            GXv_int3[68] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] >= @AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm)";
         }
         else
         {
            GXv_int3[69] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] <= @AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to)";
         }
         else
         {
            GXv_int3[70] = 1;
         }
         if ( ! AV9WWPContext_gxTpr_Userehadministradorgam && ! AV9WWPContext_gxTpr_Userehfinanceiro )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV9WWPContext__Userid)";
         }
         else
         {
            GXv_int3[71] = 1;
         }
         if ( AV59Flag == 0 )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         }
         else
         {
            GXv_int3[72] = 1;
         }
         if ( AV44GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @Gx_date)";
         }
         else
         {
            GXv_int3[73] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00343(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (short)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (int)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (decimal)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (bool)dynConstraints[58] , (bool)dynConstraints[59] , (bool)dynConstraints[60] , (short)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (DateTime)dynConstraints[66] , (DateTime)dynConstraints[67] , (bool)dynConstraints[68] , (String)dynConstraints[69] , (String)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (short)dynConstraints[73] , (decimal)dynConstraints[74] , (decimal)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (short)dynConstraints[79] , (int)dynConstraints[80] , (int)dynConstraints[81] , (DateTime)dynConstraints[82] , (short)dynConstraints[83] , (bool)dynConstraints[84] , (int)dynConstraints[85] , (int)dynConstraints[86] , (int)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] );
               case 1 :
                     return conditional_P00345(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] , (short)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (decimal)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (bool)dynConstraints[56] , (bool)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (DateTime)dynConstraints[62] , (DateTime)dynConstraints[63] , (bool)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (String)dynConstraints[67] , (String)dynConstraints[68] , (short)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (decimal)dynConstraints[72] , (decimal)dynConstraints[73] , (int)dynConstraints[74] , (short)dynConstraints[75] , (int)dynConstraints[76] , (int)dynConstraints[77] , (DateTime)dynConstraints[78] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00343 ;
          prmP00343 = new Object[] {
          new Object[] {"@AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt",SqlDbType.Char,5,0} ,
          new Object[] {"@AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel",SqlDbType.Char,5,0} ,
          new Object[] {"@lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV9WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00345 ;
          prmP00345 = new Object[] {
          new Object[] {"@AV128WWContagemResultadoContagensDS_1_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV138WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV139WWContagemResultadoContagensDS_12_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV146WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV149WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV150WWContagemResultadoContagensDS_23_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV130WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV131WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV141WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV142WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV153WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV158WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV160WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV161WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV162WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV163WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV164WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV165WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV166WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt",SqlDbType.Char,5,0} ,
          new Object[] {"@AV167WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel",SqlDbType.Char,5,0} ,
          new Object[] {"@lV168WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV169WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV170WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV171WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV172WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV173WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV175WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV176WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV177WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV178WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV179WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV180WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV181WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV9WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00343", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00343,100,0,true,false )
             ,new CursorDef("P00345", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00345,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 5) ;
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((short[]) buf[27])[0] = rslt.getShort(16) ;
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((String[]) buf[36])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[78]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[83]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[96]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[98]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[109]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[116]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[120]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[128]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[135]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[136]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[137]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[138]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[139]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[140]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[141]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[142]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[143]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[144]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[145]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[78]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[83]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[96]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[98]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[109]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[116]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[120]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[128]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[135]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[136]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[137]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[138]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[139]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[140]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[141]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[142]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[143]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[144]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[145]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                return;
       }
    }

 }

}
