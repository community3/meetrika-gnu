/*
               File: PRC_SSStatusPlanejamento
        Description: SS Status em Planejamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:49.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ssstatusplanejamento : GXProcedure
   {
      public prc_ssstatusplanejamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ssstatusplanejamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           out String aP1_StatusDemanda )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8StatusDemanda = "" ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_StatusDemanda=this.AV8StatusDemanda;
      }

      public String executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8StatusDemanda = "" ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_StatusDemanda=this.AV8StatusDemanda;
         return AV8StatusDemanda ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 out String aP1_StatusDemanda )
      {
         prc_ssstatusplanejamento objprc_ssstatusplanejamento;
         objprc_ssstatusplanejamento = new prc_ssstatusplanejamento();
         objprc_ssstatusplanejamento.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_ssstatusplanejamento.AV8StatusDemanda = "" ;
         objprc_ssstatusplanejamento.context.SetSubmitInitialConfig(context);
         objprc_ssstatusplanejamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ssstatusplanejamento);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_StatusDemanda=this.AV8StatusDemanda;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ssstatusplanejamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CF2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1732Contratante_SSStatusPlanejamento = P00CF2_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = P00CF2_n1732Contratante_SSStatusPlanejamento[0];
            AV8StatusDemanda = A1732Contratante_SSStatusPlanejamento;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CF2_A29Contratante_Codigo = new int[1] ;
         P00CF2_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         P00CF2_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         A1732Contratante_SSStatusPlanejamento = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ssstatusplanejamento__default(),
            new Object[][] {
                new Object[] {
               P00CF2_A29Contratante_Codigo, P00CF2_A1732Contratante_SSStatusPlanejamento, P00CF2_n1732Contratante_SSStatusPlanejamento
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private String AV8StatusDemanda ;
      private String scmdbuf ;
      private String A1732Contratante_SSStatusPlanejamento ;
      private bool n1732Contratante_SSStatusPlanejamento ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CF2_A29Contratante_Codigo ;
      private String[] P00CF2_A1732Contratante_SSStatusPlanejamento ;
      private bool[] P00CF2_n1732Contratante_SSStatusPlanejamento ;
      private String aP1_StatusDemanda ;
   }

   public class prc_ssstatusplanejamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CF2 ;
          prmP00CF2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CF2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_SSStatusPlanejamento] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CF2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
