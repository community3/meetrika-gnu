/*
               File: GetPromptReferenciaINMFilterData
        Description: Get Prompt Referencia INMFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:1.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptreferenciainmfilterdata : GXProcedure
   {
      public getpromptreferenciainmfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptreferenciainmfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
         return AV30OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptreferenciainmfilterdata objgetpromptreferenciainmfilterdata;
         objgetpromptreferenciainmfilterdata = new getpromptreferenciainmfilterdata();
         objgetpromptreferenciainmfilterdata.AV21DDOName = aP0_DDOName;
         objgetpromptreferenciainmfilterdata.AV19SearchTxt = aP1_SearchTxt;
         objgetpromptreferenciainmfilterdata.AV20SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptreferenciainmfilterdata.AV25OptionsJson = "" ;
         objgetpromptreferenciainmfilterdata.AV28OptionsDescJson = "" ;
         objgetpromptreferenciainmfilterdata.AV30OptionIndexesJson = "" ;
         objgetpromptreferenciainmfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptreferenciainmfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptreferenciainmfilterdata);
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptreferenciainmfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV24Options = (IGxCollection)(new GxSimpleCollection());
         AV27OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV29OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_REFERENCIAINM_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIAINM_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_REFERENCIAINM_AREATRABALHODES") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIAINM_AREATRABALHODESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV25OptionsJson = AV24Options.ToJSonString(false);
         AV28OptionsDescJson = AV27OptionsDesc.ToJSonString(false);
         AV30OptionIndexesJson = AV29OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get("PromptReferenciaINMGridState"), "") == 0 )
         {
            AV34GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptReferenciaINMGridState"), "");
         }
         else
         {
            AV34GridState.FromXml(AV32Session.Get("PromptReferenciaINMGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV34GridState.gxTpr_Filtervalues.Count )
         {
            AV35GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV34GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "REFERENCIAINM_AREATRABALHOCOD") == 0 )
            {
               AV37ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_CODIGO") == 0 )
            {
               AV10TFReferenciaINM_Codigo = (int)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
               AV11TFReferenciaINM_Codigo_To = (int)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO") == 0 )
            {
               AV12TFReferenciaINM_Descricao = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO_SEL") == 0 )
            {
               AV13TFReferenciaINM_Descricao_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_AREATRABALHOCOD") == 0 )
            {
               AV14TFReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
               AV15TFReferenciaINM_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_AREATRABALHODES") == 0 )
            {
               AV16TFReferenciaINM_AreaTrabalhoDes = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_AREATRABALHODES_SEL") == 0 )
            {
               AV17TFReferenciaINM_AreaTrabalhoDes_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_ATIVO_SEL") == 0 )
            {
               AV18TFReferenciaINM_Ativo_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV36GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV39ReferenciaINM_Descricao1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV36GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV42ReferenciaINM_Descricao2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV36GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV45ReferenciaINM_Descricao3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREFERENCIAINM_DESCRICAOOPTIONS' Routine */
         AV12TFReferenciaINM_Descricao = AV19SearchTxt;
         AV13TFReferenciaINM_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ReferenciaINM_Descricao1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ReferenciaINM_Descricao2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ReferenciaINM_Descricao3 ,
                                              AV10TFReferenciaINM_Codigo ,
                                              AV11TFReferenciaINM_Codigo_To ,
                                              AV13TFReferenciaINM_Descricao_Sel ,
                                              AV12TFReferenciaINM_Descricao ,
                                              AV14TFReferenciaINM_AreaTrabalhoCod ,
                                              AV15TFReferenciaINM_AreaTrabalhoCod_To ,
                                              AV17TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                              AV16TFReferenciaINM_AreaTrabalhoDes ,
                                              AV18TFReferenciaINM_Ativo_Sel ,
                                              A710ReferenciaINM_Descricao ,
                                              A709ReferenciaINM_Codigo ,
                                              A712ReferenciaINM_AreaTrabalhoCod ,
                                              A713ReferenciaINM_AreaTrabalhoDes ,
                                              A711ReferenciaINM_Ativo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV39ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV39ReferenciaINM_Descricao1), "%", "");
         lV42ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42ReferenciaINM_Descricao2), "%", "");
         lV45ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV45ReferenciaINM_Descricao3), "%", "");
         lV12TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFReferenciaINM_Descricao), "%", "");
         lV16TFReferenciaINM_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV16TFReferenciaINM_AreaTrabalhoDes), "%", "");
         /* Using cursor P00SB2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV39ReferenciaINM_Descricao1, lV42ReferenciaINM_Descricao2, lV45ReferenciaINM_Descricao3, AV10TFReferenciaINM_Codigo, AV11TFReferenciaINM_Codigo_To, lV12TFReferenciaINM_Descricao, AV13TFReferenciaINM_Descricao_Sel, AV14TFReferenciaINM_AreaTrabalhoCod, AV15TFReferenciaINM_AreaTrabalhoCod_To, lV16TFReferenciaINM_AreaTrabalhoDes, AV17TFReferenciaINM_AreaTrabalhoDes_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSB2 = false;
            A712ReferenciaINM_AreaTrabalhoCod = P00SB2_A712ReferenciaINM_AreaTrabalhoCod[0];
            A710ReferenciaINM_Descricao = P00SB2_A710ReferenciaINM_Descricao[0];
            A711ReferenciaINM_Ativo = P00SB2_A711ReferenciaINM_Ativo[0];
            A713ReferenciaINM_AreaTrabalhoDes = P00SB2_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = P00SB2_n713ReferenciaINM_AreaTrabalhoDes[0];
            A709ReferenciaINM_Codigo = P00SB2_A709ReferenciaINM_Codigo[0];
            A713ReferenciaINM_AreaTrabalhoDes = P00SB2_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = P00SB2_n713ReferenciaINM_AreaTrabalhoDes[0];
            AV31count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SB2_A710ReferenciaINM_Descricao[0], A710ReferenciaINM_Descricao) == 0 ) )
            {
               BRKSB2 = false;
               A709ReferenciaINM_Codigo = P00SB2_A709ReferenciaINM_Codigo[0];
               AV31count = (long)(AV31count+1);
               BRKSB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A710ReferenciaINM_Descricao)) )
            {
               AV23Option = A710ReferenciaINM_Descricao;
               AV24Options.Add(AV23Option, 0);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSB2 )
            {
               BRKSB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADREFERENCIAINM_AREATRABALHODESOPTIONS' Routine */
         AV16TFReferenciaINM_AreaTrabalhoDes = AV19SearchTxt;
         AV17TFReferenciaINM_AreaTrabalhoDes_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ReferenciaINM_Descricao1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ReferenciaINM_Descricao2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ReferenciaINM_Descricao3 ,
                                              AV10TFReferenciaINM_Codigo ,
                                              AV11TFReferenciaINM_Codigo_To ,
                                              AV13TFReferenciaINM_Descricao_Sel ,
                                              AV12TFReferenciaINM_Descricao ,
                                              AV14TFReferenciaINM_AreaTrabalhoCod ,
                                              AV15TFReferenciaINM_AreaTrabalhoCod_To ,
                                              AV17TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                              AV16TFReferenciaINM_AreaTrabalhoDes ,
                                              AV18TFReferenciaINM_Ativo_Sel ,
                                              A710ReferenciaINM_Descricao ,
                                              A709ReferenciaINM_Codigo ,
                                              A712ReferenciaINM_AreaTrabalhoCod ,
                                              A713ReferenciaINM_AreaTrabalhoDes ,
                                              A711ReferenciaINM_Ativo ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV39ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV39ReferenciaINM_Descricao1), "%", "");
         lV42ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV42ReferenciaINM_Descricao2), "%", "");
         lV45ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV45ReferenciaINM_Descricao3), "%", "");
         lV12TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFReferenciaINM_Descricao), "%", "");
         lV16TFReferenciaINM_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV16TFReferenciaINM_AreaTrabalhoDes), "%", "");
         /* Using cursor P00SB3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV39ReferenciaINM_Descricao1, lV42ReferenciaINM_Descricao2, lV45ReferenciaINM_Descricao3, AV10TFReferenciaINM_Codigo, AV11TFReferenciaINM_Codigo_To, lV12TFReferenciaINM_Descricao, AV13TFReferenciaINM_Descricao_Sel, AV14TFReferenciaINM_AreaTrabalhoCod, AV15TFReferenciaINM_AreaTrabalhoCod_To, lV16TFReferenciaINM_AreaTrabalhoDes, AV17TFReferenciaINM_AreaTrabalhoDes_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSB4 = false;
            A712ReferenciaINM_AreaTrabalhoCod = P00SB3_A712ReferenciaINM_AreaTrabalhoCod[0];
            A711ReferenciaINM_Ativo = P00SB3_A711ReferenciaINM_Ativo[0];
            A713ReferenciaINM_AreaTrabalhoDes = P00SB3_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = P00SB3_n713ReferenciaINM_AreaTrabalhoDes[0];
            A709ReferenciaINM_Codigo = P00SB3_A709ReferenciaINM_Codigo[0];
            A710ReferenciaINM_Descricao = P00SB3_A710ReferenciaINM_Descricao[0];
            A713ReferenciaINM_AreaTrabalhoDes = P00SB3_A713ReferenciaINM_AreaTrabalhoDes[0];
            n713ReferenciaINM_AreaTrabalhoDes = P00SB3_n713ReferenciaINM_AreaTrabalhoDes[0];
            AV31count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00SB3_A712ReferenciaINM_AreaTrabalhoCod[0] == A712ReferenciaINM_AreaTrabalhoCod ) )
            {
               BRKSB4 = false;
               A709ReferenciaINM_Codigo = P00SB3_A709ReferenciaINM_Codigo[0];
               AV31count = (long)(AV31count+1);
               BRKSB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A713ReferenciaINM_AreaTrabalhoDes)) )
            {
               AV23Option = A713ReferenciaINM_AreaTrabalhoDes;
               AV22InsertIndex = 1;
               while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
               {
                  AV22InsertIndex = (int)(AV22InsertIndex+1);
               }
               AV24Options.Add(AV23Option, AV22InsertIndex);
               AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
            }
            if ( AV24Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSB4 )
            {
               BRKSB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV24Options = new GxSimpleCollection();
         AV27OptionsDesc = new GxSimpleCollection();
         AV29OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV32Session = context.GetSession();
         AV34GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV35GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFReferenciaINM_Descricao = "";
         AV13TFReferenciaINM_Descricao_Sel = "";
         AV16TFReferenciaINM_AreaTrabalhoDes = "";
         AV17TFReferenciaINM_AreaTrabalhoDes_Sel = "";
         AV36GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV39ReferenciaINM_Descricao1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42ReferenciaINM_Descricao2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45ReferenciaINM_Descricao3 = "";
         scmdbuf = "";
         lV39ReferenciaINM_Descricao1 = "";
         lV42ReferenciaINM_Descricao2 = "";
         lV45ReferenciaINM_Descricao3 = "";
         lV12TFReferenciaINM_Descricao = "";
         lV16TFReferenciaINM_AreaTrabalhoDes = "";
         A710ReferenciaINM_Descricao = "";
         A713ReferenciaINM_AreaTrabalhoDes = "";
         P00SB2_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         P00SB2_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00SB2_A711ReferenciaINM_Ativo = new bool[] {false} ;
         P00SB2_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         P00SB2_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         P00SB2_A709ReferenciaINM_Codigo = new int[1] ;
         AV23Option = "";
         P00SB3_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         P00SB3_A711ReferenciaINM_Ativo = new bool[] {false} ;
         P00SB3_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         P00SB3_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         P00SB3_A709ReferenciaINM_Codigo = new int[1] ;
         P00SB3_A710ReferenciaINM_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptreferenciainmfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SB2_A712ReferenciaINM_AreaTrabalhoCod, P00SB2_A710ReferenciaINM_Descricao, P00SB2_A711ReferenciaINM_Ativo, P00SB2_A713ReferenciaINM_AreaTrabalhoDes, P00SB2_n713ReferenciaINM_AreaTrabalhoDes, P00SB2_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               P00SB3_A712ReferenciaINM_AreaTrabalhoCod, P00SB3_A711ReferenciaINM_Ativo, P00SB3_A713ReferenciaINM_AreaTrabalhoDes, P00SB3_n713ReferenciaINM_AreaTrabalhoDes, P00SB3_A709ReferenciaINM_Codigo, P00SB3_A710ReferenciaINM_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFReferenciaINM_Ativo_Sel ;
      private int AV48GXV1 ;
      private int AV37ReferenciaINM_AreaTrabalhoCod ;
      private int AV10TFReferenciaINM_Codigo ;
      private int AV11TFReferenciaINM_Codigo_To ;
      private int AV14TFReferenciaINM_AreaTrabalhoCod ;
      private int AV15TFReferenciaINM_AreaTrabalhoCod_To ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A709ReferenciaINM_Codigo ;
      private int A712ReferenciaINM_AreaTrabalhoCod ;
      private int AV22InsertIndex ;
      private long AV31count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool A711ReferenciaINM_Ativo ;
      private bool BRKSB2 ;
      private bool n713ReferenciaINM_AreaTrabalhoDes ;
      private bool BRKSB4 ;
      private String AV30OptionIndexesJson ;
      private String AV25OptionsJson ;
      private String AV28OptionsDescJson ;
      private String AV21DDOName ;
      private String AV19SearchTxt ;
      private String AV20SearchTxtTo ;
      private String AV12TFReferenciaINM_Descricao ;
      private String AV13TFReferenciaINM_Descricao_Sel ;
      private String AV16TFReferenciaINM_AreaTrabalhoDes ;
      private String AV17TFReferenciaINM_AreaTrabalhoDes_Sel ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV39ReferenciaINM_Descricao1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV42ReferenciaINM_Descricao2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV45ReferenciaINM_Descricao3 ;
      private String lV39ReferenciaINM_Descricao1 ;
      private String lV42ReferenciaINM_Descricao2 ;
      private String lV45ReferenciaINM_Descricao3 ;
      private String lV12TFReferenciaINM_Descricao ;
      private String lV16TFReferenciaINM_AreaTrabalhoDes ;
      private String A710ReferenciaINM_Descricao ;
      private String A713ReferenciaINM_AreaTrabalhoDes ;
      private String AV23Option ;
      private IGxSession AV32Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SB2_A712ReferenciaINM_AreaTrabalhoCod ;
      private String[] P00SB2_A710ReferenciaINM_Descricao ;
      private bool[] P00SB2_A711ReferenciaINM_Ativo ;
      private String[] P00SB2_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] P00SB2_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] P00SB2_A709ReferenciaINM_Codigo ;
      private int[] P00SB3_A712ReferenciaINM_AreaTrabalhoCod ;
      private bool[] P00SB3_A711ReferenciaINM_Ativo ;
      private String[] P00SB3_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] P00SB3_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] P00SB3_A709ReferenciaINM_Codigo ;
      private String[] P00SB3_A710ReferenciaINM_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV34GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV35GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV36GridStateDynamicFilter ;
   }

   public class getpromptreferenciainmfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SB2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ReferenciaINM_Descricao1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ReferenciaINM_Descricao2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ReferenciaINM_Descricao3 ,
                                             int AV10TFReferenciaINM_Codigo ,
                                             int AV11TFReferenciaINM_Codigo_To ,
                                             String AV13TFReferenciaINM_Descricao_Sel ,
                                             String AV12TFReferenciaINM_Descricao ,
                                             int AV14TFReferenciaINM_AreaTrabalhoCod ,
                                             int AV15TFReferenciaINM_AreaTrabalhoCod_To ,
                                             String AV17TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                             String AV16TFReferenciaINM_AreaTrabalhoDes ,
                                             short AV18TFReferenciaINM_Ativo_Sel ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A709ReferenciaINM_Codigo ,
                                             int A712ReferenciaINM_AreaTrabalhoCod ,
                                             String A713ReferenciaINM_AreaTrabalhoDes ,
                                             bool A711ReferenciaINM_Ativo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod, T1.[ReferenciaINM_Descricao], T1.[ReferenciaINM_Ativo], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, T1.[ReferenciaINM_Codigo] FROM ([ReferenciaINM] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ReferenciaINM_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ReferenciaINM_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ReferenciaINM_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV39ReferenciaINM_Descricao1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ReferenciaINM_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV42ReferenciaINM_Descricao2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ReferenciaINM_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV45ReferenciaINM_Descricao3 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV10TFReferenciaINM_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV10TFReferenciaINM_Codigo)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV11TFReferenciaINM_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV11TFReferenciaINM_Codigo_To)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFReferenciaINM_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like @lV12TFReferenciaINM_Descricao)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaINM_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] = @AV13TFReferenciaINM_Descricao_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV14TFReferenciaINM_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] >= @AV14TFReferenciaINM_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV15TFReferenciaINM_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] <= @AV15TFReferenciaINM_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFReferenciaINM_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFReferenciaINM_AreaTrabalhoDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV16TFReferenciaINM_AreaTrabalhoDes)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFReferenciaINM_AreaTrabalhoDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV17TFReferenciaINM_AreaTrabalhoDes_Sel)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV18TFReferenciaINM_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 1)";
         }
         if ( AV18TFReferenciaINM_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaINM_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SB3( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ReferenciaINM_Descricao1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ReferenciaINM_Descricao2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ReferenciaINM_Descricao3 ,
                                             int AV10TFReferenciaINM_Codigo ,
                                             int AV11TFReferenciaINM_Codigo_To ,
                                             String AV13TFReferenciaINM_Descricao_Sel ,
                                             String AV12TFReferenciaINM_Descricao ,
                                             int AV14TFReferenciaINM_AreaTrabalhoCod ,
                                             int AV15TFReferenciaINM_AreaTrabalhoCod_To ,
                                             String AV17TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                             String AV16TFReferenciaINM_AreaTrabalhoDes ,
                                             short AV18TFReferenciaINM_Ativo_Sel ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A709ReferenciaINM_Codigo ,
                                             int A712ReferenciaINM_AreaTrabalhoCod ,
                                             String A713ReferenciaINM_AreaTrabalhoDes ,
                                             bool A711ReferenciaINM_Ativo ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod, T1.[ReferenciaINM_Ativo], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, T1.[ReferenciaINM_Codigo], T1.[ReferenciaINM_Descricao] FROM ([ReferenciaINM] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ReferenciaINM_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ReferenciaINM_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ReferenciaINM_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV39ReferenciaINM_Descricao1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ReferenciaINM_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV42ReferenciaINM_Descricao2 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ReferenciaINM_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV45ReferenciaINM_Descricao3 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV10TFReferenciaINM_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV10TFReferenciaINM_Codigo)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV11TFReferenciaINM_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV11TFReferenciaINM_Codigo_To)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFReferenciaINM_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like @lV12TFReferenciaINM_Descricao)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFReferenciaINM_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] = @AV13TFReferenciaINM_Descricao_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV14TFReferenciaINM_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] >= @AV14TFReferenciaINM_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV15TFReferenciaINM_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] <= @AV15TFReferenciaINM_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFReferenciaINM_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFReferenciaINM_AreaTrabalhoDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV16TFReferenciaINM_AreaTrabalhoDes)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFReferenciaINM_AreaTrabalhoDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV17TFReferenciaINM_AreaTrabalhoDes_Sel)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV18TFReferenciaINM_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 1)";
         }
         if ( AV18TFReferenciaINM_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaINM_AreaTrabalhoCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] );
               case 1 :
                     return conditional_P00SB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SB2 ;
          prmP00SB2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV45ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFReferenciaINM_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFReferenciaINM_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFReferenciaINM_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00SB3 ;
          prmP00SB3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV45ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFReferenciaINM_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFReferenciaINM_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFReferenciaINM_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SB2,100,0,true,false )
             ,new CursorDef("P00SB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SB3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptreferenciainmfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptreferenciainmfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptreferenciainmfilterdata") )
          {
             return  ;
          }
          getpromptreferenciainmfilterdata worker = new getpromptreferenciainmfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
