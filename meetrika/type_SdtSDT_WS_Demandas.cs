/*
               File: type_SdtSDT_WS_Demandas
        Description: SDT_WS_Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:7.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Demanda" )]
   [XmlType(TypeName =  "Demanda" , Namespace = "Demanda" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_WS_Demandas_Demanda ))]
   [Serializable]
   public class SdtSDT_WS_Demandas : GxUserType
   {
      public SdtSDT_WS_Demandas( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WS_Demandas_Situacao = "";
      }

      public SdtSDT_WS_Demandas( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WS_Demandas deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "Demanda" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WS_Demandas)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WS_Demandas obj ;
         obj = this;
         obj.gxTpr_Situacao = deserialized.gxTpr_Situacao;
         obj.gxTpr_Demandas = deserialized.gxTpr_Demandas;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("Situacao") == 1 )
         {
            gxTv_SdtSDT_WS_Demandas_Situacao = oReader.GetAttributeByName("Situacao");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Demandas") )
               {
                  if ( gxTv_SdtSDT_WS_Demandas_Demandas == null )
                  {
                     gxTv_SdtSDT_WS_Demandas_Demandas = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "Demanda", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_WS_Demandas_Demandas.readxmlcollection(oReader, "Demandas", "Demanda");
                  }
                  gxTv_SdtSDT_WS_Demandas_Demandas_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Demanda";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "Demanda";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("Situacao", StringUtil.RTrim( gxTv_SdtSDT_WS_Demandas_Situacao));
         if ( gxTv_SdtSDT_WS_Demandas_Demandas != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "Demanda") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "Demanda";
            }
            else
            {
               sNameSpace1 = "Demanda";
            }
            gxTv_SdtSDT_WS_Demandas_Demandas.writexmlcollection(oWriter, "Demandas", sNameSpace1, "Demanda", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Situacao", gxTv_SdtSDT_WS_Demandas_Situacao, false);
         if ( gxTv_SdtSDT_WS_Demandas_Demandas != null )
         {
            AddObjectProperty("Demandas", gxTv_SdtSDT_WS_Demandas_Demandas, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "Situacao" )]
      [XmlAttribute( AttributeName = "Situacao" )]
      public String gxTpr_Situacao
      {
         get {
            return gxTv_SdtSDT_WS_Demandas_Situacao ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Situacao = (String)(value);
         }

      }

      public class gxTv_SdtSDT_WS_Demandas_Demandas_SdtSDT_WS_Demandas_Demanda_80compatibility:SdtSDT_WS_Demandas_Demanda {}
      [  SoapElement( ElementName = "Demandas" )]
      [  XmlArray( ElementName = "Demandas"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_WS_Demandas_Demanda ), ElementName= "Demanda"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_WS_Demandas_Demandas_SdtSDT_WS_Demandas_Demanda_80compatibility ), ElementName= "SDT_WS_Demandas.Demanda"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Demandas_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_WS_Demandas_Demandas == null )
            {
               gxTv_SdtSDT_WS_Demandas_Demandas = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "Demanda", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_WS_Demandas_Demandas ;
         }

         set {
            if ( gxTv_SdtSDT_WS_Demandas_Demandas == null )
            {
               gxTv_SdtSDT_WS_Demandas_Demandas = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "Demanda", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs");
            }
            gxTv_SdtSDT_WS_Demandas_Demandas_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demandas = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Demandas
      {
         get {
            if ( gxTv_SdtSDT_WS_Demandas_Demandas == null )
            {
               gxTv_SdtSDT_WS_Demandas_Demandas = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "Demanda", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs");
            }
            gxTv_SdtSDT_WS_Demandas_Demandas_N = 0;
            return gxTv_SdtSDT_WS_Demandas_Demandas ;
         }

         set {
            gxTv_SdtSDT_WS_Demandas_Demandas_N = 0;
            gxTv_SdtSDT_WS_Demandas_Demandas = value;
         }

      }

      public void gxTv_SdtSDT_WS_Demandas_Demandas_SetNull( )
      {
         gxTv_SdtSDT_WS_Demandas_Demandas_N = 1;
         gxTv_SdtSDT_WS_Demandas_Demandas = null;
         return  ;
      }

      public bool gxTv_SdtSDT_WS_Demandas_Demandas_IsNull( )
      {
         if ( gxTv_SdtSDT_WS_Demandas_Demandas == null )
         {
            return true ;
         }
         return false ;
      }

      public bool ShouldSerializegxTpr_Demandas_GxObjectCollection( )
      {
         return (bool)(gxTv_SdtSDT_WS_Demandas_Demandas != null &&(gxTv_SdtSDT_WS_Demandas_Demandas.Count>0)) ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_WS_Demandas_Situacao = "";
         gxTv_SdtSDT_WS_Demandas_Demandas_N = 1;
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_WS_Demandas_Demandas_N ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_WS_Demandas_Situacao ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_Demandas_Demanda ))]
      protected IGxCollection gxTv_SdtSDT_WS_Demandas_Demandas=null ;
   }

   [DataContract(Name = @"SDT_WS_Demandas", Namespace = "Demanda")]
   public class SdtSDT_WS_Demandas_RESTInterface : GxGenericCollectionItem<SdtSDT_WS_Demandas>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WS_Demandas_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WS_Demandas_RESTInterface( SdtSDT_WS_Demandas psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Situacao" , Order = 0 )]
      public String gxTpr_Situacao
      {
         get {
            return sdt.gxTpr_Situacao ;
         }

         set {
            sdt.gxTpr_Situacao = (String)(value);
         }

      }

      [DataMember( Name = "Demandas" , Order = 1 )]
      public GxGenericCollection<SdtSDT_WS_Demandas_Demanda_RESTInterface> gxTpr_Demandas
      {
         get {
            return new GxGenericCollection<SdtSDT_WS_Demandas_Demanda_RESTInterface>(sdt.gxTpr_Demandas) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Demandas);
         }

      }

      public SdtSDT_WS_Demandas sdt
      {
         get {
            return (SdtSDT_WS_Demandas)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WS_Demandas() ;
         }
      }

   }

}
