/*
               File: type_SdtGAMAuthenticationFacebook
        Description: GAMAuthenticationFacebook
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:43.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationFacebook : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationFacebook( )
      {
         initialize();
      }

      public SdtGAMAuthenticationFacebook( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationFacebook_externalReference == null )
         {
            GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationFacebook_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Clientid
      {
         get {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            return GAMAuthenticationFacebook_externalReference.ClientId ;
         }

         set {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            GAMAuthenticationFacebook_externalReference.ClientId = value;
         }

      }

      public String gxTpr_Clientsecret
      {
         get {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            return GAMAuthenticationFacebook_externalReference.ClientSecret ;
         }

         set {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            GAMAuthenticationFacebook_externalReference.ClientSecret = value;
         }

      }

      public String gxTpr_Siteurl
      {
         get {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            return GAMAuthenticationFacebook_externalReference.SiteURL ;
         }

         set {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            GAMAuthenticationFacebook_externalReference.SiteURL = value;
         }

      }

      public String gxTpr_Additionalscope
      {
         get {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            return GAMAuthenticationFacebook_externalReference.AdditionalScope ;
         }

         set {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            GAMAuthenticationFacebook_externalReference.AdditionalScope = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationFacebook_externalReference == null )
            {
               GAMAuthenticationFacebook_externalReference = new Artech.Security.GAMAuthenticationFacebook(context);
            }
            return GAMAuthenticationFacebook_externalReference ;
         }

         set {
            GAMAuthenticationFacebook_externalReference = (Artech.Security.GAMAuthenticationFacebook)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationFacebook GAMAuthenticationFacebook_externalReference=null ;
   }

}
