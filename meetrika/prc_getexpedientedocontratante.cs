/*
               File: PRC_GetExpedienteDoContratante
        Description: Get Expediente
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:26.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getexpedientedocontratante : GXProcedure
   {
      public prc_getexpedientedocontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getexpedientedocontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           ref DateTime aP1_InicioDoExpediente ,
                           ref DateTime aP2_FimDoExpediente )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9InicioDoExpediente = aP1_InicioDoExpediente;
         this.AV8FimDoExpediente = aP2_FimDoExpediente;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_InicioDoExpediente=this.AV9InicioDoExpediente;
         aP2_FimDoExpediente=this.AV8FimDoExpediente;
      }

      public DateTime executeUdp( ref int aP0_AreaTrabalho_Codigo ,
                                  ref DateTime aP1_InicioDoExpediente )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9InicioDoExpediente = aP1_InicioDoExpediente;
         this.AV8FimDoExpediente = aP2_FimDoExpediente;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_InicioDoExpediente=this.AV9InicioDoExpediente;
         aP2_FimDoExpediente=this.AV8FimDoExpediente;
         return AV8FimDoExpediente ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 ref DateTime aP1_InicioDoExpediente ,
                                 ref DateTime aP2_FimDoExpediente )
      {
         prc_getexpedientedocontratante objprc_getexpedientedocontratante;
         objprc_getexpedientedocontratante = new prc_getexpedientedocontratante();
         objprc_getexpedientedocontratante.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_getexpedientedocontratante.AV9InicioDoExpediente = aP1_InicioDoExpediente;
         objprc_getexpedientedocontratante.AV8FimDoExpediente = aP2_FimDoExpediente;
         objprc_getexpedientedocontratante.context.SetSubmitInitialConfig(context);
         objprc_getexpedientedocontratante.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getexpedientedocontratante);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_InicioDoExpediente=this.AV9InicioDoExpediente;
         aP2_FimDoExpediente=this.AV8FimDoExpediente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getexpedientedocontratante)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AG2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00AG2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00AG2_n29Contratante_Codigo[0];
            A1448Contratante_InicioDoExpediente = P00AG2_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00AG2_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = P00AG2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00AG2_n1192Contratante_FimDoExpediente[0];
            A1448Contratante_InicioDoExpediente = P00AG2_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = P00AG2_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = P00AG2_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = P00AG2_n1192Contratante_FimDoExpediente[0];
            AV9InicioDoExpediente = A1448Contratante_InicioDoExpediente;
            AV8FimDoExpediente = A1192Contratante_FimDoExpediente;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AG2_A29Contratante_Codigo = new int[1] ;
         P00AG2_n29Contratante_Codigo = new bool[] {false} ;
         P00AG2_A5AreaTrabalho_Codigo = new int[1] ;
         P00AG2_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00AG2_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         P00AG2_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         P00AG2_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getexpedientedocontratante__default(),
            new Object[][] {
                new Object[] {
               P00AG2_A29Contratante_Codigo, P00AG2_n29Contratante_Codigo, P00AG2_A5AreaTrabalho_Codigo, P00AG2_A1448Contratante_InicioDoExpediente, P00AG2_n1448Contratante_InicioDoExpediente, P00AG2_A1192Contratante_FimDoExpediente, P00AG2_n1192Contratante_FimDoExpediente
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private DateTime AV9InicioDoExpediente ;
      private DateTime AV8FimDoExpediente ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool n29Contratante_Codigo ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n1192Contratante_FimDoExpediente ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private DateTime aP1_InicioDoExpediente ;
      private DateTime aP2_FimDoExpediente ;
      private IDataStoreProvider pr_default ;
      private int[] P00AG2_A29Contratante_Codigo ;
      private bool[] P00AG2_n29Contratante_Codigo ;
      private int[] P00AG2_A5AreaTrabalho_Codigo ;
      private DateTime[] P00AG2_A1448Contratante_InicioDoExpediente ;
      private bool[] P00AG2_n1448Contratante_InicioDoExpediente ;
      private DateTime[] P00AG2_A1192Contratante_FimDoExpediente ;
      private bool[] P00AG2_n1192Contratante_FimDoExpediente ;
   }

   public class prc_getexpedientedocontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AG2 ;
          prmP00AG2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AG2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_InicioDoExpediente], T2.[Contratante_FimDoExpediente] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AG2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
