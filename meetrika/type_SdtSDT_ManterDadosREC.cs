/*
               File: type_SdtSDT_ManterDadosREC
        Description: SDT_ManterDadosREC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:20.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ManterDadosREC" )]
   [XmlType(TypeName =  "SDT_ManterDadosREC" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_ManterDadosREC : GxUserType
   {
      public SdtSDT_ManterDadosREC( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ManterDadosREC_Datadmn = DateTime.MinValue;
         gxTv_SdtSDT_ManterDadosREC_Demandafm = "";
      }

      public SdtSDT_ManterDadosREC( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ManterDadosREC deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ManterDadosREC)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ManterDadosREC obj ;
         obj = this;
         obj.gxTpr_Manterdados = deserialized.gxTpr_Manterdados;
         obj.gxTpr_Contratadacod = deserialized.gxTpr_Contratadacod;
         obj.gxTpr_Contratadaorigemcod = deserialized.gxTpr_Contratadaorigemcod;
         obj.gxTpr_Datadmn = deserialized.gxTpr_Datadmn;
         obj.gxTpr_Demandafm = deserialized.gxTpr_Demandafm;
         obj.gxTpr_Ehvalidacao = deserialized.gxTpr_Ehvalidacao;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ManterDados") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Manterdados = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaCod") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaOrigemCod") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataDmn") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_ManterDadosREC_Datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_ManterDadosREC_Datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DemandaFM") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EhValidacao") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Ehvalidacao = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtSDT_ManterDadosREC_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ManterDadosREC";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ManterDados", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_ManterDadosREC_Manterdados)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ManterDadosREC_Contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_ManterDadosREC_Datadmn) )
         {
            oWriter.WriteStartElement("DataDmn");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataDmn", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("DemandaFM", StringUtil.RTrim( gxTv_SdtSDT_ManterDadosREC_Demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("EhValidacao", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_ManterDadosREC_Ehvalidacao)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ManterDadosREC_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ManterDadosREC_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ManterDados", gxTv_SdtSDT_ManterDadosREC_Manterdados, false);
         AddObjectProperty("ContratadaCod", gxTv_SdtSDT_ManterDadosREC_Contratadacod, false);
         AddObjectProperty("ContratadaOrigemCod", gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_ManterDadosREC_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataDmn", sDateCnv, false);
         AddObjectProperty("DemandaFM", gxTv_SdtSDT_ManterDadosREC_Demandafm, false);
         AddObjectProperty("EhValidacao", gxTv_SdtSDT_ManterDadosREC_Ehvalidacao, false);
         AddObjectProperty("Sistema_Codigo", gxTv_SdtSDT_ManterDadosREC_Sistema_codigo, false);
         AddObjectProperty("Servico_Codigo", gxTv_SdtSDT_ManterDadosREC_Servico_codigo, false);
         return  ;
      }

      [  SoapElement( ElementName = "ManterDados" )]
      [  XmlElement( ElementName = "ManterDados"   )]
      public bool gxTpr_Manterdados
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Manterdados ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Manterdados = value;
         }

      }

      [  SoapElement( ElementName = "ContratadaCod" )]
      [  XmlElement( ElementName = "ContratadaCod"   )]
      public int gxTpr_Contratadacod
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Contratadacod ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Contratadacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContratadaOrigemCod"   )]
      public int gxTpr_Contratadaorigemcod
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "DataDmn" )]
      [  XmlElement( ElementName = "DataDmn"  , IsNullable=true )]
      public string gxTpr_Datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSDT_ManterDadosREC_Datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_ManterDadosREC_Datadmn).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_ManterDadosREC_Datadmn = DateTime.MinValue;
            else
               gxTv_SdtSDT_ManterDadosREC_Datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Datadmn
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Datadmn ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Datadmn = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "DemandaFM" )]
      [  XmlElement( ElementName = "DemandaFM"   )]
      public String gxTpr_Demandafm
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Demandafm ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Demandafm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "EhValidacao" )]
      [  XmlElement( ElementName = "EhValidacao"   )]
      public bool gxTpr_Ehvalidacao
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Ehvalidacao ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Ehvalidacao = value;
         }

      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Sistema_codigo ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtSDT_ManterDadosREC_Servico_codigo ;
         }

         set {
            gxTv_SdtSDT_ManterDadosREC_Servico_codigo = (int)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ManterDadosREC_Datadmn = DateTime.MinValue;
         gxTv_SdtSDT_ManterDadosREC_Demandafm = "";
         gxTv_SdtSDT_ManterDadosREC_Ehvalidacao = false;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_ManterDadosREC_Contratadacod ;
      protected int gxTv_SdtSDT_ManterDadosREC_Contratadaorigemcod ;
      protected int gxTv_SdtSDT_ManterDadosREC_Sistema_codigo ;
      protected int gxTv_SdtSDT_ManterDadosREC_Servico_codigo ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_ManterDadosREC_Datadmn ;
      protected bool gxTv_SdtSDT_ManterDadosREC_Manterdados ;
      protected bool gxTv_SdtSDT_ManterDadosREC_Ehvalidacao ;
      protected String gxTv_SdtSDT_ManterDadosREC_Demandafm ;
   }

   [DataContract(Name = @"SDT_ManterDadosREC", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_ManterDadosREC_RESTInterface : GxGenericCollectionItem<SdtSDT_ManterDadosREC>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ManterDadosREC_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ManterDadosREC_RESTInterface( SdtSDT_ManterDadosREC psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ManterDados" , Order = 0 )]
      public bool gxTpr_Manterdados
      {
         get {
            return sdt.gxTpr_Manterdados ;
         }

         set {
            sdt.gxTpr_Manterdados = value;
         }

      }

      [DataMember( Name = "ContratadaCod" , Order = 1 )]
      public Nullable<int> gxTpr_Contratadacod
      {
         get {
            return sdt.gxTpr_Contratadacod ;
         }

         set {
            sdt.gxTpr_Contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaOrigemCod" , Order = 2 )]
      public Nullable<int> gxTpr_Contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "DataDmn" , Order = 3 )]
      public String gxTpr_Datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Datadmn) ;
         }

         set {
            sdt.gxTpr_Datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "DemandaFM" , Order = 4 )]
      public String gxTpr_Demandafm
      {
         get {
            return sdt.gxTpr_Demandafm ;
         }

         set {
            sdt.gxTpr_Demandafm = (String)(value);
         }

      }

      [DataMember( Name = "EhValidacao" , Order = 5 )]
      public bool gxTpr_Ehvalidacao
      {
         get {
            return sdt.gxTpr_Ehvalidacao ;
         }

         set {
            sdt.gxTpr_Ehvalidacao = value;
         }

      }

      [DataMember( Name = "Sistema_Codigo" , Order = 6 )]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Codigo" , Order = 7 )]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_ManterDadosREC sdt
      {
         get {
            return (SdtSDT_ManterDadosREC)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ManterDadosREC() ;
         }
      }

   }

}
