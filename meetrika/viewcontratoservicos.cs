/*
               File: ViewContratoServicos
        Description: View Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:17:4.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewcontratoservicos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewcontratoservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewcontratoservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           String aP1_TabCode )
      {
         this.AV9ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV7TabCode = aP1_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9ContratoServicos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContratoServicos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContratoServicos_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA762( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START762( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020315231750");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewContratoServicos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewcontratoservicos:[SendSecurityCheck value for]"+"ContratoServicos_ServicoSigla:"+StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE762( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT762( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewContratoServicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contrato Servicos" ;
      }

      protected void WB760( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_762( true) ;
         }
         else
         {
            wb_table1_2_762( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_762e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START762( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Contrato Servicos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP760( ) ;
      }

      protected void WS762( )
      {
         START762( ) ;
         EVT762( ) ;
      }

      protected void EVT762( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11762 */
                              E11762 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12762 */
                              E12762 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 17 )
                        {
                           OldTabbedview = cgiGet( "W0017");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0017", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE762( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA762( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF762( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF762( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00762 */
            pr_default.execute(0, new Object[] {AV9ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A155Servico_Codigo = H00762_A155Servico_Codigo[0];
               A160ContratoServicos_Codigo = H00762_A160ContratoServicos_Codigo[0];
               A605Servico_Sigla = H00762_A605Servico_Sigla[0];
               A605Servico_Sigla = H00762_A605Servico_Sigla[0];
               A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
               /* Execute user event: E12762 */
               E12762 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB760( ) ;
         }
      }

      protected void STRUP760( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11762 */
         E11762 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A826ContratoServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoServicos_ServicoSigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewContratoServicos";
            A826ContratoServicos_ServicoSigla = cgiGet( edtContratoServicos_ServicoSigla_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewcontratoservicos:[SecurityCheckFailed value for]"+"ContratoServicos_ServicoSigla:"+StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11762 */
         E11762 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11762( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwcontratoservicos.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV18GXLvl9 = 0;
         /* Using cursor H00763 */
         pr_default.execute(1, new Object[] {AV9ContratoServicos_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A155Servico_Codigo = H00763_A155Servico_Codigo[0];
            A160ContratoServicos_Codigo = H00763_A160ContratoServicos_Codigo[0];
            A605Servico_Sigla = H00763_A605Servico_Sigla[0];
            A605Servico_Sigla = H00763_A605Servico_Sigla[0];
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A826ContratoServicos_ServicoSigla", A826ContratoServicos_ServicoSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATOSERVICOS_SERVICOSIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!"))));
            AV18GXLvl9 = 1;
            Form.Caption = A826ContratoServicos_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV18GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0017",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         lblWorkwithlink_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         lblWorkwithlink_Jsonclick = "window.history.back()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Jsonclick", lblWorkwithlink_Jsonclick);
      }

      protected void nextLoad( )
      {
      }

      protected void E12762( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Servi�o";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicosgeneral.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoServicosVnc";
         AV11Tab.gxTpr_Description = "Regras";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicos_vncwc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoServicosTelas";
         AV11Tab.gxTpr_Description = "Telas";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicos_telaswc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Prioridades";
         AV11Tab.gxTpr_Description = "Prioridades";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicosprioridadeswc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Indicadores";
         AV11Tab.gxTpr_Description = "Indicadores";
         AV11Tab.gxTpr_Webcomponent = formatLink("controservicoindicadoreswc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Custos";
         AV11Tab.gxTpr_Description = "Custos";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicoscustowc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Artefatos";
         AV11Tab.gxTpr_Description = "Artefatos";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicosartefatoswc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Remuneracao";
         AV11Tab.gxTpr_Description = "Remunera��o";
         AV11Tab.gxTpr_Webcomponent = formatLink("cntsrvremuneracaowc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoServicosUnidConversao";
         AV11Tab.gxTpr_Description = "Convers�o USC";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoservicoscontratoservicosunidconversaowc.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +AV9ContratoServicos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         /* Using cursor H00764 */
         pr_default.execute(2, new Object[] {AV9ContratoServicos_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A160ContratoServicos_Codigo = H00764_A160ContratoServicos_Codigo[0];
            A2074ContratoServicos_CalculoRmn = H00764_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = H00764_n2074ContratoServicos_CalculoRmn[0];
            A913ContratoServicos_PrazoTipo = H00764_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = H00764_n913ContratoServicos_PrazoTipo[0];
            A913ContratoServicos_PrazoTipo = H00764_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = H00764_n913ContratoServicos_PrazoTipo[0];
            AV15Index = 2;
            if ( StringUtil.StrCmp(A913ContratoServicos_PrazoTipo, "O") == 0 )
            {
               AV10Tabs.RemoveItem(2);
            }
            else
            {
               AV14Count = 0;
               /* Using cursor H00765 */
               pr_default.execute(3, new Object[] {AV9ContratoServicos_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A915ContratoSrvVnc_CntSrvCod = H00765_A915ContratoSrvVnc_CntSrvCod[0];
                  AV14Count = (short)(AV14Count+1);
                  ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Regras ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               AV15Index = (short)(AV15Index+1);
            }
            AV14Count = 0;
            /* Using cursor H00766 */
            pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A926ContratoServicosTelas_ContratoCod = H00766_A926ContratoServicosTelas_ContratoCod[0];
               AV14Count = (short)(AV14Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Telas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV15Index = (short)(AV15Index+1);
            AV14Count = 0;
            /* Using cursor H00767 */
            pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1335ContratoServicosPrioridade_CntSrvCod = H00767_A1335ContratoServicosPrioridade_CntSrvCod[0];
               AV14Count = (short)(AV14Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Prioridades ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV15Index = (short)(AV15Index+1);
            AV14Count = 0;
            /* Using cursor H00768 */
            pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1270ContratoServicosIndicador_CntSrvCod = H00768_A1270ContratoServicosIndicador_CntSrvCod[0];
               AV14Count = (short)(AV14Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Indicadores ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
               pr_default.readNext(6);
            }
            pr_default.close(6);
            AV15Index = (short)(AV15Index+1);
            AV14Count = 0;
            /* Using cursor H00769 */
            pr_default.execute(7, new Object[] {AV9ContratoServicos_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A1471ContratoServicosCusto_CntSrvCod = H00769_A1471ContratoServicosCusto_CntSrvCod[0];
               AV14Count = (short)(AV14Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Custos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
               pr_default.readNext(7);
            }
            pr_default.close(7);
            AV15Index = (short)(AV15Index+1);
            AV14Count = 0;
            /* Using cursor H007610 */
            pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               AV14Count = (short)(AV14Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Artefatos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
               pr_default.readNext(8);
            }
            pr_default.close(8);
            AV15Index = (short)(AV15Index+1);
            if ( A2074ContratoServicos_CalculoRmn > 0 )
            {
               AV14Count = 0;
               /* Using cursor H007611 */
               pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A2069ContratoServicosRmn_Sequencial = H007611_A2069ContratoServicosRmn_Sequencial[0];
                  AV14Count = (short)(AV14Count+1);
                  ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV15Index)).gxTpr_Description = "Remunera��o ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV14Count), 4, 0))+")";
                  pr_default.readNext(9);
               }
               pr_default.close(9);
            }
            else
            {
               AV10Tabs.RemoveItem(AV15Index);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_762( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_7_762( true) ;
         }
         else
         {
            wb_table2_7_762( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_762e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0017"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0017"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0017"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_762e( true) ;
         }
         else
         {
            wb_table1_2_762e( false) ;
         }
      }

      protected void wb_table2_7_762( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Servi�o :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_ServicoSigla_Internalname, StringUtil.RTrim( A826ContratoServicos_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A826ContratoServicos_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_ServicoSigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewContratoServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_762e( true) ;
         }
         else
         {
            wb_table2_7_762e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContratoServicos_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA762( ) ;
         WS762( ) ;
         WE762( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203152317557");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewcontratoservicos.js", "?20203152317557");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtContratoServicos_ServicoSigla_Internalname = "CONTRATOSERVICOS_SERVICOSIGLA";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicos_ServicoSigla_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Contrato Servicos";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A605Servico_Sigla = "";
         A826ContratoServicos_ServicoSigla = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00762_A155Servico_Codigo = new int[1] ;
         H00762_A160ContratoServicos_Codigo = new int[1] ;
         H00762_A605Servico_Sigla = new String[] {""} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00763_A155Servico_Codigo = new int[1] ;
         H00763_A160ContratoServicos_Codigo = new int[1] ;
         H00763_A605Servico_Sigla = new String[] {""} ;
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         lblWorkwithlink_Jsonclick = "";
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         H00764_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00764_A160ContratoServicos_Codigo = new int[1] ;
         H00764_A2074ContratoServicos_CalculoRmn = new short[1] ;
         H00764_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         H00764_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00764_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         A913ContratoServicos_PrazoTipo = "";
         H00765_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00765_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00766_A938ContratoServicosTelas_Sequencial = new short[1] ;
         H00766_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         H00767_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00767_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00768_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00768_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00769_A1473ContratoServicosCusto_Codigo = new int[1] ;
         H00769_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         H007610_A1749Artefatos_Codigo = new int[1] ;
         H007610_A160ContratoServicos_Codigo = new int[1] ;
         H007611_A160ContratoServicos_Codigo = new int[1] ;
         H007611_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewcontratoservicos__default(),
            new Object[][] {
                new Object[] {
               H00762_A155Servico_Codigo, H00762_A160ContratoServicos_Codigo, H00762_A605Servico_Sigla
               }
               , new Object[] {
               H00763_A155Servico_Codigo, H00763_A160ContratoServicos_Codigo, H00763_A605Servico_Sigla
               }
               , new Object[] {
               H00764_A903ContratoServicosPrazo_CntSrvCod, H00764_A160ContratoServicos_Codigo, H00764_A2074ContratoServicos_CalculoRmn, H00764_n2074ContratoServicos_CalculoRmn, H00764_A913ContratoServicos_PrazoTipo, H00764_n913ContratoServicos_PrazoTipo
               }
               , new Object[] {
               H00765_A917ContratoSrvVnc_Codigo, H00765_A915ContratoSrvVnc_CntSrvCod
               }
               , new Object[] {
               H00766_A938ContratoServicosTelas_Sequencial, H00766_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               H00767_A1336ContratoServicosPrioridade_Codigo, H00767_A1335ContratoServicosPrioridade_CntSrvCod
               }
               , new Object[] {
               H00768_A1269ContratoServicosIndicador_Codigo, H00768_A1270ContratoServicosIndicador_CntSrvCod
               }
               , new Object[] {
               H00769_A1473ContratoServicosCusto_Codigo, H00769_A1471ContratoServicosCusto_CntSrvCod
               }
               , new Object[] {
               H007610_A1749Artefatos_Codigo, H007610_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H007611_A160ContratoServicos_Codigo, H007611_A2069ContratoServicosRmn_Sequencial
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV18GXLvl9 ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short AV15Index ;
      private short AV14Count ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short nGXWrapped ;
      private int AV9ContratoServicos_Codigo ;
      private int wcpOAV9ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A605Servico_Sigla ;
      private String A826ContratoServicos_ServicoSigla ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtContratoServicos_ServicoSigla_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String A913ContratoServicos_PrazoTipo ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtContratoServicos_ServicoSigla_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n913ContratoServicos_PrazoTipo ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00762_A155Servico_Codigo ;
      private int[] H00762_A160ContratoServicos_Codigo ;
      private String[] H00762_A605Servico_Sigla ;
      private int[] H00763_A155Servico_Codigo ;
      private int[] H00763_A160ContratoServicos_Codigo ;
      private String[] H00763_A605Servico_Sigla ;
      private int[] H00764_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] H00764_A160ContratoServicos_Codigo ;
      private short[] H00764_A2074ContratoServicos_CalculoRmn ;
      private bool[] H00764_n2074ContratoServicos_CalculoRmn ;
      private String[] H00764_A913ContratoServicos_PrazoTipo ;
      private bool[] H00764_n913ContratoServicos_PrazoTipo ;
      private int[] H00765_A917ContratoSrvVnc_Codigo ;
      private int[] H00765_A915ContratoSrvVnc_CntSrvCod ;
      private short[] H00766_A938ContratoServicosTelas_Sequencial ;
      private int[] H00766_A926ContratoServicosTelas_ContratoCod ;
      private int[] H00767_A1336ContratoServicosPrioridade_Codigo ;
      private int[] H00767_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] H00768_A1269ContratoServicosIndicador_Codigo ;
      private int[] H00768_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] H00769_A1473ContratoServicosCusto_Codigo ;
      private int[] H00769_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] H007610_A1749Artefatos_Codigo ;
      private int[] H007610_A160ContratoServicos_Codigo ;
      private int[] H007611_A160ContratoServicos_Codigo ;
      private short[] H007611_A2069ContratoServicosRmn_Sequencial ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
   }

   public class viewcontratoservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00762 ;
          prmH00762 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00763 ;
          prmH00763 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00764 ;
          prmH00764 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00765 ;
          prmH00765 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00766 ;
          prmH00766 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00767 ;
          prmH00767 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00768 ;
          prmH00768 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00769 ;
          prmH00769 = new Object[] {
          new Object[] {"@AV9ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH007610 ;
          prmH007610 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH007611 ;
          prmH007611 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00762", "SELECT T1.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV9ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00762,1,0,true,true )
             ,new CursorDef("H00763", "SELECT T1.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV9ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00763,1,0,false,true )
             ,new CursorDef("H00764", "SELECT T2.[ContratoServicosPrazo_CntSrvCod], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_CalculoRmn], COALESCE( T2.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo FROM ([ContratoServicos] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicosPrazo] T2 WITH (NOLOCK) ON T2.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV9ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00764,1,0,true,true )
             ,new CursorDef("H00765", "SELECT [ContratoSrvVnc_Codigo], [ContratoSrvVnc_CntSrvCod] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @AV9ContratoServicos_Codigo ORDER BY [ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00765,100,0,false,false )
             ,new CursorDef("H00766", "SELECT [ContratoServicosTelas_Sequencial], [ContratoServicosTelas_ContratoCod] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosTelas_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00766,100,0,false,false )
             ,new CursorDef("H00767", "SELECT [ContratoServicosPrioridade_Codigo], [ContratoServicosPrioridade_CntSrvCod] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00767,100,0,false,false )
             ,new CursorDef("H00768", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00768,100,0,false,false )
             ,new CursorDef("H00769", "SELECT [ContratoServicosCusto_Codigo], [ContratoServicosCusto_CntSrvCod] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_CntSrvCod] = @AV9ContratoServicos_Codigo ORDER BY [ContratoServicosCusto_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00769,100,0,false,false )
             ,new CursorDef("H007610", "SELECT [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007610,100,0,false,false )
             ,new CursorDef("H007611", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosRmn_Sequencial] > 0 ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007611,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
