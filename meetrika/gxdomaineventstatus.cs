/*
               File: EventStatus
        Description: EventStatus
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:38.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaineventstatus
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaineventstatus ()
      {
         domain[(short)1] = "Pending";
         domain[(short)2] = "Processing Server";
         domain[(short)3] = "Confirmed Server";
         domain[(short)4] = "Rejected Server";
         domain[(short)5] = "Canceled User";
         domain[(short)6] = "Rejected FK";
         domain[(short)7] = "Rejected Invalid BC";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
