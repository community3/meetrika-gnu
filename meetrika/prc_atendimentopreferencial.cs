/*
               File: PRC_AtendimentoPreferencial
        Description: Atendimento Preferencial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:17.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_atendimentopreferencial : GXProcedure
   {
      public prc_atendimentopreferencial( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_atendimentopreferencial( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           out bool aP1_Preferencial )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Preferencial = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Preferencial=this.AV8Preferencial;
      }

      public bool executeUdp( ref int aP0_Contratada_Codigo )
      {
         this.A39Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV8Preferencial = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Preferencial=this.AV8Preferencial;
         return AV8Preferencial ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 out bool aP1_Preferencial )
      {
         prc_atendimentopreferencial objprc_atendimentopreferencial;
         objprc_atendimentopreferencial = new prc_atendimentopreferencial();
         objprc_atendimentopreferencial.A39Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_atendimentopreferencial.AV8Preferencial = false ;
         objprc_atendimentopreferencial.context.SetSubmitInitialConfig(context);
         objprc_atendimentopreferencial.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_atendimentopreferencial);
         aP0_Contratada_Codigo=this.A39Contratada_Codigo;
         aP1_Preferencial=this.AV8Preferencial;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_atendimentopreferencial)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00TJ2 */
         pr_default.execute(0, new Object[] {A39Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1867Contratada_OSPreferencial = P00TJ2_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = P00TJ2_n1867Contratada_OSPreferencial[0];
            AV8Preferencial = A1867Contratada_OSPreferencial;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TJ2_A39Contratada_Codigo = new int[1] ;
         P00TJ2_A1867Contratada_OSPreferencial = new bool[] {false} ;
         P00TJ2_n1867Contratada_OSPreferencial = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_atendimentopreferencial__default(),
            new Object[][] {
                new Object[] {
               P00TJ2_A39Contratada_Codigo, P00TJ2_A1867Contratada_OSPreferencial, P00TJ2_n1867Contratada_OSPreferencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A39Contratada_Codigo ;
      private String scmdbuf ;
      private bool AV8Preferencial ;
      private bool A1867Contratada_OSPreferencial ;
      private bool n1867Contratada_OSPreferencial ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00TJ2_A39Contratada_Codigo ;
      private bool[] P00TJ2_A1867Contratada_OSPreferencial ;
      private bool[] P00TJ2_n1867Contratada_OSPreferencial ;
      private bool aP1_Preferencial ;
   }

   public class prc_atendimentopreferencial__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TJ2 ;
          prmP00TJ2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TJ2", "SELECT TOP 1 [Contratada_Codigo], [Contratada_OSPreferencial] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TJ2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
