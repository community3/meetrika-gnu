/*
               File: type_SdtSDT_CloneAtributo_Atributo
        Description: SDT_CloneAtributo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:58.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_CloneAtributo.Atributo" )]
   [XmlType(TypeName =  "SDT_CloneAtributo.Atributo" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_CloneAtributo_Atributo : GxUserType
   {
      public SdtSDT_CloneAtributo_Atributo( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_CloneAtributo_Atributo( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_CloneAtributo_Atributo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_CloneAtributo_Atributo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_CloneAtributo_Atributo obj ;
         obj = this;
         obj.gxTpr_Oldid = deserialized.gxTpr_Oldid;
         obj.gxTpr_Newid = deserialized.gxTpr_Newid;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "OldId") )
               {
                  gxTv_SdtSDT_CloneAtributo_Atributo_Oldid = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NewId") )
               {
                  gxTv_SdtSDT_CloneAtributo_Atributo_Newid = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_CloneAtributo.Atributo";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("OldId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CloneAtributo_Atributo_Oldid), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("NewId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CloneAtributo_Atributo_Newid), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("OldId", gxTv_SdtSDT_CloneAtributo_Atributo_Oldid, false);
         AddObjectProperty("NewId", gxTv_SdtSDT_CloneAtributo_Atributo_Newid, false);
         return  ;
      }

      [  SoapElement( ElementName = "OldId" )]
      [  XmlElement( ElementName = "OldId"   )]
      public int gxTpr_Oldid
      {
         get {
            return gxTv_SdtSDT_CloneAtributo_Atributo_Oldid ;
         }

         set {
            gxTv_SdtSDT_CloneAtributo_Atributo_Oldid = (int)(value);
         }

      }

      [  SoapElement( ElementName = "NewId" )]
      [  XmlElement( ElementName = "NewId"   )]
      public int gxTpr_Newid
      {
         get {
            return gxTv_SdtSDT_CloneAtributo_Atributo_Newid ;
         }

         set {
            gxTv_SdtSDT_CloneAtributo_Atributo_Newid = (int)(value);
         }

      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_CloneAtributo_Atributo_Oldid ;
      protected int gxTv_SdtSDT_CloneAtributo_Atributo_Newid ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_CloneAtributo.Atributo", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_CloneAtributo_Atributo_RESTInterface : GxGenericCollectionItem<SdtSDT_CloneAtributo_Atributo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_CloneAtributo_Atributo_RESTInterface( ) : base()
      {
      }

      public SdtSDT_CloneAtributo_Atributo_RESTInterface( SdtSDT_CloneAtributo_Atributo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "OldId" , Order = 0 )]
      public Nullable<int> gxTpr_Oldid
      {
         get {
            return sdt.gxTpr_Oldid ;
         }

         set {
            sdt.gxTpr_Oldid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NewId" , Order = 1 )]
      public Nullable<int> gxTpr_Newid
      {
         get {
            return sdt.gxTpr_Newid ;
         }

         set {
            sdt.gxTpr_Newid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_CloneAtributo_Atributo sdt
      {
         get {
            return (SdtSDT_CloneAtributo_Atributo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_CloneAtributo_Atributo() ;
         }
      }

   }

}
