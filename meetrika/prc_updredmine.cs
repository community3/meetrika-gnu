/*
               File: PRC_UpdRedmine
        Description: Update Redmine
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:10.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updredmine : GXProcedure
   {
      public prc_updredmine( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updredmine( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Redmine_ContratanteCod ,
                           String aP1_Redmine_User ,
                           short aP2_Redmine_Secure ,
                           String aP3_Redmine_Host ,
                           String aP4_Redmine_Url ,
                           String aP5_Redmine_Key ,
                           String aP6_Redmine_Versao ,
                           String aP7_Redmine_CampoSistema ,
                           String aP8_Redmine_CampoServico )
      {
         this.A1384Redmine_ContratanteCod = aP0_Redmine_ContratanteCod;
         this.AV13Redmine_User = aP1_Redmine_User;
         this.AV12Redmine_Secure = aP2_Redmine_Secure;
         this.AV8Redmine_Host = aP3_Redmine_Host;
         this.AV9Redmine_Url = aP4_Redmine_Url;
         this.AV10Redmine_Key = aP5_Redmine_Key;
         this.AV11Redmine_Versao = aP6_Redmine_Versao;
         this.AV15Redmine_CampoSistema = aP7_Redmine_CampoSistema;
         this.AV14Redmine_CampoServico = aP8_Redmine_CampoServico;
         initialize();
         executePrivate();
         aP0_Redmine_ContratanteCod=this.A1384Redmine_ContratanteCod;
      }

      public void executeSubmit( ref int aP0_Redmine_ContratanteCod ,
                                 String aP1_Redmine_User ,
                                 short aP2_Redmine_Secure ,
                                 String aP3_Redmine_Host ,
                                 String aP4_Redmine_Url ,
                                 String aP5_Redmine_Key ,
                                 String aP6_Redmine_Versao ,
                                 String aP7_Redmine_CampoSistema ,
                                 String aP8_Redmine_CampoServico )
      {
         prc_updredmine objprc_updredmine;
         objprc_updredmine = new prc_updredmine();
         objprc_updredmine.A1384Redmine_ContratanteCod = aP0_Redmine_ContratanteCod;
         objprc_updredmine.AV13Redmine_User = aP1_Redmine_User;
         objprc_updredmine.AV12Redmine_Secure = aP2_Redmine_Secure;
         objprc_updredmine.AV8Redmine_Host = aP3_Redmine_Host;
         objprc_updredmine.AV9Redmine_Url = aP4_Redmine_Url;
         objprc_updredmine.AV10Redmine_Key = aP5_Redmine_Key;
         objprc_updredmine.AV11Redmine_Versao = aP6_Redmine_Versao;
         objprc_updredmine.AV15Redmine_CampoSistema = aP7_Redmine_CampoSistema;
         objprc_updredmine.AV14Redmine_CampoServico = aP8_Redmine_CampoServico;
         objprc_updredmine.context.SetSubmitInitialConfig(context);
         objprc_updredmine.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updredmine);
         aP0_Redmine_ContratanteCod=this.A1384Redmine_ContratanteCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updredmine)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18GXLvl2 = 0;
         /* Using cursor P009E2 */
         pr_default.execute(0, new Object[] {A1384Redmine_ContratanteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1905Redmine_User = P009E2_A1905Redmine_User[0];
            n1905Redmine_User = P009E2_n1905Redmine_User[0];
            A1904Redmine_Secure = P009E2_A1904Redmine_Secure[0];
            A1381Redmine_Host = P009E2_A1381Redmine_Host[0];
            A1382Redmine_Url = P009E2_A1382Redmine_Url[0];
            A1383Redmine_Key = P009E2_A1383Redmine_Key[0];
            A1391Redmine_Versao = P009E2_A1391Redmine_Versao[0];
            n1391Redmine_Versao = P009E2_n1391Redmine_Versao[0];
            A1906Redmine_CampoSistema = P009E2_A1906Redmine_CampoSistema[0];
            n1906Redmine_CampoSistema = P009E2_n1906Redmine_CampoSistema[0];
            A1907Redmine_CampoServico = P009E2_A1907Redmine_CampoServico[0];
            n1907Redmine_CampoServico = P009E2_n1907Redmine_CampoServico[0];
            A1380Redmine_Codigo = P009E2_A1380Redmine_Codigo[0];
            AV18GXLvl2 = 1;
            A1905Redmine_User = AV13Redmine_User;
            n1905Redmine_User = false;
            A1904Redmine_Secure = AV12Redmine_Secure;
            A1381Redmine_Host = AV8Redmine_Host;
            A1382Redmine_Url = AV9Redmine_Url;
            A1383Redmine_Key = AV10Redmine_Key;
            A1391Redmine_Versao = AV11Redmine_Versao;
            n1391Redmine_Versao = false;
            A1906Redmine_CampoSistema = AV15Redmine_CampoSistema;
            n1906Redmine_CampoSistema = false;
            A1907Redmine_CampoServico = AV14Redmine_CampoServico;
            n1907Redmine_CampoServico = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P009E3 */
            pr_default.execute(1, new Object[] {n1905Redmine_User, A1905Redmine_User, A1904Redmine_Secure, A1381Redmine_Host, A1382Redmine_Url, A1383Redmine_Key, n1391Redmine_Versao, A1391Redmine_Versao, n1906Redmine_CampoSistema, A1906Redmine_CampoSistema, n1907Redmine_CampoServico, A1907Redmine_CampoServico, A1380Redmine_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
            if (true) break;
            /* Using cursor P009E4 */
            pr_default.execute(2, new Object[] {n1905Redmine_User, A1905Redmine_User, A1904Redmine_Secure, A1381Redmine_Host, A1382Redmine_Url, A1383Redmine_Key, n1391Redmine_Versao, A1391Redmine_Versao, n1906Redmine_CampoSistema, A1906Redmine_CampoSistema, n1907Redmine_CampoServico, A1907Redmine_CampoServico, A1380Redmine_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV18GXLvl2 == 0 )
         {
            /*
               INSERT RECORD ON TABLE Redmine

            */
            A1905Redmine_User = AV13Redmine_User;
            n1905Redmine_User = false;
            A1904Redmine_Secure = AV12Redmine_Secure;
            A1381Redmine_Host = AV8Redmine_Host;
            A1382Redmine_Url = AV9Redmine_Url;
            A1383Redmine_Key = AV10Redmine_Key;
            A1391Redmine_Versao = AV11Redmine_Versao;
            n1391Redmine_Versao = false;
            A1906Redmine_CampoSistema = AV15Redmine_CampoSistema;
            n1906Redmine_CampoSistema = false;
            A1907Redmine_CampoServico = AV14Redmine_CampoServico;
            n1907Redmine_CampoServico = false;
            /* Using cursor P009E5 */
            pr_default.execute(3, new Object[] {A1384Redmine_ContratanteCod, A1381Redmine_Host, A1382Redmine_Url, A1383Redmine_Key, n1391Redmine_Versao, A1391Redmine_Versao, A1904Redmine_Secure, n1905Redmine_User, A1905Redmine_User, n1906Redmine_CampoSistema, A1906Redmine_CampoSistema, n1907Redmine_CampoServico, A1907Redmine_CampoServico});
            A1380Redmine_Codigo = P009E5_A1380Redmine_Codigo[0];
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("Redmine") ;
            if ( (pr_default.getStatus(3) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdRedmine");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009E2_A1384Redmine_ContratanteCod = new int[1] ;
         P009E2_A1905Redmine_User = new String[] {""} ;
         P009E2_n1905Redmine_User = new bool[] {false} ;
         P009E2_A1904Redmine_Secure = new short[1] ;
         P009E2_A1381Redmine_Host = new String[] {""} ;
         P009E2_A1382Redmine_Url = new String[] {""} ;
         P009E2_A1383Redmine_Key = new String[] {""} ;
         P009E2_A1391Redmine_Versao = new String[] {""} ;
         P009E2_n1391Redmine_Versao = new bool[] {false} ;
         P009E2_A1906Redmine_CampoSistema = new String[] {""} ;
         P009E2_n1906Redmine_CampoSistema = new bool[] {false} ;
         P009E2_A1907Redmine_CampoServico = new String[] {""} ;
         P009E2_n1907Redmine_CampoServico = new bool[] {false} ;
         P009E2_A1380Redmine_Codigo = new int[1] ;
         A1905Redmine_User = "";
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         A1391Redmine_Versao = "";
         A1906Redmine_CampoSistema = "";
         A1907Redmine_CampoServico = "";
         P009E5_A1380Redmine_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updredmine__default(),
            new Object[][] {
                new Object[] {
               P009E2_A1384Redmine_ContratanteCod, P009E2_A1905Redmine_User, P009E2_n1905Redmine_User, P009E2_A1904Redmine_Secure, P009E2_A1381Redmine_Host, P009E2_A1382Redmine_Url, P009E2_A1383Redmine_Key, P009E2_A1391Redmine_Versao, P009E2_n1391Redmine_Versao, P009E2_A1906Redmine_CampoSistema,
               P009E2_n1906Redmine_CampoSistema, P009E2_A1907Redmine_CampoServico, P009E2_n1907Redmine_CampoServico, P009E2_A1380Redmine_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P009E5_A1380Redmine_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12Redmine_Secure ;
      private short AV18GXLvl2 ;
      private short A1904Redmine_Secure ;
      private int A1384Redmine_ContratanteCod ;
      private int A1380Redmine_Codigo ;
      private int GX_INS165 ;
      private String AV13Redmine_User ;
      private String AV8Redmine_Host ;
      private String AV9Redmine_Url ;
      private String AV10Redmine_Key ;
      private String AV11Redmine_Versao ;
      private String AV15Redmine_CampoSistema ;
      private String AV14Redmine_CampoServico ;
      private String scmdbuf ;
      private String A1905Redmine_User ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String A1391Redmine_Versao ;
      private String A1906Redmine_CampoSistema ;
      private String A1907Redmine_CampoServico ;
      private String Gx_emsg ;
      private bool n1905Redmine_User ;
      private bool n1391Redmine_Versao ;
      private bool n1906Redmine_CampoSistema ;
      private bool n1907Redmine_CampoServico ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Redmine_ContratanteCod ;
      private IDataStoreProvider pr_default ;
      private int[] P009E2_A1384Redmine_ContratanteCod ;
      private String[] P009E2_A1905Redmine_User ;
      private bool[] P009E2_n1905Redmine_User ;
      private short[] P009E2_A1904Redmine_Secure ;
      private String[] P009E2_A1381Redmine_Host ;
      private String[] P009E2_A1382Redmine_Url ;
      private String[] P009E2_A1383Redmine_Key ;
      private String[] P009E2_A1391Redmine_Versao ;
      private bool[] P009E2_n1391Redmine_Versao ;
      private String[] P009E2_A1906Redmine_CampoSistema ;
      private bool[] P009E2_n1906Redmine_CampoSistema ;
      private String[] P009E2_A1907Redmine_CampoServico ;
      private bool[] P009E2_n1907Redmine_CampoServico ;
      private int[] P009E2_A1380Redmine_Codigo ;
      private int[] P009E5_A1380Redmine_Codigo ;
   }

   public class prc_updredmine__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009E2 ;
          prmP009E2 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009E3 ;
          prmP009E3 = new Object[] {
          new Object[] {"@Redmine_User",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Secure",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Redmine_Host",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Url",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Key",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Versao",SqlDbType.Char,20,0} ,
          new Object[] {"@Redmine_CampoSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoServico",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009E4 ;
          prmP009E4 = new Object[] {
          new Object[] {"@Redmine_User",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Secure",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Redmine_Host",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Url",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Key",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Versao",SqlDbType.Char,20,0} ,
          new Object[] {"@Redmine_CampoSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoServico",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009E5 ;
          prmP009E5 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Redmine_Host",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Url",SqlDbType.Char,100,0} ,
          new Object[] {"@Redmine_Key",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_Versao",SqlDbType.Char,20,0} ,
          new Object[] {"@Redmine_Secure",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Redmine_User",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@Redmine_CampoServico",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009E2", "SELECT TOP 1 [Redmine_ContratanteCod], [Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico], [Redmine_Codigo] FROM [Redmine] WITH (UPDLOCK) WHERE [Redmine_ContratanteCod] = @Redmine_ContratanteCod ORDER BY [Redmine_ContratanteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009E2,1,0,true,true )
             ,new CursorDef("P009E3", "UPDATE [Redmine] SET [Redmine_User]=@Redmine_User, [Redmine_Secure]=@Redmine_Secure, [Redmine_Host]=@Redmine_Host, [Redmine_Url]=@Redmine_Url, [Redmine_Key]=@Redmine_Key, [Redmine_Versao]=@Redmine_Versao, [Redmine_CampoSistema]=@Redmine_CampoSistema, [Redmine_CampoServico]=@Redmine_CampoServico  WHERE [Redmine_Codigo] = @Redmine_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009E3)
             ,new CursorDef("P009E4", "UPDATE [Redmine] SET [Redmine_User]=@Redmine_User, [Redmine_Secure]=@Redmine_Secure, [Redmine_Host]=@Redmine_Host, [Redmine_Url]=@Redmine_Url, [Redmine_Key]=@Redmine_Key, [Redmine_Versao]=@Redmine_Versao, [Redmine_CampoSistema]=@Redmine_CampoSistema, [Redmine_CampoServico]=@Redmine_CampoServico  WHERE [Redmine_Codigo] = @Redmine_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009E4)
             ,new CursorDef("P009E5", "INSERT INTO [Redmine]([Redmine_ContratanteCod], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_Secure], [Redmine_User], [Redmine_CampoSistema], [Redmine_CampoServico]) VALUES(@Redmine_ContratanteCod, @Redmine_Host, @Redmine_Url, @Redmine_Key, @Redmine_Versao, @Redmine_Secure, @Redmine_User, @Redmine_CampoSistema, @Redmine_CampoServico); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP009E5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                stmt.SetParameter(9, (int)parms[12]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                stmt.SetParameter(9, (int)parms[12]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (short)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[12]);
                }
                return;
       }
    }

 }

}
