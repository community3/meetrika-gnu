/*
               File: GetGeral_Grupo_FuncaoGeral_FuncaoWCFilterData
        Description: Get Geral_Grupo_Funcao Geral_Funcao WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:13.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getgeral_grupo_funcaogeral_funcaowcfilterdata : GXProcedure
   {
      public getgeral_grupo_funcaogeral_funcaowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getgeral_grupo_funcaogeral_funcaowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getgeral_grupo_funcaogeral_funcaowcfilterdata objgetgeral_grupo_funcaogeral_funcaowcfilterdata;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata = new getgeral_grupo_funcaogeral_funcaowcfilterdata();
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV16DDOName = aP0_DDOName;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV20OptionsJson = "" ;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV23OptionsDescJson = "" ;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.AV25OptionIndexesJson = "" ;
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetgeral_grupo_funcaogeral_funcaowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetgeral_grupo_funcaogeral_funcaowcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getgeral_grupo_funcaogeral_funcaowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_FUNCAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_FUNCAO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("Geral_Grupo_FuncaoGeral_FuncaoWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Geral_Grupo_FuncaoGeral_FuncaoWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("Geral_Grupo_FuncaoGeral_FuncaoWCGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "FUNCAO_ATIVO") == 0 )
            {
               AV32Funcao_Ativo = BooleanUtil.Val( AV30GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME") == 0 )
            {
               AV10TFFuncao_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME_SEL") == 0 )
            {
               AV11TFFuncao_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM") == 0 )
            {
               AV12TFFuncao_UONom = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM_SEL") == 0 )
            {
               AV13TFFuncao_UONom_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&GRUPOFUNCAO_CODIGO") == 0 )
            {
               AV44GrupoFuncao_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_NOME") == 0 )
            {
               AV34Funcao_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 )
            {
               AV35Funcao_UONom1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_NOME") == 0 )
               {
                  AV38Funcao_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 )
               {
                  AV39Funcao_UONom2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_NOME") == 0 )
                  {
                     AV42Funcao_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 )
                  {
                     AV43Funcao_UONom3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAO_NOMEOPTIONS' Routine */
         AV10TFFuncao_Nome = AV14SearchTxt;
         AV11TFFuncao_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Funcao_Nome1 ,
                                              AV35Funcao_UONom1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Funcao_Nome2 ,
                                              AV39Funcao_UONom2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Funcao_Nome3 ,
                                              AV43Funcao_UONom3 ,
                                              AV11TFFuncao_Nome_Sel ,
                                              AV10TFFuncao_Nome ,
                                              AV13TFFuncao_UONom_Sel ,
                                              AV12TFFuncao_UONom ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A630Funcao_Ativo ,
                                              AV44GrupoFuncao_Codigo ,
                                              A619GrupoFuncao_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV34Funcao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Funcao_Nome1), "%", "");
         lV35Funcao_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV35Funcao_UONom1), 50, "%");
         lV38Funcao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Funcao_Nome2), "%", "");
         lV39Funcao_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV39Funcao_UONom2), 50, "%");
         lV42Funcao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Funcao_Nome3), "%", "");
         lV43Funcao_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV43Funcao_UONom3), 50, "%");
         lV10TFFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncao_Nome), "%", "");
         lV12TFFuncao_UONom = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncao_UONom), 50, "%");
         /* Using cursor P00N72 */
         pr_default.execute(0, new Object[] {AV44GrupoFuncao_Codigo, lV34Funcao_Nome1, lV35Funcao_UONom1, lV38Funcao_Nome2, lV39Funcao_UONom2, lV42Funcao_Nome3, lV43Funcao_UONom3, lV10TFFuncao_Nome, AV11TFFuncao_Nome_Sel, lV12TFFuncao_UONom, AV13TFFuncao_UONom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKN72 = false;
            A635Funcao_UOCod = P00N72_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00N72_n635Funcao_UOCod[0];
            A619GrupoFuncao_Codigo = P00N72_A619GrupoFuncao_Codigo[0];
            A630Funcao_Ativo = P00N72_A630Funcao_Ativo[0];
            A622Funcao_Nome = P00N72_A622Funcao_Nome[0];
            A636Funcao_UONom = P00N72_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N72_n636Funcao_UONom[0];
            A621Funcao_Codigo = P00N72_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00N72_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N72_n636Funcao_UONom[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00N72_A619GrupoFuncao_Codigo[0] == A619GrupoFuncao_Codigo ) && ( StringUtil.StrCmp(P00N72_A622Funcao_Nome[0], A622Funcao_Nome) == 0 ) )
            {
               BRKN72 = false;
               A621Funcao_Codigo = P00N72_A621Funcao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKN72 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A622Funcao_Nome)) )
            {
               AV18Option = A622Funcao_Nome;
               AV21OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")));
               AV19Options.Add(AV18Option, 0);
               AV22OptionsDesc.Add(AV21OptionDesc, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKN72 )
            {
               BRKN72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAO_UONOMOPTIONS' Routine */
         AV12TFFuncao_UONom = AV14SearchTxt;
         AV13TFFuncao_UONom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Funcao_Nome1 ,
                                              AV35Funcao_UONom1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Funcao_Nome2 ,
                                              AV39Funcao_UONom2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Funcao_Nome3 ,
                                              AV43Funcao_UONom3 ,
                                              AV11TFFuncao_Nome_Sel ,
                                              AV10TFFuncao_Nome ,
                                              AV13TFFuncao_UONom_Sel ,
                                              AV12TFFuncao_UONom ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A630Funcao_Ativo ,
                                              AV44GrupoFuncao_Codigo ,
                                              A619GrupoFuncao_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV34Funcao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Funcao_Nome1), "%", "");
         lV35Funcao_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV35Funcao_UONom1), 50, "%");
         lV38Funcao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Funcao_Nome2), "%", "");
         lV39Funcao_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV39Funcao_UONom2), 50, "%");
         lV42Funcao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Funcao_Nome3), "%", "");
         lV43Funcao_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV43Funcao_UONom3), 50, "%");
         lV10TFFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncao_Nome), "%", "");
         lV12TFFuncao_UONom = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncao_UONom), 50, "%");
         /* Using cursor P00N73 */
         pr_default.execute(1, new Object[] {AV44GrupoFuncao_Codigo, lV34Funcao_Nome1, lV35Funcao_UONom1, lV38Funcao_Nome2, lV39Funcao_UONom2, lV42Funcao_Nome3, lV43Funcao_UONom3, lV10TFFuncao_Nome, AV11TFFuncao_Nome_Sel, lV12TFFuncao_UONom, AV13TFFuncao_UONom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKN74 = false;
            A619GrupoFuncao_Codigo = P00N73_A619GrupoFuncao_Codigo[0];
            A630Funcao_Ativo = P00N73_A630Funcao_Ativo[0];
            A635Funcao_UOCod = P00N73_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00N73_n635Funcao_UOCod[0];
            A636Funcao_UONom = P00N73_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N73_n636Funcao_UONom[0];
            A622Funcao_Nome = P00N73_A622Funcao_Nome[0];
            A621Funcao_Codigo = P00N73_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00N73_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N73_n636Funcao_UONom[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00N73_A619GrupoFuncao_Codigo[0] == A619GrupoFuncao_Codigo ) && ( P00N73_A635Funcao_UOCod[0] == A635Funcao_UOCod ) )
            {
               BRKN74 = false;
               A621Funcao_Codigo = P00N73_A621Funcao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKN74 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A636Funcao_UONom)) )
            {
               AV18Option = A636Funcao_UONom;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV19Options.Item(AV17InsertIndex)), AV18Option) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKN74 )
            {
               BRKN74 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV32Funcao_Ativo = true;
         AV10TFFuncao_Nome = "";
         AV11TFFuncao_Nome_Sel = "";
         AV12TFFuncao_UONom = "";
         AV13TFFuncao_UONom_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Funcao_Nome1 = "";
         AV35Funcao_UONom1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Funcao_Nome2 = "";
         AV39Funcao_UONom2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42Funcao_Nome3 = "";
         AV43Funcao_UONom3 = "";
         scmdbuf = "";
         lV34Funcao_Nome1 = "";
         lV35Funcao_UONom1 = "";
         lV38Funcao_Nome2 = "";
         lV39Funcao_UONom2 = "";
         lV42Funcao_Nome3 = "";
         lV43Funcao_UONom3 = "";
         lV10TFFuncao_Nome = "";
         lV12TFFuncao_UONom = "";
         A622Funcao_Nome = "";
         A636Funcao_UONom = "";
         P00N72_A635Funcao_UOCod = new int[1] ;
         P00N72_n635Funcao_UOCod = new bool[] {false} ;
         P00N72_A619GrupoFuncao_Codigo = new int[1] ;
         P00N72_A630Funcao_Ativo = new bool[] {false} ;
         P00N72_A622Funcao_Nome = new String[] {""} ;
         P00N72_A636Funcao_UONom = new String[] {""} ;
         P00N72_n636Funcao_UONom = new bool[] {false} ;
         P00N72_A621Funcao_Codigo = new int[1] ;
         AV18Option = "";
         AV21OptionDesc = "";
         P00N73_A619GrupoFuncao_Codigo = new int[1] ;
         P00N73_A630Funcao_Ativo = new bool[] {false} ;
         P00N73_A635Funcao_UOCod = new int[1] ;
         P00N73_n635Funcao_UOCod = new bool[] {false} ;
         P00N73_A636Funcao_UONom = new String[] {""} ;
         P00N73_n636Funcao_UONom = new bool[] {false} ;
         P00N73_A622Funcao_Nome = new String[] {""} ;
         P00N73_A621Funcao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getgeral_grupo_funcaogeral_funcaowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00N72_A635Funcao_UOCod, P00N72_n635Funcao_UOCod, P00N72_A619GrupoFuncao_Codigo, P00N72_A630Funcao_Ativo, P00N72_A622Funcao_Nome, P00N72_A636Funcao_UONom, P00N72_n636Funcao_UONom, P00N72_A621Funcao_Codigo
               }
               , new Object[] {
               P00N73_A619GrupoFuncao_Codigo, P00N73_A630Funcao_Ativo, P00N73_A635Funcao_UOCod, P00N73_n635Funcao_UOCod, P00N73_A636Funcao_UONom, P00N73_n636Funcao_UONom, P00N73_A622Funcao_Nome, P00N73_A621Funcao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV47GXV1 ;
      private int AV44GrupoFuncao_Codigo ;
      private int A619GrupoFuncao_Codigo ;
      private int A635Funcao_UOCod ;
      private int A621Funcao_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV12TFFuncao_UONom ;
      private String AV13TFFuncao_UONom_Sel ;
      private String AV35Funcao_UONom1 ;
      private String AV39Funcao_UONom2 ;
      private String AV43Funcao_UONom3 ;
      private String scmdbuf ;
      private String lV35Funcao_UONom1 ;
      private String lV39Funcao_UONom2 ;
      private String lV43Funcao_UONom3 ;
      private String lV12TFFuncao_UONom ;
      private String A636Funcao_UONom ;
      private bool returnInSub ;
      private bool AV32Funcao_Ativo ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool A630Funcao_Ativo ;
      private bool BRKN72 ;
      private bool n635Funcao_UOCod ;
      private bool n636Funcao_UONom ;
      private bool BRKN74 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFFuncao_Nome ;
      private String AV11TFFuncao_Nome_Sel ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV34Funcao_Nome1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV38Funcao_Nome2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV42Funcao_Nome3 ;
      private String lV34Funcao_Nome1 ;
      private String lV38Funcao_Nome2 ;
      private String lV42Funcao_Nome3 ;
      private String lV10TFFuncao_Nome ;
      private String A622Funcao_Nome ;
      private String AV18Option ;
      private String AV21OptionDesc ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00N72_A635Funcao_UOCod ;
      private bool[] P00N72_n635Funcao_UOCod ;
      private int[] P00N72_A619GrupoFuncao_Codigo ;
      private bool[] P00N72_A630Funcao_Ativo ;
      private String[] P00N72_A622Funcao_Nome ;
      private String[] P00N72_A636Funcao_UONom ;
      private bool[] P00N72_n636Funcao_UONom ;
      private int[] P00N72_A621Funcao_Codigo ;
      private int[] P00N73_A619GrupoFuncao_Codigo ;
      private bool[] P00N73_A630Funcao_Ativo ;
      private int[] P00N73_A635Funcao_UOCod ;
      private bool[] P00N73_n635Funcao_UOCod ;
      private String[] P00N73_A636Funcao_UONom ;
      private bool[] P00N73_n636Funcao_UONom ;
      private String[] P00N73_A622Funcao_Nome ;
      private int[] P00N73_A621Funcao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getgeral_grupo_funcaogeral_funcaowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00N72( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Funcao_Nome1 ,
                                             String AV35Funcao_UONom1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Funcao_Nome2 ,
                                             String AV39Funcao_UONom2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Funcao_Nome3 ,
                                             String AV43Funcao_UONom3 ,
                                             String AV11TFFuncao_Nome_Sel ,
                                             String AV10TFFuncao_Nome ,
                                             String AV13TFFuncao_UONom_Sel ,
                                             String AV12TFFuncao_UONom ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             bool A630Funcao_Ativo ,
                                             int AV44GrupoFuncao_Codigo ,
                                             int A619GrupoFuncao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[GrupoFuncao_Codigo], T1.[Funcao_Ativo], T1.[Funcao_Nome], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[GrupoFuncao_Codigo] = @AV44GrupoFuncao_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Funcao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Funcao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV34Funcao_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Funcao_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV35Funcao_UONom1 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Funcao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV38Funcao_Nome2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Funcao_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Funcao_UONom2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Funcao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV42Funcao_Nome3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Funcao_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Funcao_UONom3 + '%')";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV10TFFuncao_Nome)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV11TFFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncao_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncao_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV12TFFuncao_UONom)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncao_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV13TFFuncao_UONom_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[GrupoFuncao_Codigo], T1.[Funcao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00N73( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Funcao_Nome1 ,
                                             String AV35Funcao_UONom1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Funcao_Nome2 ,
                                             String AV39Funcao_UONom2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Funcao_Nome3 ,
                                             String AV43Funcao_UONom3 ,
                                             String AV11TFFuncao_Nome_Sel ,
                                             String AV10TFFuncao_Nome ,
                                             String AV13TFFuncao_UONom_Sel ,
                                             String AV12TFFuncao_UONom ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             bool A630Funcao_Ativo ,
                                             int AV44GrupoFuncao_Codigo ,
                                             int A619GrupoFuncao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[GrupoFuncao_Codigo], T1.[Funcao_Ativo], T1.[Funcao_UOCod] AS Funcao_UOCod, T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Nome], T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[GrupoFuncao_Codigo] = @AV44GrupoFuncao_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Funcao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Funcao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV34Funcao_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Funcao_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV35Funcao_UONom1 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Funcao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV38Funcao_Nome2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Funcao_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Funcao_UONom2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Funcao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV42Funcao_Nome3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Funcao_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Funcao_UONom3 + '%')";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV10TFFuncao_Nome)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV11TFFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncao_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncao_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV12TFFuncao_UONom)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncao_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV13TFFuncao_UONom_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[GrupoFuncao_Codigo], T1.[Funcao_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00N72(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00N73(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00N72 ;
          prmP00N72 = new Object[] {
          new Object[] {"@AV44GrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Funcao_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV35Funcao_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Funcao_Nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV39Funcao_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Funcao_Nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV43Funcao_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFFuncao_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFFuncao_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncao_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00N73 ;
          prmP00N73 = new Object[] {
          new Object[] {"@AV44GrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Funcao_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV35Funcao_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Funcao_Nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV39Funcao_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Funcao_Nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV43Funcao_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFFuncao_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV12TFFuncao_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncao_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00N72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00N72,100,0,true,false )
             ,new CursorDef("P00N73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00N73,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getgeral_grupo_funcaogeral_funcaowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getgeral_grupo_funcaogeral_funcaowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getgeral_grupo_funcaogeral_funcaowcfilterdata") )
          {
             return  ;
          }
          getgeral_grupo_funcaogeral_funcaowcfilterdata worker = new getgeral_grupo_funcaogeral_funcaowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
