/*
               File: WP_SistemaDePara
        Description: Sistemas (De Para)
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:0:41.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_sistemadepara : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_sistemadepara( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_sistemadepara( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Origem ,
                           int aP1_AreaTrabalho_Codigo )
      {
         this.AV5Origem = aP0_Origem;
         this.AV21AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavProjeto = new GXCombobox();
         cmbavSistema_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5Origem = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Origem", AV5Origem);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Origem, ""))));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV21AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21AreaTrabalho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV21AreaTrabalho_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKX2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKX2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311904113");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_sistemadepara.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV5Origem)) + "," + UrlEncode("" +AV21AreaTrabalho_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_ORIGEM", StringUtil.RTrim( A1461SistemaDePara_Origem));
         GxWebStd.gx_hidden_field( context, "vORIGEM", StringUtil.RTrim( AV5Origem));
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_ORIGEMDSC", A1463SistemaDePara_OrigemDsc);
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1460SistemaDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_ORIGEMID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1462SistemaDePara_OrigemId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMADEPARA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18SistemaDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vORIGEMDSC", AV20OrigemDsc);
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXC1", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40000GXC1), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Origem, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV21AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Origem, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV21AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Width", StringUtil.RTrim( Desvinculapanel_Width));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Title", StringUtil.RTrim( Desvinculapanel_Title));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Confirmtext", StringUtil.RTrim( Desvinculapanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttonyestext", StringUtil.RTrim( Desvinculapanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttonnotext", StringUtil.RTrim( Desvinculapanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttoncanceltext", StringUtil.RTrim( Desvinculapanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Confirmtype", StringUtil.RTrim( Desvinculapanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GxWebStd.gx_hidden_field( context, "vPROJETO_Text", StringUtil.RTrim( cmbavProjeto.Description));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Result", StringUtil.RTrim( Desvinculapanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKX2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKX2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_sistemadepara.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV5Origem)) + "," + UrlEncode("" +AV21AreaTrabalho_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_SistemaDePara" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistemas (De Para)" ;
      }

      protected void WBKX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table1_4_KX2( true) ;
         }
         else
         {
            wb_table1_4_KX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_KX2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_25_KX2( true) ;
         }
         else
         {
            wb_table2_25_KX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_25_KX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_SistemaDePara.htm");
         }
         wbLoad = true;
      }

      protected void STARTKX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Sistemas (De Para)", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKX0( ) ;
      }

      protected void WSKX2( )
      {
         STARTKX2( ) ;
         EVTKX2( ) ;
      }

      protected void EVTKX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KX2 */
                              E11KX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DESVINCULAPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KX2 */
                              E12KX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KX2 */
                              E13KX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E14KX2 */
                                    E14KX2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KX2 */
                              E15KX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16KX2 */
                              E16KX2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavProjeto.Name = "vPROJETO";
            cmbavProjeto.WebTags = "";
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 18, 0)), "(Selecionar)", 0);
            if ( cmbavProjeto.ItemCount > 0 )
            {
               AV8Projeto = (long)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)));
            }
            cmbavSistema_codigo.Name = "vSISTEMA_CODIGO";
            cmbavSistema_codigo.WebTags = "";
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Selecionar)", 0);
            if ( cmbavSistema_codigo.ItemCount > 0 )
            {
               AV10Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavProjeto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavProjeto.ItemCount > 0 )
         {
            AV8Projeto = (long)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)));
         }
         if ( cmbavSistema_codigo.ItemCount > 0 )
         {
            AV10Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFKX2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16KX2 */
            E16KX2 ();
            WBKX0( ) ;
         }
      }

      protected void STRUPKX0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00KX3 */
         pr_default.execute(0, new Object[] {AV10Sistema_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = H00KX3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13KX2 */
         E13KX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavProjeto.CurrentValue = cgiGet( cmbavProjeto_Internalname);
            AV8Projeto = (long)(NumberUtil.Val( cgiGet( cmbavProjeto_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)));
            cmbavSistema_codigo.CurrentValue = cgiGet( cmbavSistema_codigo_Internalname);
            AV10Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)));
            /* Read saved values. */
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Desvinculapanel_Width = cgiGet( "DESVINCULAPANEL_Width");
            Desvinculapanel_Title = cgiGet( "DESVINCULAPANEL_Title");
            Desvinculapanel_Confirmtext = cgiGet( "DESVINCULAPANEL_Confirmtext");
            Desvinculapanel_Buttonyestext = cgiGet( "DESVINCULAPANEL_Buttonyestext");
            Desvinculapanel_Buttonnotext = cgiGet( "DESVINCULAPANEL_Buttonnotext");
            Desvinculapanel_Buttoncanceltext = cgiGet( "DESVINCULAPANEL_Buttoncanceltext");
            Desvinculapanel_Confirmtype = cgiGet( "DESVINCULAPANEL_Confirmtype");
            Confirmpanel_Result = cgiGet( "CONFIRMPANEL_Result");
            Desvinculapanel_Result = cgiGet( "DESVINCULAPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13KX2 */
         E13KX2 ();
         if (returnInSub) return;
      }

      protected void E13KX2( )
      {
         /* Start Routine */
         AV6ProjetoSemDePara.FromXml(AV7WebSession.Get("Projetos"), "SDT_Redmineuser.memberships.membershipCollection");
         AV24GXV1 = 1;
         while ( AV24GXV1 <= AV6ProjetoSemDePara.Count )
         {
            AV16ProjectItem = ((SdtSDT_Redmineuser_memberships_membership)AV6ProjetoSemDePara.Item(AV24GXV1));
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ProjectItem.gxTpr_Project.gxTpr_Id), 18, 0)), AV16ProjectItem.gxTpr_Project.gxTpr_Name, 0);
            AV24GXV1 = (int)(AV24GXV1+1);
         }
         /* Using cursor H00KX4 */
         pr_default.execute(1, new Object[] {AV21AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A130Sistema_Ativo = H00KX4_A130Sistema_Ativo[0];
            A135Sistema_AreaTrabalhoCod = H00KX4_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = H00KX4_A127Sistema_Codigo[0];
            A129Sistema_Sigla = H00KX4_A129Sistema_Sigla[0];
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)), A129Sistema_Sigla, 0);
            AV10Sistema_Codigo = A127Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)));
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( cmbavSistema_codigo.ItemCount > 2 )
         {
            AV10Sistema_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)));
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      public void GXEnter( )
      {
         /* Execute user event: E14KX2 */
         E14KX2 ();
         if (returnInSub) return;
      }

      protected void E14KX2( )
      {
         /* Enter Routine */
         if ( (0==AV8Projeto) )
         {
            GX_msglist.addItem("Selecione o Projeto a vincular!");
         }
         else if ( (0==AV10Sistema_Codigo) )
         {
            GX_msglist.addItem("Selecione o Sistema a ser vinculado!");
         }
         else
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void E11KX2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            AV19i = 0;
            /* Using cursor H00KX5 */
            pr_default.execute(2, new Object[] {AV10Sistema_Codigo, AV5Origem});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1461SistemaDePara_Origem = H00KX5_A1461SistemaDePara_Origem[0];
               A127Sistema_Codigo = H00KX5_A127Sistema_Codigo[0];
               A1463SistemaDePara_OrigemDsc = H00KX5_A1463SistemaDePara_OrigemDsc[0];
               n1463SistemaDePara_OrigemDsc = H00KX5_n1463SistemaDePara_OrigemDsc[0];
               A1460SistemaDePara_Codigo = H00KX5_A1460SistemaDePara_Codigo[0];
               A1462SistemaDePara_OrigemId = H00KX5_A1462SistemaDePara_OrigemId[0];
               Desvinculapanel_Confirmtext = "Sistema j� vinculado com "+StringUtil.Trim( A1463SistemaDePara_OrigemDsc);
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
               AV18SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SistemaDePara_Codigo), 6, 0)));
               AV8Projeto = A1462SistemaDePara_OrigemId;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)));
               AV20OrigemDsc = A1463SistemaDePara_OrigemDsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20OrigemDsc", AV20OrigemDsc);
               AV19i = (short)(AV19i+1);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( (0==AV19i) )
            {
               /* Execute user subroutine: 'VINCULAR' */
               S112 ();
               if (returnInSub) return;
            }
            else
            {
               if ( AV19i > 1 )
               {
                  Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+" e outros!";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
               }
               else
               {
                  Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+"!";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
               }
               this.executeUsercontrolMethod("", false, "DESVINCULAPANELContainer", "Confirm", "", new Object[] {});
            }
         }
         cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", cmbavProjeto.ToJavascriptSource());
      }

      protected void E12KX2( )
      {
         /* Desvinculapanel_Close Routine */
         if ( StringUtil.StrCmp(Desvinculapanel_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'VINCULAR' */
            S112 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(Desvinculapanel_Result, "Cancel") == 0 )
         {
            AV17SistemaDePara.Load(AV10Sistema_Codigo, AV18SistemaDePara_Codigo);
            AV17SistemaDePara.Delete();
            if ( AV17SistemaDePara.Success() )
            {
               context.CommitDataStores( "WP_SistemaDePara");
               cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)), AV20OrigemDsc, 0);
            }
         }
         cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", cmbavProjeto.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'VINCULAR' Routine */
         /* Using cursor H00KX7 */
         pr_default.execute(3, new Object[] {AV10Sistema_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40000GXC1 = H00KX7_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(3);
         AV17SistemaDePara = new SdtSistemaDePara(context);
         AV17SistemaDePara.gxTpr_Sistema_codigo = AV10Sistema_Codigo;
         AV17SistemaDePara.gxTpr_Sistemadepara_codigo = (int)(A40000GXC1+1);
         AV17SistemaDePara.gxTpr_Sistemadepara_origem = AV5Origem;
         AV17SistemaDePara.gxTpr_Sistemadepara_origemid = AV8Projeto;
         AV17SistemaDePara.gxTpr_Sistemadepara_origemdsc = cmbavProjeto.Description;
         AV17SistemaDePara.Save();
         if ( AV17SistemaDePara.Success() )
         {
            context.CommitDataStores( "WP_SistemaDePara");
            cmbavProjeto.removeItem(StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)));
            if ( cmbavProjeto.ItemCount == 1 )
            {
               /* Execute user subroutine: 'DOFECHAR' */
               S122 ();
               if (returnInSub) return;
            }
         }
      }

      protected void E15KX2( )
      {
         /* 'Fechar' Routine */
         /* Execute user subroutine: 'DOFECHAR' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'DOFECHAR' Routine */
         AV7WebSession.Remove("Projetos");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E16KX2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_25_KX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DESVINCULAPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_25_KX2e( true) ;
         }
         else
         {
            wb_table2_25_KX2e( false) ;
         }
      }

      protected void wb_table1_4_KX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_7_KX2( true) ;
         }
         else
         {
            wb_table3_7_KX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_7_KX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_KX2e( true) ;
         }
         else
         {
            wb_table1_4_KX2e( false) ;
         }
      }

      protected void wb_table3_7_KX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 10, 4, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "De Projeto", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto, cmbavProjeto_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0)), 1, cmbavProjeto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_WP_SistemaDePara.htm");
            cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Projeto), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", (String)(cmbavProjeto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Para Sistema", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_codigo, cmbavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0)), 1, cmbavSistema_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WP_SistemaDePara.htm");
            cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", (String)(cmbavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:50px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Vincular", bttButton1_Jsonclick, 5, "Vincular", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SistemaDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'FECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_7_KX2e( true) ;
         }
         else
         {
            wb_table3_7_KX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5Origem = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Origem", AV5Origem);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5Origem, ""))));
         AV21AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV21AreaTrabalho_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKX2( ) ;
         WSKX2( ) ;
         WEKX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311904183");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_sistemadepara.js", "?2020311904183");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavProjeto_Internalname = "vPROJETO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         cmbavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         tblTable2_Internalname = "TABLE2";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         Desvinculapanel_Internalname = "DESVINCULAPANEL";
         tblTable3_Internalname = "TABLE3";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavSistema_codigo_Jsonclick = "";
         cmbavProjeto_Jsonclick = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         cmbavProjeto.Description = "";
         Desvinculapanel_Confirmtype = "2";
         Desvinculapanel_Buttoncanceltext = "Eliminar";
         Desvinculapanel_Buttonnotext = "Voltar";
         Desvinculapanel_Buttonyestext = "Vincular";
         Desvinculapanel_Confirmtext = "Do you want to continue?";
         Desvinculapanel_Title = "Aviso";
         Confirmpanel_Confirmtype = "1";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Sim";
         Confirmpanel_Confirmtext = "Confirma o vinculo do Projeto com o Sistema?";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Height = "50";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Sistemas (De Para)";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E14KX2',iparms:[{av:'AV8Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV10Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11KX2',iparms:[{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1461SistemaDePara_Origem',fld:'SISTEMADEPARA_ORIGEM',pic:'',nv:''},{av:'AV5Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{av:'A1463SistemaDePara_OrigemDsc',fld:'SISTEMADEPARA_ORIGEMDSC',pic:'',nv:''},{av:'A1460SistemaDePara_Codigo',fld:'SISTEMADEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1462SistemaDePara_OrigemId',fld:'SISTEMADEPARA_ORIGEMID',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV8Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'cmbavProjeto'}],oparms:[{av:'Desvinculapanel_Confirmtext',ctrl:'DESVINCULAPANEL',prop:'ConfirmText'},{av:'AV18SistemaDePara_Codigo',fld:'vSISTEMADEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV20OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''}]}");
         setEventMetadata("DESVINCULAPANEL.CLOSE","{handler:'E12KX2',iparms:[{av:'Desvinculapanel_Result',ctrl:'DESVINCULAPANEL',prop:'Result'},{av:'AV10Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18SistemaDePara_Codigo',fld:'vSISTEMADEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV20OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''},{av:'AV5Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{av:'cmbavProjeto'}],oparms:[{av:'AV8Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'FECHAR'","{handler:'E15KX2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV5Origem = "";
         Confirmpanel_Result = "";
         Desvinculapanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1461SistemaDePara_Origem = "";
         A1463SistemaDePara_OrigemDsc = "";
         AV20OrigemDsc = "";
         Confirmpanel_Width = "";
         Desvinculapanel_Width = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00KX3_A40000GXC1 = new int[1] ;
         AV6ProjetoSemDePara = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
         AV7WebSession = context.GetSession();
         AV16ProjectItem = new SdtSDT_Redmineuser_memberships_membership(context);
         H00KX4_A130Sistema_Ativo = new bool[] {false} ;
         H00KX4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00KX4_A127Sistema_Codigo = new int[1] ;
         H00KX4_A129Sistema_Sigla = new String[] {""} ;
         A129Sistema_Sigla = "";
         H00KX5_A1461SistemaDePara_Origem = new String[] {""} ;
         H00KX5_A127Sistema_Codigo = new int[1] ;
         H00KX5_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         H00KX5_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         H00KX5_A1460SistemaDePara_Codigo = new int[1] ;
         H00KX5_A1462SistemaDePara_OrigemId = new long[1] ;
         AV17SistemaDePara = new SdtSistemaDePara(context);
         H00KX7_A40000GXC1 = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_sistemadepara__default(),
            new Object[][] {
                new Object[] {
               H00KX3_A40000GXC1
               }
               , new Object[] {
               H00KX4_A130Sistema_Ativo, H00KX4_A135Sistema_AreaTrabalhoCod, H00KX4_A127Sistema_Codigo, H00KX4_A129Sistema_Sigla
               }
               , new Object[] {
               H00KX5_A1461SistemaDePara_Origem, H00KX5_A127Sistema_Codigo, H00KX5_A1463SistemaDePara_OrigemDsc, H00KX5_n1463SistemaDePara_OrigemDsc, H00KX5_A1460SistemaDePara_Codigo, H00KX5_A1462SistemaDePara_OrigemId
               }
               , new Object[] {
               H00KX7_A40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV19i ;
      private short nGXWrapped ;
      private int AV21AreaTrabalho_Codigo ;
      private int wcpOAV21AreaTrabalho_Codigo ;
      private int A127Sistema_Codigo ;
      private int A1460SistemaDePara_Codigo ;
      private int AV18SistemaDePara_Codigo ;
      private int A40000GXC1 ;
      private int lblTbjava_Visible ;
      private int AV10Sistema_Codigo ;
      private int AV24GXV1 ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int idxLst ;
      private long A1462SistemaDePara_OrigemId ;
      private long AV8Projeto ;
      private String AV5Origem ;
      private String wcpOAV5Origem ;
      private String Confirmpanel_Result ;
      private String Desvinculapanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1461SistemaDePara_Origem ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Confirmtype ;
      private String Desvinculapanel_Width ;
      private String Desvinculapanel_Title ;
      private String Desvinculapanel_Confirmtext ;
      private String Desvinculapanel_Buttonyestext ;
      private String Desvinculapanel_Buttonnotext ;
      private String Desvinculapanel_Buttoncanceltext ;
      private String Desvinculapanel_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavProjeto_Internalname ;
      private String scmdbuf ;
      private String cmbavSistema_codigo_Internalname ;
      private String A129Sistema_Sigla ;
      private String Desvinculapanel_Internalname ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String tblTable2_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String cmbavProjeto_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String cmbavSistema_codigo_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A130Sistema_Ativo ;
      private bool n1463SistemaDePara_OrigemDsc ;
      private String A1463SistemaDePara_OrigemDsc ;
      private String AV20OrigemDsc ;
      private IGxSession AV7WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavProjeto ;
      private GXCombobox cmbavSistema_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00KX3_A40000GXC1 ;
      private bool[] H00KX4_A130Sistema_Ativo ;
      private int[] H00KX4_A135Sistema_AreaTrabalhoCod ;
      private int[] H00KX4_A127Sistema_Codigo ;
      private String[] H00KX4_A129Sistema_Sigla ;
      private String[] H00KX5_A1461SistemaDePara_Origem ;
      private int[] H00KX5_A127Sistema_Codigo ;
      private String[] H00KX5_A1463SistemaDePara_OrigemDsc ;
      private bool[] H00KX5_n1463SistemaDePara_OrigemDsc ;
      private int[] H00KX5_A1460SistemaDePara_Codigo ;
      private long[] H00KX5_A1462SistemaDePara_OrigemId ;
      private int[] H00KX7_A40000GXC1 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Redmineuser_memberships_membership ))]
      private IGxCollection AV6ProjetoSemDePara ;
      private GXWebForm Form ;
      private SdtSDT_Redmineuser_memberships_membership AV16ProjectItem ;
      private SdtSistemaDePara AV17SistemaDePara ;
   }

   public class wp_sistemadepara__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KX3 ;
          prmH00KX3 = new Object[] {
          new Object[] {"@AV10Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KX4 ;
          prmH00KX4 = new Object[] {
          new Object[] {"@AV21AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KX5 ;
          prmH00KX5 = new Object[] {
          new Object[] {"@AV10Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5Origem",SqlDbType.Char,2,0}
          } ;
          Object[] prmH00KX7 ;
          prmH00KX7 = new Object[] {
          new Object[] {"@AV10Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KX3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV10Sistema_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KX3,1,0,true,false )
             ,new CursorDef("H00KX4", "SELECT [Sistema_Ativo], [Sistema_AreaTrabalhoCod], [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV21AreaTrabalho_Codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KX4,100,0,false,false )
             ,new CursorDef("H00KX5", "SELECT [SistemaDePara_Origem], [Sistema_Codigo], [SistemaDePara_OrigemDsc], [SistemaDePara_Codigo], [SistemaDePara_OrigemId] FROM [SistemaDePara] WITH (NOLOCK) WHERE ([Sistema_Codigo] = @AV10Sistema_Codigo) AND ([SistemaDePara_Origem] = @AV5Origem) ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KX5,100,0,false,false )
             ,new CursorDef("H00KX7", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV10Sistema_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KX7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 25) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((long[]) buf[5])[0] = rslt.getLong(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
