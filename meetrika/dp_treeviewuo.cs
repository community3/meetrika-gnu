/*
               File: DP_TreeViewUO
        Description: TreeView Unidades Organizacionais
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:37.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_treeviewuo : GXProcedure
   {
      public dp_treeviewuo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_treeviewuo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UnidadeOrganizacional_Vinculada ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5UnidadeOrganizacional_Vinculada = aP0_UnidadeOrganizacional_Vinculada;
         this.Gxm2rootcol = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_Meetrika", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_UnidadeOrganizacional_Vinculada )
      {
         this.AV5UnidadeOrganizacional_Vinculada = aP0_UnidadeOrganizacional_Vinculada;
         this.Gxm2rootcol = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_Meetrika", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_UnidadeOrganizacional_Vinculada ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_treeviewuo objdp_treeviewuo;
         objdp_treeviewuo = new dp_treeviewuo();
         objdp_treeviewuo.AV5UnidadeOrganizacional_Vinculada = aP0_UnidadeOrganizacional_Vinculada;
         objdp_treeviewuo.Gxm2rootcol = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_Meetrika", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs") ;
         objdp_treeviewuo.context.SetSubmitInitialConfig(context);
         objdp_treeviewuo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_treeviewuo);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_treeviewuo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV5UnidadeOrganizacional_Vinculada ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P000A2 */
         pr_default.execute(0, new Object[] {AV5UnidadeOrganizacional_Vinculada});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A629UnidadeOrganizacional_Ativo = P000A2_A629UnidadeOrganizacional_Ativo[0];
            A613UnidadeOrganizacional_Vinculada = P000A2_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P000A2_n613UnidadeOrganizacional_Vinculada[0];
            A611UnidadeOrganizacional_Codigo = P000A2_A611UnidadeOrganizacional_Codigo[0];
            A612UnidadeOrganizacional_Nome = P000A2_A612UnidadeOrganizacional_Nome[0];
            Gxm1treenodecollection = new SdtTreeNodeCollection_TreeNode(context);
            Gxm2rootcol.Add(Gxm1treenodecollection, 0);
            Gxm1treenodecollection.gxTpr_Id = StringUtil.Str( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0);
            Gxm1treenodecollection.gxTpr_Name = A612UnidadeOrganizacional_Nome;
            GXt_objcol_SdtTreeNodeCollection_TreeNode1 = new GxObjectCollection();
            new dp_treeviewuo(context ).execute(  A611UnidadeOrganizacional_Codigo, out  GXt_objcol_SdtTreeNodeCollection_TreeNode1) ;
            Gxm1treenodecollection.gxTpr_Nodes = GXt_objcol_SdtTreeNodeCollection_TreeNode1;
            Gxm1treenodecollection.gxTpr_Expanded = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000A2_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P000A2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P000A2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P000A2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         P000A2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         A612UnidadeOrganizacional_Nome = "";
         Gxm1treenodecollection = new SdtTreeNodeCollection_TreeNode(context);
         GXt_objcol_SdtTreeNodeCollection_TreeNode1 = new GxObjectCollection( context, "TreeNodeCollection.TreeNode", "GxEv3Up14_Meetrika", "SdtTreeNodeCollection_TreeNode", "GeneXus.Programs");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_treeviewuo__default(),
            new Object[][] {
                new Object[] {
               P000A2_A629UnidadeOrganizacional_Ativo, P000A2_A613UnidadeOrganizacional_Vinculada, P000A2_n613UnidadeOrganizacional_Vinculada, P000A2_A611UnidadeOrganizacional_Codigo, P000A2_A612UnidadeOrganizacional_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5UnidadeOrganizacional_Vinculada ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A611UnidadeOrganizacional_Codigo ;
      private String scmdbuf ;
      private String A612UnidadeOrganizacional_Nome ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P000A2_A629UnidadeOrganizacional_Ativo ;
      private int[] P000A2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P000A2_n613UnidadeOrganizacional_Vinculada ;
      private int[] P000A2_A611UnidadeOrganizacional_Codigo ;
      private String[] P000A2_A612UnidadeOrganizacional_Nome ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtTreeNodeCollection_TreeNode ))]
      private IGxCollection Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtTreeNodeCollection_TreeNode ))]
      private IGxCollection GXt_objcol_SdtTreeNodeCollection_TreeNode1 ;
      private SdtTreeNodeCollection_TreeNode Gxm1treenodecollection ;
   }

   public class dp_treeviewuo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P000A2( IGxContext context ,
                                             int AV5UnidadeOrganizacional_Vinculada ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ! (0==AV5UnidadeOrganizacional_Vinculada) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV5UnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( (0==AV5UnidadeOrganizacional_Vinculada) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] IS NULL or [UnidadeOrganizacional_Vinculada] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [UnidadeOrganizacional_Codigo]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P000A2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (bool)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000A2 ;
          prmP000A2 = new Object[] {
          new Object[] {"@AV5UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000A2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000A2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
