/*
               File: FuncoesAPFAtributos_BC
        Description: Atributos da Fun��o de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:37.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcoesapfatributos_bc : GXHttpHandler, IGxSilentTrn
   {
      public funcoesapfatributos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcoesapfatributos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1058( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1058( ) ;
         standaloneModal( ) ;
         AddRow1058( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11102 */
            E11102 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
               Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_100( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1058( ) ;
            }
            else
            {
               CheckExtendedTable1058( ) ;
               if ( AnyError == 0 )
               {
                  ZM1058( 4) ;
                  ZM1058( 5) ;
                  ZM1058( 6) ;
                  ZM1058( 7) ;
                  ZM1058( 8) ;
               }
               CloseExtendedTableCursors1058( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12102( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            while ( AV16GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "FuncaoAPFAtributos_FuncaoDadosCod") == 0 )
               {
                  AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "FuncaoAPFAtributos_MelhoraCod") == 0 )
               {
                  AV14Insert_FuncaoAPFAtributos_MelhoraCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
            }
         }
      }

      protected void E11102( )
      {
         /* After Trn Routine */
      }

      protected void ZM1058( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z389FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
            Z383FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
            Z384FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
            Z385FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
            Z751FuncaoAPFAtributos_Ativo = A751FuncaoAPFAtributos_Ativo;
            Z748FuncaoAPFAtributos_MelhoraCod = A748FuncaoAPFAtributos_MelhoraCod;
            Z378FuncaoAPFAtributos_FuncaoDadosCod = A378FuncaoAPFAtributos_FuncaoDadosCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z365FuncaoAPFAtributos_AtributosNom = A365FuncaoAPFAtributos_AtributosNom;
            Z366FuncaoAPFAtributos_AtrTabelaCod = A366FuncaoAPFAtributos_AtrTabelaCod;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z412FuncaoAPFAtributos_FuncaoDadosNom = A412FuncaoAPFAtributos_FuncaoDadosNom;
            Z415FuncaoAPFAtributos_FuncaoDadosTip = A415FuncaoAPFAtributos_FuncaoDadosTip;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z367FuncaoAPFAtributos_AtrTabelaNom = A367FuncaoAPFAtributos_AtrTabelaNom;
            Z393FuncaoAPFAtributos_SistemaCod = A393FuncaoAPFAtributos_SistemaCod;
         }
         if ( GX_JID == -3 )
         {
            Z389FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
            Z383FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
            Z384FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
            Z385FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
            Z751FuncaoAPFAtributos_Ativo = A751FuncaoAPFAtributos_Ativo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
            Z748FuncaoAPFAtributos_MelhoraCod = A748FuncaoAPFAtributos_MelhoraCod;
            Z378FuncaoAPFAtributos_FuncaoDadosCod = A378FuncaoAPFAtributos_FuncaoDadosCod;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            Z365FuncaoAPFAtributos_AtributosNom = A365FuncaoAPFAtributos_AtributosNom;
            Z366FuncaoAPFAtributos_AtrTabelaCod = A366FuncaoAPFAtributos_AtrTabelaCod;
            Z367FuncaoAPFAtributos_AtrTabelaNom = A367FuncaoAPFAtributos_AtrTabelaNom;
            Z393FuncaoAPFAtributos_SistemaCod = A393FuncaoAPFAtributos_SistemaCod;
            Z412FuncaoAPFAtributos_FuncaoDadosNom = A412FuncaoAPFAtributos_FuncaoDadosNom;
            Z415FuncaoAPFAtributos_FuncaoDadosTip = A415FuncaoAPFAtributos_FuncaoDadosTip;
         }
      }

      protected void standaloneNotModal( )
      {
         AV15Pgmname = "FuncoesAPFAtributos_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1058( )
      {
         /* Using cursor BC00109 */
         pr_default.execute(7, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound58 = 1;
            A166FuncaoAPF_Nome = BC00109_A166FuncaoAPF_Nome[0];
            A365FuncaoAPFAtributos_AtributosNom = BC00109_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC00109_n365FuncaoAPFAtributos_AtributosNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = BC00109_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = BC00109_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = BC00109_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = BC00109_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = BC00109_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC00109_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A389FuncoesAPFAtributos_Regra = BC00109_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = BC00109_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = BC00109_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = BC00109_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = BC00109_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = BC00109_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = BC00109_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = BC00109_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = BC00109_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = BC00109_n751FuncaoAPFAtributos_Ativo[0];
            A748FuncaoAPFAtributos_MelhoraCod = BC00109_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = BC00109_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = BC00109_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = BC00109_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC00109_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC00109_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A393FuncaoAPFAtributos_SistemaCod = BC00109_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC00109_n393FuncaoAPFAtributos_SistemaCod[0];
            ZM1058( -3) ;
         }
         pr_default.close(7);
         OnLoadActions1058( ) ;
      }

      protected void OnLoadActions1058( )
      {
      }

      protected void CheckExtendedTable1058( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00104 */
         pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
         }
         A166FuncaoAPF_Nome = BC00104_A166FuncaoAPF_Nome[0];
         pr_default.close(2);
         /* Using cursor BC00105 */
         pr_default.execute(3, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
            AnyError = 1;
         }
         A365FuncaoAPFAtributos_AtributosNom = BC00105_A365FuncaoAPFAtributos_AtributosNom[0];
         n365FuncaoAPFAtributos_AtributosNom = BC00105_n365FuncaoAPFAtributos_AtributosNom[0];
         A366FuncaoAPFAtributos_AtrTabelaCod = BC00105_A366FuncaoAPFAtributos_AtrTabelaCod[0];
         n366FuncaoAPFAtributos_AtrTabelaCod = BC00105_n366FuncaoAPFAtributos_AtrTabelaCod[0];
         pr_default.close(3);
         /* Using cursor BC00108 */
         pr_default.execute(6, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A367FuncaoAPFAtributos_AtrTabelaNom = BC00108_A367FuncaoAPFAtributos_AtrTabelaNom[0];
         n367FuncaoAPFAtributos_AtrTabelaNom = BC00108_n367FuncaoAPFAtributos_AtrTabelaNom[0];
         A393FuncaoAPFAtributos_SistemaCod = BC00108_A393FuncaoAPFAtributos_SistemaCod[0];
         n393FuncaoAPFAtributos_SistemaCod = BC00108_n393FuncaoAPFAtributos_SistemaCod[0];
         pr_default.close(6);
         /* Using cursor BC00107 */
         pr_default.execute(5, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A378FuncaoAPFAtributos_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'FuncaoAPFAtributos_FuncaoDadosTabela'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
               AnyError = 1;
            }
         }
         A412FuncaoAPFAtributos_FuncaoDadosNom = BC00107_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
         n412FuncaoAPFAtributos_FuncaoDadosNom = BC00107_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
         A415FuncaoAPFAtributos_FuncaoDadosTip = BC00107_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
         n415FuncaoAPFAtributos_FuncaoDadosTip = BC00107_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
         pr_default.close(5);
         /* Using cursor BC00106 */
         pr_default.execute(4, new Object[] {n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A748FuncaoAPFAtributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Funcao APFAtributosMelhora'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_MELHORACOD");
               AnyError = 1;
            }
         }
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1058( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1058( )
      {
         /* Using cursor BC001010 */
         pr_default.execute(8, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound58 = 1;
         }
         else
         {
            RcdFound58 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00103 */
         pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1058( 3) ;
            RcdFound58 = 1;
            A389FuncoesAPFAtributos_Regra = BC00103_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = BC00103_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = BC00103_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = BC00103_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = BC00103_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = BC00103_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = BC00103_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = BC00103_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = BC00103_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = BC00103_n751FuncaoAPFAtributos_Ativo[0];
            A165FuncaoAPF_Codigo = BC00103_A165FuncaoAPF_Codigo[0];
            A364FuncaoAPFAtributos_AtributosCod = BC00103_A364FuncaoAPFAtributos_AtributosCod[0];
            A748FuncaoAPFAtributos_MelhoraCod = BC00103_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = BC00103_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = BC00103_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = BC00103_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
            sMode58 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1058( ) ;
            if ( AnyError == 1 )
            {
               RcdFound58 = 0;
               InitializeNonKey1058( ) ;
            }
            Gx_mode = sMode58;
         }
         else
         {
            RcdFound58 = 0;
            InitializeNonKey1058( ) ;
            sMode58 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode58;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1058( ) ;
         if ( RcdFound58 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_100( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00102 */
            pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPFAtributos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z389FuncoesAPFAtributos_Regra != BC00102_A389FuncoesAPFAtributos_Regra[0] ) || ( StringUtil.StrCmp(Z383FuncoesAPFAtributos_Code, BC00102_A383FuncoesAPFAtributos_Code[0]) != 0 ) || ( StringUtil.StrCmp(Z384FuncoesAPFAtributos_Nome, BC00102_A384FuncoesAPFAtributos_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z385FuncoesAPFAtributos_Descricao, BC00102_A385FuncoesAPFAtributos_Descricao[0]) != 0 ) || ( Z751FuncaoAPFAtributos_Ativo != BC00102_A751FuncaoAPFAtributos_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z748FuncaoAPFAtributos_MelhoraCod != BC00102_A748FuncaoAPFAtributos_MelhoraCod[0] ) || ( Z378FuncaoAPFAtributos_FuncaoDadosCod != BC00102_A378FuncaoAPFAtributos_FuncaoDadosCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncoesAPFAtributos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1058( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1058( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1058( 0) ;
            CheckOptimisticConcurrency1058( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1058( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001011 */
                     pr_default.execute(9, new Object[] {n389FuncoesAPFAtributos_Regra, A389FuncoesAPFAtributos_Regra, n383FuncoesAPFAtributos_Code, A383FuncoesAPFAtributos_Code, n384FuncoesAPFAtributos_Nome, A384FuncoesAPFAtributos_Nome, n385FuncoesAPFAtributos_Descricao, A385FuncoesAPFAtributos_Descricao, n751FuncaoAPFAtributos_Ativo, A751FuncaoAPFAtributos_Ativo, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod, n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1058( ) ;
            }
            EndLevel1058( ) ;
         }
         CloseExtendedTableCursors1058( ) ;
      }

      protected void Update1058( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1058( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1058( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1058( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001012 */
                     pr_default.execute(10, new Object[] {n389FuncoesAPFAtributos_Regra, A389FuncoesAPFAtributos_Regra, n383FuncoesAPFAtributos_Code, A383FuncoesAPFAtributos_Code, n384FuncoesAPFAtributos_Nome, A384FuncoesAPFAtributos_Nome, n385FuncoesAPFAtributos_Descricao, A385FuncoesAPFAtributos_Descricao, n751FuncaoAPFAtributos_Ativo, A751FuncaoAPFAtributos_Ativo, n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod, n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPFAtributos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1058( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1058( ) ;
         }
         CloseExtendedTableCursors1058( ) ;
      }

      protected void DeferredUpdate1058( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1058( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1058( ) ;
            AfterConfirm1058( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1058( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001013 */
                  pr_default.execute(11, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode58 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1058( ) ;
         Gx_mode = sMode58;
      }

      protected void OnDeleteControls1058( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001014 */
            pr_default.execute(12, new Object[] {A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = BC001014_A166FuncaoAPF_Nome[0];
            pr_default.close(12);
            /* Using cursor BC001015 */
            pr_default.execute(13, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
            A365FuncaoAPFAtributos_AtributosNom = BC001015_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC001015_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC001015_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC001015_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            pr_default.close(13);
            /* Using cursor BC001016 */
            pr_default.execute(14, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
            A367FuncaoAPFAtributos_AtrTabelaNom = BC001016_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC001016_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A393FuncaoAPFAtributos_SistemaCod = BC001016_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC001016_n393FuncaoAPFAtributos_SistemaCod[0];
            pr_default.close(14);
            /* Using cursor BC001017 */
            pr_default.execute(15, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
            A412FuncaoAPFAtributos_FuncaoDadosNom = BC001017_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = BC001017_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = BC001017_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = BC001017_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel1058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1058( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1058( )
      {
         /* Scan By routine */
         /* Using cursor BC001018 */
         pr_default.execute(16, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         RcdFound58 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound58 = 1;
            A166FuncaoAPF_Nome = BC001018_A166FuncaoAPF_Nome[0];
            A365FuncaoAPFAtributos_AtributosNom = BC001018_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC001018_n365FuncaoAPFAtributos_AtributosNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = BC001018_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = BC001018_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = BC001018_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = BC001018_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = BC001018_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC001018_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A389FuncoesAPFAtributos_Regra = BC001018_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = BC001018_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = BC001018_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = BC001018_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = BC001018_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = BC001018_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = BC001018_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = BC001018_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = BC001018_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = BC001018_n751FuncaoAPFAtributos_Ativo[0];
            A165FuncaoAPF_Codigo = BC001018_A165FuncaoAPF_Codigo[0];
            A364FuncaoAPFAtributos_AtributosCod = BC001018_A364FuncaoAPFAtributos_AtributosCod[0];
            A748FuncaoAPFAtributos_MelhoraCod = BC001018_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = BC001018_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = BC001018_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = BC001018_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC001018_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC001018_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A393FuncaoAPFAtributos_SistemaCod = BC001018_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC001018_n393FuncaoAPFAtributos_SistemaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1058( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound58 = 0;
         ScanKeyLoad1058( ) ;
      }

      protected void ScanKeyLoad1058( )
      {
         sMode58 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound58 = 1;
            A166FuncaoAPF_Nome = BC001018_A166FuncaoAPF_Nome[0];
            A365FuncaoAPFAtributos_AtributosNom = BC001018_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC001018_n365FuncaoAPFAtributos_AtributosNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = BC001018_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = BC001018_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = BC001018_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = BC001018_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = BC001018_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC001018_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A389FuncoesAPFAtributos_Regra = BC001018_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = BC001018_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = BC001018_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = BC001018_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = BC001018_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = BC001018_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = BC001018_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = BC001018_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = BC001018_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = BC001018_n751FuncaoAPFAtributos_Ativo[0];
            A165FuncaoAPF_Codigo = BC001018_A165FuncaoAPF_Codigo[0];
            A364FuncaoAPFAtributos_AtributosCod = BC001018_A364FuncaoAPFAtributos_AtributosCod[0];
            A748FuncaoAPFAtributos_MelhoraCod = BC001018_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = BC001018_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = BC001018_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = BC001018_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC001018_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC001018_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A393FuncaoAPFAtributos_SistemaCod = BC001018_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC001018_n393FuncaoAPFAtributos_SistemaCod[0];
         }
         Gx_mode = sMode58;
      }

      protected void ScanKeyEnd1058( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm1058( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1058( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1058( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1058( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1058( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1058( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1058( )
      {
      }

      protected void AddRow1058( )
      {
         VarsToRow58( bcFuncoesAPFAtributos) ;
      }

      protected void ReadRow1058( )
      {
         RowToVars58( bcFuncoesAPFAtributos, 1) ;
      }

      protected void InitializeNonKey1058( )
      {
         A166FuncaoAPF_Nome = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         n365FuncaoAPFAtributos_AtributosNom = false;
         A378FuncaoAPFAtributos_FuncaoDadosCod = 0;
         n378FuncaoAPFAtributos_FuncaoDadosCod = false;
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         n412FuncaoAPFAtributos_FuncaoDadosNom = false;
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         n415FuncaoAPFAtributos_FuncaoDadosTip = false;
         A366FuncaoAPFAtributos_AtrTabelaCod = 0;
         n366FuncaoAPFAtributos_AtrTabelaCod = false;
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         n367FuncaoAPFAtributos_AtrTabelaNom = false;
         A393FuncaoAPFAtributos_SistemaCod = 0;
         n393FuncaoAPFAtributos_SistemaCod = false;
         A389FuncoesAPFAtributos_Regra = false;
         n389FuncoesAPFAtributos_Regra = false;
         A383FuncoesAPFAtributos_Code = "";
         n383FuncoesAPFAtributos_Code = false;
         A384FuncoesAPFAtributos_Nome = "";
         n384FuncoesAPFAtributos_Nome = false;
         A385FuncoesAPFAtributos_Descricao = "";
         n385FuncoesAPFAtributos_Descricao = false;
         A748FuncaoAPFAtributos_MelhoraCod = 0;
         n748FuncaoAPFAtributos_MelhoraCod = false;
         A751FuncaoAPFAtributos_Ativo = false;
         n751FuncaoAPFAtributos_Ativo = false;
         Z389FuncoesAPFAtributos_Regra = false;
         Z383FuncoesAPFAtributos_Code = "";
         Z384FuncoesAPFAtributos_Nome = "";
         Z385FuncoesAPFAtributos_Descricao = "";
         Z751FuncaoAPFAtributos_Ativo = false;
         Z748FuncaoAPFAtributos_MelhoraCod = 0;
         Z378FuncaoAPFAtributos_FuncaoDadosCod = 0;
      }

      protected void InitAll1058( )
      {
         A165FuncaoAPF_Codigo = 0;
         A364FuncaoAPFAtributos_AtributosCod = 0;
         InitializeNonKey1058( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow58( SdtFuncoesAPFAtributos obj58 )
      {
         obj58.gxTpr_Mode = Gx_mode;
         obj58.gxTpr_Funcaoapf_nome = A166FuncaoAPF_Nome;
         obj58.gxTpr_Funcaoapfatributos_atributosnom = A365FuncaoAPFAtributos_AtributosNom;
         obj58.gxTpr_Funcaoapfatributos_funcaodadoscod = A378FuncaoAPFAtributos_FuncaoDadosCod;
         obj58.gxTpr_Funcaoapfatributos_funcaodadosnom = A412FuncaoAPFAtributos_FuncaoDadosNom;
         obj58.gxTpr_Funcaoapfatributos_funcaodadostip = A415FuncaoAPFAtributos_FuncaoDadosTip;
         obj58.gxTpr_Funcaoapfatributos_atrtabelacod = A366FuncaoAPFAtributos_AtrTabelaCod;
         obj58.gxTpr_Funcaoapfatributos_atrtabelanom = A367FuncaoAPFAtributos_AtrTabelaNom;
         obj58.gxTpr_Funcaoapfatributos_sistemacod = A393FuncaoAPFAtributos_SistemaCod;
         obj58.gxTpr_Funcoesapfatributos_regra = A389FuncoesAPFAtributos_Regra;
         obj58.gxTpr_Funcoesapfatributos_code = A383FuncoesAPFAtributos_Code;
         obj58.gxTpr_Funcoesapfatributos_nome = A384FuncoesAPFAtributos_Nome;
         obj58.gxTpr_Funcoesapfatributos_descricao = A385FuncoesAPFAtributos_Descricao;
         obj58.gxTpr_Funcaoapfatributos_melhoracod = A748FuncaoAPFAtributos_MelhoraCod;
         obj58.gxTpr_Funcaoapfatributos_ativo = A751FuncaoAPFAtributos_Ativo;
         obj58.gxTpr_Funcaoapf_codigo = A165FuncaoAPF_Codigo;
         obj58.gxTpr_Funcaoapfatributos_atributoscod = A364FuncaoAPFAtributos_AtributosCod;
         obj58.gxTpr_Funcaoapf_codigo_Z = Z165FuncaoAPF_Codigo;
         obj58.gxTpr_Funcaoapf_nome_Z = Z166FuncaoAPF_Nome;
         obj58.gxTpr_Funcaoapfatributos_atributoscod_Z = Z364FuncaoAPFAtributos_AtributosCod;
         obj58.gxTpr_Funcaoapfatributos_atributosnom_Z = Z365FuncaoAPFAtributos_AtributosNom;
         obj58.gxTpr_Funcaoapfatributos_funcaodadoscod_Z = Z378FuncaoAPFAtributos_FuncaoDadosCod;
         obj58.gxTpr_Funcaoapfatributos_funcaodadosnom_Z = Z412FuncaoAPFAtributos_FuncaoDadosNom;
         obj58.gxTpr_Funcaoapfatributos_funcaodadostip_Z = Z415FuncaoAPFAtributos_FuncaoDadosTip;
         obj58.gxTpr_Funcaoapfatributos_atrtabelacod_Z = Z366FuncaoAPFAtributos_AtrTabelaCod;
         obj58.gxTpr_Funcaoapfatributos_atrtabelanom_Z = Z367FuncaoAPFAtributos_AtrTabelaNom;
         obj58.gxTpr_Funcaoapfatributos_sistemacod_Z = Z393FuncaoAPFAtributos_SistemaCod;
         obj58.gxTpr_Funcoesapfatributos_regra_Z = Z389FuncoesAPFAtributos_Regra;
         obj58.gxTpr_Funcoesapfatributos_code_Z = Z383FuncoesAPFAtributos_Code;
         obj58.gxTpr_Funcoesapfatributos_nome_Z = Z384FuncoesAPFAtributos_Nome;
         obj58.gxTpr_Funcoesapfatributos_descricao_Z = Z385FuncoesAPFAtributos_Descricao;
         obj58.gxTpr_Funcaoapfatributos_melhoracod_Z = Z748FuncaoAPFAtributos_MelhoraCod;
         obj58.gxTpr_Funcaoapfatributos_ativo_Z = Z751FuncaoAPFAtributos_Ativo;
         obj58.gxTpr_Funcaoapfatributos_atributosnom_N = (short)(Convert.ToInt16(n365FuncaoAPFAtributos_AtributosNom));
         obj58.gxTpr_Funcaoapfatributos_funcaodadoscod_N = (short)(Convert.ToInt16(n378FuncaoAPFAtributos_FuncaoDadosCod));
         obj58.gxTpr_Funcaoapfatributos_funcaodadosnom_N = (short)(Convert.ToInt16(n412FuncaoAPFAtributos_FuncaoDadosNom));
         obj58.gxTpr_Funcaoapfatributos_funcaodadostip_N = (short)(Convert.ToInt16(n415FuncaoAPFAtributos_FuncaoDadosTip));
         obj58.gxTpr_Funcaoapfatributos_atrtabelacod_N = (short)(Convert.ToInt16(n366FuncaoAPFAtributos_AtrTabelaCod));
         obj58.gxTpr_Funcaoapfatributos_atrtabelanom_N = (short)(Convert.ToInt16(n367FuncaoAPFAtributos_AtrTabelaNom));
         obj58.gxTpr_Funcaoapfatributos_sistemacod_N = (short)(Convert.ToInt16(n393FuncaoAPFAtributos_SistemaCod));
         obj58.gxTpr_Funcoesapfatributos_regra_N = (short)(Convert.ToInt16(n389FuncoesAPFAtributos_Regra));
         obj58.gxTpr_Funcoesapfatributos_code_N = (short)(Convert.ToInt16(n383FuncoesAPFAtributos_Code));
         obj58.gxTpr_Funcoesapfatributos_nome_N = (short)(Convert.ToInt16(n384FuncoesAPFAtributos_Nome));
         obj58.gxTpr_Funcoesapfatributos_descricao_N = (short)(Convert.ToInt16(n385FuncoesAPFAtributos_Descricao));
         obj58.gxTpr_Funcaoapfatributos_melhoracod_N = (short)(Convert.ToInt16(n748FuncaoAPFAtributos_MelhoraCod));
         obj58.gxTpr_Funcaoapfatributos_ativo_N = (short)(Convert.ToInt16(n751FuncaoAPFAtributos_Ativo));
         obj58.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow58( SdtFuncoesAPFAtributos obj58 )
      {
         obj58.gxTpr_Funcaoapf_codigo = A165FuncaoAPF_Codigo;
         obj58.gxTpr_Funcaoapfatributos_atributoscod = A364FuncaoAPFAtributos_AtributosCod;
         return  ;
      }

      public void RowToVars58( SdtFuncoesAPFAtributos obj58 ,
                               int forceLoad )
      {
         Gx_mode = obj58.gxTpr_Mode;
         A166FuncaoAPF_Nome = obj58.gxTpr_Funcaoapf_nome;
         A365FuncaoAPFAtributos_AtributosNom = obj58.gxTpr_Funcaoapfatributos_atributosnom;
         n365FuncaoAPFAtributos_AtributosNom = false;
         A378FuncaoAPFAtributos_FuncaoDadosCod = obj58.gxTpr_Funcaoapfatributos_funcaodadoscod;
         n378FuncaoAPFAtributos_FuncaoDadosCod = false;
         A412FuncaoAPFAtributos_FuncaoDadosNom = obj58.gxTpr_Funcaoapfatributos_funcaodadosnom;
         n412FuncaoAPFAtributos_FuncaoDadosNom = false;
         A415FuncaoAPFAtributos_FuncaoDadosTip = obj58.gxTpr_Funcaoapfatributos_funcaodadostip;
         n415FuncaoAPFAtributos_FuncaoDadosTip = false;
         A366FuncaoAPFAtributos_AtrTabelaCod = obj58.gxTpr_Funcaoapfatributos_atrtabelacod;
         n366FuncaoAPFAtributos_AtrTabelaCod = false;
         A367FuncaoAPFAtributos_AtrTabelaNom = obj58.gxTpr_Funcaoapfatributos_atrtabelanom;
         n367FuncaoAPFAtributos_AtrTabelaNom = false;
         A393FuncaoAPFAtributos_SistemaCod = obj58.gxTpr_Funcaoapfatributos_sistemacod;
         n393FuncaoAPFAtributos_SistemaCod = false;
         A389FuncoesAPFAtributos_Regra = obj58.gxTpr_Funcoesapfatributos_regra;
         n389FuncoesAPFAtributos_Regra = false;
         A383FuncoesAPFAtributos_Code = obj58.gxTpr_Funcoesapfatributos_code;
         n383FuncoesAPFAtributos_Code = false;
         A384FuncoesAPFAtributos_Nome = obj58.gxTpr_Funcoesapfatributos_nome;
         n384FuncoesAPFAtributos_Nome = false;
         A385FuncoesAPFAtributos_Descricao = obj58.gxTpr_Funcoesapfatributos_descricao;
         n385FuncoesAPFAtributos_Descricao = false;
         A748FuncaoAPFAtributos_MelhoraCod = obj58.gxTpr_Funcaoapfatributos_melhoracod;
         n748FuncaoAPFAtributos_MelhoraCod = false;
         A751FuncaoAPFAtributos_Ativo = obj58.gxTpr_Funcaoapfatributos_ativo;
         n751FuncaoAPFAtributos_Ativo = false;
         A165FuncaoAPF_Codigo = obj58.gxTpr_Funcaoapf_codigo;
         A364FuncaoAPFAtributos_AtributosCod = obj58.gxTpr_Funcaoapfatributos_atributoscod;
         Z165FuncaoAPF_Codigo = obj58.gxTpr_Funcaoapf_codigo_Z;
         Z166FuncaoAPF_Nome = obj58.gxTpr_Funcaoapf_nome_Z;
         Z364FuncaoAPFAtributos_AtributosCod = obj58.gxTpr_Funcaoapfatributos_atributoscod_Z;
         Z365FuncaoAPFAtributos_AtributosNom = obj58.gxTpr_Funcaoapfatributos_atributosnom_Z;
         Z378FuncaoAPFAtributos_FuncaoDadosCod = obj58.gxTpr_Funcaoapfatributos_funcaodadoscod_Z;
         Z412FuncaoAPFAtributos_FuncaoDadosNom = obj58.gxTpr_Funcaoapfatributos_funcaodadosnom_Z;
         Z415FuncaoAPFAtributos_FuncaoDadosTip = obj58.gxTpr_Funcaoapfatributos_funcaodadostip_Z;
         Z366FuncaoAPFAtributos_AtrTabelaCod = obj58.gxTpr_Funcaoapfatributos_atrtabelacod_Z;
         Z367FuncaoAPFAtributos_AtrTabelaNom = obj58.gxTpr_Funcaoapfatributos_atrtabelanom_Z;
         Z393FuncaoAPFAtributos_SistemaCod = obj58.gxTpr_Funcaoapfatributos_sistemacod_Z;
         Z389FuncoesAPFAtributos_Regra = obj58.gxTpr_Funcoesapfatributos_regra_Z;
         Z383FuncoesAPFAtributos_Code = obj58.gxTpr_Funcoesapfatributos_code_Z;
         Z384FuncoesAPFAtributos_Nome = obj58.gxTpr_Funcoesapfatributos_nome_Z;
         Z385FuncoesAPFAtributos_Descricao = obj58.gxTpr_Funcoesapfatributos_descricao_Z;
         Z748FuncaoAPFAtributos_MelhoraCod = obj58.gxTpr_Funcaoapfatributos_melhoracod_Z;
         Z751FuncaoAPFAtributos_Ativo = obj58.gxTpr_Funcaoapfatributos_ativo_Z;
         n365FuncaoAPFAtributos_AtributosNom = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_atributosnom_N));
         n378FuncaoAPFAtributos_FuncaoDadosCod = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_funcaodadoscod_N));
         n412FuncaoAPFAtributos_FuncaoDadosNom = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_funcaodadosnom_N));
         n415FuncaoAPFAtributos_FuncaoDadosTip = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_funcaodadostip_N));
         n366FuncaoAPFAtributos_AtrTabelaCod = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_atrtabelacod_N));
         n367FuncaoAPFAtributos_AtrTabelaNom = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_atrtabelanom_N));
         n393FuncaoAPFAtributos_SistemaCod = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_sistemacod_N));
         n389FuncoesAPFAtributos_Regra = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcoesapfatributos_regra_N));
         n383FuncoesAPFAtributos_Code = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcoesapfatributos_code_N));
         n384FuncoesAPFAtributos_Nome = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcoesapfatributos_nome_N));
         n385FuncoesAPFAtributos_Descricao = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcoesapfatributos_descricao_N));
         n748FuncaoAPFAtributos_MelhoraCod = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_melhoracod_N));
         n751FuncaoAPFAtributos_Ativo = (bool)(Convert.ToBoolean(obj58.gxTpr_Funcaoapfatributos_ativo_N));
         Gx_mode = obj58.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A165FuncaoAPF_Codigo = (int)getParm(obj,0);
         A364FuncaoAPFAtributos_AtributosCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1058( ) ;
         ScanKeyStart1058( ) ;
         if ( RcdFound58 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001014 */
            pr_default.execute(12, new Object[] {A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
            }
            A166FuncaoAPF_Nome = BC001014_A166FuncaoAPF_Nome[0];
            pr_default.close(12);
            /* Using cursor BC001015 */
            pr_default.execute(13, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
               AnyError = 1;
            }
            A365FuncaoAPFAtributos_AtributosNom = BC001015_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC001015_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC001015_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC001015_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            pr_default.close(13);
            /* Using cursor BC001016 */
            pr_default.execute(14, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A367FuncaoAPFAtributos_AtrTabelaNom = BC001016_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC001016_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A393FuncaoAPFAtributos_SistemaCod = BC001016_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC001016_n393FuncaoAPFAtributos_SistemaCod[0];
            pr_default.close(14);
         }
         else
         {
            Gx_mode = "UPD";
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
         }
         ZM1058( -3) ;
         OnLoadActions1058( ) ;
         AddRow1058( ) ;
         ScanKeyEnd1058( ) ;
         if ( RcdFound58 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars58( bcFuncoesAPFAtributos, 0) ;
         ScanKeyStart1058( ) ;
         if ( RcdFound58 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001014 */
            pr_default.execute(12, new Object[] {A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
               AnyError = 1;
            }
            A166FuncaoAPF_Nome = BC001014_A166FuncaoAPF_Nome[0];
            pr_default.close(12);
            /* Using cursor BC001015 */
            pr_default.execute(13, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
               AnyError = 1;
            }
            A365FuncaoAPFAtributos_AtributosNom = BC001015_A365FuncaoAPFAtributos_AtributosNom[0];
            n365FuncaoAPFAtributos_AtributosNom = BC001015_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = BC001015_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            n366FuncaoAPFAtributos_AtrTabelaCod = BC001015_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            pr_default.close(13);
            /* Using cursor BC001016 */
            pr_default.execute(14, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A367FuncaoAPFAtributos_AtrTabelaNom = BC001016_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            n367FuncaoAPFAtributos_AtrTabelaNom = BC001016_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A393FuncaoAPFAtributos_SistemaCod = BC001016_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = BC001016_n393FuncaoAPFAtributos_SistemaCod[0];
            pr_default.close(14);
         }
         else
         {
            Gx_mode = "UPD";
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
         }
         ZM1058( -3) ;
         OnLoadActions1058( ) ;
         AddRow1058( ) ;
         ScanKeyEnd1058( ) ;
         if ( RcdFound58 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars58( bcFuncoesAPFAtributos, 0) ;
         nKeyPressed = 1;
         GetKey1058( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1058( ) ;
         }
         else
         {
            if ( RcdFound58 == 1 )
            {
               if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
               {
                  A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
                  A364FuncaoAPFAtributos_AtributosCod = Z364FuncaoAPFAtributos_AtributosCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1058( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1058( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1058( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow58( bcFuncoesAPFAtributos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars58( bcFuncoesAPFAtributos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1058( ) ;
         if ( RcdFound58 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
            {
               A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
               A364FuncaoAPFAtributos_AtributosCod = Z364FuncaoAPFAtributos_AtributosCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(15);
         pr_default.close(14);
         context.RollbackDataStores( "FuncoesAPFAtributos_BC");
         VarsToRow58( bcFuncoesAPFAtributos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcFuncoesAPFAtributos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcFuncoesAPFAtributos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcFuncoesAPFAtributos )
         {
            bcFuncoesAPFAtributos = (SdtFuncoesAPFAtributos)(sdt);
            if ( StringUtil.StrCmp(bcFuncoesAPFAtributos.gxTpr_Mode, "") == 0 )
            {
               bcFuncoesAPFAtributos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow58( bcFuncoesAPFAtributos) ;
            }
            else
            {
               RowToVars58( bcFuncoesAPFAtributos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcFuncoesAPFAtributos.gxTpr_Mode, "") == 0 )
            {
               bcFuncoesAPFAtributos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars58( bcFuncoesAPFAtributos, 1) ;
         return  ;
      }

      public SdtFuncoesAPFAtributos FuncoesAPFAtributos_BC
      {
         get {
            return bcFuncoesAPFAtributos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(15);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV15Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z383FuncoesAPFAtributos_Code = "";
         A383FuncoesAPFAtributos_Code = "";
         Z384FuncoesAPFAtributos_Nome = "";
         A384FuncoesAPFAtributos_Nome = "";
         Z385FuncoesAPFAtributos_Descricao = "";
         A385FuncoesAPFAtributos_Descricao = "";
         Z166FuncaoAPF_Nome = "";
         A166FuncaoAPF_Nome = "";
         Z365FuncaoAPFAtributos_AtributosNom = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         Z412FuncaoAPFAtributos_FuncaoDadosNom = "";
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         Z415FuncaoAPFAtributos_FuncaoDadosTip = "";
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         Z367FuncaoAPFAtributos_AtrTabelaNom = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         BC00109_A166FuncaoAPF_Nome = new String[] {""} ;
         BC00109_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         BC00109_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         BC00109_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         BC00109_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         BC00109_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         BC00109_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         BC00109_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         BC00109_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         BC00109_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00109_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00109_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         BC00109_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         BC00109_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         BC00109_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         BC00109_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         BC00109_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         BC00109_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00109_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00109_A165FuncaoAPF_Codigo = new int[1] ;
         BC00109_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC00109_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         BC00109_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         BC00109_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         BC00109_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         BC00109_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         BC00109_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         BC00109_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         BC00109_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         BC00104_A166FuncaoAPF_Nome = new String[] {""} ;
         BC00105_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         BC00105_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         BC00105_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         BC00105_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         BC00108_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         BC00108_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         BC00108_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         BC00108_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         BC00107_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         BC00107_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         BC00107_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         BC00107_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         BC00106_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         BC00106_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         BC001010_A165FuncaoAPF_Codigo = new int[1] ;
         BC001010_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC00103_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00103_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00103_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         BC00103_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         BC00103_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         BC00103_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         BC00103_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         BC00103_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         BC00103_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00103_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00103_A165FuncaoAPF_Codigo = new int[1] ;
         BC00103_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC00103_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         BC00103_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         BC00103_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         BC00103_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         sMode58 = "";
         BC00102_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00102_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC00102_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         BC00102_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         BC00102_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         BC00102_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         BC00102_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         BC00102_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         BC00102_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00102_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC00102_A165FuncaoAPF_Codigo = new int[1] ;
         BC00102_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC00102_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         BC00102_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         BC00102_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         BC00102_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         BC001014_A166FuncaoAPF_Nome = new String[] {""} ;
         BC001015_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         BC001015_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         BC001015_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         BC001015_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         BC001016_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         BC001016_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         BC001016_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         BC001016_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         BC001017_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         BC001017_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         BC001017_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         BC001017_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         BC001018_A166FuncaoAPF_Nome = new String[] {""} ;
         BC001018_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         BC001018_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         BC001018_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         BC001018_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         BC001018_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         BC001018_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         BC001018_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         BC001018_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         BC001018_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC001018_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         BC001018_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         BC001018_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         BC001018_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         BC001018_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         BC001018_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         BC001018_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         BC001018_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC001018_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         BC001018_A165FuncaoAPF_Codigo = new int[1] ;
         BC001018_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         BC001018_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         BC001018_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         BC001018_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         BC001018_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         BC001018_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         BC001018_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         BC001018_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         BC001018_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcoesapfatributos_bc__default(),
            new Object[][] {
                new Object[] {
               BC00102_A389FuncoesAPFAtributos_Regra, BC00102_n389FuncoesAPFAtributos_Regra, BC00102_A383FuncoesAPFAtributos_Code, BC00102_n383FuncoesAPFAtributos_Code, BC00102_A384FuncoesAPFAtributos_Nome, BC00102_n384FuncoesAPFAtributos_Nome, BC00102_A385FuncoesAPFAtributos_Descricao, BC00102_n385FuncoesAPFAtributos_Descricao, BC00102_A751FuncaoAPFAtributos_Ativo, BC00102_n751FuncaoAPFAtributos_Ativo,
               BC00102_A165FuncaoAPF_Codigo, BC00102_A364FuncaoAPFAtributos_AtributosCod, BC00102_A748FuncaoAPFAtributos_MelhoraCod, BC00102_n748FuncaoAPFAtributos_MelhoraCod, BC00102_A378FuncaoAPFAtributos_FuncaoDadosCod, BC00102_n378FuncaoAPFAtributos_FuncaoDadosCod
               }
               , new Object[] {
               BC00103_A389FuncoesAPFAtributos_Regra, BC00103_n389FuncoesAPFAtributos_Regra, BC00103_A383FuncoesAPFAtributos_Code, BC00103_n383FuncoesAPFAtributos_Code, BC00103_A384FuncoesAPFAtributos_Nome, BC00103_n384FuncoesAPFAtributos_Nome, BC00103_A385FuncoesAPFAtributos_Descricao, BC00103_n385FuncoesAPFAtributos_Descricao, BC00103_A751FuncaoAPFAtributos_Ativo, BC00103_n751FuncaoAPFAtributos_Ativo,
               BC00103_A165FuncaoAPF_Codigo, BC00103_A364FuncaoAPFAtributos_AtributosCod, BC00103_A748FuncaoAPFAtributos_MelhoraCod, BC00103_n748FuncaoAPFAtributos_MelhoraCod, BC00103_A378FuncaoAPFAtributos_FuncaoDadosCod, BC00103_n378FuncaoAPFAtributos_FuncaoDadosCod
               }
               , new Object[] {
               BC00104_A166FuncaoAPF_Nome
               }
               , new Object[] {
               BC00105_A365FuncaoAPFAtributos_AtributosNom, BC00105_n365FuncaoAPFAtributos_AtributosNom, BC00105_A366FuncaoAPFAtributos_AtrTabelaCod, BC00105_n366FuncaoAPFAtributos_AtrTabelaCod
               }
               , new Object[] {
               BC00106_A748FuncaoAPFAtributos_MelhoraCod
               }
               , new Object[] {
               BC00107_A412FuncaoAPFAtributos_FuncaoDadosNom, BC00107_n412FuncaoAPFAtributos_FuncaoDadosNom, BC00107_A415FuncaoAPFAtributos_FuncaoDadosTip, BC00107_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
               , new Object[] {
               BC00108_A367FuncaoAPFAtributos_AtrTabelaNom, BC00108_n367FuncaoAPFAtributos_AtrTabelaNom, BC00108_A393FuncaoAPFAtributos_SistemaCod, BC00108_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               BC00109_A166FuncaoAPF_Nome, BC00109_A365FuncaoAPFAtributos_AtributosNom, BC00109_n365FuncaoAPFAtributos_AtributosNom, BC00109_A412FuncaoAPFAtributos_FuncaoDadosNom, BC00109_n412FuncaoAPFAtributos_FuncaoDadosNom, BC00109_A415FuncaoAPFAtributos_FuncaoDadosTip, BC00109_n415FuncaoAPFAtributos_FuncaoDadosTip, BC00109_A367FuncaoAPFAtributos_AtrTabelaNom, BC00109_n367FuncaoAPFAtributos_AtrTabelaNom, BC00109_A389FuncoesAPFAtributos_Regra,
               BC00109_n389FuncoesAPFAtributos_Regra, BC00109_A383FuncoesAPFAtributos_Code, BC00109_n383FuncoesAPFAtributos_Code, BC00109_A384FuncoesAPFAtributos_Nome, BC00109_n384FuncoesAPFAtributos_Nome, BC00109_A385FuncoesAPFAtributos_Descricao, BC00109_n385FuncoesAPFAtributos_Descricao, BC00109_A751FuncaoAPFAtributos_Ativo, BC00109_n751FuncaoAPFAtributos_Ativo, BC00109_A165FuncaoAPF_Codigo,
               BC00109_A364FuncaoAPFAtributos_AtributosCod, BC00109_A748FuncaoAPFAtributos_MelhoraCod, BC00109_n748FuncaoAPFAtributos_MelhoraCod, BC00109_A378FuncaoAPFAtributos_FuncaoDadosCod, BC00109_n378FuncaoAPFAtributos_FuncaoDadosCod, BC00109_A366FuncaoAPFAtributos_AtrTabelaCod, BC00109_n366FuncaoAPFAtributos_AtrTabelaCod, BC00109_A393FuncaoAPFAtributos_SistemaCod, BC00109_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               BC001010_A165FuncaoAPF_Codigo, BC001010_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001014_A166FuncaoAPF_Nome
               }
               , new Object[] {
               BC001015_A365FuncaoAPFAtributos_AtributosNom, BC001015_n365FuncaoAPFAtributos_AtributosNom, BC001015_A366FuncaoAPFAtributos_AtrTabelaCod, BC001015_n366FuncaoAPFAtributos_AtrTabelaCod
               }
               , new Object[] {
               BC001016_A367FuncaoAPFAtributos_AtrTabelaNom, BC001016_n367FuncaoAPFAtributos_AtrTabelaNom, BC001016_A393FuncaoAPFAtributos_SistemaCod, BC001016_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               BC001017_A412FuncaoAPFAtributos_FuncaoDadosNom, BC001017_n412FuncaoAPFAtributos_FuncaoDadosNom, BC001017_A415FuncaoAPFAtributos_FuncaoDadosTip, BC001017_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
               , new Object[] {
               BC001018_A166FuncaoAPF_Nome, BC001018_A365FuncaoAPFAtributos_AtributosNom, BC001018_n365FuncaoAPFAtributos_AtributosNom, BC001018_A412FuncaoAPFAtributos_FuncaoDadosNom, BC001018_n412FuncaoAPFAtributos_FuncaoDadosNom, BC001018_A415FuncaoAPFAtributos_FuncaoDadosTip, BC001018_n415FuncaoAPFAtributos_FuncaoDadosTip, BC001018_A367FuncaoAPFAtributos_AtrTabelaNom, BC001018_n367FuncaoAPFAtributos_AtrTabelaNom, BC001018_A389FuncoesAPFAtributos_Regra,
               BC001018_n389FuncoesAPFAtributos_Regra, BC001018_A383FuncoesAPFAtributos_Code, BC001018_n383FuncoesAPFAtributos_Code, BC001018_A384FuncoesAPFAtributos_Nome, BC001018_n384FuncoesAPFAtributos_Nome, BC001018_A385FuncoesAPFAtributos_Descricao, BC001018_n385FuncoesAPFAtributos_Descricao, BC001018_A751FuncaoAPFAtributos_Ativo, BC001018_n751FuncaoAPFAtributos_Ativo, BC001018_A165FuncaoAPF_Codigo,
               BC001018_A364FuncaoAPFAtributos_AtributosCod, BC001018_A748FuncaoAPFAtributos_MelhoraCod, BC001018_n748FuncaoAPFAtributos_MelhoraCod, BC001018_A378FuncaoAPFAtributos_FuncaoDadosCod, BC001018_n378FuncaoAPFAtributos_FuncaoDadosCod, BC001018_A366FuncaoAPFAtributos_AtrTabelaCod, BC001018_n366FuncaoAPFAtributos_AtrTabelaCod, BC001018_A393FuncaoAPFAtributos_SistemaCod, BC001018_n393FuncaoAPFAtributos_SistemaCod
               }
            }
         );
         AV15Pgmname = "FuncoesAPFAtributos_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12102 */
         E12102 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound58 ;
      private int trnEnded ;
      private int Z165FuncaoAPF_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int Z364FuncaoAPFAtributos_AtributosCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int AV16GXV1 ;
      private int AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod ;
      private int AV14Insert_FuncaoAPFAtributos_MelhoraCod ;
      private int Z748FuncaoAPFAtributos_MelhoraCod ;
      private int A748FuncaoAPFAtributos_MelhoraCod ;
      private int Z378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int Z366FuncaoAPFAtributos_AtrTabelaCod ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int Z393FuncaoAPFAtributos_SistemaCod ;
      private int A393FuncaoAPFAtributos_SistemaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV15Pgmname ;
      private String Z384FuncoesAPFAtributos_Nome ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String Z365FuncaoAPFAtributos_AtributosNom ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String Z415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String Z367FuncaoAPFAtributos_AtrTabelaNom ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String sMode58 ;
      private bool Z389FuncoesAPFAtributos_Regra ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool Z751FuncaoAPFAtributos_Ativo ;
      private bool A751FuncaoAPFAtributos_Ativo ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n751FuncaoAPFAtributos_Ativo ;
      private bool n748FuncaoAPFAtributos_MelhoraCod ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n393FuncaoAPFAtributos_SistemaCod ;
      private bool Gx_longc ;
      private String Z383FuncoesAPFAtributos_Code ;
      private String A383FuncoesAPFAtributos_Code ;
      private String Z385FuncoesAPFAtributos_Descricao ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String Z166FuncaoAPF_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String Z412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private IGxSession AV11WebSession ;
      private SdtFuncoesAPFAtributos bcFuncoesAPFAtributos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00109_A166FuncaoAPF_Nome ;
      private String[] BC00109_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] BC00109_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] BC00109_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] BC00109_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] BC00109_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] BC00109_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String[] BC00109_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC00109_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC00109_A389FuncoesAPFAtributos_Regra ;
      private bool[] BC00109_n389FuncoesAPFAtributos_Regra ;
      private String[] BC00109_A383FuncoesAPFAtributos_Code ;
      private bool[] BC00109_n383FuncoesAPFAtributos_Code ;
      private String[] BC00109_A384FuncoesAPFAtributos_Nome ;
      private bool[] BC00109_n384FuncoesAPFAtributos_Nome ;
      private String[] BC00109_A385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00109_n385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00109_A751FuncaoAPFAtributos_Ativo ;
      private bool[] BC00109_n751FuncaoAPFAtributos_Ativo ;
      private int[] BC00109_A165FuncaoAPF_Codigo ;
      private int[] BC00109_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC00109_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] BC00109_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] BC00109_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] BC00109_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] BC00109_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] BC00109_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] BC00109_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] BC00109_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] BC00104_A166FuncaoAPF_Nome ;
      private String[] BC00105_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] BC00105_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] BC00105_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] BC00105_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] BC00108_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC00108_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] BC00108_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] BC00108_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] BC00107_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] BC00107_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] BC00107_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] BC00107_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private int[] BC00106_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] BC00106_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] BC001010_A165FuncaoAPF_Codigo ;
      private int[] BC001010_A364FuncaoAPFAtributos_AtributosCod ;
      private bool[] BC00103_A389FuncoesAPFAtributos_Regra ;
      private bool[] BC00103_n389FuncoesAPFAtributos_Regra ;
      private String[] BC00103_A383FuncoesAPFAtributos_Code ;
      private bool[] BC00103_n383FuncoesAPFAtributos_Code ;
      private String[] BC00103_A384FuncoesAPFAtributos_Nome ;
      private bool[] BC00103_n384FuncoesAPFAtributos_Nome ;
      private String[] BC00103_A385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00103_n385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00103_A751FuncaoAPFAtributos_Ativo ;
      private bool[] BC00103_n751FuncaoAPFAtributos_Ativo ;
      private int[] BC00103_A165FuncaoAPF_Codigo ;
      private int[] BC00103_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC00103_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] BC00103_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] BC00103_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] BC00103_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] BC00102_A389FuncoesAPFAtributos_Regra ;
      private bool[] BC00102_n389FuncoesAPFAtributos_Regra ;
      private String[] BC00102_A383FuncoesAPFAtributos_Code ;
      private bool[] BC00102_n383FuncoesAPFAtributos_Code ;
      private String[] BC00102_A384FuncoesAPFAtributos_Nome ;
      private bool[] BC00102_n384FuncoesAPFAtributos_Nome ;
      private String[] BC00102_A385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00102_n385FuncoesAPFAtributos_Descricao ;
      private bool[] BC00102_A751FuncaoAPFAtributos_Ativo ;
      private bool[] BC00102_n751FuncaoAPFAtributos_Ativo ;
      private int[] BC00102_A165FuncaoAPF_Codigo ;
      private int[] BC00102_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC00102_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] BC00102_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] BC00102_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] BC00102_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] BC001014_A166FuncaoAPF_Nome ;
      private String[] BC001015_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] BC001015_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] BC001015_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] BC001015_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] BC001016_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC001016_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] BC001016_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] BC001016_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] BC001017_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] BC001017_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] BC001017_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] BC001017_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String[] BC001018_A166FuncaoAPF_Nome ;
      private String[] BC001018_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] BC001018_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] BC001018_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] BC001018_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] BC001018_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] BC001018_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String[] BC001018_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC001018_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] BC001018_A389FuncoesAPFAtributos_Regra ;
      private bool[] BC001018_n389FuncoesAPFAtributos_Regra ;
      private String[] BC001018_A383FuncoesAPFAtributos_Code ;
      private bool[] BC001018_n383FuncoesAPFAtributos_Code ;
      private String[] BC001018_A384FuncoesAPFAtributos_Nome ;
      private bool[] BC001018_n384FuncoesAPFAtributos_Nome ;
      private String[] BC001018_A385FuncoesAPFAtributos_Descricao ;
      private bool[] BC001018_n385FuncoesAPFAtributos_Descricao ;
      private bool[] BC001018_A751FuncaoAPFAtributos_Ativo ;
      private bool[] BC001018_n751FuncaoAPFAtributos_Ativo ;
      private int[] BC001018_A165FuncaoAPF_Codigo ;
      private int[] BC001018_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] BC001018_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] BC001018_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] BC001018_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] BC001018_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] BC001018_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] BC001018_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] BC001018_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] BC001018_n393FuncaoAPFAtributos_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class funcoesapfatributos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00109 ;
          prmBC00109 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00104 ;
          prmBC00104 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00105 ;
          prmBC00105 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00108 ;
          prmBC00108 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00107 ;
          prmBC00107 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00106 ;
          prmBC00106 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001010 ;
          prmBC001010 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00103 ;
          prmBC00103 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00102 ;
          prmBC00102 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001011 ;
          prmBC001011 = new Object[] {
          new Object[] {"@FuncoesAPFAtributos_Regra",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@FuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@FuncaoAPFAtributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001012 ;
          prmBC001012 = new Object[] {
          new Object[] {"@FuncoesAPFAtributos_Regra",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@FuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@FuncaoAPFAtributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001013 ;
          prmBC001013 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001017 ;
          prmBC001017 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001018 ;
          prmBC001018 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001014 ;
          prmBC001014 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001015 ;
          prmBC001015 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001016 ;
          prmBC001016 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00102", "SELECT [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, [FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod FROM [FuncoesAPFAtributos] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00102,1,0,true,false )
             ,new CursorDef("BC00103", "SELECT [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, [FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00103,1,0,true,false )
             ,new CursorDef("BC00104", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00104,1,0,true,false )
             ,new CursorDef("BC00105", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00105,1,0,true,false )
             ,new CursorDef("BC00106", "SELECT [Atributos_Codigo] AS FuncaoAPFAtributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00106,1,0,true,false )
             ,new CursorDef("BC00107", "SELECT [FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, [FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00107,1,0,true,false )
             ,new CursorDef("BC00108", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, [Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00108,1,0,true,false )
             ,new CursorDef("BC00109", "SELECT T2.[FuncaoAPF_Nome], T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T5.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T5.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, TM1.[FuncoesAPFAtributos_Regra], TM1.[FuncoesAPFAtributos_Code], TM1.[FuncoesAPFAtributos_Nome], TM1.[FuncoesAPFAtributos_Descricao], TM1.[FuncaoAPFAtributos_Ativo], TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, TM1.[FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, TM1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T4.[Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM (((([FuncoesAPFAtributos] TM1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = TM1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod]) LEFT JOIN [FuncaoDados] T5 WITH (NOLOCK) ON T5.[FuncaoDados_Codigo] = TM1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and TM1.[FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ORDER BY TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00109,100,0,true,false )
             ,new CursorDef("BC001010", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001010,1,0,true,false )
             ,new CursorDef("BC001011", "INSERT INTO [FuncoesAPFAtributos]([FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod], [FuncaoAPFAtributos_MelhoraCod], [FuncaoAPFAtributos_FuncaoDadosCod]) VALUES(@FuncoesAPFAtributos_Regra, @FuncoesAPFAtributos_Code, @FuncoesAPFAtributos_Nome, @FuncoesAPFAtributos_Descricao, @FuncaoAPFAtributos_Ativo, @FuncaoAPF_Codigo, @FuncaoAPFAtributos_AtributosCod, @FuncaoAPFAtributos_MelhoraCod, @FuncaoAPFAtributos_FuncaoDadosCod)", GxErrorMask.GX_NOMASK,prmBC001011)
             ,new CursorDef("BC001012", "UPDATE [FuncoesAPFAtributos] SET [FuncoesAPFAtributos_Regra]=@FuncoesAPFAtributos_Regra, [FuncoesAPFAtributos_Code]=@FuncoesAPFAtributos_Code, [FuncoesAPFAtributos_Nome]=@FuncoesAPFAtributos_Nome, [FuncoesAPFAtributos_Descricao]=@FuncoesAPFAtributos_Descricao, [FuncaoAPFAtributos_Ativo]=@FuncaoAPFAtributos_Ativo, [FuncaoAPFAtributos_MelhoraCod]=@FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod]=@FuncaoAPFAtributos_FuncaoDadosCod  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK,prmBC001012)
             ,new CursorDef("BC001013", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK,prmBC001013)
             ,new CursorDef("BC001014", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001014,1,0,true,false )
             ,new CursorDef("BC001015", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001015,1,0,true,false )
             ,new CursorDef("BC001016", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, [Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001016,1,0,true,false )
             ,new CursorDef("BC001017", "SELECT [FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, [FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001017,1,0,true,false )
             ,new CursorDef("BC001018", "SELECT T2.[FuncaoAPF_Nome], T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T5.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T5.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, TM1.[FuncoesAPFAtributos_Regra], TM1.[FuncoesAPFAtributos_Code], TM1.[FuncoesAPFAtributos_Nome], TM1.[FuncoesAPFAtributos_Descricao], TM1.[FuncaoAPFAtributos_Ativo], TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, TM1.[FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, TM1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T4.[Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM (((([FuncoesAPFAtributos] TM1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = TM1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod]) LEFT JOIN [FuncaoDados] T5 WITH (NOLOCK) ON T5.[FuncaoDados_Codigo] = TM1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and TM1.[FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ORDER BY TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001018,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                stmt.SetParameter(9, (int)parms[15]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
