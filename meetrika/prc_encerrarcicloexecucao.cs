/*
               File: PRC_EncerrarCicloExecucao
        Description: Encerrar Ciclo de Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:33.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_encerrarcicloexecucao : GXProcedure
   {
      public prc_encerrarcicloexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_encerrarcicloexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_DateTime )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9DateTime = aP1_DateTime;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_DateTime )
      {
         prc_encerrarcicloexecucao objprc_encerrarcicloexecucao;
         objprc_encerrarcicloexecucao = new prc_encerrarcicloexecucao();
         objprc_encerrarcicloexecucao.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_encerrarcicloexecucao.AV9DateTime = aP1_DateTime;
         objprc_encerrarcicloexecucao.context.SetSubmitInitialConfig(context);
         objprc_encerrarcicloexecucao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_encerrarcicloexecucao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_encerrarcicloexecucao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009V2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1797LogResponsavel_Codigo = P009V2_A1797LogResponsavel_Codigo[0];
            A894LogResponsavel_Acao = P009V2_A894LogResponsavel_Acao[0];
            A893LogResponsavel_DataHora = P009V2_A893LogResponsavel_DataHora[0];
            A896LogResponsavel_Owner = P009V2_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P009V2_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P009V2_n892LogResponsavel_DemandaCod[0];
            GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
            if ( A1149LogResponsavel_OwnerEhContratante )
            {
               AV12DataRejeicao = A893LogResponsavel_DataHora;
               AV16GXLvl8 = 0;
               /* Using cursor P009V3 */
               pr_default.execute(1, new Object[] {A1797LogResponsavel_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A2026ContagemResultadoNaoCnf_NaoCnfCod = P009V3_A2026ContagemResultadoNaoCnf_NaoCnfCod[0];
                  A2023ContagemResultadoNaoCnf_LogRspCod = P009V3_A2023ContagemResultadoNaoCnf_LogRspCod[0];
                  A2031ContagemResultadoNaoCnf_Glosavel = P009V3_A2031ContagemResultadoNaoCnf_Glosavel[0];
                  n2031ContagemResultadoNaoCnf_Glosavel = P009V3_n2031ContagemResultadoNaoCnf_Glosavel[0];
                  A2024ContagemResultadoNaoCnf_Codigo = P009V3_A2024ContagemResultadoNaoCnf_Codigo[0];
                  A2031ContagemResultadoNaoCnf_Glosavel = P009V3_A2031ContagemResultadoNaoCnf_Glosavel[0];
                  n2031ContagemResultadoNaoCnf_Glosavel = P009V3_n2031ContagemResultadoNaoCnf_Glosavel[0];
                  AV16GXLvl8 = 1;
                  AV10EhExtensao = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV16GXLvl8 == 0 )
               {
                  AV10EhExtensao = true;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV10EhExtensao )
         {
            /* Using cursor P009V4 */
            pr_default.execute(2, new Object[] {AV8ContagemResultado_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009V4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009V4_n1553ContagemResultado_CntSrvCod[0];
               A1407ContagemResultadoExecucao_Fim = P009V4_A1407ContagemResultadoExecucao_Fim[0];
               n1407ContagemResultadoExecucao_Fim = P009V4_n1407ContagemResultadoExecucao_Fim[0];
               A1404ContagemResultadoExecucao_OSCod = P009V4_A1404ContagemResultadoExecucao_OSCod[0];
               A1611ContagemResultado_PrzTpDias = P009V4_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009V4_n1611ContagemResultado_PrzTpDias[0];
               A1406ContagemResultadoExecucao_Inicio = P009V4_A1406ContagemResultadoExecucao_Inicio[0];
               A1410ContagemResultadoExecucao_Dias = P009V4_A1410ContagemResultadoExecucao_Dias[0];
               n1410ContagemResultadoExecucao_Dias = P009V4_n1410ContagemResultadoExecucao_Dias[0];
               A1405ContagemResultadoExecucao_Codigo = P009V4_A1405ContagemResultadoExecucao_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P009V4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009V4_n1553ContagemResultado_CntSrvCod[0];
               A1611ContagemResultado_PrzTpDias = P009V4_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009V4_n1611ContagemResultado_PrzTpDias[0];
               GXt_int2 = AV11Dias;
               new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( AV9DateTime),  A1611ContagemResultado_PrzTpDias, out  GXt_int2) ;
               AV11Dias = GXt_int2;
               AV11Dias = (short)(AV11Dias+1);
               A1407ContagemResultadoExecucao_Fim = AV9DateTime;
               n1407ContagemResultadoExecucao_Fim = false;
               A1410ContagemResultadoExecucao_Dias = AV11Dias;
               n1410ContagemResultadoExecucao_Dias = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P009V5 */
               pr_default.execute(3, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
               if (true) break;
               /* Using cursor P009V6 */
               pr_default.execute(4, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         else
         {
            /* Using cursor P009V7 */
            pr_default.execute(5, new Object[] {AV8ContagemResultado_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009V7_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009V7_n1553ContagemResultado_CntSrvCod[0];
               A1407ContagemResultadoExecucao_Fim = P009V7_A1407ContagemResultadoExecucao_Fim[0];
               n1407ContagemResultadoExecucao_Fim = P009V7_n1407ContagemResultadoExecucao_Fim[0];
               A1404ContagemResultadoExecucao_OSCod = P009V7_A1404ContagemResultadoExecucao_OSCod[0];
               A1611ContagemResultado_PrzTpDias = P009V7_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009V7_n1611ContagemResultado_PrzTpDias[0];
               A1406ContagemResultadoExecucao_Inicio = P009V7_A1406ContagemResultadoExecucao_Inicio[0];
               A1410ContagemResultadoExecucao_Dias = P009V7_A1410ContagemResultadoExecucao_Dias[0];
               n1410ContagemResultadoExecucao_Dias = P009V7_n1410ContagemResultadoExecucao_Dias[0];
               A1405ContagemResultadoExecucao_Codigo = P009V7_A1405ContagemResultadoExecucao_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P009V7_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009V7_n1553ContagemResultado_CntSrvCod[0];
               A1611ContagemResultado_PrzTpDias = P009V7_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009V7_n1611ContagemResultado_PrzTpDias[0];
               A1407ContagemResultadoExecucao_Fim = AV9DateTime;
               n1407ContagemResultadoExecucao_Fim = false;
               GXt_int2 = A1410ContagemResultadoExecucao_Dias;
               new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  A1611ContagemResultado_PrzTpDias, out  GXt_int2) ;
               A1410ContagemResultadoExecucao_Dias = GXt_int2;
               n1410ContagemResultadoExecucao_Dias = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P009V8 */
               pr_default.execute(6, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
               if (true) break;
               /* Using cursor P009V9 */
               pr_default.execute(7, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009V2_A1797LogResponsavel_Codigo = new long[1] ;
         P009V2_A894LogResponsavel_Acao = new String[] {""} ;
         P009V2_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P009V2_A896LogResponsavel_Owner = new int[1] ;
         P009V2_A892LogResponsavel_DemandaCod = new int[1] ;
         P009V2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A894LogResponsavel_Acao = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         AV12DataRejeicao = (DateTime)(DateTime.MinValue);
         P009V3_A2026ContagemResultadoNaoCnf_NaoCnfCod = new int[1] ;
         P009V3_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         P009V3_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         P009V3_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         P009V3_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         P009V4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009V4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009V4_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P009V4_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P009V4_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P009V4_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P009V4_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P009V4_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P009V4_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P009V4_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P009V4_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         P009V7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009V7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009V7_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P009V7_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P009V7_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P009V7_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P009V7_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P009V7_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P009V7_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P009V7_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P009V7_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_encerrarcicloexecucao__default(),
            new Object[][] {
                new Object[] {
               P009V2_A1797LogResponsavel_Codigo, P009V2_A894LogResponsavel_Acao, P009V2_A893LogResponsavel_DataHora, P009V2_A896LogResponsavel_Owner, P009V2_A892LogResponsavel_DemandaCod, P009V2_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P009V3_A2026ContagemResultadoNaoCnf_NaoCnfCod, P009V3_A2023ContagemResultadoNaoCnf_LogRspCod, P009V3_A2031ContagemResultadoNaoCnf_Glosavel, P009V3_n2031ContagemResultadoNaoCnf_Glosavel, P009V3_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               P009V4_A1553ContagemResultado_CntSrvCod, P009V4_n1553ContagemResultado_CntSrvCod, P009V4_A1407ContagemResultadoExecucao_Fim, P009V4_n1407ContagemResultadoExecucao_Fim, P009V4_A1404ContagemResultadoExecucao_OSCod, P009V4_A1611ContagemResultado_PrzTpDias, P009V4_n1611ContagemResultado_PrzTpDias, P009V4_A1406ContagemResultadoExecucao_Inicio, P009V4_A1410ContagemResultadoExecucao_Dias, P009V4_n1410ContagemResultadoExecucao_Dias,
               P009V4_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P009V7_A1553ContagemResultado_CntSrvCod, P009V7_n1553ContagemResultado_CntSrvCod, P009V7_A1407ContagemResultadoExecucao_Fim, P009V7_n1407ContagemResultadoExecucao_Fim, P009V7_A1404ContagemResultadoExecucao_OSCod, P009V7_A1611ContagemResultado_PrzTpDias, P009V7_n1611ContagemResultado_PrzTpDias, P009V7_A1406ContagemResultadoExecucao_Inicio, P009V7_A1410ContagemResultadoExecucao_Dias, P009V7_n1410ContagemResultadoExecucao_Dias,
               P009V7_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16GXLvl8 ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short AV11Dias ;
      private short GXt_int2 ;
      private int AV8ContagemResultado_Codigo ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private long A2023ContagemResultadoNaoCnf_LogRspCod ;
      private String scmdbuf ;
      private String A894LogResponsavel_Acao ;
      private String A1611ContagemResultado_PrzTpDias ;
      private DateTime AV9DateTime ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime AV12DataRejeicao ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private bool A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool n2031ContagemResultadoNaoCnf_Glosavel ;
      private bool AV10EhExtensao ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] P009V2_A1797LogResponsavel_Codigo ;
      private String[] P009V2_A894LogResponsavel_Acao ;
      private DateTime[] P009V2_A893LogResponsavel_DataHora ;
      private int[] P009V2_A896LogResponsavel_Owner ;
      private int[] P009V2_A892LogResponsavel_DemandaCod ;
      private bool[] P009V2_n892LogResponsavel_DemandaCod ;
      private int[] P009V3_A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private long[] P009V3_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private bool[] P009V3_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] P009V3_n2031ContagemResultadoNaoCnf_Glosavel ;
      private int[] P009V3_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] P009V4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009V4_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P009V4_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P009V4_n1407ContagemResultadoExecucao_Fim ;
      private int[] P009V4_A1404ContagemResultadoExecucao_OSCod ;
      private String[] P009V4_A1611ContagemResultado_PrzTpDias ;
      private bool[] P009V4_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P009V4_A1406ContagemResultadoExecucao_Inicio ;
      private short[] P009V4_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P009V4_n1410ContagemResultadoExecucao_Dias ;
      private int[] P009V4_A1405ContagemResultadoExecucao_Codigo ;
      private int[] P009V7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009V7_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P009V7_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P009V7_n1407ContagemResultadoExecucao_Fim ;
      private int[] P009V7_A1404ContagemResultadoExecucao_OSCod ;
      private String[] P009V7_A1611ContagemResultado_PrzTpDias ;
      private bool[] P009V7_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P009V7_A1406ContagemResultadoExecucao_Inicio ;
      private short[] P009V7_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P009V7_n1410ContagemResultadoExecucao_Dias ;
      private int[] P009V7_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_encerrarcicloexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009V2 ;
          prmP009V2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V3 ;
          prmP009V3 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP009V4 ;
          prmP009V4 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V5 ;
          prmP009V5 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V6 ;
          prmP009V6 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V7 ;
          prmP009V7 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V8 ;
          prmP009V8 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009V9 ;
          prmP009V9 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009V2", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Acao], [LogResponsavel_DataHora], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV8ContagemResultado_Codigo) AND ([LogResponsavel_Acao] = 'R') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009V2,100,0,true,false )
             ,new CursorDef("P009V3", "SELECT TOP 1 T1.[ContagemResultadoNaoCnf_NaoCnfCod] AS ContagemResultadoNaoCnf_NaoCnfCod, T1.[ContagemResultadoNaoCnf_LogRspCod], T2.[NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel, T1.[ContagemResultadoNaoCnf_Codigo] FROM ([ContagemResultadoNaoCnf] T1 WITH (NOLOCK) INNER JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContagemResultadoNaoCnf_NaoCnfCod]) WHERE (T1.[ContagemResultadoNaoCnf_LogRspCod] = @LogResponsavel_Codigo) AND (T2.[NaoConformidade_Glosavel] = 1) ORDER BY T1.[ContagemResultadoNaoCnf_LogRspCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009V3,1,0,false,true )
             ,new CursorDef("P009V4", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_Codigo] FROM (([ContagemResultadoExecucao] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultadoExecucao_OSCod] = @AV8ContagemResultado_Codigo) AND ((T1.[ContagemResultadoExecucao_Fim] = convert( DATETIME, '17530101', 112 ))) ORDER BY T1.[ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009V4,1,0,true,true )
             ,new CursorDef("P009V5", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009V5)
             ,new CursorDef("P009V6", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009V6)
             ,new CursorDef("P009V7", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_Codigo] FROM (([ContagemResultadoExecucao] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultadoExecucao_OSCod] = @AV8ContagemResultado_Codigo) AND ((T1.[ContagemResultadoExecucao_Fim] = convert( DATETIME, '17530101', 112 ))) ORDER BY T1.[ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009V7,1,0,true,true )
             ,new CursorDef("P009V8", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009V8)
             ,new CursorDef("P009V9", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009V9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
