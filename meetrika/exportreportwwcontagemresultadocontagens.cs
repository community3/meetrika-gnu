/*
               File: ExportReportWWContagemResultadoContagens
        Description: Stub for ExportReportWWContagemResultadoContagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:0.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportwwcontagemresultadocontagens : GXProcedure
   {
      public exportreportwwcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public exportreportwwcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TFContratada_AreaTrabalhoDes ,
                           String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP2_TFContagemResultado_DataDmn ,
                           DateTime aP3_TFContagemResultado_DataDmn_To ,
                           DateTime aP4_TFContagemResultado_DataCnt ,
                           DateTime aP5_TFContagemResultado_DataCnt_To ,
                           String aP6_TFContagemResultado_HoraCnt ,
                           String aP7_TFContagemResultado_HoraCnt_Sel ,
                           String aP8_TFContagemResultado_OsFsOsFm ,
                           String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP10_TFContagemrResultado_SistemaSigla ,
                           String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP12_TFContagemResultado_ContadorFMNom ,
                           String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                           String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                           short aP15_TFContagemResultadoContagens_Esforco ,
                           short aP16_TFContagemResultadoContagens_Esforco_To ,
                           decimal aP17_TFContagemResultado_PFBFS ,
                           decimal aP18_TFContagemResultado_PFBFS_To ,
                           decimal aP19_TFContagemResultado_PFLFS ,
                           decimal aP20_TFContagemResultado_PFLFS_To ,
                           decimal aP21_TFContagemResultado_PFBFM ,
                           decimal aP22_TFContagemResultado_PFBFM_To ,
                           decimal aP23_TFContagemResultado_PFLFM ,
                           decimal aP24_TFContagemResultado_PFLFM_To ,
                           short aP25_OrderedBy ,
                           bool aP26_OrderedDsc ,
                           String aP27_GridStateXML )
      {
         this.AV2TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         this.AV3TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         this.AV4TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         this.AV5TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         this.AV6TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         this.AV7TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         this.AV8TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         this.AV9TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         this.AV10TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV11TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV12TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         this.AV13TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         this.AV14TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         this.AV15TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         this.AV16TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         this.AV17TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         this.AV18TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         this.AV19TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         this.AV20TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         this.AV21TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         this.AV22TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         this.AV23TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         this.AV24TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         this.AV25TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         this.AV26TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         this.AV27OrderedBy = aP25_OrderedBy;
         this.AV28OrderedDsc = aP26_OrderedDsc;
         this.AV29GridStateXML = aP27_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_TFContratada_AreaTrabalhoDes ,
                                 String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP2_TFContagemResultado_DataDmn ,
                                 DateTime aP3_TFContagemResultado_DataDmn_To ,
                                 DateTime aP4_TFContagemResultado_DataCnt ,
                                 DateTime aP5_TFContagemResultado_DataCnt_To ,
                                 String aP6_TFContagemResultado_HoraCnt ,
                                 String aP7_TFContagemResultado_HoraCnt_Sel ,
                                 String aP8_TFContagemResultado_OsFsOsFm ,
                                 String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP10_TFContagemrResultado_SistemaSigla ,
                                 String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP12_TFContagemResultado_ContadorFMNom ,
                                 String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                                 String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                                 short aP15_TFContagemResultadoContagens_Esforco ,
                                 short aP16_TFContagemResultadoContagens_Esforco_To ,
                                 decimal aP17_TFContagemResultado_PFBFS ,
                                 decimal aP18_TFContagemResultado_PFBFS_To ,
                                 decimal aP19_TFContagemResultado_PFLFS ,
                                 decimal aP20_TFContagemResultado_PFLFS_To ,
                                 decimal aP21_TFContagemResultado_PFBFM ,
                                 decimal aP22_TFContagemResultado_PFBFM_To ,
                                 decimal aP23_TFContagemResultado_PFLFM ,
                                 decimal aP24_TFContagemResultado_PFLFM_To ,
                                 short aP25_OrderedBy ,
                                 bool aP26_OrderedDsc ,
                                 String aP27_GridStateXML )
      {
         exportreportwwcontagemresultadocontagens objexportreportwwcontagemresultadocontagens;
         objexportreportwwcontagemresultadocontagens = new exportreportwwcontagemresultadocontagens();
         objexportreportwwcontagemresultadocontagens.AV2TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         objexportreportwwcontagemresultadocontagens.AV3TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         objexportreportwwcontagemresultadocontagens.AV4TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         objexportreportwwcontagemresultadocontagens.AV5TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         objexportreportwwcontagemresultadocontagens.AV6TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         objexportreportwwcontagemresultadocontagens.AV7TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         objexportreportwwcontagemresultadocontagens.AV8TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         objexportreportwwcontagemresultadocontagens.AV9TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         objexportreportwwcontagemresultadocontagens.AV10TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         objexportreportwwcontagemresultadocontagens.AV11TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         objexportreportwwcontagemresultadocontagens.AV12TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         objexportreportwwcontagemresultadocontagens.AV13TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         objexportreportwwcontagemresultadocontagens.AV14TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         objexportreportwwcontagemresultadocontagens.AV15TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         objexportreportwwcontagemresultadocontagens.AV16TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         objexportreportwwcontagemresultadocontagens.AV17TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         objexportreportwwcontagemresultadocontagens.AV18TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         objexportreportwwcontagemresultadocontagens.AV19TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         objexportreportwwcontagemresultadocontagens.AV20TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         objexportreportwwcontagemresultadocontagens.AV21TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         objexportreportwwcontagemresultadocontagens.AV22TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         objexportreportwwcontagemresultadocontagens.AV23TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         objexportreportwwcontagemresultadocontagens.AV24TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         objexportreportwwcontagemresultadocontagens.AV25TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         objexportreportwwcontagemresultadocontagens.AV26TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         objexportreportwwcontagemresultadocontagens.AV27OrderedBy = aP25_OrderedBy;
         objexportreportwwcontagemresultadocontagens.AV28OrderedDsc = aP26_OrderedDsc;
         objexportreportwwcontagemresultadocontagens.AV29GridStateXML = aP27_GridStateXML;
         objexportreportwwcontagemresultadocontagens.context.SetSubmitInitialConfig(context);
         objexportreportwwcontagemresultadocontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportwwcontagemresultadocontagens);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportwwcontagemresultadocontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2TFContratada_AreaTrabalhoDes,(String)AV3TFContratada_AreaTrabalhoDes_Sel,(DateTime)AV4TFContagemResultado_DataDmn,(DateTime)AV5TFContagemResultado_DataDmn_To,(DateTime)AV6TFContagemResultado_DataCnt,(DateTime)AV7TFContagemResultado_DataCnt_To,(String)AV8TFContagemResultado_HoraCnt,(String)AV9TFContagemResultado_HoraCnt_Sel,(String)AV10TFContagemResultado_OsFsOsFm,(String)AV11TFContagemResultado_OsFsOsFm_Sel,(String)AV12TFContagemrResultado_SistemaSigla,(String)AV13TFContagemrResultado_SistemaSigla_Sel,(String)AV14TFContagemResultado_ContadorFMNom,(String)AV15TFContagemResultado_ContadorFMNom_Sel,(String)AV16TFContagemResultado_StatusCnt_SelsJson,(short)AV17TFContagemResultadoContagens_Esforco,(short)AV18TFContagemResultadoContagens_Esforco_To,(decimal)AV19TFContagemResultado_PFBFS,(decimal)AV20TFContagemResultado_PFBFS_To,(decimal)AV21TFContagemResultado_PFLFS,(decimal)AV22TFContagemResultado_PFLFS_To,(decimal)AV23TFContagemResultado_PFBFM,(decimal)AV24TFContagemResultado_PFBFM_To,(decimal)AV25TFContagemResultado_PFLFM,(decimal)AV26TFContagemResultado_PFLFM_To,(short)AV27OrderedBy,(bool)AV28OrderedDsc,(String)AV29GridStateXML} ;
         ClassLoader.Execute("aexportreportwwcontagemresultadocontagens","GeneXus.Programs.aexportreportwwcontagemresultadocontagens", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 28 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17TFContagemResultadoContagens_Esforco ;
      private short AV18TFContagemResultadoContagens_Esforco_To ;
      private short AV27OrderedBy ;
      private decimal AV19TFContagemResultado_PFBFS ;
      private decimal AV20TFContagemResultado_PFBFS_To ;
      private decimal AV21TFContagemResultado_PFLFS ;
      private decimal AV22TFContagemResultado_PFLFS_To ;
      private decimal AV23TFContagemResultado_PFBFM ;
      private decimal AV24TFContagemResultado_PFBFM_To ;
      private decimal AV25TFContagemResultado_PFLFM ;
      private decimal AV26TFContagemResultado_PFLFM_To ;
      private String AV8TFContagemResultado_HoraCnt ;
      private String AV9TFContagemResultado_HoraCnt_Sel ;
      private String AV12TFContagemrResultado_SistemaSigla ;
      private String AV13TFContagemrResultado_SistemaSigla_Sel ;
      private String AV14TFContagemResultado_ContadorFMNom ;
      private String AV15TFContagemResultado_ContadorFMNom_Sel ;
      private DateTime AV4TFContagemResultado_DataDmn ;
      private DateTime AV5TFContagemResultado_DataDmn_To ;
      private DateTime AV6TFContagemResultado_DataCnt ;
      private DateTime AV7TFContagemResultado_DataCnt_To ;
      private bool AV28OrderedDsc ;
      private String AV16TFContagemResultado_StatusCnt_SelsJson ;
      private String AV29GridStateXML ;
      private String AV2TFContratada_AreaTrabalhoDes ;
      private String AV3TFContratada_AreaTrabalhoDes_Sel ;
      private String AV10TFContagemResultado_OsFsOsFm ;
      private String AV11TFContagemResultado_OsFsOsFm_Sel ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
