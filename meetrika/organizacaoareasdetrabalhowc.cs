/*
               File: OrganizacaoAreasDeTrabalhoWC
        Description: Organizacao Areas De Trabalho WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:27.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class organizacaoareasdetrabalhowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public organizacaoareasdetrabalhowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public organizacaoareasdetrabalhowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_OrganizacaoCod )
      {
         this.AV7AreaTrabalho_OrganizacaoCod = aP0_AreaTrabalho_OrganizacaoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7AreaTrabalho_OrganizacaoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV16TFAreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
                  AV17TFAreaTrabalho_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFAreaTrabalho_Descricao_Sel", AV17TFAreaTrabalho_Descricao_Sel);
                  AV7AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0)));
                  AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
                  AV25Pgmname = GetNextPar( );
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAIW2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV25Pgmname = "OrganizacaoAreasDeTrabalhoWC";
               context.Gx_err = 0;
               WSIW2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Organizacao Areas De Trabalho WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762722");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("organizacaoareasdetrabalhowc.aspx") + "?" + UrlEncode("" +AV7AreaTrabalho_OrganizacaoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO", AV16TFAreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO_SEL", AV17TFAreaTrabalho_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV19DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV19DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAREATRABALHO_DESCRICAOTITLEFILTERDATA", AV15AreaTrabalho_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAREATRABALHO_DESCRICAOTITLEFILTERDATA", AV15AreaTrabalho_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAREATRABALHO_ORGANIZACAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV25Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_areatrabalho_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_areatrabalho_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_areatrabalho_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_areatrabalho_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AREATRABALHO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIW2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("organizacaoareasdetrabalhowc.js", "?2020311762769");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "OrganizacaoAreasDeTrabalhoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Organizacao Areas De Trabalho WC" ;
      }

      protected void WBIW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "organizacaoareasdetrabalhowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_IW2( true) ;
         }
         else
         {
            wb_table1_2_IW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_OrganizacaoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_OrganizacaoCod_Jsonclick, 0, "Attribute", "", "", "", edtAreaTrabalho_OrganizacaoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_OrganizacaoAreasDeTrabalhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_OrganizacaoAreasDeTrabalhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_descricao_Internalname, AV16TFAreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( AV16TFAreaTrabalho_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,16);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_OrganizacaoAreasDeTrabalhoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_descricao_sel_Internalname, AV17TFAreaTrabalho_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV17TFAreaTrabalho_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,17);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_OrganizacaoAreasDeTrabalhoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AREATRABALHO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", 0, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_OrganizacaoAreasDeTrabalhoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTIW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Organizacao Areas De Trabalho WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPIW0( ) ;
            }
         }
      }

      protected void WSIW2( )
      {
         STARTIW2( ) ;
         EVTIW2( ) ;
      }

      protected void EVTIW2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIW0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIW0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11IW2 */
                                    E11IW2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIW0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12IW2 */
                                    E12IW2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIW0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrdereddsc_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIW0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13IW2 */
                                          E13IW2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14IW2 */
                                          E14IW2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15IW2 */
                                          E15IW2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfareatrabalho_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO"), AV16TFAreaTrabalho_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfareatrabalho_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO_SEL"), AV17TFAreaTrabalho_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPIW0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIW2( ) ;
            }
         }
      }

      protected void PAIW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrdereddsc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool AV13OrderedDsc ,
                                       String AV16TFAreaTrabalho_Descricao ,
                                       String AV17TFAreaTrabalho_Descricao_Sel ,
                                       int AV7AreaTrabalho_OrganizacaoCod ,
                                       String AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace ,
                                       String AV25Pgmname ,
                                       int A5AreaTrabalho_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV25Pgmname = "OrganizacaoAreasDeTrabalhoWC";
         context.Gx_err = 0;
      }

      protected void RFIW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E14IW2 */
         E14IW2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV17TFAreaTrabalho_Descricao_Sel ,
                                                 AV16TFAreaTrabalho_Descricao ,
                                                 A6AreaTrabalho_Descricao ,
                                                 AV13OrderedDsc ,
                                                 A1216AreaTrabalho_OrganizacaoCod ,
                                                 AV7AreaTrabalho_OrganizacaoCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV16TFAreaTrabalho_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFAreaTrabalho_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
            /* Using cursor H00IW2 */
            pr_default.execute(0, new Object[] {AV7AreaTrabalho_OrganizacaoCod, lV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A5AreaTrabalho_Codigo = H00IW2_A5AreaTrabalho_Codigo[0];
               A1216AreaTrabalho_OrganizacaoCod = H00IW2_A1216AreaTrabalho_OrganizacaoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
               n1216AreaTrabalho_OrganizacaoCod = H00IW2_n1216AreaTrabalho_OrganizacaoCod[0];
               A6AreaTrabalho_Descricao = H00IW2_A6AreaTrabalho_Descricao[0];
               /* Execute user event: E15IW2 */
               E15IW2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBIW0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV17TFAreaTrabalho_Descricao_Sel ,
                                              AV16TFAreaTrabalho_Descricao ,
                                              A6AreaTrabalho_Descricao ,
                                              AV13OrderedDsc ,
                                              A1216AreaTrabalho_OrganizacaoCod ,
                                              AV7AreaTrabalho_OrganizacaoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV16TFAreaTrabalho_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFAreaTrabalho_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
         /* Using cursor H00IW3 */
         pr_default.execute(1, new Object[] {AV7AreaTrabalho_OrganizacaoCod, lV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel});
         GRID_nRecordCount = H00IW3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFAreaTrabalho_Descricao, AV17TFAreaTrabalho_Descricao_Sel, AV7AreaTrabalho_OrganizacaoCod, AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV25Pgmname, A5AreaTrabalho_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIW0( )
      {
         /* Before Start, stand alone formulas. */
         AV25Pgmname = "OrganizacaoAreasDeTrabalhoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13IW2 */
         E13IW2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV19DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAREATRABALHO_DESCRICAOTITLEFILTERDATA"), AV15AreaTrabalho_DescricaoTitleFilterData);
            /* Read variables values. */
            A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_OrganizacaoCod_Internalname), ",", "."));
            n1216AreaTrabalho_OrganizacaoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            AV16TFAreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavTfareatrabalho_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
            AV17TFAreaTrabalho_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfareatrabalho_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFAreaTrabalho_Descricao_Sel", AV17TFAreaTrabalho_Descricao_Sel);
            AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV21GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV22GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AreaTrabalho_OrganizacaoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_areatrabalho_descricao_Caption = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Caption");
            Ddo_areatrabalho_descricao_Tooltip = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Tooltip");
            Ddo_areatrabalho_descricao_Cls = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Cls");
            Ddo_areatrabalho_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filteredtext_set");
            Ddo_areatrabalho_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Selectedvalue_set");
            Ddo_areatrabalho_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Dropdownoptionstype");
            Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_areatrabalho_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includesortasc"));
            Ddo_areatrabalho_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includesortdsc"));
            Ddo_areatrabalho_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortedstatus");
            Ddo_areatrabalho_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includefilter"));
            Ddo_areatrabalho_descricao_Filtertype = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filtertype");
            Ddo_areatrabalho_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filterisrange"));
            Ddo_areatrabalho_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Includedatalist"));
            Ddo_areatrabalho_descricao_Datalisttype = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalisttype");
            Ddo_areatrabalho_descricao_Datalistproc = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalistproc");
            Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_areatrabalho_descricao_Sortasc = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortasc");
            Ddo_areatrabalho_descricao_Sortdsc = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Sortdsc");
            Ddo_areatrabalho_descricao_Loadingdata = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Loadingdata");
            Ddo_areatrabalho_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Cleanfilter");
            Ddo_areatrabalho_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Noresultsfound");
            Ddo_areatrabalho_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_areatrabalho_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Activeeventkey");
            Ddo_areatrabalho_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Filteredtext_get");
            Ddo_areatrabalho_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_AREATRABALHO_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO"), AV16TFAreaTrabalho_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAREATRABALHO_DESCRICAO_SEL"), AV17TFAreaTrabalho_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13IW2 */
         E13IW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13IW2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfareatrabalho_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfareatrabalho_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_descricao_Visible), 5, 0)));
         edtavTfareatrabalho_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfareatrabalho_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_descricao_sel_Visible), 5, 0)));
         Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_descricao_Titlecontrolidtoreplace);
         AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = Ddo_areatrabalho_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         edtAreaTrabalho_OrganizacaoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAreaTrabalho_OrganizacaoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_OrganizacaoCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV19DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV19DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E14IW2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV15AreaTrabalho_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAreaTrabalho_Descricao_Titleformat = 2;
         edtAreaTrabalho_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAreaTrabalho_Descricao_Internalname, "Title", edtAreaTrabalho_Descricao_Title);
         AV21GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GridCurrentPage), 10, 0)));
         AV22GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV15AreaTrabalho_DescricaoTitleFilterData", AV15AreaTrabalho_DescricaoTitleFilterData);
      }

      protected void E11IW2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV20PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV20PageToGo) ;
         }
      }

      protected void E12IW2( )
      {
         /* Ddo_areatrabalho_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_areatrabalho_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_areatrabalho_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV16TFAreaTrabalho_Descricao = Ddo_areatrabalho_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
            AV17TFAreaTrabalho_Descricao_Sel = Ddo_areatrabalho_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFAreaTrabalho_Descricao_Sel", AV17TFAreaTrabalho_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E15IW2( )
      {
         /* Grid_Load Routine */
         edtAreaTrabalho_Descricao_Link = formatLink("viewareatrabalho.aspx") + "?" + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_areatrabalho_descricao_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV25Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV25Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV25Pgmname+"GridState"), "");
         }
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26GXV1 = 1;
         while ( AV26GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV26GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO") == 0 )
            {
               AV16TFAreaTrabalho_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFAreaTrabalho_Descricao", AV16TFAreaTrabalho_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAreaTrabalho_Descricao)) )
               {
                  Ddo_areatrabalho_descricao_Filteredtext_set = AV16TFAreaTrabalho_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "FilteredText_set", Ddo_areatrabalho_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO_SEL") == 0 )
            {
               AV17TFAreaTrabalho_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFAreaTrabalho_Descricao_Sel", AV17TFAreaTrabalho_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) )
               {
                  Ddo_areatrabalho_descricao_Selectedvalue_set = AV17TFAreaTrabalho_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_areatrabalho_descricao_Internalname, "SelectedValue_set", Ddo_areatrabalho_descricao_Selectedvalue_set);
               }
            }
            AV26GXV1 = (int)(AV26GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV25Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAreaTrabalho_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV16TFAreaTrabalho_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV17TFAreaTrabalho_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7AreaTrabalho_OrganizacaoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&AREATRABALHO_ORGANIZACAOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV25Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV25Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "AreaTrabalho";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "AreaTrabalho_OrganizacaoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_IW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_IW2( true) ;
         }
         else
         {
            wb_table2_5_IW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_IW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IW2e( true) ;
         }
         else
         {
            wb_table1_2_IW2e( false) ;
         }
      }

      protected void wb_table2_5_IW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A6AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAreaTrabalho_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_IW2e( true) ;
         }
         else
         {
            wb_table2_5_IW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7AreaTrabalho_OrganizacaoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIW2( ) ;
         WSIW2( ) ;
         WEIW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7AreaTrabalho_OrganizacaoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAIW2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "organizacaoareasdetrabalhowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAIW2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7AreaTrabalho_OrganizacaoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0)));
         }
         wcpOAV7AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AreaTrabalho_OrganizacaoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7AreaTrabalho_OrganizacaoCod != wcpOAV7AreaTrabalho_OrganizacaoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7AreaTrabalho_OrganizacaoCod = AV7AreaTrabalho_OrganizacaoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7AreaTrabalho_OrganizacaoCod = cgiGet( sPrefix+"AV7AreaTrabalho_OrganizacaoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7AreaTrabalho_OrganizacaoCod) > 0 )
         {
            AV7AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7AreaTrabalho_OrganizacaoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0)));
         }
         else
         {
            AV7AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7AreaTrabalho_OrganizacaoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAIW2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSIW2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSIW2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7AreaTrabalho_OrganizacaoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7AreaTrabalho_OrganizacaoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7AreaTrabalho_OrganizacaoCod_CTRL", StringUtil.RTrim( sCtrlAV7AreaTrabalho_OrganizacaoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEIW2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311762889");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("organizacaoareasdetrabalhowc.js", "?2020311762889");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtAreaTrabalho_Descricao_Internalname = sPrefix+"AREATRABALHO_DESCRICAO_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtAreaTrabalho_Descricao_Internalname = sPrefix+"AREATRABALHO_DESCRICAO_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBIW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Descricao_Internalname,(String)A6AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtAreaTrabalho_Descricao_Link,(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AREATRABALHO_DESCRICAO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtAreaTrabalho_Descricao_Internalname = sPrefix+"AREATRABALHO_DESCRICAO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtAreaTrabalho_OrganizacaoCod_Internalname = sPrefix+"AREATRABALHO_ORGANIZACAOCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfareatrabalho_descricao_Internalname = sPrefix+"vTFAREATRABALHO_DESCRICAO";
         edtavTfareatrabalho_descricao_sel_Internalname = sPrefix+"vTFAREATRABALHO_DESCRICAO_SEL";
         Ddo_areatrabalho_descricao_Internalname = sPrefix+"DDO_AREATRABALHO_DESCRICAO";
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAreaTrabalho_Descricao_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtAreaTrabalho_Descricao_Link = "";
         edtAreaTrabalho_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtAreaTrabalho_Descricao_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfareatrabalho_descricao_sel_Jsonclick = "";
         edtavTfareatrabalho_descricao_sel_Visible = 1;
         edtavTfareatrabalho_descricao_Jsonclick = "";
         edtavTfareatrabalho_descricao_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtAreaTrabalho_OrganizacaoCod_Jsonclick = "";
         edtAreaTrabalho_OrganizacaoCod_Visible = 1;
         Ddo_areatrabalho_descricao_Searchbuttontext = "Pesquisar";
         Ddo_areatrabalho_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_areatrabalho_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_descricao_Loadingdata = "Carregando dados...";
         Ddo_areatrabalho_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_areatrabalho_descricao_Datalistproc = "GetOrganizacaoAreasDeTrabalhoWCFilterData";
         Ddo_areatrabalho_descricao_Datalisttype = "Dynamic";
         Ddo_areatrabalho_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_areatrabalho_descricao_Filtertype = "Character";
         Ddo_areatrabalho_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_descricao_Cls = "ColumnSettings";
         Ddo_areatrabalho_descricao_Tooltip = "Op��es";
         Ddo_areatrabalho_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV17TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV7AreaTrabalho_OrganizacaoCod',fld:'vAREATRABALHO_ORGANIZACAOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15AreaTrabalho_DescricaoTitleFilterData',fld:'vAREATRABALHO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtAreaTrabalho_Descricao_Titleformat',ctrl:'AREATRABALHO_DESCRICAO',prop:'Titleformat'},{av:'edtAreaTrabalho_Descricao_Title',ctrl:'AREATRABALHO_DESCRICAO',prop:'Title'},{av:'AV21GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV22GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11IW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV17TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV7AreaTrabalho_OrganizacaoCod',fld:'vAREATRABALHO_ORGANIZACAOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AREATRABALHO_DESCRICAO.ONOPTIONCLICKED","{handler:'E12IW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV17TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV7AreaTrabalho_OrganizacaoCod',fld:'vAREATRABALHO_ORGANIZACAOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_areatrabalho_descricao_Activeeventkey',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_descricao_Filteredtext_get',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_areatrabalho_descricao_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'AV16TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV17TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15IW2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtAreaTrabalho_Descricao_Link',ctrl:'AREATRABALHO_DESCRICAO',prop:'Link'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_areatrabalho_descricao_Activeeventkey = "";
         Ddo_areatrabalho_descricao_Filteredtext_get = "";
         Ddo_areatrabalho_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16TFAreaTrabalho_Descricao = "";
         AV17TFAreaTrabalho_Descricao_Sel = "";
         AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = "";
         AV25Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV15AreaTrabalho_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_areatrabalho_descricao_Filteredtext_set = "";
         Ddo_areatrabalho_descricao_Selectedvalue_set = "";
         Ddo_areatrabalho_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A6AreaTrabalho_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16TFAreaTrabalho_Descricao = "";
         H00IW2_A5AreaTrabalho_Codigo = new int[1] ;
         H00IW2_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         H00IW2_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         H00IW2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00IW3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7AreaTrabalho_OrganizacaoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.organizacaoareasdetrabalhowc__default(),
            new Object[][] {
                new Object[] {
               H00IW2_A5AreaTrabalho_Codigo, H00IW2_A1216AreaTrabalho_OrganizacaoCod, H00IW2_n1216AreaTrabalho_OrganizacaoCod, H00IW2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00IW3_AGRID_nRecordCount
               }
            }
         );
         AV25Pgmname = "OrganizacaoAreasDeTrabalhoWC";
         /* GeneXus formulas. */
         AV25Pgmname = "OrganizacaoAreasDeTrabalhoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAreaTrabalho_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7AreaTrabalho_OrganizacaoCod ;
      private int wcpOAV7AreaTrabalho_OrganizacaoCod ;
      private int subGrid_Rows ;
      private int A5AreaTrabalho_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int edtAreaTrabalho_OrganizacaoCod_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfareatrabalho_descricao_Visible ;
      private int edtavTfareatrabalho_descricao_sel_Visible ;
      private int edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV20PageToGo ;
      private int AV26GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV21GridCurrentPage ;
      private long AV22GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_areatrabalho_descricao_Activeeventkey ;
      private String Ddo_areatrabalho_descricao_Filteredtext_get ;
      private String Ddo_areatrabalho_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV25Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_areatrabalho_descricao_Caption ;
      private String Ddo_areatrabalho_descricao_Tooltip ;
      private String Ddo_areatrabalho_descricao_Cls ;
      private String Ddo_areatrabalho_descricao_Filteredtext_set ;
      private String Ddo_areatrabalho_descricao_Selectedvalue_set ;
      private String Ddo_areatrabalho_descricao_Dropdownoptionstype ;
      private String Ddo_areatrabalho_descricao_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_descricao_Sortedstatus ;
      private String Ddo_areatrabalho_descricao_Filtertype ;
      private String Ddo_areatrabalho_descricao_Datalisttype ;
      private String Ddo_areatrabalho_descricao_Datalistproc ;
      private String Ddo_areatrabalho_descricao_Sortasc ;
      private String Ddo_areatrabalho_descricao_Sortdsc ;
      private String Ddo_areatrabalho_descricao_Loadingdata ;
      private String Ddo_areatrabalho_descricao_Cleanfilter ;
      private String Ddo_areatrabalho_descricao_Noresultsfound ;
      private String Ddo_areatrabalho_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtAreaTrabalho_OrganizacaoCod_Internalname ;
      private String edtAreaTrabalho_OrganizacaoCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfareatrabalho_descricao_Internalname ;
      private String edtavTfareatrabalho_descricao_Jsonclick ;
      private String edtavTfareatrabalho_descricao_sel_Internalname ;
      private String edtavTfareatrabalho_descricao_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_areatrabalho_descricao_Internalname ;
      private String edtAreaTrabalho_Descricao_Title ;
      private String edtAreaTrabalho_Descricao_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7AreaTrabalho_OrganizacaoCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_areatrabalho_descricao_Includesortasc ;
      private bool Ddo_areatrabalho_descricao_Includesortdsc ;
      private bool Ddo_areatrabalho_descricao_Includefilter ;
      private bool Ddo_areatrabalho_descricao_Filterisrange ;
      private bool Ddo_areatrabalho_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV16TFAreaTrabalho_Descricao ;
      private String AV17TFAreaTrabalho_Descricao_Sel ;
      private String AV18ddo_AreaTrabalho_DescricaoTitleControlIdToReplace ;
      private String A6AreaTrabalho_Descricao ;
      private String lV16TFAreaTrabalho_Descricao ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00IW2_A5AreaTrabalho_Codigo ;
      private int[] H00IW2_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H00IW2_n1216AreaTrabalho_OrganizacaoCod ;
      private String[] H00IW2_A6AreaTrabalho_Descricao ;
      private long[] H00IW3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV15AreaTrabalho_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV19DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class organizacaoareasdetrabalhowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IW2( IGxContext context ,
                                             String AV17TFAreaTrabalho_Descricao_Sel ,
                                             String AV16TFAreaTrabalho_Descricao ,
                                             String A6AreaTrabalho_Descricao ,
                                             bool AV13OrderedDsc ,
                                             int A1216AreaTrabalho_OrganizacaoCod ,
                                             int AV7AreaTrabalho_OrganizacaoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [AreaTrabalho_Codigo], [AreaTrabalho_OrganizacaoCod], [AreaTrabalho_Descricao]";
         sFromString = " FROM [AreaTrabalho] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([AreaTrabalho_OrganizacaoCod] = @AV7AreaTrabalho_OrganizacaoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAreaTrabalho_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] like @lV16TFAreaTrabalho_Descricao)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] = @AV17TFAreaTrabalho_Descricao_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AreaTrabalho_OrganizacaoCod], [AreaTrabalho_Descricao]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AreaTrabalho_OrganizacaoCod] DESC, [AreaTrabalho_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [AreaTrabalho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IW3( IGxContext context ,
                                             String AV17TFAreaTrabalho_Descricao_Sel ,
                                             String AV16TFAreaTrabalho_Descricao ,
                                             String A6AreaTrabalho_Descricao ,
                                             bool AV13OrderedDsc ,
                                             int A1216AreaTrabalho_OrganizacaoCod ,
                                             int AV7AreaTrabalho_OrganizacaoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AreaTrabalho_OrganizacaoCod] = @AV7AreaTrabalho_OrganizacaoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAreaTrabalho_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] like @lV16TFAreaTrabalho_Descricao)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAreaTrabalho_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] = @AV17TFAreaTrabalho_Descricao_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IW2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 1 :
                     return conditional_H00IW3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IW2 ;
          prmH00IW2 = new Object[] {
          new Object[] {"@AV7AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFAreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFAreaTrabalho_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IW3 ;
          prmH00IW3 = new Object[] {
          new Object[] {"@AV7AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFAreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV17TFAreaTrabalho_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IW2,11,0,true,false )
             ,new CursorDef("H00IW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IW3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

}
