/*
               File: PRC_ReAbrirCicloExc
        Description: Re Abrir Ciclo Exc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:40.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_reabrircicloexc : GXProcedure
   {
      public prc_reabrircicloexc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_reabrircicloexc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoExecucao_Codigo )
      {
         this.A1405ContagemResultadoExecucao_Codigo = aP0_ContagemResultadoExecucao_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoExecucao_Codigo=this.A1405ContagemResultadoExecucao_Codigo;
      }

      public int executeUdp( )
      {
         this.A1405ContagemResultadoExecucao_Codigo = aP0_ContagemResultadoExecucao_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoExecucao_Codigo=this.A1405ContagemResultadoExecucao_Codigo;
         return A1405ContagemResultadoExecucao_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoExecucao_Codigo )
      {
         prc_reabrircicloexc objprc_reabrircicloexc;
         objprc_reabrircicloexc = new prc_reabrircicloexc();
         objprc_reabrircicloexc.A1405ContagemResultadoExecucao_Codigo = aP0_ContagemResultadoExecucao_Codigo;
         objprc_reabrircicloexc.context.SetSubmitInitialConfig(context);
         objprc_reabrircicloexc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_reabrircicloexc);
         aP0_ContagemResultadoExecucao_Codigo=this.A1405ContagemResultadoExecucao_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_reabrircicloexc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WY2 */
         pr_default.execute(0, new Object[] {A1405ContagemResultadoExecucao_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1410ContagemResultadoExecucao_Dias = P00WY2_A1410ContagemResultadoExecucao_Dias[0];
            n1410ContagemResultadoExecucao_Dias = P00WY2_n1410ContagemResultadoExecucao_Dias[0];
            A1407ContagemResultadoExecucao_Fim = P00WY2_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00WY2_n1407ContagemResultadoExecucao_Fim[0];
            A1410ContagemResultadoExecucao_Dias = 0;
            n1410ContagemResultadoExecucao_Dias = false;
            n1410ContagemResultadoExecucao_Dias = true;
            A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
            n1407ContagemResultadoExecucao_Fim = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00WY3 */
            pr_default.execute(1, new Object[] {n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            if (true) break;
            /* Using cursor P00WY4 */
            pr_default.execute(2, new Object[] {n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WY2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P00WY2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00WY2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P00WY2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00WY2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_reabrircicloexc__default(),
            new Object[][] {
                new Object[] {
               P00WY2_A1405ContagemResultadoExecucao_Codigo, P00WY2_A1410ContagemResultadoExecucao_Dias, P00WY2_n1410ContagemResultadoExecucao_Dias, P00WY2_A1407ContagemResultadoExecucao_Fim, P00WY2_n1407ContagemResultadoExecucao_Fim
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1410ContagemResultadoExecucao_Dias ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private String scmdbuf ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoExecucao_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00WY2_A1405ContagemResultadoExecucao_Codigo ;
      private short[] P00WY2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00WY2_n1410ContagemResultadoExecucao_Dias ;
      private DateTime[] P00WY2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00WY2_n1407ContagemResultadoExecucao_Fim ;
   }

   public class prc_reabrircicloexc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WY2 ;
          prmP00WY2 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WY3 ;
          prmP00WY3 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WY4 ;
          prmP00WY4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WY2", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Fim] FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo ORDER BY [ContagemResultadoExecucao_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WY2,1,0,true,true )
             ,new CursorDef("P00WY3", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias, [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WY3)
             ,new CursorDef("P00WY4", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias, [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WY4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
