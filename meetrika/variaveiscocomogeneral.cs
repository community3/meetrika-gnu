/*
               File: VariaveisCocomoGeneral
        Description: Variaveis Cocomo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:25:24.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class variaveiscocomogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public variaveiscocomogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public variaveiscocomogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_VariavelCocomo_AreaTrabalhoCod ,
                           String aP1_VariavelCocomo_Sigla ,
                           String aP2_VariavelCocomo_Tipo )
      {
         this.A961VariavelCocomo_AreaTrabalhoCod = aP0_VariavelCocomo_AreaTrabalhoCod;
         this.A962VariavelCocomo_Sigla = aP1_VariavelCocomo_Sigla;
         this.A964VariavelCocomo_Tipo = aP2_VariavelCocomo_Tipo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbVariavelCocomo_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A961VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
                  A962VariavelCocomo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
                  A964VariavelCocomo_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A961VariavelCocomo_AreaTrabalhoCod,(String)A962VariavelCocomo_Sigla,(String)A964VariavelCocomo_Tipo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGM2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "VariaveisCocomoGeneral";
               context.Gx_err = 0;
               WSGM2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Variaveis Cocomo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117252492");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("variaveiscocomogeneral.aspx") + "?" + UrlEncode("" +A961VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A962VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(A964VariavelCocomo_Tipo))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA962VariavelCocomo_Sigla", StringUtil.RTrim( wcpOA962VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA964VariavelCocomo_Tipo", StringUtil.RTrim( wcpOA964VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"VARIAVELCOCOMO_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A992VariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_DATA", GetSecureSignedToken( sPrefix, A966VariavelCocomo_Data));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_EXTRABAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_MUITOBAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_BAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_NOMINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_ALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_MUITOALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_EXTRAALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_VARIAVELCOCOMO_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGM2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("variaveiscocomogeneral.js", "?20203117252494");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "VariaveisCocomoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Variaveis Cocomo General" ;
      }

      protected void WBGM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "variaveiscocomogeneral.aspx");
            }
            wb_table1_2_GM2( true) ;
         }
         else
         {
            wb_table1_2_GM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GM2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_AreaTrabalhoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ProjetoCod_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_ProjetoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_VariaveisCocomoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTGM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Variaveis Cocomo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGM0( ) ;
            }
         }
      }

      protected void WSGM2( )
      {
         STARTGM2( ) ;
         EVTGM2( ) ;
      }

      protected void EVTGM2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GM2 */
                                    E11GM2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GM2 */
                                    E12GM2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GM2 */
                                    E13GM2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GM2 */
                                    E14GM2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGM0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGM2( ) ;
            }
         }
      }

      protected void PAGM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbVariavelCocomo_Tipo.Name = "VARIAVELCOCOMO_TIPO";
            cmbVariavelCocomo_Tipo.WebTags = "";
            cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
            cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
            cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
            if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
            {
               A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
         {
            A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "VariaveisCocomoGeneral";
         context.Gx_err = 0;
      }

      protected void RFGM2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00GM2 */
            pr_default.execute(0, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A992VariavelCocomo_Sequencial = H00GM2_A992VariavelCocomo_Sequencial[0];
               A965VariavelCocomo_ProjetoCod = H00GM2_A965VariavelCocomo_ProjetoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")));
               n965VariavelCocomo_ProjetoCod = H00GM2_n965VariavelCocomo_ProjetoCod[0];
               A972VariavelCocomo_ExtraAlto = H00GM2_A972VariavelCocomo_ExtraAlto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_EXTRAALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n972VariavelCocomo_ExtraAlto = H00GM2_n972VariavelCocomo_ExtraAlto[0];
               A971VariavelCocomo_MuitoAlto = H00GM2_A971VariavelCocomo_MuitoAlto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_MUITOALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n971VariavelCocomo_MuitoAlto = H00GM2_n971VariavelCocomo_MuitoAlto[0];
               A970VariavelCocomo_Alto = H00GM2_A970VariavelCocomo_Alto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_ALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n970VariavelCocomo_Alto = H00GM2_n970VariavelCocomo_Alto[0];
               A969VariavelCocomo_Nominal = H00GM2_A969VariavelCocomo_Nominal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_NOMINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n969VariavelCocomo_Nominal = H00GM2_n969VariavelCocomo_Nominal[0];
               A968VariavelCocomo_Baixo = H00GM2_A968VariavelCocomo_Baixo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_BAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n968VariavelCocomo_Baixo = H00GM2_n968VariavelCocomo_Baixo[0];
               A967VariavelCocomo_MuitoBaixo = H00GM2_A967VariavelCocomo_MuitoBaixo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_MUITOBAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n967VariavelCocomo_MuitoBaixo = H00GM2_n967VariavelCocomo_MuitoBaixo[0];
               A986VariavelCocomo_ExtraBaixo = H00GM2_A986VariavelCocomo_ExtraBaixo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_EXTRABAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n986VariavelCocomo_ExtraBaixo = H00GM2_n986VariavelCocomo_ExtraBaixo[0];
               A966VariavelCocomo_Data = H00GM2_A966VariavelCocomo_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_DATA", GetSecureSignedToken( sPrefix, A966VariavelCocomo_Data));
               n966VariavelCocomo_Data = H00GM2_n966VariavelCocomo_Data[0];
               A963VariavelCocomo_Nome = H00GM2_A963VariavelCocomo_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
               /* Execute user event: E12GM2 */
               E12GM2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBGM0( ) ;
         }
      }

      protected void STRUPGM0( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "VariaveisCocomoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11GM2 */
         E11GM2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A963VariavelCocomo_Nome = StringUtil.Upper( cgiGet( edtVariavelCocomo_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
            A966VariavelCocomo_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtVariavelCocomo_Data_Internalname), 0));
            n966VariavelCocomo_Data = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_DATA", GetSecureSignedToken( sPrefix, A966VariavelCocomo_Data));
            A986VariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".");
            n986VariavelCocomo_ExtraBaixo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_EXTRABAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A967VariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".");
            n967VariavelCocomo_MuitoBaixo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_MUITOBAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A968VariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".");
            n968VariavelCocomo_Baixo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_BAIXO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A969VariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".");
            n969VariavelCocomo_Nominal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_NOMINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A970VariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".");
            n970VariavelCocomo_Alto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_ALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A971VariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".");
            n971VariavelCocomo_MuitoAlto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_MUITOALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A972VariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".");
            n972VariavelCocomo_ExtraAlto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_EXTRAALTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A965VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_ProjetoCod_Internalname), ",", "."));
            n965VariavelCocomo_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_VARIAVELCOCOMO_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA961VariavelCocomo_AreaTrabalhoCod"), ",", "."));
            wcpOA962VariavelCocomo_Sigla = cgiGet( sPrefix+"wcpOA962VariavelCocomo_Sigla");
            wcpOA964VariavelCocomo_Tipo = cgiGet( sPrefix+"wcpOA964VariavelCocomo_Tipo");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11GM2 */
         E11GM2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11GM2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12GM2( )
      {
         /* Load Routine */
         edtVariavelCocomo_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Visible), 5, 0)));
         edtVariavelCocomo_ProjetoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtVariavelCocomo_ProjetoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ProjetoCod_Visible), 5, 0)));
      }

      protected void E13GM2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A961VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A962VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(A964VariavelCocomo_Tipo)) + "," + UrlEncode("" +A992VariavelCocomo_Sequencial);
         context.wjLocDisableFrm = 1;
      }

      protected void E14GM2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A961VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A962VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(A964VariavelCocomo_Tipo)) + "," + UrlEncode("" +A992VariavelCocomo_Sequencial);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV10TrnContext.gxTpr_Callerondelete = false;
         AV10TrnContext.gxTpr_Callerurl = AV13HTTPRequest.ScriptName+"?"+AV13HTTPRequest.QueryString;
         AV10TrnContext.gxTpr_Transactionname = "VariaveisCocomo";
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "VariavelCocomo_AreaTrabalhoCod";
         AV11TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7VariavelCocomo_AreaTrabalhoCod), 6, 0);
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "VariavelCocomo_Sigla";
         AV11TrnContextAtt.gxTpr_Attributevalue = AV8VariavelCocomo_Sigla;
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "VariavelCocomo_Tipo";
         AV11TrnContextAtt.gxTpr_Attributevalue = AV9VariavelCocomo_Tipo;
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV12Session.Set("TrnContext", AV10TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_GM2( true) ;
         }
         else
         {
            wb_table2_8_GM2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_GM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_GM2( true) ;
         }
         else
         {
            wb_table3_66_GM2( false) ;
         }
         return  ;
      }

      protected void wb_table3_66_GM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GM2e( true) ;
         }
         else
         {
            wb_table1_2_GM2e( false) ;
         }
      }

      protected void wb_table3_66_GM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_GM2e( true) ;
         }
         else
         {
            wb_table3_66_GM2e( false) ;
         }
      }

      protected void wb_table2_8_GM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_sigla_Internalname, "Sigla", "", "", lblTextblockvariavelcocomo_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Sigla_Internalname, StringUtil.RTrim( A962VariavelCocomo_Sigla), StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_nome_Internalname, "Nome", "", "", lblTextblockvariavelcocomo_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Nome_Internalname, StringUtil.RTrim( A963VariavelCocomo_Nome), StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_tipo_Internalname, "Tipo", "", "", lblTextblockvariavelcocomo_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbVariavelCocomo_Tipo, cmbVariavelCocomo_Tipo_Internalname, StringUtil.RTrim( A964VariavelCocomo_Tipo), 1, cmbVariavelCocomo_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_VariaveisCocomoGeneral.htm");
            cmbVariavelCocomo_Tipo.CurrentValue = StringUtil.RTrim( A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbVariavelCocomo_Tipo_Internalname, "Values", (String)(cmbVariavelCocomo_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_data_Internalname, "Data", "", "", lblTextblockvariavelcocomo_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtVariavelCocomo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Data_Internalname, context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"), context.localUtil.Format( A966VariavelCocomo_Data, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtVariavelCocomo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_extrabaixo_Internalname, "Extra Baixo", "", "", lblTextblockvariavelcocomo_extrabaixo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ExtraBaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ",", "")), context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ExtraBaixo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_muitobaixo_Internalname, "Muito Baixo", "", "", lblTextblockvariavelcocomo_muitobaixo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_MuitoBaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ",", "")), context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_MuitoBaixo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_baixo_Internalname, "Baixo", "", "", lblTextblockvariavelcocomo_baixo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Baixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ",", "")), context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Baixo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_nominal_Internalname, "Nominal", "", "", lblTextblockvariavelcocomo_nominal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Nominal_Internalname, StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ",", "")), context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Nominal_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_alto_Internalname, "Alto", "", "", lblTextblockvariavelcocomo_alto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Alto_Internalname, StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ",", "")), context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Alto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_muitoalto_Internalname, "Muito Alto", "", "", lblTextblockvariavelcocomo_muitoalto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_MuitoAlto_Internalname, StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ",", "")), context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_MuitoAlto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_extraalto_Internalname, "Extra Alto", "", "", lblTextblockvariavelcocomo_extraalto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ExtraAlto_Internalname, StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ",", "")), context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ExtraAlto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_GM2e( true) ;
         }
         else
         {
            wb_table2_8_GM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A961VariavelCocomo_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         A962VariavelCocomo_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
         A964VariavelCocomo_Tipo = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGM2( ) ;
         WSGM2( ) ;
         WEGM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA961VariavelCocomo_AreaTrabalhoCod = (String)((String)getParm(obj,0));
         sCtrlA962VariavelCocomo_Sigla = (String)((String)getParm(obj,1));
         sCtrlA964VariavelCocomo_Tipo = (String)((String)getParm(obj,2));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGM2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "variaveiscocomogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGM2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A961VariavelCocomo_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            A962VariavelCocomo_Sigla = (String)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
            A964VariavelCocomo_Tipo = (String)getParm(obj,4);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
         wcpOA961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA961VariavelCocomo_AreaTrabalhoCod"), ",", "."));
         wcpOA962VariavelCocomo_Sigla = cgiGet( sPrefix+"wcpOA962VariavelCocomo_Sigla");
         wcpOA964VariavelCocomo_Tipo = cgiGet( sPrefix+"wcpOA964VariavelCocomo_Tipo");
         if ( ! GetJustCreated( ) && ( ( A961VariavelCocomo_AreaTrabalhoCod != wcpOA961VariavelCocomo_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A962VariavelCocomo_Sigla, wcpOA962VariavelCocomo_Sigla) != 0 ) || ( StringUtil.StrCmp(A964VariavelCocomo_Tipo, wcpOA964VariavelCocomo_Tipo) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOA961VariavelCocomo_AreaTrabalhoCod = A961VariavelCocomo_AreaTrabalhoCod;
         wcpOA962VariavelCocomo_Sigla = A962VariavelCocomo_Sigla;
         wcpOA964VariavelCocomo_Tipo = A964VariavelCocomo_Tipo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA961VariavelCocomo_AreaTrabalhoCod = cgiGet( sPrefix+"A961VariavelCocomo_AreaTrabalhoCod_CTRL");
         if ( StringUtil.Len( sCtrlA961VariavelCocomo_AreaTrabalhoCod) > 0 )
         {
            A961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA961VariavelCocomo_AreaTrabalhoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            A961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A961VariavelCocomo_AreaTrabalhoCod_PARM"), ",", "."));
         }
         sCtrlA962VariavelCocomo_Sigla = cgiGet( sPrefix+"A962VariavelCocomo_Sigla_CTRL");
         if ( StringUtil.Len( sCtrlA962VariavelCocomo_Sigla) > 0 )
         {
            A962VariavelCocomo_Sigla = cgiGet( sCtrlA962VariavelCocomo_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
         }
         else
         {
            A962VariavelCocomo_Sigla = cgiGet( sPrefix+"A962VariavelCocomo_Sigla_PARM");
         }
         sCtrlA964VariavelCocomo_Tipo = cgiGet( sPrefix+"A964VariavelCocomo_Tipo_CTRL");
         if ( StringUtil.Len( sCtrlA964VariavelCocomo_Tipo) > 0 )
         {
            A964VariavelCocomo_Tipo = cgiGet( sCtrlA964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
         else
         {
            A964VariavelCocomo_Tipo = cgiGet( sPrefix+"A964VariavelCocomo_Tipo_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGM2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGM2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGM2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A961VariavelCocomo_AreaTrabalhoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA961VariavelCocomo_AreaTrabalhoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A961VariavelCocomo_AreaTrabalhoCod_CTRL", StringUtil.RTrim( sCtrlA961VariavelCocomo_AreaTrabalhoCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A962VariavelCocomo_Sigla_PARM", StringUtil.RTrim( A962VariavelCocomo_Sigla));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA962VariavelCocomo_Sigla)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A962VariavelCocomo_Sigla_CTRL", StringUtil.RTrim( sCtrlA962VariavelCocomo_Sigla));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A964VariavelCocomo_Tipo_PARM", StringUtil.RTrim( A964VariavelCocomo_Tipo));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA964VariavelCocomo_Tipo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A964VariavelCocomo_Tipo_CTRL", StringUtil.RTrim( sCtrlA964VariavelCocomo_Tipo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGM2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117252535");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("variaveiscocomogeneral.js", "?20203117252535");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockvariavelcocomo_sigla_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_SIGLA";
         edtVariavelCocomo_Sigla_Internalname = sPrefix+"VARIAVELCOCOMO_SIGLA";
         lblTextblockvariavelcocomo_nome_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_NOME";
         edtVariavelCocomo_Nome_Internalname = sPrefix+"VARIAVELCOCOMO_NOME";
         lblTextblockvariavelcocomo_tipo_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_TIPO";
         cmbVariavelCocomo_Tipo_Internalname = sPrefix+"VARIAVELCOCOMO_TIPO";
         lblTextblockvariavelcocomo_data_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_DATA";
         edtVariavelCocomo_Data_Internalname = sPrefix+"VARIAVELCOCOMO_DATA";
         lblTextblockvariavelcocomo_extrabaixo_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_EXTRABAIXO";
         edtVariavelCocomo_ExtraBaixo_Internalname = sPrefix+"VARIAVELCOCOMO_EXTRABAIXO";
         lblTextblockvariavelcocomo_muitobaixo_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_MUITOBAIXO";
         edtVariavelCocomo_MuitoBaixo_Internalname = sPrefix+"VARIAVELCOCOMO_MUITOBAIXO";
         lblTextblockvariavelcocomo_baixo_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_BAIXO";
         edtVariavelCocomo_Baixo_Internalname = sPrefix+"VARIAVELCOCOMO_BAIXO";
         lblTextblockvariavelcocomo_nominal_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_NOMINAL";
         edtVariavelCocomo_Nominal_Internalname = sPrefix+"VARIAVELCOCOMO_NOMINAL";
         lblTextblockvariavelcocomo_alto_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_ALTO";
         edtVariavelCocomo_Alto_Internalname = sPrefix+"VARIAVELCOCOMO_ALTO";
         lblTextblockvariavelcocomo_muitoalto_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_MUITOALTO";
         edtVariavelCocomo_MuitoAlto_Internalname = sPrefix+"VARIAVELCOCOMO_MUITOALTO";
         lblTextblockvariavelcocomo_extraalto_Internalname = sPrefix+"TEXTBLOCKVARIAVELCOCOMO_EXTRAALTO";
         edtVariavelCocomo_ExtraAlto_Internalname = sPrefix+"VARIAVELCOCOMO_EXTRAALTO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtVariavelCocomo_AreaTrabalhoCod_Internalname = sPrefix+"VARIAVELCOCOMO_AREATRABALHOCOD";
         edtVariavelCocomo_ProjetoCod_Internalname = sPrefix+"VARIAVELCOCOMO_PROJETOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtVariavelCocomo_ExtraAlto_Jsonclick = "";
         edtVariavelCocomo_MuitoAlto_Jsonclick = "";
         edtVariavelCocomo_Alto_Jsonclick = "";
         edtVariavelCocomo_Nominal_Jsonclick = "";
         edtVariavelCocomo_Baixo_Jsonclick = "";
         edtVariavelCocomo_MuitoBaixo_Jsonclick = "";
         edtVariavelCocomo_ExtraBaixo_Jsonclick = "";
         edtVariavelCocomo_Data_Jsonclick = "";
         cmbVariavelCocomo_Tipo_Jsonclick = "";
         edtVariavelCocomo_Nome_Jsonclick = "";
         edtVariavelCocomo_Sigla_Jsonclick = "";
         edtVariavelCocomo_ProjetoCod_Jsonclick = "";
         edtVariavelCocomo_ProjetoCod_Visible = 1;
         edtVariavelCocomo_AreaTrabalhoCod_Jsonclick = "";
         edtVariavelCocomo_AreaTrabalhoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13GM2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14GM2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA962VariavelCocomo_Sigla = "";
         wcpOA964VariavelCocomo_Tipo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A963VariavelCocomo_Nome = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00GM2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GM2_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GM2_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GM2_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GM2_A965VariavelCocomo_ProjetoCod = new int[1] ;
         H00GM2_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         H00GM2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         H00GM2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         H00GM2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         H00GM2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         H00GM2_A970VariavelCocomo_Alto = new decimal[1] ;
         H00GM2_n970VariavelCocomo_Alto = new bool[] {false} ;
         H00GM2_A969VariavelCocomo_Nominal = new decimal[1] ;
         H00GM2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         H00GM2_A968VariavelCocomo_Baixo = new decimal[1] ;
         H00GM2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         H00GM2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         H00GM2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         H00GM2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         H00GM2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         H00GM2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         H00GM2_n966VariavelCocomo_Data = new bool[] {false} ;
         H00GM2_A963VariavelCocomo_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV13HTTPRequest = new GxHttpRequest( context);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV8VariavelCocomo_Sigla = "";
         AV9VariavelCocomo_Tipo = "";
         AV12Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockvariavelcocomo_sigla_Jsonclick = "";
         lblTextblockvariavelcocomo_nome_Jsonclick = "";
         lblTextblockvariavelcocomo_tipo_Jsonclick = "";
         lblTextblockvariavelcocomo_data_Jsonclick = "";
         lblTextblockvariavelcocomo_extrabaixo_Jsonclick = "";
         lblTextblockvariavelcocomo_muitobaixo_Jsonclick = "";
         lblTextblockvariavelcocomo_baixo_Jsonclick = "";
         lblTextblockvariavelcocomo_nominal_Jsonclick = "";
         lblTextblockvariavelcocomo_alto_Jsonclick = "";
         lblTextblockvariavelcocomo_muitoalto_Jsonclick = "";
         lblTextblockvariavelcocomo_extraalto_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA961VariavelCocomo_AreaTrabalhoCod = "";
         sCtrlA962VariavelCocomo_Sigla = "";
         sCtrlA964VariavelCocomo_Tipo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.variaveiscocomogeneral__default(),
            new Object[][] {
                new Object[] {
               H00GM2_A961VariavelCocomo_AreaTrabalhoCod, H00GM2_A962VariavelCocomo_Sigla, H00GM2_A964VariavelCocomo_Tipo, H00GM2_A992VariavelCocomo_Sequencial, H00GM2_A965VariavelCocomo_ProjetoCod, H00GM2_n965VariavelCocomo_ProjetoCod, H00GM2_A972VariavelCocomo_ExtraAlto, H00GM2_n972VariavelCocomo_ExtraAlto, H00GM2_A971VariavelCocomo_MuitoAlto, H00GM2_n971VariavelCocomo_MuitoAlto,
               H00GM2_A970VariavelCocomo_Alto, H00GM2_n970VariavelCocomo_Alto, H00GM2_A969VariavelCocomo_Nominal, H00GM2_n969VariavelCocomo_Nominal, H00GM2_A968VariavelCocomo_Baixo, H00GM2_n968VariavelCocomo_Baixo, H00GM2_A967VariavelCocomo_MuitoBaixo, H00GM2_n967VariavelCocomo_MuitoBaixo, H00GM2_A986VariavelCocomo_ExtraBaixo, H00GM2_n986VariavelCocomo_ExtraBaixo,
               H00GM2_A966VariavelCocomo_Data, H00GM2_n966VariavelCocomo_Data, H00GM2_A963VariavelCocomo_Nome
               }
            }
         );
         AV16Pgmname = "VariaveisCocomoGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "VariaveisCocomoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A992VariavelCocomo_Sequencial ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int wcpOA961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private int edtVariavelCocomo_AreaTrabalhoCod_Visible ;
      private int edtVariavelCocomo_ProjetoCod_Visible ;
      private int AV7VariavelCocomo_AreaTrabalhoCod ;
      private int idxLst ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private String A962VariavelCocomo_Sigla ;
      private String A964VariavelCocomo_Tipo ;
      private String wcpOA962VariavelCocomo_Sigla ;
      private String wcpOA964VariavelCocomo_Tipo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A963VariavelCocomo_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Internalname ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Jsonclick ;
      private String edtVariavelCocomo_ProjetoCod_Internalname ;
      private String edtVariavelCocomo_ProjetoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtVariavelCocomo_Nome_Internalname ;
      private String edtVariavelCocomo_Data_Internalname ;
      private String edtVariavelCocomo_ExtraBaixo_Internalname ;
      private String edtVariavelCocomo_MuitoBaixo_Internalname ;
      private String edtVariavelCocomo_Baixo_Internalname ;
      private String edtVariavelCocomo_Nominal_Internalname ;
      private String edtVariavelCocomo_Alto_Internalname ;
      private String edtVariavelCocomo_MuitoAlto_Internalname ;
      private String edtVariavelCocomo_ExtraAlto_Internalname ;
      private String AV8VariavelCocomo_Sigla ;
      private String AV9VariavelCocomo_Tipo ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockvariavelcocomo_sigla_Internalname ;
      private String lblTextblockvariavelcocomo_sigla_Jsonclick ;
      private String edtVariavelCocomo_Sigla_Internalname ;
      private String edtVariavelCocomo_Sigla_Jsonclick ;
      private String lblTextblockvariavelcocomo_nome_Internalname ;
      private String lblTextblockvariavelcocomo_nome_Jsonclick ;
      private String edtVariavelCocomo_Nome_Jsonclick ;
      private String lblTextblockvariavelcocomo_tipo_Internalname ;
      private String lblTextblockvariavelcocomo_tipo_Jsonclick ;
      private String cmbVariavelCocomo_Tipo_Internalname ;
      private String cmbVariavelCocomo_Tipo_Jsonclick ;
      private String lblTextblockvariavelcocomo_data_Internalname ;
      private String lblTextblockvariavelcocomo_data_Jsonclick ;
      private String edtVariavelCocomo_Data_Jsonclick ;
      private String lblTextblockvariavelcocomo_extrabaixo_Internalname ;
      private String lblTextblockvariavelcocomo_extrabaixo_Jsonclick ;
      private String edtVariavelCocomo_ExtraBaixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_muitobaixo_Internalname ;
      private String lblTextblockvariavelcocomo_muitobaixo_Jsonclick ;
      private String edtVariavelCocomo_MuitoBaixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_baixo_Internalname ;
      private String lblTextblockvariavelcocomo_baixo_Jsonclick ;
      private String edtVariavelCocomo_Baixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_nominal_Internalname ;
      private String lblTextblockvariavelcocomo_nominal_Jsonclick ;
      private String edtVariavelCocomo_Nominal_Jsonclick ;
      private String lblTextblockvariavelcocomo_alto_Internalname ;
      private String lblTextblockvariavelcocomo_alto_Jsonclick ;
      private String edtVariavelCocomo_Alto_Jsonclick ;
      private String lblTextblockvariavelcocomo_muitoalto_Internalname ;
      private String lblTextblockvariavelcocomo_muitoalto_Jsonclick ;
      private String edtVariavelCocomo_MuitoAlto_Jsonclick ;
      private String lblTextblockvariavelcocomo_extraalto_Internalname ;
      private String lblTextblockvariavelcocomo_extraalto_Jsonclick ;
      private String edtVariavelCocomo_ExtraAlto_Jsonclick ;
      private String sCtrlA961VariavelCocomo_AreaTrabalhoCod ;
      private String sCtrlA962VariavelCocomo_Sigla ;
      private String sCtrlA964VariavelCocomo_Tipo ;
      private DateTime A966VariavelCocomo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n970VariavelCocomo_Alto ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n966VariavelCocomo_Data ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbVariavelCocomo_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00GM2_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] H00GM2_A962VariavelCocomo_Sigla ;
      private String[] H00GM2_A964VariavelCocomo_Tipo ;
      private short[] H00GM2_A992VariavelCocomo_Sequencial ;
      private int[] H00GM2_A965VariavelCocomo_ProjetoCod ;
      private bool[] H00GM2_n965VariavelCocomo_ProjetoCod ;
      private decimal[] H00GM2_A972VariavelCocomo_ExtraAlto ;
      private bool[] H00GM2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] H00GM2_A971VariavelCocomo_MuitoAlto ;
      private bool[] H00GM2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] H00GM2_A970VariavelCocomo_Alto ;
      private bool[] H00GM2_n970VariavelCocomo_Alto ;
      private decimal[] H00GM2_A969VariavelCocomo_Nominal ;
      private bool[] H00GM2_n969VariavelCocomo_Nominal ;
      private decimal[] H00GM2_A968VariavelCocomo_Baixo ;
      private bool[] H00GM2_n968VariavelCocomo_Baixo ;
      private decimal[] H00GM2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] H00GM2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] H00GM2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] H00GM2_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] H00GM2_A966VariavelCocomo_Data ;
      private bool[] H00GM2_n966VariavelCocomo_Data ;
      private String[] H00GM2_A963VariavelCocomo_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV13HTTPRequest ;
      private IGxSession AV12Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
   }

   public class variaveiscocomogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GM2 ;
          prmH00GM2 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GM2", "SELECT [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial], [VariavelCocomo_ProjetoCod], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_Nome] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GM2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
