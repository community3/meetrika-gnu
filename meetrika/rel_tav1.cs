/*
               File: REL_TAV1
        Description: Stub for REL_TAV1
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:54.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_tav1 : GXProcedure
   {
      public rel_tav1( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_tav1( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Lote_Codigo )
      {
         this.AV2Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.AV2Lote_Codigo;
      }

      public int executeUdp( )
      {
         this.AV2Lote_Codigo = aP0_Lote_Codigo;
         initialize();
         executePrivate();
         aP0_Lote_Codigo=this.AV2Lote_Codigo;
         return AV2Lote_Codigo ;
      }

      public void executeSubmit( ref int aP0_Lote_Codigo )
      {
         rel_tav1 objrel_tav1;
         objrel_tav1 = new rel_tav1();
         objrel_tav1.AV2Lote_Codigo = aP0_Lote_Codigo;
         objrel_tav1.context.SetSubmitInitialConfig(context);
         objrel_tav1.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_tav1);
         aP0_Lote_Codigo=this.AV2Lote_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_tav1)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Lote_Codigo} ;
         ClassLoader.Execute("arel_tav1","GeneXus.Programs.arel_tav1", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
            AV2Lote_Codigo = (int)(args[0]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Lote_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Lote_Codigo ;
      private Object[] args ;
   }

}
