/*
               File: type_SdtGAMRepositoryConnectionFileFilter
        Description: GAMRepositoryConnectionFileFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:39.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRepositoryConnectionFileFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMRepositoryConnectionFileFilter( )
      {
         initialize();
      }

      public SdtGAMRepositoryConnectionFileFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRepositoryConnectionFileFilter_externalReference == null )
         {
            GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRepositoryConnectionFileFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMRepositoryConnectionFileFilter_externalReference == null )
            {
               GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
            }
            return GAMRepositoryConnectionFileFilter_externalReference.GUID ;
         }

         set {
            if ( GAMRepositoryConnectionFileFilter_externalReference == null )
            {
               GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
            }
            GAMRepositoryConnectionFileFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMRepositoryConnectionFileFilter_externalReference == null )
            {
               GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
            }
            return GAMRepositoryConnectionFileFilter_externalReference.Name ;
         }

         set {
            if ( GAMRepositoryConnectionFileFilter_externalReference == null )
            {
               GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
            }
            GAMRepositoryConnectionFileFilter_externalReference.Name = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRepositoryConnectionFileFilter_externalReference == null )
            {
               GAMRepositoryConnectionFileFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFileFilter(context);
            }
            return GAMRepositoryConnectionFileFilter_externalReference ;
         }

         set {
            GAMRepositoryConnectionFileFilter_externalReference = (Artech.Security.GAMRepositoryConnectionFileFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRepositoryConnectionFileFilter GAMRepositoryConnectionFileFilter_externalReference=null ;
   }

}
