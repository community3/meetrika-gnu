/*
               File: RegrasContagem
        Description: Regras de Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:45.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class regrascontagem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A865RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A865RegrasContagem_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7RegrasContagem_Regra = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7RegrasContagem_Regra", AV7RegrasContagem_Regra);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGRASCONTAGEM_REGRA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7RegrasContagem_Regra, ""))));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         radRegrasContagem_Definido.Name = "REGRASCONTAGEM_DEFINIDO";
         radRegrasContagem_Definido.WebTags = "";
         radRegrasContagem_Definido.addItem("A", "Ata de Reuni�o", 0);
         radRegrasContagem_Definido.addItem("E", "E_Mail", 0);
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Regras de Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtRegrasContagem_Regra_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public regrascontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public regrascontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           String aP1_RegrasContagem_Regra )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7RegrasContagem_Regra = aP1_RegrasContagem_Regra;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         radRegrasContagem_Definido = new GXRadio();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table3_59_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2Q108e( true) ;
         }
         else
         {
            wb_table1_2_2Q108e( false) ;
         }
      }

      protected void wb_table3_59_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_2Q108e( true) ;
         }
         else
         {
            wb_table3_59_2Q108e( false) ;
         }
      }

      protected void wb_table2_5_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2Q108e( true) ;
         }
         else
         {
            wb_table2_5_2Q108e( false) ;
         }
      }

      protected void wb_table4_13_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_regra_Internalname, "Regra", "", "", lblTextblockregrascontagem_regra_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRegrasContagem_Regra_Internalname, A860RegrasContagem_Regra, StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRegrasContagem_Regra_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtRegrasContagem_Regra_Enabled, 1, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_data_Internalname, "Data", "", "", lblTextblockregrascontagem_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtRegrasContagem_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtRegrasContagem_Data_Internalname, context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"), context.localUtil.Format( A861RegrasContagem_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRegrasContagem_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtRegrasContagem_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_RegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtRegrasContagem_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtRegrasContagem_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_RegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_validade_Internalname, "Validade", "", "", lblTextblockregrascontagem_validade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtRegrasContagem_Validade_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtRegrasContagem_Validade_Internalname, context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"), context.localUtil.Format( A862RegrasContagem_Validade, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRegrasContagem_Validade_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtRegrasContagem_Validade_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_RegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtRegrasContagem_Validade_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtRegrasContagem_Validade_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_RegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_responsavel_Internalname, "Respons�vel", "", "", lblTextblockregrascontagem_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRegrasContagem_Responsavel_Internalname, StringUtil.RTrim( A863RegrasContagem_Responsavel), StringUtil.RTrim( context.localUtil.Format( A863RegrasContagem_Responsavel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRegrasContagem_Responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtRegrasContagem_Responsavel_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_descricao_Internalname, "Descric�o", "", "", lblTextblockregrascontagem_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtRegrasContagem_Descricao_Internalname, A864RegrasContagem_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, 1, edtRegrasContagem_Descricao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregrascontagem_definido_Internalname, "Onde foi definido", "", "", lblTextblockregrascontagem_definido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_radio_ctrl( context, radRegrasContagem_Definido, radRegrasContagem_Definido_Internalname, StringUtil.RTrim( A1004RegrasContagem_Definido), "", 1, radRegrasContagem_Definido.Enabled, 0, 0, StyleString, ClassString, "", 0, radRegrasContagem_Definido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "HLP_RegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Anexos", 1, 0, "px", 0, "px", "Group", "", "HLP_RegrasContagem.htm");
            wb_table5_45_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table5_45_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2Q108e( true) ;
         }
         else
         {
            wb_table4_13_2Q108e( false) ;
         }
      }

      protected void wb_table5_45_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_48_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table6_48_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_45_2Q108e( true) ;
         }
         else
         {
            wb_table5_45_2Q108e( false) ;
         }
      }

      protected void wb_table6_48_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_51_2Q108( true) ;
         }
         return  ;
      }

      protected void wb_table7_51_2Q108e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_48_2Q108e( true) ;
         }
         else
         {
            wb_table6_48_2Q108e( false) ;
         }
      }

      protected void wb_table7_51_2Q108( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblarqevd_Internalname, tblTblarqevd_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0054"+"", StringUtil.RTrim( WebComp_Wcarquivosevd_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0054"+""+"\""+((WebComp_Wcarquivosevd_Visible==1) ? "" : " style=\"display:none;\"")) ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcarquivosevd), StringUtil.Lower( WebComp_Wcarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0054"+"");
                  }
                  WebComp_Wcarquivosevd.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcarquivosevd), StringUtil.Lower( WebComp_Wcarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0056"+"", StringUtil.RTrim( WebComp_Wcviewarquivosevd_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0056"+""+"\""+((WebComp_Wcviewarquivosevd_Visible==1) ? "" : " style=\"display:none;\"")) ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcviewarquivosevd), StringUtil.Lower( WebComp_Wcviewarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0056"+"");
                  }
                  WebComp_Wcviewarquivosevd.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcviewarquivosevd), StringUtil.Lower( WebComp_Wcviewarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_51_2Q108e( true) ;
         }
         else
         {
            wb_table7_51_2Q108e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  WebComp_Wcarquivosevd.componentstart();
               }
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcviewarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
               {
                  WebComp_Wcviewarquivosevd.componentstart();
               }
            }
         }
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112Q2 */
         E112Q2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A860RegrasContagem_Regra = cgiGet( edtRegrasContagem_Regra_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
               if ( context.localUtil.VCDate( cgiGet( edtRegrasContagem_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "REGRASCONTAGEM_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtRegrasContagem_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A861RegrasContagem_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A861RegrasContagem_Data", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
               }
               else
               {
                  A861RegrasContagem_Data = context.localUtil.CToD( cgiGet( edtRegrasContagem_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A861RegrasContagem_Data", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtRegrasContagem_Validade_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Validade"}), 1, "REGRASCONTAGEM_VALIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtRegrasContagem_Validade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A862RegrasContagem_Validade = DateTime.MinValue;
                  n862RegrasContagem_Validade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A862RegrasContagem_Validade", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
               }
               else
               {
                  A862RegrasContagem_Validade = context.localUtil.CToD( cgiGet( edtRegrasContagem_Validade_Internalname), 2);
                  n862RegrasContagem_Validade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A862RegrasContagem_Validade", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
               }
               n862RegrasContagem_Validade = ((DateTime.MinValue==A862RegrasContagem_Validade) ? true : false);
               A863RegrasContagem_Responsavel = StringUtil.Upper( cgiGet( edtRegrasContagem_Responsavel_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A863RegrasContagem_Responsavel", A863RegrasContagem_Responsavel);
               A864RegrasContagem_Descricao = cgiGet( edtRegrasContagem_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A864RegrasContagem_Descricao", A864RegrasContagem_Descricao);
               A1004RegrasContagem_Definido = cgiGet( radRegrasContagem_Definido_Internalname);
               n1004RegrasContagem_Definido = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1004RegrasContagem_Definido", A1004RegrasContagem_Definido);
               n1004RegrasContagem_Definido = (String.IsNullOrEmpty(StringUtil.RTrim( A1004RegrasContagem_Definido)) ? true : false);
               /* Read saved values. */
               Z860RegrasContagem_Regra = cgiGet( "Z860RegrasContagem_Regra");
               Z861RegrasContagem_Data = context.localUtil.CToD( cgiGet( "Z861RegrasContagem_Data"), 0);
               Z862RegrasContagem_Validade = context.localUtil.CToD( cgiGet( "Z862RegrasContagem_Validade"), 0);
               n862RegrasContagem_Validade = ((DateTime.MinValue==A862RegrasContagem_Validade) ? true : false);
               Z863RegrasContagem_Responsavel = cgiGet( "Z863RegrasContagem_Responsavel");
               Z1004RegrasContagem_Definido = cgiGet( "Z1004RegrasContagem_Definido");
               n1004RegrasContagem_Definido = (String.IsNullOrEmpty(StringUtil.RTrim( A1004RegrasContagem_Definido)) ? true : false);
               Z865RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z865RegrasContagem_AreaTrabalhoCod"), ",", "."));
               A865RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z865RegrasContagem_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N865RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N865RegrasContagem_AreaTrabalhoCod"), ",", "."));
               AV7RegrasContagem_Regra = cgiGet( "vREGRASCONTAGEM_REGRA");
               AV11Insert_RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_REGRASCONTAGEM_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A865RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "REGRASCONTAGEM_AREATRABALHOCOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "RegrasContagem";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A865RegrasContagem_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( StringUtil.StrCmp(A860RegrasContagem_Regra, Z860RegrasContagem_Regra) != 0 ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("regrascontagem:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("regrascontagem:[SecurityCheckFailed value for]"+"RegrasContagem_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A865RegrasContagem_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A860RegrasContagem_Regra = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode108 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode108;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound108 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2Q0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "REGRASCONTAGEM_REGRA");
                        AnyError = 1;
                        GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112Q2 */
                           E112Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122Q2 */
                           E122Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 4);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 54 )
                     {
                        OldWcarquivosevd = cgiGet( "W0054");
                        if ( ( StringUtil.Len( OldWcarquivosevd) == 0 ) || ( StringUtil.StrCmp(OldWcarquivosevd, WebComp_Wcarquivosevd_Component) != 0 ) )
                        {
                           WebComp_Wcarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", OldWcarquivosevd, new Object[] {context} );
                           WebComp_Wcarquivosevd.ComponentInit();
                           WebComp_Wcarquivosevd.Name = "OldWcarquivosevd";
                           WebComp_Wcarquivosevd_Component = OldWcarquivosevd;
                        }
                        if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
                        {
                           WebComp_Wcarquivosevd.componentprocess("W0054", "", sEvt);
                        }
                        WebComp_Wcarquivosevd_Component = OldWcarquivosevd;
                     }
                     else if ( nCmpId == 56 )
                     {
                        OldWcviewarquivosevd = cgiGet( "W0056");
                        if ( ( StringUtil.Len( OldWcviewarquivosevd) == 0 ) || ( StringUtil.StrCmp(OldWcviewarquivosevd, WebComp_Wcviewarquivosevd_Component) != 0 ) )
                        {
                           WebComp_Wcviewarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", OldWcviewarquivosevd, new Object[] {context} );
                           WebComp_Wcviewarquivosevd.ComponentInit();
                           WebComp_Wcviewarquivosevd.Name = "OldWcviewarquivosevd";
                           WebComp_Wcviewarquivosevd_Component = OldWcviewarquivosevd;
                        }
                        if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
                        {
                           WebComp_Wcviewarquivosevd.componentprocess("W0056", "", sEvt);
                        }
                        WebComp_Wcviewarquivosevd_Component = OldWcviewarquivosevd;
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122Q2 */
            E122Q2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2Q108( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2Q108( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2Q0( )
      {
         BeforeValidate2Q108( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2Q108( ) ;
            }
            else
            {
               CheckExtendedTable2Q108( ) ;
               CloseExtendedTableCursors2Q108( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2Q0( )
      {
      }

      protected void E112Q2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "RegrasContagem_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_RegrasContagem_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcarquivosevd_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wcarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcarquivosevd.ComponentInit();
            WebComp_Wcarquivosevd.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcarquivosevd_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
         {
            WebComp_Wcarquivosevd.setjustcreated();
            WebComp_Wcarquivosevd.componentprepare(new Object[] {(String)"W0054",(String)""});
            WebComp_Wcarquivosevd.componentbind(new Object[] {});
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcviewarquivosevd_Component), StringUtil.Lower( "WC_RegrasContagemAnexos")) != 0 )
         {
            WebComp_Wcviewarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", "wc_regrascontagemanexos", new Object[] {context} );
            WebComp_Wcviewarquivosevd.ComponentInit();
            WebComp_Wcviewarquivosevd.Name = "WC_RegrasContagemAnexos";
            WebComp_Wcviewarquivosevd_Component = "WC_RegrasContagemAnexos";
         }
         if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
         {
            WebComp_Wcviewarquivosevd.setjustcreated();
            WebComp_Wcviewarquivosevd.componentprepare(new Object[] {(String)"W0056",(String)"",(String)AV7RegrasContagem_Regra});
            WebComp_Wcviewarquivosevd.componentbind(new Object[] {(String)""});
         }
      }

      protected void E122Q2( )
      {
         /* After Trn Routine */
         new prc_newregracontagemanexo(context ).execute( ref  A860RegrasContagem_Regra) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwregrascontagem.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2Q108( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z861RegrasContagem_Data = T002Q3_A861RegrasContagem_Data[0];
               Z862RegrasContagem_Validade = T002Q3_A862RegrasContagem_Validade[0];
               Z863RegrasContagem_Responsavel = T002Q3_A863RegrasContagem_Responsavel[0];
               Z1004RegrasContagem_Definido = T002Q3_A1004RegrasContagem_Definido[0];
               Z865RegrasContagem_AreaTrabalhoCod = T002Q3_A865RegrasContagem_AreaTrabalhoCod[0];
            }
            else
            {
               Z861RegrasContagem_Data = A861RegrasContagem_Data;
               Z862RegrasContagem_Validade = A862RegrasContagem_Validade;
               Z863RegrasContagem_Responsavel = A863RegrasContagem_Responsavel;
               Z1004RegrasContagem_Definido = A1004RegrasContagem_Definido;
               Z865RegrasContagem_AreaTrabalhoCod = A865RegrasContagem_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -12 )
         {
            Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
            Z861RegrasContagem_Data = A861RegrasContagem_Data;
            Z862RegrasContagem_Validade = A862RegrasContagem_Validade;
            Z863RegrasContagem_Responsavel = A863RegrasContagem_Responsavel;
            Z864RegrasContagem_Descricao = A864RegrasContagem_Descricao;
            Z1004RegrasContagem_Definido = A1004RegrasContagem_Definido;
            Z865RegrasContagem_AreaTrabalhoCod = A865RegrasContagem_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "RegrasContagem";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7RegrasContagem_Regra)) )
         {
            A860RegrasContagem_Regra = AV7RegrasContagem_Regra;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7RegrasContagem_Regra)) )
         {
            edtRegrasContagem_Regra_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Regra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Regra_Enabled), 5, 0)));
         }
         else
         {
            edtRegrasContagem_Regra_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Regra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Regra_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7RegrasContagem_Regra)) )
         {
            edtRegrasContagem_Regra_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Regra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Regra_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         WebComp_Wcarquivosevd_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "gxHTMLWrpW0054"+"", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcarquivosevd_Visible), 5, 0)));
         WebComp_Wcviewarquivosevd_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "gxHTMLWrpW0056"+"", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcviewarquivosevd_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_RegrasContagem_AreaTrabalhoCod) )
         {
            A865RegrasContagem_AreaTrabalhoCod = AV11Insert_RegrasContagem_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A865RegrasContagem_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0)));
            }
         }
      }

      protected void Load2Q108( )
      {
         /* Using cursor T002Q5 */
         pr_default.execute(3, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound108 = 1;
            A861RegrasContagem_Data = T002Q5_A861RegrasContagem_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A861RegrasContagem_Data", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
            A862RegrasContagem_Validade = T002Q5_A862RegrasContagem_Validade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A862RegrasContagem_Validade", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
            n862RegrasContagem_Validade = T002Q5_n862RegrasContagem_Validade[0];
            A863RegrasContagem_Responsavel = T002Q5_A863RegrasContagem_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A863RegrasContagem_Responsavel", A863RegrasContagem_Responsavel);
            A864RegrasContagem_Descricao = T002Q5_A864RegrasContagem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A864RegrasContagem_Descricao", A864RegrasContagem_Descricao);
            A1004RegrasContagem_Definido = T002Q5_A1004RegrasContagem_Definido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1004RegrasContagem_Definido", A1004RegrasContagem_Definido);
            n1004RegrasContagem_Definido = T002Q5_n1004RegrasContagem_Definido[0];
            A865RegrasContagem_AreaTrabalhoCod = T002Q5_A865RegrasContagem_AreaTrabalhoCod[0];
            ZM2Q108( -12) ;
         }
         pr_default.close(3);
         OnLoadActions2Q108( ) ;
      }

      protected void OnLoadActions2Q108( )
      {
      }

      protected void CheckExtendedTable2Q108( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A861RegrasContagem_Data) || ( A861RegrasContagem_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "REGRASCONTAGEM_DATA");
            AnyError = 1;
            GX_FocusControl = edtRegrasContagem_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A862RegrasContagem_Validade) || ( A862RegrasContagem_Validade >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Validade fora do intervalo", "OutOfRange", 1, "REGRASCONTAGEM_VALIDADE");
            AnyError = 1;
            GX_FocusControl = edtRegrasContagem_Validade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002Q4 */
         pr_default.execute(2, new Object[] {A865RegrasContagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Regras Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2Q108( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A865RegrasContagem_AreaTrabalhoCod )
      {
         /* Using cursor T002Q6 */
         pr_default.execute(4, new Object[] {A865RegrasContagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Regras Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey2Q108( )
      {
         /* Using cursor T002Q7 */
         pr_default.execute(5, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound108 = 1;
         }
         else
         {
            RcdFound108 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002Q3 */
         pr_default.execute(1, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2Q108( 12) ;
            RcdFound108 = 1;
            A860RegrasContagem_Regra = T002Q3_A860RegrasContagem_Regra[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
            A861RegrasContagem_Data = T002Q3_A861RegrasContagem_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A861RegrasContagem_Data", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
            A862RegrasContagem_Validade = T002Q3_A862RegrasContagem_Validade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A862RegrasContagem_Validade", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
            n862RegrasContagem_Validade = T002Q3_n862RegrasContagem_Validade[0];
            A863RegrasContagem_Responsavel = T002Q3_A863RegrasContagem_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A863RegrasContagem_Responsavel", A863RegrasContagem_Responsavel);
            A864RegrasContagem_Descricao = T002Q3_A864RegrasContagem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A864RegrasContagem_Descricao", A864RegrasContagem_Descricao);
            A1004RegrasContagem_Definido = T002Q3_A1004RegrasContagem_Definido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1004RegrasContagem_Definido", A1004RegrasContagem_Definido);
            n1004RegrasContagem_Definido = T002Q3_n1004RegrasContagem_Definido[0];
            A865RegrasContagem_AreaTrabalhoCod = T002Q3_A865RegrasContagem_AreaTrabalhoCod[0];
            Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
            sMode108 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2Q108( ) ;
            if ( AnyError == 1 )
            {
               RcdFound108 = 0;
               InitializeNonKey2Q108( ) ;
            }
            Gx_mode = sMode108;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound108 = 0;
            InitializeNonKey2Q108( ) ;
            sMode108 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode108;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2Q108( ) ;
         if ( RcdFound108 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound108 = 0;
         /* Using cursor T002Q8 */
         pr_default.execute(6, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( StringUtil.StrCmp(T002Q8_A860RegrasContagem_Regra[0], A860RegrasContagem_Regra) < 0 ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( StringUtil.StrCmp(T002Q8_A860RegrasContagem_Regra[0], A860RegrasContagem_Regra) > 0 ) ) )
            {
               A860RegrasContagem_Regra = T002Q8_A860RegrasContagem_Regra[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
               RcdFound108 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound108 = 0;
         /* Using cursor T002Q9 */
         pr_default.execute(7, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( StringUtil.StrCmp(T002Q9_A860RegrasContagem_Regra[0], A860RegrasContagem_Regra) > 0 ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( StringUtil.StrCmp(T002Q9_A860RegrasContagem_Regra[0], A860RegrasContagem_Regra) < 0 ) ) )
            {
               A860RegrasContagem_Regra = T002Q9_A860RegrasContagem_Regra[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
               RcdFound108 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2Q108( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtRegrasContagem_Regra_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2Q108( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound108 == 1 )
            {
               if ( StringUtil.StrCmp(A860RegrasContagem_Regra, Z860RegrasContagem_Regra) != 0 )
               {
                  A860RegrasContagem_Regra = Z860RegrasContagem_Regra;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "REGRASCONTAGEM_REGRA");
                  AnyError = 1;
                  GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2Q108( ) ;
                  GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A860RegrasContagem_Regra, Z860RegrasContagem_Regra) != 0 )
               {
                  /* Insert record */
                  GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2Q108( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "REGRASCONTAGEM_REGRA");
                     AnyError = 1;
                     GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtRegrasContagem_Regra_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2Q108( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( StringUtil.StrCmp(A860RegrasContagem_Regra, Z860RegrasContagem_Regra) != 0 )
         {
            A860RegrasContagem_Regra = Z860RegrasContagem_Regra;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "REGRASCONTAGEM_REGRA");
            AnyError = 1;
            GX_FocusControl = edtRegrasContagem_Regra_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtRegrasContagem_Regra_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2Q108( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002Q2 */
            pr_default.execute(0, new Object[] {A860RegrasContagem_Regra});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagem"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z861RegrasContagem_Data != T002Q2_A861RegrasContagem_Data[0] ) || ( Z862RegrasContagem_Validade != T002Q2_A862RegrasContagem_Validade[0] ) || ( StringUtil.StrCmp(Z863RegrasContagem_Responsavel, T002Q2_A863RegrasContagem_Responsavel[0]) != 0 ) || ( StringUtil.StrCmp(Z1004RegrasContagem_Definido, T002Q2_A1004RegrasContagem_Definido[0]) != 0 ) || ( Z865RegrasContagem_AreaTrabalhoCod != T002Q2_A865RegrasContagem_AreaTrabalhoCod[0] ) )
            {
               if ( Z861RegrasContagem_Data != T002Q2_A861RegrasContagem_Data[0] )
               {
                  GXUtil.WriteLog("regrascontagem:[seudo value changed for attri]"+"RegrasContagem_Data");
                  GXUtil.WriteLogRaw("Old: ",Z861RegrasContagem_Data);
                  GXUtil.WriteLogRaw("Current: ",T002Q2_A861RegrasContagem_Data[0]);
               }
               if ( Z862RegrasContagem_Validade != T002Q2_A862RegrasContagem_Validade[0] )
               {
                  GXUtil.WriteLog("regrascontagem:[seudo value changed for attri]"+"RegrasContagem_Validade");
                  GXUtil.WriteLogRaw("Old: ",Z862RegrasContagem_Validade);
                  GXUtil.WriteLogRaw("Current: ",T002Q2_A862RegrasContagem_Validade[0]);
               }
               if ( StringUtil.StrCmp(Z863RegrasContagem_Responsavel, T002Q2_A863RegrasContagem_Responsavel[0]) != 0 )
               {
                  GXUtil.WriteLog("regrascontagem:[seudo value changed for attri]"+"RegrasContagem_Responsavel");
                  GXUtil.WriteLogRaw("Old: ",Z863RegrasContagem_Responsavel);
                  GXUtil.WriteLogRaw("Current: ",T002Q2_A863RegrasContagem_Responsavel[0]);
               }
               if ( StringUtil.StrCmp(Z1004RegrasContagem_Definido, T002Q2_A1004RegrasContagem_Definido[0]) != 0 )
               {
                  GXUtil.WriteLog("regrascontagem:[seudo value changed for attri]"+"RegrasContagem_Definido");
                  GXUtil.WriteLogRaw("Old: ",Z1004RegrasContagem_Definido);
                  GXUtil.WriteLogRaw("Current: ",T002Q2_A1004RegrasContagem_Definido[0]);
               }
               if ( Z865RegrasContagem_AreaTrabalhoCod != T002Q2_A865RegrasContagem_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("regrascontagem:[seudo value changed for attri]"+"RegrasContagem_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z865RegrasContagem_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T002Q2_A865RegrasContagem_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RegrasContagem"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2Q108( )
      {
         BeforeValidate2Q108( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Q108( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2Q108( 0) ;
            CheckOptimisticConcurrency2Q108( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Q108( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2Q108( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Q10 */
                     pr_default.execute(8, new Object[] {A860RegrasContagem_Regra, A861RegrasContagem_Data, n862RegrasContagem_Validade, A862RegrasContagem_Validade, A863RegrasContagem_Responsavel, A864RegrasContagem_Descricao, n1004RegrasContagem_Definido, A1004RegrasContagem_Definido, A865RegrasContagem_AreaTrabalhoCod});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagem") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2Q0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2Q108( ) ;
            }
            EndLevel2Q108( ) ;
         }
         CloseExtendedTableCursors2Q108( ) ;
      }

      protected void Update2Q108( )
      {
         BeforeValidate2Q108( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Q108( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Q108( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Q108( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2Q108( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Q11 */
                     pr_default.execute(9, new Object[] {A861RegrasContagem_Data, n862RegrasContagem_Validade, A862RegrasContagem_Validade, A863RegrasContagem_Responsavel, A864RegrasContagem_Descricao, n1004RegrasContagem_Definido, A1004RegrasContagem_Definido, A865RegrasContagem_AreaTrabalhoCod, A860RegrasContagem_Regra});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagem") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagem"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2Q108( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2Q108( ) ;
         }
         CloseExtendedTableCursors2Q108( ) ;
      }

      protected void DeferredUpdate2Q108( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2Q108( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Q108( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2Q108( ) ;
            AfterConfirm2Q108( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2Q108( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002Q12 */
                  pr_default.execute(10, new Object[] {A860RegrasContagem_Regra});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("RegrasContagem") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode108 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2Q108( ) ;
         Gx_mode = sMode108;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2Q108( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T002Q13 */
            pr_default.execute(11, new Object[] {A860RegrasContagem_Regra});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
         }
      }

      protected void EndLevel2Q108( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2Q108( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "RegrasContagem");
            if ( AnyError == 0 )
            {
               ConfirmValues2Q0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "RegrasContagem");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2Q108( )
      {
         /* Scan By routine */
         /* Using cursor T002Q14 */
         pr_default.execute(12);
         RcdFound108 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound108 = 1;
            A860RegrasContagem_Regra = T002Q14_A860RegrasContagem_Regra[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2Q108( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound108 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound108 = 1;
            A860RegrasContagem_Regra = T002Q14_A860RegrasContagem_Regra[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         }
      }

      protected void ScanEnd2Q108( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm2Q108( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2Q108( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2Q108( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2Q108( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2Q108( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2Q108( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2Q108( )
      {
         edtRegrasContagem_Regra_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Regra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Regra_Enabled), 5, 0)));
         edtRegrasContagem_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Data_Enabled), 5, 0)));
         edtRegrasContagem_Validade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Validade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Validade_Enabled), 5, 0)));
         edtRegrasContagem_Responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Responsavel_Enabled), 5, 0)));
         edtRegrasContagem_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRegrasContagem_Descricao_Enabled), 5, 0)));
         radRegrasContagem_Definido.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radRegrasContagem_Definido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(radRegrasContagem_Definido.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2Q0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117234737");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("regrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV7RegrasContagem_Regra))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z860RegrasContagem_Regra", Z860RegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "Z861RegrasContagem_Data", context.localUtil.DToC( Z861RegrasContagem_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z862RegrasContagem_Validade", context.localUtil.DToC( Z862RegrasContagem_Validade, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z863RegrasContagem_Responsavel", StringUtil.RTrim( Z863RegrasContagem_Responsavel));
         GxWebStd.gx_hidden_field( context, "Z1004RegrasContagem_Definido", StringUtil.RTrim( Z1004RegrasContagem_Definido));
         GxWebStd.gx_hidden_field( context, "Z865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z865RegrasContagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vREGRASCONTAGEM_REGRA", AV7RegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "vINSERT_REGRASCONTAGEM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_RegrasContagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREGRASCONTAGEM_REGRA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7RegrasContagem_Regra, ""))));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "RegrasContagem";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A865RegrasContagem_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("regrascontagem:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("regrascontagem:[SendSecurityCheck value for]"+"RegrasContagem_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A865RegrasContagem_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcarquivosevd == null ) )
         {
            WebComp_Wcarquivosevd.componentjscripts();
         }
         if ( ! ( WebComp_Wcviewarquivosevd == null ) )
         {
            WebComp_Wcviewarquivosevd.componentjscripts();
         }
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  WebComp_Wcarquivosevd.componentstart();
               }
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcviewarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
               {
                  WebComp_Wcviewarquivosevd.componentstart();
               }
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  WebComp_Wcarquivosevd.componentstart();
               }
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( WebComp_Wcviewarquivosevd_Visible != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
               {
                  WebComp_Wcviewarquivosevd.componentstart();
               }
            }
         }
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("regrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV7RegrasContagem_Regra)) ;
      }

      public override String GetPgmname( )
      {
         return "RegrasContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Regras de Contagem" ;
      }

      protected void InitializeNonKey2Q108( )
      {
         A861RegrasContagem_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A861RegrasContagem_Data", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
         A862RegrasContagem_Validade = DateTime.MinValue;
         n862RegrasContagem_Validade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A862RegrasContagem_Validade", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
         n862RegrasContagem_Validade = ((DateTime.MinValue==A862RegrasContagem_Validade) ? true : false);
         A863RegrasContagem_Responsavel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A863RegrasContagem_Responsavel", A863RegrasContagem_Responsavel);
         A864RegrasContagem_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A864RegrasContagem_Descricao", A864RegrasContagem_Descricao);
         A1004RegrasContagem_Definido = "";
         n1004RegrasContagem_Definido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1004RegrasContagem_Definido", A1004RegrasContagem_Definido);
         n1004RegrasContagem_Definido = (String.IsNullOrEmpty(StringUtil.RTrim( A1004RegrasContagem_Definido)) ? true : false);
         A865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0)));
         Z861RegrasContagem_Data = DateTime.MinValue;
         Z862RegrasContagem_Validade = DateTime.MinValue;
         Z863RegrasContagem_Responsavel = "";
         Z1004RegrasContagem_Definido = "";
         Z865RegrasContagem_AreaTrabalhoCod = 0;
      }

      protected void InitAll2Q108( )
      {
         A860RegrasContagem_Regra = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         InitializeNonKey2Q108( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A865RegrasContagem_AreaTrabalhoCod = i865RegrasContagem_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A865RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A865RegrasContagem_AreaTrabalhoCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Wcarquivosevd == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
            {
               WebComp_Wcarquivosevd.componentthemes();
            }
         }
         if ( ! ( WebComp_Wcviewarquivosevd == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcviewarquivosevd_Component) != 0 )
            {
               WebComp_Wcviewarquivosevd.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117234765");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("regrascontagem.js", "?20203117234765");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockregrascontagem_regra_Internalname = "TEXTBLOCKREGRASCONTAGEM_REGRA";
         edtRegrasContagem_Regra_Internalname = "REGRASCONTAGEM_REGRA";
         lblTextblockregrascontagem_data_Internalname = "TEXTBLOCKREGRASCONTAGEM_DATA";
         edtRegrasContagem_Data_Internalname = "REGRASCONTAGEM_DATA";
         lblTextblockregrascontagem_validade_Internalname = "TEXTBLOCKREGRASCONTAGEM_VALIDADE";
         edtRegrasContagem_Validade_Internalname = "REGRASCONTAGEM_VALIDADE";
         lblTextblockregrascontagem_responsavel_Internalname = "TEXTBLOCKREGRASCONTAGEM_RESPONSAVEL";
         edtRegrasContagem_Responsavel_Internalname = "REGRASCONTAGEM_RESPONSAVEL";
         lblTextblockregrascontagem_descricao_Internalname = "TEXTBLOCKREGRASCONTAGEM_DESCRICAO";
         edtRegrasContagem_Descricao_Internalname = "REGRASCONTAGEM_DESCRICAO";
         lblTextblockregrascontagem_definido_Internalname = "TEXTBLOCKREGRASCONTAGEM_DEFINIDO";
         radRegrasContagem_Definido_Internalname = "REGRASCONTAGEM_DEFINIDO";
         tblTblarqevd_Internalname = "TBLARQEVD";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         grpUnnamedgroup2_Internalname = "UNNAMEDGROUP2";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Regras de Contagem";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Regras de Contagem";
         WebComp_Wcviewarquivosevd_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "gxHTMLWrpW0056"+"", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcviewarquivosevd_Visible), 5, 0)));
         WebComp_Wcarquivosevd_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "gxHTMLWrpW0054"+"", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(WebComp_Wcarquivosevd_Visible), 5, 0)));
         radRegrasContagem_Definido_Jsonclick = "";
         radRegrasContagem_Definido.Enabled = 1;
         edtRegrasContagem_Descricao_Enabled = 1;
         edtRegrasContagem_Responsavel_Jsonclick = "";
         edtRegrasContagem_Responsavel_Enabled = 1;
         edtRegrasContagem_Validade_Jsonclick = "";
         edtRegrasContagem_Validade_Enabled = 1;
         edtRegrasContagem_Data_Jsonclick = "";
         edtRegrasContagem_Data_Enabled = 1;
         edtRegrasContagem_Regra_Jsonclick = "";
         edtRegrasContagem_Regra_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7RegrasContagem_Regra',fld:'vREGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122Q2',iparms:[{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7RegrasContagem_Regra = "";
         Z860RegrasContagem_Regra = "";
         Z861RegrasContagem_Data = DateTime.MinValue;
         Z862RegrasContagem_Validade = DateTime.MinValue;
         Z863RegrasContagem_Responsavel = "";
         Z1004RegrasContagem_Definido = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockregrascontagem_regra_Jsonclick = "";
         A860RegrasContagem_Regra = "";
         lblTextblockregrascontagem_data_Jsonclick = "";
         A861RegrasContagem_Data = DateTime.MinValue;
         lblTextblockregrascontagem_validade_Jsonclick = "";
         A862RegrasContagem_Validade = DateTime.MinValue;
         lblTextblockregrascontagem_responsavel_Jsonclick = "";
         A863RegrasContagem_Responsavel = "";
         lblTextblockregrascontagem_descricao_Jsonclick = "";
         A864RegrasContagem_Descricao = "";
         lblTextblockregrascontagem_definido_Jsonclick = "";
         A1004RegrasContagem_Definido = "";
         WebComp_Wcarquivosevd_Component = "";
         OldWcarquivosevd = "";
         WebComp_Wcviewarquivosevd_Component = "";
         OldWcviewarquivosevd = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode108 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z864RegrasContagem_Descricao = "";
         T002Q5_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q5_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         T002Q5_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         T002Q5_n862RegrasContagem_Validade = new bool[] {false} ;
         T002Q5_A863RegrasContagem_Responsavel = new String[] {""} ;
         T002Q5_A864RegrasContagem_Descricao = new String[] {""} ;
         T002Q5_A1004RegrasContagem_Definido = new String[] {""} ;
         T002Q5_n1004RegrasContagem_Definido = new bool[] {false} ;
         T002Q5_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         T002Q4_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         T002Q6_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         T002Q7_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q3_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q3_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         T002Q3_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         T002Q3_n862RegrasContagem_Validade = new bool[] {false} ;
         T002Q3_A863RegrasContagem_Responsavel = new String[] {""} ;
         T002Q3_A864RegrasContagem_Descricao = new String[] {""} ;
         T002Q3_A1004RegrasContagem_Definido = new String[] {""} ;
         T002Q3_n1004RegrasContagem_Definido = new bool[] {false} ;
         T002Q3_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         T002Q8_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q9_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q2_A860RegrasContagem_Regra = new String[] {""} ;
         T002Q2_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         T002Q2_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         T002Q2_n862RegrasContagem_Validade = new bool[] {false} ;
         T002Q2_A863RegrasContagem_Responsavel = new String[] {""} ;
         T002Q2_A864RegrasContagem_Descricao = new String[] {""} ;
         T002Q2_A1004RegrasContagem_Definido = new String[] {""} ;
         T002Q2_n1004RegrasContagem_Definido = new bool[] {false} ;
         T002Q2_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         T002Q13_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T002Q14_A860RegrasContagem_Regra = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.regrascontagem__default(),
            new Object[][] {
                new Object[] {
               T002Q2_A860RegrasContagem_Regra, T002Q2_A861RegrasContagem_Data, T002Q2_A862RegrasContagem_Validade, T002Q2_n862RegrasContagem_Validade, T002Q2_A863RegrasContagem_Responsavel, T002Q2_A864RegrasContagem_Descricao, T002Q2_A1004RegrasContagem_Definido, T002Q2_n1004RegrasContagem_Definido, T002Q2_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               T002Q3_A860RegrasContagem_Regra, T002Q3_A861RegrasContagem_Data, T002Q3_A862RegrasContagem_Validade, T002Q3_n862RegrasContagem_Validade, T002Q3_A863RegrasContagem_Responsavel, T002Q3_A864RegrasContagem_Descricao, T002Q3_A1004RegrasContagem_Definido, T002Q3_n1004RegrasContagem_Definido, T002Q3_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               T002Q4_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               T002Q5_A860RegrasContagem_Regra, T002Q5_A861RegrasContagem_Data, T002Q5_A862RegrasContagem_Validade, T002Q5_n862RegrasContagem_Validade, T002Q5_A863RegrasContagem_Responsavel, T002Q5_A864RegrasContagem_Descricao, T002Q5_A1004RegrasContagem_Definido, T002Q5_n1004RegrasContagem_Definido, T002Q5_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               T002Q6_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               T002Q7_A860RegrasContagem_Regra
               }
               , new Object[] {
               T002Q8_A860RegrasContagem_Regra
               }
               , new Object[] {
               T002Q9_A860RegrasContagem_Regra
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002Q13_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               T002Q14_A860RegrasContagem_Regra
               }
            }
         );
         WebComp_Wcarquivosevd = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcviewarquivosevd = new GeneXus.Http.GXNullWebComponent();
         AV14Pgmname = "RegrasContagem";
         Z865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A865RegrasContagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound108 ;
      private short nCmpId ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int Z865RegrasContagem_AreaTrabalhoCod ;
      private int N865RegrasContagem_AreaTrabalhoCod ;
      private int A865RegrasContagem_AreaTrabalhoCod ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtRegrasContagem_Regra_Enabled ;
      private int edtRegrasContagem_Data_Enabled ;
      private int edtRegrasContagem_Validade_Enabled ;
      private int edtRegrasContagem_Responsavel_Enabled ;
      private int edtRegrasContagem_Descricao_Enabled ;
      private int WebComp_Wcarquivosevd_Visible ;
      private int WebComp_Wcviewarquivosevd_Visible ;
      private int AV11Insert_RegrasContagem_AreaTrabalhoCod ;
      private int AV15GXV1 ;
      private int i865RegrasContagem_AreaTrabalhoCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z863RegrasContagem_Responsavel ;
      private String Z1004RegrasContagem_Definido ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtRegrasContagem_Regra_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockregrascontagem_regra_Internalname ;
      private String lblTextblockregrascontagem_regra_Jsonclick ;
      private String edtRegrasContagem_Regra_Jsonclick ;
      private String lblTextblockregrascontagem_data_Internalname ;
      private String lblTextblockregrascontagem_data_Jsonclick ;
      private String edtRegrasContagem_Data_Internalname ;
      private String edtRegrasContagem_Data_Jsonclick ;
      private String lblTextblockregrascontagem_validade_Internalname ;
      private String lblTextblockregrascontagem_validade_Jsonclick ;
      private String edtRegrasContagem_Validade_Internalname ;
      private String edtRegrasContagem_Validade_Jsonclick ;
      private String lblTextblockregrascontagem_responsavel_Internalname ;
      private String lblTextblockregrascontagem_responsavel_Jsonclick ;
      private String edtRegrasContagem_Responsavel_Internalname ;
      private String A863RegrasContagem_Responsavel ;
      private String edtRegrasContagem_Responsavel_Jsonclick ;
      private String lblTextblockregrascontagem_descricao_Internalname ;
      private String lblTextblockregrascontagem_descricao_Jsonclick ;
      private String edtRegrasContagem_Descricao_Internalname ;
      private String lblTextblockregrascontagem_definido_Internalname ;
      private String lblTextblockregrascontagem_definido_Jsonclick ;
      private String radRegrasContagem_Definido_Internalname ;
      private String A1004RegrasContagem_Definido ;
      private String radRegrasContagem_Definido_Jsonclick ;
      private String grpUnnamedgroup2_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String tblTblarqevd_Internalname ;
      private String WebComp_Wcarquivosevd_Component ;
      private String OldWcarquivosevd ;
      private String WebComp_Wcviewarquivosevd_Component ;
      private String OldWcviewarquivosevd ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode108 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z861RegrasContagem_Data ;
      private DateTime Z862RegrasContagem_Validade ;
      private DateTime A861RegrasContagem_Data ;
      private DateTime A862RegrasContagem_Validade ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n862RegrasContagem_Validade ;
      private bool n1004RegrasContagem_Definido ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A864RegrasContagem_Descricao ;
      private String Z864RegrasContagem_Descricao ;
      private String wcpOAV7RegrasContagem_Regra ;
      private String Z860RegrasContagem_Regra ;
      private String AV7RegrasContagem_Regra ;
      private String A860RegrasContagem_Regra ;
      private IGxSession AV10WebSession ;
      private GXWebComponent WebComp_Wcarquivosevd ;
      private GXWebComponent WebComp_Wcviewarquivosevd ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXRadio radRegrasContagem_Definido ;
      private IDataStoreProvider pr_default ;
      private String[] T002Q5_A860RegrasContagem_Regra ;
      private DateTime[] T002Q5_A861RegrasContagem_Data ;
      private DateTime[] T002Q5_A862RegrasContagem_Validade ;
      private bool[] T002Q5_n862RegrasContagem_Validade ;
      private String[] T002Q5_A863RegrasContagem_Responsavel ;
      private String[] T002Q5_A864RegrasContagem_Descricao ;
      private String[] T002Q5_A1004RegrasContagem_Definido ;
      private bool[] T002Q5_n1004RegrasContagem_Definido ;
      private int[] T002Q5_A865RegrasContagem_AreaTrabalhoCod ;
      private int[] T002Q4_A865RegrasContagem_AreaTrabalhoCod ;
      private int[] T002Q6_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] T002Q7_A860RegrasContagem_Regra ;
      private String[] T002Q3_A860RegrasContagem_Regra ;
      private DateTime[] T002Q3_A861RegrasContagem_Data ;
      private DateTime[] T002Q3_A862RegrasContagem_Validade ;
      private bool[] T002Q3_n862RegrasContagem_Validade ;
      private String[] T002Q3_A863RegrasContagem_Responsavel ;
      private String[] T002Q3_A864RegrasContagem_Descricao ;
      private String[] T002Q3_A1004RegrasContagem_Definido ;
      private bool[] T002Q3_n1004RegrasContagem_Definido ;
      private int[] T002Q3_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] T002Q8_A860RegrasContagem_Regra ;
      private String[] T002Q9_A860RegrasContagem_Regra ;
      private String[] T002Q2_A860RegrasContagem_Regra ;
      private DateTime[] T002Q2_A861RegrasContagem_Data ;
      private DateTime[] T002Q2_A862RegrasContagem_Validade ;
      private bool[] T002Q2_n862RegrasContagem_Validade ;
      private String[] T002Q2_A863RegrasContagem_Responsavel ;
      private String[] T002Q2_A864RegrasContagem_Descricao ;
      private String[] T002Q2_A1004RegrasContagem_Definido ;
      private bool[] T002Q2_n1004RegrasContagem_Definido ;
      private int[] T002Q2_A865RegrasContagem_AreaTrabalhoCod ;
      private int[] T002Q13_A1005RegrasContagemAnexo_Codigo ;
      private String[] T002Q14_A860RegrasContagem_Regra ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class regrascontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002Q5 ;
          prmT002Q5 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q4 ;
          prmT002Q4 = new Object[] {
          new Object[] {"@RegrasContagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Q6 ;
          prmT002Q6 = new Object[] {
          new Object[] {"@RegrasContagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Q7 ;
          prmT002Q7 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q3 ;
          prmT002Q3 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q8 ;
          prmT002Q8 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q9 ;
          prmT002Q9 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q2 ;
          prmT002Q2 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q10 ;
          prmT002Q10 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@RegrasContagem_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagem_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagem_Responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagem_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@RegrasContagem_Definido",SqlDbType.Char,1,0} ,
          new Object[] {"@RegrasContagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Q11 ;
          prmT002Q11 = new Object[] {
          new Object[] {"@RegrasContagem_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagem_Validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagem_Responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagem_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@RegrasContagem_Definido",SqlDbType.Char,1,0} ,
          new Object[] {"@RegrasContagem_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q12 ;
          prmT002Q12 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q13 ;
          prmT002Q13 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT002Q14 ;
          prmT002Q14 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T002Q2", "SELECT [RegrasContagem_Regra], [RegrasContagem_Data], [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Descricao], [RegrasContagem_Definido], [RegrasContagem_AreaTrabalhoCod] AS RegrasContagem_AreaTrabalhoCod FROM [RegrasContagem] WITH (UPDLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q2,1,0,true,false )
             ,new CursorDef("T002Q3", "SELECT [RegrasContagem_Regra], [RegrasContagem_Data], [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Descricao], [RegrasContagem_Definido], [RegrasContagem_AreaTrabalhoCod] AS RegrasContagem_AreaTrabalhoCod FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q3,1,0,true,false )
             ,new CursorDef("T002Q4", "SELECT [AreaTrabalho_Codigo] AS RegrasContagem_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @RegrasContagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q4,1,0,true,false )
             ,new CursorDef("T002Q5", "SELECT TM1.[RegrasContagem_Regra], TM1.[RegrasContagem_Data], TM1.[RegrasContagem_Validade], TM1.[RegrasContagem_Responsavel], TM1.[RegrasContagem_Descricao], TM1.[RegrasContagem_Definido], TM1.[RegrasContagem_AreaTrabalhoCod] AS RegrasContagem_AreaTrabalhoCod FROM [RegrasContagem] TM1 WITH (NOLOCK) WHERE TM1.[RegrasContagem_Regra] = @RegrasContagem_Regra ORDER BY TM1.[RegrasContagem_Regra]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q5,100,0,true,false )
             ,new CursorDef("T002Q6", "SELECT [AreaTrabalho_Codigo] AS RegrasContagem_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @RegrasContagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q6,1,0,true,false )
             ,new CursorDef("T002Q7", "SELECT [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q7,1,0,true,false )
             ,new CursorDef("T002Q8", "SELECT TOP 1 [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE ( [RegrasContagem_Regra] > @RegrasContagem_Regra) ORDER BY [RegrasContagem_Regra]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q8,1,0,true,true )
             ,new CursorDef("T002Q9", "SELECT TOP 1 [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE ( [RegrasContagem_Regra] < @RegrasContagem_Regra) ORDER BY [RegrasContagem_Regra] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q9,1,0,true,true )
             ,new CursorDef("T002Q10", "INSERT INTO [RegrasContagem]([RegrasContagem_Regra], [RegrasContagem_Data], [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Descricao], [RegrasContagem_Definido], [RegrasContagem_AreaTrabalhoCod]) VALUES(@RegrasContagem_Regra, @RegrasContagem_Data, @RegrasContagem_Validade, @RegrasContagem_Responsavel, @RegrasContagem_Descricao, @RegrasContagem_Definido, @RegrasContagem_AreaTrabalhoCod)", GxErrorMask.GX_NOMASK,prmT002Q10)
             ,new CursorDef("T002Q11", "UPDATE [RegrasContagem] SET [RegrasContagem_Data]=@RegrasContagem_Data, [RegrasContagem_Validade]=@RegrasContagem_Validade, [RegrasContagem_Responsavel]=@RegrasContagem_Responsavel, [RegrasContagem_Descricao]=@RegrasContagem_Descricao, [RegrasContagem_Definido]=@RegrasContagem_Definido, [RegrasContagem_AreaTrabalhoCod]=@RegrasContagem_AreaTrabalhoCod  WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra", GxErrorMask.GX_NOMASK,prmT002Q11)
             ,new CursorDef("T002Q12", "DELETE FROM [RegrasContagem]  WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra", GxErrorMask.GX_NOMASK,prmT002Q12)
             ,new CursorDef("T002Q13", "SELECT TOP 1 [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q13,1,0,true,true )
             ,new CursorDef("T002Q14", "SELECT [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) ORDER BY [RegrasContagem_Regra]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Q14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[3]);
                }
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                stmt.SetParameter(7, (int)parms[8]);
                return;
             case 9 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                stmt.SetParameter(6, (int)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
