/*
               File: WC_SistemaContagens
        Description: Sistema Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:0.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_sistemacontagens : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_sistemacontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_sistemacontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Sistema_Codigo )
      {
         this.AV5Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
         aP0_Sistema_Codigo=this.AV5Sistema_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbProjeto_TipoContagem = new GXCombobox();
         cmbProjeto_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV5Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV5Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_2_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_2_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  AV5Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
                  A740Projeto_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( AV5Sistema_Codigo, A740Projeto_SistemaCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEE2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSEE2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Contagens") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020326912072");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_sistemacontagens.aspx") + "?" + UrlEncode("" +AV5Sistema_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_2", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV5Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV5Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Sistema_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEE2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_sistemacontagens.js", "?2020326912073");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_SistemaContagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Contagens" ;
      }

      protected void WBEE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_sistemacontagens.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"2\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sigla") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(42), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prazo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(154), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Custo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Escopo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(45), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Esfor�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A650Projeto_Sigla));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtProjeto_Sigla_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A653Projeto_Escopo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A651Projeto_TipoContagem));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A658Projeto_Status));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 2 )
         {
            wbEnd = 0;
            nRC_GXsfl_2 = (short)(nGXsfl_2_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
         }
         wbLoad = true;
      }

      protected void STARTEE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Contagens", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEE0( ) ;
            }
         }
      }

      protected void WSEE2( )
      {
         STARTEE2( ) ;
         EVTEE2( ) ;
      }

      protected void EVTEE2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEE0( ) ;
                              }
                              nGXsfl_2_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
                              SubsflControlProps_22( ) ;
                              A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
                              A740Projeto_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtProjeto_SistemaCod_Internalname), ",", "."));
                              A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
                              A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Prazo_Internalname), ",", "."));
                              A655Projeto_Custo = context.localUtil.CToN( cgiGet( edtProjeto_Custo_Internalname), ",", ".");
                              A653Projeto_Escopo = cgiGet( edtProjeto_Escopo_Internalname);
                              A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Esforco_Internalname), ",", "."));
                              cmbProjeto_TipoContagem.Name = cmbProjeto_TipoContagem_Internalname;
                              cmbProjeto_TipoContagem.CurrentValue = cgiGet( cmbProjeto_TipoContagem_Internalname);
                              A651Projeto_TipoContagem = cgiGet( cmbProjeto_TipoContagem_Internalname);
                              cmbProjeto_Status.Name = cmbProjeto_Status_Internalname;
                              cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
                              A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E11EE2 */
                                          E11EE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPEE0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEE2( ) ;
            }
         }
      }

      protected void PAEE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "PROJETO_TIPOCONTAGEM_" + sGXsfl_2_idx;
            cmbProjeto_TipoContagem.Name = GXCCtl;
            cmbProjeto_TipoContagem.WebTags = "";
            cmbProjeto_TipoContagem.addItem("", "(Nenhum)", 0);
            cmbProjeto_TipoContagem.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbProjeto_TipoContagem.addItem("M", "Projeto de Melhor�a", 0);
            cmbProjeto_TipoContagem.addItem("C", "Projeto de Contagem", 0);
            cmbProjeto_TipoContagem.addItem("A", "Aplica��o", 0);
            if ( cmbProjeto_TipoContagem.ItemCount > 0 )
            {
               A651Projeto_TipoContagem = cmbProjeto_TipoContagem.getValidValue(A651Projeto_TipoContagem);
            }
            GXCCtl = "PROJETO_STATUS_" + sGXsfl_2_idx;
            cmbProjeto_Status.Name = GXCCtl;
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_22( ) ;
         while ( nGXsfl_2_idx <= nRC_GXsfl_2 )
         {
            sendrow_22( ) ;
            nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
            sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
            SubsflControlProps_22( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int AV5Sistema_Codigo ,
                                       int A740Projeto_SistemaCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFEE2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A740Projeto_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_SIGLA", StringUtil.RTrim( A650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_ESCOPO", A653Projeto_Escopo);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_TIPOCONTAGEM", StringUtil.RTrim( A651Projeto_TipoContagem));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_STATUS", StringUtil.RTrim( A658Projeto_Status));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFEE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 2;
         nGXsfl_2_idx = 1;
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         nGXsfl_2_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_22( ) ;
            /* Using cursor H00EE3 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A658Projeto_Status = H00EE3_A658Projeto_Status[0];
               A651Projeto_TipoContagem = H00EE3_A651Projeto_TipoContagem[0];
               A653Projeto_Escopo = H00EE3_A653Projeto_Escopo[0];
               A650Projeto_Sigla = H00EE3_A650Projeto_Sigla[0];
               A657Projeto_Esforco = H00EE3_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00EE3_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00EE3_A656Projeto_Prazo[0];
               A648Projeto_Codigo = H00EE3_A648Projeto_Codigo[0];
               A657Projeto_Esforco = H00EE3_A657Projeto_Esforco[0];
               A655Projeto_Custo = H00EE3_A655Projeto_Custo[0];
               A656Projeto_Prazo = H00EE3_A656Projeto_Prazo[0];
               GXt_int1 = A740Projeto_SistemaCod;
               new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
               A740Projeto_SistemaCod = GXt_int1;
               if ( A740Projeto_SistemaCod == AV5Sistema_Codigo )
               {
                  /* Execute user event: E11EE2 */
                  E11EE2 ();
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 2;
            WBEE0( ) ;
         }
         nGXsfl_2_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPEE0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_2 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_2"), ",", "."));
            wcpOAV5Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV5Sistema_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      private void E11EE2( )
      {
         /* Load Routine */
         edtProjeto_Sigla_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A740Projeto_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         sendrow_22( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_2_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(2, GridRow);
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEE2( ) ;
         WSEE2( ) ;
         WEEE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV5Sistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEE2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_sistemacontagens");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEE2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV5Sistema_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
         }
         wcpOAV5Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV5Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV5Sistema_Codigo != wcpOAV5Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV5Sistema_Codigo = AV5Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV5Sistema_Codigo = cgiGet( sPrefix+"AV5Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV5Sistema_Codigo) > 0 )
         {
            AV5Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV5Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
         }
         else
         {
            AV5Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV5Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEE2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEE2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV5Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV5Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV5Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlAV5Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020326912117");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_sistemacontagens.js", "?2020326912118");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_22( )
      {
         edtProjeto_Codigo_Internalname = sPrefix+"PROJETO_CODIGO_"+sGXsfl_2_idx;
         edtProjeto_SistemaCod_Internalname = sPrefix+"PROJETO_SISTEMACOD_"+sGXsfl_2_idx;
         edtProjeto_Sigla_Internalname = sPrefix+"PROJETO_SIGLA_"+sGXsfl_2_idx;
         edtProjeto_Prazo_Internalname = sPrefix+"PROJETO_PRAZO_"+sGXsfl_2_idx;
         edtProjeto_Custo_Internalname = sPrefix+"PROJETO_CUSTO_"+sGXsfl_2_idx;
         edtProjeto_Escopo_Internalname = sPrefix+"PROJETO_ESCOPO_"+sGXsfl_2_idx;
         edtProjeto_Esforco_Internalname = sPrefix+"PROJETO_ESFORCO_"+sGXsfl_2_idx;
         cmbProjeto_TipoContagem_Internalname = sPrefix+"PROJETO_TIPOCONTAGEM_"+sGXsfl_2_idx;
         cmbProjeto_Status_Internalname = sPrefix+"PROJETO_STATUS_"+sGXsfl_2_idx;
      }

      protected void SubsflControlProps_fel_22( )
      {
         edtProjeto_Codigo_Internalname = sPrefix+"PROJETO_CODIGO_"+sGXsfl_2_fel_idx;
         edtProjeto_SistemaCod_Internalname = sPrefix+"PROJETO_SISTEMACOD_"+sGXsfl_2_fel_idx;
         edtProjeto_Sigla_Internalname = sPrefix+"PROJETO_SIGLA_"+sGXsfl_2_fel_idx;
         edtProjeto_Prazo_Internalname = sPrefix+"PROJETO_PRAZO_"+sGXsfl_2_fel_idx;
         edtProjeto_Custo_Internalname = sPrefix+"PROJETO_CUSTO_"+sGXsfl_2_fel_idx;
         edtProjeto_Escopo_Internalname = sPrefix+"PROJETO_ESCOPO_"+sGXsfl_2_fel_idx;
         edtProjeto_Esforco_Internalname = sPrefix+"PROJETO_ESFORCO_"+sGXsfl_2_fel_idx;
         cmbProjeto_TipoContagem_Internalname = sPrefix+"PROJETO_TIPOCONTAGEM_"+sGXsfl_2_fel_idx;
         cmbProjeto_Status_Internalname = sPrefix+"PROJETO_STATUS_"+sGXsfl_2_fel_idx;
      }

      protected void sendrow_22( )
      {
         SubsflControlProps_22( ) ;
         WBEE0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_2_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_2_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A740Projeto_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_SistemaCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Sigla_Internalname,StringUtil.RTrim( A650Projeto_Sigla),StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtProjeto_Sigla_Link,(String)"",(String)"",(String)"",(String)edtProjeto_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)-1,(bool)false,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A656Projeto_Prazo), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)42,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Custo_Internalname,StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ",", "")),context.localUtil.Format( A655Projeto_Custo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Custo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)154,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"Valor2Casas",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Escopo_Internalname,(String)A653Projeto_Escopo,(String)A653Projeto_Escopo,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Escopo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)570,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)2,(short)1,(short)0,(short)-1,(bool)false,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Esforco_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A657Projeto_Esforco), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Esforco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)45,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_2_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "PROJETO_TIPOCONTAGEM_" + sGXsfl_2_idx;
            cmbProjeto_TipoContagem.Name = GXCCtl;
            cmbProjeto_TipoContagem.WebTags = "";
            cmbProjeto_TipoContagem.addItem("", "(Nenhum)", 0);
            cmbProjeto_TipoContagem.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbProjeto_TipoContagem.addItem("M", "Projeto de Melhor�a", 0);
            cmbProjeto_TipoContagem.addItem("C", "Projeto de Contagem", 0);
            cmbProjeto_TipoContagem.addItem("A", "Aplica��o", 0);
            if ( cmbProjeto_TipoContagem.ItemCount > 0 )
            {
               A651Projeto_TipoContagem = cmbProjeto_TipoContagem.getValidValue(A651Projeto_TipoContagem);
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbProjeto_TipoContagem,(String)cmbProjeto_TipoContagem_Internalname,StringUtil.RTrim( A651Projeto_TipoContagem),(short)1,(String)cmbProjeto_TipoContagem_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
         cmbProjeto_TipoContagem.CurrentValue = StringUtil.RTrim( A651Projeto_TipoContagem);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_TipoContagem_Internalname, "Values", (String)(cmbProjeto_TipoContagem.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_2_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "PROJETO_STATUS_" + sGXsfl_2_idx;
            cmbProjeto_Status.Name = GXCCtl;
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbProjeto_Status,(String)cmbProjeto_Status_Internalname,StringUtil.RTrim( A658Projeto_Status),(short)1,(String)cmbProjeto_Status_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
         cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_CODIGO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SISTEMACOD"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(A740Projeto_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SIGLA"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_ESCOPO"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, A653Projeto_Escopo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TIPOCONTAGEM"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_STATUS"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GridContainer.AddRow(GridRow);
         nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         /* End function sendrow_22 */
      }

      protected void init_default_properties( )
      {
         edtProjeto_Codigo_Internalname = sPrefix+"PROJETO_CODIGO";
         edtProjeto_SistemaCod_Internalname = sPrefix+"PROJETO_SISTEMACOD";
         edtProjeto_Sigla_Internalname = sPrefix+"PROJETO_SIGLA";
         edtProjeto_Prazo_Internalname = sPrefix+"PROJETO_PRAZO";
         edtProjeto_Custo_Internalname = sPrefix+"PROJETO_CUSTO";
         edtProjeto_Escopo_Internalname = sPrefix+"PROJETO_ESCOPO";
         edtProjeto_Esforco_Internalname = sPrefix+"PROJETO_ESFORCO";
         cmbProjeto_TipoContagem_Internalname = sPrefix+"PROJETO_TIPOCONTAGEM";
         cmbProjeto_Status_Internalname = sPrefix+"PROJETO_STATUS";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbProjeto_Status_Jsonclick = "";
         cmbProjeto_TipoContagem_Jsonclick = "";
         edtProjeto_Esforco_Jsonclick = "";
         edtProjeto_Escopo_Jsonclick = "";
         edtProjeto_Custo_Jsonclick = "";
         edtProjeto_Prazo_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_SistemaCod_Jsonclick = "";
         edtProjeto_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtProjeto_Sigla_Link = "";
         subGrid_Sortable = 0;
         subGrid_Class = "WorkWith";
         subGrid_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV5Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A740Projeto_SistemaCod',fld:'PROJETO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         GridContainer = new GXWebGrid( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         A650Projeto_Sigla = "";
         A653Projeto_Escopo = "";
         A651Projeto_TipoContagem = "";
         A658Projeto_Status = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         scmdbuf = "";
         H00EE3_A658Projeto_Status = new String[] {""} ;
         H00EE3_A651Projeto_TipoContagem = new String[] {""} ;
         H00EE3_A653Projeto_Escopo = new String[] {""} ;
         H00EE3_A650Projeto_Sigla = new String[] {""} ;
         H00EE3_A657Projeto_Esforco = new short[1] ;
         H00EE3_A655Projeto_Custo = new decimal[1] ;
         H00EE3_A656Projeto_Prazo = new short[1] ;
         H00EE3_A648Projeto_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV5Sistema_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_sistemacontagens__default(),
            new Object[][] {
                new Object[] {
               H00EE3_A658Projeto_Status, H00EE3_A651Projeto_TipoContagem, H00EE3_A653Projeto_Escopo, H00EE3_A650Projeto_Sigla, H00EE3_A657Projeto_Esforco, H00EE3_A655Projeto_Custo, H00EE3_A656Projeto_Prazo, H00EE3_A648Projeto_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_2 ;
      private short nGXsfl_2_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Sortable ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_2_Refreshing=0 ;
      private short subGrid_Backstyle ;
      private short GRID_nEOF ;
      private int AV5Sistema_Codigo ;
      private int wcpOAV5Sistema_Codigo ;
      private int A740Projeto_SistemaCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int A648Projeto_Codigo ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int subGrid_Islastpage ;
      private int GXt_int1 ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal A655Projeto_Custo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_2_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sStyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Link ;
      private String A651Projeto_TipoContagem ;
      private String A658Projeto_Status ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtProjeto_Codigo_Internalname ;
      private String edtProjeto_SistemaCod_Internalname ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Prazo_Internalname ;
      private String edtProjeto_Custo_Internalname ;
      private String edtProjeto_Escopo_Internalname ;
      private String edtProjeto_Esforco_Internalname ;
      private String cmbProjeto_TipoContagem_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String sCtrlAV5Sistema_Codigo ;
      private String sGXsfl_2_fel_idx="0001" ;
      private String ROClassString ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String edtProjeto_SistemaCod_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String edtProjeto_Prazo_Jsonclick ;
      private String edtProjeto_Custo_Jsonclick ;
      private String edtProjeto_Escopo_Jsonclick ;
      private String edtProjeto_Esforco_Jsonclick ;
      private String cmbProjeto_TipoContagem_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private String A653Projeto_Escopo ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Sistema_Codigo ;
      private GXCombobox cmbProjeto_TipoContagem ;
      private GXCombobox cmbProjeto_Status ;
      private IDataStoreProvider pr_default ;
      private String[] H00EE3_A658Projeto_Status ;
      private String[] H00EE3_A651Projeto_TipoContagem ;
      private String[] H00EE3_A653Projeto_Escopo ;
      private String[] H00EE3_A650Projeto_Sigla ;
      private short[] H00EE3_A657Projeto_Esforco ;
      private decimal[] H00EE3_A655Projeto_Custo ;
      private short[] H00EE3_A656Projeto_Prazo ;
      private int[] H00EE3_A648Projeto_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class wc_sistemacontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EE3 ;
          prmH00EE3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EE3", "SELECT T1.[Projeto_Status], T1.[Projeto_TipoContagem], T1.[Projeto_Escopo], T1.[Projeto_Sigla], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo, T1.[Projeto_Codigo] FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Custo]) AS Projeto_Custo, SUM([Sistema_Prazo]) AS Projeto_Prazo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo]) ORDER BY T1.[Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EE3,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
