/*
               File: type_SdtContratanteUsuarioSistema
        Description: Sistemas do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:42.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratanteUsuarioSistema" )]
   [XmlType(TypeName =  "ContratanteUsuarioSistema" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratanteUsuarioSistema : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratanteUsuarioSistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratanteUsuarioSistema_Mode = "";
      }

      public SdtContratanteUsuarioSistema( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV63ContratanteUsuario_ContratanteCod ,
                        int AV60ContratanteUsuario_UsuarioCod ,
                        int AV127Sistema_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV63ContratanteUsuario_ContratanteCod,(int)AV60ContratanteUsuario_UsuarioCod,(int)AV127Sistema_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratanteUsuario_ContratanteCod", typeof(int)}, new Object[]{"ContratanteUsuario_UsuarioCod", typeof(int)}, new Object[]{"Sistema_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratanteUsuarioSistema");
         metadata.Set("BT", "ContratanteUsuarioSistema");
         metadata.Set("PK", "[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\",\"Sistema_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratanteUsuario_ContratanteCod\" ],\"FKMap\":[  ] },{ \"FK\":[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\" ],\"FKMap\":[  ] },{ \"FK\":[ \"ContratanteUsuario_UsuarioCod\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_codigo_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratanteUsuarioSistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratanteUsuarioSistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratanteUsuarioSistema obj ;
         obj = this;
         obj.gxTpr_Contratanteusuario_contratantecod = deserialized.gxTpr_Contratanteusuario_contratantecod;
         obj.gxTpr_Contratanteusuario_usuariocod = deserialized.gxTpr_Contratanteusuario_usuariocod;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratanteusuario_contratantecod_Z = deserialized.gxTpr_Contratanteusuario_contratantecod_Z;
         obj.gxTpr_Contratanteusuario_usuariocod_Z = deserialized.gxTpr_Contratanteusuario_usuariocod_Z;
         obj.gxTpr_Sistema_codigo_Z = deserialized.gxTpr_Sistema_codigo_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteCod") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioCod") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteCod_Z") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioCod_Z") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo_Z") )
               {
                  gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratanteUsuarioSistema";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratanteUsuario_ContratanteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratanteUsuarioSistema_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Sistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratanteUsuario_ContratanteCod", gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod, false);
         AddObjectProperty("ContratanteUsuario_UsuarioCod", gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod, false);
         AddObjectProperty("Sistema_Codigo", gxTv_SdtContratanteUsuarioSistema_Sistema_codigo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratanteUsuarioSistema_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratanteUsuarioSistema_Initialized, false);
            AddObjectProperty("ContratanteUsuario_ContratanteCod_Z", gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioCod_Z", gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z, false);
            AddObjectProperty("Sistema_Codigo_Z", gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteCod"   )]
      public int gxTpr_Contratanteusuario_contratantecod
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod ;
         }

         set {
            if ( gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod != value )
            {
               gxTv_SdtContratanteUsuarioSistema_Mode = "INS";
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z_SetNull( );
            }
            gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioCod"   )]
      public int gxTpr_Contratanteusuario_usuariocod
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod ;
         }

         set {
            if ( gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod != value )
            {
               gxTv_SdtContratanteUsuarioSistema_Mode = "INS";
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z_SetNull( );
            }
            gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Sistema_codigo ;
         }

         set {
            if ( gxTv_SdtContratanteUsuarioSistema_Sistema_codigo != value )
            {
               gxTv_SdtContratanteUsuarioSistema_Mode = "INS";
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z_SetNull( );
            }
            gxTv_SdtContratanteUsuarioSistema_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Mode ;
         }

         set {
            gxTv_SdtContratanteUsuarioSistema_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuarioSistema_Mode_SetNull( )
      {
         gxTv_SdtContratanteUsuarioSistema_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuarioSistema_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Initialized ;
         }

         set {
            gxTv_SdtContratanteUsuarioSistema_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuarioSistema_Initialized_SetNull( )
      {
         gxTv_SdtContratanteUsuarioSistema_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuarioSistema_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteCod_Z"   )]
      public int gxTpr_Contratanteusuario_contratantecod_Z
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioCod_Z"   )]
      public int gxTpr_Contratanteusuario_usuariocod_Z
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo_Z" )]
      [  XmlElement( ElementName = "Sistema_Codigo_Z"   )]
      public int gxTpr_Sistema_codigo_Z
      {
         get {
            return gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z ;
         }

         set {
            gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratanteUsuarioSistema_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratanteusuariosistema", "GeneXus.Programs.contratanteusuariosistema_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratanteUsuarioSistema_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod ;
      private int gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod ;
      private int gxTv_SdtContratanteUsuarioSistema_Sistema_codigo ;
      private int gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_contratantecod_Z ;
      private int gxTv_SdtContratanteUsuarioSistema_Contratanteusuario_usuariocod_Z ;
      private int gxTv_SdtContratanteUsuarioSistema_Sistema_codigo_Z ;
      private String gxTv_SdtContratanteUsuarioSistema_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratanteUsuarioSistema", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratanteUsuarioSistema_RESTInterface : GxGenericCollectionItem<SdtContratanteUsuarioSistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratanteUsuarioSistema_RESTInterface( ) : base()
      {
      }

      public SdtContratanteUsuarioSistema_RESTInterface( SdtContratanteUsuarioSistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratanteUsuario_ContratanteCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_contratantecod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_contratantecod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_contratantecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_usuariocod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_usuariocod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContratanteUsuarioSistema sdt
      {
         get {
            return (SdtContratanteUsuarioSistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratanteUsuarioSistema() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 8 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
