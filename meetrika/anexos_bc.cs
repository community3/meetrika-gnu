/*
               File: Anexos_BC
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:53:2.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class anexos_bc : GXHttpHandler, IGxSilentTrn
   {
      public anexos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public anexos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow36133( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey36133( ) ;
         standaloneModal( ) ;
         AddRow36133( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1106Anexo_Codigo = A1106Anexo_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_360( )
      {
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls36133( ) ;
            }
            else
            {
               CheckExtendedTable36133( ) ;
               if ( AnyError == 0 )
               {
                  ZM36133( 7) ;
                  ZM36133( 8) ;
                  ZM36133( 9) ;
               }
               CloseExtendedTableCursors36133( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode133 = Gx_mode;
            CONFIRM_36134( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode133;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode133;
         }
      }

      protected void CONFIRM_36134( )
      {
         s1497Anexo_Entidade = O1497Anexo_Entidade;
         nGXsfl_134_idx = 0;
         while ( nGXsfl_134_idx < bcAnexos.gxTpr_De.Count )
         {
            ReadRow36134( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound134 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_134 != 0 ) )
            {
               GetKey36134( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound134 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate36134( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable36134( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM36134( 12) ;
                           ZM36134( 13) ;
                        }
                        CloseExtendedTableCursors36134( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                        O1497Anexo_Entidade = A1497Anexo_Entidade;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound134 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey36134( ) ;
                        Load36134( ) ;
                        BeforeValidate36134( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls36134( ) ;
                           O1497Anexo_Entidade = A1497Anexo_Entidade;
                        }
                     }
                     else
                     {
                        if ( nIsMod_134 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate36134( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable36134( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM36134( 12) ;
                                 ZM36134( 13) ;
                              }
                              CloseExtendedTableCursors36134( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                              O1497Anexo_Entidade = A1497Anexo_Entidade;
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow134( ((SdtAnexos_De)bcAnexos.gxTpr_De.Item(nGXsfl_134_idx))) ;
            }
         }
         O1497Anexo_Entidade = s1497Anexo_Entidade;
         /* Start of After( level) rules */
         /* Using cursor BC00369 */
         pr_default.execute(3, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1498Anexo_DemandaCod = BC00369_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC00369_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
         }
         /* Using cursor BC00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = BC00367_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = BC00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
         /* Using cursor BC00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         pr_default.close(2);
         /* End of After( level) rules */
      }

      protected void ZM36133( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
         if ( GX_JID == -6 )
         {
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1101Anexo_Arquivo = A1101Anexo_Arquivo;
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1450Anexo_Link = A1450Anexo_Link;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z1108Anexo_TipoArq = A1108Anexo_TipoArq;
            Z1107Anexo_NomeArq = A1107Anexo_NomeArq;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1103Anexo_Data) && ( Gx_BScreen == 0 ) )
         {
            A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load36133( )
      {
         /* Using cursor BC003614 */
         pr_default.execute(7, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound133 = 1;
            A1102Anexo_UserId = BC003614_A1102Anexo_UserId[0];
            A1103Anexo_Data = BC003614_A1103Anexo_Data[0];
            A646TipoDocumento_Nome = BC003614_A646TipoDocumento_Nome[0];
            A1123Anexo_Descricao = BC003614_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = BC003614_n1123Anexo_Descricao[0];
            A1450Anexo_Link = BC003614_A1450Anexo_Link[0];
            n1450Anexo_Link = BC003614_n1450Anexo_Link[0];
            A1495Anexo_Owner = BC003614_A1495Anexo_Owner[0];
            n1495Anexo_Owner = BC003614_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = BC003614_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = BC003614_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = BC003614_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = BC003614_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = BC003614_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003614_n645TipoDocumento_Codigo[0];
            A1498Anexo_DemandaCod = BC003614_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC003614_n1498Anexo_DemandaCod[0];
            A1101Anexo_Arquivo = BC003614_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = BC003614_n1101Anexo_Arquivo[0];
            ZM36133( -6) ;
         }
         pr_default.close(7);
         OnLoadActions36133( ) ;
      }

      protected void OnLoadActions36133( )
      {
         /* Using cursor BC00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = BC00367_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = BC00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         pr_default.close(2);
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
      }

      protected void CheckExtendedTable36133( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00369 */
         pr_default.execute(3, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1498Anexo_DemandaCod = BC00369_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC00369_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
         }
         pr_default.close(3);
         /* Using cursor BC00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = BC00367_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = BC00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         pr_default.close(2);
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
         if ( ! ( (DateTime.MinValue==A1103Anexo_Data) || ( A1103Anexo_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC003612 */
         pr_default.execute(6, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC003612_A646TipoDocumento_Nome[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors36133( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey36133( )
      {
         /* Using cursor BC003615 */
         pr_default.execute(8, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound133 = 1;
         }
         else
         {
            RcdFound133 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003611 */
         pr_default.execute(5, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM36133( 6) ;
            RcdFound133 = 1;
            A1106Anexo_Codigo = BC003611_A1106Anexo_Codigo[0];
            A1102Anexo_UserId = BC003611_A1102Anexo_UserId[0];
            A1103Anexo_Data = BC003611_A1103Anexo_Data[0];
            A1123Anexo_Descricao = BC003611_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = BC003611_n1123Anexo_Descricao[0];
            A1450Anexo_Link = BC003611_A1450Anexo_Link[0];
            n1450Anexo_Link = BC003611_n1450Anexo_Link[0];
            A1495Anexo_Owner = BC003611_A1495Anexo_Owner[0];
            n1495Anexo_Owner = BC003611_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = BC003611_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = BC003611_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = BC003611_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = BC003611_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = BC003611_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003611_n645TipoDocumento_Codigo[0];
            A1101Anexo_Arquivo = BC003611_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = BC003611_n1101Anexo_Arquivo[0];
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            sMode133 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load36133( ) ;
            if ( AnyError == 1 )
            {
               RcdFound133 = 0;
               InitializeNonKey36133( ) ;
            }
            Gx_mode = sMode133;
         }
         else
         {
            RcdFound133 = 0;
            InitializeNonKey36133( ) ;
            sMode133 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode133;
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey36133( ) ;
         if ( RcdFound133 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_360( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency36133( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003610 */
            pr_default.execute(4, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Anexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z1102Anexo_UserId != BC003610_A1102Anexo_UserId[0] ) || ( Z1103Anexo_Data != BC003610_A1103Anexo_Data[0] ) || ( StringUtil.StrCmp(Z1123Anexo_Descricao, BC003610_A1123Anexo_Descricao[0]) != 0 ) || ( Z1495Anexo_Owner != BC003610_A1495Anexo_Owner[0] ) || ( Z645TipoDocumento_Codigo != BC003610_A645TipoDocumento_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Anexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert36133( )
      {
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36133( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM36133( 0) ;
            CheckOptimisticConcurrency36133( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36133( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert36133( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003616 */
                     pr_default.execute(9, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, A1102Anexo_UserId, A1103Anexo_Data, n1123Anexo_Descricao, A1123Anexo_Descricao, n1450Anexo_Link, A1450Anexo_Link, n1495Anexo_Owner, A1495Anexo_Owner, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     A1106Anexo_Codigo = BC003616_A1106Anexo_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel36133( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load36133( ) ;
            }
            EndLevel36133( ) ;
         }
         CloseExtendedTableCursors36133( ) ;
      }

      protected void Update36133( )
      {
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36133( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36133( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36133( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate36133( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003617 */
                     pr_default.execute(10, new Object[] {A1102Anexo_UserId, A1103Anexo_Data, n1123Anexo_Descricao, A1123Anexo_Descricao, n1450Anexo_Link, A1450Anexo_Link, n1495Anexo_Owner, A1495Anexo_Owner, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A1106Anexo_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Anexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate36133( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel36133( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel36133( ) ;
         }
         CloseExtendedTableCursors36133( ) ;
      }

      protected void DeferredUpdate36133( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC003618 */
            pr_default.execute(11, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, A1106Anexo_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36133( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls36133( ) ;
            AfterConfirm36133( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete36133( ) ;
               if ( AnyError == 0 )
               {
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  ScanKeyStart36134( ) ;
                  while ( RcdFound134 != 0 )
                  {
                     getByPrimaryKey36134( ) ;
                     Delete36134( ) ;
                     ScanKeyNext36134( ) ;
                     O1497Anexo_Entidade = A1497Anexo_Entidade;
                  }
                  ScanKeyEnd36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003619 */
                     pr_default.execute(12, new Object[] {A1106Anexo_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode133 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel36133( ) ;
         Gx_mode = sMode133;
      }

      protected void OnDeleteControls36133( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003621 */
            pr_default.execute(13, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               A1498Anexo_DemandaCod = BC003621_A1498Anexo_DemandaCod[0];
               n1498Anexo_DemandaCod = BC003621_n1498Anexo_DemandaCod[0];
            }
            else
            {
               A1498Anexo_DemandaCod = 0;
               n1498Anexo_DemandaCod = false;
            }
            pr_default.close(13);
            /* Using cursor BC003625 */
            pr_default.execute(14, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               A1496Anexo_AreaTrabalhoCod = BC003625_A1496Anexo_AreaTrabalhoCod[0];
               n1496Anexo_AreaTrabalhoCod = BC003625_n1496Anexo_AreaTrabalhoCod[0];
            }
            else
            {
               A1496Anexo_AreaTrabalhoCod = 0;
               n1496Anexo_AreaTrabalhoCod = false;
            }
            pr_default.close(14);
            /* Using cursor BC003626 */
            pr_default.execute(15, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC003626_A646TipoDocumento_Nome[0];
            pr_default.close(15);
            GXt_char1 = A1497Anexo_Entidade;
            new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
            A1497Anexo_Entidade = GXt_char1;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC003627 */
            pr_default.execute(16, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
         }
      }

      protected void ProcessNestedLevel36134( )
      {
         s1497Anexo_Entidade = O1497Anexo_Entidade;
         nGXsfl_134_idx = 0;
         while ( nGXsfl_134_idx < bcAnexos.gxTpr_De.Count )
         {
            ReadRow36134( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound134 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_134 != 0 ) )
            {
               standaloneNotModal36134( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert36134( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete36134( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update36134( ) ;
                  }
               }
               O1497Anexo_Entidade = A1497Anexo_Entidade;
            }
            KeyVarsToRow134( ((SdtAnexos_De)bcAnexos.gxTpr_De.Item(nGXsfl_134_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_134_idx = 0;
            while ( nGXsfl_134_idx < bcAnexos.gxTpr_De.Count )
            {
               ReadRow36134( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound134 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcAnexos.gxTpr_De.RemoveElement(nGXsfl_134_idx);
                  nGXsfl_134_idx = (short)(nGXsfl_134_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey36134( ) ;
                  VarsToRow134( ((SdtAnexos_De)bcAnexos.gxTpr_De.Item(nGXsfl_134_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* Using cursor BC003621 */
         pr_default.execute(13, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A1498Anexo_DemandaCod = BC003621_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC003621_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
         }
         /* Using cursor BC003625 */
         pr_default.execute(14, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = BC003625_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = BC003625_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
         /* Using cursor BC003625 */
         pr_default.execute(14, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         pr_default.close(14);
         /* End of After( level) rules */
         InitAll36134( ) ;
         if ( AnyError != 0 )
         {
            O1497Anexo_Entidade = s1497Anexo_Entidade;
         }
         nRcdExists_134 = 0;
         nIsMod_134 = 0;
         Gxremove134 = 0;
      }

      protected void ProcessLevel36133( )
      {
         /* Save parent mode. */
         sMode133 = Gx_mode;
         ProcessNestedLevel36134( ) ;
         if ( AnyError != 0 )
         {
            O1497Anexo_Entidade = s1497Anexo_Entidade;
         }
         /* Restore parent mode. */
         Gx_mode = sMode133;
         /* ' Update level parameters */
      }

      protected void EndLevel36133( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete36133( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart36133( )
      {
         /* Using cursor BC003629 */
         pr_default.execute(17, new Object[] {A1106Anexo_Codigo});
         RcdFound133 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound133 = 1;
            A1106Anexo_Codigo = BC003629_A1106Anexo_Codigo[0];
            A1102Anexo_UserId = BC003629_A1102Anexo_UserId[0];
            A1103Anexo_Data = BC003629_A1103Anexo_Data[0];
            A646TipoDocumento_Nome = BC003629_A646TipoDocumento_Nome[0];
            A1123Anexo_Descricao = BC003629_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = BC003629_n1123Anexo_Descricao[0];
            A1450Anexo_Link = BC003629_A1450Anexo_Link[0];
            n1450Anexo_Link = BC003629_n1450Anexo_Link[0];
            A1495Anexo_Owner = BC003629_A1495Anexo_Owner[0];
            n1495Anexo_Owner = BC003629_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = BC003629_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = BC003629_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = BC003629_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = BC003629_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = BC003629_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003629_n645TipoDocumento_Codigo[0];
            A1498Anexo_DemandaCod = BC003629_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC003629_n1498Anexo_DemandaCod[0];
            A1101Anexo_Arquivo = BC003629_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = BC003629_n1101Anexo_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext36133( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound133 = 0;
         ScanKeyLoad36133( ) ;
      }

      protected void ScanKeyLoad36133( )
      {
         sMode133 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound133 = 1;
            A1106Anexo_Codigo = BC003629_A1106Anexo_Codigo[0];
            A1102Anexo_UserId = BC003629_A1102Anexo_UserId[0];
            A1103Anexo_Data = BC003629_A1103Anexo_Data[0];
            A646TipoDocumento_Nome = BC003629_A646TipoDocumento_Nome[0];
            A1123Anexo_Descricao = BC003629_A1123Anexo_Descricao[0];
            n1123Anexo_Descricao = BC003629_n1123Anexo_Descricao[0];
            A1450Anexo_Link = BC003629_A1450Anexo_Link[0];
            n1450Anexo_Link = BC003629_n1450Anexo_Link[0];
            A1495Anexo_Owner = BC003629_A1495Anexo_Owner[0];
            n1495Anexo_Owner = BC003629_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = BC003629_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = BC003629_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = BC003629_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = BC003629_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = BC003629_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003629_n645TipoDocumento_Codigo[0];
            A1498Anexo_DemandaCod = BC003629_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = BC003629_n1498Anexo_DemandaCod[0];
            A1101Anexo_Arquivo = BC003629_A1101Anexo_Arquivo[0];
            n1101Anexo_Arquivo = BC003629_n1101Anexo_Arquivo[0];
         }
         Gx_mode = sMode133;
      }

      protected void ScanKeyEnd36133( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm36133( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert36133( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate36133( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete36133( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete36133( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate36133( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes36133( )
      {
      }

      protected void ZM36134( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z1107Anexo_NomeArq = A1107Anexo_NomeArq;
            Z1108Anexo_TipoArq = A1108Anexo_TipoArq;
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z1107Anexo_NomeArq = A1107Anexo_NomeArq;
            Z1108Anexo_TipoArq = A1108Anexo_TipoArq;
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z1107Anexo_NomeArq = A1107Anexo_NomeArq;
            Z1108Anexo_TipoArq = A1108Anexo_TipoArq;
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z1496Anexo_AreaTrabalhoCod = A1496Anexo_AreaTrabalhoCod;
            Z1497Anexo_Entidade = A1497Anexo_Entidade;
         }
         if ( GX_JID == -10 )
         {
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
      }

      protected void standaloneNotModal36134( )
      {
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
      }

      protected void standaloneModal36134( )
      {
      }

      protected void Load36134( )
      {
         /* Using cursor BC003630 */
         pr_default.execute(18, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound134 = 1;
            ZM36134( -10) ;
         }
         pr_default.close(18);
         OnLoadActions36134( ) ;
      }

      protected void OnLoadActions36134( )
      {
      }

      protected void CheckExtendedTable36134( )
      {
         Gx_BScreen = 1;
         standaloneModal36134( ) ;
         Gx_BScreen = 0;
         if ( ! ( ( A1110AnexoDe_Tabela == 1 ) || ( A1110AnexoDe_Tabela == 2 ) || ( A1110AnexoDe_Tabela == 3 ) ) )
         {
            GX_msglist.addItem("Campo Tabela fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors36134( )
      {
      }

      protected void enableDisable36134( )
      {
      }

      protected void GetKey36134( )
      {
         /* Using cursor BC003631 */
         pr_default.execute(19, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound134 = 1;
         }
         else
         {
            RcdFound134 = 0;
         }
         pr_default.close(19);
      }

      protected void getByPrimaryKey36134( )
      {
         /* Using cursor BC00363 */
         pr_default.execute(1, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM36134( 10) ;
            RcdFound134 = 1;
            InitializeNonKey36134( ) ;
            A1109AnexoDe_Id = BC00363_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = BC00363_A1110AnexoDe_Tabela[0];
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
            sMode134 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal36134( ) ;
            Load36134( ) ;
            Gx_mode = sMode134;
         }
         else
         {
            RcdFound134 = 0;
            InitializeNonKey36134( ) ;
            sMode134 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal36134( ) ;
            Gx_mode = sMode134;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes36134( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency36134( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00362 */
            pr_default.execute(0, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AnexosDe"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AnexosDe"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert36134( )
      {
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36134( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM36134( 0) ;
            CheckOptimisticConcurrency36134( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36134( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003632 */
                     pr_default.execute(20, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("AnexosDe") ;
                     if ( (pr_default.getStatus(20) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load36134( ) ;
            }
            EndLevel36134( ) ;
         }
         CloseExtendedTableCursors36134( ) ;
      }

      protected void Update36134( )
      {
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36134( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36134( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36134( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [AnexosDe] */
                     DeferredUpdate36134( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey36134( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel36134( ) ;
         }
         CloseExtendedTableCursors36134( ) ;
      }

      protected void DeferredUpdate36134( )
      {
      }

      protected void Delete36134( )
      {
         Gx_mode = "DLT";
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36134( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls36134( ) ;
            AfterConfirm36134( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete36134( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003633 */
                  pr_default.execute(21, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
                  pr_default.close(21);
                  dsDefault.SmartCacheProvider.SetUpdated("AnexosDe") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode134 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel36134( ) ;
         Gx_mode = sMode134;
      }

      protected void OnDeleteControls36134( )
      {
         standaloneModal36134( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel36134( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart36134( )
      {
         /* Scan By routine */
         /* Using cursor BC003634 */
         pr_default.execute(22, new Object[] {A1106Anexo_Codigo});
         RcdFound134 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound134 = 1;
            A1109AnexoDe_Id = BC003634_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = BC003634_A1110AnexoDe_Tabela[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext36134( )
      {
         /* Scan next routine */
         pr_default.readNext(22);
         RcdFound134 = 0;
         ScanKeyLoad36134( ) ;
      }

      protected void ScanKeyLoad36134( )
      {
         sMode134 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound134 = 1;
            A1109AnexoDe_Id = BC003634_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = BC003634_A1110AnexoDe_Tabela[0];
         }
         Gx_mode = sMode134;
      }

      protected void ScanKeyEnd36134( )
      {
         pr_default.close(22);
      }

      protected void AfterConfirm36134( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert36134( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate36134( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete36134( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete36134( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate36134( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes36134( )
      {
      }

      protected void AddRow36133( )
      {
         VarsToRow133( bcAnexos) ;
      }

      protected void ReadRow36133( )
      {
         RowToVars133( bcAnexos, 1) ;
      }

      protected void AddRow36134( )
      {
         SdtAnexos_De obj134 ;
         obj134 = new SdtAnexos_De(context);
         VarsToRow134( obj134) ;
         bcAnexos.gxTpr_De.Add(obj134, 0);
         obj134.gxTpr_Mode = "UPD";
         obj134.gxTpr_Modified = 0;
      }

      protected void ReadRow36134( )
      {
         nGXsfl_134_idx = (short)(nGXsfl_134_idx+1);
         RowToVars134( ((SdtAnexos_De)bcAnexos.gxTpr_De.Item(nGXsfl_134_idx)), 1) ;
      }

      protected void InitializeNonKey36133( )
      {
         A1496Anexo_AreaTrabalhoCod = 0;
         n1496Anexo_AreaTrabalhoCod = false;
         A1497Anexo_Entidade = "";
         A1101Anexo_Arquivo = "";
         n1101Anexo_Arquivo = false;
         A1102Anexo_UserId = 0;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A1123Anexo_Descricao = "";
         n1123Anexo_Descricao = false;
         A1450Anexo_Link = "";
         n1450Anexo_Link = false;
         A1495Anexo_Owner = 0;
         n1495Anexo_Owner = false;
         A1498Anexo_DemandaCod = 0;
         n1498Anexo_DemandaCod = false;
         A1108Anexo_TipoArq = "";
         n1108Anexo_TipoArq = false;
         A1107Anexo_NomeArq = "";
         n1107Anexo_NomeArq = false;
         A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         Z1102Anexo_UserId = 0;
         Z1103Anexo_Data = (DateTime)(DateTime.MinValue);
         Z1123Anexo_Descricao = "";
         Z1495Anexo_Owner = 0;
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll36133( )
      {
         A1106Anexo_Codigo = 0;
         InitializeNonKey36133( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1103Anexo_Data = i1103Anexo_Data;
      }

      protected void InitializeNonKey36134( )
      {
      }

      protected void InitAll36134( )
      {
         A1109AnexoDe_Id = 0;
         A1110AnexoDe_Tabela = 0;
         InitializeNonKey36134( ) ;
      }

      protected void StandaloneModalInsert36134( )
      {
      }

      public void VarsToRow133( SdtAnexos obj133 )
      {
         obj133.gxTpr_Mode = Gx_mode;
         obj133.gxTpr_Anexo_areatrabalhocod = A1496Anexo_AreaTrabalhoCod;
         obj133.gxTpr_Anexo_entidade = A1497Anexo_Entidade;
         obj133.gxTpr_Anexo_arquivo = A1101Anexo_Arquivo;
         obj133.gxTpr_Anexo_userid = A1102Anexo_UserId;
         obj133.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj133.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj133.gxTpr_Anexo_descricao = A1123Anexo_Descricao;
         obj133.gxTpr_Anexo_link = A1450Anexo_Link;
         obj133.gxTpr_Anexo_owner = A1495Anexo_Owner;
         obj133.gxTpr_Anexo_demandacod = A1498Anexo_DemandaCod;
         obj133.gxTpr_Anexo_tipoarq = A1108Anexo_TipoArq;
         obj133.gxTpr_Anexo_nomearq = A1107Anexo_NomeArq;
         obj133.gxTpr_Anexo_data = A1103Anexo_Data;
         obj133.gxTpr_Anexo_codigo = A1106Anexo_Codigo;
         obj133.gxTpr_Anexo_codigo_Z = Z1106Anexo_Codigo;
         obj133.gxTpr_Anexo_nomearq_Z = Z1107Anexo_NomeArq;
         obj133.gxTpr_Anexo_tipoarq_Z = Z1108Anexo_TipoArq;
         obj133.gxTpr_Anexo_userid_Z = Z1102Anexo_UserId;
         obj133.gxTpr_Anexo_data_Z = Z1103Anexo_Data;
         obj133.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj133.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj133.gxTpr_Anexo_descricao_Z = Z1123Anexo_Descricao;
         obj133.gxTpr_Anexo_owner_Z = Z1495Anexo_Owner;
         obj133.gxTpr_Anexo_demandacod_Z = Z1498Anexo_DemandaCod;
         obj133.gxTpr_Anexo_areatrabalhocod_Z = Z1496Anexo_AreaTrabalhoCod;
         obj133.gxTpr_Anexo_entidade_Z = Z1497Anexo_Entidade;
         obj133.gxTpr_Anexo_nomearq_N = (short)(Convert.ToInt16(n1107Anexo_NomeArq));
         obj133.gxTpr_Anexo_tipoarq_N = (short)(Convert.ToInt16(n1108Anexo_TipoArq));
         obj133.gxTpr_Anexo_arquivo_N = (short)(Convert.ToInt16(n1101Anexo_Arquivo));
         obj133.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj133.gxTpr_Anexo_descricao_N = (short)(Convert.ToInt16(n1123Anexo_Descricao));
         obj133.gxTpr_Anexo_link_N = (short)(Convert.ToInt16(n1450Anexo_Link));
         obj133.gxTpr_Anexo_owner_N = (short)(Convert.ToInt16(n1495Anexo_Owner));
         obj133.gxTpr_Anexo_demandacod_N = (short)(Convert.ToInt16(n1498Anexo_DemandaCod));
         obj133.gxTpr_Anexo_areatrabalhocod_N = (short)(Convert.ToInt16(n1496Anexo_AreaTrabalhoCod));
         obj133.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow133( SdtAnexos obj133 )
      {
         obj133.gxTpr_Anexo_codigo = A1106Anexo_Codigo;
         return  ;
      }

      public void RowToVars133( SdtAnexos obj133 ,
                                int forceLoad )
      {
         Gx_mode = obj133.gxTpr_Mode;
         A1496Anexo_AreaTrabalhoCod = obj133.gxTpr_Anexo_areatrabalhocod;
         n1496Anexo_AreaTrabalhoCod = false;
         A1497Anexo_Entidade = obj133.gxTpr_Anexo_entidade;
         A1101Anexo_Arquivo = obj133.gxTpr_Anexo_arquivo;
         n1101Anexo_Arquivo = false;
         A1102Anexo_UserId = obj133.gxTpr_Anexo_userid;
         A645TipoDocumento_Codigo = obj133.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj133.gxTpr_Tipodocumento_nome;
         A1123Anexo_Descricao = obj133.gxTpr_Anexo_descricao;
         n1123Anexo_Descricao = false;
         A1450Anexo_Link = obj133.gxTpr_Anexo_link;
         n1450Anexo_Link = false;
         A1495Anexo_Owner = obj133.gxTpr_Anexo_owner;
         n1495Anexo_Owner = false;
         A1498Anexo_DemandaCod = obj133.gxTpr_Anexo_demandacod;
         n1498Anexo_DemandaCod = false;
         A1108Anexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj133.gxTpr_Anexo_tipoarq)) ? FileUtil.GetFileType( A1101Anexo_Arquivo) : obj133.gxTpr_Anexo_tipoarq);
         n1108Anexo_TipoArq = false;
         A1107Anexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj133.gxTpr_Anexo_nomearq)) ? FileUtil.GetFileName( A1101Anexo_Arquivo) : obj133.gxTpr_Anexo_nomearq);
         n1107Anexo_NomeArq = false;
         A1103Anexo_Data = obj133.gxTpr_Anexo_data;
         A1106Anexo_Codigo = obj133.gxTpr_Anexo_codigo;
         Z1106Anexo_Codigo = obj133.gxTpr_Anexo_codigo_Z;
         Z1107Anexo_NomeArq = obj133.gxTpr_Anexo_nomearq_Z;
         Z1108Anexo_TipoArq = obj133.gxTpr_Anexo_tipoarq_Z;
         Z1102Anexo_UserId = obj133.gxTpr_Anexo_userid_Z;
         Z1103Anexo_Data = obj133.gxTpr_Anexo_data_Z;
         Z645TipoDocumento_Codigo = obj133.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj133.gxTpr_Tipodocumento_nome_Z;
         Z1123Anexo_Descricao = obj133.gxTpr_Anexo_descricao_Z;
         Z1495Anexo_Owner = obj133.gxTpr_Anexo_owner_Z;
         Z1498Anexo_DemandaCod = obj133.gxTpr_Anexo_demandacod_Z;
         Z1496Anexo_AreaTrabalhoCod = obj133.gxTpr_Anexo_areatrabalhocod_Z;
         Z1497Anexo_Entidade = obj133.gxTpr_Anexo_entidade_Z;
         n1107Anexo_NomeArq = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_nomearq_N));
         n1108Anexo_TipoArq = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_tipoarq_N));
         n1101Anexo_Arquivo = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_arquivo_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj133.gxTpr_Tipodocumento_codigo_N));
         n1123Anexo_Descricao = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_descricao_N));
         n1450Anexo_Link = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_link_N));
         n1495Anexo_Owner = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_owner_N));
         n1498Anexo_DemandaCod = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_demandacod_N));
         n1496Anexo_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj133.gxTpr_Anexo_areatrabalhocod_N));
         Gx_mode = obj133.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow134( SdtAnexos_De obj134 )
      {
         obj134.gxTpr_Mode = Gx_mode;
         obj134.gxTpr_Anexode_id = A1109AnexoDe_Id;
         obj134.gxTpr_Anexode_tabela = A1110AnexoDe_Tabela;
         obj134.gxTpr_Anexode_id_Z = Z1109AnexoDe_Id;
         obj134.gxTpr_Anexode_tabela_Z = Z1110AnexoDe_Tabela;
         obj134.gxTpr_Modified = nIsMod_134;
         return  ;
      }

      public void KeyVarsToRow134( SdtAnexos_De obj134 )
      {
         obj134.gxTpr_Anexode_id = A1109AnexoDe_Id;
         obj134.gxTpr_Anexode_tabela = A1110AnexoDe_Tabela;
         return  ;
      }

      public void RowToVars134( SdtAnexos_De obj134 ,
                                int forceLoad )
      {
         Gx_mode = obj134.gxTpr_Mode;
         A1109AnexoDe_Id = obj134.gxTpr_Anexode_id;
         A1110AnexoDe_Tabela = obj134.gxTpr_Anexode_tabela;
         Z1109AnexoDe_Id = obj134.gxTpr_Anexode_id_Z;
         Z1110AnexoDe_Tabela = obj134.gxTpr_Anexode_tabela_Z;
         nIsMod_134 = obj134.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1106Anexo_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey36133( ) ;
         ScanKeyStart36133( ) ;
         if ( RcdFound133 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
         }
         ZM36133( -6) ;
         OnLoadActions36133( ) ;
         AddRow36133( ) ;
         bcAnexos.gxTpr_De.ClearCollection();
         if ( RcdFound133 == 1 )
         {
            ScanKeyStart36134( ) ;
            nGXsfl_134_idx = 1;
            while ( RcdFound134 != 0 )
            {
               Z1106Anexo_Codigo = A1106Anexo_Codigo;
               Z1109AnexoDe_Id = A1109AnexoDe_Id;
               Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
               ZM36134( -10) ;
               OnLoadActions36134( ) ;
               nRcdExists_134 = 1;
               nIsMod_134 = 0;
               AddRow36134( ) ;
               nGXsfl_134_idx = (short)(nGXsfl_134_idx+1);
               ScanKeyNext36134( ) ;
            }
            ScanKeyEnd36134( ) ;
         }
         ScanKeyEnd36133( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars133( bcAnexos, 0) ;
         ScanKeyStart36133( ) ;
         if ( RcdFound133 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
         }
         ZM36133( -6) ;
         OnLoadActions36133( ) ;
         AddRow36133( ) ;
         bcAnexos.gxTpr_De.ClearCollection();
         if ( RcdFound133 == 1 )
         {
            ScanKeyStart36134( ) ;
            nGXsfl_134_idx = 1;
            while ( RcdFound134 != 0 )
            {
               Z1106Anexo_Codigo = A1106Anexo_Codigo;
               Z1109AnexoDe_Id = A1109AnexoDe_Id;
               Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
               ZM36134( -10) ;
               OnLoadActions36134( ) ;
               nRcdExists_134 = 1;
               nIsMod_134 = 0;
               AddRow36134( ) ;
               nGXsfl_134_idx = (short)(nGXsfl_134_idx+1);
               ScanKeyNext36134( ) ;
            }
            ScanKeyEnd36134( ) ;
         }
         ScanKeyEnd36133( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars133( bcAnexos, 0) ;
         nKeyPressed = 1;
         GetKey36133( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1497Anexo_Entidade = O1497Anexo_Entidade;
            Insert36133( ) ;
         }
         else
         {
            if ( RcdFound133 == 1 )
            {
               if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
               {
                  A1106Anexo_Codigo = Z1106Anexo_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  Update36133( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A1497Anexo_Entidade = O1497Anexo_Entidade;
                        Insert36133( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A1497Anexo_Entidade = O1497Anexo_Entidade;
                        Insert36133( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow133( bcAnexos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars133( bcAnexos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey36133( ) ;
         if ( RcdFound133 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
            {
               A1106Anexo_Codigo = Z1106Anexo_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(5);
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
         pr_default.close(13);
         context.RollbackDataStores( "Anexos_BC");
         VarsToRow133( bcAnexos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAnexos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAnexos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAnexos )
         {
            bcAnexos = (SdtAnexos)(sdt);
            if ( StringUtil.StrCmp(bcAnexos.gxTpr_Mode, "") == 0 )
            {
               bcAnexos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow133( bcAnexos) ;
            }
            else
            {
               RowToVars133( bcAnexos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAnexos.gxTpr_Mode, "") == 0 )
            {
               bcAnexos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars133( bcAnexos, 1) ;
         return  ;
      }

      public SdtAnexos Anexos_BC
      {
         get {
            return bcAnexos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(5);
         pr_default.close(15);
         pr_default.close(14);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode133 = "";
         s1497Anexo_Entidade = "";
         O1497Anexo_Entidade = "";
         A1497Anexo_Entidade = "";
         BC00369_A1498Anexo_DemandaCod = new int[1] ;
         BC00369_n1498Anexo_DemandaCod = new bool[] {false} ;
         BC00367_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         BC00367_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         Z1103Anexo_Data = (DateTime)(DateTime.MinValue);
         A1103Anexo_Data = (DateTime)(DateTime.MinValue);
         Z1123Anexo_Descricao = "";
         A1123Anexo_Descricao = "";
         Z1497Anexo_Entidade = "";
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z1101Anexo_Arquivo = "";
         A1101Anexo_Arquivo = "";
         Z1450Anexo_Link = "";
         A1450Anexo_Link = "";
         Z1108Anexo_TipoArq = "";
         A1108Anexo_TipoArq = "";
         Z1107Anexo_NomeArq = "";
         A1107Anexo_NomeArq = "";
         BC003614_A1106Anexo_Codigo = new int[1] ;
         BC003614_A1102Anexo_UserId = new int[1] ;
         BC003614_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC003614_A646TipoDocumento_Nome = new String[] {""} ;
         BC003614_A1123Anexo_Descricao = new String[] {""} ;
         BC003614_n1123Anexo_Descricao = new bool[] {false} ;
         BC003614_A1450Anexo_Link = new String[] {""} ;
         BC003614_n1450Anexo_Link = new bool[] {false} ;
         BC003614_A1495Anexo_Owner = new int[1] ;
         BC003614_n1495Anexo_Owner = new bool[] {false} ;
         BC003614_A1108Anexo_TipoArq = new String[] {""} ;
         BC003614_n1108Anexo_TipoArq = new bool[] {false} ;
         BC003614_A1107Anexo_NomeArq = new String[] {""} ;
         BC003614_n1107Anexo_NomeArq = new bool[] {false} ;
         BC003614_A645TipoDocumento_Codigo = new int[1] ;
         BC003614_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC003614_A1498Anexo_DemandaCod = new int[1] ;
         BC003614_n1498Anexo_DemandaCod = new bool[] {false} ;
         BC003614_A1101Anexo_Arquivo = new String[] {""} ;
         BC003614_n1101Anexo_Arquivo = new bool[] {false} ;
         A1101Anexo_Arquivo_Filetype = "";
         A1101Anexo_Arquivo_Filename = "";
         BC003612_A646TipoDocumento_Nome = new String[] {""} ;
         BC003615_A1106Anexo_Codigo = new int[1] ;
         BC003611_A1106Anexo_Codigo = new int[1] ;
         BC003611_A1102Anexo_UserId = new int[1] ;
         BC003611_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC003611_A1123Anexo_Descricao = new String[] {""} ;
         BC003611_n1123Anexo_Descricao = new bool[] {false} ;
         BC003611_A1450Anexo_Link = new String[] {""} ;
         BC003611_n1450Anexo_Link = new bool[] {false} ;
         BC003611_A1495Anexo_Owner = new int[1] ;
         BC003611_n1495Anexo_Owner = new bool[] {false} ;
         BC003611_A1108Anexo_TipoArq = new String[] {""} ;
         BC003611_n1108Anexo_TipoArq = new bool[] {false} ;
         BC003611_A1107Anexo_NomeArq = new String[] {""} ;
         BC003611_n1107Anexo_NomeArq = new bool[] {false} ;
         BC003611_A645TipoDocumento_Codigo = new int[1] ;
         BC003611_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC003611_A1101Anexo_Arquivo = new String[] {""} ;
         BC003611_n1101Anexo_Arquivo = new bool[] {false} ;
         BC003610_A1106Anexo_Codigo = new int[1] ;
         BC003610_A1102Anexo_UserId = new int[1] ;
         BC003610_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC003610_A1123Anexo_Descricao = new String[] {""} ;
         BC003610_n1123Anexo_Descricao = new bool[] {false} ;
         BC003610_A1450Anexo_Link = new String[] {""} ;
         BC003610_n1450Anexo_Link = new bool[] {false} ;
         BC003610_A1495Anexo_Owner = new int[1] ;
         BC003610_n1495Anexo_Owner = new bool[] {false} ;
         BC003610_A1108Anexo_TipoArq = new String[] {""} ;
         BC003610_n1108Anexo_TipoArq = new bool[] {false} ;
         BC003610_A1107Anexo_NomeArq = new String[] {""} ;
         BC003610_n1107Anexo_NomeArq = new bool[] {false} ;
         BC003610_A645TipoDocumento_Codigo = new int[1] ;
         BC003610_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC003610_A1101Anexo_Arquivo = new String[] {""} ;
         BC003610_n1101Anexo_Arquivo = new bool[] {false} ;
         BC003616_A1106Anexo_Codigo = new int[1] ;
         BC003621_A1498Anexo_DemandaCod = new int[1] ;
         BC003621_n1498Anexo_DemandaCod = new bool[] {false} ;
         BC003625_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         BC003625_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         BC003626_A646TipoDocumento_Nome = new String[] {""} ;
         BC003627_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC003629_A1106Anexo_Codigo = new int[1] ;
         BC003629_A1102Anexo_UserId = new int[1] ;
         BC003629_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC003629_A646TipoDocumento_Nome = new String[] {""} ;
         BC003629_A1123Anexo_Descricao = new String[] {""} ;
         BC003629_n1123Anexo_Descricao = new bool[] {false} ;
         BC003629_A1450Anexo_Link = new String[] {""} ;
         BC003629_n1450Anexo_Link = new bool[] {false} ;
         BC003629_A1495Anexo_Owner = new int[1] ;
         BC003629_n1495Anexo_Owner = new bool[] {false} ;
         BC003629_A1108Anexo_TipoArq = new String[] {""} ;
         BC003629_n1108Anexo_TipoArq = new bool[] {false} ;
         BC003629_A1107Anexo_NomeArq = new String[] {""} ;
         BC003629_n1107Anexo_NomeArq = new bool[] {false} ;
         BC003629_A645TipoDocumento_Codigo = new int[1] ;
         BC003629_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC003629_A1498Anexo_DemandaCod = new int[1] ;
         BC003629_n1498Anexo_DemandaCod = new bool[] {false} ;
         BC003629_A1101Anexo_Arquivo = new String[] {""} ;
         BC003629_n1101Anexo_Arquivo = new bool[] {false} ;
         GXt_char1 = "";
         BC003630_A1106Anexo_Codigo = new int[1] ;
         BC003630_A1109AnexoDe_Id = new int[1] ;
         BC003630_A1110AnexoDe_Tabela = new int[1] ;
         BC003631_A1106Anexo_Codigo = new int[1] ;
         BC003631_A1109AnexoDe_Id = new int[1] ;
         BC003631_A1110AnexoDe_Tabela = new int[1] ;
         BC00363_A1106Anexo_Codigo = new int[1] ;
         BC00363_A1109AnexoDe_Id = new int[1] ;
         BC00363_A1110AnexoDe_Tabela = new int[1] ;
         sMode134 = "";
         BC00362_A1106Anexo_Codigo = new int[1] ;
         BC00362_A1109AnexoDe_Id = new int[1] ;
         BC00362_A1110AnexoDe_Tabela = new int[1] ;
         BC003634_A1106Anexo_Codigo = new int[1] ;
         BC003634_A1109AnexoDe_Id = new int[1] ;
         BC003634_A1110AnexoDe_Tabela = new int[1] ;
         i1103Anexo_Data = (DateTime)(DateTime.MinValue);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.anexos_bc__default(),
            new Object[][] {
                new Object[] {
               BC00362_A1106Anexo_Codigo, BC00362_A1109AnexoDe_Id, BC00362_A1110AnexoDe_Tabela
               }
               , new Object[] {
               BC00363_A1106Anexo_Codigo, BC00363_A1109AnexoDe_Id, BC00363_A1110AnexoDe_Tabela
               }
               , new Object[] {
               BC00367_A1496Anexo_AreaTrabalhoCod, BC00367_n1496Anexo_AreaTrabalhoCod
               }
               , new Object[] {
               BC00369_A1498Anexo_DemandaCod, BC00369_n1498Anexo_DemandaCod
               }
               , new Object[] {
               BC003610_A1106Anexo_Codigo, BC003610_A1102Anexo_UserId, BC003610_A1103Anexo_Data, BC003610_A1123Anexo_Descricao, BC003610_n1123Anexo_Descricao, BC003610_A1450Anexo_Link, BC003610_n1450Anexo_Link, BC003610_A1495Anexo_Owner, BC003610_n1495Anexo_Owner, BC003610_A1108Anexo_TipoArq,
               BC003610_n1108Anexo_TipoArq, BC003610_A1107Anexo_NomeArq, BC003610_n1107Anexo_NomeArq, BC003610_A645TipoDocumento_Codigo, BC003610_n645TipoDocumento_Codigo, BC003610_A1101Anexo_Arquivo, BC003610_n1101Anexo_Arquivo
               }
               , new Object[] {
               BC003611_A1106Anexo_Codigo, BC003611_A1102Anexo_UserId, BC003611_A1103Anexo_Data, BC003611_A1123Anexo_Descricao, BC003611_n1123Anexo_Descricao, BC003611_A1450Anexo_Link, BC003611_n1450Anexo_Link, BC003611_A1495Anexo_Owner, BC003611_n1495Anexo_Owner, BC003611_A1108Anexo_TipoArq,
               BC003611_n1108Anexo_TipoArq, BC003611_A1107Anexo_NomeArq, BC003611_n1107Anexo_NomeArq, BC003611_A645TipoDocumento_Codigo, BC003611_n645TipoDocumento_Codigo, BC003611_A1101Anexo_Arquivo, BC003611_n1101Anexo_Arquivo
               }
               , new Object[] {
               BC003612_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC003614_A1106Anexo_Codigo, BC003614_A1102Anexo_UserId, BC003614_A1103Anexo_Data, BC003614_A646TipoDocumento_Nome, BC003614_A1123Anexo_Descricao, BC003614_n1123Anexo_Descricao, BC003614_A1450Anexo_Link, BC003614_n1450Anexo_Link, BC003614_A1495Anexo_Owner, BC003614_n1495Anexo_Owner,
               BC003614_A1108Anexo_TipoArq, BC003614_n1108Anexo_TipoArq, BC003614_A1107Anexo_NomeArq, BC003614_n1107Anexo_NomeArq, BC003614_A645TipoDocumento_Codigo, BC003614_n645TipoDocumento_Codigo, BC003614_A1498Anexo_DemandaCod, BC003614_n1498Anexo_DemandaCod, BC003614_A1101Anexo_Arquivo, BC003614_n1101Anexo_Arquivo
               }
               , new Object[] {
               BC003615_A1106Anexo_Codigo
               }
               , new Object[] {
               BC003616_A1106Anexo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003621_A1498Anexo_DemandaCod, BC003621_n1498Anexo_DemandaCod
               }
               , new Object[] {
               BC003625_A1496Anexo_AreaTrabalhoCod, BC003625_n1496Anexo_AreaTrabalhoCod
               }
               , new Object[] {
               BC003626_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC003627_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               BC003629_A1106Anexo_Codigo, BC003629_A1102Anexo_UserId, BC003629_A1103Anexo_Data, BC003629_A646TipoDocumento_Nome, BC003629_A1123Anexo_Descricao, BC003629_n1123Anexo_Descricao, BC003629_A1450Anexo_Link, BC003629_n1450Anexo_Link, BC003629_A1495Anexo_Owner, BC003629_n1495Anexo_Owner,
               BC003629_A1108Anexo_TipoArq, BC003629_n1108Anexo_TipoArq, BC003629_A1107Anexo_NomeArq, BC003629_n1107Anexo_NomeArq, BC003629_A645TipoDocumento_Codigo, BC003629_n645TipoDocumento_Codigo, BC003629_A1498Anexo_DemandaCod, BC003629_n1498Anexo_DemandaCod, BC003629_A1101Anexo_Arquivo, BC003629_n1101Anexo_Arquivo
               }
               , new Object[] {
               BC003630_A1106Anexo_Codigo, BC003630_A1109AnexoDe_Id, BC003630_A1110AnexoDe_Tabela
               }
               , new Object[] {
               BC003631_A1106Anexo_Codigo, BC003631_A1109AnexoDe_Id, BC003631_A1110AnexoDe_Tabela
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003634_A1106Anexo_Codigo, BC003634_A1109AnexoDe_Id, BC003634_A1110AnexoDe_Tabela
               }
            }
         );
         Z1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short nGXsfl_134_idx=1 ;
      private short nIsMod_134 ;
      private short RcdFound134 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound133 ;
      private short nRcdExists_134 ;
      private short Gxremove134 ;
      private int trnEnded ;
      private int Z1106Anexo_Codigo ;
      private int A1106Anexo_Codigo ;
      private int A1498Anexo_DemandaCod ;
      private int A1496Anexo_AreaTrabalhoCod ;
      private int A1495Anexo_Owner ;
      private int Z1102Anexo_UserId ;
      private int A1102Anexo_UserId ;
      private int Z1495Anexo_Owner ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private int Z1498Anexo_DemandaCod ;
      private int Z1496Anexo_AreaTrabalhoCod ;
      private int Z1109AnexoDe_Id ;
      private int A1109AnexoDe_Id ;
      private int Z1110AnexoDe_Tabela ;
      private int A1110AnexoDe_Tabela ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode133 ;
      private String s1497Anexo_Entidade ;
      private String O1497Anexo_Entidade ;
      private String A1497Anexo_Entidade ;
      private String Z1497Anexo_Entidade ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String Z1108Anexo_TipoArq ;
      private String A1108Anexo_TipoArq ;
      private String Z1107Anexo_NomeArq ;
      private String A1107Anexo_NomeArq ;
      private String A1101Anexo_Arquivo_Filetype ;
      private String A1101Anexo_Arquivo_Filename ;
      private String GXt_char1 ;
      private String sMode134 ;
      private DateTime Z1103Anexo_Data ;
      private DateTime A1103Anexo_Data ;
      private DateTime i1103Anexo_Data ;
      private bool n1498Anexo_DemandaCod ;
      private bool n1496Anexo_AreaTrabalhoCod ;
      private bool n1123Anexo_Descricao ;
      private bool n1450Anexo_Link ;
      private bool n1495Anexo_Owner ;
      private bool n1108Anexo_TipoArq ;
      private bool n1107Anexo_NomeArq ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1101Anexo_Arquivo ;
      private String Z1450Anexo_Link ;
      private String A1450Anexo_Link ;
      private String Z1123Anexo_Descricao ;
      private String A1123Anexo_Descricao ;
      private String Z1101Anexo_Arquivo ;
      private String A1101Anexo_Arquivo ;
      private SdtAnexos bcAnexos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00369_A1498Anexo_DemandaCod ;
      private bool[] BC00369_n1498Anexo_DemandaCod ;
      private int[] BC00367_A1496Anexo_AreaTrabalhoCod ;
      private bool[] BC00367_n1496Anexo_AreaTrabalhoCod ;
      private int[] BC003614_A1106Anexo_Codigo ;
      private int[] BC003614_A1102Anexo_UserId ;
      private DateTime[] BC003614_A1103Anexo_Data ;
      private String[] BC003614_A646TipoDocumento_Nome ;
      private String[] BC003614_A1123Anexo_Descricao ;
      private bool[] BC003614_n1123Anexo_Descricao ;
      private String[] BC003614_A1450Anexo_Link ;
      private bool[] BC003614_n1450Anexo_Link ;
      private int[] BC003614_A1495Anexo_Owner ;
      private bool[] BC003614_n1495Anexo_Owner ;
      private String[] BC003614_A1108Anexo_TipoArq ;
      private bool[] BC003614_n1108Anexo_TipoArq ;
      private String[] BC003614_A1107Anexo_NomeArq ;
      private bool[] BC003614_n1107Anexo_NomeArq ;
      private int[] BC003614_A645TipoDocumento_Codigo ;
      private bool[] BC003614_n645TipoDocumento_Codigo ;
      private int[] BC003614_A1498Anexo_DemandaCod ;
      private bool[] BC003614_n1498Anexo_DemandaCod ;
      private String[] BC003614_A1101Anexo_Arquivo ;
      private bool[] BC003614_n1101Anexo_Arquivo ;
      private String[] BC003612_A646TipoDocumento_Nome ;
      private int[] BC003615_A1106Anexo_Codigo ;
      private int[] BC003611_A1106Anexo_Codigo ;
      private int[] BC003611_A1102Anexo_UserId ;
      private DateTime[] BC003611_A1103Anexo_Data ;
      private String[] BC003611_A1123Anexo_Descricao ;
      private bool[] BC003611_n1123Anexo_Descricao ;
      private String[] BC003611_A1450Anexo_Link ;
      private bool[] BC003611_n1450Anexo_Link ;
      private int[] BC003611_A1495Anexo_Owner ;
      private bool[] BC003611_n1495Anexo_Owner ;
      private String[] BC003611_A1108Anexo_TipoArq ;
      private bool[] BC003611_n1108Anexo_TipoArq ;
      private String[] BC003611_A1107Anexo_NomeArq ;
      private bool[] BC003611_n1107Anexo_NomeArq ;
      private int[] BC003611_A645TipoDocumento_Codigo ;
      private bool[] BC003611_n645TipoDocumento_Codigo ;
      private String[] BC003611_A1101Anexo_Arquivo ;
      private bool[] BC003611_n1101Anexo_Arquivo ;
      private int[] BC003610_A1106Anexo_Codigo ;
      private int[] BC003610_A1102Anexo_UserId ;
      private DateTime[] BC003610_A1103Anexo_Data ;
      private String[] BC003610_A1123Anexo_Descricao ;
      private bool[] BC003610_n1123Anexo_Descricao ;
      private String[] BC003610_A1450Anexo_Link ;
      private bool[] BC003610_n1450Anexo_Link ;
      private int[] BC003610_A1495Anexo_Owner ;
      private bool[] BC003610_n1495Anexo_Owner ;
      private String[] BC003610_A1108Anexo_TipoArq ;
      private bool[] BC003610_n1108Anexo_TipoArq ;
      private String[] BC003610_A1107Anexo_NomeArq ;
      private bool[] BC003610_n1107Anexo_NomeArq ;
      private int[] BC003610_A645TipoDocumento_Codigo ;
      private bool[] BC003610_n645TipoDocumento_Codigo ;
      private String[] BC003610_A1101Anexo_Arquivo ;
      private bool[] BC003610_n1101Anexo_Arquivo ;
      private int[] BC003616_A1106Anexo_Codigo ;
      private int[] BC003621_A1498Anexo_DemandaCod ;
      private bool[] BC003621_n1498Anexo_DemandaCod ;
      private int[] BC003625_A1496Anexo_AreaTrabalhoCod ;
      private bool[] BC003625_n1496Anexo_AreaTrabalhoCod ;
      private String[] BC003626_A646TipoDocumento_Nome ;
      private int[] BC003627_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC003629_A1106Anexo_Codigo ;
      private int[] BC003629_A1102Anexo_UserId ;
      private DateTime[] BC003629_A1103Anexo_Data ;
      private String[] BC003629_A646TipoDocumento_Nome ;
      private String[] BC003629_A1123Anexo_Descricao ;
      private bool[] BC003629_n1123Anexo_Descricao ;
      private String[] BC003629_A1450Anexo_Link ;
      private bool[] BC003629_n1450Anexo_Link ;
      private int[] BC003629_A1495Anexo_Owner ;
      private bool[] BC003629_n1495Anexo_Owner ;
      private String[] BC003629_A1108Anexo_TipoArq ;
      private bool[] BC003629_n1108Anexo_TipoArq ;
      private String[] BC003629_A1107Anexo_NomeArq ;
      private bool[] BC003629_n1107Anexo_NomeArq ;
      private int[] BC003629_A645TipoDocumento_Codigo ;
      private bool[] BC003629_n645TipoDocumento_Codigo ;
      private int[] BC003629_A1498Anexo_DemandaCod ;
      private bool[] BC003629_n1498Anexo_DemandaCod ;
      private String[] BC003629_A1101Anexo_Arquivo ;
      private bool[] BC003629_n1101Anexo_Arquivo ;
      private int[] BC003630_A1106Anexo_Codigo ;
      private int[] BC003630_A1109AnexoDe_Id ;
      private int[] BC003630_A1110AnexoDe_Tabela ;
      private int[] BC003631_A1106Anexo_Codigo ;
      private int[] BC003631_A1109AnexoDe_Id ;
      private int[] BC003631_A1110AnexoDe_Tabela ;
      private int[] BC00363_A1106Anexo_Codigo ;
      private int[] BC00363_A1109AnexoDe_Id ;
      private int[] BC00363_A1110AnexoDe_Tabela ;
      private int[] BC00362_A1106Anexo_Codigo ;
      private int[] BC00362_A1109AnexoDe_Id ;
      private int[] BC00362_A1110AnexoDe_Tabela ;
      private int[] BC003634_A1106Anexo_Codigo ;
      private int[] BC003634_A1109AnexoDe_Id ;
      private int[] BC003634_A1110AnexoDe_Tabela ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class anexos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new ForEachCursor(def[22])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003614 ;
          prmBC003614 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00369 ;
          prmBC00369 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00367 ;
          prmBC00367 = new Object[] {
          new Object[] {"@Anexo_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003612 ;
          prmBC003612 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003615 ;
          prmBC003615 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003611 ;
          prmBC003611 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003610 ;
          prmBC003610 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003616 ;
          prmBC003616 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Anexo_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Anexo_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Anexo_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003617 ;
          prmBC003617 = new Object[] {
          new Object[] {"@Anexo_UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Anexo_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Anexo_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Anexo_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003618 ;
          prmBC003618 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003619 ;
          prmBC003619 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003626 ;
          prmBC003626 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003627 ;
          prmBC003627 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003621 ;
          prmBC003621 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003625 ;
          prmBC003625 = new Object[] {
          new Object[] {"@Anexo_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003629 ;
          prmBC003629 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003630 ;
          prmBC003630 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003631 ;
          prmBC003631 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00363 ;
          prmBC00363 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00362 ;
          prmBC00362 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003632 ;
          prmBC003632 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003633 ;
          prmBC003633 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003634 ;
          prmBC003634 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00362", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00362,1,0,true,false )
             ,new CursorDef("BC00363", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00363,1,0,true,false )
             ,new CursorDef("BC00367", "SELECT COALESCE( T1.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod FROM (SELECT CASE  WHEN COALESCE( T2.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T3.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod FROM (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T2,  (SELECT T5.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[ContagemResultado_Codigo] FROM ([ContagemResultado] T4 WITH (NOLOCK) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) WHERE T4.[ContagemResultado_Codigo] = @Anexo_DemandaCod ) T3 WHERE T2.[Anexo_Codigo] = @Anexo_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00367,1,0,true,false )
             ,new CursorDef("BC00369", "SELECT COALESCE( T1.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod FROM (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T1 WHERE T1.[Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00369,1,0,true,false )
             ,new CursorDef("BC003610", "SELECT [Anexo_Codigo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo], [Anexo_Arquivo] FROM [Anexos] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003610,1,0,true,false )
             ,new CursorDef("BC003611", "SELECT [Anexo_Codigo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo], [Anexo_Arquivo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003611,1,0,true,false )
             ,new CursorDef("BC003612", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003612,1,0,true,false )
             ,new CursorDef("BC003614", "SELECT TM1.[Anexo_Codigo], TM1.[Anexo_UserId], TM1.[Anexo_Data], T3.[TipoDocumento_Nome], TM1.[Anexo_Descricao], TM1.[Anexo_Link], TM1.[Anexo_Owner], TM1.[Anexo_TipoArq], TM1.[Anexo_NomeArq], TM1.[TipoDocumento_Codigo], COALESCE( T2.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod, TM1.[Anexo_Arquivo] FROM (([Anexos] TM1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) GROUP BY [Anexo_Codigo] ) T2 ON T2.[Anexo_Codigo] = TM1.[Anexo_Codigo]) LEFT JOIN [TipoDocumento] T3 WITH (NOLOCK) ON T3.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[Anexo_Codigo] = @Anexo_Codigo ORDER BY TM1.[Anexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003614,100,0,true,false )
             ,new CursorDef("BC003615", "SELECT [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003615,1,0,true,false )
             ,new CursorDef("BC003616", "INSERT INTO [Anexos]([Anexo_Arquivo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo]) VALUES(@Anexo_Arquivo, @Anexo_UserId, @Anexo_Data, @Anexo_Descricao, @Anexo_Link, @Anexo_Owner, @Anexo_TipoArq, @Anexo_NomeArq, @TipoDocumento_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003616)
             ,new CursorDef("BC003617", "UPDATE [Anexos] SET [Anexo_UserId]=@Anexo_UserId, [Anexo_Data]=@Anexo_Data, [Anexo_Descricao]=@Anexo_Descricao, [Anexo_Link]=@Anexo_Link, [Anexo_Owner]=@Anexo_Owner, [Anexo_TipoArq]=@Anexo_TipoArq, [Anexo_NomeArq]=@Anexo_NomeArq, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmBC003617)
             ,new CursorDef("BC003618", "UPDATE [Anexos] SET [Anexo_Arquivo]=@Anexo_Arquivo  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmBC003618)
             ,new CursorDef("BC003619", "DELETE FROM [Anexos]  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmBC003619)
             ,new CursorDef("BC003621", "SELECT COALESCE( T1.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod FROM (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T1 WHERE T1.[Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003621,1,0,true,false )
             ,new CursorDef("BC003625", "SELECT COALESCE( T1.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod FROM (SELECT CASE  WHEN COALESCE( T2.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T3.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod FROM (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T2,  (SELECT T5.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[ContagemResultado_Codigo] FROM ([ContagemResultado] T4 WITH (NOLOCK) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) WHERE T4.[ContagemResultado_Codigo] = @Anexo_DemandaCod ) T3 WHERE T2.[Anexo_Codigo] = @Anexo_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003625,1,0,true,false )
             ,new CursorDef("BC003626", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003626,1,0,true,false )
             ,new CursorDef("BC003627", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_AnxCod] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003627,1,0,true,true )
             ,new CursorDef("BC003629", "SELECT TM1.[Anexo_Codigo], TM1.[Anexo_UserId], TM1.[Anexo_Data], T3.[TipoDocumento_Nome], TM1.[Anexo_Descricao], TM1.[Anexo_Link], TM1.[Anexo_Owner], TM1.[Anexo_TipoArq], TM1.[Anexo_NomeArq], TM1.[TipoDocumento_Codigo], COALESCE( T2.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod, TM1.[Anexo_Arquivo] FROM (([Anexos] TM1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) GROUP BY [Anexo_Codigo] ) T2 ON T2.[Anexo_Codigo] = TM1.[Anexo_Codigo]) LEFT JOIN [TipoDocumento] T3 WITH (NOLOCK) ON T3.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[Anexo_Codigo] = @Anexo_Codigo ORDER BY TM1.[Anexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003629,100,0,true,false )
             ,new CursorDef("BC003630", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo and [AnexoDe_Id] = @AnexoDe_Id and [AnexoDe_Tabela] = @AnexoDe_Tabela ORDER BY [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003630,11,0,true,false )
             ,new CursorDef("BC003631", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003631,1,0,true,false )
             ,new CursorDef("BC003632", "INSERT INTO [AnexosDe]([Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela]) VALUES(@Anexo_Codigo, @AnexoDe_Id, @AnexoDe_Tabela)", GxErrorMask.GX_NOMASK,prmBC003632)
             ,new CursorDef("BC003633", "DELETE FROM [AnexosDe]  WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela", GxErrorMask.GX_NOMASK,prmBC003633)
             ,new CursorDef("BC003634", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ORDER BY [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003634,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(12, rslt.getString(8, 10), rslt.getString(9, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(12, rslt.getString(8, 10), rslt.getString(9, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                stmt.SetParameter(9, (int)parms[14]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
