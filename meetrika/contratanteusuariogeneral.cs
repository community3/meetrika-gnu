/*
               File: ContratanteUsuarioGeneral
        Description: Contratante Usuario General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:55.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratanteusuariogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratanteusuariogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratanteusuariogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratanteUsuario_ContratanteCod ,
                           int aP1_ContratanteUsuario_UsuarioCod )
      {
         this.A63ContratanteUsuario_ContratanteCod = aP0_ContratanteUsuario_ContratanteCod;
         this.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
                  A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A63ContratanteUsuario_ContratanteCod,(int)A60ContratanteUsuario_UsuarioCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA8H2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "ContratanteUsuarioGeneral";
               context.Gx_err = 0;
               /* Using cursor H008H2 */
               pr_default.execute(0, new Object[] {A63ContratanteUsuario_ContratanteCod});
               A340ContratanteUsuario_ContratantePesCod = H008H2_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H008H2_n340ContratanteUsuario_ContratantePesCod[0];
               A65ContratanteUsuario_ContratanteFan = H008H2_A65ContratanteUsuario_ContratanteFan[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
               n65ContratanteUsuario_ContratanteFan = H008H2_n65ContratanteUsuario_ContratanteFan[0];
               pr_default.close(0);
               /* Using cursor H008H3 */
               pr_default.execute(1, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
               A64ContratanteUsuario_ContratanteRaz = H008H3_A64ContratanteUsuario_ContratanteRaz[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
               n64ContratanteUsuario_ContratanteRaz = H008H3_n64ContratanteUsuario_ContratanteRaz[0];
               pr_default.close(1);
               /* Using cursor H008H4 */
               pr_default.execute(2, new Object[] {A60ContratanteUsuario_UsuarioCod});
               A61ContratanteUsuario_UsuarioPessoaCod = H008H4_A61ContratanteUsuario_UsuarioPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
               n61ContratanteUsuario_UsuarioPessoaCod = H008H4_n61ContratanteUsuario_UsuarioPessoaCod[0];
               pr_default.close(2);
               /* Using cursor H008H5 */
               pr_default.execute(3, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
               A62ContratanteUsuario_UsuarioPessoaNom = H008H5_A62ContratanteUsuario_UsuarioPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
               n62ContratanteUsuario_UsuarioPessoaNom = H008H5_n62ContratanteUsuario_UsuarioPessoaNom[0];
               pr_default.close(3);
               WS8H2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratante Usuario General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117155582");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratanteusuariogeneral.aspx") + "?" + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm8H2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratanteusuariogeneral.js", "?20203117155584");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratanteUsuarioGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante Usuario General" ;
      }

      protected void WB8H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratanteusuariogeneral.aspx");
            }
            wb_table1_2_8H2( true) ;
         }
         else
         {
            wb_table1_2_8H2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8H2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START8H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratante Usuario General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP8H0( ) ;
            }
         }
      }

      protected void WS8H2( )
      {
         START8H2( ) ;
         EVT8H2( ) ;
      }

      protected void EVT8H2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E118H2 */
                                    E118H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E128H2 */
                                    E128H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E138H2 */
                                    E138H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E148H2 */
                                    E148H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8H2( ) ;
            }
         }
      }

      protected void PA8H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8H2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "ContratanteUsuarioGeneral";
         context.Gx_err = 0;
      }

      protected void RF8H2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H008H6 */
            pr_default.execute(4, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               /* Execute user event: E128H2 */
               E128H2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            WB8H0( ) ;
         }
      }

      protected void STRUP8H0( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "ContratanteUsuarioGeneral";
         context.Gx_err = 0;
         /* Using cursor H008H7 */
         pr_default.execute(5, new Object[] {A63ContratanteUsuario_ContratanteCod});
         A340ContratanteUsuario_ContratantePesCod = H008H7_A340ContratanteUsuario_ContratantePesCod[0];
         n340ContratanteUsuario_ContratantePesCod = H008H7_n340ContratanteUsuario_ContratantePesCod[0];
         A65ContratanteUsuario_ContratanteFan = H008H7_A65ContratanteUsuario_ContratanteFan[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
         n65ContratanteUsuario_ContratanteFan = H008H7_n65ContratanteUsuario_ContratanteFan[0];
         pr_default.close(5);
         /* Using cursor H008H8 */
         pr_default.execute(6, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
         A64ContratanteUsuario_ContratanteRaz = H008H8_A64ContratanteUsuario_ContratanteRaz[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
         n64ContratanteUsuario_ContratanteRaz = H008H8_n64ContratanteUsuario_ContratanteRaz[0];
         pr_default.close(6);
         /* Using cursor H008H9 */
         pr_default.execute(7, new Object[] {A60ContratanteUsuario_UsuarioCod});
         A61ContratanteUsuario_UsuarioPessoaCod = H008H9_A61ContratanteUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
         n61ContratanteUsuario_UsuarioPessoaCod = H008H9_n61ContratanteUsuario_UsuarioPessoaCod[0];
         pr_default.close(7);
         /* Using cursor H008H10 */
         pr_default.execute(8, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
         A62ContratanteUsuario_UsuarioPessoaNom = H008H10_A62ContratanteUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
         n62ContratanteUsuario_UsuarioPessoaNom = H008H10_n62ContratanteUsuario_UsuarioPessoaNom[0];
         pr_default.close(8);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118H2 */
         E118H2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A65ContratanteUsuario_ContratanteFan = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteFan_Internalname));
            n65ContratanteUsuario_ContratanteFan = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
            A64ContratanteUsuario_ContratanteRaz = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteRaz_Internalname));
            n64ContratanteUsuario_ContratanteRaz = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
            A61ContratanteUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioPessoaCod_Internalname), ",", "."));
            n61ContratanteUsuario_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            A62ContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratanteUsuario_UsuarioPessoaNom_Internalname));
            n62ContratanteUsuario_UsuarioPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
            /* Read saved values. */
            wcpOA63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA63ContratanteUsuario_ContratanteCod"), ",", "."));
            wcpOA60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA60ContratanteUsuario_UsuarioCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118H2 */
         E118H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E118H2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E128H2( )
      {
         /* Load Routine */
         edtContratanteUsuario_ContratanteRaz_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A340ContratanteUsuario_ContratantePesCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratanteUsuario_ContratanteRaz_Internalname, "Link", edtContratanteUsuario_ContratanteRaz_Link);
         edtContratanteUsuario_UsuarioPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A60ContratanteUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratanteUsuario_UsuarioPessoaNom_Internalname, "Link", edtContratanteUsuario_UsuarioPessoaNom_Link);
      }

      protected void E138H2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E148H2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A63ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +A60ContratanteUsuario_UsuarioCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratanteUsuario";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratanteUsuario_ContratanteCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratanteUsuario_ContratanteCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratanteUsuario_UsuarioCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8ContratanteUsuario_UsuarioCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_8H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8H2( true) ;
         }
         else
         {
            wb_table2_8_8H2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_8H2( true) ;
         }
         else
         {
            wb_table3_41_8H2( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_8H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8H2e( true) ;
         }
         else
         {
            wb_table1_2_8H2e( false) ;
         }
      }

      protected void wb_table3_41_8H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_8H2e( true) ;
         }
         else
         {
            wb_table3_41_8H2e( false) ;
         }
      }

      protected void wb_table2_8_8H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_contratantecod_Internalname, "Usuario_Contratante Cod", "", "", lblTextblockcontratanteusuario_contratantecod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_ContratanteCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_contratantefan_Internalname, "Usuario_Contratante Fan", "", "", lblTextblockcontratanteusuario_contratantefan_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteFan_Internalname, StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan), StringUtil.RTrim( context.localUtil.Format( A65ContratanteUsuario_ContratanteFan, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_ContratanteFan_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_contratanteraz_Internalname, "Usuario_Contratante Raz", "", "", lblTextblockcontratanteusuario_contratanteraz_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteRaz_Internalname, StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz), StringUtil.RTrim( context.localUtil.Format( A64ContratanteUsuario_ContratanteRaz, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratanteUsuario_ContratanteRaz_Link, "", "", "", edtContratanteUsuario_ContratanteRaz_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_usuariocod_Internalname, "Usuario_Cod", "", "", lblTextblockcontratanteusuario_usuariocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_UsuarioCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_usuariopessoacod_Internalname, "Cod", "", "", lblTextblockcontratanteusuario_usuariopessoacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_UsuarioPessoaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_usuariopessoanom_Internalname, "Nom", "", "", lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A62ContratanteUsuario_UsuarioPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratanteUsuario_UsuarioPessoaNom_Link, "", "", "", edtContratanteUsuario_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8H2e( true) ;
         }
         else
         {
            wb_table2_8_8H2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A63ContratanteUsuario_ContratanteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         A60ContratanteUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8H2( ) ;
         WS8H2( ) ;
         WE8H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA63ContratanteUsuario_ContratanteCod = (String)((String)getParm(obj,0));
         sCtrlA60ContratanteUsuario_UsuarioCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA8H2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratanteusuariogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA8H2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A63ContratanteUsuario_ContratanteCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            A60ContratanteUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         }
         wcpOA63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA63ContratanteUsuario_ContratanteCod"), ",", "."));
         wcpOA60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA60ContratanteUsuario_UsuarioCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A63ContratanteUsuario_ContratanteCod != wcpOA63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != wcpOA60ContratanteUsuario_UsuarioCod ) ) )
         {
            setjustcreated();
         }
         wcpOA63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
         wcpOA60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA63ContratanteUsuario_ContratanteCod = cgiGet( sPrefix+"A63ContratanteUsuario_ContratanteCod_CTRL");
         if ( StringUtil.Len( sCtrlA63ContratanteUsuario_ContratanteCod) > 0 )
         {
            A63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA63ContratanteUsuario_ContratanteCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         }
         else
         {
            A63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A63ContratanteUsuario_ContratanteCod_PARM"), ",", "."));
         }
         sCtrlA60ContratanteUsuario_UsuarioCod = cgiGet( sPrefix+"A60ContratanteUsuario_UsuarioCod_CTRL");
         if ( StringUtil.Len( sCtrlA60ContratanteUsuario_UsuarioCod) > 0 )
         {
            A60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA60ContratanteUsuario_UsuarioCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         }
         else
         {
            A60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A60ContratanteUsuario_UsuarioCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA8H2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS8H2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS8H2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A63ContratanteUsuario_ContratanteCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA63ContratanteUsuario_ContratanteCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A63ContratanteUsuario_ContratanteCod_CTRL", StringUtil.RTrim( sCtrlA63ContratanteUsuario_ContratanteCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A60ContratanteUsuario_UsuarioCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA60ContratanteUsuario_UsuarioCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A60ContratanteUsuario_UsuarioCod_CTRL", StringUtil.RTrim( sCtrlA60ContratanteUsuario_UsuarioCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE8H2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117155631");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratanteusuariogeneral.js", "?20203117155632");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratanteusuario_contratantecod_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_CONTRATANTECOD";
         edtContratanteUsuario_ContratanteCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTECOD";
         lblTextblockcontratanteusuario_contratantefan_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_CONTRATANTEFAN";
         edtContratanteUsuario_ContratanteFan_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTEFAN";
         lblTextblockcontratanteusuario_contratanteraz_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_CONTRATANTERAZ";
         edtContratanteUsuario_ContratanteRaz_Internalname = sPrefix+"CONTRATANTEUSUARIO_CONTRATANTERAZ";
         lblTextblockcontratanteusuario_usuariocod_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_USUARIOCOD";
         edtContratanteUsuario_UsuarioCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOCOD";
         lblTextblockcontratanteusuario_usuariopessoacod_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_USUARIOPESSOACOD";
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOACOD";
         lblTextblockcontratanteusuario_usuariopessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATANTEUSUARIO_USUARIOPESSOANOM";
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = sPrefix+"CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratanteUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioCod_Jsonclick = "";
         edtContratanteUsuario_ContratanteRaz_Jsonclick = "";
         edtContratanteUsuario_ContratanteFan_Jsonclick = "";
         edtContratanteUsuario_ContratanteCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaNom_Link = "";
         edtContratanteUsuario_ContratanteRaz_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E138H2',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E148H2',iparms:[{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         scmdbuf = "";
         H008H2_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H008H2_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H008H2_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H008H2_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         A65ContratanteUsuario_ContratanteFan = "";
         H008H3_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H008H3_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         A64ContratanteUsuario_ContratanteRaz = "";
         H008H4_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H008H4_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008H5_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008H5_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H008H6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H008H6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H008H6_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H008H6_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H008H6_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008H6_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H008H6_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H008H6_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008H6_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H008H6_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H008H6_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H008H6_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         H008H7_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H008H7_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H008H7_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H008H7_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         H008H8_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H008H8_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H008H9_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H008H9_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008H10_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008H10_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratanteusuario_contratantecod_Jsonclick = "";
         lblTextblockcontratanteusuario_contratantefan_Jsonclick = "";
         lblTextblockcontratanteusuario_contratanteraz_Jsonclick = "";
         lblTextblockcontratanteusuario_usuariocod_Jsonclick = "";
         lblTextblockcontratanteusuario_usuariopessoacod_Jsonclick = "";
         lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA63ContratanteUsuario_ContratanteCod = "";
         sCtrlA60ContratanteUsuario_UsuarioCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratanteusuariogeneral__default(),
            new Object[][] {
                new Object[] {
               H008H2_A340ContratanteUsuario_ContratantePesCod, H008H2_n340ContratanteUsuario_ContratantePesCod, H008H2_A65ContratanteUsuario_ContratanteFan, H008H2_n65ContratanteUsuario_ContratanteFan
               }
               , new Object[] {
               H008H3_A64ContratanteUsuario_ContratanteRaz, H008H3_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               H008H4_A61ContratanteUsuario_UsuarioPessoaCod, H008H4_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H008H5_A62ContratanteUsuario_UsuarioPessoaNom, H008H5_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H008H6_A63ContratanteUsuario_ContratanteCod, H008H6_A60ContratanteUsuario_UsuarioCod, H008H6_A340ContratanteUsuario_ContratantePesCod, H008H6_n340ContratanteUsuario_ContratantePesCod, H008H6_A62ContratanteUsuario_UsuarioPessoaNom, H008H6_n62ContratanteUsuario_UsuarioPessoaNom, H008H6_A61ContratanteUsuario_UsuarioPessoaCod, H008H6_n61ContratanteUsuario_UsuarioPessoaCod, H008H6_A64ContratanteUsuario_ContratanteRaz, H008H6_n64ContratanteUsuario_ContratanteRaz,
               H008H6_A65ContratanteUsuario_ContratanteFan, H008H6_n65ContratanteUsuario_ContratanteFan
               }
               , new Object[] {
               H008H7_A340ContratanteUsuario_ContratantePesCod, H008H7_n340ContratanteUsuario_ContratantePesCod, H008H7_A65ContratanteUsuario_ContratanteFan, H008H7_n65ContratanteUsuario_ContratanteFan
               }
               , new Object[] {
               H008H8_A64ContratanteUsuario_ContratanteRaz, H008H8_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               H008H9_A61ContratanteUsuario_UsuarioPessoaCod, H008H9_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H008H10_A62ContratanteUsuario_UsuarioPessoaNom, H008H10_n62ContratanteUsuario_UsuarioPessoaNom
               }
            }
         );
         AV16Pgmname = "ContratanteUsuarioGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "ContratanteUsuarioGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int wcpOA63ContratanteUsuario_ContratanteCod ;
      private int wcpOA60ContratanteUsuario_UsuarioCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int AV7ContratanteUsuario_ContratanteCod ;
      private int AV8ContratanteUsuario_UsuarioCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String scmdbuf ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratanteUsuario_ContratanteFan_Internalname ;
      private String edtContratanteUsuario_ContratanteRaz_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Internalname ;
      private String edtContratanteUsuario_ContratanteRaz_Link ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratanteusuario_contratantecod_Internalname ;
      private String lblTextblockcontratanteusuario_contratantecod_Jsonclick ;
      private String edtContratanteUsuario_ContratanteCod_Internalname ;
      private String edtContratanteUsuario_ContratanteCod_Jsonclick ;
      private String lblTextblockcontratanteusuario_contratantefan_Internalname ;
      private String lblTextblockcontratanteusuario_contratantefan_Jsonclick ;
      private String edtContratanteUsuario_ContratanteFan_Jsonclick ;
      private String lblTextblockcontratanteusuario_contratanteraz_Internalname ;
      private String lblTextblockcontratanteusuario_contratanteraz_Jsonclick ;
      private String edtContratanteUsuario_ContratanteRaz_Jsonclick ;
      private String lblTextblockcontratanteusuario_usuariocod_Internalname ;
      private String lblTextblockcontratanteusuario_usuariocod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioCod_Internalname ;
      private String edtContratanteUsuario_UsuarioCod_Jsonclick ;
      private String lblTextblockcontratanteusuario_usuariopessoacod_Internalname ;
      private String lblTextblockcontratanteusuario_usuariopessoacod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Jsonclick ;
      private String lblTextblockcontratanteusuario_usuariopessoanom_Internalname ;
      private String lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Jsonclick ;
      private String sCtrlA63ContratanteUsuario_ContratanteCod ;
      private String sCtrlA60ContratanteUsuario_UsuarioCod ;
      private bool entryPointCalled ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H008H2_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H008H2_n340ContratanteUsuario_ContratantePesCod ;
      private String[] H008H2_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H008H2_n65ContratanteUsuario_ContratanteFan ;
      private String[] H008H3_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H008H3_n64ContratanteUsuario_ContratanteRaz ;
      private int[] H008H4_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H008H4_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] H008H5_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H008H5_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H008H6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H008H6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H008H6_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H008H6_n340ContratanteUsuario_ContratantePesCod ;
      private String[] H008H6_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H008H6_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H008H6_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H008H6_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] H008H6_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H008H6_n64ContratanteUsuario_ContratanteRaz ;
      private String[] H008H6_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H008H6_n65ContratanteUsuario_ContratanteFan ;
      private int[] H008H7_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H008H7_n340ContratanteUsuario_ContratantePesCod ;
      private String[] H008H7_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H008H7_n65ContratanteUsuario_ContratanteFan ;
      private String[] H008H8_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H008H8_n64ContratanteUsuario_ContratanteRaz ;
      private int[] H008H9_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H008H9_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] H008H10_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H008H10_n62ContratanteUsuario_UsuarioPessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class contratanteusuariogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008H2 ;
          prmH008H2 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H3 ;
          prmH008H3 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H4 ;
          prmH008H4 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H5 ;
          prmH008H5 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H6 ;
          prmH008H6 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H7 ;
          prmH008H7 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H8 ;
          prmH008H8 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H9 ;
          prmH008H9 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008H10 ;
          prmH008H10 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008H2", "SELECT [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H2,1,0,true,true )
             ,new CursorDef("H008H3", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H3,1,0,true,true )
             ,new CursorDef("H008H4", "SELECT [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H4,1,0,true,true )
             ,new CursorDef("H008H5", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H5,1,0,true,true )
             ,new CursorDef("H008H6", "SELECT T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T5.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T4.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T3.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and T1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H6,1,0,true,true )
             ,new CursorDef("H008H7", "SELECT [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H7,1,0,true,true )
             ,new CursorDef("H008H8", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H8,1,0,true,true )
             ,new CursorDef("H008H9", "SELECT [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H9,1,0,true,true )
             ,new CursorDef("H008H10", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008H10,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
