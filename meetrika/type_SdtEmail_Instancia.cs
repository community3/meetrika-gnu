/*
               File: type_SdtEmail_Instancia
        Description: Email_Instancia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:41.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Email_Instancia" )]
   [XmlType(TypeName =  "Email_Instancia" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtEmail_Instancia : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmail_Instancia( )
      {
         /* Constructor for serialization */
         gxTv_SdtEmail_Instancia_Email_titulo = "";
         gxTv_SdtEmail_Instancia_Email_corpo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_titulo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_corpo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_parms = "";
         gxTv_SdtEmail_Instancia_Mode = "";
         gxTv_SdtEmail_Instancia_Email_titulo_Z = "";
         gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z = "";
      }

      public SdtEmail_Instancia( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV1666Email_Instancia_Guid )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV1666Email_Instancia_Guid});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Email_Instancia_Guid", typeof(Guid)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Email_Instancia");
         metadata.Set("BT", "Email_Instancia");
         metadata.Set("PK", "[ \"Email_Instancia_Guid\" ]");
         metadata.Set("PKAssigned", "[ \"Email_Instancia_Guid\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Email_Guid\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_guid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_guid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_titulo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_titulo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_areatrabalho_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtEmail_Instancia deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtEmail_Instancia)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtEmail_Instancia obj ;
         obj = this;
         obj.gxTpr_Email_instancia_guid = (Guid)(deserialized.gxTpr_Email_instancia_guid);
         obj.gxTpr_Email_guid = (Guid)(deserialized.gxTpr_Email_guid);
         obj.gxTpr_Email_titulo = deserialized.gxTpr_Email_titulo;
         obj.gxTpr_Email_corpo = deserialized.gxTpr_Email_corpo;
         obj.gxTpr_Email_instancia_titulo = deserialized.gxTpr_Email_instancia_titulo;
         obj.gxTpr_Email_instancia_corpo = deserialized.gxTpr_Email_instancia_corpo;
         obj.gxTpr_Email_instancia_areatrabalho = deserialized.gxTpr_Email_instancia_areatrabalho;
         obj.gxTpr_Email_instancia_parms = deserialized.gxTpr_Email_instancia_parms;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Email_instancia_guid_Z = (Guid)(deserialized.gxTpr_Email_instancia_guid_Z);
         obj.gxTpr_Email_guid_Z = (Guid)(deserialized.gxTpr_Email_guid_Z);
         obj.gxTpr_Email_titulo_Z = deserialized.gxTpr_Email_titulo_Z;
         obj.gxTpr_Email_instancia_titulo_Z = deserialized.gxTpr_Email_instancia_titulo_Z;
         obj.gxTpr_Email_instancia_areatrabalho_Z = deserialized.gxTpr_Email_instancia_areatrabalho_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Guid") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_guid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Guid") )
               {
                  gxTv_SdtEmail_Instancia_Email_guid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Titulo") )
               {
                  gxTv_SdtEmail_Instancia_Email_titulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Corpo") )
               {
                  gxTv_SdtEmail_Instancia_Email_corpo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Titulo") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_titulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Corpo") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_corpo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_AreaTrabalho") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Parms") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_parms = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtEmail_Instancia_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtEmail_Instancia_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Guid_Z") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_guid_Z = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Guid_Z") )
               {
                  gxTv_SdtEmail_Instancia_Email_guid_Z = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Titulo_Z") )
               {
                  gxTv_SdtEmail_Instancia_Email_titulo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Titulo_Z") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_AreaTrabalho_Z") )
               {
                  gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Email_Instancia";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Email_Instancia_Guid", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_guid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Guid", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_guid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Titulo", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_titulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Corpo", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_corpo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Titulo", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_titulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Corpo", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_corpo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_AreaTrabalho", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Parms", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_parms));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtEmail_Instancia_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Guid_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_guid_Z.ToString()));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Guid_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_guid_Z.ToString()));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Titulo_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_titulo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Titulo_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_AreaTrabalho_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Email_Instancia_Guid", gxTv_SdtEmail_Instancia_Email_instancia_guid, false);
         AddObjectProperty("Email_Guid", gxTv_SdtEmail_Instancia_Email_guid, false);
         AddObjectProperty("Email_Titulo", gxTv_SdtEmail_Instancia_Email_titulo, false);
         AddObjectProperty("Email_Corpo", gxTv_SdtEmail_Instancia_Email_corpo, false);
         AddObjectProperty("Email_Instancia_Titulo", gxTv_SdtEmail_Instancia_Email_instancia_titulo, false);
         AddObjectProperty("Email_Instancia_Corpo", gxTv_SdtEmail_Instancia_Email_instancia_corpo, false);
         AddObjectProperty("Email_Instancia_AreaTrabalho", gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho, false);
         AddObjectProperty("Email_Instancia_Parms", gxTv_SdtEmail_Instancia_Email_instancia_parms, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtEmail_Instancia_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtEmail_Instancia_Initialized, false);
            AddObjectProperty("Email_Instancia_Guid_Z", gxTv_SdtEmail_Instancia_Email_instancia_guid_Z, false);
            AddObjectProperty("Email_Guid_Z", gxTv_SdtEmail_Instancia_Email_guid_Z, false);
            AddObjectProperty("Email_Titulo_Z", gxTv_SdtEmail_Instancia_Email_titulo_Z, false);
            AddObjectProperty("Email_Instancia_Titulo_Z", gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z, false);
            AddObjectProperty("Email_Instancia_AreaTrabalho_Z", gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Guid" )]
      [  XmlElement( ElementName = "Email_Instancia_Guid"   )]
      public Guid gxTpr_Email_instancia_guid
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_guid ;
         }

         set {
            if ( gxTv_SdtEmail_Instancia_Email_instancia_guid != value )
            {
               gxTv_SdtEmail_Instancia_Mode = "INS";
               this.gxTv_SdtEmail_Instancia_Email_instancia_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Email_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Email_titulo_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z_SetNull( );
            }
            gxTv_SdtEmail_Instancia_Email_instancia_guid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Guid" )]
      [  XmlElement( ElementName = "Email_Guid"   )]
      public Guid gxTpr_Email_guid
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_guid ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_guid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Titulo" )]
      [  XmlElement( ElementName = "Email_Titulo"   )]
      public String gxTpr_Email_titulo
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_titulo ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_titulo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Corpo" )]
      [  XmlElement( ElementName = "Email_Corpo"   )]
      public String gxTpr_Email_corpo
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_corpo ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_corpo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Titulo" )]
      [  XmlElement( ElementName = "Email_Instancia_Titulo"   )]
      public String gxTpr_Email_instancia_titulo
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_titulo ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_titulo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Corpo" )]
      [  XmlElement( ElementName = "Email_Instancia_Corpo"   )]
      public String gxTpr_Email_instancia_corpo
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_corpo ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_corpo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_AreaTrabalho" )]
      [  XmlElement( ElementName = "Email_Instancia_AreaTrabalho"   )]
      public int gxTpr_Email_instancia_areatrabalho
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Parms" )]
      [  XmlElement( ElementName = "Email_Instancia_Parms"   )]
      public String gxTpr_Email_instancia_parms
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_parms ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_parms = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtEmail_Instancia_Mode ;
         }

         set {
            gxTv_SdtEmail_Instancia_Mode = (String)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Mode_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Mode = "";
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtEmail_Instancia_Initialized ;
         }

         set {
            gxTv_SdtEmail_Instancia_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Initialized_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Guid_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Guid_Z"   )]
      public Guid gxTpr_Email_instancia_guid_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_guid_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_guid_Z = (Guid)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Email_instancia_guid_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Email_instancia_guid_Z = (Guid)(System.Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Email_instancia_guid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Guid_Z" )]
      [  XmlElement( ElementName = "Email_Guid_Z"   )]
      public Guid gxTpr_Email_guid_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_guid_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_guid_Z = (Guid)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Email_guid_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Email_guid_Z = (Guid)(System.Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Email_guid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Titulo_Z" )]
      [  XmlElement( ElementName = "Email_Titulo_Z"   )]
      public String gxTpr_Email_titulo_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_titulo_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_titulo_Z = (String)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Email_titulo_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Email_titulo_Z = "";
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Email_titulo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Titulo_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Titulo_Z"   )]
      public String gxTpr_Email_instancia_titulo_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z = (String)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z = "";
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_AreaTrabalho_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_AreaTrabalho_Z"   )]
      public int gxTpr_Email_instancia_areatrabalho_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z = (int)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z = 0;
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtEmail_Instancia_Email_titulo = "";
         gxTv_SdtEmail_Instancia_Email_corpo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_titulo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_corpo = "";
         gxTv_SdtEmail_Instancia_Email_instancia_parms = "";
         gxTv_SdtEmail_Instancia_Mode = "";
         gxTv_SdtEmail_Instancia_Email_titulo_Z = "";
         gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z = "";
         gxTv_SdtEmail_Instancia_Email_instancia_guid = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Email_guid = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Email_instancia_guid_Z = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Email_guid_Z = (Guid)(System.Guid.Empty);
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "email_instancia", "GeneXus.Programs.email_instancia_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtEmail_Instancia_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho ;
      private int gxTv_SdtEmail_Instancia_Email_instancia_areatrabalho_Z ;
      private String gxTv_SdtEmail_Instancia_Mode ;
      private String sTagName ;
      private String gxTv_SdtEmail_Instancia_Email_corpo ;
      private String gxTv_SdtEmail_Instancia_Email_instancia_corpo ;
      private String gxTv_SdtEmail_Instancia_Email_instancia_parms ;
      private String gxTv_SdtEmail_Instancia_Email_titulo ;
      private String gxTv_SdtEmail_Instancia_Email_instancia_titulo ;
      private String gxTv_SdtEmail_Instancia_Email_titulo_Z ;
      private String gxTv_SdtEmail_Instancia_Email_instancia_titulo_Z ;
      private Guid gxTv_SdtEmail_Instancia_Email_instancia_guid ;
      private Guid gxTv_SdtEmail_Instancia_Email_guid ;
      private Guid gxTv_SdtEmail_Instancia_Email_instancia_guid_Z ;
      private Guid gxTv_SdtEmail_Instancia_Email_guid_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Email_Instancia", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtEmail_Instancia_RESTInterface : GxGenericCollectionItem<SdtEmail_Instancia>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmail_Instancia_RESTInterface( ) : base()
      {
      }

      public SdtEmail_Instancia_RESTInterface( SdtEmail_Instancia psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Email_Instancia_Guid" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Email_instancia_guid
      {
         get {
            return sdt.gxTpr_Email_instancia_guid ;
         }

         set {
            sdt.gxTpr_Email_instancia_guid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "Email_Guid" , Order = 1 )]
      [GxSeudo()]
      public Guid gxTpr_Email_guid
      {
         get {
            return sdt.gxTpr_Email_guid ;
         }

         set {
            sdt.gxTpr_Email_guid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "Email_Titulo" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Email_titulo
      {
         get {
            return sdt.gxTpr_Email_titulo ;
         }

         set {
            sdt.gxTpr_Email_titulo = (String)(value);
         }

      }

      [DataMember( Name = "Email_Corpo" , Order = 3 )]
      public String gxTpr_Email_corpo
      {
         get {
            return sdt.gxTpr_Email_corpo ;
         }

         set {
            sdt.gxTpr_Email_corpo = (String)(value);
         }

      }

      [DataMember( Name = "Email_Instancia_Titulo" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Email_instancia_titulo
      {
         get {
            return sdt.gxTpr_Email_instancia_titulo ;
         }

         set {
            sdt.gxTpr_Email_instancia_titulo = (String)(value);
         }

      }

      [DataMember( Name = "Email_Instancia_Corpo" , Order = 5 )]
      public String gxTpr_Email_instancia_corpo
      {
         get {
            return sdt.gxTpr_Email_instancia_corpo ;
         }

         set {
            sdt.gxTpr_Email_instancia_corpo = (String)(value);
         }

      }

      [DataMember( Name = "Email_Instancia_AreaTrabalho" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Email_instancia_areatrabalho
      {
         get {
            return sdt.gxTpr_Email_instancia_areatrabalho ;
         }

         set {
            sdt.gxTpr_Email_instancia_areatrabalho = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Email_Instancia_Parms" , Order = 7 )]
      public String gxTpr_Email_instancia_parms
      {
         get {
            return sdt.gxTpr_Email_instancia_parms ;
         }

         set {
            sdt.gxTpr_Email_instancia_parms = (String)(value);
         }

      }

      public SdtEmail_Instancia sdt
      {
         get {
            return (SdtEmail_Instancia)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtEmail_Instancia() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
