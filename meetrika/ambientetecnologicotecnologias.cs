/*
               File: AmbienteTecnologicoTecnologias
        Description: Ambiente Tecnologico Tecnologias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:6.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ambientetecnologicotecnologias : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A351AmbienteTecnologico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A131Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A131Tecnologia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynAmbienteTecnologico_Codigo.Name = "AMBIENTETECNOLOGICO_CODIGO";
         dynAmbienteTecnologico_Codigo.WebTags = "";
         dynAmbienteTecnologico_Codigo.removeAllItems();
         /* Using cursor T001K6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynAmbienteTecnologico_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T001K6_A351AmbienteTecnologico_Codigo[0]), 6, 0)), T001K6_A352AmbienteTecnologico_Descricao[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynAmbienteTecnologico_Codigo.ItemCount > 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         dynTecnologia_Codigo.Name = "TECNOLOGIA_CODIGO";
         dynTecnologia_Codigo.WebTags = "";
         dynTecnologia_Codigo.removeAllItems();
         /* Using cursor T001K7 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            dynTecnologia_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T001K7_A131Tecnologia_Codigo[0]), 6, 0)), T001K7_A132Tecnologia_Nome[0], 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( dynTecnologia_Codigo.ItemCount > 0 )
         {
            A131Tecnologia_Codigo = (int)(NumberUtil.Val( dynTecnologia_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
         chkAmbienteTecnologicoTecnologias_Ativo.Name = "AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO";
         chkAmbienteTecnologicoTecnologias_Ativo.WebTags = "";
         chkAmbienteTecnologicoTecnologias_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologicoTecnologias_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologicoTecnologias_Ativo.Caption);
         chkAmbienteTecnologicoTecnologias_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Ambiente Tecnologico Tecnologias", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public ambientetecnologicotecnologias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public ambientetecnologicotecnologias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynAmbienteTecnologico_Codigo = new GXCombobox();
         dynTecnologia_Codigo = new GXCombobox();
         chkAmbienteTecnologicoTecnologias_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynAmbienteTecnologico_Codigo.ItemCount > 0 )
         {
            A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
         if ( dynTecnologia_Codigo.ItemCount > 0 )
         {
            A131Tecnologia_Codigo = (int)(NumberUtil.Val( dynTecnologia_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1K57( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1K57e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1K57( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1K57( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1K57e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Ambiente Tecnologico Tecnologias", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_AmbienteTecnologicoTecnologias.htm");
            wb_table3_28_1K57( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1K57e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1K57e( true) ;
         }
         else
         {
            wb_table1_2_1K57e( false) ;
         }
      }

      protected void wb_table3_28_1K57( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1K57( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1K57e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoTecnologias.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoTecnologias.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1K57e( true) ;
         }
         else
         {
            wb_table3_28_1K57e( false) ;
         }
      }

      protected void wb_table4_34_1K57( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_codigo_Internalname, "Ambiente Tecnol�gico", "", "", lblTextblockambientetecnologico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAmbienteTecnologico_Codigo, dynAmbienteTecnologico_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)), 1, dynAmbienteTecnologico_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAmbienteTecnologico_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_AmbienteTecnologicoTecnologias.htm");
            dynAmbienteTecnologico_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Values", (String)(dynAmbienteTecnologico_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblTextblockambientetecnologico_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")), ((edtAmbienteTecnologico_AreaTrabalhoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtAmbienteTecnologico_AreaTrabalhoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktecnologia_codigo_Internalname, "Tecnologia", "", "", lblTextblocktecnologia_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTecnologia_Codigo, dynTecnologia_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)), 1, dynTecnologia_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTecnologia_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_AmbienteTecnologicoTecnologias.htm");
            dynTecnologia_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTecnologia_Codigo_Internalname, "Values", (String)(dynTecnologia_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologicotecnologias_ativo_Internalname, "Tecnologico Tecnologias_Ativo", "", "", lblTextblockambientetecnologicotecnologias_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoTecnologias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAmbienteTecnologicoTecnologias_Ativo_Internalname, StringUtil.BoolToStr( A354AmbienteTecnologicoTecnologias_Ativo), "", "", 1, chkAmbienteTecnologicoTecnologias_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(54, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1K57e( true) ;
         }
         else
         {
            wb_table4_34_1K57e( false) ;
         }
      }

      protected void wb_table2_5_1K57( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoTecnologias.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1K57e( true) ;
         }
         else
         {
            wb_table2_5_1K57e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynAmbienteTecnologico_Codigo.CurrentValue = cgiGet( dynAmbienteTecnologico_Codigo_Internalname);
               A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( cgiGet( dynAmbienteTecnologico_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               A728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_AreaTrabalhoCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
               dynTecnologia_Codigo.CurrentValue = cgiGet( dynTecnologia_Codigo_Internalname);
               A131Tecnologia_Codigo = (int)(NumberUtil.Val( cgiGet( dynTecnologia_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               A354AmbienteTecnologicoTecnologias_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologicoTecnologias_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
               /* Read saved values. */
               Z351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z351AmbienteTecnologico_Codigo"), ",", "."));
               Z131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z131Tecnologia_Codigo"), ",", "."));
               Z354AmbienteTecnologicoTecnologias_Ativo = StringUtil.StrToBool( cgiGet( "Z354AmbienteTecnologicoTecnologias_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A352AmbienteTecnologico_Descricao = cgiGet( "AMBIENTETECNOLOGICO_DESCRICAO");
               A132Tecnologia_Nome = cgiGet( "TECNOLOGIA_NOME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                  A131Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1K57( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1K57( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1K0( )
      {
      }

      protected void ZM1K57( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z354AmbienteTecnologicoTecnologias_Ativo = T001K3_A354AmbienteTecnologicoTecnologias_Ativo[0];
            }
            else
            {
               Z354AmbienteTecnologicoTecnologias_Ativo = A354AmbienteTecnologicoTecnologias_Ativo;
            }
         }
         if ( GX_JID == -2 )
         {
            Z354AmbienteTecnologicoTecnologias_Ativo = A354AmbienteTecnologicoTecnologias_Ativo;
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z728AmbienteTecnologico_AreaTrabalhoCod = A728AmbienteTecnologico_AreaTrabalhoCod;
            Z132Tecnologia_Nome = A132Tecnologia_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A354AmbienteTecnologicoTecnologias_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A354AmbienteTecnologicoTecnologias_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1K57( )
      {
         /* Using cursor T001K8 */
         pr_default.execute(6, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound57 = 1;
            A352AmbienteTecnologico_Descricao = T001K8_A352AmbienteTecnologico_Descricao[0];
            A132Tecnologia_Nome = T001K8_A132Tecnologia_Nome[0];
            A354AmbienteTecnologicoTecnologias_Ativo = T001K8_A354AmbienteTecnologicoTecnologias_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
            A728AmbienteTecnologico_AreaTrabalhoCod = T001K8_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            ZM1K57( -2) ;
         }
         pr_default.close(6);
         OnLoadActions1K57( ) ;
      }

      protected void OnLoadActions1K57( )
      {
      }

      protected void CheckExtendedTable1K57( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T001K4 */
         pr_default.execute(2, new Object[] {A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A352AmbienteTecnologico_Descricao = T001K4_A352AmbienteTecnologico_Descricao[0];
         A728AmbienteTecnologico_AreaTrabalhoCod = T001K4_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         pr_default.close(2);
         /* Using cursor T001K5 */
         pr_default.execute(3, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A132Tecnologia_Nome = T001K5_A132Tecnologia_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1K57( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A351AmbienteTecnologico_Codigo )
      {
         /* Using cursor T001K9 */
         pr_default.execute(7, new Object[] {A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A352AmbienteTecnologico_Descricao = T001K9_A352AmbienteTecnologico_Descricao[0];
         A728AmbienteTecnologico_AreaTrabalhoCod = T001K9_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A352AmbienteTecnologico_Descricao)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_4( int A131Tecnologia_Codigo )
      {
         /* Using cursor T001K10 */
         pr_default.execute(8, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A132Tecnologia_Nome = T001K10_A132Tecnologia_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A132Tecnologia_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey1K57( )
      {
         /* Using cursor T001K11 */
         pr_default.execute(9, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound57 = 1;
         }
         else
         {
            RcdFound57 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001K3 */
         pr_default.execute(1, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1K57( 2) ;
            RcdFound57 = 1;
            A354AmbienteTecnologicoTecnologias_Ativo = T001K3_A354AmbienteTecnologicoTecnologias_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
            A351AmbienteTecnologico_Codigo = T001K3_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            A131Tecnologia_Codigo = T001K3_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z131Tecnologia_Codigo = A131Tecnologia_Codigo;
            sMode57 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1K57( ) ;
            if ( AnyError == 1 )
            {
               RcdFound57 = 0;
               InitializeNonKey1K57( ) ;
            }
            Gx_mode = sMode57;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound57 = 0;
            InitializeNonKey1K57( ) ;
            sMode57 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode57;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound57 = 0;
         /* Using cursor T001K12 */
         pr_default.execute(10, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001K12_A351AmbienteTecnologico_Codigo[0] < A351AmbienteTecnologico_Codigo ) || ( T001K12_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( T001K12_A131Tecnologia_Codigo[0] < A131Tecnologia_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001K12_A351AmbienteTecnologico_Codigo[0] > A351AmbienteTecnologico_Codigo ) || ( T001K12_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( T001K12_A131Tecnologia_Codigo[0] > A131Tecnologia_Codigo ) ) )
            {
               A351AmbienteTecnologico_Codigo = T001K12_A351AmbienteTecnologico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               A131Tecnologia_Codigo = T001K12_A131Tecnologia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               RcdFound57 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound57 = 0;
         /* Using cursor T001K13 */
         pr_default.execute(11, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T001K13_A351AmbienteTecnologico_Codigo[0] > A351AmbienteTecnologico_Codigo ) || ( T001K13_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( T001K13_A131Tecnologia_Codigo[0] > A131Tecnologia_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T001K13_A351AmbienteTecnologico_Codigo[0] < A351AmbienteTecnologico_Codigo ) || ( T001K13_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( T001K13_A131Tecnologia_Codigo[0] < A131Tecnologia_Codigo ) ) )
            {
               A351AmbienteTecnologico_Codigo = T001K13_A351AmbienteTecnologico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               A131Tecnologia_Codigo = T001K13_A131Tecnologia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
               RcdFound57 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1K57( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1K57( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound57 == 1 )
            {
               if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
               {
                  A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                  A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1K57( ) ;
                  GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1K57( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AMBIENTETECNOLOGICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1K57( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) || ( A131Tecnologia_Codigo != Z131Tecnologia_Codigo ) )
         {
            A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            A131Tecnologia_Codigo = Z131Tecnologia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1K57( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1K57( ) ;
         if ( RcdFound57 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound57 != 0 )
            {
               ScanNext1K57( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1K57( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1K57( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001K2 */
            pr_default.execute(0, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z354AmbienteTecnologicoTecnologias_Ativo != T001K2_A354AmbienteTecnologicoTecnologias_Ativo[0] ) )
            {
               if ( Z354AmbienteTecnologicoTecnologias_Ativo != T001K2_A354AmbienteTecnologicoTecnologias_Ativo[0] )
               {
                  GXUtil.WriteLog("ambientetecnologicotecnologias:[seudo value changed for attri]"+"AmbienteTecnologicoTecnologias_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z354AmbienteTecnologicoTecnologias_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T001K2_A354AmbienteTecnologicoTecnologias_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1K57( )
      {
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1K57( 0) ;
            CheckOptimisticConcurrency1K57( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1K57( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1K57( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001K14 */
                     pr_default.execute(12, new Object[] {A354AmbienteTecnologicoTecnologias_Ativo, A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1K0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1K57( ) ;
            }
            EndLevel1K57( ) ;
         }
         CloseExtendedTableCursors1K57( ) ;
      }

      protected void Update1K57( )
      {
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1K57( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1K57( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1K57( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001K15 */
                     pr_default.execute(13, new Object[] {A354AmbienteTecnologicoTecnologias_Ativo, A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologicoTecnologias"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1K57( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1K0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1K57( ) ;
         }
         CloseExtendedTableCursors1K57( ) ;
      }

      protected void DeferredUpdate1K57( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1K57( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1K57( ) ;
            AfterConfirm1K57( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1K57( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001K16 */
                  pr_default.execute(14, new Object[] {A351AmbienteTecnologico_Codigo, A131Tecnologia_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologicoTecnologias") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound57 == 0 )
                        {
                           InitAll1K57( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1K0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode57 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1K57( ) ;
         Gx_mode = sMode57;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1K57( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001K17 */
            pr_default.execute(15, new Object[] {A351AmbienteTecnologico_Codigo});
            A352AmbienteTecnologico_Descricao = T001K17_A352AmbienteTecnologico_Descricao[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = T001K17_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            pr_default.close(15);
            /* Using cursor T001K18 */
            pr_default.execute(16, new Object[] {A131Tecnologia_Codigo});
            A132Tecnologia_Nome = T001K18_A132Tecnologia_Nome[0];
            pr_default.close(16);
         }
      }

      protected void EndLevel1K57( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1K57( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            context.CommitDataStores( "AmbienteTecnologicoTecnologias");
            if ( AnyError == 0 )
            {
               ConfirmValues1K0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            context.RollbackDataStores( "AmbienteTecnologicoTecnologias");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1K57( )
      {
         /* Using cursor T001K19 */
         pr_default.execute(17);
         RcdFound57 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound57 = 1;
            A351AmbienteTecnologico_Codigo = T001K19_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            A131Tecnologia_Codigo = T001K19_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1K57( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound57 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound57 = 1;
            A351AmbienteTecnologico_Codigo = T001K19_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            A131Tecnologia_Codigo = T001K19_A131Tecnologia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1K57( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm1K57( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1K57( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1K57( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1K57( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1K57( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1K57( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1K57( )
      {
         dynAmbienteTecnologico_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAmbienteTecnologico_Codigo.Enabled), 5, 0)));
         edtAmbienteTecnologico_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_AreaTrabalhoCod_Enabled), 5, 0)));
         dynTecnologia_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTecnologia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTecnologia_Codigo.Enabled), 5, 0)));
         chkAmbienteTecnologicoTecnologias_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologicoTecnologias_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAmbienteTecnologicoTecnologias_Ativo.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1K0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311720828");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("ambientetecnologicotecnologias.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z354AmbienteTecnologicoTecnologias_Ativo", Z354AmbienteTecnologicoTecnologias_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_DESCRICAO", A352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_NOME", StringUtil.RTrim( A132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("ambientetecnologicotecnologias.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "AmbienteTecnologicoTecnologias" ;
      }

      public override String GetPgmdesc( )
      {
         return "Ambiente Tecnologico Tecnologias" ;
      }

      protected void InitializeNonKey1K57( )
      {
         A352AmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
         A728AmbienteTecnologico_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         A132Tecnologia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A132Tecnologia_Nome", A132Tecnologia_Nome);
         A354AmbienteTecnologicoTecnologias_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
         Z354AmbienteTecnologicoTecnologias_Ativo = false;
      }

      protected void InitAll1K57( )
      {
         A351AmbienteTecnologico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         A131Tecnologia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A131Tecnologia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A131Tecnologia_Codigo), 6, 0)));
         InitializeNonKey1K57( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A354AmbienteTecnologicoTecnologias_Ativo = i354AmbienteTecnologicoTecnologias_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A354AmbienteTecnologicoTecnologias_Ativo", A354AmbienteTecnologicoTecnologias_Ativo);
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311720833");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("ambientetecnologicotecnologias.js", "?2020311720833");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockambientetecnologico_codigo_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_CODIGO";
         dynAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO";
         lblTextblockambientetecnologico_areatrabalhocod_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         edtAmbienteTecnologico_AreaTrabalhoCod_Internalname = "AMBIENTETECNOLOGICO_AREATRABALHOCOD";
         lblTextblocktecnologia_codigo_Internalname = "TEXTBLOCKTECNOLOGIA_CODIGO";
         dynTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO";
         lblTextblockambientetecnologicotecnologias_ativo_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO";
         chkAmbienteTecnologicoTecnologias_Ativo_Internalname = "AMBIENTETECNOLOGICOTECNOLOGIAS_ATIVO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Ambiente Tecnologico Tecnologias";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         chkAmbienteTecnologicoTecnologias_Ativo.Enabled = 1;
         dynTecnologia_Codigo_Jsonclick = "";
         dynTecnologia_Codigo.Enabled = 1;
         edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick = "";
         edtAmbienteTecnologico_AreaTrabalhoCod_Enabled = 0;
         dynAmbienteTecnologico_Codigo_Jsonclick = "";
         dynAmbienteTecnologico_Codigo.Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkAmbienteTecnologicoTecnologias_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATECNOLOGIA_CODIGO1K1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATECNOLOGIA_CODIGO_data1K1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATECNOLOGIA_CODIGO_html1K1( )
      {
         int gxdynajaxvalue ;
         GXDLATECNOLOGIA_CODIGO_data1K1( ) ;
         gxdynajaxindex = 1;
         dynTecnologia_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTecnologia_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATECNOLOGIA_CODIGO_data1K1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T001K20 */
         pr_default.execute(18);
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001K20_A131Tecnologia_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001K20_A132Tecnologia_Nome[0]));
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      protected void GXDLAAMBIENTETECNOLOGICO_CODIGO1K1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAAMBIENTETECNOLOGICO_CODIGO_data1K1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAAMBIENTETECNOLOGICO_CODIGO_html1K1( )
      {
         int gxdynajaxvalue ;
         GXDLAAMBIENTETECNOLOGICO_CODIGO_data1K1( ) ;
         gxdynajaxindex = 1;
         dynAmbienteTecnologico_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAmbienteTecnologico_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAAMBIENTETECNOLOGICO_CODIGO_data1K1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T001K21 */
         pr_default.execute(19);
         while ( (pr_default.getStatus(19) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001K21_A351AmbienteTecnologico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T001K21_A352AmbienteTecnologico_Descricao[0]);
            pr_default.readNext(19);
         }
         pr_default.close(19);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001K17 */
         pr_default.execute(15, new Object[] {A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A352AmbienteTecnologico_Descricao = T001K17_A352AmbienteTecnologico_Descricao[0];
         A728AmbienteTecnologico_AreaTrabalhoCod = T001K17_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         pr_default.close(15);
         /* Using cursor T001K18 */
         pr_default.execute(16, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A132Tecnologia_Nome = T001K18_A132Tecnologia_Nome[0];
         pr_default.close(16);
         GX_FocusControl = chkAmbienteTecnologicoTecnologias_Ativo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Ambientetecnologico_codigo( GXCombobox dynGX_Parm1 ,
                                                    String GX_Parm2 ,
                                                    int GX_Parm3 )
      {
         dynAmbienteTecnologico_Codigo = dynGX_Parm1;
         A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.CurrentValue, "."));
         A352AmbienteTecnologico_Descricao = GX_Parm2;
         A728AmbienteTecnologico_AreaTrabalhoCod = GX_Parm3;
         /* Using cursor T001K17 */
         pr_default.execute(15, new Object[] {A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T62'.", "ForeignKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynAmbienteTecnologico_Codigo_Internalname;
         }
         A352AmbienteTecnologico_Descricao = T001K17_A352AmbienteTecnologico_Descricao[0];
         A728AmbienteTecnologico_AreaTrabalhoCod = T001K17_A728AmbienteTecnologico_AreaTrabalhoCod[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A352AmbienteTecnologico_Descricao = "";
            A728AmbienteTecnologico_AreaTrabalhoCod = 0;
         }
         isValidOutput.Add(A352AmbienteTecnologico_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tecnologia_codigo( GXCombobox dynGX_Parm1 ,
                                           GXCombobox dynGX_Parm2 ,
                                           bool GX_Parm3 ,
                                           String GX_Parm4 )
      {
         dynAmbienteTecnologico_Codigo = dynGX_Parm1;
         A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( dynAmbienteTecnologico_Codigo.CurrentValue, "."));
         dynTecnologia_Codigo = dynGX_Parm2;
         A131Tecnologia_Codigo = (int)(NumberUtil.Val( dynTecnologia_Codigo.CurrentValue, "."));
         A354AmbienteTecnologicoTecnologias_Ativo = GX_Parm3;
         A132Tecnologia_Nome = GX_Parm4;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T001K18 */
         pr_default.execute(16, new Object[] {A131Tecnologia_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tecnologia'.", "ForeignKeyNotFound", 1, "TECNOLOGIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynTecnologia_Codigo_Internalname;
         }
         A132Tecnologia_Nome = T001K18_A132Tecnologia_Nome[0];
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A352AmbienteTecnologico_Descricao = "";
            A728AmbienteTecnologico_AreaTrabalhoCod = 0;
            A132Tecnologia_Nome = "";
         }
         isValidOutput.Add(A354AmbienteTecnologicoTecnologias_Ativo);
         isValidOutput.Add(A352AmbienteTecnologico_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A132Tecnologia_Nome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z131Tecnologia_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z354AmbienteTecnologicoTecnologias_Ativo);
         isValidOutput.Add(Z352AmbienteTecnologico_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z132Tecnologia_Nome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T001K6_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K6_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001K7_A131Tecnologia_Codigo = new int[1] ;
         T001K7_A132Tecnologia_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockambientetecnologico_codigo_Jsonclick = "";
         lblTextblockambientetecnologico_areatrabalhocod_Jsonclick = "";
         lblTextblocktecnologia_codigo_Jsonclick = "";
         lblTextblockambientetecnologicotecnologias_ativo_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         A352AmbienteTecnologico_Descricao = "";
         A132Tecnologia_Nome = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z352AmbienteTecnologico_Descricao = "";
         Z132Tecnologia_Nome = "";
         T001K8_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001K8_A132Tecnologia_Nome = new String[] {""} ;
         T001K8_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         T001K8_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K8_A131Tecnologia_Codigo = new int[1] ;
         T001K8_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001K4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001K4_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001K5_A132Tecnologia_Nome = new String[] {""} ;
         T001K9_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001K9_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001K10_A132Tecnologia_Nome = new String[] {""} ;
         T001K11_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K11_A131Tecnologia_Codigo = new int[1] ;
         T001K3_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         T001K3_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K3_A131Tecnologia_Codigo = new int[1] ;
         sMode57 = "";
         T001K12_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K12_A131Tecnologia_Codigo = new int[1] ;
         T001K13_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K13_A131Tecnologia_Codigo = new int[1] ;
         T001K2_A354AmbienteTecnologicoTecnologias_Ativo = new bool[] {false} ;
         T001K2_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K2_A131Tecnologia_Codigo = new int[1] ;
         T001K17_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001K17_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001K18_A132Tecnologia_Nome = new String[] {""} ;
         T001K19_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K19_A131Tecnologia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001K20_A131Tecnologia_Codigo = new int[1] ;
         T001K20_A132Tecnologia_Nome = new String[] {""} ;
         T001K21_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001K21_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ambientetecnologicotecnologias__default(),
            new Object[][] {
                new Object[] {
               T001K2_A354AmbienteTecnologicoTecnologias_Ativo, T001K2_A351AmbienteTecnologico_Codigo, T001K2_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001K3_A354AmbienteTecnologicoTecnologias_Ativo, T001K3_A351AmbienteTecnologico_Codigo, T001K3_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001K4_A352AmbienteTecnologico_Descricao, T001K4_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001K5_A132Tecnologia_Nome
               }
               , new Object[] {
               T001K6_A351AmbienteTecnologico_Codigo, T001K6_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               T001K7_A131Tecnologia_Codigo, T001K7_A132Tecnologia_Nome
               }
               , new Object[] {
               T001K8_A352AmbienteTecnologico_Descricao, T001K8_A132Tecnologia_Nome, T001K8_A354AmbienteTecnologicoTecnologias_Ativo, T001K8_A351AmbienteTecnologico_Codigo, T001K8_A131Tecnologia_Codigo, T001K8_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001K9_A352AmbienteTecnologico_Descricao, T001K9_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001K10_A132Tecnologia_Nome
               }
               , new Object[] {
               T001K11_A351AmbienteTecnologico_Codigo, T001K11_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001K12_A351AmbienteTecnologico_Codigo, T001K12_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001K13_A351AmbienteTecnologico_Codigo, T001K13_A131Tecnologia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001K17_A352AmbienteTecnologico_Descricao, T001K17_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001K18_A132Tecnologia_Nome
               }
               , new Object[] {
               T001K19_A351AmbienteTecnologico_Codigo, T001K19_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001K20_A131Tecnologia_Codigo, T001K20_A132Tecnologia_Nome
               }
               , new Object[] {
               T001K21_A351AmbienteTecnologico_Codigo, T001K21_A352AmbienteTecnologico_Descricao
               }
            }
         );
         Z354AmbienteTecnologicoTecnologias_Ativo = true;
         A354AmbienteTecnologicoTecnologias_Ativo = true;
         i354AmbienteTecnologicoTecnologias_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound57 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z351AmbienteTecnologico_Codigo ;
      private int Z131Tecnologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A131Tecnologia_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int edtAmbienteTecnologico_AreaTrabalhoCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z728AmbienteTecnologico_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkAmbienteTecnologicoTecnologias_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynAmbienteTecnologico_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockambientetecnologico_codigo_Internalname ;
      private String lblTextblockambientetecnologico_codigo_Jsonclick ;
      private String dynAmbienteTecnologico_Codigo_Jsonclick ;
      private String lblTextblockambientetecnologico_areatrabalhocod_Internalname ;
      private String lblTextblockambientetecnologico_areatrabalhocod_Jsonclick ;
      private String edtAmbienteTecnologico_AreaTrabalhoCod_Internalname ;
      private String edtAmbienteTecnologico_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblocktecnologia_codigo_Internalname ;
      private String lblTextblocktecnologia_codigo_Jsonclick ;
      private String dynTecnologia_Codigo_Internalname ;
      private String dynTecnologia_Codigo_Jsonclick ;
      private String lblTextblockambientetecnologicotecnologias_ativo_Internalname ;
      private String lblTextblockambientetecnologicotecnologias_ativo_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String A132Tecnologia_Nome ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z132Tecnologia_Nome ;
      private String sMode57 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z354AmbienteTecnologicoTecnologias_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A354AmbienteTecnologicoTecnologias_Ativo ;
      private bool i354AmbienteTecnologicoTecnologias_Ativo ;
      private String A352AmbienteTecnologico_Descricao ;
      private String Z352AmbienteTecnologico_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T001K6_A351AmbienteTecnologico_Codigo ;
      private String[] T001K6_A352AmbienteTecnologico_Descricao ;
      private int[] T001K7_A131Tecnologia_Codigo ;
      private String[] T001K7_A132Tecnologia_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynAmbienteTecnologico_Codigo ;
      private GXCombobox dynTecnologia_Codigo ;
      private GXCheckbox chkAmbienteTecnologicoTecnologias_Ativo ;
      private String[] T001K8_A352AmbienteTecnologico_Descricao ;
      private String[] T001K8_A132Tecnologia_Nome ;
      private bool[] T001K8_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] T001K8_A351AmbienteTecnologico_Codigo ;
      private int[] T001K8_A131Tecnologia_Codigo ;
      private int[] T001K8_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] T001K4_A352AmbienteTecnologico_Descricao ;
      private int[] T001K4_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] T001K5_A132Tecnologia_Nome ;
      private String[] T001K9_A352AmbienteTecnologico_Descricao ;
      private int[] T001K9_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] T001K10_A132Tecnologia_Nome ;
      private int[] T001K11_A351AmbienteTecnologico_Codigo ;
      private int[] T001K11_A131Tecnologia_Codigo ;
      private bool[] T001K3_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] T001K3_A351AmbienteTecnologico_Codigo ;
      private int[] T001K3_A131Tecnologia_Codigo ;
      private int[] T001K12_A351AmbienteTecnologico_Codigo ;
      private int[] T001K12_A131Tecnologia_Codigo ;
      private int[] T001K13_A351AmbienteTecnologico_Codigo ;
      private int[] T001K13_A131Tecnologia_Codigo ;
      private bool[] T001K2_A354AmbienteTecnologicoTecnologias_Ativo ;
      private int[] T001K2_A351AmbienteTecnologico_Codigo ;
      private int[] T001K2_A131Tecnologia_Codigo ;
      private String[] T001K17_A352AmbienteTecnologico_Descricao ;
      private int[] T001K17_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] T001K18_A132Tecnologia_Nome ;
      private int[] T001K19_A351AmbienteTecnologico_Codigo ;
      private int[] T001K19_A131Tecnologia_Codigo ;
      private int[] T001K20_A131Tecnologia_Codigo ;
      private String[] T001K20_A132Tecnologia_Nome ;
      private int[] T001K21_A351AmbienteTecnologico_Codigo ;
      private String[] T001K21_A352AmbienteTecnologico_Descricao ;
      private GXWebForm Form ;
   }

   public class ambientetecnologicotecnologias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001K6 ;
          prmT001K6 = new Object[] {
          } ;
          Object[] prmT001K7 ;
          prmT001K7 = new Object[] {
          } ;
          Object[] prmT001K8 ;
          prmT001K8 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K4 ;
          prmT001K4 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K5 ;
          prmT001K5 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K9 ;
          prmT001K9 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K10 ;
          prmT001K10 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K11 ;
          prmT001K11 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K3 ;
          prmT001K3 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K12 ;
          prmT001K12 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K13 ;
          prmT001K13 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K2 ;
          prmT001K2 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K14 ;
          prmT001K14 = new Object[] {
          new Object[] {"@AmbienteTecnologicoTecnologias_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K15 ;
          prmT001K15 = new Object[] {
          new Object[] {"@AmbienteTecnologicoTecnologias_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K16 ;
          prmT001K16 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K19 ;
          prmT001K19 = new Object[] {
          } ;
          Object[] prmT001K20 ;
          prmT001K20 = new Object[] {
          } ;
          Object[] prmT001K21 ;
          prmT001K21 = new Object[] {
          } ;
          Object[] prmT001K17 ;
          prmT001K17 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001K18 ;
          prmT001K18 = new Object[] {
          new Object[] {"@Tecnologia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001K2", "SELECT [AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (UPDLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K2,1,0,true,false )
             ,new CursorDef("T001K3", "SELECT [AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K3,1,0,true,false )
             ,new CursorDef("T001K4", "SELECT [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K4,1,0,true,false )
             ,new CursorDef("T001K5", "SELECT [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K5,1,0,true,false )
             ,new CursorDef("T001K6", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) ORDER BY [AmbienteTecnologico_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K6,0,0,true,false )
             ,new CursorDef("T001K7", "SELECT [Tecnologia_Codigo], [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) ORDER BY [Tecnologia_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K7,0,0,true,false )
             ,new CursorDef("T001K8", "SELECT T2.[AmbienteTecnologico_Descricao], T3.[Tecnologia_Nome], TM1.[AmbienteTecnologicoTecnologias_Ativo], TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo], T2.[AmbienteTecnologico_AreaTrabalhoCod] FROM (([AmbienteTecnologicoTecnologias] TM1 WITH (NOLOCK) INNER JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = TM1.[AmbienteTecnologico_Codigo]) INNER JOIN [Tecnologia] T3 WITH (NOLOCK) ON T3.[Tecnologia_Codigo] = TM1.[Tecnologia_Codigo]) WHERE TM1.[AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo and TM1.[Tecnologia_Codigo] = @Tecnologia_Codigo ORDER BY TM1.[AmbienteTecnologico_Codigo], TM1.[Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001K8,100,0,true,false )
             ,new CursorDef("T001K9", "SELECT [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K9,1,0,true,false )
             ,new CursorDef("T001K10", "SELECT [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K10,1,0,true,false )
             ,new CursorDef("T001K11", "SELECT [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001K11,1,0,true,false )
             ,new CursorDef("T001K12", "SELECT TOP 1 [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE ( [AmbienteTecnologico_Codigo] > @AmbienteTecnologico_Codigo or [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo and [Tecnologia_Codigo] > @Tecnologia_Codigo) ORDER BY [AmbienteTecnologico_Codigo], [Tecnologia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001K12,1,0,true,true )
             ,new CursorDef("T001K13", "SELECT TOP 1 [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE ( [AmbienteTecnologico_Codigo] < @AmbienteTecnologico_Codigo or [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo and [Tecnologia_Codigo] < @Tecnologia_Codigo) ORDER BY [AmbienteTecnologico_Codigo] DESC, [Tecnologia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001K13,1,0,true,true )
             ,new CursorDef("T001K14", "INSERT INTO [AmbienteTecnologicoTecnologias]([AmbienteTecnologicoTecnologias_Ativo], [AmbienteTecnologico_Codigo], [Tecnologia_Codigo]) VALUES(@AmbienteTecnologicoTecnologias_Ativo, @AmbienteTecnologico_Codigo, @Tecnologia_Codigo)", GxErrorMask.GX_NOMASK,prmT001K14)
             ,new CursorDef("T001K15", "UPDATE [AmbienteTecnologicoTecnologias] SET [AmbienteTecnologicoTecnologias_Ativo]=@AmbienteTecnologicoTecnologias_Ativo  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmT001K15)
             ,new CursorDef("T001K16", "DELETE FROM [AmbienteTecnologicoTecnologias]  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo AND [Tecnologia_Codigo] = @Tecnologia_Codigo", GxErrorMask.GX_NOMASK,prmT001K16)
             ,new CursorDef("T001K17", "SELECT [AmbienteTecnologico_Descricao], [AmbienteTecnologico_AreaTrabalhoCod] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K17,1,0,true,false )
             ,new CursorDef("T001K18", "SELECT [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) WHERE [Tecnologia_Codigo] = @Tecnologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K18,1,0,true,false )
             ,new CursorDef("T001K19", "SELECT [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) ORDER BY [AmbienteTecnologico_Codigo], [Tecnologia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001K19,100,0,true,false )
             ,new CursorDef("T001K20", "SELECT [Tecnologia_Codigo], [Tecnologia_Nome] FROM [Tecnologia] WITH (NOLOCK) ORDER BY [Tecnologia_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K20,0,0,true,false )
             ,new CursorDef("T001K21", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) ORDER BY [AmbienteTecnologico_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001K21,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
