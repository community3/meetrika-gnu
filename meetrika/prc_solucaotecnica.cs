/*
               File: PRC_SolucaoTecnica
        Description: Solucao Tecnica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:27.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_solucaotecnica : GXProcedure
   {
      public prc_solucaotecnica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_solucaotecnica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ProjetoMelhoria_FnCod ,
                           ref int aP1_ProjetoMelhoria_ProjetoCod ,
                           String aP2_Tipo ,
                           out String aP3_ProjetoMelhoria_Observacao )
      {
         this.AV11ProjetoMelhoria_FnCod = aP0_ProjetoMelhoria_FnCod;
         this.A694ProjetoMelhoria_ProjetoCod = aP1_ProjetoMelhoria_ProjetoCod;
         this.AV9Tipo = aP2_Tipo;
         this.AV8ProjetoMelhoria_Observacao = "" ;
         initialize();
         executePrivate();
         aP0_ProjetoMelhoria_FnCod=this.AV11ProjetoMelhoria_FnCod;
         aP1_ProjetoMelhoria_ProjetoCod=this.A694ProjetoMelhoria_ProjetoCod;
         aP3_ProjetoMelhoria_Observacao=this.AV8ProjetoMelhoria_Observacao;
      }

      public String executeUdp( ref int aP0_ProjetoMelhoria_FnCod ,
                                ref int aP1_ProjetoMelhoria_ProjetoCod ,
                                String aP2_Tipo )
      {
         this.AV11ProjetoMelhoria_FnCod = aP0_ProjetoMelhoria_FnCod;
         this.A694ProjetoMelhoria_ProjetoCod = aP1_ProjetoMelhoria_ProjetoCod;
         this.AV9Tipo = aP2_Tipo;
         this.AV8ProjetoMelhoria_Observacao = "" ;
         initialize();
         executePrivate();
         aP0_ProjetoMelhoria_FnCod=this.AV11ProjetoMelhoria_FnCod;
         aP1_ProjetoMelhoria_ProjetoCod=this.A694ProjetoMelhoria_ProjetoCod;
         aP3_ProjetoMelhoria_Observacao=this.AV8ProjetoMelhoria_Observacao;
         return AV8ProjetoMelhoria_Observacao ;
      }

      public void executeSubmit( ref int aP0_ProjetoMelhoria_FnCod ,
                                 ref int aP1_ProjetoMelhoria_ProjetoCod ,
                                 String aP2_Tipo ,
                                 out String aP3_ProjetoMelhoria_Observacao )
      {
         prc_solucaotecnica objprc_solucaotecnica;
         objprc_solucaotecnica = new prc_solucaotecnica();
         objprc_solucaotecnica.AV11ProjetoMelhoria_FnCod = aP0_ProjetoMelhoria_FnCod;
         objprc_solucaotecnica.A694ProjetoMelhoria_ProjetoCod = aP1_ProjetoMelhoria_ProjetoCod;
         objprc_solucaotecnica.AV9Tipo = aP2_Tipo;
         objprc_solucaotecnica.AV8ProjetoMelhoria_Observacao = "" ;
         objprc_solucaotecnica.context.SetSubmitInitialConfig(context);
         objprc_solucaotecnica.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_solucaotecnica);
         aP0_ProjetoMelhoria_FnCod=this.AV11ProjetoMelhoria_FnCod;
         aP1_ProjetoMelhoria_ProjetoCod=this.A694ProjetoMelhoria_ProjetoCod;
         aP3_ProjetoMelhoria_Observacao=this.AV8ProjetoMelhoria_Observacao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_solucaotecnica)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9Tipo ,
                                              A697ProjetoMelhoria_FnDadosCod ,
                                              AV11ProjetoMelhoria_FnCod ,
                                              A698ProjetoMelhoria_FnAPFCod ,
                                              A694ProjetoMelhoria_ProjetoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor P005G2 */
         pr_default.execute(0, new Object[] {A694ProjetoMelhoria_ProjetoCod, AV11ProjetoMelhoria_FnCod, AV11ProjetoMelhoria_FnCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A698ProjetoMelhoria_FnAPFCod = P005G2_A698ProjetoMelhoria_FnAPFCod[0];
            n698ProjetoMelhoria_FnAPFCod = P005G2_n698ProjetoMelhoria_FnAPFCod[0];
            A697ProjetoMelhoria_FnDadosCod = P005G2_A697ProjetoMelhoria_FnDadosCod[0];
            n697ProjetoMelhoria_FnDadosCod = P005G2_n697ProjetoMelhoria_FnDadosCod[0];
            A704ProjetoMelhoria_Observacao = P005G2_A704ProjetoMelhoria_Observacao[0];
            n704ProjetoMelhoria_Observacao = P005G2_n704ProjetoMelhoria_Observacao[0];
            A736ProjetoMelhoria_Codigo = P005G2_A736ProjetoMelhoria_Codigo[0];
            AV8ProjetoMelhoria_Observacao = A704ProjetoMelhoria_Observacao;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005G2_A694ProjetoMelhoria_ProjetoCod = new int[1] ;
         P005G2_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         P005G2_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         P005G2_A697ProjetoMelhoria_FnDadosCod = new int[1] ;
         P005G2_n697ProjetoMelhoria_FnDadosCod = new bool[] {false} ;
         P005G2_A704ProjetoMelhoria_Observacao = new String[] {""} ;
         P005G2_n704ProjetoMelhoria_Observacao = new bool[] {false} ;
         P005G2_A736ProjetoMelhoria_Codigo = new int[1] ;
         A704ProjetoMelhoria_Observacao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_solucaotecnica__default(),
            new Object[][] {
                new Object[] {
               P005G2_A694ProjetoMelhoria_ProjetoCod, P005G2_A698ProjetoMelhoria_FnAPFCod, P005G2_n698ProjetoMelhoria_FnAPFCod, P005G2_A697ProjetoMelhoria_FnDadosCod, P005G2_n697ProjetoMelhoria_FnDadosCod, P005G2_A704ProjetoMelhoria_Observacao, P005G2_n704ProjetoMelhoria_Observacao, P005G2_A736ProjetoMelhoria_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11ProjetoMelhoria_FnCod ;
      private int A694ProjetoMelhoria_ProjetoCod ;
      private int A697ProjetoMelhoria_FnDadosCod ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int A736ProjetoMelhoria_Codigo ;
      private String AV9Tipo ;
      private String scmdbuf ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private bool n697ProjetoMelhoria_FnDadosCod ;
      private bool n704ProjetoMelhoria_Observacao ;
      private String AV8ProjetoMelhoria_Observacao ;
      private String A704ProjetoMelhoria_Observacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ProjetoMelhoria_FnCod ;
      private int aP1_ProjetoMelhoria_ProjetoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005G2_A694ProjetoMelhoria_ProjetoCod ;
      private int[] P005G2_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] P005G2_n698ProjetoMelhoria_FnAPFCod ;
      private int[] P005G2_A697ProjetoMelhoria_FnDadosCod ;
      private bool[] P005G2_n697ProjetoMelhoria_FnDadosCod ;
      private String[] P005G2_A704ProjetoMelhoria_Observacao ;
      private bool[] P005G2_n704ProjetoMelhoria_Observacao ;
      private int[] P005G2_A736ProjetoMelhoria_Codigo ;
      private String aP3_ProjetoMelhoria_Observacao ;
   }

   public class prc_solucaotecnica__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P005G2( IGxContext context ,
                                             String AV9Tipo ,
                                             int A697ProjetoMelhoria_FnDadosCod ,
                                             int AV11ProjetoMelhoria_FnCod ,
                                             int A698ProjetoMelhoria_FnAPFCod ,
                                             int A694ProjetoMelhoria_ProjetoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ProjetoMelhoria_ProjetoCod], [ProjetoMelhoria_FnAPFCod], [ProjetoMelhoria_FnDadosCod], [ProjetoMelhoria_Observacao], [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ProjetoMelhoria_ProjetoCod] = @ProjetoMelhoria_ProjetoCod)";
         if ( StringUtil.StrCmp(AV9Tipo, "D") == 0 )
         {
            sWhereString = sWhereString + " and ([ProjetoMelhoria_FnDadosCod] = @AV11ProjetoMelhoria_FnCod)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( StringUtil.StrCmp(AV9Tipo, "T") == 0 )
         {
            sWhereString = sWhereString + " and ([ProjetoMelhoria_FnAPFCod] = @AV11ProjetoMelhoria_FnCod)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ProjetoMelhoria_ProjetoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P005G2(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005G2 ;
          prmP005G2 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ProjetoMelhoria_FnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ProjetoMelhoria_FnCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005G2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
    }

 }

}
