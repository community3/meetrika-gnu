/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:21:29.53
*/
gx.evt.autoSkip = false;
gx.define('contratoservicostelasgeneral', true, function (CmpContext) {
   this.ServerClass =  "contratoservicostelasgeneral" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.A938ContratoServicosTelas_Sequencial=gx.fn.getIntegerValue("CONTRATOSERVICOSTELAS_SEQUENCIAL",'.') ;
   };
   this.Valid_Contratoservicostelas_contratocod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATOSERVICOSTELAS_CONTRATOCOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e13g92_client=function()
   {
      this.executeServerEvent("'DOUPDATE'", false, null, false, false);
   };
   this.e14g92_client=function()
   {
      this.executeServerEvent("'DODELETE'", false, null, false, false);
   };
   this.e15g92_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e16g92_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,21,23,26,28,31,33,36,42];
   this.GXLastCtrlId =42;
   GXValidFnc[2]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[8]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[11]={fld:"TEXTBLOCKCONTRATADA_PESSOANOM", format:0,grid:0};
   GXValidFnc[13]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOANOM",gxz:"Z41Contratada_PessoaNom",gxold:"O41Contratada_PessoaNom",gxvar:"A41Contratada_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A41Contratada_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z41Contratada_PessoaNom=Value},v2c:function(){gx.fn.setControlValue("CONTRATADA_PESSOANOM",gx.O.A41Contratada_PessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A41Contratada_PessoaNom=this.val()},val:function(){return gx.fn.getControlValue("CONTRATADA_PESSOANOM")},nac:gx.falseFn};
   this.declareDomainHdlr( 13 , function() {
   });
   GXValidFnc[16]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_TELA", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_TELA",gxz:"Z931ContratoServicosTelas_Tela",gxold:"O931ContratoServicosTelas_Tela",gxvar:"A931ContratoServicosTelas_Tela",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A931ContratoServicosTelas_Tela=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z931ContratoServicosTelas_Tela=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_TELA",gx.O.A931ContratoServicosTelas_Tela,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A931ContratoServicosTelas_Tela=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_TELA")},nac:gx.falseFn};
   this.declareDomainHdlr( 18 , function() {
   });
   GXValidFnc[21]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_LINK", format:0,grid:0};
   GXValidFnc[23]={lvl:0,type:"svchar",len:80,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_LINK",gxz:"Z928ContratoServicosTelas_Link",gxold:"O928ContratoServicosTelas_Link",gxvar:"A928ContratoServicosTelas_Link",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A928ContratoServicosTelas_Link=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z928ContratoServicosTelas_Link=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_LINK",gx.O.A928ContratoServicosTelas_Link,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A928ContratoServicosTelas_Link=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_LINK")},nac:gx.falseFn};
   this.declareDomainHdlr( 23 , function() {
   });
   GXValidFnc[26]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_PARMS", format:0,grid:0};
   GXValidFnc[28]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_PARMS",gxz:"Z929ContratoServicosTelas_Parms",gxold:"O929ContratoServicosTelas_Parms",gxvar:"A929ContratoServicosTelas_Parms",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A929ContratoServicosTelas_Parms=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z929ContratoServicosTelas_Parms=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_PARMS",gx.O.A929ContratoServicosTelas_Parms,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A929ContratoServicosTelas_Parms=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_PARMS")},nac:gx.falseFn};
   this.declareDomainHdlr( 28 , function() {
   });
   GXValidFnc[31]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_STATUS", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_STATUS",gxz:"Z932ContratoServicosTelas_Status",gxold:"O932ContratoServicosTelas_Status",gxvar:"A932ContratoServicosTelas_Status",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A932ContratoServicosTelas_Status=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z932ContratoServicosTelas_Status=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSERVICOSTELAS_STATUS",gx.O.A932ContratoServicosTelas_Status);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A932ContratoServicosTelas_Status=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_STATUS")},nac:gx.falseFn};
   this.declareDomainHdlr( 33 , function() {
   });
   GXValidFnc[36]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[42]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contratoservicostelas_contratocod,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_CONTRATOCOD",gxz:"Z926ContratoServicosTelas_ContratoCod",gxold:"O926ContratoServicosTelas_ContratoCod",gxvar:"A926ContratoServicosTelas_ContratoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A926ContratoServicosTelas_ContratoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z926ContratoServicosTelas_ContratoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_CONTRATOCOD",gx.O.A926ContratoServicosTelas_ContratoCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A926ContratoServicosTelas_ContratoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSERVICOSTELAS_CONTRATOCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 42 , function() {
   });
   this.A41Contratada_PessoaNom = "" ;
   this.Z41Contratada_PessoaNom = "" ;
   this.O41Contratada_PessoaNom = "" ;
   this.A931ContratoServicosTelas_Tela = "" ;
   this.Z931ContratoServicosTelas_Tela = "" ;
   this.O931ContratoServicosTelas_Tela = "" ;
   this.A928ContratoServicosTelas_Link = "" ;
   this.Z928ContratoServicosTelas_Link = "" ;
   this.O928ContratoServicosTelas_Link = "" ;
   this.A929ContratoServicosTelas_Parms = "" ;
   this.Z929ContratoServicosTelas_Parms = "" ;
   this.O929ContratoServicosTelas_Parms = "" ;
   this.A932ContratoServicosTelas_Status = "" ;
   this.Z932ContratoServicosTelas_Status = "" ;
   this.O932ContratoServicosTelas_Status = "" ;
   this.A926ContratoServicosTelas_ContratoCod = 0 ;
   this.Z926ContratoServicosTelas_ContratoCod = 0 ;
   this.O926ContratoServicosTelas_ContratoCod = 0 ;
   this.A41Contratada_PessoaNom = "" ;
   this.A931ContratoServicosTelas_Tela = "" ;
   this.A928ContratoServicosTelas_Link = "" ;
   this.A929ContratoServicosTelas_Parms = "" ;
   this.A932ContratoServicosTelas_Status = "" ;
   this.A926ContratoServicosTelas_ContratoCod = 0 ;
   this.A938ContratoServicosTelas_Sequencial = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.A40Contratada_PessoaCod = 0 ;
   this.Events = {"e13g92_client": ["'DOUPDATE'", true] ,"e14g92_client": ["'DODELETE'", true] ,"e15g92_client": ["ENTER", true] ,"e16g92_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["LOAD"] = [[],[{av:'gx.fn.getCtrlProperty("CONTRATOSERVICOSTELAS_CONTRATOCOD","Visible")',ctrl:'CONTRATOSERVICOSTELAS_CONTRATOCOD',prop:'Visible'}]];
   this.EvtParms["'DOUPDATE'"] = [[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],[]];
   this.EvtParms["'DODELETE'"] = [[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],[]];
   this.setVCMap("A938ContratoServicosTelas_Sequencial", "CONTRATOSERVICOSTELAS_SEQUENCIAL", 0, "int");
   this.InitStandaloneVars( );
});
