/*
               File: GetWWContratadaFilterData
        Description: Get WWContratada Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:20.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratadafilterdata : GXProcedure
   {
      public getwwcontratadafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratadafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
         return AV36OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratadafilterdata objgetwwcontratadafilterdata;
         objgetwwcontratadafilterdata = new getwwcontratadafilterdata();
         objgetwwcontratadafilterdata.AV27DDOName = aP0_DDOName;
         objgetwwcontratadafilterdata.AV25SearchTxt = aP1_SearchTxt;
         objgetwwcontratadafilterdata.AV26SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratadafilterdata.AV31OptionsJson = "" ;
         objgetwwcontratadafilterdata.AV34OptionsDescJson = "" ;
         objgetwwcontratadafilterdata.AV36OptionIndexesJson = "" ;
         objgetwwcontratadafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratadafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratadafilterdata);
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratadafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV30Options = (IGxCollection)(new GxSimpleCollection());
         AV33OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV35OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_PESSOA_IE") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_IEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_PESSOA_TELEFONE") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_TELEFONEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV31OptionsJson = AV30Options.ToJSonString(false);
         AV34OptionsDescJson = AV33OptionsDesc.ToJSonString(false);
         AV36OptionIndexesJson = AV35OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get("WWContratadaGridState"), "") == 0 )
         {
            AV40GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratadaGridState"), "");
         }
         else
         {
            AV40GridState.FromXml(AV38Session.Get("WWContratadaGridState"), "");
         }
         AV54GXV1 = 1;
         while ( AV54GXV1 <= AV40GridState.gxTpr_Filtervalues.Count )
         {
            AV41GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV40GridState.gxTpr_Filtervalues.Item(AV54GXV1));
            if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV43Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "CONTRATADA_CODIGO") == 0 )
            {
               AV44Contratada_Codigo = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV12TFContratada_PessoaCNPJ = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV13TFContratada_PessoaCNPJ_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE") == 0 )
            {
               AV14TFPessoa_IE = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE_SEL") == 0 )
            {
               AV15TFPessoa_IE_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE") == 0 )
            {
               AV16TFPessoa_Telefone = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE_SEL") == 0 )
            {
               AV17TFPessoa_Telefone_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_SS") == 0 )
            {
               AV18TFContratada_SS = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratada_SS_To = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_OS") == 0 )
            {
               AV20TFContratada_OS = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContratada_OS_To = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_LOTE") == 0 )
            {
               AV22TFContratada_Lote = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV23TFContratada_Lote_To = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_ATIVO_SEL") == 0 )
            {
               AV24TFContratada_Ativo_Sel = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            AV54GXV1 = (int)(AV54GXV1+1);
         }
         if ( AV40GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV42GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV40GridState.gxTpr_Dynamicfilters.Item(1));
            AV45DynamicFiltersSelector1 = AV42GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV46Contratada_PessoaNom1 = AV42GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "CONTRATADA_PESSOACNPJ") == 0 )
            {
               AV47Contratada_PessoaCNPJ1 = AV42GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV40GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV48DynamicFiltersEnabled2 = true;
               AV42GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV40GridState.gxTpr_Dynamicfilters.Item(2));
               AV49DynamicFiltersSelector2 = AV42GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV50Contratada_PessoaNom2 = AV42GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOACNPJ") == 0 )
               {
                  AV51Contratada_PessoaCNPJ2 = AV42GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV25SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         AV56WWContratadaDS_1_Contratada_areatrabalhocod = AV43Contratada_AreaTrabalhoCod;
         AV57WWContratadaDS_2_Contratada_codigo = AV44Contratada_Codigo;
         AV58WWContratadaDS_3_Dynamicfiltersselector1 = AV45DynamicFiltersSelector1;
         AV59WWContratadaDS_4_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV60WWContratadaDS_5_Contratada_pessoacnpj1 = AV47Contratada_PessoaCNPJ1;
         AV61WWContratadaDS_6_Dynamicfiltersenabled2 = AV48DynamicFiltersEnabled2;
         AV62WWContratadaDS_7_Dynamicfiltersselector2 = AV49DynamicFiltersSelector2;
         AV63WWContratadaDS_8_Contratada_pessoanom2 = AV50Contratada_PessoaNom2;
         AV64WWContratadaDS_9_Contratada_pessoacnpj2 = AV51Contratada_PessoaCNPJ2;
         AV65WWContratadaDS_10_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV67WWContratadaDS_12_Tfcontratada_pessoacnpj = AV12TFContratada_PessoaCNPJ;
         AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV13TFContratada_PessoaCNPJ_Sel;
         AV69WWContratadaDS_14_Tfpessoa_ie = AV14TFPessoa_IE;
         AV70WWContratadaDS_15_Tfpessoa_ie_sel = AV15TFPessoa_IE_Sel;
         AV71WWContratadaDS_16_Tfpessoa_telefone = AV16TFPessoa_Telefone;
         AV72WWContratadaDS_17_Tfpessoa_telefone_sel = AV17TFPessoa_Telefone_Sel;
         AV73WWContratadaDS_18_Tfcontratada_ss = AV18TFContratada_SS;
         AV74WWContratadaDS_19_Tfcontratada_ss_to = AV19TFContratada_SS_To;
         AV75WWContratadaDS_20_Tfcontratada_os = AV20TFContratada_OS;
         AV76WWContratadaDS_21_Tfcontratada_os_to = AV21TFContratada_OS_To;
         AV77WWContratadaDS_22_Tfcontratada_lote = AV22TFContratada_Lote;
         AV78WWContratadaDS_23_Tfcontratada_lote_to = AV23TFContratada_Lote_To;
         AV79WWContratadaDS_24_Tfcontratada_ativo_sel = AV24TFContratada_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                              AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                              AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                              AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                              AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                              AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                              AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                              AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                              AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                              AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                              AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                              AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                              AV69WWContratadaDS_14_Tfpessoa_ie ,
                                              AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                              AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                              AV73WWContratadaDS_18_Tfcontratada_ss ,
                                              AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                              AV75WWContratadaDS_20_Tfcontratada_os ,
                                              AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                              AV77WWContratadaDS_22_Tfcontratada_lote ,
                                              AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                              AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                              A39Contratada_Codigo ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A518Pessoa_IE ,
                                              A522Pessoa_Telefone ,
                                              A1451Contratada_SS ,
                                              A524Contratada_OS ,
                                              A530Contratada_Lote ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV59WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
         lV60WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
         lV63WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
         lV64WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
         lV65WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
         lV67WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
         lV69WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie), 15, "%");
         lV71WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
         /* Using cursor P00FA2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, lV59WWContratadaDS_4_Contratada_pessoanom1, lV60WWContratadaDS_5_Contratada_pessoacnpj1, lV63WWContratadaDS_8_Contratada_pessoanom2, lV64WWContratadaDS_9_Contratada_pessoacnpj2, lV65WWContratadaDS_10_Tfcontratada_pessoanom, AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV67WWContratadaDS_12_Tfcontratada_pessoacnpj, AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV69WWContratadaDS_14_Tfpessoa_ie, AV70WWContratadaDS_15_Tfpessoa_ie_sel, lV71WWContratadaDS_16_Tfpessoa_telefone, AV72WWContratadaDS_17_Tfpessoa_telefone_sel, AV73WWContratadaDS_18_Tfcontratada_ss, AV74WWContratadaDS_19_Tfcontratada_ss_to, AV75WWContratadaDS_20_Tfcontratada_os, AV76WWContratadaDS_21_Tfcontratada_os_to, AV77WWContratadaDS_22_Tfcontratada_lote, AV78WWContratadaDS_23_Tfcontratada_lote_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFA2 = false;
            A40Contratada_PessoaCod = P00FA2_A40Contratada_PessoaCod[0];
            A43Contratada_Ativo = P00FA2_A43Contratada_Ativo[0];
            A530Contratada_Lote = P00FA2_A530Contratada_Lote[0];
            n530Contratada_Lote = P00FA2_n530Contratada_Lote[0];
            A524Contratada_OS = P00FA2_A524Contratada_OS[0];
            n524Contratada_OS = P00FA2_n524Contratada_OS[0];
            A1451Contratada_SS = P00FA2_A1451Contratada_SS[0];
            n1451Contratada_SS = P00FA2_n1451Contratada_SS[0];
            A522Pessoa_Telefone = P00FA2_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA2_n522Pessoa_Telefone[0];
            A518Pessoa_IE = P00FA2_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA2_n518Pessoa_IE[0];
            A42Contratada_PessoaCNPJ = P00FA2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA2_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = P00FA2_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00FA2_A52Contratada_AreaTrabalhoCod[0];
            A522Pessoa_Telefone = P00FA2_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA2_n522Pessoa_Telefone[0];
            A518Pessoa_IE = P00FA2_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA2_n518Pessoa_IE[0];
            A42Contratada_PessoaCNPJ = P00FA2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA2_n41Contratada_PessoaNom[0];
            AV37count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00FA2_A40Contratada_PessoaCod[0] == A40Contratada_PessoaCod ) )
            {
               BRKFA2 = false;
               A39Contratada_Codigo = P00FA2_A39Contratada_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKFA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV29Option = A41Contratada_PessoaNom;
               AV28InsertIndex = 1;
               while ( ( AV28InsertIndex <= AV30Options.Count ) && ( StringUtil.StrCmp(((String)AV30Options.Item(AV28InsertIndex)), AV29Option) < 0 ) )
               {
                  AV28InsertIndex = (int)(AV28InsertIndex+1);
               }
               AV30Options.Add(AV29Option, AV28InsertIndex);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), AV28InsertIndex);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFA2 )
            {
               BRKFA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV12TFContratada_PessoaCNPJ = AV25SearchTxt;
         AV13TFContratada_PessoaCNPJ_Sel = "";
         AV56WWContratadaDS_1_Contratada_areatrabalhocod = AV43Contratada_AreaTrabalhoCod;
         AV57WWContratadaDS_2_Contratada_codigo = AV44Contratada_Codigo;
         AV58WWContratadaDS_3_Dynamicfiltersselector1 = AV45DynamicFiltersSelector1;
         AV59WWContratadaDS_4_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV60WWContratadaDS_5_Contratada_pessoacnpj1 = AV47Contratada_PessoaCNPJ1;
         AV61WWContratadaDS_6_Dynamicfiltersenabled2 = AV48DynamicFiltersEnabled2;
         AV62WWContratadaDS_7_Dynamicfiltersselector2 = AV49DynamicFiltersSelector2;
         AV63WWContratadaDS_8_Contratada_pessoanom2 = AV50Contratada_PessoaNom2;
         AV64WWContratadaDS_9_Contratada_pessoacnpj2 = AV51Contratada_PessoaCNPJ2;
         AV65WWContratadaDS_10_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV67WWContratadaDS_12_Tfcontratada_pessoacnpj = AV12TFContratada_PessoaCNPJ;
         AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV13TFContratada_PessoaCNPJ_Sel;
         AV69WWContratadaDS_14_Tfpessoa_ie = AV14TFPessoa_IE;
         AV70WWContratadaDS_15_Tfpessoa_ie_sel = AV15TFPessoa_IE_Sel;
         AV71WWContratadaDS_16_Tfpessoa_telefone = AV16TFPessoa_Telefone;
         AV72WWContratadaDS_17_Tfpessoa_telefone_sel = AV17TFPessoa_Telefone_Sel;
         AV73WWContratadaDS_18_Tfcontratada_ss = AV18TFContratada_SS;
         AV74WWContratadaDS_19_Tfcontratada_ss_to = AV19TFContratada_SS_To;
         AV75WWContratadaDS_20_Tfcontratada_os = AV20TFContratada_OS;
         AV76WWContratadaDS_21_Tfcontratada_os_to = AV21TFContratada_OS_To;
         AV77WWContratadaDS_22_Tfcontratada_lote = AV22TFContratada_Lote;
         AV78WWContratadaDS_23_Tfcontratada_lote_to = AV23TFContratada_Lote_To;
         AV79WWContratadaDS_24_Tfcontratada_ativo_sel = AV24TFContratada_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                              AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                              AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                              AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                              AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                              AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                              AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                              AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                              AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                              AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                              AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                              AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                              AV69WWContratadaDS_14_Tfpessoa_ie ,
                                              AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                              AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                              AV73WWContratadaDS_18_Tfcontratada_ss ,
                                              AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                              AV75WWContratadaDS_20_Tfcontratada_os ,
                                              AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                              AV77WWContratadaDS_22_Tfcontratada_lote ,
                                              AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                              AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                              A39Contratada_Codigo ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A518Pessoa_IE ,
                                              A522Pessoa_Telefone ,
                                              A1451Contratada_SS ,
                                              A524Contratada_OS ,
                                              A530Contratada_Lote ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV59WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
         lV60WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
         lV63WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
         lV64WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
         lV65WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
         lV67WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
         lV69WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie), 15, "%");
         lV71WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
         /* Using cursor P00FA3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, lV59WWContratadaDS_4_Contratada_pessoanom1, lV60WWContratadaDS_5_Contratada_pessoacnpj1, lV63WWContratadaDS_8_Contratada_pessoanom2, lV64WWContratadaDS_9_Contratada_pessoacnpj2, lV65WWContratadaDS_10_Tfcontratada_pessoanom, AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV67WWContratadaDS_12_Tfcontratada_pessoacnpj, AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV69WWContratadaDS_14_Tfpessoa_ie, AV70WWContratadaDS_15_Tfpessoa_ie_sel, lV71WWContratadaDS_16_Tfpessoa_telefone, AV72WWContratadaDS_17_Tfpessoa_telefone_sel, AV73WWContratadaDS_18_Tfcontratada_ss, AV74WWContratadaDS_19_Tfcontratada_ss_to, AV75WWContratadaDS_20_Tfcontratada_os, AV76WWContratadaDS_21_Tfcontratada_os_to, AV77WWContratadaDS_22_Tfcontratada_lote, AV78WWContratadaDS_23_Tfcontratada_lote_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFA4 = false;
            A40Contratada_PessoaCod = P00FA3_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00FA3_A52Contratada_AreaTrabalhoCod[0];
            A42Contratada_PessoaCNPJ = P00FA3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA3_n42Contratada_PessoaCNPJ[0];
            A43Contratada_Ativo = P00FA3_A43Contratada_Ativo[0];
            A530Contratada_Lote = P00FA3_A530Contratada_Lote[0];
            n530Contratada_Lote = P00FA3_n530Contratada_Lote[0];
            A524Contratada_OS = P00FA3_A524Contratada_OS[0];
            n524Contratada_OS = P00FA3_n524Contratada_OS[0];
            A1451Contratada_SS = P00FA3_A1451Contratada_SS[0];
            n1451Contratada_SS = P00FA3_n1451Contratada_SS[0];
            A522Pessoa_Telefone = P00FA3_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA3_n522Pessoa_Telefone[0];
            A518Pessoa_IE = P00FA3_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA3_n518Pessoa_IE[0];
            A41Contratada_PessoaNom = P00FA3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA3_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = P00FA3_A39Contratada_Codigo[0];
            A42Contratada_PessoaCNPJ = P00FA3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA3_n42Contratada_PessoaCNPJ[0];
            A522Pessoa_Telefone = P00FA3_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA3_n522Pessoa_Telefone[0];
            A518Pessoa_IE = P00FA3_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA3_n518Pessoa_IE[0];
            A41Contratada_PessoaNom = P00FA3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA3_n41Contratada_PessoaNom[0];
            AV37count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00FA3_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKFA4 = false;
               A40Contratada_PessoaCod = P00FA3_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00FA3_A39Contratada_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKFA4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV29Option = A42Contratada_PessoaCNPJ;
               AV30Options.Add(AV29Option, 0);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFA4 )
            {
               BRKFA4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPESSOA_IEOPTIONS' Routine */
         AV14TFPessoa_IE = AV25SearchTxt;
         AV15TFPessoa_IE_Sel = "";
         AV56WWContratadaDS_1_Contratada_areatrabalhocod = AV43Contratada_AreaTrabalhoCod;
         AV57WWContratadaDS_2_Contratada_codigo = AV44Contratada_Codigo;
         AV58WWContratadaDS_3_Dynamicfiltersselector1 = AV45DynamicFiltersSelector1;
         AV59WWContratadaDS_4_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV60WWContratadaDS_5_Contratada_pessoacnpj1 = AV47Contratada_PessoaCNPJ1;
         AV61WWContratadaDS_6_Dynamicfiltersenabled2 = AV48DynamicFiltersEnabled2;
         AV62WWContratadaDS_7_Dynamicfiltersselector2 = AV49DynamicFiltersSelector2;
         AV63WWContratadaDS_8_Contratada_pessoanom2 = AV50Contratada_PessoaNom2;
         AV64WWContratadaDS_9_Contratada_pessoacnpj2 = AV51Contratada_PessoaCNPJ2;
         AV65WWContratadaDS_10_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV67WWContratadaDS_12_Tfcontratada_pessoacnpj = AV12TFContratada_PessoaCNPJ;
         AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV13TFContratada_PessoaCNPJ_Sel;
         AV69WWContratadaDS_14_Tfpessoa_ie = AV14TFPessoa_IE;
         AV70WWContratadaDS_15_Tfpessoa_ie_sel = AV15TFPessoa_IE_Sel;
         AV71WWContratadaDS_16_Tfpessoa_telefone = AV16TFPessoa_Telefone;
         AV72WWContratadaDS_17_Tfpessoa_telefone_sel = AV17TFPessoa_Telefone_Sel;
         AV73WWContratadaDS_18_Tfcontratada_ss = AV18TFContratada_SS;
         AV74WWContratadaDS_19_Tfcontratada_ss_to = AV19TFContratada_SS_To;
         AV75WWContratadaDS_20_Tfcontratada_os = AV20TFContratada_OS;
         AV76WWContratadaDS_21_Tfcontratada_os_to = AV21TFContratada_OS_To;
         AV77WWContratadaDS_22_Tfcontratada_lote = AV22TFContratada_Lote;
         AV78WWContratadaDS_23_Tfcontratada_lote_to = AV23TFContratada_Lote_To;
         AV79WWContratadaDS_24_Tfcontratada_ativo_sel = AV24TFContratada_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                              AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                              AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                              AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                              AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                              AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                              AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                              AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                              AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                              AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                              AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                              AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                              AV69WWContratadaDS_14_Tfpessoa_ie ,
                                              AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                              AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                              AV73WWContratadaDS_18_Tfcontratada_ss ,
                                              AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                              AV75WWContratadaDS_20_Tfcontratada_os ,
                                              AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                              AV77WWContratadaDS_22_Tfcontratada_lote ,
                                              AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                              AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                              A39Contratada_Codigo ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A518Pessoa_IE ,
                                              A522Pessoa_Telefone ,
                                              A1451Contratada_SS ,
                                              A524Contratada_OS ,
                                              A530Contratada_Lote ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV59WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
         lV60WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
         lV63WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
         lV64WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
         lV65WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
         lV67WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
         lV69WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie), 15, "%");
         lV71WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
         /* Using cursor P00FA4 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, lV59WWContratadaDS_4_Contratada_pessoanom1, lV60WWContratadaDS_5_Contratada_pessoacnpj1, lV63WWContratadaDS_8_Contratada_pessoanom2, lV64WWContratadaDS_9_Contratada_pessoacnpj2, lV65WWContratadaDS_10_Tfcontratada_pessoanom, AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV67WWContratadaDS_12_Tfcontratada_pessoacnpj, AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV69WWContratadaDS_14_Tfpessoa_ie, AV70WWContratadaDS_15_Tfpessoa_ie_sel, lV71WWContratadaDS_16_Tfpessoa_telefone, AV72WWContratadaDS_17_Tfpessoa_telefone_sel, AV73WWContratadaDS_18_Tfcontratada_ss, AV74WWContratadaDS_19_Tfcontratada_ss_to, AV75WWContratadaDS_20_Tfcontratada_os, AV76WWContratadaDS_21_Tfcontratada_os_to, AV77WWContratadaDS_22_Tfcontratada_lote, AV78WWContratadaDS_23_Tfcontratada_lote_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKFA6 = false;
            A40Contratada_PessoaCod = P00FA4_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00FA4_A52Contratada_AreaTrabalhoCod[0];
            A518Pessoa_IE = P00FA4_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA4_n518Pessoa_IE[0];
            A43Contratada_Ativo = P00FA4_A43Contratada_Ativo[0];
            A530Contratada_Lote = P00FA4_A530Contratada_Lote[0];
            n530Contratada_Lote = P00FA4_n530Contratada_Lote[0];
            A524Contratada_OS = P00FA4_A524Contratada_OS[0];
            n524Contratada_OS = P00FA4_n524Contratada_OS[0];
            A1451Contratada_SS = P00FA4_A1451Contratada_SS[0];
            n1451Contratada_SS = P00FA4_n1451Contratada_SS[0];
            A522Pessoa_Telefone = P00FA4_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA4_n522Pessoa_Telefone[0];
            A42Contratada_PessoaCNPJ = P00FA4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA4_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA4_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = P00FA4_A39Contratada_Codigo[0];
            A518Pessoa_IE = P00FA4_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA4_n518Pessoa_IE[0];
            A522Pessoa_Telefone = P00FA4_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA4_n522Pessoa_Telefone[0];
            A42Contratada_PessoaCNPJ = P00FA4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA4_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA4_n41Contratada_PessoaNom[0];
            AV37count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00FA4_A518Pessoa_IE[0], A518Pessoa_IE) == 0 ) )
            {
               BRKFA6 = false;
               A40Contratada_PessoaCod = P00FA4_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00FA4_A39Contratada_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKFA6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A518Pessoa_IE)) )
            {
               AV29Option = A518Pessoa_IE;
               AV30Options.Add(AV29Option, 0);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFA6 )
            {
               BRKFA6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADPESSOA_TELEFONEOPTIONS' Routine */
         AV16TFPessoa_Telefone = AV25SearchTxt;
         AV17TFPessoa_Telefone_Sel = "";
         AV56WWContratadaDS_1_Contratada_areatrabalhocod = AV43Contratada_AreaTrabalhoCod;
         AV57WWContratadaDS_2_Contratada_codigo = AV44Contratada_Codigo;
         AV58WWContratadaDS_3_Dynamicfiltersselector1 = AV45DynamicFiltersSelector1;
         AV59WWContratadaDS_4_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV60WWContratadaDS_5_Contratada_pessoacnpj1 = AV47Contratada_PessoaCNPJ1;
         AV61WWContratadaDS_6_Dynamicfiltersenabled2 = AV48DynamicFiltersEnabled2;
         AV62WWContratadaDS_7_Dynamicfiltersselector2 = AV49DynamicFiltersSelector2;
         AV63WWContratadaDS_8_Contratada_pessoanom2 = AV50Contratada_PessoaNom2;
         AV64WWContratadaDS_9_Contratada_pessoacnpj2 = AV51Contratada_PessoaCNPJ2;
         AV65WWContratadaDS_10_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV67WWContratadaDS_12_Tfcontratada_pessoacnpj = AV12TFContratada_PessoaCNPJ;
         AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV13TFContratada_PessoaCNPJ_Sel;
         AV69WWContratadaDS_14_Tfpessoa_ie = AV14TFPessoa_IE;
         AV70WWContratadaDS_15_Tfpessoa_ie_sel = AV15TFPessoa_IE_Sel;
         AV71WWContratadaDS_16_Tfpessoa_telefone = AV16TFPessoa_Telefone;
         AV72WWContratadaDS_17_Tfpessoa_telefone_sel = AV17TFPessoa_Telefone_Sel;
         AV73WWContratadaDS_18_Tfcontratada_ss = AV18TFContratada_SS;
         AV74WWContratadaDS_19_Tfcontratada_ss_to = AV19TFContratada_SS_To;
         AV75WWContratadaDS_20_Tfcontratada_os = AV20TFContratada_OS;
         AV76WWContratadaDS_21_Tfcontratada_os_to = AV21TFContratada_OS_To;
         AV77WWContratadaDS_22_Tfcontratada_lote = AV22TFContratada_Lote;
         AV78WWContratadaDS_23_Tfcontratada_lote_to = AV23TFContratada_Lote_To;
         AV79WWContratadaDS_24_Tfcontratada_ativo_sel = AV24TFContratada_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                              AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                              AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                              AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                              AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                              AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                              AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                              AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                              AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                              AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                              AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                              AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                              AV69WWContratadaDS_14_Tfpessoa_ie ,
                                              AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                              AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                              AV73WWContratadaDS_18_Tfcontratada_ss ,
                                              AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                              AV75WWContratadaDS_20_Tfcontratada_os ,
                                              AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                              AV77WWContratadaDS_22_Tfcontratada_lote ,
                                              AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                              AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                              A39Contratada_Codigo ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A518Pessoa_IE ,
                                              A522Pessoa_Telefone ,
                                              A1451Contratada_SS ,
                                              A524Contratada_OS ,
                                              A530Contratada_Lote ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV59WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
         lV60WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
         lV63WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
         lV64WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
         lV65WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
         lV67WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
         lV69WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie), 15, "%");
         lV71WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
         /* Using cursor P00FA5 */
         pr_default.execute(3, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, lV59WWContratadaDS_4_Contratada_pessoanom1, lV60WWContratadaDS_5_Contratada_pessoacnpj1, lV63WWContratadaDS_8_Contratada_pessoanom2, lV64WWContratadaDS_9_Contratada_pessoacnpj2, lV65WWContratadaDS_10_Tfcontratada_pessoanom, AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV67WWContratadaDS_12_Tfcontratada_pessoacnpj, AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV69WWContratadaDS_14_Tfpessoa_ie, AV70WWContratadaDS_15_Tfpessoa_ie_sel, lV71WWContratadaDS_16_Tfpessoa_telefone, AV72WWContratadaDS_17_Tfpessoa_telefone_sel, AV73WWContratadaDS_18_Tfcontratada_ss, AV74WWContratadaDS_19_Tfcontratada_ss_to, AV75WWContratadaDS_20_Tfcontratada_os, AV76WWContratadaDS_21_Tfcontratada_os_to, AV77WWContratadaDS_22_Tfcontratada_lote, AV78WWContratadaDS_23_Tfcontratada_lote_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKFA8 = false;
            A40Contratada_PessoaCod = P00FA5_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00FA5_A52Contratada_AreaTrabalhoCod[0];
            A522Pessoa_Telefone = P00FA5_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA5_n522Pessoa_Telefone[0];
            A43Contratada_Ativo = P00FA5_A43Contratada_Ativo[0];
            A530Contratada_Lote = P00FA5_A530Contratada_Lote[0];
            n530Contratada_Lote = P00FA5_n530Contratada_Lote[0];
            A524Contratada_OS = P00FA5_A524Contratada_OS[0];
            n524Contratada_OS = P00FA5_n524Contratada_OS[0];
            A1451Contratada_SS = P00FA5_A1451Contratada_SS[0];
            n1451Contratada_SS = P00FA5_n1451Contratada_SS[0];
            A518Pessoa_IE = P00FA5_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA5_n518Pessoa_IE[0];
            A42Contratada_PessoaCNPJ = P00FA5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA5_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA5_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = P00FA5_A39Contratada_Codigo[0];
            A522Pessoa_Telefone = P00FA5_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00FA5_n522Pessoa_Telefone[0];
            A518Pessoa_IE = P00FA5_A518Pessoa_IE[0];
            n518Pessoa_IE = P00FA5_n518Pessoa_IE[0];
            A42Contratada_PessoaCNPJ = P00FA5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FA5_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FA5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FA5_n41Contratada_PessoaNom[0];
            AV37count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00FA5_A522Pessoa_Telefone[0], A522Pessoa_Telefone) == 0 ) )
            {
               BRKFA8 = false;
               A40Contratada_PessoaCod = P00FA5_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00FA5_A39Contratada_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKFA8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A522Pessoa_Telefone)) )
            {
               AV29Option = A522Pessoa_Telefone;
               AV30Options.Add(AV29Option, 0);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFA8 )
            {
               BRKFA8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV30Options = new GxSimpleCollection();
         AV33OptionsDesc = new GxSimpleCollection();
         AV35OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV38Session = context.GetSession();
         AV40GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV41GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV12TFContratada_PessoaCNPJ = "";
         AV13TFContratada_PessoaCNPJ_Sel = "";
         AV14TFPessoa_IE = "";
         AV15TFPessoa_IE_Sel = "";
         AV16TFPessoa_Telefone = "";
         AV17TFPessoa_Telefone_Sel = "";
         AV42GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV45DynamicFiltersSelector1 = "";
         AV46Contratada_PessoaNom1 = "";
         AV47Contratada_PessoaCNPJ1 = "";
         AV49DynamicFiltersSelector2 = "";
         AV50Contratada_PessoaNom2 = "";
         AV51Contratada_PessoaCNPJ2 = "";
         AV58WWContratadaDS_3_Dynamicfiltersselector1 = "";
         AV59WWContratadaDS_4_Contratada_pessoanom1 = "";
         AV60WWContratadaDS_5_Contratada_pessoacnpj1 = "";
         AV62WWContratadaDS_7_Dynamicfiltersselector2 = "";
         AV63WWContratadaDS_8_Contratada_pessoanom2 = "";
         AV64WWContratadaDS_9_Contratada_pessoacnpj2 = "";
         AV65WWContratadaDS_10_Tfcontratada_pessoanom = "";
         AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel = "";
         AV67WWContratadaDS_12_Tfcontratada_pessoacnpj = "";
         AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = "";
         AV69WWContratadaDS_14_Tfpessoa_ie = "";
         AV70WWContratadaDS_15_Tfpessoa_ie_sel = "";
         AV71WWContratadaDS_16_Tfpessoa_telefone = "";
         AV72WWContratadaDS_17_Tfpessoa_telefone_sel = "";
         scmdbuf = "";
         lV59WWContratadaDS_4_Contratada_pessoanom1 = "";
         lV60WWContratadaDS_5_Contratada_pessoacnpj1 = "";
         lV63WWContratadaDS_8_Contratada_pessoanom2 = "";
         lV64WWContratadaDS_9_Contratada_pessoacnpj2 = "";
         lV65WWContratadaDS_10_Tfcontratada_pessoanom = "";
         lV67WWContratadaDS_12_Tfcontratada_pessoacnpj = "";
         lV69WWContratadaDS_14_Tfpessoa_ie = "";
         lV71WWContratadaDS_16_Tfpessoa_telefone = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A522Pessoa_Telefone = "";
         P00FA2_A40Contratada_PessoaCod = new int[1] ;
         P00FA2_A43Contratada_Ativo = new bool[] {false} ;
         P00FA2_A530Contratada_Lote = new short[1] ;
         P00FA2_n530Contratada_Lote = new bool[] {false} ;
         P00FA2_A524Contratada_OS = new int[1] ;
         P00FA2_n524Contratada_OS = new bool[] {false} ;
         P00FA2_A1451Contratada_SS = new int[1] ;
         P00FA2_n1451Contratada_SS = new bool[] {false} ;
         P00FA2_A522Pessoa_Telefone = new String[] {""} ;
         P00FA2_n522Pessoa_Telefone = new bool[] {false} ;
         P00FA2_A518Pessoa_IE = new String[] {""} ;
         P00FA2_n518Pessoa_IE = new bool[] {false} ;
         P00FA2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FA2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FA2_A41Contratada_PessoaNom = new String[] {""} ;
         P00FA2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00FA2_A39Contratada_Codigo = new int[1] ;
         P00FA2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         AV29Option = "";
         P00FA3_A40Contratada_PessoaCod = new int[1] ;
         P00FA3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00FA3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FA3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FA3_A43Contratada_Ativo = new bool[] {false} ;
         P00FA3_A530Contratada_Lote = new short[1] ;
         P00FA3_n530Contratada_Lote = new bool[] {false} ;
         P00FA3_A524Contratada_OS = new int[1] ;
         P00FA3_n524Contratada_OS = new bool[] {false} ;
         P00FA3_A1451Contratada_SS = new int[1] ;
         P00FA3_n1451Contratada_SS = new bool[] {false} ;
         P00FA3_A522Pessoa_Telefone = new String[] {""} ;
         P00FA3_n522Pessoa_Telefone = new bool[] {false} ;
         P00FA3_A518Pessoa_IE = new String[] {""} ;
         P00FA3_n518Pessoa_IE = new bool[] {false} ;
         P00FA3_A41Contratada_PessoaNom = new String[] {""} ;
         P00FA3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00FA3_A39Contratada_Codigo = new int[1] ;
         P00FA4_A40Contratada_PessoaCod = new int[1] ;
         P00FA4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00FA4_A518Pessoa_IE = new String[] {""} ;
         P00FA4_n518Pessoa_IE = new bool[] {false} ;
         P00FA4_A43Contratada_Ativo = new bool[] {false} ;
         P00FA4_A530Contratada_Lote = new short[1] ;
         P00FA4_n530Contratada_Lote = new bool[] {false} ;
         P00FA4_A524Contratada_OS = new int[1] ;
         P00FA4_n524Contratada_OS = new bool[] {false} ;
         P00FA4_A1451Contratada_SS = new int[1] ;
         P00FA4_n1451Contratada_SS = new bool[] {false} ;
         P00FA4_A522Pessoa_Telefone = new String[] {""} ;
         P00FA4_n522Pessoa_Telefone = new bool[] {false} ;
         P00FA4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FA4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FA4_A41Contratada_PessoaNom = new String[] {""} ;
         P00FA4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00FA4_A39Contratada_Codigo = new int[1] ;
         P00FA5_A40Contratada_PessoaCod = new int[1] ;
         P00FA5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00FA5_A522Pessoa_Telefone = new String[] {""} ;
         P00FA5_n522Pessoa_Telefone = new bool[] {false} ;
         P00FA5_A43Contratada_Ativo = new bool[] {false} ;
         P00FA5_A530Contratada_Lote = new short[1] ;
         P00FA5_n530Contratada_Lote = new bool[] {false} ;
         P00FA5_A524Contratada_OS = new int[1] ;
         P00FA5_n524Contratada_OS = new bool[] {false} ;
         P00FA5_A1451Contratada_SS = new int[1] ;
         P00FA5_n1451Contratada_SS = new bool[] {false} ;
         P00FA5_A518Pessoa_IE = new String[] {""} ;
         P00FA5_n518Pessoa_IE = new bool[] {false} ;
         P00FA5_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FA5_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FA5_A41Contratada_PessoaNom = new String[] {""} ;
         P00FA5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00FA5_A39Contratada_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratadafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FA2_A40Contratada_PessoaCod, P00FA2_A43Contratada_Ativo, P00FA2_A530Contratada_Lote, P00FA2_n530Contratada_Lote, P00FA2_A524Contratada_OS, P00FA2_n524Contratada_OS, P00FA2_A1451Contratada_SS, P00FA2_n1451Contratada_SS, P00FA2_A522Pessoa_Telefone, P00FA2_n522Pessoa_Telefone,
               P00FA2_A518Pessoa_IE, P00FA2_n518Pessoa_IE, P00FA2_A42Contratada_PessoaCNPJ, P00FA2_n42Contratada_PessoaCNPJ, P00FA2_A41Contratada_PessoaNom, P00FA2_n41Contratada_PessoaNom, P00FA2_A39Contratada_Codigo, P00FA2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00FA3_A40Contratada_PessoaCod, P00FA3_A52Contratada_AreaTrabalhoCod, P00FA3_A42Contratada_PessoaCNPJ, P00FA3_n42Contratada_PessoaCNPJ, P00FA3_A43Contratada_Ativo, P00FA3_A530Contratada_Lote, P00FA3_n530Contratada_Lote, P00FA3_A524Contratada_OS, P00FA3_n524Contratada_OS, P00FA3_A1451Contratada_SS,
               P00FA3_n1451Contratada_SS, P00FA3_A522Pessoa_Telefone, P00FA3_n522Pessoa_Telefone, P00FA3_A518Pessoa_IE, P00FA3_n518Pessoa_IE, P00FA3_A41Contratada_PessoaNom, P00FA3_n41Contratada_PessoaNom, P00FA3_A39Contratada_Codigo
               }
               , new Object[] {
               P00FA4_A40Contratada_PessoaCod, P00FA4_A52Contratada_AreaTrabalhoCod, P00FA4_A518Pessoa_IE, P00FA4_n518Pessoa_IE, P00FA4_A43Contratada_Ativo, P00FA4_A530Contratada_Lote, P00FA4_n530Contratada_Lote, P00FA4_A524Contratada_OS, P00FA4_n524Contratada_OS, P00FA4_A1451Contratada_SS,
               P00FA4_n1451Contratada_SS, P00FA4_A522Pessoa_Telefone, P00FA4_n522Pessoa_Telefone, P00FA4_A42Contratada_PessoaCNPJ, P00FA4_n42Contratada_PessoaCNPJ, P00FA4_A41Contratada_PessoaNom, P00FA4_n41Contratada_PessoaNom, P00FA4_A39Contratada_Codigo
               }
               , new Object[] {
               P00FA5_A40Contratada_PessoaCod, P00FA5_A52Contratada_AreaTrabalhoCod, P00FA5_A522Pessoa_Telefone, P00FA5_n522Pessoa_Telefone, P00FA5_A43Contratada_Ativo, P00FA5_A530Contratada_Lote, P00FA5_n530Contratada_Lote, P00FA5_A524Contratada_OS, P00FA5_n524Contratada_OS, P00FA5_A1451Contratada_SS,
               P00FA5_n1451Contratada_SS, P00FA5_A518Pessoa_IE, P00FA5_n518Pessoa_IE, P00FA5_A42Contratada_PessoaCNPJ, P00FA5_n42Contratada_PessoaCNPJ, P00FA5_A41Contratada_PessoaNom, P00FA5_n41Contratada_PessoaNom, P00FA5_A39Contratada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV22TFContratada_Lote ;
      private short AV23TFContratada_Lote_To ;
      private short AV24TFContratada_Ativo_Sel ;
      private short AV77WWContratadaDS_22_Tfcontratada_lote ;
      private short AV78WWContratadaDS_23_Tfcontratada_lote_to ;
      private short AV79WWContratadaDS_24_Tfcontratada_ativo_sel ;
      private short A530Contratada_Lote ;
      private int AV54GXV1 ;
      private int AV43Contratada_AreaTrabalhoCod ;
      private int AV44Contratada_Codigo ;
      private int AV18TFContratada_SS ;
      private int AV19TFContratada_SS_To ;
      private int AV20TFContratada_OS ;
      private int AV21TFContratada_OS_To ;
      private int AV56WWContratadaDS_1_Contratada_areatrabalhocod ;
      private int AV57WWContratadaDS_2_Contratada_codigo ;
      private int AV73WWContratadaDS_18_Tfcontratada_ss ;
      private int AV74WWContratadaDS_19_Tfcontratada_ss_to ;
      private int AV75WWContratadaDS_20_Tfcontratada_os ;
      private int AV76WWContratadaDS_21_Tfcontratada_os_to ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A39Contratada_Codigo ;
      private int A1451Contratada_SS ;
      private int A524Contratada_OS ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int AV28InsertIndex ;
      private long AV37count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV14TFPessoa_IE ;
      private String AV15TFPessoa_IE_Sel ;
      private String AV16TFPessoa_Telefone ;
      private String AV17TFPessoa_Telefone_Sel ;
      private String AV46Contratada_PessoaNom1 ;
      private String AV50Contratada_PessoaNom2 ;
      private String AV59WWContratadaDS_4_Contratada_pessoanom1 ;
      private String AV63WWContratadaDS_8_Contratada_pessoanom2 ;
      private String AV65WWContratadaDS_10_Tfcontratada_pessoanom ;
      private String AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ;
      private String AV69WWContratadaDS_14_Tfpessoa_ie ;
      private String AV70WWContratadaDS_15_Tfpessoa_ie_sel ;
      private String AV71WWContratadaDS_16_Tfpessoa_telefone ;
      private String AV72WWContratadaDS_17_Tfpessoa_telefone_sel ;
      private String scmdbuf ;
      private String lV59WWContratadaDS_4_Contratada_pessoanom1 ;
      private String lV63WWContratadaDS_8_Contratada_pessoanom2 ;
      private String lV65WWContratadaDS_10_Tfcontratada_pessoanom ;
      private String lV69WWContratadaDS_14_Tfpessoa_ie ;
      private String lV71WWContratadaDS_16_Tfpessoa_telefone ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A522Pessoa_Telefone ;
      private bool returnInSub ;
      private bool AV48DynamicFiltersEnabled2 ;
      private bool AV61WWContratadaDS_6_Dynamicfiltersenabled2 ;
      private bool A43Contratada_Ativo ;
      private bool BRKFA2 ;
      private bool n530Contratada_Lote ;
      private bool n524Contratada_OS ;
      private bool n1451Contratada_SS ;
      private bool n522Pessoa_Telefone ;
      private bool n518Pessoa_IE ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKFA4 ;
      private bool BRKFA6 ;
      private bool BRKFA8 ;
      private String AV36OptionIndexesJson ;
      private String AV31OptionsJson ;
      private String AV34OptionsDescJson ;
      private String AV27DDOName ;
      private String AV25SearchTxt ;
      private String AV26SearchTxtTo ;
      private String AV12TFContratada_PessoaCNPJ ;
      private String AV13TFContratada_PessoaCNPJ_Sel ;
      private String AV45DynamicFiltersSelector1 ;
      private String AV47Contratada_PessoaCNPJ1 ;
      private String AV49DynamicFiltersSelector2 ;
      private String AV51Contratada_PessoaCNPJ2 ;
      private String AV58WWContratadaDS_3_Dynamicfiltersselector1 ;
      private String AV60WWContratadaDS_5_Contratada_pessoacnpj1 ;
      private String AV62WWContratadaDS_7_Dynamicfiltersselector2 ;
      private String AV64WWContratadaDS_9_Contratada_pessoacnpj2 ;
      private String AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ;
      private String AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ;
      private String lV60WWContratadaDS_5_Contratada_pessoacnpj1 ;
      private String lV64WWContratadaDS_9_Contratada_pessoacnpj2 ;
      private String lV67WWContratadaDS_12_Tfcontratada_pessoacnpj ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV29Option ;
      private IGxSession AV38Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FA2_A40Contratada_PessoaCod ;
      private bool[] P00FA2_A43Contratada_Ativo ;
      private short[] P00FA2_A530Contratada_Lote ;
      private bool[] P00FA2_n530Contratada_Lote ;
      private int[] P00FA2_A524Contratada_OS ;
      private bool[] P00FA2_n524Contratada_OS ;
      private int[] P00FA2_A1451Contratada_SS ;
      private bool[] P00FA2_n1451Contratada_SS ;
      private String[] P00FA2_A522Pessoa_Telefone ;
      private bool[] P00FA2_n522Pessoa_Telefone ;
      private String[] P00FA2_A518Pessoa_IE ;
      private bool[] P00FA2_n518Pessoa_IE ;
      private String[] P00FA2_A42Contratada_PessoaCNPJ ;
      private bool[] P00FA2_n42Contratada_PessoaCNPJ ;
      private String[] P00FA2_A41Contratada_PessoaNom ;
      private bool[] P00FA2_n41Contratada_PessoaNom ;
      private int[] P00FA2_A39Contratada_Codigo ;
      private int[] P00FA2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00FA3_A40Contratada_PessoaCod ;
      private int[] P00FA3_A52Contratada_AreaTrabalhoCod ;
      private String[] P00FA3_A42Contratada_PessoaCNPJ ;
      private bool[] P00FA3_n42Contratada_PessoaCNPJ ;
      private bool[] P00FA3_A43Contratada_Ativo ;
      private short[] P00FA3_A530Contratada_Lote ;
      private bool[] P00FA3_n530Contratada_Lote ;
      private int[] P00FA3_A524Contratada_OS ;
      private bool[] P00FA3_n524Contratada_OS ;
      private int[] P00FA3_A1451Contratada_SS ;
      private bool[] P00FA3_n1451Contratada_SS ;
      private String[] P00FA3_A522Pessoa_Telefone ;
      private bool[] P00FA3_n522Pessoa_Telefone ;
      private String[] P00FA3_A518Pessoa_IE ;
      private bool[] P00FA3_n518Pessoa_IE ;
      private String[] P00FA3_A41Contratada_PessoaNom ;
      private bool[] P00FA3_n41Contratada_PessoaNom ;
      private int[] P00FA3_A39Contratada_Codigo ;
      private int[] P00FA4_A40Contratada_PessoaCod ;
      private int[] P00FA4_A52Contratada_AreaTrabalhoCod ;
      private String[] P00FA4_A518Pessoa_IE ;
      private bool[] P00FA4_n518Pessoa_IE ;
      private bool[] P00FA4_A43Contratada_Ativo ;
      private short[] P00FA4_A530Contratada_Lote ;
      private bool[] P00FA4_n530Contratada_Lote ;
      private int[] P00FA4_A524Contratada_OS ;
      private bool[] P00FA4_n524Contratada_OS ;
      private int[] P00FA4_A1451Contratada_SS ;
      private bool[] P00FA4_n1451Contratada_SS ;
      private String[] P00FA4_A522Pessoa_Telefone ;
      private bool[] P00FA4_n522Pessoa_Telefone ;
      private String[] P00FA4_A42Contratada_PessoaCNPJ ;
      private bool[] P00FA4_n42Contratada_PessoaCNPJ ;
      private String[] P00FA4_A41Contratada_PessoaNom ;
      private bool[] P00FA4_n41Contratada_PessoaNom ;
      private int[] P00FA4_A39Contratada_Codigo ;
      private int[] P00FA5_A40Contratada_PessoaCod ;
      private int[] P00FA5_A52Contratada_AreaTrabalhoCod ;
      private String[] P00FA5_A522Pessoa_Telefone ;
      private bool[] P00FA5_n522Pessoa_Telefone ;
      private bool[] P00FA5_A43Contratada_Ativo ;
      private short[] P00FA5_A530Contratada_Lote ;
      private bool[] P00FA5_n530Contratada_Lote ;
      private int[] P00FA5_A524Contratada_OS ;
      private bool[] P00FA5_n524Contratada_OS ;
      private int[] P00FA5_A1451Contratada_SS ;
      private bool[] P00FA5_n1451Contratada_SS ;
      private String[] P00FA5_A518Pessoa_IE ;
      private bool[] P00FA5_n518Pessoa_IE ;
      private String[] P00FA5_A42Contratada_PessoaCNPJ ;
      private bool[] P00FA5_n42Contratada_PessoaCNPJ ;
      private String[] P00FA5_A41Contratada_PessoaNom ;
      private bool[] P00FA5_n41Contratada_PessoaNom ;
      private int[] P00FA5_A39Contratada_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV40GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV41GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV42GridStateDynamicFilter ;
   }

   public class getwwcontratadafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FA2( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV69WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV73WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV75WWContratadaDS_20_Tfcontratada_os ,
                                             int AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV77WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [20] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Ativo], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T2.[Pessoa_Telefone], T2.[Pessoa_IE], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Codigo], T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV59WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV60WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV63WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV64WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV65WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV67WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV69WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV70WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV71WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV72WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV73WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV73WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV74WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV74WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV75WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV75WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV76WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV76WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV77WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV77WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV78WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV78WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contratada_PessoaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00FA3( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV69WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV73WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV75WWContratadaDS_20_Tfcontratada_os ,
                                             int AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV77WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [20] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Ativo], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T2.[Pessoa_Telefone], T2.[Pessoa_IE], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV59WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV60WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV63WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV64WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV65WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV67WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV69WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV70WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV71WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV72WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV73WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV73WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV74WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV74WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV75WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV75WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV76WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV76WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV77WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV77WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV78WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV78WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Docto]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00FA4( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV69WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV73WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV75WWContratadaDS_20_Tfcontratada_os ,
                                             int AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV77WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [20] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_IE], T1.[Contratada_Ativo], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T2.[Pessoa_Telefone], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV59WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV60WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV63WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV64WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV65WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV67WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV69WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV70WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV71WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV72WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV73WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV73WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV74WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV74WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV75WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV75WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV76WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV76WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV77WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV77WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV78WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV78WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_IE]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00FA5( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV58WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV59WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV60WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV61WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV62WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV63WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV64WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV65WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV67WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV70WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV69WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV72WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV71WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV73WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV74WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV75WWContratadaDS_20_Tfcontratada_os ,
                                             int AV76WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV77WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV78WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV79WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [20] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Telefone], T1.[Contratada_Ativo], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T2.[Pessoa_IE], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV59WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV60WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV63WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV61WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV64WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV65WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV67WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV69WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV70WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV71WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV72WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV73WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV73WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV74WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV74WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (0==AV75WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV75WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (0==AV76WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV76WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (0==AV77WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV77WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV78WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV78WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV79WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Telefone]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FA2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 1 :
                     return conditional_P00FA3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 2 :
                     return conditional_P00FA4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
               case 3 :
                     return conditional_P00FA5(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FA2 ;
          prmP00FA2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV65WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV70WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV72WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV74WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV75WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV76WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV77WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00FA3 ;
          prmP00FA3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV65WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV70WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV72WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV74WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV75WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV76WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV77WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00FA4 ;
          prmP00FA4 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV65WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV70WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV72WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV74WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV75WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV76WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV77WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00FA5 ;
          prmP00FA5 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV63WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV65WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV67WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV70WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV71WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV72WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV73WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV74WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV75WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV76WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV77WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FA2,100,0,true,false )
             ,new CursorDef("P00FA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FA3,100,0,true,false )
             ,new CursorDef("P00FA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FA4,100,0,true,false )
             ,new CursorDef("P00FA5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FA5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratadafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratadafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratadafilterdata") )
          {
             return  ;
          }
          getwwcontratadafilterdata worker = new getwwcontratadafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
