/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:19:50.11
*/
gx.evt.autoSkip = false;
gx.define('servicogruposervicowc', true, function (CmpContext) {
   this.ServerClass =  "servicogruposervicowc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7ServicoGrupo_Codigo=gx.fn.getIntegerValue("vSERVICOGRUPO_CODIGO",'.') ;
      this.AV87Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV11GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A601ContagemResultado_Servico=gx.fn.getIntegerValue("CONTAGEMRESULTADO_SERVICO",'.') ;
      this.A1636ContagemResultado_ServicoSS=gx.fn.getIntegerValue("CONTAGEMRESULTADO_SERVICOSS",'.') ;
      this.A829UsuarioServicos_ServicoCod=gx.fn.getIntegerValue("USUARIOSERVICOS_SERVICOCOD",'.') ;
   };
   this.Validv_Servico_tipohierarquia=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSERVICO_TIPOHIERARQUIA");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(47) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("SERVICO_CODIGO", gx.fn.currentGridRowImpl(47));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("vSERVICO_NOME1","Visible", false );
      gx.fn.setCtrlProperty("vSERVICO_ISPUBLICO1","Visible", false );
      if ( this.AV16DynamicFiltersSelector1 == "SERVICO_NOME" )
      {
         gx.fn.setCtrlProperty("vSERVICO_NOME1","Visible", true );
      }
      else if ( this.AV16DynamicFiltersSelector1 == "SERVICO_ISPUBLICO" )
      {
         gx.fn.setCtrlProperty("vSERVICO_ISPUBLICO1","Visible", true );
      }
   };
   this.s142_client=function()
   {
      this.s162_client();
      if ( this.AV14OrderedBy == 1 )
      {
         this.DDO_SERVICO_NOMEContainer.SortedStatus =  (this.AV15OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV14OrderedBy == 2 )
      {
         this.DDO_SERVICO_SIGLAContainer.SortedStatus =  (this.AV15OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV14OrderedBy == 3 )
      {
         this.DDO_SERVICO_ATIVOContainer.SortedStatus =  (this.AV15OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s162_client=function()
   {
      this.DDO_SERVICO_NOMEContainer.SortedStatus =  ""  ;
      this.DDO_SERVICO_SIGLAContainer.SortedStatus =  ""  ;
      this.DDO_SERVICO_ATIVOContainer.SortedStatus =  ""  ;
   };
   this.s182_client=function()
   {
   };
   this.e246u2_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_associationservicousuarios.aspx",[this.A155Servico_Codigo]), []);
      this.refreshOutputs([]);
   };
   this.e116u2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e136u2_client=function()
   {
      this.executeServerEvent("DDO_SERVICO_NOME.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e146u2_client=function()
   {
      this.executeServerEvent("DDO_SERVICO_SIGLA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e156u2_client=function()
   {
      this.executeServerEvent("DDO_SERVICO_ATIVO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e166u2_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e176u2_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e186u2_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e236u2_client=function()
   {
      this.executeServerEvent("VDELETE.CLICK", true, arguments[0], false, false);
   };
   this.e126u2_client=function()
   {
      this.executeServerEvent("CONFIRMPANEL.CLOSE", false, null, true, true);
   };
   this.e196u2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e256u2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e266u2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,14,16,18,19,21,24,26,28,30,33,35,37,39,40,44,48,49,50,51,52,53,54,55,56,62,67,68,69,70,71,73,75,77];
   this.GXLastCtrlId =77;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",47,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"servicogruposervicowc",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",48,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",49,36,"px",17,"px","e236u2_client","","","Image","");
   GridContainer.addBitmap("&Display","vDISPLAY",50,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(155,51,"SERVICO_CODIGO","Código","","Servico_Codigo","int",0,"px",6,6,"right",null,[],155,"Servico_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(157,52,"SERVICOGRUPO_CODIGO","Grupo","","ServicoGrupo_Codigo","int",0,"px",6,6,"right",null,[],157,"ServicoGrupo_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(608,53,"SERVICO_NOME","","","Servico_Nome","char",380,"px",50,50,"left",null,[],608,"Servico_Nome",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(605,54,"SERVICO_SIGLA","","","Servico_Sigla","char",0,"px",15,15,"left",null,[],605,"Servico_Sigla",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addComboBox(632,55,"SERVICO_ATIVO","","Servico_Ativo","boolean",null,0,true,false,0,"px","");
   GridContainer.addBitmap("&Associarusuarios","vASSOCIARUSUARIOS",56,36,"px",17,"px","e246u2_client","","","Image","");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 66, 18, "DVelop_WorkWithPlusUtilities", this.CmpContext + "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_SERVICO_NOMEContainer = gx.uc.getNew(this, 72, 18, "BootstrapDropDownOptions", this.CmpContext + "DDO_SERVICO_NOMEContainer", "Ddo_servico_nome");
   var DDO_SERVICO_NOMEContainer = this.DDO_SERVICO_NOMEContainer;
   DDO_SERVICO_NOMEContainer.setProp("Icon", "Icon", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("Caption", "Caption", "", "str");
   DDO_SERVICO_NOMEContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SERVICO_NOMEContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SERVICO_NOMEContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SERVICO_NOMEContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SERVICO_NOMEContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SERVICO_NOMEContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SERVICO_NOMEContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SERVICO_NOMEContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SERVICO_NOMEContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SERVICO_NOMEContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SERVICO_NOMEContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_SERVICO_NOMEContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_SERVICO_NOMEContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_SERVICO_NOMEContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_SERVICO_NOMEContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_SERVICO_NOMEContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("DataListProc", "Datalistproc", "GetServicoGrupoServicoWCFilterData", "str");
   DDO_SERVICO_NOMEContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_SERVICO_NOMEContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SERVICO_NOMEContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SERVICO_NOMEContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SERVICO_NOMEContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_SERVICO_NOMEContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SERVICO_NOMEContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_SERVICO_NOMEContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_SERVICO_NOMEContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SERVICO_NOMEContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SERVICO_NOMEContainer.addV2CFunction('AV72DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SERVICO_NOMEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV72DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV72DDO_TitleSettingsIcons); });
   DDO_SERVICO_NOMEContainer.addV2CFunction('AV61Servico_NomeTitleFilterData', "vSERVICO_NOMETITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SERVICO_NOMEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61Servico_NomeTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSERVICO_NOMETITLEFILTERDATA",UC.ParentObject.AV61Servico_NomeTitleFilterData); });
   DDO_SERVICO_NOMEContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SERVICO_NOMEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SERVICO_NOMEContainer.setProp("Class", "Class", "", "char");
   DDO_SERVICO_NOMEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SERVICO_NOMEContainer.addEventHandler("OnOptionClicked", this.e136u2_client);
   this.setUserControl(DDO_SERVICO_NOMEContainer);
   this.DDO_SERVICO_SIGLAContainer = gx.uc.getNew(this, 74, 18, "BootstrapDropDownOptions", this.CmpContext + "DDO_SERVICO_SIGLAContainer", "Ddo_servico_sigla");
   var DDO_SERVICO_SIGLAContainer = this.DDO_SERVICO_SIGLAContainer;
   DDO_SERVICO_SIGLAContainer.setProp("Icon", "Icon", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("Caption", "Caption", "", "str");
   DDO_SERVICO_SIGLAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SERVICO_SIGLAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SERVICO_SIGLAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SERVICO_SIGLAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SERVICO_SIGLAContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SERVICO_SIGLAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SERVICO_SIGLAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SERVICO_SIGLAContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SERVICO_SIGLAContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_SERVICO_SIGLAContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_SERVICO_SIGLAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("DataListProc", "Datalistproc", "GetServicoGrupoServicoWCFilterData", "str");
   DDO_SERVICO_SIGLAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_SERVICO_SIGLAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SERVICO_SIGLAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SERVICO_SIGLAContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SERVICO_SIGLAContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_SERVICO_SIGLAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SERVICO_SIGLAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_SERVICO_SIGLAContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_SERVICO_SIGLAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SERVICO_SIGLAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SERVICO_SIGLAContainer.addV2CFunction('AV72DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SERVICO_SIGLAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV72DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV72DDO_TitleSettingsIcons); });
   DDO_SERVICO_SIGLAContainer.addV2CFunction('AV65Servico_SiglaTitleFilterData', "vSERVICO_SIGLATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SERVICO_SIGLAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV65Servico_SiglaTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSERVICO_SIGLATITLEFILTERDATA",UC.ParentObject.AV65Servico_SiglaTitleFilterData); });
   DDO_SERVICO_SIGLAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SERVICO_SIGLAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SERVICO_SIGLAContainer.setProp("Class", "Class", "", "char");
   DDO_SERVICO_SIGLAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SERVICO_SIGLAContainer.addEventHandler("OnOptionClicked", this.e146u2_client);
   this.setUserControl(DDO_SERVICO_SIGLAContainer);
   this.DDO_SERVICO_ATIVOContainer = gx.uc.getNew(this, 76, 18, "BootstrapDropDownOptions", this.CmpContext + "DDO_SERVICO_ATIVOContainer", "Ddo_servico_ativo");
   var DDO_SERVICO_ATIVOContainer = this.DDO_SERVICO_ATIVOContainer;
   DDO_SERVICO_ATIVOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SERVICO_ATIVOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SERVICO_ATIVOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SERVICO_ATIVOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SERVICO_ATIVOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SERVICO_ATIVOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SERVICO_ATIVOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SERVICO_ATIVOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SERVICO_ATIVOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SERVICO_ATIVOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_SERVICO_ATIVOContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_SERVICO_ATIVOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_SERVICO_ATIVOContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_SERVICO_ATIVOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_SERVICO_ATIVOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "1:Marcado,2:Desmarcado", "str");
   DDO_SERVICO_ATIVOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SERVICO_ATIVOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SERVICO_ATIVOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SERVICO_ATIVOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SERVICO_ATIVOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SERVICO_ATIVOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SERVICO_ATIVOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SERVICO_ATIVOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SERVICO_ATIVOContainer.addV2CFunction('AV72DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SERVICO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV72DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV72DDO_TitleSettingsIcons); });
   DDO_SERVICO_ATIVOContainer.addV2CFunction('AV69Servico_AtivoTitleFilterData', "vSERVICO_ATIVOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SERVICO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69Servico_AtivoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSERVICO_ATIVOTITLEFILTERDATA",UC.ParentObject.AV69Servico_AtivoTitleFilterData); });
   DDO_SERVICO_ATIVOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SERVICO_ATIVOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SERVICO_ATIVOContainer.setProp("Class", "Class", "", "char");
   DDO_SERVICO_ATIVOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SERVICO_ATIVOContainer.addEventHandler("OnOptionClicked", this.e156u2_client);
   this.setUserControl(DDO_SERVICO_ATIVOContainer);
   this.CONFIRMPANELContainer = gx.uc.getNew(this, 65, 18, "DVelop_ConfirmPanel", this.CmpContext + "CONFIRMPANELContainer", "Confirmpanel");
   var CONFIRMPANELContainer = this.CONFIRMPANELContainer;
   CONFIRMPANELContainer.setProp("Width", "Width", "", "str");
   CONFIRMPANELContainer.setProp("Height", "Height", "50", "str");
   CONFIRMPANELContainer.setProp("Closeable", "Closeable", true, "bool");
   CONFIRMPANELContainer.setProp("Title", "Title", "Atenção", "str");
   CONFIRMPANELContainer.setProp("ConfirmText", "Confirmtext", "Este serviço esta vinculado a alguns usuários. Confirma elimina-lo?", "str");
   CONFIRMPANELContainer.setProp("Icon", "Icon", "2", "str");
   CONFIRMPANELContainer.setProp("Modal", "Modal", true, "bool");
   CONFIRMPANELContainer.setProp("Result", "Result", "", "char");
   CONFIRMPANELContainer.setProp("ButtonYesText", "Buttonyestext", "Sim", "str");
   CONFIRMPANELContainer.setProp("ButtonNoText", "Buttonnotext", "Não", "str");
   CONFIRMPANELContainer.setProp("ButtonCancelText", "Buttoncanceltext", "Cancelar", "str");
   CONFIRMPANELContainer.setProp("ConfirmType", "Confirmtype", "1", "str");
   CONFIRMPANELContainer.setProp("Draggable", "Draggeable", true, "bool");
   CONFIRMPANELContainer.setProp("Visible", "Visible", true, "bool");
   CONFIRMPANELContainer.setProp("Enabled", "Enabled", true, "boolean");
   CONFIRMPANELContainer.setProp("Class", "Class", "", "char");
   CONFIRMPANELContainer.setC2ShowFunction(function(UC) { UC.Show(); });
   CONFIRMPANELContainer.addEventHandler("Close", this.e126u2_client);
   this.setUserControl(CONFIRMPANELContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 59, 18, "DVelop_DVPaginationBar", this.CmpContext + "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV74GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV74GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV74GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV75GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV75GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV75GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e116u2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[8]={fld:"TABLESEARCH",grid:0};
   GXValidFnc[11]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[14]={fld:"INSERT",grid:0};
   GXValidFnc[16]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV14OrderedBy",gxold:"OV14OrderedBy",gxvar:"AV14OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV14OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[19]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV15OrderedDsc",gxold:"OV15OrderedDsc",gxvar:"AV15OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV15OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV15OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV15OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[21]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[24]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[26]={fld:"FILTERTEXTSERVICO_TIPOHIERARQUIA", format:0,grid:0};
   GXValidFnc[28]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Servico_tipohierarquia,isvalid:null,rgrid:[],fld:"vSERVICO_TIPOHIERARQUIA",gxz:"ZV57Servico_TipoHierarquia",gxold:"OV57Servico_TipoHierarquia",gxvar:"AV57Servico_TipoHierarquia",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV57Servico_TipoHierarquia=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV57Servico_TipoHierarquia=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_TIPOHIERARQUIA",gx.O.AV57Servico_TipoHierarquia)},c2v:function(){if(this.val()!==undefined)gx.O.AV57Servico_TipoHierarquia=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_TIPOHIERARQUIA",'.')},nac:gx.falseFn};
   GXValidFnc[30]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[33]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[35]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV16DynamicFiltersSelector1",gxold:"OV16DynamicFiltersSelector1",gxvar:"AV16DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV16DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV16DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[37]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[39]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vSERVICO_NOME1",gxz:"ZV54Servico_Nome1",gxold:"OV54Servico_Nome1",gxvar:"AV54Servico_Nome1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV54Servico_Nome1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV54Servico_Nome1=Value},v2c:function(){gx.fn.setControlValue("vSERVICO_NOME1",gx.O.AV54Servico_Nome1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV54Servico_Nome1=this.val()},val:function(){return gx.fn.getControlValue("vSERVICO_NOME1")},nac:gx.falseFn};
   GXValidFnc[40]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vSERVICO_ISPUBLICO1",gxz:"ZV76Servico_IsPublico1",gxold:"OV76Servico_IsPublico1",gxvar:"AV76Servico_IsPublico1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV76Servico_IsPublico1=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV76Servico_IsPublico1=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_ISPUBLICO1",gx.O.AV76Servico_IsPublico1)},c2v:function(){if(this.val()!==undefined)gx.O.AV76Servico_IsPublico1=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vSERVICO_ISPUBLICO1")},nac:gx.falseFn};
   GXValidFnc[44]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[48]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV30Update",gxold:"OV30Update",gxvar:"AV30Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV30Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV30Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(47),gx.O.AV30Update,gx.O.AV80Update_GXI)},c2v:function(){gx.O.AV80Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV30Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(47))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(47))}, gxvar_GXI:'AV80Update_GXI',nac:gx.falseFn};
   GXValidFnc[49]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV29Delete",gxold:"OV29Delete",gxvar:"AV29Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV29Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(47),gx.O.AV29Delete,gx.O.AV81Delete_GXI)},c2v:function(){gx.O.AV81Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV29Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(47))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(47))}, gxvar_GXI:'AV81Delete_GXI',nac:gx.falseFn};
   GXValidFnc[50]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDISPLAY",gxz:"ZV32Display",gxold:"OV32Display",gxvar:"AV32Display",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV32Display=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32Display=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDISPLAY",row || gx.fn.currentGridRowImpl(47),gx.O.AV32Display,gx.O.AV82Display_GXI)},c2v:function(){gx.O.AV82Display_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV32Display=this.val()},val:function(row){return gx.fn.getGridControlValue("vDISPLAY",row || gx.fn.currentGridRowImpl(47))},val_GXI:function(row){return gx.fn.getGridControlValue("vDISPLAY_GXI",row || gx.fn.currentGridRowImpl(47))}, gxvar_GXI:'AV82Display_GXI',nac:gx.falseFn};
   GXValidFnc[51]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:this.Valid_Servico_codigo,isvalid:null,rgrid:[],fld:"SERVICO_CODIGO",gxz:"Z155Servico_Codigo",gxold:"O155Servico_Codigo",gxvar:"A155Servico_Codigo",ucs:[],op:[28],ip:[28],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z155Servico_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SERVICO_CODIGO",row || gx.fn.currentGridRowImpl(47),gx.O.A155Servico_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SERVICO_CODIGO",row || gx.fn.currentGridRowImpl(47),'.')},nac:gx.falseFn};
   GXValidFnc[52]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SERVICOGRUPO_CODIGO",gxz:"Z157ServicoGrupo_Codigo",gxold:"O157ServicoGrupo_Codigo",gxvar:"A157ServicoGrupo_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A157ServicoGrupo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z157ServicoGrupo_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SERVICOGRUPO_CODIGO",row || gx.fn.currentGridRowImpl(47),gx.O.A157ServicoGrupo_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A157ServicoGrupo_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SERVICOGRUPO_CODIGO",row || gx.fn.currentGridRowImpl(47),'.')},nac:gx.falseFn};
   GXValidFnc[53]={lvl:2,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_NOME",gxz:"Z608Servico_Nome",gxold:"O608Servico_Nome",gxvar:"A608Servico_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A608Servico_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z608Servico_Nome=Value},v2c:function(row){gx.fn.setGridControlValue("SERVICO_NOME",row || gx.fn.currentGridRowImpl(47),gx.O.A608Servico_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A608Servico_Nome=this.val()},val:function(row){return gx.fn.getGridControlValue("SERVICO_NOME",row || gx.fn.currentGridRowImpl(47))},nac:gx.falseFn};
   GXValidFnc[54]={lvl:2,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_SIGLA",gxz:"Z605Servico_Sigla",gxold:"O605Servico_Sigla",gxvar:"A605Servico_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A605Servico_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z605Servico_Sigla=Value},v2c:function(row){gx.fn.setGridControlValue("SERVICO_SIGLA",row || gx.fn.currentGridRowImpl(47),gx.O.A605Servico_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A605Servico_Sigla=this.val()},val:function(row){return gx.fn.getGridControlValue("SERVICO_SIGLA",row || gx.fn.currentGridRowImpl(47))},nac:gx.falseFn};
   GXValidFnc[55]={lvl:2,type:"boolean",len:4,dec:0,sign:false,ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_ATIVO",gxz:"Z632Servico_Ativo",gxold:"O632Servico_Ativo",gxvar:"A632Servico_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A632Servico_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z632Servico_Ativo=gx.lang.booleanValue(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("SERVICO_ATIVO",row || gx.fn.currentGridRowImpl(47),gx.O.A632Servico_Ativo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A632Servico_Ativo=gx.lang.booleanValue(this.val())},val:function(row){return gx.fn.getGridControlValue("SERVICO_ATIVO",row || gx.fn.currentGridRowImpl(47))},nac:gx.falseFn};
   GXValidFnc[56]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:47,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vASSOCIARUSUARIOS",gxz:"ZV56AssociarUsuarios",gxold:"OV56AssociarUsuarios",gxvar:"AV56AssociarUsuarios",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV56AssociarUsuarios=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV56AssociarUsuarios=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vASSOCIARUSUARIOS",row || gx.fn.currentGridRowImpl(47),gx.O.AV56AssociarUsuarios,gx.O.AV83Associarusuarios_GXI)},c2v:function(){gx.O.AV83Associarusuarios_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV56AssociarUsuarios=this.val()},val:function(row){return gx.fn.getGridControlValue("vASSOCIARUSUARIOS",row || gx.fn.currentGridRowImpl(47))},val_GXI:function(row){return gx.fn.getGridControlValue("vASSOCIARUSUARIOS_GXI",row || gx.fn.currentGridRowImpl(47))}, gxvar_GXI:'AV83Associarusuarios_GXI',nac:gx.falseFn};
   GXValidFnc[62]={fld:"USRTABLE",grid:0};
   GXValidFnc[67]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSERVICO_NOME",gxz:"ZV62TFServico_Nome",gxold:"OV62TFServico_Nome",gxvar:"AV62TFServico_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV62TFServico_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV62TFServico_Nome=Value},v2c:function(){gx.fn.setControlValue("vTFSERVICO_NOME",gx.O.AV62TFServico_Nome,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV62TFServico_Nome=this.val()},val:function(){return gx.fn.getControlValue("vTFSERVICO_NOME")},nac:gx.falseFn};
   GXValidFnc[68]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSERVICO_NOME_SEL",gxz:"ZV63TFServico_Nome_Sel",gxold:"OV63TFServico_Nome_Sel",gxvar:"AV63TFServico_Nome_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV63TFServico_Nome_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV63TFServico_Nome_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFSERVICO_NOME_SEL",gx.O.AV63TFServico_Nome_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV63TFServico_Nome_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFSERVICO_NOME_SEL")},nac:gx.falseFn};
   GXValidFnc[69]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSERVICO_SIGLA",gxz:"ZV66TFServico_Sigla",gxold:"OV66TFServico_Sigla",gxvar:"AV66TFServico_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV66TFServico_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV66TFServico_Sigla=Value},v2c:function(){gx.fn.setControlValue("vTFSERVICO_SIGLA",gx.O.AV66TFServico_Sigla,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV66TFServico_Sigla=this.val()},val:function(){return gx.fn.getControlValue("vTFSERVICO_SIGLA")},nac:gx.falseFn};
   GXValidFnc[70]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSERVICO_SIGLA_SEL",gxz:"ZV67TFServico_Sigla_Sel",gxold:"OV67TFServico_Sigla_Sel",gxvar:"AV67TFServico_Sigla_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV67TFServico_Sigla_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV67TFServico_Sigla_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFSERVICO_SIGLA_SEL",gx.O.AV67TFServico_Sigla_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV67TFServico_Sigla_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFSERVICO_SIGLA_SEL")},nac:gx.falseFn};
   GXValidFnc[71]={lvl:0,type:"int",len:1,dec:0,sign:false,pic:"9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSERVICO_ATIVO_SEL",gxz:"ZV70TFServico_Ativo_Sel",gxold:"OV70TFServico_Ativo_Sel",gxvar:"AV70TFServico_Ativo_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV70TFServico_Ativo_Sel=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV70TFServico_Ativo_Sel=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFSERVICO_ATIVO_SEL",gx.O.AV70TFServico_Ativo_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV70TFServico_Ativo_Sel=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFSERVICO_ATIVO_SEL",'.')},nac:gx.falseFn};
   GXValidFnc[73]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE",gxz:"ZV64ddo_Servico_NomeTitleControlIdToReplace",gxold:"OV64ddo_Servico_NomeTitleControlIdToReplace",gxvar:"AV64ddo_Servico_NomeTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV64ddo_Servico_NomeTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV64ddo_Servico_NomeTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE",gx.O.AV64ddo_Servico_NomeTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV64ddo_Servico_NomeTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[75]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE",gxz:"ZV68ddo_Servico_SiglaTitleControlIdToReplace",gxold:"OV68ddo_Servico_SiglaTitleControlIdToReplace",gxvar:"AV68ddo_Servico_SiglaTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV68ddo_Servico_SiglaTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV68ddo_Servico_SiglaTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE",gx.O.AV68ddo_Servico_SiglaTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV68ddo_Servico_SiglaTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[77]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE",gxz:"ZV71ddo_Servico_AtivoTitleControlIdToReplace",gxold:"OV71ddo_Servico_AtivoTitleControlIdToReplace",gxvar:"AV71ddo_Servico_AtivoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV71ddo_Servico_AtivoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV71ddo_Servico_AtivoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE",gx.O.AV71ddo_Servico_AtivoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV71ddo_Servico_AtivoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV14OrderedBy = 0 ;
   this.ZV14OrderedBy = 0 ;
   this.OV14OrderedBy = 0 ;
   this.AV15OrderedDsc = false ;
   this.ZV15OrderedDsc = false ;
   this.OV15OrderedDsc = false ;
   this.AV57Servico_TipoHierarquia = 0 ;
   this.ZV57Servico_TipoHierarquia = 0 ;
   this.OV57Servico_TipoHierarquia = 0 ;
   this.AV16DynamicFiltersSelector1 = "" ;
   this.ZV16DynamicFiltersSelector1 = "" ;
   this.OV16DynamicFiltersSelector1 = "" ;
   this.AV54Servico_Nome1 = "" ;
   this.ZV54Servico_Nome1 = "" ;
   this.OV54Servico_Nome1 = "" ;
   this.AV76Servico_IsPublico1 = false ;
   this.ZV76Servico_IsPublico1 = false ;
   this.OV76Servico_IsPublico1 = false ;
   this.ZV30Update = "" ;
   this.OV30Update = "" ;
   this.ZV29Delete = "" ;
   this.OV29Delete = "" ;
   this.ZV32Display = "" ;
   this.OV32Display = "" ;
   this.Z155Servico_Codigo = 0 ;
   this.O155Servico_Codigo = 0 ;
   this.Z157ServicoGrupo_Codigo = 0 ;
   this.O157ServicoGrupo_Codigo = 0 ;
   this.Z608Servico_Nome = "" ;
   this.O608Servico_Nome = "" ;
   this.Z605Servico_Sigla = "" ;
   this.O605Servico_Sigla = "" ;
   this.Z632Servico_Ativo = false ;
   this.O632Servico_Ativo = false ;
   this.ZV56AssociarUsuarios = "" ;
   this.OV56AssociarUsuarios = "" ;
   this.AV62TFServico_Nome = "" ;
   this.ZV62TFServico_Nome = "" ;
   this.OV62TFServico_Nome = "" ;
   this.AV63TFServico_Nome_Sel = "" ;
   this.ZV63TFServico_Nome_Sel = "" ;
   this.OV63TFServico_Nome_Sel = "" ;
   this.AV66TFServico_Sigla = "" ;
   this.ZV66TFServico_Sigla = "" ;
   this.OV66TFServico_Sigla = "" ;
   this.AV67TFServico_Sigla_Sel = "" ;
   this.ZV67TFServico_Sigla_Sel = "" ;
   this.OV67TFServico_Sigla_Sel = "" ;
   this.AV70TFServico_Ativo_Sel = 0 ;
   this.ZV70TFServico_Ativo_Sel = 0 ;
   this.OV70TFServico_Ativo_Sel = 0 ;
   this.AV64ddo_Servico_NomeTitleControlIdToReplace = "" ;
   this.ZV64ddo_Servico_NomeTitleControlIdToReplace = "" ;
   this.OV64ddo_Servico_NomeTitleControlIdToReplace = "" ;
   this.AV68ddo_Servico_SiglaTitleControlIdToReplace = "" ;
   this.ZV68ddo_Servico_SiglaTitleControlIdToReplace = "" ;
   this.OV68ddo_Servico_SiglaTitleControlIdToReplace = "" ;
   this.AV71ddo_Servico_AtivoTitleControlIdToReplace = "" ;
   this.ZV71ddo_Servico_AtivoTitleControlIdToReplace = "" ;
   this.OV71ddo_Servico_AtivoTitleControlIdToReplace = "" ;
   this.AV14OrderedBy = 0 ;
   this.AV15OrderedDsc = false ;
   this.AV57Servico_TipoHierarquia = 0 ;
   this.AV16DynamicFiltersSelector1 = "" ;
   this.AV54Servico_Nome1 = "" ;
   this.AV76Servico_IsPublico1 = false ;
   this.AV74GridCurrentPage = 0 ;
   this.AV62TFServico_Nome = "" ;
   this.AV63TFServico_Nome_Sel = "" ;
   this.AV66TFServico_Sigla = "" ;
   this.AV67TFServico_Sigla_Sel = "" ;
   this.AV70TFServico_Ativo_Sel = 0 ;
   this.AV72DDO_TitleSettingsIcons = {} ;
   this.AV64ddo_Servico_NomeTitleControlIdToReplace = "" ;
   this.AV68ddo_Servico_SiglaTitleControlIdToReplace = "" ;
   this.AV71ddo_Servico_AtivoTitleControlIdToReplace = "" ;
   this.AV7ServicoGrupo_Codigo = 0 ;
   this.A1635Servico_IsPublico = false ;
   this.A1530Servico_TipoHierarquia = 0 ;
   this.AV30Update = "" ;
   this.AV29Delete = "" ;
   this.AV32Display = "" ;
   this.A155Servico_Codigo = 0 ;
   this.A157ServicoGrupo_Codigo = 0 ;
   this.A608Servico_Nome = "" ;
   this.A605Servico_Sigla = "" ;
   this.A632Servico_Ativo = false ;
   this.AV56AssociarUsuarios = "" ;
   this.A829UsuarioServicos_ServicoCod = 0 ;
   this.A1636ContagemResultado_ServicoSS = 0 ;
   this.A601ContagemResultado_Servico = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.AV87Pgmname = "" ;
   this.AV11GridState = {} ;
   this.AV6WWPContext = {} ;
   this.Events = {"e116u2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e136u2_client": ["DDO_SERVICO_NOME.ONOPTIONCLICKED", true] ,"e146u2_client": ["DDO_SERVICO_SIGLA.ONOPTIONCLICKED", true] ,"e156u2_client": ["DDO_SERVICO_ATIVO.ONOPTIONCLICKED", true] ,"e166u2_client": ["VORDEREDBY.CLICK", true] ,"e176u2_client": ["'DOCLEANFILTERS'", true] ,"e186u2_client": ["'DOINSERT'", true] ,"e236u2_client": ["VDELETE.CLICK", true] ,"e126u2_client": ["CONFIRMPANEL.CLOSE", true] ,"e196u2_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e256u2_client": ["ENTER", true] ,"e266u2_client": ["CANCEL", true] ,"e246u2_client": ["'DOASSOCIARUSUARIOS'", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false}],[{av:'AV61Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV65Servico_SiglaTitleFilterData',fld:'vSERVICO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV69Servico_AtivoTitleFilterData',fld:'vSERVICO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'SERVICO_NOME',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("SERVICO_NOME","Title")',ctrl:'SERVICO_NOME',prop:'Title'},{ctrl:'SERVICO_SIGLA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("SERVICO_SIGLA","Title")',ctrl:'SERVICO_SIGLA',prop:'Title'},{ctrl:'SERVICO_ATIVO'},{av:'AV74GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV75GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vASSOCIARUSUARIOS","Title")',ctrl:'vASSOCIARUSUARIOS',prop:'Title'},{av:'gx.fn.getCtrlProperty("INSERT","Visible")',ctrl:'INSERT',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vUPDATE","Visible")',ctrl:'vUPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDELETE","Visible")',ctrl:'vDELETE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Visible")',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_SERVICO_NOME.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.DDO_SERVICO_NOMEContainer.ActiveEventKey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'this.DDO_SERVICO_NOMEContainer.FilteredText_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'this.DDO_SERVICO_NOMEContainer.SelectedValue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SERVICO_NOMEContainer.SortedStatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'this.DDO_SERVICO_SIGLAContainer.SortedStatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'this.DDO_SERVICO_ATIVOContainer.SortedStatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SERVICO_SIGLA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.DDO_SERVICO_SIGLAContainer.ActiveEventKey',ctrl:'DDO_SERVICO_SIGLA',prop:'ActiveEventKey'},{av:'this.DDO_SERVICO_SIGLAContainer.FilteredText_get',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_get'},{av:'this.DDO_SERVICO_SIGLAContainer.SelectedValue_get',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_get'}],[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SERVICO_SIGLAContainer.SortedStatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'this.DDO_SERVICO_NOMEContainer.SortedStatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'this.DDO_SERVICO_ATIVOContainer.SortedStatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SERVICO_ATIVO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.DDO_SERVICO_ATIVOContainer.ActiveEventKey',ctrl:'DDO_SERVICO_ATIVO',prop:'ActiveEventKey'},{av:'this.DDO_SERVICO_ATIVOContainer.SelectedValue_get',ctrl:'DDO_SERVICO_ATIVO',prop:'SelectedValue_get'}],[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SERVICO_ATIVOContainer.SortedStatus',ctrl:'DDO_SERVICO_ATIVO',prop:'SortedStatus'},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'this.DDO_SERVICO_NOMEContainer.SortedStatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'this.DDO_SERVICO_SIGLAContainer.SortedStatus',ctrl:'DDO_SERVICO_SIGLA',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0}],[{av:'AV30Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'AV32Display',fld:'vDISPLAY',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDISPLAY","Tooltiptext")',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Link")',ctrl:'vDISPLAY',prop:'Link'},{av:'AV56AssociarUsuarios',fld:'vASSOCIARUSUARIOS',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vASSOCIARUSUARIOS","Tooltiptext")',ctrl:'vASSOCIARUSUARIOS',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("SERVICO_NOME","Forecolor")',ctrl:'SERVICO_NOME',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("SERVICO_SIGLA","Forecolor")',ctrl:'SERVICO_SIGLA',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vASSOCIARUSUARIOS","Enabled")',ctrl:'vASSOCIARUSUARIOS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vDELETE","Visible")',ctrl:'vDELETE',prop:'Visible'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vSERVICO_NOME1","Visible")',ctrl:'vSERVICO_NOME1',prop:'Visible'},{ctrl:'vSERVICO_ISPUBLICO1'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'this.DDO_SERVICO_NOMEContainer.FilteredText_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'this.DDO_SERVICO_NOMEContainer.SelectedValue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'this.DDO_SERVICO_SIGLAContainer.FilteredText_set',ctrl:'DDO_SERVICO_SIGLA',prop:'FilteredText_set'},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'this.DDO_SERVICO_SIGLAContainer.SelectedValue_set',ctrl:'DDO_SERVICO_SIGLA',prop:'SelectedValue_set'},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'this.DDO_SERVICO_ATIVOContainer.SelectedValue_set',ctrl:'DDO_SERVICO_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("vSERVICO_NOME1","Visible")',ctrl:'vSERVICO_NOME1',prop:'Visible'},{ctrl:'vSERVICO_ISPUBLICO1'},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false}]];
   this.EvtParms["'DOASSOCIARUSUARIOS'"] = [[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["VDELETE.CLICK"] = [[{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["CONFIRMPANEL.CLOSE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV54Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV76Servico_IsPublico1',fld:'vSERVICO_ISPUBLICO1',pic:'',nv:false},{av:'AV62TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV63TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFServico_Sigla',fld:'vTFSERVICO_SIGLA',pic:'@!',nv:''},{av:'AV67TFServico_Sigla_Sel',fld:'vTFSERVICO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV70TFServico_Ativo_Sel',fld:'vTFSERVICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Servico_SiglaTitleControlIdToReplace',fld:'vDDO_SERVICO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Servico_AtivoTitleControlIdToReplace',fld:'vDDO_SERVICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV57Servico_TipoHierarquia',fld:'vSERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.CONFIRMPANELContainer.Result',ctrl:'CONFIRMPANEL',prop:'Result'}],[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("AV7ServicoGrupo_Codigo", "vSERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("AV87Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV11GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A601ContagemResultado_Servico", "CONTAGEMRESULTADO_SERVICO", 0, "int");
   this.setVCMap("A1636ContagemResultado_ServicoSS", "CONTAGEMRESULTADO_SERVICOSS", 0, "int");
   this.setVCMap("A829UsuarioServicos_ServicoCod", "USUARIOSERVICOS_SERVICOCOD", 0, "int");
   this.setVCMap("AV7ServicoGrupo_Codigo", "vSERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("AV87Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV11GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A601ContagemResultado_Servico", "CONTAGEMRESULTADO_SERVICO", 0, "int");
   this.setVCMap("A1636ContagemResultado_ServicoSS", "CONTAGEMRESULTADO_SERVICOSS", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[18]);
   GridContainer.addRefreshingVar(this.GXValidFnc[19]);
   GridContainer.addRefreshingVar(this.GXValidFnc[35]);
   GridContainer.addRefreshingVar(this.GXValidFnc[39]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[67]);
   GridContainer.addRefreshingVar(this.GXValidFnc[68]);
   GridContainer.addRefreshingVar(this.GXValidFnc[69]);
   GridContainer.addRefreshingVar(this.GXValidFnc[70]);
   GridContainer.addRefreshingVar(this.GXValidFnc[71]);
   GridContainer.addRefreshingVar({rfrVar:"AV7ServicoGrupo_Codigo"});
   GridContainer.addRefreshingVar(this.GXValidFnc[73]);
   GridContainer.addRefreshingVar(this.GXValidFnc[75]);
   GridContainer.addRefreshingVar(this.GXValidFnc[77]);
   GridContainer.addRefreshingVar({rfrVar:"AV87Pgmname"});
   GridContainer.addRefreshingVar(this.GXValidFnc[28]);
   GridContainer.addRefreshingVar({rfrVar:"AV11GridState"});
   GridContainer.addRefreshingVar({rfrVar:"A155Servico_Codigo", rfrProp:"Value", gxAttId:"155"});
   GridContainer.addRefreshingVar({rfrVar:"A632Servico_Ativo", rfrProp:"Value", gxAttId:"632"});
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"A601ContagemResultado_Servico"});
   GridContainer.addRefreshingVar({rfrVar:"A1636ContagemResultado_ServicoSS"});
   this.InitStandaloneVars( );
});
