/*
               File: type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem
        Description: GxSynchroEventResultSDT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:56.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem" )]
   [XmlType(TypeName =  "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem : GxUserType
   {
      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table = "";
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates = "";
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions = "";
      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem obj ;
         obj = this;
         obj.gxTpr_Table = deserialized.gxTpr_Table;
         obj.gxTpr_Updates = deserialized.gxTpr_Updates;
         obj.gxTpr_Conditions = deserialized.gxTpr_Conditions;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Table") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Updates") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Conditions") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Table", StringUtil.RTrim( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Updates", StringUtil.RTrim( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Conditions", StringUtil.RTrim( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Table", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table, false);
         AddObjectProperty("Updates", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates, false);
         AddObjectProperty("Conditions", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions, false);
         return  ;
      }

      [  SoapElement( ElementName = "Table" )]
      [  XmlElement( ElementName = "Table"   )]
      public String gxTpr_Table
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Updates" )]
      [  XmlElement( ElementName = "Updates"   )]
      public String gxTpr_Updates
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Conditions" )]
      [  XmlElement( ElementName = "Conditions"   )]
      public String gxTpr_Conditions
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table = "";
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates = "";
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Updates ;
      protected String gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Conditions ;
      protected String gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_Table ;
   }

   [DataContract(Name = @"GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_RESTInterface : GxGenericCollectionItem<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_RESTInterface( ) : base()
      {
      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_RESTInterface( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Table" , Order = 0 )]
      public String gxTpr_Table
      {
         get {
            return sdt.gxTpr_Table ;
         }

         set {
            sdt.gxTpr_Table = (String)(value);
         }

      }

      [DataMember( Name = "Updates" , Order = 1 )]
      public String gxTpr_Updates
      {
         get {
            return sdt.gxTpr_Updates ;
         }

         set {
            sdt.gxTpr_Updates = (String)(value);
         }

      }

      [DataMember( Name = "Conditions" , Order = 2 )]
      public String gxTpr_Conditions
      {
         get {
            return sdt.gxTpr_Conditions ;
         }

         set {
            sdt.gxTpr_Conditions = (String)(value);
         }

      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem sdt
      {
         get {
            return (SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem() ;
         }
      }

   }

}
