/*
               File: GetServicoContratoServicoWCFilterData
        Description: Get Servico Contrato Servico WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:38.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getservicocontratoservicowcfilterdata : GXProcedure
   {
      public getservicocontratoservicowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getservicocontratoservicowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getservicocontratoservicowcfilterdata objgetservicocontratoservicowcfilterdata;
         objgetservicocontratoservicowcfilterdata = new getservicocontratoservicowcfilterdata();
         objgetservicocontratoservicowcfilterdata.AV14DDOName = aP0_DDOName;
         objgetservicocontratoservicowcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetservicocontratoservicowcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetservicocontratoservicowcfilterdata.AV18OptionsJson = "" ;
         objgetservicocontratoservicowcfilterdata.AV21OptionsDescJson = "" ;
         objgetservicocontratoservicowcfilterdata.AV23OptionIndexesJson = "" ;
         objgetservicocontratoservicowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetservicocontratoservicowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetservicocontratoservicowcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getservicocontratoservicowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("ServicoContratoServicoWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ServicoContratoServicoWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("ServicoContratoServicoWCGridState"), "");
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV30Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&SERVICO_CODIGO") == 0 )
            {
               AV39Servico_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV32Contratada_PessoaNom1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV35Contratada_PessoaNom2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV38Contratada_PessoaNom3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV12SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV31DynamicFiltersSelector1 ,
                                              AV32Contratada_PessoaNom1 ,
                                              AV33DynamicFiltersEnabled2 ,
                                              AV34DynamicFiltersSelector2 ,
                                              AV35Contratada_PessoaNom2 ,
                                              AV36DynamicFiltersEnabled3 ,
                                              AV37DynamicFiltersSelector3 ,
                                              AV38Contratada_PessoaNom3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              A41Contratada_PessoaNom ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              AV39Servico_Codigo ,
                                              A155Servico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV32Contratada_PessoaNom1), 100, "%");
         lV35Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV35Contratada_PessoaNom2), 100, "%");
         lV38Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV38Contratada_PessoaNom3), 100, "%");
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         /* Using cursor P00JO2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV32Contratada_PessoaNom1, lV35Contratada_PessoaNom2, lV38Contratada_PessoaNom3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJO2 = false;
            A40Contratada_PessoaCod = P00JO2_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00JO2_A52Contratada_AreaTrabalhoCod[0];
            A41Contratada_PessoaNom = P00JO2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JO2_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = P00JO2_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00JO2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JO2_n41Contratada_PessoaNom[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JO2_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKJO2 = false;
               A40Contratada_PessoaCod = P00JO2_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00JO2_A39Contratada_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKJO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV16Option = A41Contratada_PessoaNom;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJO2 )
            {
               BRKJO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32Contratada_PessoaNom1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35Contratada_PessoaNom2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38Contratada_PessoaNom3 = "";
         scmdbuf = "";
         lV32Contratada_PessoaNom1 = "";
         lV35Contratada_PessoaNom2 = "";
         lV38Contratada_PessoaNom3 = "";
         lV10TFContratada_PessoaNom = "";
         A41Contratada_PessoaNom = "";
         P00JO2_A40Contratada_PessoaCod = new int[1] ;
         P00JO2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00JO2_A41Contratada_PessoaNom = new String[] {""} ;
         P00JO2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00JO2_A39Contratada_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getservicocontratoservicowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JO2_A40Contratada_PessoaCod, P00JO2_A52Contratada_AreaTrabalhoCod, P00JO2_A41Contratada_PessoaNom, P00JO2_n41Contratada_PessoaNom, P00JO2_A39Contratada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV42GXV1 ;
      private int AV30Contratada_AreaTrabalhoCod ;
      private int AV39Servico_Codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A155Servico_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private long AV24count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV32Contratada_PessoaNom1 ;
      private String AV35Contratada_PessoaNom2 ;
      private String AV38Contratada_PessoaNom3 ;
      private String scmdbuf ;
      private String lV32Contratada_PessoaNom1 ;
      private String lV35Contratada_PessoaNom2 ;
      private String lV38Contratada_PessoaNom3 ;
      private String lV10TFContratada_PessoaNom ;
      private String A41Contratada_PessoaNom ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool BRKJO2 ;
      private bool n41Contratada_PessoaNom ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JO2_A40Contratada_PessoaCod ;
      private int[] P00JO2_A52Contratada_AreaTrabalhoCod ;
      private String[] P00JO2_A41Contratada_PessoaNom ;
      private bool[] P00JO2_n41Contratada_PessoaNom ;
      private int[] P00JO2_A39Contratada_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getservicocontratoservicowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JO2( IGxContext context ,
                                             String AV31DynamicFiltersSelector1 ,
                                             String AV32Contratada_PessoaNom1 ,
                                             bool AV33DynamicFiltersEnabled2 ,
                                             String AV34DynamicFiltersSelector2 ,
                                             String AV35Contratada_PessoaNom2 ,
                                             bool AV36DynamicFiltersEnabled3 ,
                                             String AV37DynamicFiltersSelector3 ,
                                             String AV38Contratada_PessoaNom3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String A41Contratada_PessoaNom ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int AV39Servico_Codigo ,
                                             int A155Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV32Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV33DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV35Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV38Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JO2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JO2 ;
          prmP00JO2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV35Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JO2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getservicocontratoservicowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getservicocontratoservicowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getservicocontratoservicowcfilterdata") )
          {
             return  ;
          }
          getservicocontratoservicowcfilterdata worker = new getservicocontratoservicowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
