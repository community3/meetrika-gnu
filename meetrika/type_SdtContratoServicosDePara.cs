/*
               File: type_SdtContratoServicosDePara
        Description: Servicos (De Para)
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:11:21.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoServicosDePara" )]
   [XmlType(TypeName =  "ContratoServicosDePara" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoServicosDePara : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosDePara( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 = "";
         gxTv_SdtContratoServicosDePara_Mode = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z = "";
      }

      public SdtContratoServicosDePara( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV160ContratoServicos_Codigo ,
                        int AV1465ContratoServicosDePara_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV160ContratoServicos_Codigo,(int)AV1465ContratoServicosDePara_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoServicos_Codigo", typeof(int)}, new Object[]{"ContratoServicosDePara_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoServicosDePara");
         metadata.Set("BT", "ContratoServicosDePara");
         metadata.Set("PK", "[ \"ContratoServicos_Codigo\",\"ContratoServicosDePara_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origenid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origenid2_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origenid_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origemdsc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origenid2_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoservicosdepara_origemdsc2_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoServicosDePara deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoServicosDePara)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoServicosDePara obj ;
         obj = this;
         obj.gxTpr_Contratoservicos_codigo = deserialized.gxTpr_Contratoservicos_codigo;
         obj.gxTpr_Contratoservicosdepara_codigo = deserialized.gxTpr_Contratoservicosdepara_codigo;
         obj.gxTpr_Contratoservicosdepara_origem = deserialized.gxTpr_Contratoservicosdepara_origem;
         obj.gxTpr_Contratoservicosdepara_origenid = deserialized.gxTpr_Contratoservicosdepara_origenid;
         obj.gxTpr_Contratoservicosdepara_origemdsc = deserialized.gxTpr_Contratoservicosdepara_origemdsc;
         obj.gxTpr_Contratoservicosdepara_origenid2 = deserialized.gxTpr_Contratoservicosdepara_origenid2;
         obj.gxTpr_Contratoservicosdepara_origemdsc2 = deserialized.gxTpr_Contratoservicosdepara_origemdsc2;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoservicos_codigo_Z = deserialized.gxTpr_Contratoservicos_codigo_Z;
         obj.gxTpr_Contratoservicosdepara_codigo_Z = deserialized.gxTpr_Contratoservicosdepara_codigo_Z;
         obj.gxTpr_Contratoservicosdepara_origem_Z = deserialized.gxTpr_Contratoservicosdepara_origem_Z;
         obj.gxTpr_Contratoservicosdepara_origenid_Z = deserialized.gxTpr_Contratoservicosdepara_origenid_Z;
         obj.gxTpr_Contratoservicosdepara_origenid2_Z = deserialized.gxTpr_Contratoservicosdepara_origenid2_Z;
         obj.gxTpr_Contratoservicosdepara_origenid_N = deserialized.gxTpr_Contratoservicosdepara_origenid_N;
         obj.gxTpr_Contratoservicosdepara_origemdsc_N = deserialized.gxTpr_Contratoservicosdepara_origemdsc_N;
         obj.gxTpr_Contratoservicosdepara_origenid2_N = deserialized.gxTpr_Contratoservicosdepara_origenid2_N;
         obj.gxTpr_Contratoservicosdepara_origemdsc2_N = deserialized.gxTpr_Contratoservicosdepara_origemdsc2_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_Codigo") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_Origem") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigemDsc") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId2") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2 = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigemDsc2") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoServicosDePara_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoServicosDePara_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_Codigo_Z") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_Origem_Z") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId_Z") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId2_Z") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId_N") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigemDsc_N") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigenId2_N") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicosDePara_OrigemDsc2_N") )
               {
                  gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoServicosDePara";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_Origem", StringUtil.RTrim( gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_OrigenId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_OrigemDsc", StringUtil.RTrim( gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_OrigenId2", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2), 18, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicosDePara_OrigemDsc2", StringUtil.RTrim( gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoServicosDePara_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_Origem_Z", StringUtil.RTrim( gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigenId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z), 18, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigenId2_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z), 18, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigenId_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigemDsc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigenId2_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoServicosDePara_OrigemDsc2_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicos_Codigo", gxTv_SdtContratoServicosDePara_Contratoservicos_codigo, false);
         AddObjectProperty("ContratoServicosDePara_Codigo", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo, false);
         AddObjectProperty("ContratoServicosDePara_Origem", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem, false);
         AddObjectProperty("ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid), 18, 0)), false);
         AddObjectProperty("ContratoServicosDePara_OrigemDsc", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc, false);
         AddObjectProperty("ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2), 18, 0)), false);
         AddObjectProperty("ContratoServicosDePara_OrigemDsc2", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoServicosDePara_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoServicosDePara_Initialized, false);
            AddObjectProperty("ContratoServicos_Codigo_Z", gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z, false);
            AddObjectProperty("ContratoServicosDePara_Codigo_Z", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z, false);
            AddObjectProperty("ContratoServicosDePara_Origem_Z", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z, false);
            AddObjectProperty("ContratoServicosDePara_OrigenId_Z", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z), 18, 0)), false);
            AddObjectProperty("ContratoServicosDePara_OrigenId2_Z", StringUtil.LTrim( StringUtil.Str( (decimal)(gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z), 18, 0)), false);
            AddObjectProperty("ContratoServicosDePara_OrigenId_N", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N, false);
            AddObjectProperty("ContratoServicosDePara_OrigemDsc_N", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N, false);
            AddObjectProperty("ContratoServicosDePara_OrigenId2_N", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N, false);
            AddObjectProperty("ContratoServicosDePara_OrigemDsc2_N", gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo"   )]
      public int gxTpr_Contratoservicos_codigo
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicos_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosDePara_Contratoservicos_codigo != value )
            {
               gxTv_SdtContratoServicosDePara_Mode = "INS";
               this.gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z_SetNull( );
            }
            gxTv_SdtContratoServicosDePara_Contratoservicos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosDePara_Codigo" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_Codigo"   )]
      public int gxTpr_Contratoservicosdepara_codigo
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo ;
         }

         set {
            if ( gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo != value )
            {
               gxTv_SdtContratoServicosDePara_Mode = "INS";
               this.gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z_SetNull( );
               this.gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z_SetNull( );
            }
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosDePara_Origem" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_Origem"   )]
      public String gxTpr_Contratoservicosdepara_origem
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId"   )]
      public long gxTpr_Contratoservicosdepara_origenid
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N = 0;
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N = 1;
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigemDsc" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigemDsc"   )]
      public String gxTpr_Contratoservicosdepara_origemdsc
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N = 0;
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N = 1;
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId2" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId2"   )]
      public long gxTpr_Contratoservicosdepara_origenid2
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2 ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N = 0;
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2 = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N = 1;
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2 = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigemDsc2" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigemDsc2"   )]
      public String gxTpr_Contratoservicosdepara_origemdsc2
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N = 0;
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N = 1;
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoServicosDePara_Mode ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Mode_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoServicosDePara_Initialized ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Initialized_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicos_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicos_Codigo_Z"   )]
      public int gxTpr_Contratoservicos_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_Codigo_Z" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_Codigo_Z"   )]
      public int gxTpr_Contratoservicosdepara_codigo_Z
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_Origem_Z" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_Origem_Z"   )]
      public String gxTpr_Contratoservicosdepara_origem_Z
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId_Z" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId_Z"   )]
      public long gxTpr_Contratoservicosdepara_origenid_Z
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId2_Z" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId2_Z"   )]
      public long gxTpr_Contratoservicosdepara_origenid2_Z
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z = (long)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId_N" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId_N"   )]
      public short gxTpr_Contratoservicosdepara_origenid_N
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigemDsc_N" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigemDsc_N"   )]
      public short gxTpr_Contratoservicosdepara_origemdsc_N
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigenId2_N" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigenId2_N"   )]
      public short gxTpr_Contratoservicosdepara_origenid2_N
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoServicosDePara_OrigemDsc2_N" )]
      [  XmlElement( ElementName = "ContratoServicosDePara_OrigemDsc2_N"   )]
      public short gxTpr_Contratoservicosdepara_origemdsc2_N
      {
         get {
            return gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N ;
         }

         set {
            gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N_SetNull( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 = "";
         gxTv_SdtContratoServicosDePara_Mode = "";
         gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoservicosdepara", "GeneXus.Programs.contratoservicosdepara_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoServicosDePara_Initialized ;
      private short gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_N ;
      private short gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc_N ;
      private short gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_N ;
      private short gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoServicosDePara_Contratoservicos_codigo ;
      private int gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo ;
      private int gxTv_SdtContratoServicosDePara_Contratoservicos_codigo_Z ;
      private int gxTv_SdtContratoServicosDePara_Contratoservicosdepara_codigo_Z ;
      private long gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid ;
      private long gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2 ;
      private long gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid_Z ;
      private long gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origenid2_Z ;
      private String gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem ;
      private String gxTv_SdtContratoServicosDePara_Mode ;
      private String gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origem_Z ;
      private String sTagName ;
      private String gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc ;
      private String gxTv_SdtContratoServicosDePara_Contratoservicosdepara_origemdsc2 ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoServicosDePara", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoServicosDePara_RESTInterface : GxGenericCollectionItem<SdtContratoServicosDePara>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoServicosDePara_RESTInterface( ) : base()
      {
      }

      public SdtContratoServicosDePara_RESTInterface( SdtContratoServicosDePara psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicos_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicos_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosDePara_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoservicosdepara_codigo
      {
         get {
            return sdt.gxTpr_Contratoservicosdepara_codigo ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicosDePara_Origem" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosdepara_origem
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicosdepara_origem) ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_origem = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosDePara_OrigenId" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosdepara_origenid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratoservicosdepara_origenid), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_origenid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratoServicosDePara_OrigemDsc" , Order = 4 )]
      public String gxTpr_Contratoservicosdepara_origemdsc
      {
         get {
            return sdt.gxTpr_Contratoservicosdepara_origemdsc ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_origemdsc = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicosDePara_OrigenId2" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contratoservicosdepara_origenid2
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratoservicosdepara_origenid2), 18, 0)) ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_origenid2 = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratoServicosDePara_OrigemDsc2" , Order = 6 )]
      public String gxTpr_Contratoservicosdepara_origemdsc2
      {
         get {
            return sdt.gxTpr_Contratoservicosdepara_origemdsc2 ;
         }

         set {
            sdt.gxTpr_Contratoservicosdepara_origemdsc2 = (String)(value);
         }

      }

      public SdtContratoServicosDePara sdt
      {
         get {
            return (SdtContratoServicosDePara)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoServicosDePara() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 18 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
