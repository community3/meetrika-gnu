/*
               File: ItemNaoMensuravelGeneral
        Description: Item Nao Mensuravel General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:41.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class itemnaomensuravelgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public itemnaomensuravelgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public itemnaomensuravelgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ItemNaoMensuravel_AreaTrabalhoCod ,
                           String aP1_ItemNaoMensuravel_Codigo )
      {
         this.A718ItemNaoMensuravel_AreaTrabalhoCod = aP0_ItemNaoMensuravel_AreaTrabalhoCod;
         this.A715ItemNaoMensuravel_Codigo = aP1_ItemNaoMensuravel_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynReferenciaINM_Codigo = new GXCombobox();
         cmbItemNaoMensuravel_Tipo = new GXCombobox();
         chkItemNaoMensuravel_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
                  A715ItemNaoMensuravel_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A718ItemNaoMensuravel_AreaTrabalhoCod,(String)A715ItemNaoMensuravel_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"REFERENCIAINM_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAREFERENCIAINM_CODIGOE92( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAE92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ItemNaoMensuravelGeneral";
               context.Gx_err = 0;
               GXAREFERENCIAINM_CODIGO_htmlE92( ) ;
               WSE92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Item Nao Mensuravel General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117224168");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("itemnaomensuravelgeneral.aspx") + "?" + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA715ItemNaoMensuravel_Codigo", StringUtil.RTrim( wcpOA715ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_DESCRICAO", GetSecureSignedToken( sPrefix, A714ItemNaoMensuravel_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_REFERENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ITEMNAOMENSURAVEL_ATIVO", GetSecureSignedToken( sPrefix, A716ItemNaoMensuravel_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormE92( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("itemnaomensuravelgeneral.js", "?20203117224171");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ItemNaoMensuravelGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Item Nao Mensuravel General" ;
      }

      protected void WBE90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "itemnaomensuravelgeneral.aspx");
            }
            wb_table1_2_E92( true) ;
         }
         else
         {
            wb_table1_2_E92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_E92e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTE92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Item Nao Mensuravel General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPE90( ) ;
            }
         }
      }

      protected void WSE92( )
      {
         STARTE92( ) ;
         EVTE92( ) ;
      }

      protected void EVTE92( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11E92 */
                                    E11E92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12E92 */
                                    E12E92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13E92 */
                                    E13E92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14E92 */
                                    E14E92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15E92 */
                                    E15E92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPE90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEE92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormE92( ) ;
            }
         }
      }

      protected void PAE92( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynReferenciaINM_Codigo.Name = "REFERENCIAINM_CODIGO";
            dynReferenciaINM_Codigo.WebTags = "";
            cmbItemNaoMensuravel_Tipo.Name = "ITEMNAOMENSURAVEL_TIPO";
            cmbItemNaoMensuravel_Tipo.WebTags = "";
            cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
            cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
            cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
            if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
            {
               A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
            }
            chkItemNaoMensuravel_Ativo.Name = "ITEMNAOMENSURAVEL_ATIVO";
            chkItemNaoMensuravel_Ativo.WebTags = "";
            chkItemNaoMensuravel_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkItemNaoMensuravel_Ativo_Internalname, "TitleCaption", chkItemNaoMensuravel_Ativo.Caption);
            chkItemNaoMensuravel_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAREFERENCIAINM_CODIGOE92( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAREFERENCIAINM_CODIGO_dataE92( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAREFERENCIAINM_CODIGO_htmlE92( )
      {
         int gxdynajaxvalue ;
         GXDLAREFERENCIAINM_CODIGO_dataE92( ) ;
         gxdynajaxindex = 1;
         dynReferenciaINM_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynReferenciaINM_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynReferenciaINM_Codigo.ItemCount > 0 )
         {
            A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( dynReferenciaINM_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
         }
      }

      protected void GXDLAREFERENCIAINM_CODIGO_dataE92( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00E92 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00E92_A709ReferenciaINM_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00E92_A710ReferenciaINM_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynReferenciaINM_Codigo.ItemCount > 0 )
         {
            A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( dynReferenciaINM_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
         }
         if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
         {
            A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFE92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ItemNaoMensuravelGeneral";
         context.Gx_err = 0;
      }

      protected void RFE92( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00E93 */
            pr_default.execute(1, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A716ItemNaoMensuravel_Ativo = H00E93_A716ItemNaoMensuravel_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_ATIVO", GetSecureSignedToken( sPrefix, A716ItemNaoMensuravel_Ativo));
               A717ItemNaoMensuravel_Tipo = H00E93_A717ItemNaoMensuravel_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
               A709ReferenciaINM_Codigo = H00E93_A709ReferenciaINM_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
               A1804ItemNaoMensuravel_Referencia = H00E93_A1804ItemNaoMensuravel_Referencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_REFERENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, ""))));
               n1804ItemNaoMensuravel_Referencia = H00E93_n1804ItemNaoMensuravel_Referencia[0];
               A719ItemNaoMensuravel_Valor = H00E93_A719ItemNaoMensuravel_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A714ItemNaoMensuravel_Descricao = H00E93_A714ItemNaoMensuravel_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_DESCRICAO", GetSecureSignedToken( sPrefix, A714ItemNaoMensuravel_Descricao));
               GXAREFERENCIAINM_CODIGO_htmlE92( ) ;
               /* Execute user event: E12E92 */
               E12E92 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBE90( ) ;
         }
      }

      protected void STRUPE90( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ItemNaoMensuravelGeneral";
         context.Gx_err = 0;
         GXAREFERENCIAINM_CODIGO_htmlE92( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11E92 */
         E11E92 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A714ItemNaoMensuravel_Descricao = cgiGet( edtItemNaoMensuravel_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_DESCRICAO", GetSecureSignedToken( sPrefix, A714ItemNaoMensuravel_Descricao));
            A719ItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1804ItemNaoMensuravel_Referencia = cgiGet( edtItemNaoMensuravel_Referencia_Internalname);
            n1804ItemNaoMensuravel_Referencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_REFERENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, ""))));
            dynReferenciaINM_Codigo.CurrentValue = cgiGet( dynReferenciaINM_Codigo_Internalname);
            A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( cgiGet( dynReferenciaINM_Codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
            cmbItemNaoMensuravel_Tipo.CurrentValue = cgiGet( cmbItemNaoMensuravel_Tipo_Internalname);
            A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cgiGet( cmbItemNaoMensuravel_Tipo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
            A716ItemNaoMensuravel_Ativo = StringUtil.StrToBool( cgiGet( chkItemNaoMensuravel_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ITEMNAOMENSURAVEL_ATIVO", GetSecureSignedToken( sPrefix, A716ItemNaoMensuravel_Ativo));
            /* Read saved values. */
            wcpOA718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA718ItemNaoMensuravel_AreaTrabalhoCod"), ",", "."));
            wcpOA715ItemNaoMensuravel_Codigo = cgiGet( sPrefix+"wcpOA715ItemNaoMensuravel_Codigo");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXAREFERENCIAINM_CODIGO_htmlE92( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11E92 */
         E11E92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11E92( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12E92( )
      {
         /* Load Routine */
      }

      protected void E13E92( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         context.wjLocDisableFrm = 1;
      }

      protected void E14E92( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         context.wjLocDisableFrm = 1;
      }

      protected void E15E92( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ItemNaoMensuravel";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ItemNaoMensuravel_AreaTrabalhoCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ItemNaoMensuravel_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = AV7ItemNaoMensuravel_Codigo;
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_E92( true) ;
         }
         else
         {
            wb_table2_8_E92( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_E92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_E92( true) ;
         }
         else
         {
            wb_table3_41_E92( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_E92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_E92e( true) ;
         }
         else
         {
            wb_table1_2_E92e( false) ;
         }
      }

      protected void wb_table3_41_E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_E92e( true) ;
         }
         else
         {
            wb_table3_41_E92e( false) ;
         }
      }

      protected void wb_table2_8_E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_descricao_Internalname, "Descri��o", "", "", lblTextblockitemnaomensuravel_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtItemNaoMensuravel_Descricao_Internalname, A714ItemNaoMensuravel_Descricao, "", "", 0, 1, 0, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "DescricaoLonga2M", "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_valor_Internalname, "Valor", "", "", lblTextblockitemnaomensuravel_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ",", "")), context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_referencia_Internalname, "Refer�ncia", "", "", lblTextblockitemnaomensuravel_referencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Referencia_Internalname, StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia), StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Referencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciainm_codigo_Internalname, "Guia", "", "", lblTextblockreferenciainm_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynReferenciaINM_Codigo, dynReferenciaINM_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)), 1, dynReferenciaINM_Codigo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ItemNaoMensuravelGeneral.htm");
            dynReferenciaINM_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynReferenciaINM_Codigo_Internalname, "Values", (String)(dynReferenciaINM_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_tipo_Internalname, "Tipo do Item", "", "", lblTextblockitemnaomensuravel_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbItemNaoMensuravel_Tipo, cmbItemNaoMensuravel_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)), 1, cmbItemNaoMensuravel_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ItemNaoMensuravelGeneral.htm");
            cmbItemNaoMensuravel_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbItemNaoMensuravel_Tipo_Internalname, "Values", (String)(cmbItemNaoMensuravel_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_ativo_Internalname, "Ativo", "", "", lblTextblockitemnaomensuravel_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravelGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkItemNaoMensuravel_Ativo_Internalname, StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_E92e( true) ;
         }
         else
         {
            wb_table2_8_E92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A718ItemNaoMensuravel_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         A715ItemNaoMensuravel_Codigo = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAE92( ) ;
         WSE92( ) ;
         WEE92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod = (String)((String)getParm(obj,0));
         sCtrlA715ItemNaoMensuravel_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAE92( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "itemnaomensuravelgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAE92( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            A715ItemNaoMensuravel_Codigo = (String)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         }
         wcpOA718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA718ItemNaoMensuravel_AreaTrabalhoCod"), ",", "."));
         wcpOA715ItemNaoMensuravel_Codigo = cgiGet( sPrefix+"wcpOA715ItemNaoMensuravel_Codigo");
         if ( ! GetJustCreated( ) && ( ( A718ItemNaoMensuravel_AreaTrabalhoCod != wcpOA718ItemNaoMensuravel_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A715ItemNaoMensuravel_Codigo, wcpOA715ItemNaoMensuravel_Codigo) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOA718ItemNaoMensuravel_AreaTrabalhoCod = A718ItemNaoMensuravel_AreaTrabalhoCod;
         wcpOA715ItemNaoMensuravel_Codigo = A715ItemNaoMensuravel_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod = cgiGet( sPrefix+"A718ItemNaoMensuravel_AreaTrabalhoCod_CTRL");
         if ( StringUtil.Len( sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod) > 0 )
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A718ItemNaoMensuravel_AreaTrabalhoCod_PARM"), ",", "."));
         }
         sCtrlA715ItemNaoMensuravel_Codigo = cgiGet( sPrefix+"A715ItemNaoMensuravel_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA715ItemNaoMensuravel_Codigo) > 0 )
         {
            A715ItemNaoMensuravel_Codigo = cgiGet( sCtrlA715ItemNaoMensuravel_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         }
         else
         {
            A715ItemNaoMensuravel_Codigo = cgiGet( sPrefix+"A715ItemNaoMensuravel_Codigo_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAE92( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSE92( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSE92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A718ItemNaoMensuravel_AreaTrabalhoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A718ItemNaoMensuravel_AreaTrabalhoCod_CTRL", StringUtil.RTrim( sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A715ItemNaoMensuravel_Codigo_PARM", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA715ItemNaoMensuravel_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A715ItemNaoMensuravel_Codigo_CTRL", StringUtil.RTrim( sCtrlA715ItemNaoMensuravel_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEE92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117224216");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("itemnaomensuravelgeneral.js", "?20203117224216");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockitemnaomensuravel_descricao_Internalname = sPrefix+"TEXTBLOCKITEMNAOMENSURAVEL_DESCRICAO";
         edtItemNaoMensuravel_Descricao_Internalname = sPrefix+"ITEMNAOMENSURAVEL_DESCRICAO";
         lblTextblockitemnaomensuravel_valor_Internalname = sPrefix+"TEXTBLOCKITEMNAOMENSURAVEL_VALOR";
         edtItemNaoMensuravel_Valor_Internalname = sPrefix+"ITEMNAOMENSURAVEL_VALOR";
         lblTextblockitemnaomensuravel_referencia_Internalname = sPrefix+"TEXTBLOCKITEMNAOMENSURAVEL_REFERENCIA";
         edtItemNaoMensuravel_Referencia_Internalname = sPrefix+"ITEMNAOMENSURAVEL_REFERENCIA";
         lblTextblockreferenciainm_codigo_Internalname = sPrefix+"TEXTBLOCKREFERENCIAINM_CODIGO";
         dynReferenciaINM_Codigo_Internalname = sPrefix+"REFERENCIAINM_CODIGO";
         lblTextblockitemnaomensuravel_tipo_Internalname = sPrefix+"TEXTBLOCKITEMNAOMENSURAVEL_TIPO";
         cmbItemNaoMensuravel_Tipo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_TIPO";
         lblTextblockitemnaomensuravel_ativo_Internalname = sPrefix+"TEXTBLOCKITEMNAOMENSURAVEL_ATIVO";
         chkItemNaoMensuravel_Ativo_Internalname = sPrefix+"ITEMNAOMENSURAVEL_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbItemNaoMensuravel_Tipo_Jsonclick = "";
         dynReferenciaINM_Codigo_Jsonclick = "";
         edtItemNaoMensuravel_Referencia_Jsonclick = "";
         edtItemNaoMensuravel_Valor_Jsonclick = "";
         chkItemNaoMensuravel_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13E92',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14E92',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15E92',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA715ItemNaoMensuravel_Codigo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A714ItemNaoMensuravel_Descricao = "";
         A1804ItemNaoMensuravel_Referencia = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00E92_A709ReferenciaINM_Codigo = new int[1] ;
         H00E92_A710ReferenciaINM_Descricao = new String[] {""} ;
         H00E93_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00E93_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00E93_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         H00E93_A717ItemNaoMensuravel_Tipo = new short[1] ;
         H00E93_A709ReferenciaINM_Codigo = new int[1] ;
         H00E93_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         H00E93_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         H00E93_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         H00E93_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV7ItemNaoMensuravel_Codigo = "";
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockitemnaomensuravel_descricao_Jsonclick = "";
         lblTextblockitemnaomensuravel_valor_Jsonclick = "";
         lblTextblockitemnaomensuravel_referencia_Jsonclick = "";
         lblTextblockreferenciainm_codigo_Jsonclick = "";
         lblTextblockitemnaomensuravel_tipo_Jsonclick = "";
         lblTextblockitemnaomensuravel_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod = "";
         sCtrlA715ItemNaoMensuravel_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.itemnaomensuravelgeneral__default(),
            new Object[][] {
                new Object[] {
               H00E92_A709ReferenciaINM_Codigo, H00E92_A710ReferenciaINM_Descricao
               }
               , new Object[] {
               H00E93_A718ItemNaoMensuravel_AreaTrabalhoCod, H00E93_A715ItemNaoMensuravel_Codigo, H00E93_A716ItemNaoMensuravel_Ativo, H00E93_A717ItemNaoMensuravel_Tipo, H00E93_A709ReferenciaINM_Codigo, H00E93_A1804ItemNaoMensuravel_Referencia, H00E93_n1804ItemNaoMensuravel_Referencia, H00E93_A719ItemNaoMensuravel_Valor, H00E93_A714ItemNaoMensuravel_Descricao
               }
            }
         );
         AV15Pgmname = "ItemNaoMensuravelGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ItemNaoMensuravelGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A717ItemNaoMensuravel_Tipo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int wcpOA718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private int gxdynajaxindex ;
      private int AV12ItemNaoMensuravel_AreaTrabalhoCod ;
      private int idxLst ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String wcpOA715ItemNaoMensuravel_Codigo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1804ItemNaoMensuravel_Referencia ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkItemNaoMensuravel_Ativo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtItemNaoMensuravel_Descricao_Internalname ;
      private String edtItemNaoMensuravel_Valor_Internalname ;
      private String edtItemNaoMensuravel_Referencia_Internalname ;
      private String dynReferenciaINM_Codigo_Internalname ;
      private String cmbItemNaoMensuravel_Tipo_Internalname ;
      private String AV7ItemNaoMensuravel_Codigo ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockitemnaomensuravel_descricao_Internalname ;
      private String lblTextblockitemnaomensuravel_descricao_Jsonclick ;
      private String lblTextblockitemnaomensuravel_valor_Internalname ;
      private String lblTextblockitemnaomensuravel_valor_Jsonclick ;
      private String edtItemNaoMensuravel_Valor_Jsonclick ;
      private String lblTextblockitemnaomensuravel_referencia_Internalname ;
      private String lblTextblockitemnaomensuravel_referencia_Jsonclick ;
      private String edtItemNaoMensuravel_Referencia_Jsonclick ;
      private String lblTextblockreferenciainm_codigo_Internalname ;
      private String lblTextblockreferenciainm_codigo_Jsonclick ;
      private String dynReferenciaINM_Codigo_Jsonclick ;
      private String lblTextblockitemnaomensuravel_tipo_Internalname ;
      private String lblTextblockitemnaomensuravel_tipo_Jsonclick ;
      private String cmbItemNaoMensuravel_Tipo_Jsonclick ;
      private String lblTextblockitemnaomensuravel_ativo_Internalname ;
      private String lblTextblockitemnaomensuravel_ativo_Jsonclick ;
      private String sCtrlA718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String sCtrlA715ItemNaoMensuravel_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1804ItemNaoMensuravel_Referencia ;
      private bool returnInSub ;
      private String A714ItemNaoMensuravel_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynReferenciaINM_Codigo ;
      private GXCombobox cmbItemNaoMensuravel_Tipo ;
      private GXCheckbox chkItemNaoMensuravel_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00E92_A709ReferenciaINM_Codigo ;
      private String[] H00E92_A710ReferenciaINM_Descricao ;
      private int[] H00E93_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] H00E93_A715ItemNaoMensuravel_Codigo ;
      private bool[] H00E93_A716ItemNaoMensuravel_Ativo ;
      private short[] H00E93_A717ItemNaoMensuravel_Tipo ;
      private int[] H00E93_A709ReferenciaINM_Codigo ;
      private String[] H00E93_A1804ItemNaoMensuravel_Referencia ;
      private bool[] H00E93_n1804ItemNaoMensuravel_Referencia ;
      private decimal[] H00E93_A719ItemNaoMensuravel_Valor ;
      private String[] H00E93_A714ItemNaoMensuravel_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class itemnaomensuravelgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00E92 ;
          prmH00E92 = new Object[] {
          } ;
          Object[] prmH00E93 ;
          prmH00E93 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00E92", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) ORDER BY [ReferenciaINM_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E92,0,0,true,false )
             ,new CursorDef("H00E93", "SELECT [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_Ativo], [ItemNaoMensuravel_Tipo], [ReferenciaINM_Codigo], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Descricao] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E93,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((String[]) buf[8])[0] = rslt.getLongVarchar(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
