/*
               File: CriarCiclosCrr
        Description: Criar Ciclos Crr
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:7.65
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class acriarcicloscrr : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public acriarcicloscrr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public acriarcicloscrr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         acriarcicloscrr objacriarcicloscrr;
         objacriarcicloscrr = new acriarcicloscrr();
         objacriarcicloscrr.context.SetSubmitInitialConfig(context);
         objacriarcicloscrr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objacriarcicloscrr);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((acriarcicloscrr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
            /* Using cursor P00XI2 */
            pr_default.execute(0, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A516Contratada_TipoFabrica = P00XI2_A516Contratada_TipoFabrica[0];
               A52Contratada_AreaTrabalhoCod = P00XI2_A52Contratada_AreaTrabalhoCod[0];
               A39Contratada_Codigo = P00XI2_A39Contratada_Codigo[0];
               AV9Contratadas.Add(A39Contratada_Codigo, 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A490ContagemResultado_ContratadaCod ,
                                                 AV9Contratadas ,
                                                 A484ContagemResultado_StatusDmn ,
                                                 A471ContagemResultado_DataDmn },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE
                                                 }
            });
            /* Using cursor P00XI3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A471ContagemResultado_DataDmn = P00XI3_A471ContagemResultado_DataDmn[0];
               A484ContagemResultado_StatusDmn = P00XI3_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00XI3_n484ContagemResultado_StatusDmn[0];
               A490ContagemResultado_ContratadaCod = P00XI3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00XI3_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00XI3_A456ContagemResultado_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P00XI3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00XI3_n1553ContagemResultado_CntSrvCod[0];
               A493ContagemResultado_DemandaFM = P00XI3_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00XI3_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = P00XI3_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P00XI3_n457ContagemResultado_Demanda[0];
               if ( A471ContagemResultado_DataDmn >= context.localUtil.CToD( "01/01/2019", 2) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV10Codigo = A456ContagemResultado_Codigo;
                  AV11ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
                  HXI0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), 8, Gx_line+0, 47, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 67, Gx_line+0, 224, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, "")), 242, Gx_line+0, 254, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  /* Execute user subroutine: 'CICLOSRETORNADA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(1);
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'CICLOSCORRECAO' */
                  S121 ();
                  if ( returnInSub )
                  {
                     pr_default.close(1);
                     this.cleanup();
                     if (true) return;
                  }
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            context.CommitDataStores( "CriarCiclosCrr");
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HXI0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CICLOSRETORNADA' Routine */
         AV16Retornada = false;
         /* Using cursor P00XI4 */
         pr_default.execute(2, new Object[] {AV10Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1234LogResponsavel_NovoStatus = P00XI4_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = P00XI4_n1234LogResponsavel_NovoStatus[0];
            A893LogResponsavel_DataHora = P00XI4_A893LogResponsavel_DataHora[0];
            A1553ContagemResultado_CntSrvCod = P00XI4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XI4_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00XI4_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XI4_n1611ContagemResultado_PrzTpDias[0];
            A1797LogResponsavel_Codigo = P00XI4_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P00XI4_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P00XI4_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00XI4_n892LogResponsavel_DemandaCod[0];
            A1553ContagemResultado_CntSrvCod = P00XI4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XI4_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00XI4_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XI4_n1611ContagemResultado_PrzTpDias[0];
            GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
            if ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "D") == 0 )
            {
               AV16Retornada = A1149LogResponsavel_OwnerEhContratante;
               if ( AV16Retornada )
               {
                  AV13PrazoEntrega = A893LogResponsavel_DataHora;
                  GXt_int2 = AV12Dias;
                  new prc_diasparacorrecao(context ).execute( ref  A1553ContagemResultado_CntSrvCod,  A892LogResponsavel_DemandaCod, out  GXt_int2) ;
                  AV12Dias = GXt_int2;
                  GXt_dtime3 = AV13PrazoEntrega;
                  new prc_adddiasuteis(context ).execute(  AV13PrazoEntrega,  AV12Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime3) ;
                  AV13PrazoEntrega = GXt_dtime3;
                  new prc_novocicloexecucao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora,  AV13PrazoEntrega,  A1611ContagemResultado_PrzTpDias,  true) ;
                  AV17Nome = "Novo ciclo " + context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ") + "-" + context.localUtil.TToC( AV13PrazoEntrega, 8, 5, 0, 3, "/", ":", " ");
                  HXI0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Nome, "@!")), 67, Gx_line+0, 328, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
               }
               else
               {
                  new prc_encerrarciclocorrecao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora) ;
                  AV17Nome = "Encerra ciclo " + context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ");
                  HXI0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Nome, "@!")), 67, Gx_line+0, 328, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
               }
            }
            else if ( AV16Retornada && ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "R") == 0 ) )
            {
               AV16Retornada = false;
               AV17Nome = "Encerra ciclo " + context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ");
               new prc_encerrarciclocorrecao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora) ;
               HXI0( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Nome, "@!")), 67, Gx_line+0, 328, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S121( )
      {
         /* 'CICLOSCORRECAO' Routine */
         AV15Count = 1;
         /* Using cursor P00XI5 */
         pr_default.execute(3, new Object[] {AV10Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A894LogResponsavel_Acao = P00XI5_A894LogResponsavel_Acao[0];
            A892LogResponsavel_DemandaCod = P00XI5_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00XI5_n892LogResponsavel_DemandaCod[0];
            A893LogResponsavel_DataHora = P00XI5_A893LogResponsavel_DataHora[0];
            A1553ContagemResultado_CntSrvCod = P00XI5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XI5_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00XI5_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XI5_n1611ContagemResultado_PrzTpDias[0];
            A1797LogResponsavel_Codigo = P00XI5_A1797LogResponsavel_Codigo[0];
            A1553ContagemResultado_CntSrvCod = P00XI5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XI5_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00XI5_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XI5_n1611ContagemResultado_PrzTpDias[0];
            if ( ( AV15Count /  ( decimal )( 2 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV15Count/ (decimal)(2))) )) )
            {
               if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "N") == 0 )
               {
                  AV13PrazoEntrega = A893LogResponsavel_DataHora;
                  GXt_int2 = AV12Dias;
                  new prc_diasparacorrecao(context ).execute( ref  A1553ContagemResultado_CntSrvCod,  A892LogResponsavel_DemandaCod, out  GXt_int2) ;
                  AV12Dias = GXt_int2;
                  GXt_dtime3 = AV13PrazoEntrega;
                  new prc_adddiasuteis(context ).execute(  AV13PrazoEntrega,  AV12Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime3) ;
                  AV13PrazoEntrega = GXt_dtime3;
                  new prc_novocicloexecucao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora,  AV13PrazoEntrega,  A1611ContagemResultado_PrzTpDias,  true) ;
                  AV17Nome = "Novo ciclo " + context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ") + "-" + context.localUtil.TToC( AV13PrazoEntrega, 8, 5, 0, 3, "/", ":", " ");
                  HXI0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Nome, "@!")), 67, Gx_line+0, 328, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
               }
               else
               {
                  new prc_encerrarciclocorrecao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora) ;
                  AV17Nome = "Encerra ciclo " + context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ");
                  HXI0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Nome, "@!")), 67, Gx_line+0, 328, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
               }
            }
            AV15Count = (short)(AV15Count+1);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void HXI0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00XI2_A516Contratada_TipoFabrica = new String[] {""} ;
         P00XI2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00XI2_A39Contratada_Codigo = new int[1] ;
         A516Contratada_TipoFabrica = "";
         AV9Contratadas = new GxSimpleCollection();
         A484ContagemResultado_StatusDmn = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         P00XI3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00XI3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00XI3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00XI3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00XI3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00XI3_A456ContagemResultado_Codigo = new int[1] ;
         P00XI3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XI3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XI3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00XI3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00XI3_A457ContagemResultado_Demanda = new String[] {""} ;
         P00XI3_n457ContagemResultado_Demanda = new bool[] {false} ;
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A501ContagemResultado_OsFsOsFm = "";
         P00XI4_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P00XI4_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P00XI4_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00XI4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XI4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XI4_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00XI4_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00XI4_A1797LogResponsavel_Codigo = new long[1] ;
         P00XI4_A896LogResponsavel_Owner = new int[1] ;
         P00XI4_A892LogResponsavel_DemandaCod = new int[1] ;
         P00XI4_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A1234LogResponsavel_NovoStatus = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV17Nome = "";
         P00XI5_A894LogResponsavel_Acao = new String[] {""} ;
         P00XI5_A892LogResponsavel_DemandaCod = new int[1] ;
         P00XI5_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00XI5_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00XI5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XI5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XI5_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00XI5_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00XI5_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.acriarcicloscrr__default(),
            new Object[][] {
                new Object[] {
               P00XI2_A516Contratada_TipoFabrica, P00XI2_A52Contratada_AreaTrabalhoCod, P00XI2_A39Contratada_Codigo
               }
               , new Object[] {
               P00XI3_A471ContagemResultado_DataDmn, P00XI3_A484ContagemResultado_StatusDmn, P00XI3_n484ContagemResultado_StatusDmn, P00XI3_A490ContagemResultado_ContratadaCod, P00XI3_n490ContagemResultado_ContratadaCod, P00XI3_A456ContagemResultado_Codigo, P00XI3_A1553ContagemResultado_CntSrvCod, P00XI3_n1553ContagemResultado_CntSrvCod, P00XI3_A493ContagemResultado_DemandaFM, P00XI3_n493ContagemResultado_DemandaFM,
               P00XI3_A457ContagemResultado_Demanda, P00XI3_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P00XI4_A1234LogResponsavel_NovoStatus, P00XI4_n1234LogResponsavel_NovoStatus, P00XI4_A893LogResponsavel_DataHora, P00XI4_A1553ContagemResultado_CntSrvCod, P00XI4_n1553ContagemResultado_CntSrvCod, P00XI4_A1611ContagemResultado_PrzTpDias, P00XI4_n1611ContagemResultado_PrzTpDias, P00XI4_A1797LogResponsavel_Codigo, P00XI4_A896LogResponsavel_Owner, P00XI4_A892LogResponsavel_DemandaCod,
               P00XI4_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00XI5_A894LogResponsavel_Acao, P00XI5_A892LogResponsavel_DemandaCod, P00XI5_n892LogResponsavel_DemandaCod, P00XI5_A893LogResponsavel_DataHora, P00XI5_A1553ContagemResultado_CntSrvCod, P00XI5_n1553ContagemResultado_CntSrvCod, P00XI5_A1611ContagemResultado_PrzTpDias, P00XI5_n1611ContagemResultado_PrzTpDias, P00XI5_A1797LogResponsavel_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV12Dias ;
      private short AV15Count ;
      private short GXt_int2 ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV10Codigo ;
      private int AV11ContratoServicos_Codigo ;
      private int Gx_OldLine ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1234LogResponsavel_NovoStatus ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV17Nome ;
      private String A894LogResponsavel_Acao ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime AV13PrazoEntrega ;
      private DateTime GXt_dtime3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool returnInSub ;
      private bool AV16Retornada ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A501ContagemResultado_OsFsOsFm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00XI2_A516Contratada_TipoFabrica ;
      private int[] P00XI2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00XI2_A39Contratada_Codigo ;
      private DateTime[] P00XI3_A471ContagemResultado_DataDmn ;
      private String[] P00XI3_A484ContagemResultado_StatusDmn ;
      private bool[] P00XI3_n484ContagemResultado_StatusDmn ;
      private int[] P00XI3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00XI3_n490ContagemResultado_ContratadaCod ;
      private int[] P00XI3_A456ContagemResultado_Codigo ;
      private int[] P00XI3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XI3_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XI3_A493ContagemResultado_DemandaFM ;
      private bool[] P00XI3_n493ContagemResultado_DemandaFM ;
      private String[] P00XI3_A457ContagemResultado_Demanda ;
      private bool[] P00XI3_n457ContagemResultado_Demanda ;
      private String[] P00XI4_A1234LogResponsavel_NovoStatus ;
      private bool[] P00XI4_n1234LogResponsavel_NovoStatus ;
      private DateTime[] P00XI4_A893LogResponsavel_DataHora ;
      private int[] P00XI4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XI4_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XI4_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00XI4_n1611ContagemResultado_PrzTpDias ;
      private long[] P00XI4_A1797LogResponsavel_Codigo ;
      private int[] P00XI4_A896LogResponsavel_Owner ;
      private int[] P00XI4_A892LogResponsavel_DemandaCod ;
      private bool[] P00XI4_n892LogResponsavel_DemandaCod ;
      private String[] P00XI5_A894LogResponsavel_Acao ;
      private int[] P00XI5_A892LogResponsavel_DemandaCod ;
      private bool[] P00XI5_n892LogResponsavel_DemandaCod ;
      private DateTime[] P00XI5_A893LogResponsavel_DataHora ;
      private int[] P00XI5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XI5_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XI5_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00XI5_n1611ContagemResultado_PrzTpDias ;
      private long[] P00XI5_A1797LogResponsavel_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9Contratadas ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class acriarcicloscrr__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00XI3( IGxContext context ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV9Contratadas ,
                                             String A484ContagemResultado_StatusDmn ,
                                             DateTime A471ContagemResultado_DataDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_DataDmn], [ContagemResultado_StatusDmn], [ContagemResultado_ContratadaCod], [ContagemResultado_Codigo], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_DemandaFM], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Contratadas, "[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([ContagemResultado_StatusDmn] IN ('A','D','H','R'))";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_StatusDmn]";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00XI3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XI2 ;
          prmP00XI2 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XI4 ;
          prmP00XI4 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XI5 ;
          prmP00XI5 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XI3 ;
          prmP00XI3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XI2", "SELECT [Contratada_TipoFabrica], [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo) AND ([Contratada_TipoFabrica] = 'M') ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XI2,100,0,false,false )
             ,new CursorDef("P00XI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XI3,100,0,true,false )
             ,new CursorDef("P00XI4", "SELECT T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_DataHora], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Owner], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV10Codigo) AND (T1.[LogResponsavel_NovoStatus] IN ('D','R')) ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XI4,100,0,true,false )
             ,new CursorDef("P00XI5", "SELECT T1.[LogResponsavel_Acao], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_DataHora], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[LogResponsavel_Codigo] FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV10Codigo) AND (T1.[LogResponsavel_Acao] IN ('N','T','RN')) ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XI5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((long[]) buf[7])[0] = rslt.getLong(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((long[]) buf[8])[0] = rslt.getLong(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
