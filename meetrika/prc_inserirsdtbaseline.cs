/*
               File: PRC_InserirSDTBaseline
        Description: Stub for PRC_InserirSDTBaseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:56.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inserirsdtbaseline : GXProcedure
   {
      public prc_inserirsdtbaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_inserirsdtbaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           String aP1_Sistema_Sigla ,
                           String aP2_FileName ,
                           String aP3_Aba ,
                           ref bool aP4_Criar )
      {
         this.AV2Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV3Sistema_Sigla = aP1_Sistema_Sigla;
         this.AV4FileName = aP2_FileName;
         this.AV5Aba = aP3_Aba;
         this.AV6Criar = aP4_Criar;
         initialize();
         executePrivate();
         aP4_Criar=this.AV6Criar;
      }

      public bool executeUdp( int aP0_Sistema_Codigo ,
                              String aP1_Sistema_Sigla ,
                              String aP2_FileName ,
                              String aP3_Aba )
      {
         this.AV2Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV3Sistema_Sigla = aP1_Sistema_Sigla;
         this.AV4FileName = aP2_FileName;
         this.AV5Aba = aP3_Aba;
         this.AV6Criar = aP4_Criar;
         initialize();
         executePrivate();
         aP4_Criar=this.AV6Criar;
         return AV6Criar ;
      }

      public void executeSubmit( int aP0_Sistema_Codigo ,
                                 String aP1_Sistema_Sigla ,
                                 String aP2_FileName ,
                                 String aP3_Aba ,
                                 ref bool aP4_Criar )
      {
         prc_inserirsdtbaseline objprc_inserirsdtbaseline;
         objprc_inserirsdtbaseline = new prc_inserirsdtbaseline();
         objprc_inserirsdtbaseline.AV2Sistema_Codigo = aP0_Sistema_Codigo;
         objprc_inserirsdtbaseline.AV3Sistema_Sigla = aP1_Sistema_Sigla;
         objprc_inserirsdtbaseline.AV4FileName = aP2_FileName;
         objprc_inserirsdtbaseline.AV5Aba = aP3_Aba;
         objprc_inserirsdtbaseline.AV6Criar = aP4_Criar;
         objprc_inserirsdtbaseline.context.SetSubmitInitialConfig(context);
         objprc_inserirsdtbaseline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inserirsdtbaseline);
         aP4_Criar=this.AV6Criar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inserirsdtbaseline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Sistema_Codigo,(String)AV3Sistema_Sigla,(String)AV4FileName,(String)AV5Aba,(bool)AV6Criar} ;
         ClassLoader.Execute("aprc_inserirsdtbaseline","GeneXus.Programs.aprc_inserirsdtbaseline", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 5 ) )
         {
            AV6Criar = (bool)(args[4]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2Sistema_Codigo ;
      private String AV3Sistema_Sigla ;
      private String AV4FileName ;
      private String AV5Aba ;
      private bool AV6Criar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP4_Criar ;
      private Object[] args ;
   }

}
