/*
               File: GetContratoContratoObrigacaoWCFilterData
        Description: Get Contrato Contrato Obrigacao WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/28/2019 16:48:35.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratoobrigacaowcfilterdata : GXProcedure
   {
      public getcontratocontratoobrigacaowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratoobrigacaowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratoobrigacaowcfilterdata objgetcontratocontratoobrigacaowcfilterdata;
         objgetcontratocontratoobrigacaowcfilterdata = new getcontratocontratoobrigacaowcfilterdata();
         objgetcontratocontratoobrigacaowcfilterdata.AV20DDOName = aP0_DDOName;
         objgetcontratocontratoobrigacaowcfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetcontratocontratoobrigacaowcfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratoobrigacaowcfilterdata.AV24OptionsJson = "" ;
         objgetcontratocontratoobrigacaowcfilterdata.AV27OptionsDescJson = "" ;
         objgetcontratocontratoobrigacaowcfilterdata.AV29OptionIndexesJson = "" ;
         objgetcontratocontratoobrigacaowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratoobrigacaowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratoobrigacaowcfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratoobrigacaowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATOOBRIGACAO_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATOOBRIGACAO_SUBITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOBRIGACAO_SUBITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATOOBRIGACAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOBRIGACAO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("ContratoContratoObrigacaoWCGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoObrigacaoWCGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("ContratoContratoObrigacaoWCGridState"), "");
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM") == 0 )
            {
               AV10TFContratoObrigacao_Item = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM_SEL") == 0 )
            {
               AV11TFContratoObrigacao_Item_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_SUBITEM") == 0 )
            {
               AV12TFContratoObrigacao_SubItem = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_SUBITEM_SEL") == 0 )
            {
               AV13TFContratoObrigacao_SubItem_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_DATAMAXIMA") == 0 )
            {
               AV14TFContratoObrigacao_DataMaxima = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContratoObrigacao_DataMaxima_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_DESCRICAO") == 0 )
            {
               AV16TFContratoObrigacao_Descricao = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_DESCRICAO_SEL") == 0 )
            {
               AV17TFContratoObrigacao_Descricao_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV36Contrato_Codigo = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' Routine */
         AV10TFContratoObrigacao_Item = AV18SearchTxt;
         AV11TFContratoObrigacao_Item_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoObrigacao_Item_Sel ,
                                              AV10TFContratoObrigacao_Item ,
                                              AV13TFContratoObrigacao_SubItem_Sel ,
                                              AV12TFContratoObrigacao_SubItem ,
                                              AV14TFContratoObrigacao_DataMaxima ,
                                              AV15TFContratoObrigacao_DataMaxima_To ,
                                              AV17TFContratoObrigacao_Descricao_Sel ,
                                              AV16TFContratoObrigacao_Descricao ,
                                              A322ContratoObrigacao_Item ,
                                              A1425ContratoObrigacao_SubItem ,
                                              A1424ContratoObrigacao_DataMaxima ,
                                              A323ContratoObrigacao_Descricao ,
                                              AV36Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoObrigacao_Item = StringUtil.Concat( StringUtil.RTrim( AV10TFContratoObrigacao_Item), "%", "");
         lV12TFContratoObrigacao_SubItem = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoObrigacao_SubItem), "%", "");
         lV16TFContratoObrigacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoObrigacao_Descricao), "%", "");
         /* Using cursor P00UH2 */
         pr_default.execute(0, new Object[] {AV36Contrato_Codigo, lV10TFContratoObrigacao_Item, AV11TFContratoObrigacao_Item_Sel, lV12TFContratoObrigacao_SubItem, AV13TFContratoObrigacao_SubItem_Sel, AV14TFContratoObrigacao_DataMaxima, AV15TFContratoObrigacao_DataMaxima_To, lV16TFContratoObrigacao_Descricao, AV17TFContratoObrigacao_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUH2 = false;
            A74Contrato_Codigo = P00UH2_A74Contrato_Codigo[0];
            A322ContratoObrigacao_Item = P00UH2_A322ContratoObrigacao_Item[0];
            A323ContratoObrigacao_Descricao = P00UH2_A323ContratoObrigacao_Descricao[0];
            A1424ContratoObrigacao_DataMaxima = P00UH2_A1424ContratoObrigacao_DataMaxima[0];
            n1424ContratoObrigacao_DataMaxima = P00UH2_n1424ContratoObrigacao_DataMaxima[0];
            A1425ContratoObrigacao_SubItem = P00UH2_A1425ContratoObrigacao_SubItem[0];
            n1425ContratoObrigacao_SubItem = P00UH2_n1425ContratoObrigacao_SubItem[0];
            A321ContratoObrigacao_Codigo = P00UH2_A321ContratoObrigacao_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UH2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UH2_A322ContratoObrigacao_Item[0], A322ContratoObrigacao_Item) == 0 ) )
            {
               BRKUH2 = false;
               A321ContratoObrigacao_Codigo = P00UH2_A321ContratoObrigacao_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKUH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A322ContratoObrigacao_Item)) )
            {
               AV22Option = A322ContratoObrigacao_Item;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUH2 )
            {
               BRKUH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOBRIGACAO_SUBITEMOPTIONS' Routine */
         AV12TFContratoObrigacao_SubItem = AV18SearchTxt;
         AV13TFContratoObrigacao_SubItem_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFContratoObrigacao_Item_Sel ,
                                              AV10TFContratoObrigacao_Item ,
                                              AV13TFContratoObrigacao_SubItem_Sel ,
                                              AV12TFContratoObrigacao_SubItem ,
                                              AV14TFContratoObrigacao_DataMaxima ,
                                              AV15TFContratoObrigacao_DataMaxima_To ,
                                              AV17TFContratoObrigacao_Descricao_Sel ,
                                              AV16TFContratoObrigacao_Descricao ,
                                              A322ContratoObrigacao_Item ,
                                              A1425ContratoObrigacao_SubItem ,
                                              A1424ContratoObrigacao_DataMaxima ,
                                              A323ContratoObrigacao_Descricao ,
                                              AV36Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoObrigacao_Item = StringUtil.Concat( StringUtil.RTrim( AV10TFContratoObrigacao_Item), "%", "");
         lV12TFContratoObrigacao_SubItem = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoObrigacao_SubItem), "%", "");
         lV16TFContratoObrigacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoObrigacao_Descricao), "%", "");
         /* Using cursor P00UH3 */
         pr_default.execute(1, new Object[] {AV36Contrato_Codigo, lV10TFContratoObrigacao_Item, AV11TFContratoObrigacao_Item_Sel, lV12TFContratoObrigacao_SubItem, AV13TFContratoObrigacao_SubItem_Sel, AV14TFContratoObrigacao_DataMaxima, AV15TFContratoObrigacao_DataMaxima_To, lV16TFContratoObrigacao_Descricao, AV17TFContratoObrigacao_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUH4 = false;
            A74Contrato_Codigo = P00UH3_A74Contrato_Codigo[0];
            A1425ContratoObrigacao_SubItem = P00UH3_A1425ContratoObrigacao_SubItem[0];
            n1425ContratoObrigacao_SubItem = P00UH3_n1425ContratoObrigacao_SubItem[0];
            A323ContratoObrigacao_Descricao = P00UH3_A323ContratoObrigacao_Descricao[0];
            A1424ContratoObrigacao_DataMaxima = P00UH3_A1424ContratoObrigacao_DataMaxima[0];
            n1424ContratoObrigacao_DataMaxima = P00UH3_n1424ContratoObrigacao_DataMaxima[0];
            A322ContratoObrigacao_Item = P00UH3_A322ContratoObrigacao_Item[0];
            A321ContratoObrigacao_Codigo = P00UH3_A321ContratoObrigacao_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UH3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UH3_A1425ContratoObrigacao_SubItem[0], A1425ContratoObrigacao_SubItem) == 0 ) )
            {
               BRKUH4 = false;
               A321ContratoObrigacao_Codigo = P00UH3_A321ContratoObrigacao_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKUH4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1425ContratoObrigacao_SubItem)) )
            {
               AV22Option = A1425ContratoObrigacao_SubItem;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUH4 )
            {
               BRKUH4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOOBRIGACAO_DESCRICAOOPTIONS' Routine */
         AV16TFContratoObrigacao_Descricao = AV18SearchTxt;
         AV17TFContratoObrigacao_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV11TFContratoObrigacao_Item_Sel ,
                                              AV10TFContratoObrigacao_Item ,
                                              AV13TFContratoObrigacao_SubItem_Sel ,
                                              AV12TFContratoObrigacao_SubItem ,
                                              AV14TFContratoObrigacao_DataMaxima ,
                                              AV15TFContratoObrigacao_DataMaxima_To ,
                                              AV17TFContratoObrigacao_Descricao_Sel ,
                                              AV16TFContratoObrigacao_Descricao ,
                                              A322ContratoObrigacao_Item ,
                                              A1425ContratoObrigacao_SubItem ,
                                              A1424ContratoObrigacao_DataMaxima ,
                                              A323ContratoObrigacao_Descricao ,
                                              AV36Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoObrigacao_Item = StringUtil.Concat( StringUtil.RTrim( AV10TFContratoObrigacao_Item), "%", "");
         lV12TFContratoObrigacao_SubItem = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoObrigacao_SubItem), "%", "");
         lV16TFContratoObrigacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoObrigacao_Descricao), "%", "");
         /* Using cursor P00UH4 */
         pr_default.execute(2, new Object[] {AV36Contrato_Codigo, lV10TFContratoObrigacao_Item, AV11TFContratoObrigacao_Item_Sel, lV12TFContratoObrigacao_SubItem, AV13TFContratoObrigacao_SubItem_Sel, AV14TFContratoObrigacao_DataMaxima, AV15TFContratoObrigacao_DataMaxima_To, lV16TFContratoObrigacao_Descricao, AV17TFContratoObrigacao_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A323ContratoObrigacao_Descricao = P00UH4_A323ContratoObrigacao_Descricao[0];
            A1424ContratoObrigacao_DataMaxima = P00UH4_A1424ContratoObrigacao_DataMaxima[0];
            n1424ContratoObrigacao_DataMaxima = P00UH4_n1424ContratoObrigacao_DataMaxima[0];
            A1425ContratoObrigacao_SubItem = P00UH4_A1425ContratoObrigacao_SubItem[0];
            n1425ContratoObrigacao_SubItem = P00UH4_n1425ContratoObrigacao_SubItem[0];
            A322ContratoObrigacao_Item = P00UH4_A322ContratoObrigacao_Item[0];
            A74Contrato_Codigo = P00UH4_A74Contrato_Codigo[0];
            A321ContratoObrigacao_Codigo = P00UH4_A321ContratoObrigacao_Codigo[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A323ContratoObrigacao_Descricao)) )
            {
               AV22Option = A323ContratoObrigacao_Descricao;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               if ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) == 0 ) )
               {
                  AV30count = (long)(NumberUtil.Val( ((String)AV28OptionIndexes.Item(AV21InsertIndex)), "."));
                  AV30count = (long)(AV30count+1);
                  AV28OptionIndexes.RemoveItem(AV21InsertIndex);
                  AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
               }
               else
               {
                  AV23Options.Add(AV22Option, AV21InsertIndex);
                  AV28OptionIndexes.Add("1", AV21InsertIndex);
               }
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoObrigacao_Item = "";
         AV11TFContratoObrigacao_Item_Sel = "";
         AV12TFContratoObrigacao_SubItem = "";
         AV13TFContratoObrigacao_SubItem_Sel = "";
         AV14TFContratoObrigacao_DataMaxima = DateTime.MinValue;
         AV15TFContratoObrigacao_DataMaxima_To = DateTime.MinValue;
         AV16TFContratoObrigacao_Descricao = "";
         AV17TFContratoObrigacao_Descricao_Sel = "";
         scmdbuf = "";
         lV10TFContratoObrigacao_Item = "";
         lV12TFContratoObrigacao_SubItem = "";
         lV16TFContratoObrigacao_Descricao = "";
         A322ContratoObrigacao_Item = "";
         A1425ContratoObrigacao_SubItem = "";
         A1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
         A323ContratoObrigacao_Descricao = "";
         P00UH2_A74Contrato_Codigo = new int[1] ;
         P00UH2_A322ContratoObrigacao_Item = new String[] {""} ;
         P00UH2_A323ContratoObrigacao_Descricao = new String[] {""} ;
         P00UH2_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         P00UH2_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         P00UH2_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         P00UH2_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         P00UH2_A321ContratoObrigacao_Codigo = new int[1] ;
         AV22Option = "";
         P00UH3_A74Contrato_Codigo = new int[1] ;
         P00UH3_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         P00UH3_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         P00UH3_A323ContratoObrigacao_Descricao = new String[] {""} ;
         P00UH3_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         P00UH3_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         P00UH3_A322ContratoObrigacao_Item = new String[] {""} ;
         P00UH3_A321ContratoObrigacao_Codigo = new int[1] ;
         P00UH4_A323ContratoObrigacao_Descricao = new String[] {""} ;
         P00UH4_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         P00UH4_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         P00UH4_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         P00UH4_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         P00UH4_A322ContratoObrigacao_Item = new String[] {""} ;
         P00UH4_A74Contrato_Codigo = new int[1] ;
         P00UH4_A321ContratoObrigacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratoobrigacaowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UH2_A74Contrato_Codigo, P00UH2_A322ContratoObrigacao_Item, P00UH2_A323ContratoObrigacao_Descricao, P00UH2_A1424ContratoObrigacao_DataMaxima, P00UH2_n1424ContratoObrigacao_DataMaxima, P00UH2_A1425ContratoObrigacao_SubItem, P00UH2_n1425ContratoObrigacao_SubItem, P00UH2_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               P00UH3_A74Contrato_Codigo, P00UH3_A1425ContratoObrigacao_SubItem, P00UH3_n1425ContratoObrigacao_SubItem, P00UH3_A323ContratoObrigacao_Descricao, P00UH3_A1424ContratoObrigacao_DataMaxima, P00UH3_n1424ContratoObrigacao_DataMaxima, P00UH3_A322ContratoObrigacao_Item, P00UH3_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               P00UH4_A323ContratoObrigacao_Descricao, P00UH4_A1424ContratoObrigacao_DataMaxima, P00UH4_n1424ContratoObrigacao_DataMaxima, P00UH4_A1425ContratoObrigacao_SubItem, P00UH4_n1425ContratoObrigacao_SubItem, P00UH4_A322ContratoObrigacao_Item, P00UH4_A74Contrato_Codigo, P00UH4_A321ContratoObrigacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV39GXV1 ;
      private int AV36Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A321ContratoObrigacao_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String scmdbuf ;
      private DateTime AV14TFContratoObrigacao_DataMaxima ;
      private DateTime AV15TFContratoObrigacao_DataMaxima_To ;
      private DateTime A1424ContratoObrigacao_DataMaxima ;
      private bool returnInSub ;
      private bool BRKUH2 ;
      private bool n1424ContratoObrigacao_DataMaxima ;
      private bool n1425ContratoObrigacao_SubItem ;
      private bool BRKUH4 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String A323ContratoObrigacao_Descricao ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFContratoObrigacao_Item ;
      private String AV11TFContratoObrigacao_Item_Sel ;
      private String AV12TFContratoObrigacao_SubItem ;
      private String AV13TFContratoObrigacao_SubItem_Sel ;
      private String AV16TFContratoObrigacao_Descricao ;
      private String AV17TFContratoObrigacao_Descricao_Sel ;
      private String lV10TFContratoObrigacao_Item ;
      private String lV12TFContratoObrigacao_SubItem ;
      private String lV16TFContratoObrigacao_Descricao ;
      private String A322ContratoObrigacao_Item ;
      private String A1425ContratoObrigacao_SubItem ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UH2_A74Contrato_Codigo ;
      private String[] P00UH2_A322ContratoObrigacao_Item ;
      private String[] P00UH2_A323ContratoObrigacao_Descricao ;
      private DateTime[] P00UH2_A1424ContratoObrigacao_DataMaxima ;
      private bool[] P00UH2_n1424ContratoObrigacao_DataMaxima ;
      private String[] P00UH2_A1425ContratoObrigacao_SubItem ;
      private bool[] P00UH2_n1425ContratoObrigacao_SubItem ;
      private int[] P00UH2_A321ContratoObrigacao_Codigo ;
      private int[] P00UH3_A74Contrato_Codigo ;
      private String[] P00UH3_A1425ContratoObrigacao_SubItem ;
      private bool[] P00UH3_n1425ContratoObrigacao_SubItem ;
      private String[] P00UH3_A323ContratoObrigacao_Descricao ;
      private DateTime[] P00UH3_A1424ContratoObrigacao_DataMaxima ;
      private bool[] P00UH3_n1424ContratoObrigacao_DataMaxima ;
      private String[] P00UH3_A322ContratoObrigacao_Item ;
      private int[] P00UH3_A321ContratoObrigacao_Codigo ;
      private String[] P00UH4_A323ContratoObrigacao_Descricao ;
      private DateTime[] P00UH4_A1424ContratoObrigacao_DataMaxima ;
      private bool[] P00UH4_n1424ContratoObrigacao_DataMaxima ;
      private String[] P00UH4_A1425ContratoObrigacao_SubItem ;
      private bool[] P00UH4_n1425ContratoObrigacao_SubItem ;
      private String[] P00UH4_A322ContratoObrigacao_Item ;
      private int[] P00UH4_A74Contrato_Codigo ;
      private int[] P00UH4_A321ContratoObrigacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
   }

   public class getcontratocontratoobrigacaowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UH2( IGxContext context ,
                                             String AV11TFContratoObrigacao_Item_Sel ,
                                             String AV10TFContratoObrigacao_Item ,
                                             String AV13TFContratoObrigacao_SubItem_Sel ,
                                             String AV12TFContratoObrigacao_SubItem ,
                                             DateTime AV14TFContratoObrigacao_DataMaxima ,
                                             DateTime AV15TFContratoObrigacao_DataMaxima_To ,
                                             String AV17TFContratoObrigacao_Descricao_Sel ,
                                             String AV16TFContratoObrigacao_Descricao ,
                                             String A322ContratoObrigacao_Item ,
                                             String A1425ContratoObrigacao_SubItem ,
                                             DateTime A1424ContratoObrigacao_DataMaxima ,
                                             String A323ContratoObrigacao_Descricao ,
                                             int AV36Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_Descricao], [ContratoObrigacao_DataMaxima], [ContratoObrigacao_SubItem], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV36Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoObrigacao_Item)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] like @lV10TFContratoObrigacao_Item)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] = @AV11TFContratoObrigacao_Item_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoObrigacao_SubItem)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] like @lV12TFContratoObrigacao_SubItem)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] = @AV13TFContratoObrigacao_SubItem_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoObrigacao_DataMaxima) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] >= @AV14TFContratoObrigacao_DataMaxima)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoObrigacao_DataMaxima_To) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] <= @AV15TFContratoObrigacao_DataMaxima_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoObrigacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] like @lV16TFContratoObrigacao_Descricao)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] = @AV17TFContratoObrigacao_Descricao_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoObrigacao_Item]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UH3( IGxContext context ,
                                             String AV11TFContratoObrigacao_Item_Sel ,
                                             String AV10TFContratoObrigacao_Item ,
                                             String AV13TFContratoObrigacao_SubItem_Sel ,
                                             String AV12TFContratoObrigacao_SubItem ,
                                             DateTime AV14TFContratoObrigacao_DataMaxima ,
                                             DateTime AV15TFContratoObrigacao_DataMaxima_To ,
                                             String AV17TFContratoObrigacao_Descricao_Sel ,
                                             String AV16TFContratoObrigacao_Descricao ,
                                             String A322ContratoObrigacao_Item ,
                                             String A1425ContratoObrigacao_SubItem ,
                                             DateTime A1424ContratoObrigacao_DataMaxima ,
                                             String A323ContratoObrigacao_Descricao ,
                                             int AV36Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [9] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoObrigacao_SubItem], [ContratoObrigacao_Descricao], [ContratoObrigacao_DataMaxima], [ContratoObrigacao_Item], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV36Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoObrigacao_Item)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] like @lV10TFContratoObrigacao_Item)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] = @AV11TFContratoObrigacao_Item_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoObrigacao_SubItem)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] like @lV12TFContratoObrigacao_SubItem)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] = @AV13TFContratoObrigacao_SubItem_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoObrigacao_DataMaxima) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] >= @AV14TFContratoObrigacao_DataMaxima)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoObrigacao_DataMaxima_To) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] <= @AV15TFContratoObrigacao_DataMaxima_To)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoObrigacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] like @lV16TFContratoObrigacao_Descricao)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] = @AV17TFContratoObrigacao_Descricao_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoObrigacao_SubItem]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UH4( IGxContext context ,
                                             String AV11TFContratoObrigacao_Item_Sel ,
                                             String AV10TFContratoObrigacao_Item ,
                                             String AV13TFContratoObrigacao_SubItem_Sel ,
                                             String AV12TFContratoObrigacao_SubItem ,
                                             DateTime AV14TFContratoObrigacao_DataMaxima ,
                                             DateTime AV15TFContratoObrigacao_DataMaxima_To ,
                                             String AV17TFContratoObrigacao_Descricao_Sel ,
                                             String AV16TFContratoObrigacao_Descricao ,
                                             String A322ContratoObrigacao_Item ,
                                             String A1425ContratoObrigacao_SubItem ,
                                             DateTime A1424ContratoObrigacao_DataMaxima ,
                                             String A323ContratoObrigacao_Descricao ,
                                             int AV36Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [9] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [ContratoObrigacao_Descricao], [ContratoObrigacao_DataMaxima], [ContratoObrigacao_SubItem], [ContratoObrigacao_Item], [Contrato_Codigo], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV36Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoObrigacao_Item)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] like @lV10TFContratoObrigacao_Item)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoObrigacao_Item_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Item] = @AV11TFContratoObrigacao_Item_Sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoObrigacao_SubItem)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] like @lV12TFContratoObrigacao_SubItem)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_SubItem_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_SubItem] = @AV13TFContratoObrigacao_SubItem_Sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoObrigacao_DataMaxima) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] >= @AV14TFContratoObrigacao_DataMaxima)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoObrigacao_DataMaxima_To) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_DataMaxima] <= @AV15TFContratoObrigacao_DataMaxima_To)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoObrigacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] like @lV16TFContratoObrigacao_Descricao)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoObrigacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoObrigacao_Descricao] = @AV17TFContratoObrigacao_Descricao_Sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
               case 1 :
                     return conditional_P00UH3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
               case 2 :
                     return conditional_P00UH4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (DateTime)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UH2 ;
          prmP00UH2 = new Object[] {
          new Object[] {"@AV36Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV11TFContratoObrigacao_Item_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV12TFContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFContratoObrigacao_SubItem_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV14TFContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContratoObrigacao_DataMaxima_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContratoObrigacao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoObrigacao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00UH3 ;
          prmP00UH3 = new Object[] {
          new Object[] {"@AV36Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV11TFContratoObrigacao_Item_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV12TFContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFContratoObrigacao_SubItem_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV14TFContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContratoObrigacao_DataMaxima_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContratoObrigacao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoObrigacao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00UH4 ;
          prmP00UH4 = new Object[] {
          new Object[] {"@AV36Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV11TFContratoObrigacao_Item_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV12TFContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFContratoObrigacao_SubItem_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV14TFContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContratoObrigacao_DataMaxima_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContratoObrigacao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoObrigacao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UH2,100,0,true,false )
             ,new CursorDef("P00UH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UH3,100,0,true,false )
             ,new CursorDef("P00UH4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UH4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratoobrigacaowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratoobrigacaowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratoobrigacaowcfilterdata") )
          {
             return  ;
          }
          getcontratocontratoobrigacaowcfilterdata worker = new getcontratocontratoobrigacaowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
