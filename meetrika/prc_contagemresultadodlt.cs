/*
               File: PRC_ContagemResultadoDlt
        Description: Delete Contagem Resultado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:46.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultadodlt : GXProcedure
   {
      public prc_contagemresultadodlt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultadodlt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_contagemresultadodlt objprc_contagemresultadodlt;
         objprc_contagemresultadodlt = new prc_contagemresultadodlt();
         objprc_contagemresultadodlt.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contagemresultadodlt.context.SetSubmitInitialConfig(context);
         objprc_contagemresultadodlt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultadodlt);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultadodlt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00392 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00393 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            /* End optimized DELETE. */
            /* Using cursor P00394 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ContagemResultadoDlt");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00392_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultadodlt__default(),
            new Object[][] {
                new Object[] {
               P00392_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00392_A456ContagemResultado_Codigo ;
   }

   public class prc_contagemresultadodlt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00392 ;
          prmP00392 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00393 ;
          prmP00393 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00394 ;
          prmP00394 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00392", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00392,1,0,true,true )
             ,new CursorDef("P00393", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00393)
             ,new CursorDef("P00394", "DELETE FROM [ContagemResultado]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00394)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
