/*
               File: CheckListContagemResultadoChckLstLogWC
        Description: Check List Contagem Resultado Chck Lst Log WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:36.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class checklistcontagemresultadochcklstlogwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public checklistcontagemresultadochcklstlogwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public checklistcontagemresultadochcklstlogwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultadoChckLstLog_ChckLstCod )
      {
         this.AV7ContagemResultadoChckLstLog_ChckLstCod = aP0_ContagemResultadoChckLstLog_ChckLstCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultadoChckLstLog_ChckLstCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContagemResultadoChckLstLog_ChckLstCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_105 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_105_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_105_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18ContagemResultadoChckLstLog_DataHora1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
                  AV19ContagemResultadoChckLstLog_DataHora_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
                  AV20ContagemResultadoChckLstLog_UsuarioNome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
                  AV22DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
                  AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV24ContagemResultadoChckLstLog_DataHora2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
                  AV25ContagemResultadoChckLstLog_DataHora_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
                  AV26ContagemResultadoChckLstLog_UsuarioNome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
                  AV28DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
                  AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                  AV30ContagemResultadoChckLstLog_DataHora3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
                  AV31ContagemResultadoChckLstLog_DataHora_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
                  AV32ContagemResultadoChckLstLog_UsuarioNome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
                  AV21DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
                  AV27DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
                  AV7ContagemResultadoChckLstLog_ChckLstCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
                  AV46Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV34DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
                  AV33DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
                  A820ContagemResultadoChckLstLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A822ContagemResultadoChckLstLog_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAO72( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV46Pgmname = "CheckListContagemResultadoChckLstLogWC";
               Gx_date = DateTimeUtil.Today( context);
               context.Gx_err = 0;
               WSO72( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Check List Contagem Resultado Chck Lst Log WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117233690");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("checklistcontagemresultadochcklstlogwc.aspx") + "?" + UrlEncode("" +AV7ContagemResultadoChckLstLog_ChckLstCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1", StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV22DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2", StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV28DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3", StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV21DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV27DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_105", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_105), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV46Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV34DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV33DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormO72( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("checklistcontagemresultadochcklstlogwc.js", "?20203117233737");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "CheckListContagemResultadoChckLstLogWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List Contagem Resultado Chck Lst Log WC" ;
      }

      protected void WBO70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "checklistcontagemresultadochcklstlogwc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_O72( true) ;
         }
         else
         {
            wb_table1_2_O72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_ChckLstCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoChckLstLog_ChckLstCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(120, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV27DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(121, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"");
         }
         wbLoad = true;
      }

      protected void STARTO72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Check List Contagem Resultado Chck Lst Log WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPO70( ) ;
            }
         }
      }

      protected void WSO72( )
      {
         STARTO72( ) ;
         EVTO72( ) ;
      }

      protected void EVTO72( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11O72 */
                                    E11O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12O72 */
                                    E12O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13O72 */
                                    E13O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14O72 */
                                    E14O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15O72 */
                                    E15O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16O72 */
                                    E16O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17O72 */
                                    E17O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18O72 */
                                    E18O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19O72 */
                                    E19O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20O72 */
                                    E20O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21O72 */
                                    E21O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22O72 */
                                    E22O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23O72 */
                                    E23O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24O72 */
                                    E24O72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO70( ) ;
                              }
                              nGXsfl_105_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_105_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_105_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1052( ) ;
                              AV36Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV43Update_GXI : context.convertURL( context.PathToRelativeUrl( AV36Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV44Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              A820ContagemResultadoChckLstLog_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Codigo_Internalname), ",", "."));
                              A814ContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoChckLstLog_DataHora_Internalname), 0);
                              A812ContagemResultadoChckLstLog_ChckLstDes = cgiGet( edtContagemResultadoChckLstLog_ChckLstDes_Internalname);
                              n812ContagemResultadoChckLstLog_ChckLstDes = false;
                              A822ContagemResultadoChckLstLog_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_UsuarioCod_Internalname), ",", "."));
                              A823ContagemResultadoChckLstLog_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_PessoaCod_Internalname), ",", "."));
                              n823ContagemResultadoChckLstLog_PessoaCod = false;
                              A817ContagemResultadoChckLstLog_UsuarioNome = StringUtil.Upper( cgiGet( edtContagemResultadoChckLstLog_UsuarioNome_Internalname));
                              n817ContagemResultadoChckLstLog_UsuarioNome = false;
                              A824ContagemResultadoChckLstLog_Etapa = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Etapa_Internalname), ",", "."));
                              n824ContagemResultadoChckLstLog_Etapa = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25O72 */
                                          E25O72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26O72 */
                                          E26O72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E27O72 */
                                          E27O72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora1 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1"), 0) != AV18ContagemResultadoChckLstLog_DataHora1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora_to1 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1"), 0) != AV19ContagemResultadoChckLstLog_DataHora_To1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_usuarionome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1"), AV20ContagemResultadoChckLstLog_UsuarioNome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora2 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2"), 0) != AV24ContagemResultadoChckLstLog_DataHora2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora_to2 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2"), 0) != AV25ContagemResultadoChckLstLog_DataHora_To2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_usuarionome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2"), AV26ContagemResultadoChckLstLog_UsuarioNome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora3 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3"), 0) != AV30ContagemResultadoChckLstLog_DataHora3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_datahora_to3 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3"), 0) != AV31ContagemResultadoChckLstLog_DataHora_To3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemresultadochcklstlog_usuarionome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3"), AV32ContagemResultadoChckLstLog_UsuarioNome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPO70( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEO72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormO72( ) ;
            }
         }
      }

      protected void PAO72( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "Per�odo", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1052( ) ;
         while ( nGXsfl_105_idx <= nRC_GXsfl_105 )
         {
            sendrow_1052( ) ;
            nGXsfl_105_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_105_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_105_idx+1));
            sGXsfl_105_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_105_idx), 4, 0)), 4, "0");
            SubsflControlProps_1052( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       DateTime AV18ContagemResultadoChckLstLog_DataHora1 ,
                                       DateTime AV19ContagemResultadoChckLstLog_DataHora_To1 ,
                                       String AV20ContagemResultadoChckLstLog_UsuarioNome1 ,
                                       String AV22DynamicFiltersSelector2 ,
                                       short AV23DynamicFiltersOperator2 ,
                                       DateTime AV24ContagemResultadoChckLstLog_DataHora2 ,
                                       DateTime AV25ContagemResultadoChckLstLog_DataHora_To2 ,
                                       String AV26ContagemResultadoChckLstLog_UsuarioNome2 ,
                                       String AV28DynamicFiltersSelector3 ,
                                       short AV29DynamicFiltersOperator3 ,
                                       DateTime AV30ContagemResultadoChckLstLog_DataHora3 ,
                                       DateTime AV31ContagemResultadoChckLstLog_DataHora_To3 ,
                                       String AV32ContagemResultadoChckLstLog_UsuarioNome3 ,
                                       bool AV21DynamicFiltersEnabled2 ,
                                       bool AV27DynamicFiltersEnabled3 ,
                                       int AV7ContagemResultadoChckLstLog_ChckLstCod ,
                                       String AV46Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV34DynamicFiltersIgnoreFirst ,
                                       bool AV33DynamicFiltersRemoving ,
                                       int A820ContagemResultadoChckLstLog_Codigo ,
                                       int A822ContagemResultadoChckLstLog_UsuarioCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFO72( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_ETAPA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A824ContagemResultadoChckLstLog_Etapa), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFO72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV46Pgmname = "CheckListContagemResultadoChckLstLogWC";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFO72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 105;
         /* Execute user event: E26O72 */
         E26O72 ();
         nGXsfl_105_idx = 1;
         sGXsfl_105_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_105_idx), 4, 0)), 4, "0");
         SubsflControlProps_1052( ) ;
         nGXsfl_105_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1052( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18ContagemResultadoChckLstLog_DataHora1 ,
                                                 AV19ContagemResultadoChckLstLog_DataHora_To1 ,
                                                 AV20ContagemResultadoChckLstLog_UsuarioNome1 ,
                                                 AV21DynamicFiltersEnabled2 ,
                                                 AV22DynamicFiltersSelector2 ,
                                                 AV23DynamicFiltersOperator2 ,
                                                 AV24ContagemResultadoChckLstLog_DataHora2 ,
                                                 AV25ContagemResultadoChckLstLog_DataHora_To2 ,
                                                 AV26ContagemResultadoChckLstLog_UsuarioNome2 ,
                                                 AV27DynamicFiltersEnabled3 ,
                                                 AV28DynamicFiltersSelector3 ,
                                                 AV29DynamicFiltersOperator3 ,
                                                 AV30ContagemResultadoChckLstLog_DataHora3 ,
                                                 AV31ContagemResultadoChckLstLog_DataHora_To3 ,
                                                 AV32ContagemResultadoChckLstLog_UsuarioNome3 ,
                                                 A814ContagemResultadoChckLstLog_DataHora ,
                                                 A817ContagemResultadoChckLstLog_UsuarioNome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A811ContagemResultadoChckLstLog_ChckLstCod ,
                                                 AV7ContagemResultadoChckLstLog_ChckLstCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV20ContagemResultadoChckLstLog_UsuarioNome1 = StringUtil.PadR( StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
            lV20ContagemResultadoChckLstLog_UsuarioNome1 = StringUtil.PadR( StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
            lV26ContagemResultadoChckLstLog_UsuarioNome2 = StringUtil.PadR( StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
            lV26ContagemResultadoChckLstLog_UsuarioNome2 = StringUtil.PadR( StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
            lV32ContagemResultadoChckLstLog_UsuarioNome3 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
            lV32ContagemResultadoChckLstLog_UsuarioNome3 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
            /* Using cursor H00O72 */
            pr_default.execute(0, new Object[] {AV7ContagemResultadoChckLstLog_ChckLstCod, AV18ContagemResultadoChckLstLog_DataHora1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV19ContagemResultadoChckLstLog_DataHora_To1, lV20ContagemResultadoChckLstLog_UsuarioNome1, lV20ContagemResultadoChckLstLog_UsuarioNome1, AV24ContagemResultadoChckLstLog_DataHora2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV25ContagemResultadoChckLstLog_DataHora_To2, lV26ContagemResultadoChckLstLog_UsuarioNome2, lV26ContagemResultadoChckLstLog_UsuarioNome2, AV30ContagemResultadoChckLstLog_DataHora3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV31ContagemResultadoChckLstLog_DataHora_To3, lV32ContagemResultadoChckLstLog_UsuarioNome3, lV32ContagemResultadoChckLstLog_UsuarioNome3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_105_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A811ContagemResultadoChckLstLog_ChckLstCod = H00O72_A811ContagemResultadoChckLstLog_ChckLstCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
               n811ContagemResultadoChckLstLog_ChckLstCod = H00O72_n811ContagemResultadoChckLstLog_ChckLstCod[0];
               A824ContagemResultadoChckLstLog_Etapa = H00O72_A824ContagemResultadoChckLstLog_Etapa[0];
               n824ContagemResultadoChckLstLog_Etapa = H00O72_n824ContagemResultadoChckLstLog_Etapa[0];
               A817ContagemResultadoChckLstLog_UsuarioNome = H00O72_A817ContagemResultadoChckLstLog_UsuarioNome[0];
               n817ContagemResultadoChckLstLog_UsuarioNome = H00O72_n817ContagemResultadoChckLstLog_UsuarioNome[0];
               A823ContagemResultadoChckLstLog_PessoaCod = H00O72_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = H00O72_n823ContagemResultadoChckLstLog_PessoaCod[0];
               A822ContagemResultadoChckLstLog_UsuarioCod = H00O72_A822ContagemResultadoChckLstLog_UsuarioCod[0];
               A812ContagemResultadoChckLstLog_ChckLstDes = H00O72_A812ContagemResultadoChckLstLog_ChckLstDes[0];
               n812ContagemResultadoChckLstLog_ChckLstDes = H00O72_n812ContagemResultadoChckLstLog_ChckLstDes[0];
               A814ContagemResultadoChckLstLog_DataHora = H00O72_A814ContagemResultadoChckLstLog_DataHora[0];
               A820ContagemResultadoChckLstLog_Codigo = H00O72_A820ContagemResultadoChckLstLog_Codigo[0];
               A812ContagemResultadoChckLstLog_ChckLstDes = H00O72_A812ContagemResultadoChckLstLog_ChckLstDes[0];
               n812ContagemResultadoChckLstLog_ChckLstDes = H00O72_n812ContagemResultadoChckLstLog_ChckLstDes[0];
               A823ContagemResultadoChckLstLog_PessoaCod = H00O72_A823ContagemResultadoChckLstLog_PessoaCod[0];
               n823ContagemResultadoChckLstLog_PessoaCod = H00O72_n823ContagemResultadoChckLstLog_PessoaCod[0];
               A817ContagemResultadoChckLstLog_UsuarioNome = H00O72_A817ContagemResultadoChckLstLog_UsuarioNome[0];
               n817ContagemResultadoChckLstLog_UsuarioNome = H00O72_n817ContagemResultadoChckLstLog_UsuarioNome[0];
               /* Execute user event: E27O72 */
               E27O72 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 105;
            WBO70( ) ;
         }
         nGXsfl_105_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18ContagemResultadoChckLstLog_DataHora1 ,
                                              AV19ContagemResultadoChckLstLog_DataHora_To1 ,
                                              AV20ContagemResultadoChckLstLog_UsuarioNome1 ,
                                              AV21DynamicFiltersEnabled2 ,
                                              AV22DynamicFiltersSelector2 ,
                                              AV23DynamicFiltersOperator2 ,
                                              AV24ContagemResultadoChckLstLog_DataHora2 ,
                                              AV25ContagemResultadoChckLstLog_DataHora_To2 ,
                                              AV26ContagemResultadoChckLstLog_UsuarioNome2 ,
                                              AV27DynamicFiltersEnabled3 ,
                                              AV28DynamicFiltersSelector3 ,
                                              AV29DynamicFiltersOperator3 ,
                                              AV30ContagemResultadoChckLstLog_DataHora3 ,
                                              AV31ContagemResultadoChckLstLog_DataHora_To3 ,
                                              AV32ContagemResultadoChckLstLog_UsuarioNome3 ,
                                              A814ContagemResultadoChckLstLog_DataHora ,
                                              A817ContagemResultadoChckLstLog_UsuarioNome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A811ContagemResultadoChckLstLog_ChckLstCod ,
                                              AV7ContagemResultadoChckLstLog_ChckLstCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV20ContagemResultadoChckLstLog_UsuarioNome1 = StringUtil.PadR( StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
         lV20ContagemResultadoChckLstLog_UsuarioNome1 = StringUtil.PadR( StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
         lV26ContagemResultadoChckLstLog_UsuarioNome2 = StringUtil.PadR( StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
         lV26ContagemResultadoChckLstLog_UsuarioNome2 = StringUtil.PadR( StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
         lV32ContagemResultadoChckLstLog_UsuarioNome3 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
         lV32ContagemResultadoChckLstLog_UsuarioNome3 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
         /* Using cursor H00O73 */
         pr_default.execute(1, new Object[] {AV7ContagemResultadoChckLstLog_ChckLstCod, AV18ContagemResultadoChckLstLog_DataHora1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV19ContagemResultadoChckLstLog_DataHora_To1, lV20ContagemResultadoChckLstLog_UsuarioNome1, lV20ContagemResultadoChckLstLog_UsuarioNome1, AV24ContagemResultadoChckLstLog_DataHora2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV25ContagemResultadoChckLstLog_DataHora_To2, lV26ContagemResultadoChckLstLog_UsuarioNome2, lV26ContagemResultadoChckLstLog_UsuarioNome2, AV30ContagemResultadoChckLstLog_DataHora3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV31ContagemResultadoChckLstLog_DataHora_To3, lV32ContagemResultadoChckLstLog_UsuarioNome3, lV32ContagemResultadoChckLstLog_UsuarioNome3});
         GRID_nRecordCount = H00O73_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPO70( )
      {
         /* Before Start, stand alone formulas. */
         AV46Pgmname = "CheckListContagemResultadoChckLstLogWC";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25O72 */
         E25O72 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora1"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContagemResultadoChckLstLog_DataHora1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18ContagemResultadoChckLstLog_DataHora1 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora_To1"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContagemResultadoChckLstLog_DataHora_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV19ContagemResultadoChckLstLog_DataHora_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            AV20ContagemResultadoChckLstLog_UsuarioNome1 = StringUtil.Upper( cgiGet( edtavContagemresultadochcklstlog_usuarionome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV22DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora2"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultadoChckLstLog_DataHora2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24ContagemResultadoChckLstLog_DataHora2 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora_To2"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContagemResultadoChckLstLog_DataHora_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV25ContagemResultadoChckLstLog_DataHora_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            AV26ContagemResultadoChckLstLog_UsuarioNome2 = StringUtil.Upper( cgiGet( edtavContagemresultadochcklstlog_usuarionome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV28DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora3"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContagemResultadoChckLstLog_DataHora3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV30ContagemResultadoChckLstLog_DataHora3 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadochcklstlog_datahora_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Chck Lst Log_Data Hora_To3"}), 1, "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3");
               GX_FocusControl = edtavContagemresultadochcklstlog_datahora_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31ContagemResultadoChckLstLog_DataHora_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV31ContagemResultadoChckLstLog_DataHora_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadochcklstlog_datahora_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV32ContagemResultadoChckLstLog_UsuarioNome3 = StringUtil.Upper( cgiGet( edtavContagemresultadochcklstlog_usuarionome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
            A811ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_ChckLstCod_Internalname), ",", "."));
            n811ContagemResultadoChckLstLog_ChckLstCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
            AV21DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
            AV27DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_105 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_105"), ",", "."));
            AV39GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV40GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultadoChckLstLog_ChckLstCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1"), 0) != AV18ContagemResultadoChckLstLog_DataHora1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1"), 0) != AV19ContagemResultadoChckLstLog_DataHora_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1"), AV20ContagemResultadoChckLstLog_UsuarioNome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2"), 0) != AV24ContagemResultadoChckLstLog_DataHora2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2"), 0) != AV25ContagemResultadoChckLstLog_DataHora_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2"), AV26ContagemResultadoChckLstLog_UsuarioNome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3"), 0) != AV30ContagemResultadoChckLstLog_DataHora3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3"), 0) != AV31ContagemResultadoChckLstLog_DataHora_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3"), AV32ContagemResultadoChckLstLog_UsuarioNome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25O72 */
         E25O72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25O72( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector2 = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersSelector3 = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtContagemResultadoChckLstLog_ChckLstCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_ChckLstCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_ChckLstCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Id", 0);
         cmbavOrderedby.addItem("3", "List", 0);
         cmbavOrderedby.addItem("4", "Id", 0);
         cmbavOrderedby.addItem("5", "Id", 0);
         cmbavOrderedby.addItem("6", "Usu�rio", 0);
         cmbavOrderedby.addItem("7", "Lst Log_Etapa", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E26O72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Passado", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Hoje", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "No futuro", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV27DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Passado", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Hoje", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "No futuro", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultadoChckLstLog_Codigo_Titleformat = 2;
         edtContagemResultadoChckLstLog_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Id", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_Codigo_Internalname, "Title", edtContagemResultadoChckLstLog_Codigo_Title);
         edtContagemResultadoChckLstLog_DataHora_Titleformat = 2;
         edtContagemResultadoChckLstLog_DataHora_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Data", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_DataHora_Internalname, "Title", edtContagemResultadoChckLstLog_DataHora_Title);
         edtContagemResultadoChckLstLog_ChckLstDes_Titleformat = 2;
         edtContagemResultadoChckLstLog_ChckLstDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "List", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_ChckLstDes_Internalname, "Title", edtContagemResultadoChckLstLog_ChckLstDes_Title);
         edtContagemResultadoChckLstLog_UsuarioCod_Titleformat = 2;
         edtContagemResultadoChckLstLog_UsuarioCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Id", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_UsuarioCod_Internalname, "Title", edtContagemResultadoChckLstLog_UsuarioCod_Title);
         edtContagemResultadoChckLstLog_PessoaCod_Titleformat = 2;
         edtContagemResultadoChckLstLog_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Id", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_PessoaCod_Internalname, "Title", edtContagemResultadoChckLstLog_PessoaCod_Title);
         edtContagemResultadoChckLstLog_UsuarioNome_Titleformat = 2;
         edtContagemResultadoChckLstLog_UsuarioNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_UsuarioNome_Internalname, "Title", edtContagemResultadoChckLstLog_UsuarioNome_Title);
         edtContagemResultadoChckLstLog_Etapa_Titleformat = 2;
         edtContagemResultadoChckLstLog_Etapa_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Lst Log_Etapa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoChckLstLog_Etapa_Internalname, "Title", edtContagemResultadoChckLstLog_Etapa_Title);
         AV39GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39GridCurrentPage), 10, 0)));
         AV40GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11O72( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV38PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV38PageToGo) ;
         }
      }

      private void E27O72( )
      {
         /* Grid_Load Routine */
         AV36Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV36Update);
         AV43Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contagemresultadochcklstlog.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A820ContagemResultadoChckLstLog_Codigo);
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV35Delete);
         AV44Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contagemresultadochcklstlog.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A820ContagemResultadoChckLstLog_Codigo);
         edtContagemResultadoChckLstLog_UsuarioNome_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A822ContagemResultadoChckLstLog_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 105;
         }
         sendrow_1052( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_105_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(105, GridRow);
         }
      }

      protected void E12O72( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17O72( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV21DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
      }

      protected void E13O72( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18O72( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19O72( )
      {
         /* Dynamicfiltersoperator1_Click Routine */
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            AV18ContagemResultadoChckLstLog_DataHora1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            AV19ContagemResultadoChckLstLog_DataHora_To1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1OPERATORVALUES' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
      }

      protected void E20O72( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV27DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
      }

      protected void E14O72( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21O72( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV23DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E22O72( )
      {
         /* Dynamicfiltersoperator2_Click Routine */
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            AV24ContagemResultadoChckLstLog_DataHora2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            AV25ContagemResultadoChckLstLog_DataHora_To2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2OPERATORVALUES' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
      }

      protected void E15O72( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23O72( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV29DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E24O72( )
      {
         /* Dynamicfiltersoperator3_Click Routine */
         if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            AV30ContagemResultadoChckLstLog_DataHora3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            AV31ContagemResultadoChckLstLog_DataHora_To3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3OPERATORVALUES' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContagemResultadoChckLstLog_DataHora1, AV19ContagemResultadoChckLstLog_DataHora_To1, AV20ContagemResultadoChckLstLog_UsuarioNome1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24ContagemResultadoChckLstLog_DataHora2, AV25ContagemResultadoChckLstLog_DataHora_To2, AV26ContagemResultadoChckLstLog_UsuarioNome2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30ContagemResultadoChckLstLog_DataHora3, AV31ContagemResultadoChckLstLog_DataHora_To3, AV32ContagemResultadoChckLstLog_UsuarioNome3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7ContagemResultadoChckLstLog_ChckLstCod, AV46Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A820ContagemResultadoChckLstLog_Codigo, A822ContagemResultadoChckLstLog_UsuarioCod, sPrefix) ;
         }
      }

      protected void E16O72( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contagemresultadochcklstlog.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible), 5, 0)));
         edtavContagemresultadochcklstlog_usuarionome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1OPERATORVALUES' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
         {
            edtavContagemresultadochcklstlog_usuarionome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1OPERATORVALUES' Routine */
         cellContagemresultadochcklstlog_datahora_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell1_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell1_Class);
         cellContagemresultadochcklstlog_datahora_to_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell1_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell1_Class);
         lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class);
         if ( AV17DynamicFiltersOperator1 == 0 )
         {
            AV18ContagemResultadoChckLstLog_DataHora1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV17DynamicFiltersOperator1 == 1 )
         {
            AV18ContagemResultadoChckLstLog_DataHora1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
            AV19ContagemResultadoChckLstLog_DataHora_To1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV17DynamicFiltersOperator1 == 2 )
         {
            AV18ContagemResultadoChckLstLog_DataHora1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV17DynamicFiltersOperator1 == 3 )
         {
            cellContagemresultadochcklstlog_datahora_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell1_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell1_Class);
            cellContagemresultadochcklstlog_datahora_to_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell1_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell1_Class);
            lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible), 5, 0)));
         edtavContagemresultadochcklstlog_usuarionome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2OPERATORVALUES' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
         {
            edtavContagemresultadochcklstlog_usuarionome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2OPERATORVALUES' Routine */
         cellContagemresultadochcklstlog_datahora_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell2_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell2_Class);
         cellContagemresultadochcklstlog_datahora_to_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell2_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell2_Class);
         lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class);
         if ( AV23DynamicFiltersOperator2 == 0 )
         {
            AV24ContagemResultadoChckLstLog_DataHora2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV23DynamicFiltersOperator2 == 1 )
         {
            AV24ContagemResultadoChckLstLog_DataHora2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
            AV25ContagemResultadoChckLstLog_DataHora_To2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV23DynamicFiltersOperator2 == 2 )
         {
            AV24ContagemResultadoChckLstLog_DataHora2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV23DynamicFiltersOperator2 == 3 )
         {
            cellContagemresultadochcklstlog_datahora_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell2_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell2_Class);
            cellContagemresultadochcklstlog_datahora_to_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell2_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell2_Class);
            lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class);
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible), 5, 0)));
         edtavContagemresultadochcklstlog_usuarionome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3OPERATORVALUES' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
         {
            edtavContagemresultadochcklstlog_usuarionome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultadochcklstlog_usuarionome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadochcklstlog_usuarionome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'UPDATECONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3OPERATORVALUES' Routine */
         cellContagemresultadochcklstlog_datahora_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell3_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell3_Class);
         cellContagemresultadochcklstlog_datahora_to_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell3_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell3_Class);
         lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class);
         if ( AV29DynamicFiltersOperator3 == 0 )
         {
            AV30ContagemResultadoChckLstLog_DataHora3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV29DynamicFiltersOperator3 == 1 )
         {
            AV30ContagemResultadoChckLstLog_DataHora3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
            AV31ContagemResultadoChckLstLog_DataHora_To3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV29DynamicFiltersOperator3 == 2 )
         {
            AV30ContagemResultadoChckLstLog_DataHora3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV29DynamicFiltersOperator3 == 3 )
         {
            cellContagemresultadochcklstlog_datahora_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_cell3_Internalname, "Class", cellContagemresultadochcklstlog_datahora_cell3_Class);
            cellContagemresultadochcklstlog_datahora_to_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellContagemresultadochcklstlog_datahora_to_cell3_Internalname, "Class", cellContagemresultadochcklstlog_datahora_to_cell3_Class);
            lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Internalname, "Class", lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class);
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         AV22DynamicFiltersSelector2 = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         AV23DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         AV24ContagemResultadoChckLstLog_DataHora2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
         AV25ContagemResultadoChckLstLog_DataHora_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         AV28DynamicFiltersSelector3 = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         AV29DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         AV30ContagemResultadoChckLstLog_DataHora3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
         AV31ContagemResultadoChckLstLog_DataHora_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get(AV46Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV46Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV37Session.Get(AV46Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContagemResultadoChckLstLog_DataHora1 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContagemResultadoChckLstLog_DataHora1", context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " "));
               AV19ContagemResultadoChckLstLog_DataHora_To1 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContagemResultadoChckLstLog_DataHora_To1", context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV20ContagemResultadoChckLstLog_UsuarioNome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContagemResultadoChckLstLog_UsuarioNome1", AV20ContagemResultadoChckLstLog_UsuarioNome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV21DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV22DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
               {
                  AV23DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV24ContagemResultadoChckLstLog_DataHora2 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContagemResultadoChckLstLog_DataHora2", context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " "));
                  AV25ContagemResultadoChckLstLog_DataHora_To2 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContagemResultadoChckLstLog_DataHora_To2", context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
               {
                  AV23DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV26ContagemResultadoChckLstLog_UsuarioNome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContagemResultadoChckLstLog_UsuarioNome2", AV26ContagemResultadoChckLstLog_UsuarioNome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV27DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV28DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 )
                  {
                     AV29DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                     AV30ContagemResultadoChckLstLog_DataHora3 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ContagemResultadoChckLstLog_DataHora3", context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " "));
                     AV31ContagemResultadoChckLstLog_DataHora_To3 = context.localUtil.CToT( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoChckLstLog_DataHora_To3", context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 )
                  {
                     AV29DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                     AV32ContagemResultadoChckLstLog_UsuarioNome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ContagemResultadoChckLstLog_UsuarioNome3", AV32ContagemResultadoChckLstLog_UsuarioNome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV33DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV37Session.Get(AV46Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV46Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV34DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV18ContagemResultadoChckLstLog_DataHora1) && (DateTime.MinValue==AV19ContagemResultadoChckLstLog_DataHora_To1) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 8, 5, 0, 3, "/", ":", " ");
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV20ContagemResultadoChckLstLog_UsuarioNome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV24ContagemResultadoChckLstLog_DataHora2) && (DateTime.MinValue==AV25ContagemResultadoChckLstLog_DataHora_To2) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 8, 5, 0, 3, "/", ":", " ");
               AV13GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator2;
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26ContagemResultadoChckLstLog_UsuarioNome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator2;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV27DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV28DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ! ( (DateTime.MinValue==AV30ContagemResultadoChckLstLog_DataHora3) && (DateTime.MinValue==AV31ContagemResultadoChckLstLog_DataHora_To3) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 8, 5, 0, 3, "/", ":", " ");
               AV13GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator3;
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32ContagemResultadoChckLstLog_UsuarioNome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator3;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV46Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoChckLstLog";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoChckLstLog_ChckLstCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV37Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_O72( true) ;
         }
         else
         {
            wb_table2_8_O72( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_102_O72( true) ;
         }
         else
         {
            wb_table3_102_O72( false) ;
         }
         return  ;
      }

      protected void wb_table3_102_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_O72e( true) ;
         }
         else
         {
            wb_table1_2_O72e( false) ;
         }
      }

      protected void wb_table3_102_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"105\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_ChckLstDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_ChckLstDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_ChckLstDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_UsuarioNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_UsuarioNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_UsuarioNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoChckLstLog_Etapa_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoChckLstLog_Etapa_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoChckLstLog_Etapa_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_DataHora_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A812ContagemResultadoChckLstLog_ChckLstDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_ChckLstDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_ChckLstDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_UsuarioNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_UsuarioNome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoChckLstLog_UsuarioNome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoChckLstLog_Etapa_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoChckLstLog_Etapa_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 105 )
         {
            wbEnd = 0;
            nRC_GXsfl_105 = (short)(nGXsfl_105_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_102_O72e( true) ;
         }
         else
         {
            wb_table3_102_O72e( false) ;
         }
      }

      protected void wb_table2_8_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_O72( true) ;
         }
         else
         {
            wb_table4_11_O72( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_O72( true) ;
         }
         else
         {
            wb_table5_21_O72( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_O72e( true) ;
         }
         else
         {
            wb_table2_8_O72e( false) ;
         }
      }

      protected void wb_table5_21_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_O72( true) ;
         }
         else
         {
            wb_table6_24_O72( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_O72e( true) ;
         }
         else
         {
            wb_table5_21_O72e( false) ;
         }
      }

      protected void wb_table6_24_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_O72( true) ;
         }
         else
         {
            wb_table7_33_O72( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_58_O72( true) ;
         }
         else
         {
            wb_table8_58_O72( false) ;
         }
         return  ;
      }

      protected void wb_table8_58_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV28DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_83_O72( true) ;
         }
         else
         {
            wb_table9_83_O72( false) ;
         }
         return  ;
      }

      protected void wb_table9_83_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_O72e( true) ;
         }
         else
         {
            wb_table6_24_O72e( false) ;
         }
      }

      protected void wb_table9_83_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSOPERATOR3.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_88_O72( true) ;
         }
         else
         {
            wb_table10_88_O72( false) ;
         }
         return  ;
      }

      protected void wb_table10_88_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_usuarionome3_Internalname, StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3), StringUtil.RTrim( context.localUtil.Format( AV32ContagemResultadoChckLstLog_UsuarioNome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_usuarionome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadochcklstlog_usuarionome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_83_O72e( true) ;
         }
         else
         {
            wb_table9_83_O72e( false) ;
         }
      }

      protected void wb_table10_88_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_cell3_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora3_Internalname, context.localUtil.TToC( AV30ContagemResultadoChckLstLog_DataHora3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV30ContagemResultadoChckLstLog_DataHora3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,91);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class, 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_to_cell3_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_to_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora_to3_Internalname, context.localUtil.TToC( AV31ContagemResultadoChckLstLog_DataHora_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV31ContagemResultadoChckLstLog_DataHora_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_88_O72e( true) ;
         }
         else
         {
            wb_table10_88_O72e( false) ;
         }
      }

      protected void wb_table8_58_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSOPERATOR2.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_63_O72( true) ;
         }
         else
         {
            wb_table11_63_O72( false) ;
         }
         return  ;
      }

      protected void wb_table11_63_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_usuarionome2_Internalname, StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2), StringUtil.RTrim( context.localUtil.Format( AV26ContagemResultadoChckLstLog_UsuarioNome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_usuarionome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadochcklstlog_usuarionome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_58_O72e( true) ;
         }
         else
         {
            wb_table8_58_O72e( false) ;
         }
      }

      protected void wb_table11_63_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_cell2_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora2_Internalname, context.localUtil.TToC( AV24ContagemResultadoChckLstLog_DataHora2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24ContagemResultadoChckLstLog_DataHora2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class, 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_to_cell2_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_to_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora_to2_Internalname, context.localUtil.TToC( AV25ContagemResultadoChckLstLog_DataHora_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV25ContagemResultadoChckLstLog_DataHora_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_63_O72e( true) ;
         }
         else
         {
            wb_table11_63_O72e( false) ;
         }
      }

      protected void wb_table7_33_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSOPERATOR1.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_38_O72( true) ;
         }
         else
         {
            wb_table12_38_O72( false) ;
         }
         return  ;
      }

      protected void wb_table12_38_O72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_usuarionome1_Internalname, StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1), StringUtil.RTrim( context.localUtil.Format( AV20ContagemResultadoChckLstLog_UsuarioNome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_usuarionome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadochcklstlog_usuarionome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_O72e( true) ;
         }
         else
         {
            wb_table7_33_O72e( false) ;
         }
      }

      protected void wb_table12_38_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname, tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_cell1_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora1_Internalname, context.localUtil.TToC( AV18ContagemResultadoChckLstLog_DataHora1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18ContagemResultadoChckLstLog_DataHora1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class, 0, "", 1, 1, 0, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultadochcklstlog_datahora_to_cell1_Internalname+"\"  class='"+cellContagemresultadochcklstlog_datahora_to_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_105_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadochcklstlog_datahora_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadochcklstlog_datahora_to1_Internalname, context.localUtil.TToC( AV19ContagemResultadoChckLstLog_DataHora_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV19ContagemResultadoChckLstLog_DataHora_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadochcklstlog_datahora_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadochcklstlog_datahora_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_38_O72e( true) ;
         }
         else
         {
            wb_table12_38_O72e( false) ;
         }
      }

      protected void wb_table4_11_O72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListContagemResultadoChckLstLogWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_O72e( true) ;
         }
         else
         {
            wb_table4_11_O72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultadoChckLstLog_ChckLstCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAO72( ) ;
         WSO72( ) ;
         WEO72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAO72( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "checklistcontagemresultadochcklstlogwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAO72( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultadoChckLstLog_ChckLstCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
         }
         wcpOAV7ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultadoChckLstLog_ChckLstCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultadoChckLstLog_ChckLstCod != wcpOAV7ContagemResultadoChckLstLog_ChckLstCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultadoChckLstLog_ChckLstCod = AV7ContagemResultadoChckLstLog_ChckLstCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod = cgiGet( sPrefix+"AV7ContagemResultadoChckLstLog_ChckLstCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod) > 0 )
         {
            AV7ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
         }
         else
         {
            AV7ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultadoChckLstLog_ChckLstCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAO72( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSO72( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSO72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultadoChckLstLog_ChckLstCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultadoChckLstLog_ChckLstCod_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEO72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117234230");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("checklistcontagemresultadochcklstlogwc.js", "?20203117234230");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1052( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_105_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CODIGO_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_ChckLstDes_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_UsuarioCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_PessoaCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_"+sGXsfl_105_idx;
         edtContagemResultadoChckLstLog_Etapa_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA_"+sGXsfl_105_idx;
      }

      protected void SubsflControlProps_fel_1052( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_105_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CODIGO_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_ChckLstDes_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_UsuarioCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_PessoaCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_"+sGXsfl_105_fel_idx;
         edtContagemResultadoChckLstLog_Etapa_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA_"+sGXsfl_105_fel_idx;
      }

      protected void sendrow_1052( )
      {
         SubsflControlProps_1052( ) ;
         WBO70( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_105_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_105_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_105_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV43Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV43Update_GXI : context.PathToRelativeUrl( AV36Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV44Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A820ContagemResultadoChckLstLog_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_DataHora_Internalname,context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_ChckLstDes_Internalname,(String)A812ContagemResultadoChckLstLog_ChckLstDes,(String)A812ContagemResultadoChckLstLog_ChckLstDes,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_ChckLstDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)105,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_UsuarioNome_Internalname,StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome),StringUtil.RTrim( context.localUtil.Format( A817ContagemResultadoChckLstLog_UsuarioNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoChckLstLog_UsuarioNome_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoChckLstLog_Etapa_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ",", "")),context.localUtil.Format( (decimal)(A824ContagemResultadoChckLstLog_Etapa), "9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoChckLstLog_Etapa_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)105,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA"+"_"+sGXsfl_105_idx, GetSecureSignedToken( sPrefix+sGXsfl_105_idx, context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD"+"_"+sGXsfl_105_idx, GetSecureSignedToken( sPrefix+sGXsfl_105_idx, context.localUtil.Format( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCHCKLSTLOG_ETAPA"+"_"+sGXsfl_105_idx, GetSecureSignedToken( sPrefix+sGXsfl_105_idx, context.localUtil.Format( (decimal)(A824ContagemResultadoChckLstLog_Etapa), "9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_105_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_105_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_105_idx+1));
            sGXsfl_105_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_105_idx), 4, 0)), 4, "0");
            SubsflControlProps_1052( ) ;
         }
         /* End function sendrow_1052 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultadochcklstlog_datahora1_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1";
         cellContagemresultadochcklstlog_datahora_cell1_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11";
         cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell1_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultadochcklstlog_datahora_to1_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1";
         cellContagemresultadochcklstlog_datahora_to_cell1_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1";
         edtavContagemresultadochcklstlog_usuarionome1_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultadochcklstlog_datahora2_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2";
         cellContagemresultadochcklstlog_datahora_cell2_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12";
         cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell2_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultadochcklstlog_datahora_to2_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2";
         cellContagemresultadochcklstlog_datahora_to_cell2_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2";
         edtavContagemresultadochcklstlog_usuarionome2_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultadochcklstlog_datahora3_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3";
         cellContagemresultadochcklstlog_datahora_cell3_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13";
         cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell3_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultadochcklstlog_datahora_to3_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3";
         cellContagemresultadochcklstlog_datahora_to_cell3_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3";
         edtavContagemresultadochcklstlog_usuarionome3_Internalname = sPrefix+"vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContagemResultadoChckLstLog_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CODIGO";
         edtContagemResultadoChckLstLog_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         edtContagemResultadoChckLstLog_ChckLstDes_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES";
         edtContagemResultadoChckLstLog_UsuarioCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD";
         edtContagemResultadoChckLstLog_PessoaCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD";
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         edtContagemResultadoChckLstLog_Etapa_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_ETAPA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContagemResultadoChckLstLog_ChckLstCod_Internalname = sPrefix+"CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoChckLstLog_Etapa_Jsonclick = "";
         edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick = "";
         edtContagemResultadoChckLstLog_PessoaCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_ChckLstDes_Jsonclick = "";
         edtContagemResultadoChckLstLog_DataHora_Jsonclick = "";
         edtContagemResultadoChckLstLog_Codigo_Jsonclick = "";
         edtavContagemresultadochcklstlog_datahora_to1_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_to_cell1_Class = "";
         edtavContagemresultadochcklstlog_datahora1_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_cell1_Class = "";
         edtavContagemresultadochcklstlog_usuarionome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultadochcklstlog_datahora_to2_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_to_cell2_Class = "";
         edtavContagemresultadochcklstlog_datahora2_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_cell2_Class = "";
         edtavContagemresultadochcklstlog_usuarionome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultadochcklstlog_datahora_to3_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_to_cell3_Class = "";
         edtavContagemresultadochcklstlog_datahora3_Jsonclick = "";
         cellContagemresultadochcklstlog_datahora_cell3_Class = "";
         edtavContagemresultadochcklstlog_usuarionome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultadoChckLstLog_UsuarioNome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContagemResultadoChckLstLog_Etapa_Titleformat = 0;
         edtContagemResultadoChckLstLog_UsuarioNome_Titleformat = 0;
         edtContagemResultadoChckLstLog_PessoaCod_Titleformat = 0;
         edtContagemResultadoChckLstLog_UsuarioCod_Titleformat = 0;
         edtContagemResultadoChckLstLog_ChckLstDes_Titleformat = 0;
         edtContagemResultadoChckLstLog_DataHora_Titleformat = 0;
         edtContagemResultadoChckLstLog_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemresultadochcklstlog_usuarionome3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible = 1;
         lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemresultadochcklstlog_usuarionome2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible = 1;
         lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemresultadochcklstlog_usuarionome1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible = 1;
         edtContagemResultadoChckLstLog_Etapa_Title = "Lst Log_Etapa";
         edtContagemResultadoChckLstLog_UsuarioNome_Title = "Usu�rio";
         edtContagemResultadoChckLstLog_PessoaCod_Title = "Id";
         edtContagemResultadoChckLstLog_UsuarioCod_Title = "Id";
         edtContagemResultadoChckLstLog_ChckLstDes_Title = "List";
         edtContagemResultadoChckLstLog_DataHora_Title = "Data";
         edtContagemResultadoChckLstLog_Codigo_Title = "Id";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_ChckLstCod_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemResultadoChckLstLog_Codigo_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_Codigo_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',prop:'Title'},{av:'edtContagemResultadoChckLstLog_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_DataHora_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Title'},{av:'edtContagemResultadoChckLstLog_ChckLstDes_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_ChckLstDes_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',prop:'Title'},{av:'edtContagemResultadoChckLstLog_UsuarioCod_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_UsuarioCod_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',prop:'Title'},{av:'edtContagemResultadoChckLstLog_PessoaCod_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_PessoaCod_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD',prop:'Title'},{av:'edtContagemResultadoChckLstLog_UsuarioNome_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_UsuarioNome_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Title'},{av:'edtContagemResultadoChckLstLog_Etapa_Titleformat',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',prop:'Titleformat'},{av:'edtContagemResultadoChckLstLog_Etapa_Title',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',prop:'Title'},{av:'AV39GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV40GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E27O72',iparms:[{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV36Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContagemResultadoChckLstLog_UsuarioNome_Link',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome2_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome3_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome1_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadochcklstlog_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18O72',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome1_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadochcklstlog_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR1.CLICK","{handler:'E19O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadochcklstlog_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome2_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome3_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome1_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadochcklstlog_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21O72',iparms:[{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome2_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cellContagemresultadochcklstlog_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR2.CLICK","{handler:'E22O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadochcklstlog_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome2_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome3_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome1_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultadochcklstlog_datahora_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL2',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell2_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL2',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL1',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell1_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL1',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23O72',iparms:[{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',prop:'Visible'},{av:'edtavContagemresultadochcklstlog_usuarionome3_Visible',ctrl:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'cellContagemresultadochcklstlog_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR3.CLICK","{handler:'E24O72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContagemResultadoChckLstLog_DataHora1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoChckLstLog_DataHora_To1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoChckLstLog_UsuarioNome1',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultadoChckLstLog_DataHora2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoChckLstLog_DataHora_To2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoChckLstLog_UsuarioNome2',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV32ContagemResultadoChckLstLog_UsuarioNome3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7ContagemResultadoChckLstLog_ChckLstCod',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A822ContagemResultadoChckLstLog_UsuarioCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV30ContagemResultadoChckLstLog_DataHora3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA3',pic:'99/99/99 99:99',nv:''},{av:'AV31ContagemResultadoChckLstLog_DataHora_To3',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO3',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultadochcklstlog_datahora_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_CELL3',prop:'Class'},{av:'cellContagemresultadochcklstlog_datahora_to_cell3_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO_CELL3',prop:'Class'},{av:'lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_RANGEMIDDLETEXT_13',prop:'Class'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16O72',iparms:[{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A820ContagemResultadoChckLstLog_Codigo',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18ContagemResultadoChckLstLog_DataHora1 = (DateTime)(DateTime.MinValue);
         AV19ContagemResultadoChckLstLog_DataHora_To1 = (DateTime)(DateTime.MinValue);
         AV20ContagemResultadoChckLstLog_UsuarioNome1 = "";
         AV22DynamicFiltersSelector2 = "";
         AV24ContagemResultadoChckLstLog_DataHora2 = (DateTime)(DateTime.MinValue);
         AV25ContagemResultadoChckLstLog_DataHora_To2 = (DateTime)(DateTime.MinValue);
         AV26ContagemResultadoChckLstLog_UsuarioNome2 = "";
         AV28DynamicFiltersSelector3 = "";
         AV30ContagemResultadoChckLstLog_DataHora3 = (DateTime)(DateTime.MinValue);
         AV31ContagemResultadoChckLstLog_DataHora_To3 = (DateTime)(DateTime.MinValue);
         AV32ContagemResultadoChckLstLog_UsuarioNome3 = "";
         AV46Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         Gx_date = DateTime.MinValue;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV36Update = "";
         AV43Update_GXI = "";
         AV35Delete = "";
         AV44Delete_GXI = "";
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV20ContagemResultadoChckLstLog_UsuarioNome1 = "";
         lV26ContagemResultadoChckLstLog_UsuarioNome2 = "";
         lV32ContagemResultadoChckLstLog_UsuarioNome3 = "";
         H00O72_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         H00O72_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         H00O72_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         H00O72_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         H00O72_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         H00O72_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         H00O72_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         H00O72_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         H00O72_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         H00O72_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         H00O72_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         H00O72_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00O72_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         H00O73_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV37Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Jsonclick = "";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Jsonclick = "";
         lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.checklistcontagemresultadochcklstlogwc__default(),
            new Object[][] {
                new Object[] {
               H00O72_A811ContagemResultadoChckLstLog_ChckLstCod, H00O72_n811ContagemResultadoChckLstLog_ChckLstCod, H00O72_A824ContagemResultadoChckLstLog_Etapa, H00O72_n824ContagemResultadoChckLstLog_Etapa, H00O72_A817ContagemResultadoChckLstLog_UsuarioNome, H00O72_n817ContagemResultadoChckLstLog_UsuarioNome, H00O72_A823ContagemResultadoChckLstLog_PessoaCod, H00O72_n823ContagemResultadoChckLstLog_PessoaCod, H00O72_A822ContagemResultadoChckLstLog_UsuarioCod, H00O72_A812ContagemResultadoChckLstLog_ChckLstDes,
               H00O72_n812ContagemResultadoChckLstLog_ChckLstDes, H00O72_A814ContagemResultadoChckLstLog_DataHora, H00O72_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               H00O73_AGRID_nRecordCount
               }
            }
         );
         AV46Pgmname = "CheckListContagemResultadoChckLstLogWC";
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         AV46Pgmname = "CheckListContagemResultadoChckLstLogWC";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_105 ;
      private short nGXsfl_105_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV23DynamicFiltersOperator2 ;
      private short AV29DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A824ContagemResultadoChckLstLog_Etapa ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_105_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoChckLstLog_Codigo_Titleformat ;
      private short edtContagemResultadoChckLstLog_DataHora_Titleformat ;
      private short edtContagemResultadoChckLstLog_ChckLstDes_Titleformat ;
      private short edtContagemResultadoChckLstLog_UsuarioCod_Titleformat ;
      private short edtContagemResultadoChckLstLog_PessoaCod_Titleformat ;
      private short edtContagemResultadoChckLstLog_UsuarioNome_Titleformat ;
      private short edtContagemResultadoChckLstLog_Etapa_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContagemResultadoChckLstLog_ChckLstCod ;
      private int wcpOAV7ContagemResultadoChckLstLog_ChckLstCod ;
      private int subGrid_Rows ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int edtContagemResultadoChckLstLog_ChckLstCod_Visible ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV38PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Visible ;
      private int edtavContagemresultadochcklstlog_usuarionome1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Visible ;
      private int edtavContagemresultadochcklstlog_usuarionome2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Visible ;
      private int edtavContagemresultadochcklstlog_usuarionome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV39GridCurrentPage ;
      private long AV40GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_105_idx="0001" ;
      private String AV20ContagemResultadoChckLstLog_UsuarioNome1 ;
      private String AV26ContagemResultadoChckLstLog_UsuarioNome2 ;
      private String AV32ContagemResultadoChckLstLog_UsuarioNome3 ;
      private String AV46Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoChckLstLog_ChckLstCod_Internalname ;
      private String edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultadoChckLstLog_Codigo_Internalname ;
      private String edtContagemResultadoChckLstLog_DataHora_Internalname ;
      private String edtContagemResultadoChckLstLog_ChckLstDes_Internalname ;
      private String edtContagemResultadoChckLstLog_UsuarioCod_Internalname ;
      private String edtContagemResultadoChckLstLog_PessoaCod_Internalname ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Internalname ;
      private String edtContagemResultadoChckLstLog_Etapa_Internalname ;
      private String scmdbuf ;
      private String lV20ContagemResultadoChckLstLog_UsuarioNome1 ;
      private String lV26ContagemResultadoChckLstLog_UsuarioNome2 ;
      private String lV32ContagemResultadoChckLstLog_UsuarioNome3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora1_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora_to1_Internalname ;
      private String edtavContagemresultadochcklstlog_usuarionome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora2_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora_to2_Internalname ;
      private String edtavContagemresultadochcklstlog_usuarionome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora3_Internalname ;
      private String edtavContagemresultadochcklstlog_datahora_to3_Internalname ;
      private String edtavContagemresultadochcklstlog_usuarionome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtContagemResultadoChckLstLog_Codigo_Title ;
      private String edtContagemResultadoChckLstLog_DataHora_Title ;
      private String edtContagemResultadoChckLstLog_ChckLstDes_Title ;
      private String edtContagemResultadoChckLstLog_UsuarioCod_Title ;
      private String edtContagemResultadoChckLstLog_PessoaCod_Title ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Title ;
      private String edtContagemResultadoChckLstLog_Etapa_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Link ;
      private String tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora1_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_cell1_Class ;
      private String cellContagemresultadochcklstlog_datahora_cell1_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_to_cell1_Class ;
      private String cellContagemresultadochcklstlog_datahora_to_cell1_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Class ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora2_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_cell2_Class ;
      private String cellContagemresultadochcklstlog_datahora_cell2_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_to_cell2_Class ;
      private String cellContagemresultadochcklstlog_datahora_to_cell2_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Class ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadochcklstlog_datahora3_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_cell3_Class ;
      private String cellContagemresultadochcklstlog_datahora_cell3_Internalname ;
      private String cellContagemresultadochcklstlog_datahora_to_cell3_Class ;
      private String cellContagemresultadochcklstlog_datahora_to_cell3_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Class ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultadochcklstlog_usuarionome3_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora3_Jsonclick ;
      private String cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_13_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultadochcklstlog_usuarionome2_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora2_Jsonclick ;
      private String cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_12_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultadochcklstlog_usuarionome1_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora1_Jsonclick ;
      private String cellContagemresultadochcklstlog_datahora_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultadochcklstlog_datahora_rangemiddletext_11_Jsonclick ;
      private String edtavContagemresultadochcklstlog_datahora_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ContagemResultadoChckLstLog_ChckLstCod ;
      private String sGXsfl_105_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultadoChckLstLog_Codigo_Jsonclick ;
      private String edtContagemResultadoChckLstLog_DataHora_Jsonclick ;
      private String edtContagemResultadoChckLstLog_ChckLstDes_Jsonclick ;
      private String edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick ;
      private String edtContagemResultadoChckLstLog_PessoaCod_Jsonclick ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick ;
      private String edtContagemResultadoChckLstLog_Etapa_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV18ContagemResultadoChckLstLog_DataHora1 ;
      private DateTime AV19ContagemResultadoChckLstLog_DataHora_To1 ;
      private DateTime AV24ContagemResultadoChckLstLog_DataHora2 ;
      private DateTime AV25ContagemResultadoChckLstLog_DataHora_To2 ;
      private DateTime AV30ContagemResultadoChckLstLog_DataHora3 ;
      private DateTime AV31ContagemResultadoChckLstLog_DataHora_To3 ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV21DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersEnabled3 ;
      private bool AV34DynamicFiltersIgnoreFirst ;
      private bool AV33DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool n824ContagemResultadoChckLstLog_Etapa ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV36Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private String A812ContagemResultadoChckLstLog_ChckLstDes ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV22DynamicFiltersSelector2 ;
      private String AV28DynamicFiltersSelector3 ;
      private String AV43Update_GXI ;
      private String AV44Delete_GXI ;
      private String AV36Update ;
      private String AV35Delete ;
      private IGxSession AV37Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00O72_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] H00O72_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private short[] H00O72_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] H00O72_n824ContagemResultadoChckLstLog_Etapa ;
      private String[] H00O72_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] H00O72_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] H00O72_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] H00O72_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] H00O72_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private String[] H00O72_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] H00O72_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private DateTime[] H00O72_A814ContagemResultadoChckLstLog_DataHora ;
      private int[] H00O72_A820ContagemResultadoChckLstLog_Codigo ;
      private long[] H00O73_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class checklistcontagemresultadochcklstlogwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00O72( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             DateTime AV18ContagemResultadoChckLstLog_DataHora1 ,
                                             DateTime AV19ContagemResultadoChckLstLog_DataHora_To1 ,
                                             String AV20ContagemResultadoChckLstLog_UsuarioNome1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             short AV23DynamicFiltersOperator2 ,
                                             DateTime AV24ContagemResultadoChckLstLog_DataHora2 ,
                                             DateTime AV25ContagemResultadoChckLstLog_DataHora_To2 ,
                                             String AV26ContagemResultadoChckLstLog_UsuarioNome2 ,
                                             bool AV27DynamicFiltersEnabled3 ,
                                             String AV28DynamicFiltersSelector3 ,
                                             short AV29DynamicFiltersOperator3 ,
                                             DateTime AV30ContagemResultadoChckLstLog_DataHora3 ,
                                             DateTime AV31ContagemResultadoChckLstLog_DataHora_To3 ,
                                             String AV32ContagemResultadoChckLstLog_UsuarioNome3 ,
                                             DateTime A814ContagemResultadoChckLstLog_DataHora ,
                                             String A817ContagemResultadoChckLstLog_UsuarioNome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A811ContagemResultadoChckLstLog_ChckLstCod ,
                                             int AV7ContagemResultadoChckLstLog_ChckLstCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, T1.[ContagemResultadoChckLstLog_Etapa], T4.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, T3.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod, T1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T2.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T1.[ContagemResultadoChckLstLog_DataHora], T1.[ContagemResultadoChckLstLog_Codigo]";
         sFromString = " FROM ((([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[ContagemResultadoChckLstLog_ChckLstCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultadoChckLstLog_ChckLstCod] = @AV7ContagemResultadoChckLstLog_ChckLstCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoChckLstLog_DataHora1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV18ContagemResultadoChckLstLog_DataHora1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 2 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV18ContagemResultadoChckLstLog_DataHora1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV18ContagemResultadoChckLstLog_DataHora1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV19ContagemResultadoChckLstLog_DataHora_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV19ContagemResultadoChckLstLog_DataHora_To1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV19ContagemResultadoChckLstLog_DataHora_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV19ContagemResultadoChckLstLog_DataHora_To1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20ContagemResultadoChckLstLog_UsuarioNome1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV20ContagemResultadoChckLstLog_UsuarioNome1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoChckLstLog_DataHora2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV24ContagemResultadoChckLstLog_DataHora2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV23DynamicFiltersOperator2 == 1 ) || ( AV23DynamicFiltersOperator2 == 2 ) || ( AV23DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV24ContagemResultadoChckLstLog_DataHora2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV24ContagemResultadoChckLstLog_DataHora2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoChckLstLog_DataHora_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV25ContagemResultadoChckLstLog_DataHora_To2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoChckLstLog_DataHora_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV25ContagemResultadoChckLstLog_DataHora_To2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV26ContagemResultadoChckLstLog_UsuarioNome2)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV26ContagemResultadoChckLstLog_UsuarioNome2)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoChckLstLog_DataHora3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV30ContagemResultadoChckLstLog_DataHora3)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV29DynamicFiltersOperator3 == 1 ) || ( AV29DynamicFiltersOperator3 == 2 ) || ( AV29DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV30ContagemResultadoChckLstLog_DataHora3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV30ContagemResultadoChckLstLog_DataHora3)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV31ContagemResultadoChckLstLog_DataHora_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV31ContagemResultadoChckLstLog_DataHora_To3)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV31ContagemResultadoChckLstLog_DataHora_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV31ContagemResultadoChckLstLog_DataHora_To3)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV32ContagemResultadoChckLstLog_UsuarioNome3)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV32ContagemResultadoChckLstLog_UsuarioNome3)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T1.[ContagemResultadoChckLstLog_DataHora]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T1.[ContagemResultadoChckLstLog_DataHora] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T1.[ContagemResultadoChckLstLog_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T1.[ContagemResultadoChckLstLog_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T2.[CheckList_Descricao]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T2.[CheckList_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T1.[ContagemResultadoChckLstLog_UsuarioCod]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T1.[ContagemResultadoChckLstLog_UsuarioCod] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T3.[Usuario_PessoaCod]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T3.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T4.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod], T1.[ContagemResultadoChckLstLog_Etapa]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_ChckLstCod] DESC, T1.[ContagemResultadoChckLstLog_Etapa] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoChckLstLog_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00O73( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             DateTime AV18ContagemResultadoChckLstLog_DataHora1 ,
                                             DateTime AV19ContagemResultadoChckLstLog_DataHora_To1 ,
                                             String AV20ContagemResultadoChckLstLog_UsuarioNome1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             short AV23DynamicFiltersOperator2 ,
                                             DateTime AV24ContagemResultadoChckLstLog_DataHora2 ,
                                             DateTime AV25ContagemResultadoChckLstLog_DataHora_To2 ,
                                             String AV26ContagemResultadoChckLstLog_UsuarioNome2 ,
                                             bool AV27DynamicFiltersEnabled3 ,
                                             String AV28DynamicFiltersSelector3 ,
                                             short AV29DynamicFiltersOperator3 ,
                                             DateTime AV30ContagemResultadoChckLstLog_DataHora3 ,
                                             DateTime AV31ContagemResultadoChckLstLog_DataHora_To3 ,
                                             String AV32ContagemResultadoChckLstLog_UsuarioNome3 ,
                                             DateTime A814ContagemResultadoChckLstLog_DataHora ,
                                             String A817ContagemResultadoChckLstLog_UsuarioNome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A811ContagemResultadoChckLstLog_ChckLstCod ,
                                             int AV7ContagemResultadoChckLstLog_ChckLstCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [19] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[ContagemResultadoChckLstLog_ChckLstCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoChckLstLog_ChckLstCod] = @AV7ContagemResultadoChckLstLog_ChckLstCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (DateTime.MinValue==AV18ContagemResultadoChckLstLog_DataHora1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV18ContagemResultadoChckLstLog_DataHora1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 2 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV18ContagemResultadoChckLstLog_DataHora1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV18ContagemResultadoChckLstLog_DataHora1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (DateTime.MinValue==AV19ContagemResultadoChckLstLog_DataHora_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV19ContagemResultadoChckLstLog_DataHora_To1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV17DynamicFiltersOperator1 == 3 ) && ( ! (DateTime.MinValue==AV19ContagemResultadoChckLstLog_DataHora_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV19ContagemResultadoChckLstLog_DataHora_To1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20ContagemResultadoChckLstLog_UsuarioNome1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultadoChckLstLog_UsuarioNome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV20ContagemResultadoChckLstLog_UsuarioNome1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoChckLstLog_DataHora2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV24ContagemResultadoChckLstLog_DataHora2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV23DynamicFiltersOperator2 == 1 ) || ( AV23DynamicFiltersOperator2 == 2 ) || ( AV23DynamicFiltersOperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV24ContagemResultadoChckLstLog_DataHora2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV24ContagemResultadoChckLstLog_DataHora2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoChckLstLog_DataHora_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV25ContagemResultadoChckLstLog_DataHora_To2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV23DynamicFiltersOperator2 == 3 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoChckLstLog_DataHora_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV25ContagemResultadoChckLstLog_DataHora_To2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV26ContagemResultadoChckLstLog_UsuarioNome2)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26ContagemResultadoChckLstLog_UsuarioNome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV26ContagemResultadoChckLstLog_UsuarioNome2)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! (DateTime.MinValue==AV30ContagemResultadoChckLstLog_DataHora3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV30ContagemResultadoChckLstLog_DataHora3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( ( AV29DynamicFiltersOperator3 == 1 ) || ( AV29DynamicFiltersOperator3 == 2 ) || ( AV29DynamicFiltersOperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV30ContagemResultadoChckLstLog_DataHora3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] >= @AV30ContagemResultadoChckLstLog_DataHora3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! (DateTime.MinValue==AV31ContagemResultadoChckLstLog_DataHora_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] < @AV31ContagemResultadoChckLstLog_DataHora_To3)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA") == 0 ) && ( AV29DynamicFiltersOperator3 == 3 ) && ( ! (DateTime.MinValue==AV31ContagemResultadoChckLstLog_DataHora_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoChckLstLog_DataHora] <= @AV31ContagemResultadoChckLstLog_DataHora_To3)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV32ContagemResultadoChckLstLog_UsuarioNome3)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoChckLstLog_UsuarioNome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV32ContagemResultadoChckLstLog_UsuarioNome3)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00O72(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (bool)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
               case 1 :
                     return conditional_H00O73(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (bool)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00O72 ;
          prmH00O72 = new Object[] {
          new Object[] {"@AV7ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoChckLstLog_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoChckLstLog_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19ContagemResultadoChckLstLog_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19ContagemResultadoChckLstLog_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV20ContagemResultadoChckLstLog_UsuarioNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV20ContagemResultadoChckLstLog_UsuarioNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24ContagemResultadoChckLstLog_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoChckLstLog_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoChckLstLog_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoChckLstLog_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV26ContagemResultadoChckLstLog_UsuarioNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV26ContagemResultadoChckLstLog_UsuarioNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV30ContagemResultadoChckLstLog_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoChckLstLog_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31ContagemResultadoChckLstLog_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31ContagemResultadoChckLstLog_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV32ContagemResultadoChckLstLog_UsuarioNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32ContagemResultadoChckLstLog_UsuarioNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00O73 ;
          prmH00O73 = new Object[] {
          new Object[] {"@AV7ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContagemResultadoChckLstLog_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18ContagemResultadoChckLstLog_DataHora1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19ContagemResultadoChckLstLog_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19ContagemResultadoChckLstLog_DataHora_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV20ContagemResultadoChckLstLog_UsuarioNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV20ContagemResultadoChckLstLog_UsuarioNome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24ContagemResultadoChckLstLog_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoChckLstLog_DataHora2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoChckLstLog_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoChckLstLog_DataHora_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV26ContagemResultadoChckLstLog_UsuarioNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV26ContagemResultadoChckLstLog_UsuarioNome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV30ContagemResultadoChckLstLog_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV30ContagemResultadoChckLstLog_DataHora3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31ContagemResultadoChckLstLog_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31ContagemResultadoChckLstLog_DataHora_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV32ContagemResultadoChckLstLog_UsuarioNome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32ContagemResultadoChckLstLog_UsuarioNome3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00O72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O72,11,0,true,false )
             ,new CursorDef("H00O73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O73,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
       }
    }

 }

}
