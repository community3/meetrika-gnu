/*
               File: PRC_EhGestor
        Description: Retorna Eh Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:46.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ehgestor : GXProcedure
   {
      public prc_ehgestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ehgestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_Usuario_Codigo ,
                           out bool aP2_EhGestor )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV8EhGestor = false ;
         initialize();
         executePrivate();
         aP2_EhGestor=this.AV8EhGestor;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo ,
                              int aP1_Usuario_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11Usuario_Codigo = aP1_Usuario_Codigo;
         this.AV8EhGestor = false ;
         initialize();
         executePrivate();
         aP2_EhGestor=this.AV8EhGestor;
         return AV8EhGestor ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_Usuario_Codigo ,
                                 out bool aP2_EhGestor )
      {
         prc_ehgestor objprc_ehgestor;
         objprc_ehgestor = new prc_ehgestor();
         objprc_ehgestor.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_ehgestor.AV11Usuario_Codigo = aP1_Usuario_Codigo;
         objprc_ehgestor.AV8EhGestor = false ;
         objprc_ehgestor.context.SetSubmitInitialConfig(context);
         objprc_ehgestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ehgestor);
         aP2_EhGestor=this.AV8EhGestor;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ehgestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007B2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P007B2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P007B2_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P007B2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P007B2_n1603ContagemResultado_CntCod[0];
            A1603ContagemResultado_CntCod = P007B2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P007B2_n1603ContagemResultado_CntCod[0];
            /* Using cursor P007B3 */
            pr_default.execute(1, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod, AV11Usuario_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P007B3_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P007B3_A1079ContratoGestor_UsuarioCod[0];
               AV8EhGestor = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007B2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P007B2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P007B2_A456ContagemResultado_Codigo = new int[1] ;
         P007B2_A1603ContagemResultado_CntCod = new int[1] ;
         P007B2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P007B3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P007B3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ehgestor__default(),
            new Object[][] {
                new Object[] {
               P007B2_A1553ContagemResultado_CntSrvCod, P007B2_n1553ContagemResultado_CntSrvCod, P007B2_A456ContagemResultado_Codigo, P007B2_A1603ContagemResultado_CntCod, P007B2_n1603ContagemResultado_CntCod
               }
               , new Object[] {
               P007B3_A1078ContratoGestor_ContratoCod, P007B3_A1079ContratoGestor_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV11Usuario_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private String scmdbuf ;
      private bool AV8EhGestor ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P007B2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P007B2_n1553ContagemResultado_CntSrvCod ;
      private int[] P007B2_A456ContagemResultado_Codigo ;
      private int[] P007B2_A1603ContagemResultado_CntCod ;
      private bool[] P007B2_n1603ContagemResultado_CntCod ;
      private int[] P007B3_A1078ContratoGestor_ContratoCod ;
      private int[] P007B3_A1079ContratoGestor_UsuarioCod ;
      private bool aP2_EhGestor ;
   }

   public class prc_ehgestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007B2 ;
          prmP007B2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007B3 ;
          prmP007B3 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007B2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007B2,1,0,true,true )
             ,new CursorDef("P007B3", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContagemResultado_CntCod and [ContratoGestor_UsuarioCod] = @AV11Usuario_Codigo ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007B3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
