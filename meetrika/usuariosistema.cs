/*
               File: UsuarioSistema
        Description: Usuario Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:38.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuariosistema : GXProcedure
   {
      public usuariosistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuariosistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_UsuarioSistema", "GxEv3Up14_Meetrika", "SdtSDT_UsuarioSistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_UsuarioSistema", "GxEv3Up14_Meetrika", "SdtSDT_UsuarioSistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         usuariosistema objusuariosistema;
         objusuariosistema = new usuariosistema();
         objusuariosistema.Gxm2rootcol = new GxObjectCollection( context, "SDT_UsuarioSistema", "GxEv3Up14_Meetrika", "SdtSDT_UsuarioSistema", "GeneXus.Programs") ;
         objusuariosistema.context.SetSubmitInitialConfig(context);
         objusuariosistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objusuariosistema);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((usuariosistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000F2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A57Usuario_PessoaCod = P000F2_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = P000F2_A1Usuario_Codigo[0];
            A2Usuario_Nome = P000F2_A2Usuario_Nome[0];
            n2Usuario_Nome = P000F2_n2Usuario_Nome[0];
            A1647Usuario_Email = P000F2_A1647Usuario_Email[0];
            n1647Usuario_Email = P000F2_n1647Usuario_Email[0];
            A325Usuario_PessoaDoc = P000F2_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P000F2_n325Usuario_PessoaDoc[0];
            A325Usuario_PessoaDoc = P000F2_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = P000F2_n325Usuario_PessoaDoc[0];
            Gxm1sdt_usuariosistema = new SdtSDT_UsuarioSistema(context);
            Gxm2rootcol.Add(Gxm1sdt_usuariosistema, 0);
            Gxm1sdt_usuariosistema.gxTpr_External_id = A1Usuario_Codigo;
            Gxm1sdt_usuariosistema.gxTpr_Name = A2Usuario_Nome;
            Gxm1sdt_usuariosistema.gxTpr_Email = A1647Usuario_Email;
            Gxm1sdt_usuariosistema.gxTpr_Cpf = A325Usuario_PessoaDoc;
            GXt_dtime1 = (DateTime)(DateTime.MinValue);
            new prc_gamuserdatacriacao(context ).execute(  A1Usuario_Codigo, out  GXt_dtime1) ;
            Gxm1sdt_usuariosistema.gxTpr_Created_at = GXt_dtime1;
            GXt_dtime1 = (DateTime)(DateTime.MinValue);
            new prc_gamuserdataatualizacao(context ).execute(  A1Usuario_Codigo, out  GXt_dtime1) ;
            Gxm1sdt_usuariosistema.gxTpr_Updated_at = GXt_dtime1;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000F2_A57Usuario_PessoaCod = new int[1] ;
         P000F2_A1Usuario_Codigo = new int[1] ;
         P000F2_A2Usuario_Nome = new String[] {""} ;
         P000F2_n2Usuario_Nome = new bool[] {false} ;
         P000F2_A1647Usuario_Email = new String[] {""} ;
         P000F2_n1647Usuario_Email = new bool[] {false} ;
         P000F2_A325Usuario_PessoaDoc = new String[] {""} ;
         P000F2_n325Usuario_PessoaDoc = new bool[] {false} ;
         A2Usuario_Nome = "";
         A1647Usuario_Email = "";
         A325Usuario_PessoaDoc = "";
         Gxm1sdt_usuariosistema = new SdtSDT_UsuarioSistema(context);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuariosistema__default(),
            new Object[][] {
                new Object[] {
               P000F2_A57Usuario_PessoaCod, P000F2_A1Usuario_Codigo, P000F2_A2Usuario_Nome, P000F2_n2Usuario_Nome, P000F2_A1647Usuario_Email, P000F2_n1647Usuario_Email, P000F2_A325Usuario_PessoaDoc, P000F2_n325Usuario_PessoaDoc
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private String A2Usuario_Nome ;
      private DateTime GXt_dtime1 ;
      private bool n2Usuario_Nome ;
      private bool n1647Usuario_Email ;
      private bool n325Usuario_PessoaDoc ;
      private String A1647Usuario_Email ;
      private String A325Usuario_PessoaDoc ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000F2_A57Usuario_PessoaCod ;
      private int[] P000F2_A1Usuario_Codigo ;
      private String[] P000F2_A2Usuario_Nome ;
      private bool[] P000F2_n2Usuario_Nome ;
      private String[] P000F2_A1647Usuario_Email ;
      private bool[] P000F2_n1647Usuario_Email ;
      private String[] P000F2_A325Usuario_PessoaDoc ;
      private bool[] P000F2_n325Usuario_PessoaDoc ;
      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_UsuarioSistema ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_UsuarioSistema Gxm1sdt_usuariosistema ;
   }

   public class usuariosistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000F2 ;
          prmP000F2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P000F2", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T1.[Usuario_Nome], T1.[Usuario_Email], T2.[Pessoa_Docto] AS Usuario_PessoaDoc FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000F2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.usuariosistema_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class usuariosistema_services : GxRestService
 {
    [OperationContract]
    [WebInvoke(Method =  "GET" ,
    	BodyStyle =  WebMessageBodyStyle.Bare  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/?")]
    public GxGenericCollection<SdtSDT_UsuarioSistema_RESTInterface> execute( )
    {
       GxGenericCollection<SdtSDT_UsuarioSistema_RESTInterface> Gxm2rootcol = new GxGenericCollection<SdtSDT_UsuarioSistema_RESTInterface>() ;
       try
       {
          if ( ! ProcessHeaders("usuariosistema") )
          {
             return null ;
          }
          usuariosistema worker = new usuariosistema(context) ;
          worker.IsMain = RunAsMain ;
          IGxCollection gxrGxm2rootcol = new GxObjectCollection() ;
          worker.execute(out gxrGxm2rootcol );
          worker.cleanup( );
          Gxm2rootcol = new GxGenericCollection<SdtSDT_UsuarioSistema_RESTInterface>(gxrGxm2rootcol) ;
          return Gxm2rootcol ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
       return null ;
    }

 }

}
