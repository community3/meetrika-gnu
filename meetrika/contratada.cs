/*
               File: Contratada
        Description: Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:18:29.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_UF") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvCONTRATADA_UF0C13( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATADA_MUNICIPIOCOD") == 0 )
         {
            AV25Contratada_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATADA_MUNICIPIOCOD0C13( AV25Contratada_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"CONTRATADA_PESSOACOD") == 0 )
         {
            AV11Insert_Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASACONTRATADA_PESSOACOD0C13( AV11Insert_Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel23"+"_"+"vISCONFIGURACOESVALIDAS") == 0 )
         {
            A40000Contratada_Logo_GXI = GetNextPar( );
            n40000Contratada_Logo_GXI = false;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            A1664Contratada_Logo = GetNextPar( );
            n1664Contratada_Logo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX23ASAISCONFIGURACOESVALIDAS0C13( A40000Contratada_Logo_GXI, A1664Contratada_Logo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_60") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_60( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_62") == 0 )
         {
            A349Contratada_MunicipioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n349Contratada_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_62( A349Contratada_MunicipioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_61") == 0 )
         {
            A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_61( A52Contratada_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratada_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratada_TipoFabrica.Name = "CONTRATADA_TIPOFABRICA";
         cmbContratada_TipoFabrica.WebTags = "";
         cmbContratada_TipoFabrica.addItem("", "(Nenhuma)", 0);
         cmbContratada_TipoFabrica.addItem("S", "F�brica de Software", 0);
         cmbContratada_TipoFabrica.addItem("M", "F�brica de M�trica", 0);
         cmbContratada_TipoFabrica.addItem("I", "Departamento / Setor", 0);
         cmbContratada_TipoFabrica.addItem("O", "Outras", 0);
         if ( cmbContratada_TipoFabrica.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A516Contratada_TipoFabrica)) )
            {
               A516Contratada_TipoFabrica = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            }
            A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         }
         dynavContratada_uf.Name = "vCONTRATADA_UF";
         dynavContratada_uf.WebTags = "";
         dynContratada_MunicipioCod.Name = "CONTRATADA_MUNICIPIOCOD";
         dynContratada_MunicipioCod.WebTags = "";
         cmbContratada_UsaOSistema.Name = "CONTRATADA_USAOSISTEMA";
         cmbContratada_UsaOSistema.WebTags = "";
         cmbContratada_UsaOSistema.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratada_UsaOSistema.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratada_UsaOSistema.ItemCount > 0 )
         {
            A1481Contratada_UsaOSistema = StringUtil.StrToBool( cmbContratada_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1481Contratada_UsaOSistema)));
            n1481Contratada_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
         }
         cmbContratada_OSPreferencial.Name = "CONTRATADA_OSPREFERENCIAL";
         cmbContratada_OSPreferencial.WebTags = "";
         cmbContratada_OSPreferencial.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratada_OSPreferencial.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratada_OSPreferencial.ItemCount > 0 )
         {
            A1867Contratada_OSPreferencial = StringUtil.StrToBool( cmbContratada_OSPreferencial.getValidValue(StringUtil.BoolToStr( A1867Contratada_OSPreferencial)));
            n1867Contratada_OSPreferencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
         }
         chkContratada_Ativo.Name = "CONTRATADA_ATIVO";
         chkContratada_Ativo.WebTags = "";
         chkContratada_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "TitleCaption", chkContratada_Ativo.Caption);
         chkContratada_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contratada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contratada_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Contratada_Codigo = aP1_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratada_TipoFabrica = new GXCombobox();
         dynavContratada_uf = new GXCombobox();
         dynContratada_MunicipioCod = new GXCombobox();
         cmbContratada_UsaOSistema = new GXCombobox();
         cmbContratada_OSPreferencial = new GXCombobox();
         chkContratada_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratada_TipoFabrica.ItemCount > 0 )
         {
            A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         }
         if ( dynavContratada_uf.ItemCount > 0 )
         {
            AV25Contratada_UF = dynavContratada_uf.getValidValue(AV25Contratada_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
         }
         if ( dynContratada_MunicipioCod.ItemCount > 0 )
         {
            A349Contratada_MunicipioCod = (int)(NumberUtil.Val( dynContratada_MunicipioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0))), "."));
            n349Contratada_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
         }
         if ( cmbContratada_UsaOSistema.ItemCount > 0 )
         {
            A1481Contratada_UsaOSistema = StringUtil.StrToBool( cmbContratada_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1481Contratada_UsaOSistema)));
            n1481Contratada_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
         }
         if ( cmbContratada_OSPreferencial.ItemCount > 0 )
         {
            A1867Contratada_OSPreferencial = StringUtil.StrToBool( cmbContratada_OSPreferencial.getValidValue(StringUtil.BoolToStr( A1867Contratada_OSPreferencial)));
            n1867Contratada_OSPreferencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0C13( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0C13e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Codigo), 6, 0, ",", "")), ((edtavContrato_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Contrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV36Contrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContrato_codigo_Visible, edtavContrato_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Contratada_PessoaCod), 6, 0, ",", "")), ((edtavContratada_pessoacod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23Contratada_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV23Contratada_PessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_pessoacod_Visible, edtavContratada_pessoacod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")), ((edtContratada_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratada_Codigo_Visible, edtContratada_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratada_PessoaCod_Visible, edtContratada_PessoaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contratada.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0C13( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0C13( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0C13e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_133_0C13( true) ;
         }
         return  ;
      }

      protected void wb_table3_133_0C13e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 1, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0C13e( true) ;
         }
         else
         {
            wb_table1_2_0C13e( false) ;
         }
      }

      protected void wb_table3_133_0C13( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_133_0C13e( true) ;
         }
         else
         {
            wb_table3_133_0C13e( false) ;
         }
      }

      protected void wb_table2_5_0C13( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_14_0C13( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_0C13e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0C13e( true) ;
         }
         else
         {
            wb_table2_5_0C13e( false) ;
         }
      }

      protected void wb_table4_14_0C13( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_tipofabrica_Internalname, "Tipo", "", "", lblTextblockcontratada_tipofabrica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_TipoFabrica, cmbContratada_TipoFabrica_Internalname, StringUtil.RTrim( A516Contratada_TipoFabrica), 1, cmbContratada_TipoFabrica_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e110c13_client"+"'", "char", "", 1, cmbContratada_TipoFabrica.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_Contratada.htm");
            cmbContratada_TipoFabrica.CurrentValue = StringUtil.RTrim( A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_TipoFabrica_Internalname, "Values", (String)(cmbContratada_TipoFabrica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_razaosocial_Internalname, "Raz�o Social", "", "", lblTextblockcontratada_razaosocial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_razaosocial_Internalname, StringUtil.RTrim( AV24Contratada_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( AV24Contratada_RazaoSocial, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_razaosocial_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtavContratada_razaosocial_Enabled, 0, "text", "", 100, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_logo_Internalname, "Logomarca", "", "", lblTextblockcontratada_logo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataContentCell'>") ;
            /* Static Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            A1664Contratada_Logo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)));
            GxWebStd.gx_bitmap( context, imgContratada_Logo_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.PathToRelativeUrl( A1664Contratada_Logo)), "", "", "", context.GetTheme( ), 1, imgContratada_Logo_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", "", "", 0, A1664Contratada_Logo_IsBlob, true, "HLP_Contratada.htm");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.PathToRelativeUrl( A1664Contratada_Logo)));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "IsBlob", StringUtil.BoolToStr( A1664Contratada_Logo_IsBlob));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_cnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_cnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratada_cnpj_Visible, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_cnpj_Internalname, AV21Contratada_CNPJ, StringUtil.RTrim( context.localUtil.Format( AV21Contratada_CNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_cnpj_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_cnpj_Visible, edtavContratada_cnpj_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ie_Internalname, "Insc. Estadual", "", "", lblTextblockpessoa_ie_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockpessoa_ie_Visible, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_ie_Internalname, StringUtil.RTrim( AV32Pessoa_IE), StringUtil.RTrim( context.localUtil.Format( AV32Pessoa_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_ie_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPessoa_ie_Visible, edtavPessoa_ie_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_sigla_Internalname, "Sigla", "", "", lblTextblockcontratada_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_Sigla_Internalname, StringUtil.RTrim( A438Contratada_Sigla), StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_banconome_Internalname, "Banco", "", "", lblTextblockcontratada_banconome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_banconome_Internalname, StringUtil.RTrim( AV19Contratada_BancoNome), StringUtil.RTrim( context.localUtil.Format( AV19Contratada_BancoNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_banconome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratada_banconome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_banconro_Internalname, "N�mero", "", "", lblTextblockcontratada_banconro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_banconro_Internalname, StringUtil.RTrim( AV20Contratada_BancoNro), StringUtil.RTrim( context.localUtil.Format( AV20Contratada_BancoNro, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_banconro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratada_banconro_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_agencianome_Internalname, "Ag�ncia", "", "", lblTextblockcontratada_agencianome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_agencianome_Internalname, StringUtil.RTrim( AV17Contratada_AgenciaNome), StringUtil.RTrim( context.localUtil.Format( AV17Contratada_AgenciaNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_agencianome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratada_agencianome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_agencianro_Internalname, "N�mero", "", "", lblTextblockcontratada_agencianro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_agencianro_Internalname, StringUtil.RTrim( AV18Contratada_AgenciaNro), StringUtil.RTrim( context.localUtil.Format( AV18Contratada_AgenciaNro, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_agencianro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratada_agencianro_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_contacorrente_Internalname, "Conta Corrente", "", "", lblTextblockcontratada_contacorrente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_contacorrente_Internalname, StringUtil.RTrim( AV22Contratada_ContaCorrente), StringUtil.RTrim( context.localUtil.Format( AV22Contratada_ContaCorrente, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_contacorrente_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratada_contacorrente_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_endereco_Internalname, "Endere�o", "", "", lblTextblockpessoa_endereco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_endereco_Internalname, AV30Pessoa_Endereco, StringUtil.RTrim( context.localUtil.Format( AV30Pessoa_Endereco, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_endereco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPessoa_endereco_Enabled, 0, "text", "", 700, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_cep_Internalname, "CEP", "", "", lblTextblockpessoa_cep_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_cep_Internalname, StringUtil.RTrim( AV29Pessoa_CEP), StringUtil.RTrim( context.localUtil.Format( AV29Pessoa_CEP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_cep_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPessoa_cep_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_uf_Internalname, "Estado", "", "", lblTextblockcontratada_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_uf, dynavContratada_uf_Internalname, StringUtil.RTrim( AV25Contratada_UF), 1, dynavContratada_uf_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynavContratada_uf.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_Contratada.htm");
            dynavContratada_uf.CurrentValue = StringUtil.RTrim( AV25Contratada_UF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_uf_Internalname, "Values", (String)(dynavContratada_uf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_municipiocod_Internalname, "Municipio", "", "", lblTextblockcontratada_municipiocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratada_MunicipioCod, dynContratada_MunicipioCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)), 1, dynContratada_MunicipioCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratada_MunicipioCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "", true, "HLP_Contratada.htm");
            dynContratada_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_MunicipioCod_Internalname, "Values", (String)(dynContratada_MunicipioCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_telefone_Internalname, "Telefone", "", "", lblTextblockpessoa_telefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_telefone_Internalname, StringUtil.RTrim( AV33Pessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( AV33Pessoa_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_telefone_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPessoa_telefone_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_fax_Internalname, "Fax", "", "", lblTextblockpessoa_fax_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_fax_Internalname, StringUtil.RTrim( AV31Pessoa_Fax), StringUtil.RTrim( context.localUtil.Format( AV31Pessoa_Fax, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_fax_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPessoa_fax_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ss_Internalname, "�ltima SS", "", "", lblTextblockcontratada_ss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_SS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ",", "")), ((edtContratada_SS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_SS_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_SS_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_os_Internalname, "�ltima OS", "", "", lblTextblockcontratada_os_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_OS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ",", "")), ((edtContratada_OS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_OS_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_OS_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_lote_Internalname, "�ltimo Lote", "", "", lblTextblockcontratada_lote_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_Lote_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ",", "")), ((edtContratada_Lote_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")) : context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Lote_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_Lote_Enabled, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratada_usaosistema_cell_Internalname+"\"  class='"+cellTextblockcontratada_usaosistema_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_usaosistema_Internalname, "Usa o Sistema", "", "", lblTextblockcontratada_usaosistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratada_usaosistema_cell_Internalname+"\"  class='"+cellContratada_usaosistema_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_UsaOSistema, cmbContratada_UsaOSistema_Internalname, StringUtil.BoolToStr( A1481Contratada_UsaOSistema), 1, cmbContratada_UsaOSistema_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbContratada_UsaOSistema.Visible, cmbContratada_UsaOSistema.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "", true, "HLP_Contratada.htm");
            cmbContratada_UsaOSistema.CurrentValue = StringUtil.BoolToStr( A1481Contratada_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_UsaOSistema_Internalname, "Values", (String)(cmbContratada_UsaOSistema.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ospreferencial_Internalname, "Preferencial", "", "", lblTextblockcontratada_ospreferencial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_115_0C13( true) ;
         }
         return  ;
      }

      protected void wb_table5_115_0C13e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_cntpadrao_Internalname, "Contrato padr�o", "", "", lblTextblockcontratada_cntpadrao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_CntPadrao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1953Contratada_CntPadrao), 6, 0, ",", "")), ((edtContratada_CntPadrao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1953Contratada_CntPadrao), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1953Contratada_CntPadrao), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_CntPadrao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_CntPadrao_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_ativo_Internalname, "Ativo", "", "", lblTextblockcontratada_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratada_Ativo_Internalname, StringUtil.BoolToStr( A43Contratada_Ativo), "", "", 1, chkContratada_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(129, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_0C13e( true) ;
         }
         else
         {
            wb_table4_14_0C13e( false) ;
         }
      }

      protected void wb_table5_115_0C13( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_ospreferencial_Internalname, tblTablemergedcontratada_ospreferencial_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_OSPreferencial, cmbContratada_OSPreferencial_Internalname, StringUtil.BoolToStr( A1867Contratada_OSPreferencial), 1, cmbContratada_OSPreferencial_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "Atendimento preferencial", 1, cmbContratada_OSPreferencial.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", "", true, "HLP_Contratada.htm");
            cmbContratada_OSPreferencial.CurrentValue = StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_OSPreferencial_Internalname, "Values", (String)(cmbContratada_OSPreferencial.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratada_ospreferencial_righttext_Internalname, "(1 OS por vez)", "", "", lblContratada_ospreferencial_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblContratada_ospreferencial_righttext_Visible, 1, 0, "HLP_Contratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_115_0C13e( true) ;
         }
         else
         {
            wb_table5_115_0C13e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E120C2 */
         E120C2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               cmbContratada_TipoFabrica.CurrentValue = cgiGet( cmbContratada_TipoFabrica_Internalname);
               A516Contratada_TipoFabrica = cgiGet( cmbContratada_TipoFabrica_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
               AV24Contratada_RazaoSocial = StringUtil.Upper( cgiGet( edtavContratada_razaosocial_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
               A1664Contratada_Logo = cgiGet( imgContratada_Logo_Internalname);
               n1664Contratada_Logo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
               n1664Contratada_Logo = (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? true : false);
               AV21Contratada_CNPJ = cgiGet( edtavContratada_cnpj_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
               AV32Pessoa_IE = cgiGet( edtavPessoa_ie_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
               A438Contratada_Sigla = StringUtil.Upper( cgiGet( edtContratada_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
               AV19Contratada_BancoNome = StringUtil.Upper( cgiGet( edtavContratada_banconome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
               AV20Contratada_BancoNro = cgiGet( edtavContratada_banconro_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
               AV17Contratada_AgenciaNome = StringUtil.Upper( cgiGet( edtavContratada_agencianome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
               AV18Contratada_AgenciaNro = cgiGet( edtavContratada_agencianro_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
               AV22Contratada_ContaCorrente = cgiGet( edtavContratada_contacorrente_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
               AV30Pessoa_Endereco = cgiGet( edtavPessoa_endereco_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
               AV29Pessoa_CEP = cgiGet( edtavPessoa_cep_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
               dynavContratada_uf.CurrentValue = cgiGet( dynavContratada_uf_Internalname);
               AV25Contratada_UF = cgiGet( dynavContratada_uf_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
               dynContratada_MunicipioCod.CurrentValue = cgiGet( dynContratada_MunicipioCod_Internalname);
               A349Contratada_MunicipioCod = (int)(NumberUtil.Val( cgiGet( dynContratada_MunicipioCod_Internalname), "."));
               n349Contratada_MunicipioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
               n349Contratada_MunicipioCod = ((0==A349Contratada_MunicipioCod) ? true : false);
               AV33Pessoa_Telefone = cgiGet( edtavPessoa_telefone_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
               AV31Pessoa_Fax = cgiGet( edtavPessoa_fax_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_SS");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_SS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1451Contratada_SS = 0;
                  n1451Contratada_SS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
               }
               else
               {
                  A1451Contratada_SS = (int)(context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", "."));
                  n1451Contratada_SS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
               }
               n1451Contratada_SS = ((0==A1451Contratada_SS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_OS");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_OS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A524Contratada_OS = 0;
                  n524Contratada_OS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
               }
               else
               {
                  A524Contratada_OS = (int)(context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", "."));
                  n524Contratada_OS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
               }
               n524Contratada_OS = ((0==A524Contratada_OS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_LOTE");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_Lote_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A530Contratada_Lote = 0;
                  n530Contratada_Lote = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
               }
               else
               {
                  A530Contratada_Lote = (short)(context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", "."));
                  n530Contratada_Lote = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
               }
               n530Contratada_Lote = ((0==A530Contratada_Lote) ? true : false);
               cmbContratada_UsaOSistema.CurrentValue = cgiGet( cmbContratada_UsaOSistema_Internalname);
               A1481Contratada_UsaOSistema = StringUtil.StrToBool( cgiGet( cmbContratada_UsaOSistema_Internalname));
               n1481Contratada_UsaOSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
               n1481Contratada_UsaOSistema = ((false==A1481Contratada_UsaOSistema) ? true : false);
               cmbContratada_OSPreferencial.CurrentValue = cgiGet( cmbContratada_OSPreferencial_Internalname);
               A1867Contratada_OSPreferencial = StringUtil.StrToBool( cgiGet( cmbContratada_OSPreferencial_Internalname));
               n1867Contratada_OSPreferencial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
               n1867Contratada_OSPreferencial = ((false==A1867Contratada_OSPreferencial) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_CntPadrao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_CntPadrao_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_CNTPADRAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_CntPadrao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1953Contratada_CntPadrao = 0;
                  n1953Contratada_CntPadrao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0)));
               }
               else
               {
                  A1953Contratada_CntPadrao = (int)(context.localUtil.CToN( cgiGet( edtContratada_CntPadrao_Internalname), ",", "."));
                  n1953Contratada_CntPadrao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0)));
               }
               n1953Contratada_CntPadrao = ((0==A1953Contratada_CntPadrao) ? true : false);
               A43Contratada_Ativo = StringUtil.StrToBool( cgiGet( chkContratada_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
               AV36Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
               AV23Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_PESSOACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_PessoaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A40Contratada_PessoaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
               }
               else
               {
                  A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
               }
               /* Read saved values. */
               Z39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z39Contratada_Codigo"), ",", "."));
               Z438Contratada_Sigla = cgiGet( "Z438Contratada_Sigla");
               Z516Contratada_TipoFabrica = cgiGet( "Z516Contratada_TipoFabrica");
               Z342Contratada_BancoNome = cgiGet( "Z342Contratada_BancoNome");
               n342Contratada_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A342Contratada_BancoNome)) ? true : false);
               Z343Contratada_BancoNro = cgiGet( "Z343Contratada_BancoNro");
               n343Contratada_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A343Contratada_BancoNro)) ? true : false);
               Z344Contratada_AgenciaNome = cgiGet( "Z344Contratada_AgenciaNome");
               n344Contratada_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A344Contratada_AgenciaNome)) ? true : false);
               Z345Contratada_AgenciaNro = cgiGet( "Z345Contratada_AgenciaNro");
               n345Contratada_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A345Contratada_AgenciaNro)) ? true : false);
               Z51Contratada_ContaCorrente = cgiGet( "Z51Contratada_ContaCorrente");
               n51Contratada_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A51Contratada_ContaCorrente)) ? true : false);
               Z524Contratada_OS = (int)(context.localUtil.CToN( cgiGet( "Z524Contratada_OS"), ",", "."));
               n524Contratada_OS = ((0==A524Contratada_OS) ? true : false);
               Z1451Contratada_SS = (int)(context.localUtil.CToN( cgiGet( "Z1451Contratada_SS"), ",", "."));
               n1451Contratada_SS = ((0==A1451Contratada_SS) ? true : false);
               Z530Contratada_Lote = (short)(context.localUtil.CToN( cgiGet( "Z530Contratada_Lote"), ",", "."));
               n530Contratada_Lote = ((0==A530Contratada_Lote) ? true : false);
               Z1481Contratada_UsaOSistema = StringUtil.StrToBool( cgiGet( "Z1481Contratada_UsaOSistema"));
               n1481Contratada_UsaOSistema = ((false==A1481Contratada_UsaOSistema) ? true : false);
               Z1867Contratada_OSPreferencial = StringUtil.StrToBool( cgiGet( "Z1867Contratada_OSPreferencial"));
               n1867Contratada_OSPreferencial = ((false==A1867Contratada_OSPreferencial) ? true : false);
               Z1953Contratada_CntPadrao = (int)(context.localUtil.CToN( cgiGet( "Z1953Contratada_CntPadrao"), ",", "."));
               n1953Contratada_CntPadrao = ((0==A1953Contratada_CntPadrao) ? true : false);
               Z43Contratada_Ativo = StringUtil.StrToBool( cgiGet( "Z43Contratada_Ativo"));
               Z40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z40Contratada_PessoaCod"), ",", "."));
               Z52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z52Contratada_AreaTrabalhoCod"), ",", "."));
               Z349Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "Z349Contratada_MunicipioCod"), ",", "."));
               n349Contratada_MunicipioCod = ((0==A349Contratada_MunicipioCod) ? true : false);
               A342Contratada_BancoNome = cgiGet( "Z342Contratada_BancoNome");
               n342Contratada_BancoNome = false;
               n342Contratada_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A342Contratada_BancoNome)) ? true : false);
               A343Contratada_BancoNro = cgiGet( "Z343Contratada_BancoNro");
               n343Contratada_BancoNro = false;
               n343Contratada_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A343Contratada_BancoNro)) ? true : false);
               A344Contratada_AgenciaNome = cgiGet( "Z344Contratada_AgenciaNome");
               n344Contratada_AgenciaNome = false;
               n344Contratada_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A344Contratada_AgenciaNome)) ? true : false);
               A345Contratada_AgenciaNro = cgiGet( "Z345Contratada_AgenciaNro");
               n345Contratada_AgenciaNro = false;
               n345Contratada_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A345Contratada_AgenciaNro)) ? true : false);
               A51Contratada_ContaCorrente = cgiGet( "Z51Contratada_ContaCorrente");
               n51Contratada_ContaCorrente = false;
               n51Contratada_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A51Contratada_ContaCorrente)) ? true : false);
               A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z52Contratada_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N52Contratada_AreaTrabalhoCod"), ",", "."));
               N40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "N40Contratada_PessoaCod"), ",", "."));
               N349Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "N349Contratada_MunicipioCod"), ",", "."));
               n349Contratada_MunicipioCod = ((0==A349Contratada_MunicipioCod) ? true : false);
               AV7Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATADA_CODIGO"), ",", "."));
               AV14Insert_Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATADA_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_AREATRABALHOCOD"), ",", "."));
               AV11Insert_Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATADA_PESSOACOD"), ",", "."));
               AV26Insert_Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATADA_MUNICIPIOCOD"), ",", "."));
               A40000Contratada_Logo_GXI = cgiGet( "CONTRATADA_LOGO_GXI");
               n40000Contratada_Logo_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? true : false);
               AV37IsConfiguracoesValidas = cgiGet( "vISCONFIGURACOESVALIDAS");
               A42Contratada_PessoaCNPJ = cgiGet( "CONTRATADA_PESSOACNPJ");
               n42Contratada_PessoaCNPJ = false;
               A41Contratada_PessoaNom = cgiGet( "CONTRATADA_PESSOANOM");
               n41Contratada_PessoaNom = false;
               A342Contratada_BancoNome = cgiGet( "CONTRATADA_BANCONOME");
               n342Contratada_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A342Contratada_BancoNome)) ? true : false);
               A343Contratada_BancoNro = cgiGet( "CONTRATADA_BANCONRO");
               n343Contratada_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A343Contratada_BancoNro)) ? true : false);
               A344Contratada_AgenciaNome = cgiGet( "CONTRATADA_AGENCIANOME");
               n344Contratada_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A344Contratada_AgenciaNome)) ? true : false);
               A345Contratada_AgenciaNro = cgiGet( "CONTRATADA_AGENCIANRO");
               n345Contratada_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A345Contratada_AgenciaNro)) ? true : false);
               A51Contratada_ContaCorrente = cgiGet( "CONTRATADA_CONTACORRENTE");
               n51Contratada_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A51Contratada_ContaCorrente)) ? true : false);
               A350Contratada_UF = cgiGet( "CONTRATADA_UF");
               n350Contratada_UF = false;
               A518Pessoa_IE = cgiGet( "PESSOA_IE");
               n518Pessoa_IE = false;
               A519Pessoa_Endereco = cgiGet( "PESSOA_ENDERECO");
               n519Pessoa_Endereco = false;
               A521Pessoa_CEP = cgiGet( "PESSOA_CEP");
               n521Pessoa_CEP = false;
               A522Pessoa_Telefone = cgiGet( "PESSOA_TELEFONE");
               n522Pessoa_Telefone = false;
               A523Pessoa_Fax = cgiGet( "PESSOA_FAX");
               n523Pessoa_Fax = false;
               AV28ok = (int)(context.localUtil.CToN( cgiGet( "vOK"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV35AuditingObject);
               A1127Contratada_LogoArquivo = cgiGet( "CONTRATADA_LOGOARQUIVO");
               n1127Contratada_LogoArquivo = false;
               n1127Contratada_LogoArquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1127Contratada_LogoArquivo)) ? true : false);
               A1129Contratada_LogoTipoArq = cgiGet( "CONTRATADA_LOGOTIPOARQ");
               n1129Contratada_LogoTipoArq = false;
               n1129Contratada_LogoTipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1129Contratada_LogoTipoArq)) ? true : false);
               A1128Contratada_LogoNomeArq = cgiGet( "CONTRATADA_LOGONOMEARQ");
               n1128Contratada_LogoNomeArq = false;
               n1128Contratada_LogoNomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1128Contratada_LogoNomeArq)) ? true : false);
               A53Contratada_AreaTrabalhoDes = cgiGet( "CONTRATADA_AREATRABALHODES");
               n53Contratada_AreaTrabalhoDes = false;
               A1592Contratada_AreaTrbClcPFnl = cgiGet( "CONTRATADA_AREATRBCLCPFNL");
               n1592Contratada_AreaTrbClcPFnl = false;
               A1595Contratada_AreaTrbSrvPdr = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_AREATRBSRVPDR"), ",", "."));
               n1595Contratada_AreaTrbSrvPdr = false;
               AV39Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgContratada_Logo_Internalname, ref  A1664Contratada_Logo, ref  A40000Contratada_Logo_GXI);
               n40000Contratada_Logo_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? true : false);
               n1664Contratada_Logo = (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? true : false);
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Contratada";
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV39Pgmname, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A39Contratada_Codigo != Z39Contratada_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratada:[SecurityCheckFailed value for]"+"Contratada_Codigo:"+context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratada:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratada:[SecurityCheckFailed value for]"+"Contratada_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("contratada:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV39Pgmname, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode13 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode13;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound13 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0C0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATADA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratada_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120C2 */
                           E120C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130C2 */
                           E130C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCONTRATADA_CNPJ.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140C2 */
                           E140C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E130C2 */
            E130C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0C13( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0C13( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_razaosocial_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_banconome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_banconome_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_banconro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_banconro_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_agencianome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_agencianome_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_agencianro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_agencianro_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_contacorrente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_contacorrente_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_endereco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_endereco_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_cep_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_cep_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_uf.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_telefone_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_fax_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacod_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0C0( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0C13( ) ;
            }
            else
            {
               CheckExtendedTable0C13( ) ;
               CloseExtendedTableCursors0C13( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0C0( )
      {
      }

      protected void E120C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV39Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV40GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GXV1), 8, 0)));
            while ( AV40GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV40GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_AreaTrabalhoCod") == 0 )
               {
                  AV14Insert_Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_Contratada_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_PessoaCod") == 0 )
               {
                  AV11Insert_Contratada_PessoaCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contratada_PessoaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_MunicipioCod") == 0 )
               {
                  AV26Insert_Contratada_MunicipioCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Insert_Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Insert_Contratada_MunicipioCod), 6, 0)));
               }
               AV40GXV1 = (int)(AV40GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GXV1), 8, 0)));
            }
         }
         edtavContrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Visible), 5, 0)));
         edtavContratada_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacod_Visible), 5, 0)));
         edtContratada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Visible), 5, 0)));
         edtContratada_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCod_Visible), 5, 0)));
         AV23Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
      }

      protected void E130C2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV35AuditingObject,  AV39Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pgmname", AV39Pgmname);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
            {
               GXt_int1 = AV36Contrato_Codigo;
               new prc_inscontratocontratadainterna(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo,  A39Contratada_Codigo,  AV24Contratada_RazaoSocial, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
               AV36Contrato_Codigo = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Codigo), 6, 0)));
               context.wjLoc = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV36Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratoServicos"));
               context.wjLocDisableFrm = 1;
            }
            else
            {
               lblTbjava_Caption = "<script languaje 'javascript'>"+StringUtil.NewLine( )+"window.history.replaceState('','','/meetrika/ViewContratada.aspx?"+StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0))+",');"+StringUtil.NewLine( )+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
               context.wjLoc = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               context.wjLocDisableFrm = 1;
            }
         }
         else if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratada.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         new wwpbaseobjects.audittransaction(context ).execute(  AV35AuditingObject,  AV39Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pgmname", AV39Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratada.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         cmbContratada_UsaOSistema.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_UsaOSistema_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_UsaOSistema.Visible), 5, 0)));
         cellContratada_usaosistema_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratada_usaosistema_cell_Internalname, "Class", cellContratada_usaosistema_cell_Class);
         cellTextblockcontratada_usaosistema_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratada_usaosistema_cell_Internalname, "Class", cellTextblockcontratada_usaosistema_cell_Class);
      }

      protected void E140C2( )
      {
         /* Contratada_cnpj_Isvalid Routine */
         if ( StringUtil.StrCmp(AV21Contratada_CNPJ, "Interno") != 0 )
         {
            if ( new prc_validadocumento(context).executeUdp(  AV21Contratada_CNPJ,  "J") )
            {
               GX_msglist.addItem("Documento inv�lido!");
            }
         }
         GXt_int1 = AV23Contratada_PessoaCod;
         new prc_retornacontratadapelocnpj(context ).execute(  AV21Contratada_CNPJ, out  AV24Contratada_RazaoSocial, out  AV19Contratada_BancoNome, out  AV20Contratada_BancoNro, out  AV17Contratada_AgenciaNome, out  AV18Contratada_AgenciaNro, out  AV22Contratada_ContaCorrente, out  A438Contratada_Sigla, out  AV32Pessoa_IE, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
         AV23Contratada_PessoaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32Pessoa_IE)) )
         {
            GX_FocusControl = edtavPessoa_ie_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            GX_FocusControl = edtavContratada_razaosocial_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( A438Contratada_Sigla)) )
         {
            GX_FocusControl = edtContratada_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            GX_FocusControl = Dvpanel_tableattributes_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
      }

      protected void ZM0C13( short GX_JID )
      {
         if ( ( GX_JID == 59 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z438Contratada_Sigla = T000C3_A438Contratada_Sigla[0];
               Z516Contratada_TipoFabrica = T000C3_A516Contratada_TipoFabrica[0];
               Z342Contratada_BancoNome = T000C3_A342Contratada_BancoNome[0];
               Z343Contratada_BancoNro = T000C3_A343Contratada_BancoNro[0];
               Z344Contratada_AgenciaNome = T000C3_A344Contratada_AgenciaNome[0];
               Z345Contratada_AgenciaNro = T000C3_A345Contratada_AgenciaNro[0];
               Z51Contratada_ContaCorrente = T000C3_A51Contratada_ContaCorrente[0];
               Z524Contratada_OS = T000C3_A524Contratada_OS[0];
               Z1451Contratada_SS = T000C3_A1451Contratada_SS[0];
               Z530Contratada_Lote = T000C3_A530Contratada_Lote[0];
               Z1481Contratada_UsaOSistema = T000C3_A1481Contratada_UsaOSistema[0];
               Z1867Contratada_OSPreferencial = T000C3_A1867Contratada_OSPreferencial[0];
               Z1953Contratada_CntPadrao = T000C3_A1953Contratada_CntPadrao[0];
               Z43Contratada_Ativo = T000C3_A43Contratada_Ativo[0];
               Z40Contratada_PessoaCod = T000C3_A40Contratada_PessoaCod[0];
               Z52Contratada_AreaTrabalhoCod = T000C3_A52Contratada_AreaTrabalhoCod[0];
               Z349Contratada_MunicipioCod = T000C3_A349Contratada_MunicipioCod[0];
            }
            else
            {
               Z438Contratada_Sigla = A438Contratada_Sigla;
               Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
               Z342Contratada_BancoNome = A342Contratada_BancoNome;
               Z343Contratada_BancoNro = A343Contratada_BancoNro;
               Z344Contratada_AgenciaNome = A344Contratada_AgenciaNome;
               Z345Contratada_AgenciaNro = A345Contratada_AgenciaNro;
               Z51Contratada_ContaCorrente = A51Contratada_ContaCorrente;
               Z524Contratada_OS = A524Contratada_OS;
               Z1451Contratada_SS = A1451Contratada_SS;
               Z530Contratada_Lote = A530Contratada_Lote;
               Z1481Contratada_UsaOSistema = A1481Contratada_UsaOSistema;
               Z1867Contratada_OSPreferencial = A1867Contratada_OSPreferencial;
               Z1953Contratada_CntPadrao = A1953Contratada_CntPadrao;
               Z43Contratada_Ativo = A43Contratada_Ativo;
               Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
               Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
               Z349Contratada_MunicipioCod = A349Contratada_MunicipioCod;
            }
         }
         if ( GX_JID == -59 )
         {
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z342Contratada_BancoNome = A342Contratada_BancoNome;
            Z343Contratada_BancoNro = A343Contratada_BancoNro;
            Z344Contratada_AgenciaNome = A344Contratada_AgenciaNome;
            Z345Contratada_AgenciaNro = A345Contratada_AgenciaNro;
            Z51Contratada_ContaCorrente = A51Contratada_ContaCorrente;
            Z524Contratada_OS = A524Contratada_OS;
            Z1451Contratada_SS = A1451Contratada_SS;
            Z530Contratada_Lote = A530Contratada_Lote;
            Z1127Contratada_LogoArquivo = A1127Contratada_LogoArquivo;
            Z1481Contratada_UsaOSistema = A1481Contratada_UsaOSistema;
            Z1867Contratada_OSPreferencial = A1867Contratada_OSPreferencial;
            Z1953Contratada_CntPadrao = A1953Contratada_CntPadrao;
            Z43Contratada_Ativo = A43Contratada_Ativo;
            Z1664Contratada_Logo = A1664Contratada_Logo;
            Z1129Contratada_LogoTipoArq = A1129Contratada_LogoTipoArq;
            Z1128Contratada_LogoNomeArq = A1128Contratada_LogoNomeArq;
            Z40000Contratada_Logo_GXI = A40000Contratada_Logo_GXI;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z349Contratada_MunicipioCod = A349Contratada_MunicipioCod;
            Z53Contratada_AreaTrabalhoDes = A53Contratada_AreaTrabalhoDes;
            Z1592Contratada_AreaTrbClcPFnl = A1592Contratada_AreaTrbClcPFnl;
            Z1595Contratada_AreaTrbSrvPdr = A1595Contratada_AreaTrbSrvPdr;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z350Contratada_UF = A350Contratada_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvCONTRATADA_UF_html0C13( ) ;
         edtContratada_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
         AV39Pgmname = "Contratada";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Pgmname", AV39Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContratada_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Contratada_Codigo) )
         {
            A39Contratada_Codigo = AV7Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         cmbContratada_UsaOSistema.Visible = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_UsaOSistema_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_UsaOSistema.Visible), 5, 0)));
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellContratada_usaosistema_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratada_usaosistema_cell_Internalname, "Class", cellContratada_usaosistema_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellContratada_usaosistema_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratada_usaosistema_cell_Internalname, "Class", cellContratada_usaosistema_cell_Class);
            }
         }
         if ( ! ( AV8WWPContext.gxTpr_Userehadministradorgam ) )
         {
            cellTextblockcontratada_usaosistema_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratada_usaosistema_cell_Internalname, "Class", cellTextblockcontratada_usaosistema_cell_Class);
         }
         else
         {
            if ( AV8WWPContext.gxTpr_Userehadministradorgam )
            {
               cellTextblockcontratada_usaosistema_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratada_usaosistema_cell_Internalname, "Class", cellTextblockcontratada_usaosistema_cell_Class);
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratada_PessoaCod) )
         {
            edtContratada_PessoaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratada_PessoaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV26Insert_Contratada_MunicipioCod) )
         {
            dynContratada_MunicipioCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_MunicipioCod.Enabled), 5, 0)));
         }
         else
         {
            dynContratada_MunicipioCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_MunicipioCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         edtavContratada_cnpj_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Enabled), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV26Insert_Contratada_MunicipioCod) )
         {
            A349Contratada_MunicipioCod = AV26Insert_Contratada_MunicipioCod;
            n349Contratada_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratada_PessoaCod) )
         {
            A40Contratada_PessoaCod = AV11Insert_Contratada_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Contratada_AreaTrabalhoCod) )
         {
            A52Contratada_AreaTrabalhoCod = AV14Insert_Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A52Contratada_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A43Contratada_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A43Contratada_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A516Contratada_TipoFabrica)) && ( Gx_BScreen == 0 ) )
         {
            A516Contratada_TipoFabrica = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000C6 */
            pr_default.execute(4, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
            A350Contratada_UF = T000C6_A350Contratada_UF[0];
            n350Contratada_UF = T000C6_n350Contratada_UF[0];
            pr_default.close(4);
            /* Using cursor T000C4 */
            pr_default.execute(2, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000C4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = T000C4_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000C4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T000C4_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = T000C4_A518Pessoa_IE[0];
            n518Pessoa_IE = T000C4_n518Pessoa_IE[0];
            A519Pessoa_Endereco = T000C4_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = T000C4_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = T000C4_A521Pessoa_CEP[0];
            n521Pessoa_CEP = T000C4_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = T000C4_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = T000C4_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = T000C4_A523Pessoa_Fax[0];
            n523Pessoa_Fax = T000C4_n523Pessoa_Fax[0];
            pr_default.close(2);
            /* Using cursor T000C5 */
            pr_default.execute(3, new Object[] {A52Contratada_AreaTrabalhoCod});
            A53Contratada_AreaTrabalhoDes = T000C5_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = T000C5_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = T000C5_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = T000C5_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = T000C5_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = T000C5_n1595Contratada_AreaTrbSrvPdr[0];
            pr_default.close(3);
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               edtavPessoa_ie_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockpessoa_ie_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpessoa_ie_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               edtavContratada_cnpj_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockcontratada_cnpj_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratada_cnpj_Visible), 5, 0)));
            }
         }
      }

      protected void Load0C13( )
      {
         /* Using cursor T000C7 */
         pr_default.execute(5, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound13 = 1;
            A53Contratada_AreaTrabalhoDes = T000C7_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = T000C7_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = T000C7_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = T000C7_n1592Contratada_AreaTrbClcPFnl[0];
            A41Contratada_PessoaNom = T000C7_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = T000C7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000C7_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T000C7_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = T000C7_A518Pessoa_IE[0];
            n518Pessoa_IE = T000C7_n518Pessoa_IE[0];
            A519Pessoa_Endereco = T000C7_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = T000C7_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = T000C7_A521Pessoa_CEP[0];
            n521Pessoa_CEP = T000C7_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = T000C7_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = T000C7_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = T000C7_A523Pessoa_Fax[0];
            n523Pessoa_Fax = T000C7_n523Pessoa_Fax[0];
            A438Contratada_Sigla = T000C7_A438Contratada_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
            A516Contratada_TipoFabrica = T000C7_A516Contratada_TipoFabrica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            A342Contratada_BancoNome = T000C7_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = T000C7_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = T000C7_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = T000C7_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = T000C7_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = T000C7_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = T000C7_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = T000C7_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = T000C7_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = T000C7_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = T000C7_A524Contratada_OS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
            n524Contratada_OS = T000C7_n524Contratada_OS[0];
            A1451Contratada_SS = T000C7_A1451Contratada_SS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
            n1451Contratada_SS = T000C7_n1451Contratada_SS[0];
            A530Contratada_Lote = T000C7_A530Contratada_Lote[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
            n530Contratada_Lote = T000C7_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = T000C7_A1481Contratada_UsaOSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
            n1481Contratada_UsaOSistema = T000C7_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = T000C7_A1867Contratada_OSPreferencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
            n1867Contratada_OSPreferencial = T000C7_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = T000C7_A1953Contratada_CntPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0)));
            n1953Contratada_CntPadrao = T000C7_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = T000C7_A43Contratada_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
            A1129Contratada_LogoTipoArq = T000C7_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = T000C7_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = T000C7_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = T000C7_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40000Contratada_Logo_GXI = T000C7_A40000Contratada_Logo_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            n40000Contratada_Logo_GXI = T000C7_n40000Contratada_Logo_GXI[0];
            A40Contratada_PessoaCod = T000C7_A40Contratada_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            A52Contratada_AreaTrabalhoCod = T000C7_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = T000C7_A349Contratada_MunicipioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            n349Contratada_MunicipioCod = T000C7_n349Contratada_MunicipioCod[0];
            A1595Contratada_AreaTrbSrvPdr = T000C7_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = T000C7_n1595Contratada_AreaTrbSrvPdr[0];
            A350Contratada_UF = T000C7_A350Contratada_UF[0];
            n350Contratada_UF = T000C7_n350Contratada_UF[0];
            A1127Contratada_LogoArquivo = T000C7_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = T000C7_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = T000C7_A1664Contratada_Logo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            n1664Contratada_Logo = T000C7_n1664Contratada_Logo[0];
            ZM0C13( -59) ;
         }
         pr_default.close(5);
         OnLoadActions0C13( ) ;
      }

      protected void OnLoadActions0C13( )
      {
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==AV23Contratada_PessoaCod) )
         {
            AV23Contratada_PessoaCod = A40Contratada_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            edtavPessoa_ie_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockpessoa_ie_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpessoa_ie_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            edtavContratada_cnpj_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockcontratada_cnpj_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratada_cnpj_Visible), 5, 0)));
         }
         lblContratada_ospreferencial_righttext_Visible = (A1867Contratada_OSPreferencial ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratada_ospreferencial_righttext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblContratada_ospreferencial_righttext_Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            AV24Contratada_RazaoSocial = StringUtil.Trim( A41Contratada_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
         {
            AV21Contratada_CNPJ = A42Contratada_PessoaCNPJ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV32Pessoa_IE)) )
         {
            AV32Pessoa_IE = A518Pessoa_IE;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_BancoNome)) )
         {
            AV19Contratada_BancoNome = StringUtil.Trim( A342Contratada_BancoNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV20Contratada_BancoNro)) )
         {
            AV20Contratada_BancoNro = StringUtil.Trim( A343Contratada_BancoNro);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_AgenciaNome)) )
         {
            AV17Contratada_AgenciaNome = StringUtil.Trim( A344Contratada_AgenciaNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AgenciaNro)) )
         {
            AV18Contratada_AgenciaNro = StringUtil.Trim( A345Contratada_AgenciaNro);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_ContaCorrente)) )
         {
            AV22Contratada_ContaCorrente = StringUtil.Trim( A51Contratada_ContaCorrente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV30Pessoa_Endereco)) )
         {
            AV30Pessoa_Endereco = A519Pessoa_Endereco;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV29Pessoa_CEP)) )
         {
            AV29Pessoa_CEP = A521Pessoa_CEP;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV25Contratada_UF)) )
         {
            AV25Contratada_UF = A350Contratada_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
         }
         GXACONTRATADA_MUNICIPIOCOD_html0C13( AV25Contratada_UF) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV33Pessoa_Telefone)) )
         {
            AV33Pessoa_Telefone = A522Pessoa_Telefone;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV31Pessoa_Fax)) )
         {
            AV31Pessoa_Fax = A523Pessoa_Fax;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
         }
      }

      protected void CheckExtendedTable0C13( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==AV23Contratada_PessoaCod) )
         {
            AV23Contratada_PessoaCod = A40Contratada_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
         }
         /* Using cursor T000C4 */
         pr_default.execute(2, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            new prc_inserepessoadadosbasicos(context ).execute( ref  A40Contratada_PessoaCod,  AV24Contratada_RazaoSocial,  "J",  AV21Contratada_CNPJ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
            /* Using cursor T000C4 */
            pr_default.execute(2, new Object[] {A40Contratada_PessoaCod});
            if ( (pr_default.getStatus(2) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "CONTRATADA_PESSOACOD");
               AnyError = 1;
               GX_FocusControl = edtContratada_PessoaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A41Contratada_PessoaNom = T000C4_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T000C4_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000C4_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T000C4_n42Contratada_PessoaCNPJ[0];
         A518Pessoa_IE = T000C4_A518Pessoa_IE[0];
         n518Pessoa_IE = T000C4_n518Pessoa_IE[0];
         A519Pessoa_Endereco = T000C4_A519Pessoa_Endereco[0];
         n519Pessoa_Endereco = T000C4_n519Pessoa_Endereco[0];
         A521Pessoa_CEP = T000C4_A521Pessoa_CEP[0];
         n521Pessoa_CEP = T000C4_n521Pessoa_CEP[0];
         A522Pessoa_Telefone = T000C4_A522Pessoa_Telefone[0];
         n522Pessoa_Telefone = T000C4_n522Pessoa_Telefone[0];
         A523Pessoa_Fax = T000C4_A523Pessoa_Fax[0];
         n523Pessoa_Fax = T000C4_n523Pessoa_Fax[0];
         pr_default.close(2);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A438Contratada_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "CONTRATADA_SIGLA");
            AnyError = 1;
            GX_FocusControl = edtContratada_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "S") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "M") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "O") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "CONTRATADA_TIPOFABRICA");
            AnyError = 1;
            GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            edtavPessoa_ie_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockpessoa_ie_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpessoa_ie_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            edtavContratada_cnpj_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Visible), 5, 0)));
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockcontratada_cnpj_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratada_cnpj_Visible), 5, 0)));
         }
         /* Using cursor T000C6 */
         pr_default.execute(4, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A349Contratada_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratada_Municipio'.", "ForeignKeyNotFound", 1, "CONTRATADA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynContratada_MunicipioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A350Contratada_UF = T000C6_A350Contratada_UF[0];
         n350Contratada_UF = T000C6_n350Contratada_UF[0];
         pr_default.close(4);
         lblContratada_ospreferencial_righttext_Visible = (A1867Contratada_OSPreferencial ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratada_ospreferencial_righttext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblContratada_ospreferencial_righttext_Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            AV24Contratada_RazaoSocial = StringUtil.Trim( A41Contratada_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            GX_msglist.addItem("Raz�o Social � obrigat�rio.", 1, "vCONTRATADA_RAZAOSOCIAL");
            AnyError = 1;
            GX_FocusControl = edtavContratada_razaosocial_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
         {
            AV21Contratada_CNPJ = A42Contratada_PessoaCNPJ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
         {
            GX_msglist.addItem("CNPJ � obrigat�rio.", 1, "CONTRATADA_TIPOFABRICA");
            AnyError = 1;
            GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV32Pessoa_IE)) )
         {
            AV32Pessoa_IE = A518Pessoa_IE;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_BancoNome)) )
         {
            AV19Contratada_BancoNome = StringUtil.Trim( A342Contratada_BancoNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV20Contratada_BancoNro)) )
         {
            AV20Contratada_BancoNro = StringUtil.Trim( A343Contratada_BancoNro);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_AgenciaNome)) )
         {
            AV17Contratada_AgenciaNome = StringUtil.Trim( A344Contratada_AgenciaNome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AgenciaNro)) )
         {
            AV18Contratada_AgenciaNro = StringUtil.Trim( A345Contratada_AgenciaNro);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_ContaCorrente)) )
         {
            AV22Contratada_ContaCorrente = StringUtil.Trim( A51Contratada_ContaCorrente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV30Pessoa_Endereco)) )
         {
            AV30Pessoa_Endereco = A519Pessoa_Endereco;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV29Pessoa_CEP)) )
         {
            AV29Pessoa_CEP = A521Pessoa_CEP;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV25Contratada_UF)) )
         {
            AV25Contratada_UF = A350Contratada_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
         }
         GXACONTRATADA_MUNICIPIOCOD_html0C13( AV25Contratada_UF) ;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV33Pessoa_Telefone)) )
         {
            AV33Pessoa_Telefone = A522Pessoa_Telefone;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV31Pessoa_Fax)) )
         {
            AV31Pessoa_Fax = A523Pessoa_Fax;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
         }
         /* Using cursor T000C5 */
         pr_default.execute(3, new Object[] {A52Contratada_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A53Contratada_AreaTrabalhoDes = T000C5_A53Contratada_AreaTrabalhoDes[0];
         n53Contratada_AreaTrabalhoDes = T000C5_n53Contratada_AreaTrabalhoDes[0];
         A1592Contratada_AreaTrbClcPFnl = T000C5_A1592Contratada_AreaTrbClcPFnl[0];
         n1592Contratada_AreaTrbClcPFnl = T000C5_n1592Contratada_AreaTrbClcPFnl[0];
         A1595Contratada_AreaTrbSrvPdr = T000C5_A1595Contratada_AreaTrbSrvPdr[0];
         n1595Contratada_AreaTrbSrvPdr = T000C5_n1595Contratada_AreaTrbSrvPdr[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors0C13( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_60( int A40Contratada_PessoaCod )
      {
         /* Using cursor T000C8 */
         pr_default.execute(6, new Object[] {A40Contratada_PessoaCod});
         A41Contratada_PessoaNom = T000C8_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T000C8_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000C8_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T000C8_n42Contratada_PessoaCNPJ[0];
         A518Pessoa_IE = T000C8_A518Pessoa_IE[0];
         n518Pessoa_IE = T000C8_n518Pessoa_IE[0];
         A519Pessoa_Endereco = T000C8_A519Pessoa_Endereco[0];
         n519Pessoa_Endereco = T000C8_n519Pessoa_Endereco[0];
         A521Pessoa_CEP = T000C8_A521Pessoa_CEP[0];
         n521Pessoa_CEP = T000C8_n521Pessoa_CEP[0];
         A522Pessoa_Telefone = T000C8_A522Pessoa_Telefone[0];
         n522Pessoa_Telefone = T000C8_n522Pessoa_Telefone[0];
         A523Pessoa_Fax = T000C8_A523Pessoa_Fax[0];
         n523Pessoa_Fax = T000C8_n523Pessoa_Fax[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A42Contratada_PessoaCNPJ)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A518Pessoa_IE))+"\""+","+"\""+GXUtil.EncodeJSConstant( A519Pessoa_Endereco)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A521Pessoa_CEP))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A522Pessoa_Telefone))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A523Pessoa_Fax))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_62( int A349Contratada_MunicipioCod )
      {
         /* Using cursor T000C9 */
         pr_default.execute(7, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A349Contratada_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratada_Municipio'.", "ForeignKeyNotFound", 1, "CONTRATADA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynContratada_MunicipioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A350Contratada_UF = T000C9_A350Contratada_UF[0];
         n350Contratada_UF = T000C9_n350Contratada_UF[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A350Contratada_UF))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_61( int A52Contratada_AreaTrabalhoCod )
      {
         /* Using cursor T000C10 */
         pr_default.execute(8, new Object[] {A52Contratada_AreaTrabalhoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A53Contratada_AreaTrabalhoDes = T000C10_A53Contratada_AreaTrabalhoDes[0];
         n53Contratada_AreaTrabalhoDes = T000C10_n53Contratada_AreaTrabalhoDes[0];
         A1592Contratada_AreaTrbClcPFnl = T000C10_A1592Contratada_AreaTrbClcPFnl[0];
         n1592Contratada_AreaTrbClcPFnl = T000C10_n1592Contratada_AreaTrbClcPFnl[0];
         A1595Contratada_AreaTrbSrvPdr = T000C10_A1595Contratada_AreaTrbSrvPdr[0];
         n1595Contratada_AreaTrbSrvPdr = T000C10_n1595Contratada_AreaTrbSrvPdr[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A53Contratada_AreaTrabalhoDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1592Contratada_AreaTrbClcPFnl))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey0C13( )
      {
         /* Using cursor T000C11 */
         pr_default.execute(9, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound13 = 1;
         }
         else
         {
            RcdFound13 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000C3 */
         pr_default.execute(1, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0C13( 59) ;
            RcdFound13 = 1;
            A39Contratada_Codigo = T000C3_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            A438Contratada_Sigla = T000C3_A438Contratada_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
            A516Contratada_TipoFabrica = T000C3_A516Contratada_TipoFabrica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            A342Contratada_BancoNome = T000C3_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = T000C3_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = T000C3_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = T000C3_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = T000C3_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = T000C3_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = T000C3_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = T000C3_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = T000C3_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = T000C3_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = T000C3_A524Contratada_OS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
            n524Contratada_OS = T000C3_n524Contratada_OS[0];
            A1451Contratada_SS = T000C3_A1451Contratada_SS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
            n1451Contratada_SS = T000C3_n1451Contratada_SS[0];
            A530Contratada_Lote = T000C3_A530Contratada_Lote[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
            n530Contratada_Lote = T000C3_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = T000C3_A1481Contratada_UsaOSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
            n1481Contratada_UsaOSistema = T000C3_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = T000C3_A1867Contratada_OSPreferencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
            n1867Contratada_OSPreferencial = T000C3_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = T000C3_A1953Contratada_CntPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0)));
            n1953Contratada_CntPadrao = T000C3_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = T000C3_A43Contratada_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
            A1129Contratada_LogoTipoArq = T000C3_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = T000C3_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = T000C3_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = T000C3_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40000Contratada_Logo_GXI = T000C3_A40000Contratada_Logo_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            n40000Contratada_Logo_GXI = T000C3_n40000Contratada_Logo_GXI[0];
            A40Contratada_PessoaCod = T000C3_A40Contratada_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            A52Contratada_AreaTrabalhoCod = T000C3_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = T000C3_A349Contratada_MunicipioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            n349Contratada_MunicipioCod = T000C3_n349Contratada_MunicipioCod[0];
            A1127Contratada_LogoArquivo = T000C3_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = T000C3_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = T000C3_A1664Contratada_Logo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            n1664Contratada_Logo = T000C3_n1664Contratada_Logo[0];
            Z39Contratada_Codigo = A39Contratada_Codigo;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0C13( ) ;
            if ( AnyError == 1 )
            {
               RcdFound13 = 0;
               InitializeNonKey0C13( ) ;
            }
            Gx_mode = sMode13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound13 = 0;
            InitializeNonKey0C13( ) ;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0C13( ) ;
         if ( RcdFound13 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound13 = 0;
         /* Using cursor T000C12 */
         pr_default.execute(10, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T000C12_A39Contratada_Codigo[0] < A39Contratada_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T000C12_A39Contratada_Codigo[0] > A39Contratada_Codigo ) ) )
            {
               A39Contratada_Codigo = T000C12_A39Contratada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               RcdFound13 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound13 = 0;
         /* Using cursor T000C13 */
         pr_default.execute(11, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000C13_A39Contratada_Codigo[0] > A39Contratada_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000C13_A39Contratada_Codigo[0] < A39Contratada_Codigo ) ) )
            {
               A39Contratada_Codigo = T000C13_A39Contratada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               RcdFound13 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0C13( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0C13( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound13 == 1 )
            {
               if ( A39Contratada_Codigo != Z39Contratada_Codigo )
               {
                  A39Contratada_Codigo = Z39Contratada_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATADA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0C13( ) ;
                  GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A39Contratada_Codigo != Z39Contratada_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0C13( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATADA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratada_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0C13( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A39Contratada_Codigo != Z39Contratada_Codigo )
         {
            A39Contratada_Codigo = Z39Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbContratada_TipoFabrica_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000C2 */
            pr_default.execute(0, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratada"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z438Contratada_Sigla, T000C2_A438Contratada_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z516Contratada_TipoFabrica, T000C2_A516Contratada_TipoFabrica[0]) != 0 ) || ( StringUtil.StrCmp(Z342Contratada_BancoNome, T000C2_A342Contratada_BancoNome[0]) != 0 ) || ( StringUtil.StrCmp(Z343Contratada_BancoNro, T000C2_A343Contratada_BancoNro[0]) != 0 ) || ( StringUtil.StrCmp(Z344Contratada_AgenciaNome, T000C2_A344Contratada_AgenciaNome[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z345Contratada_AgenciaNro, T000C2_A345Contratada_AgenciaNro[0]) != 0 ) || ( StringUtil.StrCmp(Z51Contratada_ContaCorrente, T000C2_A51Contratada_ContaCorrente[0]) != 0 ) || ( Z524Contratada_OS != T000C2_A524Contratada_OS[0] ) || ( Z1451Contratada_SS != T000C2_A1451Contratada_SS[0] ) || ( Z530Contratada_Lote != T000C2_A530Contratada_Lote[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1481Contratada_UsaOSistema != T000C2_A1481Contratada_UsaOSistema[0] ) || ( Z1867Contratada_OSPreferencial != T000C2_A1867Contratada_OSPreferencial[0] ) || ( Z1953Contratada_CntPadrao != T000C2_A1953Contratada_CntPadrao[0] ) || ( Z43Contratada_Ativo != T000C2_A43Contratada_Ativo[0] ) || ( Z40Contratada_PessoaCod != T000C2_A40Contratada_PessoaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z52Contratada_AreaTrabalhoCod != T000C2_A52Contratada_AreaTrabalhoCod[0] ) || ( Z349Contratada_MunicipioCod != T000C2_A349Contratada_MunicipioCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z438Contratada_Sigla, T000C2_A438Contratada_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z438Contratada_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A438Contratada_Sigla[0]);
               }
               if ( StringUtil.StrCmp(Z516Contratada_TipoFabrica, T000C2_A516Contratada_TipoFabrica[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_TipoFabrica");
                  GXUtil.WriteLogRaw("Old: ",Z516Contratada_TipoFabrica);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A516Contratada_TipoFabrica[0]);
               }
               if ( StringUtil.StrCmp(Z342Contratada_BancoNome, T000C2_A342Contratada_BancoNome[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_BancoNome");
                  GXUtil.WriteLogRaw("Old: ",Z342Contratada_BancoNome);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A342Contratada_BancoNome[0]);
               }
               if ( StringUtil.StrCmp(Z343Contratada_BancoNro, T000C2_A343Contratada_BancoNro[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_BancoNro");
                  GXUtil.WriteLogRaw("Old: ",Z343Contratada_BancoNro);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A343Contratada_BancoNro[0]);
               }
               if ( StringUtil.StrCmp(Z344Contratada_AgenciaNome, T000C2_A344Contratada_AgenciaNome[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_AgenciaNome");
                  GXUtil.WriteLogRaw("Old: ",Z344Contratada_AgenciaNome);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A344Contratada_AgenciaNome[0]);
               }
               if ( StringUtil.StrCmp(Z345Contratada_AgenciaNro, T000C2_A345Contratada_AgenciaNro[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_AgenciaNro");
                  GXUtil.WriteLogRaw("Old: ",Z345Contratada_AgenciaNro);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A345Contratada_AgenciaNro[0]);
               }
               if ( StringUtil.StrCmp(Z51Contratada_ContaCorrente, T000C2_A51Contratada_ContaCorrente[0]) != 0 )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_ContaCorrente");
                  GXUtil.WriteLogRaw("Old: ",Z51Contratada_ContaCorrente);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A51Contratada_ContaCorrente[0]);
               }
               if ( Z524Contratada_OS != T000C2_A524Contratada_OS[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_OS");
                  GXUtil.WriteLogRaw("Old: ",Z524Contratada_OS);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A524Contratada_OS[0]);
               }
               if ( Z1451Contratada_SS != T000C2_A1451Contratada_SS[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_SS");
                  GXUtil.WriteLogRaw("Old: ",Z1451Contratada_SS);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A1451Contratada_SS[0]);
               }
               if ( Z530Contratada_Lote != T000C2_A530Contratada_Lote[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_Lote");
                  GXUtil.WriteLogRaw("Old: ",Z530Contratada_Lote);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A530Contratada_Lote[0]);
               }
               if ( Z1481Contratada_UsaOSistema != T000C2_A1481Contratada_UsaOSistema[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_UsaOSistema");
                  GXUtil.WriteLogRaw("Old: ",Z1481Contratada_UsaOSistema);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A1481Contratada_UsaOSistema[0]);
               }
               if ( Z1867Contratada_OSPreferencial != T000C2_A1867Contratada_OSPreferencial[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_OSPreferencial");
                  GXUtil.WriteLogRaw("Old: ",Z1867Contratada_OSPreferencial);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A1867Contratada_OSPreferencial[0]);
               }
               if ( Z1953Contratada_CntPadrao != T000C2_A1953Contratada_CntPadrao[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_CntPadrao");
                  GXUtil.WriteLogRaw("Old: ",Z1953Contratada_CntPadrao);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A1953Contratada_CntPadrao[0]);
               }
               if ( Z43Contratada_Ativo != T000C2_A43Contratada_Ativo[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z43Contratada_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A43Contratada_Ativo[0]);
               }
               if ( Z40Contratada_PessoaCod != T000C2_A40Contratada_PessoaCod[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_PessoaCod");
                  GXUtil.WriteLogRaw("Old: ",Z40Contratada_PessoaCod);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A40Contratada_PessoaCod[0]);
               }
               if ( Z52Contratada_AreaTrabalhoCod != T000C2_A52Contratada_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z52Contratada_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A52Contratada_AreaTrabalhoCod[0]);
               }
               if ( Z349Contratada_MunicipioCod != T000C2_A349Contratada_MunicipioCod[0] )
               {
                  GXUtil.WriteLog("contratada:[seudo value changed for attri]"+"Contratada_MunicipioCod");
                  GXUtil.WriteLogRaw("Old: ",Z349Contratada_MunicipioCod);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A349Contratada_MunicipioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contratada"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C13( 0) ;
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C14 */
                     A1129Contratada_LogoTipoArq = A1127Contratada_LogoArquivo_Filetype;
                     n1129Contratada_LogoTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1129Contratada_LogoTipoArq", A1129Contratada_LogoTipoArq);
                     A1128Contratada_LogoNomeArq = A1127Contratada_LogoArquivo_Filename;
                     n1128Contratada_LogoNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1128Contratada_LogoNomeArq", A1128Contratada_LogoNomeArq);
                     pr_default.execute(12, new Object[] {A438Contratada_Sigla, A516Contratada_TipoFabrica, n342Contratada_BancoNome, A342Contratada_BancoNome, n343Contratada_BancoNro, A343Contratada_BancoNro, n344Contratada_AgenciaNome, A344Contratada_AgenciaNome, n345Contratada_AgenciaNro, A345Contratada_AgenciaNro, n51Contratada_ContaCorrente, A51Contratada_ContaCorrente, n524Contratada_OS, A524Contratada_OS, n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, n1127Contratada_LogoArquivo, A1127Contratada_LogoArquivo, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, n1867Contratada_OSPreferencial, A1867Contratada_OSPreferencial, n1953Contratada_CntPadrao, A1953Contratada_CntPadrao, A43Contratada_Ativo, n1664Contratada_Logo, A1664Contratada_Logo, n1129Contratada_LogoTipoArq, A1129Contratada_LogoTipoArq, n1128Contratada_LogoNomeArq, A1128Contratada_LogoNomeArq, n40000Contratada_Logo_GXI, A40000Contratada_Logo_GXI, A40Contratada_PessoaCod, A52Contratada_AreaTrabalhoCod, n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
                     A39Contratada_Codigo = T000C14_A39Contratada_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0C0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C13( ) ;
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void Update0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C15 */
                     A1129Contratada_LogoTipoArq = A1127Contratada_LogoArquivo_Filetype;
                     n1129Contratada_LogoTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1129Contratada_LogoTipoArq", A1129Contratada_LogoTipoArq);
                     A1128Contratada_LogoNomeArq = A1127Contratada_LogoArquivo_Filename;
                     n1128Contratada_LogoNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1128Contratada_LogoNomeArq", A1128Contratada_LogoNomeArq);
                     pr_default.execute(13, new Object[] {A438Contratada_Sigla, A516Contratada_TipoFabrica, n342Contratada_BancoNome, A342Contratada_BancoNome, n343Contratada_BancoNro, A343Contratada_BancoNro, n344Contratada_AgenciaNome, A344Contratada_AgenciaNome, n345Contratada_AgenciaNro, A345Contratada_AgenciaNro, n51Contratada_ContaCorrente, A51Contratada_ContaCorrente, n524Contratada_OS, A524Contratada_OS, n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, n1867Contratada_OSPreferencial, A1867Contratada_OSPreferencial, n1953Contratada_CntPadrao, A1953Contratada_CntPadrao, A43Contratada_Ativo, n1129Contratada_LogoTipoArq, A1129Contratada_LogoTipoArq, n1128Contratada_LogoNomeArq, A1128Contratada_LogoNomeArq, A40Contratada_PessoaCod, A52Contratada_AreaTrabalhoCod, n349Contratada_MunicipioCod, A349Contratada_MunicipioCod, A39Contratada_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratada"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        GXt_int1 = AV28ok;
                        new prc_novapessoacontratada(context ).execute(  A40Contratada_PessoaCod,  AV21Contratada_CNPJ,  AV24Contratada_RazaoSocial,  AV32Pessoa_IE,  AV30Pessoa_Endereco,  AV29Pessoa_CEP,  AV33Pessoa_Telefone,  AV31Pessoa_Fax,  A349Contratada_MunicipioCod,  AV8WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_int1) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
                        AV28ok = GXt_int1;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ok", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ok), 6, 0)));
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void DeferredUpdate0C13( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000C16 */
            pr_default.execute(14, new Object[] {n1127Contratada_LogoArquivo, A1127Contratada_LogoArquivo, A39Contratada_Codigo});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000C17 */
            pr_default.execute(15, new Object[] {n1664Contratada_Logo, A1664Contratada_Logo, n40000Contratada_Logo_GXI, A40000Contratada_Logo_GXI, A39Contratada_Codigo});
            pr_default.close(15);
            dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C13( ) ;
            AfterConfirm0C13( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C13( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000C18 */
                  pr_default.execute(16, new Object[] {A39Contratada_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode13 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0C13( ) ;
         Gx_mode = sMode13;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0C13( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000C19 */
            pr_default.execute(17, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000C19_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = T000C19_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000C19_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = T000C19_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = T000C19_A518Pessoa_IE[0];
            n518Pessoa_IE = T000C19_n518Pessoa_IE[0];
            A519Pessoa_Endereco = T000C19_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = T000C19_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = T000C19_A521Pessoa_CEP[0];
            n521Pessoa_CEP = T000C19_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = T000C19_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = T000C19_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = T000C19_A523Pessoa_Fax[0];
            n523Pessoa_Fax = T000C19_n523Pessoa_Fax[0];
            pr_default.close(17);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==AV23Contratada_PessoaCod) )
            {
               AV23Contratada_PessoaCod = A40Contratada_PessoaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               edtavPessoa_ie_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockpessoa_ie_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpessoa_ie_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               edtavContratada_cnpj_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Visible), 5, 0)));
            }
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockcontratada_cnpj_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratada_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratada_cnpj_Visible), 5, 0)));
            }
            /* Using cursor T000C20 */
            pr_default.execute(18, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
            A350Contratada_UF = T000C20_A350Contratada_UF[0];
            n350Contratada_UF = T000C20_n350Contratada_UF[0];
            pr_default.close(18);
            lblContratada_ospreferencial_righttext_Visible = (A1867Contratada_OSPreferencial ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContratada_ospreferencial_righttext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblContratada_ospreferencial_righttext_Visible), 5, 0)));
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
            {
               AV24Contratada_RazaoSocial = StringUtil.Trim( A41Contratada_PessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
            {
               AV21Contratada_CNPJ = A42Contratada_PessoaCNPJ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV32Pessoa_IE)) )
            {
               AV32Pessoa_IE = A518Pessoa_IE;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV30Pessoa_Endereco)) )
            {
               AV30Pessoa_Endereco = A519Pessoa_Endereco;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV29Pessoa_CEP)) )
            {
               AV29Pessoa_CEP = A521Pessoa_CEP;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV25Contratada_UF)) )
            {
               AV25Contratada_UF = A350Contratada_UF;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
            }
            GXACONTRATADA_MUNICIPIOCOD_html0C13( AV25Contratada_UF) ;
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV33Pessoa_Telefone)) )
            {
               AV33Pessoa_Telefone = A522Pessoa_Telefone;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV31Pessoa_Fax)) )
            {
               AV31Pessoa_Fax = A523Pessoa_Fax;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_BancoNome)) )
            {
               AV19Contratada_BancoNome = StringUtil.Trim( A342Contratada_BancoNome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV20Contratada_BancoNro)) )
            {
               AV20Contratada_BancoNro = StringUtil.Trim( A343Contratada_BancoNro);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_AgenciaNome)) )
            {
               AV17Contratada_AgenciaNome = StringUtil.Trim( A344Contratada_AgenciaNome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AgenciaNro)) )
            {
               AV18Contratada_AgenciaNro = StringUtil.Trim( A345Contratada_AgenciaNro);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_ContaCorrente)) )
            {
               AV22Contratada_ContaCorrente = StringUtil.Trim( A51Contratada_ContaCorrente);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
            }
            /* Using cursor T000C21 */
            pr_default.execute(19, new Object[] {A52Contratada_AreaTrabalhoCod});
            A53Contratada_AreaTrabalhoDes = T000C21_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = T000C21_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = T000C21_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = T000C21_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = T000C21_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = T000C21_n1595Contratada_AreaTrbSrvPdr[0];
            pr_default.close(19);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000C22 */
            pr_default.execute(20, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tributo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T000C23 */
            pr_default.execute(21, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T000C24 */
            pr_default.execute(22, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resutlado_Contratada Origem"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T000C25 */
            pr_default.execute(23, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Contratada"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T000C26 */
            pr_default.execute(24, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T000C27 */
            pr_default.execute(25, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor T000C28 */
            pr_default.execute(26, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
         }
      }

      protected void EndLevel0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(19);
            pr_default.close(18);
            context.CommitDataStores( "Contratada");
            if ( AnyError == 0 )
            {
               ConfirmValues0C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(19);
            pr_default.close(18);
            context.RollbackDataStores( "Contratada");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0C13( )
      {
         /* Scan By routine */
         /* Using cursor T000C29 */
         pr_default.execute(27);
         RcdFound13 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound13 = 1;
            A39Contratada_Codigo = T000C29_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0C13( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound13 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound13 = 1;
            A39Contratada_Codigo = T000C29_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0C13( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm0C13( )
      {
         /* After Confirm Rules */
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) && String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI)) ) )
         {
            GXt_char2 = AV37IsConfiguracoesValidas;
            new prc_validatamanhoimagem(context ).execute(  A1664Contratada_Logo, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            AV37IsConfiguracoesValidas = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37IsConfiguracoesValidas", AV37IsConfiguracoesValidas);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37IsConfiguracoesValidas)) && ! ( String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) && String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI)) ) )
         {
            GX_msglist.addItem(AV37IsConfiguracoesValidas, 1, "CONTRATADA_LOGO");
            AnyError = 1;
            GX_FocusControl = imgContratada_Logo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            return  ;
         }
      }

      protected void BeforeInsert0C13( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C13( )
      {
         /* Before Update Rules */
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete0C13( )
      {
         /* Before Delete Rules */
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete0C13( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate0C13( )
      {
         /* Before Validate Rules */
         A342Contratada_BancoNome = AV19Contratada_BancoNome;
         n342Contratada_BancoNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A342Contratada_BancoNome", A342Contratada_BancoNome);
         A343Contratada_BancoNro = AV20Contratada_BancoNro;
         n343Contratada_BancoNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A343Contratada_BancoNro", A343Contratada_BancoNro);
         A344Contratada_AgenciaNome = AV17Contratada_AgenciaNome;
         n344Contratada_AgenciaNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A344Contratada_AgenciaNome", A344Contratada_AgenciaNome);
         A345Contratada_AgenciaNro = AV18Contratada_AgenciaNro;
         n345Contratada_AgenciaNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A345Contratada_AgenciaNro", A345Contratada_AgenciaNro);
         A51Contratada_ContaCorrente = AV22Contratada_ContaCorrente;
         n51Contratada_ContaCorrente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51Contratada_ContaCorrente", A51Contratada_ContaCorrente);
         GXt_int1 = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         if ( ( StringUtil.StrCmp(AV21Contratada_CNPJ, "Interno") != 0 ) && ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && new prc_existecontratada(context).executeUdp( ref  AV21Contratada_CNPJ, ref  GXt_int1) ) || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(AV21Contratada_CNPJ, A42Contratada_PessoaCNPJ) != 0 ) ) ) )
         {
            GX_msglist.addItem("A PJ j� encontra-se cadastrada como Contratada nesta �rea de Trabalho!", 1, "vCONTRATADA_CNPJ");
            AnyError = 1;
            GX_FocusControl = edtavContratada_cnpj_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A349Contratada_MunicipioCod) )
         {
            A349Contratada_MunicipioCod = 0;
            n349Contratada_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            n349Contratada_MunicipioCod = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            GXt_int1 = A40Contratada_PessoaCod;
            new prc_novapessoacontratada(context ).execute(  AV23Contratada_PessoaCod,  AV21Contratada_CNPJ,  AV24Contratada_RazaoSocial,  AV32Pessoa_IE,  AV30Pessoa_Endereco,  AV29Pessoa_CEP,  AV33Pessoa_Telefone,  AV31Pessoa_Fax,  A349Contratada_MunicipioCod,  AV8WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contratada_PessoaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            A40Contratada_PessoaCod = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         }
      }

      protected void DisableAttributes0C13( )
      {
         cmbContratada_TipoFabrica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_TipoFabrica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_TipoFabrica.Enabled), 5, 0)));
         edtavContratada_razaosocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_razaosocial_Enabled), 5, 0)));
         imgContratada_Logo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgContratada_Logo_Enabled), 5, 0)));
         edtavContratada_cnpj_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_cnpj_Enabled), 5, 0)));
         edtavPessoa_ie_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_ie_Enabled), 5, 0)));
         edtContratada_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Sigla_Enabled), 5, 0)));
         edtavContratada_banconome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_banconome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_banconome_Enabled), 5, 0)));
         edtavContratada_banconro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_banconro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_banconro_Enabled), 5, 0)));
         edtavContratada_agencianome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_agencianome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_agencianome_Enabled), 5, 0)));
         edtavContratada_agencianro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_agencianro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_agencianro_Enabled), 5, 0)));
         edtavContratada_contacorrente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_contacorrente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_contacorrente_Enabled), 5, 0)));
         edtavPessoa_endereco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_endereco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_endereco_Enabled), 5, 0)));
         edtavPessoa_cep_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_cep_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_cep_Enabled), 5, 0)));
         dynavContratada_uf.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_uf.Enabled), 5, 0)));
         dynContratada_MunicipioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_MunicipioCod.Enabled), 5, 0)));
         edtavPessoa_telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_telefone_Enabled), 5, 0)));
         edtavPessoa_fax_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPessoa_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPessoa_fax_Enabled), 5, 0)));
         edtContratada_SS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_SS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_SS_Enabled), 5, 0)));
         edtContratada_OS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_OS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_OS_Enabled), 5, 0)));
         edtContratada_Lote_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Lote_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Lote_Enabled), 5, 0)));
         cmbContratada_UsaOSistema.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_UsaOSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_UsaOSistema.Enabled), 5, 0)));
         cmbContratada_OSPreferencial.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_OSPreferencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_OSPreferencial.Enabled), 5, 0)));
         edtContratada_CntPadrao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_CntPadrao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_CntPadrao_Enabled), 5, 0)));
         chkContratada_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratada_Ativo.Enabled), 5, 0)));
         edtavContratada_pessoacod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacod_Enabled), 5, 0)));
         edtContratada_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
         edtContratada_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0C0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120183445");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z438Contratada_Sigla", StringUtil.RTrim( Z438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, "Z516Contratada_TipoFabrica", StringUtil.RTrim( Z516Contratada_TipoFabrica));
         GxWebStd.gx_hidden_field( context, "Z342Contratada_BancoNome", StringUtil.RTrim( Z342Contratada_BancoNome));
         GxWebStd.gx_hidden_field( context, "Z343Contratada_BancoNro", StringUtil.RTrim( Z343Contratada_BancoNro));
         GxWebStd.gx_hidden_field( context, "Z344Contratada_AgenciaNome", StringUtil.RTrim( Z344Contratada_AgenciaNome));
         GxWebStd.gx_hidden_field( context, "Z345Contratada_AgenciaNro", StringUtil.RTrim( Z345Contratada_AgenciaNro));
         GxWebStd.gx_hidden_field( context, "Z51Contratada_ContaCorrente", StringUtil.RTrim( Z51Contratada_ContaCorrente));
         GxWebStd.gx_hidden_field( context, "Z524Contratada_OS", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z524Contratada_OS), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1451Contratada_SS", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1451Contratada_SS), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z530Contratada_Lote", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z530Contratada_Lote), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1481Contratada_UsaOSistema", Z1481Contratada_UsaOSistema);
         GxWebStd.gx_boolean_hidden_field( context, "Z1867Contratada_OSPreferencial", Z1867Contratada_OSPreferencial);
         GxWebStd.gx_hidden_field( context, "Z1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1953Contratada_CntPadrao), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z43Contratada_Ativo", Z43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "Z40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z349Contratada_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A349Contratada_MunicipioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATADA_MUNICIPIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26Insert_Contratada_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_LOGO_GXI", A40000Contratada_Logo_GXI);
         GxWebStd.gx_hidden_field( context, "vISCONFIGURACOESVALIDAS", AV37IsConfiguracoesValidas);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACNPJ", A42Contratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOANOM", StringUtil.RTrim( A41Contratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_BANCONOME", StringUtil.RTrim( A342Contratada_BancoNome));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_BANCONRO", StringUtil.RTrim( A343Contratada_BancoNro));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AGENCIANOME", StringUtil.RTrim( A344Contratada_AgenciaNome));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AGENCIANRO", StringUtil.RTrim( A345Contratada_AgenciaNro));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CONTACORRENTE", StringUtil.RTrim( A51Contratada_ContaCorrente));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_UF", StringUtil.RTrim( A350Contratada_UF));
         GxWebStd.gx_hidden_field( context, "PESSOA_IE", StringUtil.RTrim( A518Pessoa_IE));
         GxWebStd.gx_hidden_field( context, "PESSOA_ENDERECO", A519Pessoa_Endereco);
         GxWebStd.gx_hidden_field( context, "PESSOA_CEP", StringUtil.RTrim( A521Pessoa_CEP));
         GxWebStd.gx_hidden_field( context, "PESSOA_TELEFONE", StringUtil.RTrim( A522Pessoa_Telefone));
         GxWebStd.gx_hidden_field( context, "PESSOA_FAX", StringUtil.RTrim( A523Pessoa_Fax));
         GxWebStd.gx_hidden_field( context, "vOK", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28ok), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV35AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV35AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_LOGOARQUIVO", A1127Contratada_LogoArquivo);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_LOGOTIPOARQ", StringUtil.RTrim( A1129Contratada_LogoTipoArq));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_LOGONOMEARQ", StringUtil.RTrim( A1128Contratada_LogoNomeArq));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHODES", A53Contratada_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRBCLCPFNL", StringUtil.RTrim( A1592Contratada_AreaTrbClcPFnl));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRBSRVPDR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV39Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratada_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "CONTRATADA_LOGO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A1664Contratada_Logo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Contratada";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV39Pgmname, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratada:[SendSecurityCheck value for]"+"Contratada_Codigo:"+context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratada:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratada:[SendSecurityCheck value for]"+"Contratada_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("contratada:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV39Pgmname, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contratada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Contratada" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada" ;
      }

      protected void InitializeNonKey0C13( )
      {
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A349Contratada_MunicipioCod = 0;
         n349Contratada_MunicipioCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
         n349Contratada_MunicipioCod = ((0==A349Contratada_MunicipioCod) ? true : false);
         AV24Contratada_RazaoSocial = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contratada_RazaoSocial", AV24Contratada_RazaoSocial);
         AV21Contratada_CNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratada_CNPJ", AV21Contratada_CNPJ);
         AV32Pessoa_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Pessoa_IE", AV32Pessoa_IE);
         AV19Contratada_BancoNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratada_BancoNome", AV19Contratada_BancoNome);
         AV20Contratada_BancoNro = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratada_BancoNro", AV20Contratada_BancoNro);
         AV17Contratada_AgenciaNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_AgenciaNome", AV17Contratada_AgenciaNome);
         AV18Contratada_AgenciaNro = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_AgenciaNro", AV18Contratada_AgenciaNro);
         AV22Contratada_ContaCorrente = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_ContaCorrente", AV22Contratada_ContaCorrente);
         AV30Pessoa_Endereco = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Pessoa_Endereco", AV30Pessoa_Endereco);
         AV29Pessoa_CEP = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Pessoa_CEP", AV29Pessoa_CEP);
         AV25Contratada_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contratada_UF", AV25Contratada_UF);
         AV33Pessoa_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Pessoa_Telefone", AV33Pessoa_Telefone);
         AV31Pessoa_Fax = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pessoa_Fax", AV31Pessoa_Fax);
         AV35AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV37IsConfiguracoesValidas = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37IsConfiguracoesValidas", AV37IsConfiguracoesValidas);
         A53Contratada_AreaTrabalhoDes = "";
         n53Contratada_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
         A1592Contratada_AreaTrbClcPFnl = "";
         n1592Contratada_AreaTrbClcPFnl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1592Contratada_AreaTrbClcPFnl", A1592Contratada_AreaTrbClcPFnl);
         A1595Contratada_AreaTrbSrvPdr = 0;
         n1595Contratada_AreaTrbSrvPdr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1595Contratada_AreaTrbSrvPdr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         A518Pessoa_IE = "";
         n518Pessoa_IE = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A518Pessoa_IE", A518Pessoa_IE);
         A519Pessoa_Endereco = "";
         n519Pessoa_Endereco = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
         A521Pessoa_CEP = "";
         n521Pessoa_CEP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
         A522Pessoa_Telefone = "";
         n522Pessoa_Telefone = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
         A523Pessoa_Fax = "";
         n523Pessoa_Fax = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A523Pessoa_Fax", A523Pessoa_Fax);
         A438Contratada_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
         A342Contratada_BancoNome = "";
         n342Contratada_BancoNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A342Contratada_BancoNome", A342Contratada_BancoNome);
         A343Contratada_BancoNro = "";
         n343Contratada_BancoNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A343Contratada_BancoNro", A343Contratada_BancoNro);
         A344Contratada_AgenciaNome = "";
         n344Contratada_AgenciaNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A344Contratada_AgenciaNome", A344Contratada_AgenciaNome);
         A345Contratada_AgenciaNro = "";
         n345Contratada_AgenciaNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A345Contratada_AgenciaNro", A345Contratada_AgenciaNro);
         A51Contratada_ContaCorrente = "";
         n51Contratada_ContaCorrente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51Contratada_ContaCorrente", A51Contratada_ContaCorrente);
         A350Contratada_UF = "";
         n350Contratada_UF = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A350Contratada_UF", A350Contratada_UF);
         A524Contratada_OS = 0;
         n524Contratada_OS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A524Contratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0)));
         n524Contratada_OS = ((0==A524Contratada_OS) ? true : false);
         A1451Contratada_SS = 0;
         n1451Contratada_SS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1451Contratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0)));
         n1451Contratada_SS = ((0==A1451Contratada_SS) ? true : false);
         A530Contratada_Lote = 0;
         n530Contratada_Lote = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A530Contratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0)));
         n530Contratada_Lote = ((0==A530Contratada_Lote) ? true : false);
         A1127Contratada_LogoArquivo = "";
         n1127Contratada_LogoArquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1127Contratada_LogoArquivo", A1127Contratada_LogoArquivo);
         A1481Contratada_UsaOSistema = false;
         n1481Contratada_UsaOSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1481Contratada_UsaOSistema", A1481Contratada_UsaOSistema);
         n1481Contratada_UsaOSistema = ((false==A1481Contratada_UsaOSistema) ? true : false);
         A1867Contratada_OSPreferencial = false;
         n1867Contratada_OSPreferencial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1867Contratada_OSPreferencial", A1867Contratada_OSPreferencial);
         n1867Contratada_OSPreferencial = ((false==A1867Contratada_OSPreferencial) ? true : false);
         A1953Contratada_CntPadrao = 0;
         n1953Contratada_CntPadrao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1953Contratada_CntPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0)));
         n1953Contratada_CntPadrao = ((0==A1953Contratada_CntPadrao) ? true : false);
         A1664Contratada_Logo = "";
         n1664Contratada_Logo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
         n1664Contratada_Logo = (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? true : false);
         A1129Contratada_LogoTipoArq = "";
         n1129Contratada_LogoTipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1129Contratada_LogoTipoArq", A1129Contratada_LogoTipoArq);
         A1128Contratada_LogoNomeArq = "";
         n1128Contratada_LogoNomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1128Contratada_LogoNomeArq", A1128Contratada_LogoNomeArq);
         A40000Contratada_Logo_GXI = "";
         n40000Contratada_Logo_GXI = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
         AV28ok = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ok", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ok), 6, 0)));
         A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A516Contratada_TipoFabrica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         A43Contratada_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
         Z438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         Z342Contratada_BancoNome = "";
         Z343Contratada_BancoNro = "";
         Z344Contratada_AgenciaNome = "";
         Z345Contratada_AgenciaNro = "";
         Z51Contratada_ContaCorrente = "";
         Z524Contratada_OS = 0;
         Z1451Contratada_SS = 0;
         Z530Contratada_Lote = 0;
         Z1481Contratada_UsaOSistema = false;
         Z1867Contratada_OSPreferencial = false;
         Z1953Contratada_CntPadrao = 0;
         Z43Contratada_Ativo = false;
         Z40Contratada_PessoaCod = 0;
         Z52Contratada_AreaTrabalhoCod = 0;
         Z349Contratada_MunicipioCod = 0;
      }

      protected void InitAll0C13( )
      {
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         InitializeNonKey0C13( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A52Contratada_AreaTrabalhoCod = i52Contratada_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A43Contratada_Ativo = i43Contratada_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
         A516Contratada_TipoFabrica = i516Contratada_TipoFabrica;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120183498");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratada.js", "?20203120183499");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_tipofabrica_Internalname = "TEXTBLOCKCONTRATADA_TIPOFABRICA";
         cmbContratada_TipoFabrica_Internalname = "CONTRATADA_TIPOFABRICA";
         lblTextblockcontratada_razaosocial_Internalname = "TEXTBLOCKCONTRATADA_RAZAOSOCIAL";
         edtavContratada_razaosocial_Internalname = "vCONTRATADA_RAZAOSOCIAL";
         lblTextblockcontratada_logo_Internalname = "TEXTBLOCKCONTRATADA_LOGO";
         imgContratada_Logo_Internalname = "CONTRATADA_LOGO";
         lblTextblockcontratada_cnpj_Internalname = "TEXTBLOCKCONTRATADA_CNPJ";
         edtavContratada_cnpj_Internalname = "vCONTRATADA_CNPJ";
         lblTextblockpessoa_ie_Internalname = "TEXTBLOCKPESSOA_IE";
         edtavPessoa_ie_Internalname = "vPESSOA_IE";
         lblTextblockcontratada_sigla_Internalname = "TEXTBLOCKCONTRATADA_SIGLA";
         edtContratada_Sigla_Internalname = "CONTRATADA_SIGLA";
         lblTextblockcontratada_banconome_Internalname = "TEXTBLOCKCONTRATADA_BANCONOME";
         edtavContratada_banconome_Internalname = "vCONTRATADA_BANCONOME";
         lblTextblockcontratada_banconro_Internalname = "TEXTBLOCKCONTRATADA_BANCONRO";
         edtavContratada_banconro_Internalname = "vCONTRATADA_BANCONRO";
         lblTextblockcontratada_agencianome_Internalname = "TEXTBLOCKCONTRATADA_AGENCIANOME";
         edtavContratada_agencianome_Internalname = "vCONTRATADA_AGENCIANOME";
         lblTextblockcontratada_agencianro_Internalname = "TEXTBLOCKCONTRATADA_AGENCIANRO";
         edtavContratada_agencianro_Internalname = "vCONTRATADA_AGENCIANRO";
         lblTextblockcontratada_contacorrente_Internalname = "TEXTBLOCKCONTRATADA_CONTACORRENTE";
         edtavContratada_contacorrente_Internalname = "vCONTRATADA_CONTACORRENTE";
         lblTextblockpessoa_endereco_Internalname = "TEXTBLOCKPESSOA_ENDERECO";
         edtavPessoa_endereco_Internalname = "vPESSOA_ENDERECO";
         lblTextblockpessoa_cep_Internalname = "TEXTBLOCKPESSOA_CEP";
         edtavPessoa_cep_Internalname = "vPESSOA_CEP";
         lblTextblockcontratada_uf_Internalname = "TEXTBLOCKCONTRATADA_UF";
         dynavContratada_uf_Internalname = "vCONTRATADA_UF";
         lblTextblockcontratada_municipiocod_Internalname = "TEXTBLOCKCONTRATADA_MUNICIPIOCOD";
         dynContratada_MunicipioCod_Internalname = "CONTRATADA_MUNICIPIOCOD";
         lblTextblockpessoa_telefone_Internalname = "TEXTBLOCKPESSOA_TELEFONE";
         edtavPessoa_telefone_Internalname = "vPESSOA_TELEFONE";
         lblTextblockpessoa_fax_Internalname = "TEXTBLOCKPESSOA_FAX";
         edtavPessoa_fax_Internalname = "vPESSOA_FAX";
         lblTextblockcontratada_ss_Internalname = "TEXTBLOCKCONTRATADA_SS";
         edtContratada_SS_Internalname = "CONTRATADA_SS";
         lblTextblockcontratada_os_Internalname = "TEXTBLOCKCONTRATADA_OS";
         edtContratada_OS_Internalname = "CONTRATADA_OS";
         lblTextblockcontratada_lote_Internalname = "TEXTBLOCKCONTRATADA_LOTE";
         edtContratada_Lote_Internalname = "CONTRATADA_LOTE";
         lblTextblockcontratada_usaosistema_Internalname = "TEXTBLOCKCONTRATADA_USAOSISTEMA";
         cellTextblockcontratada_usaosistema_cell_Internalname = "TEXTBLOCKCONTRATADA_USAOSISTEMA_CELL";
         cmbContratada_UsaOSistema_Internalname = "CONTRATADA_USAOSISTEMA";
         cellContratada_usaosistema_cell_Internalname = "CONTRATADA_USAOSISTEMA_CELL";
         lblTextblockcontratada_ospreferencial_Internalname = "TEXTBLOCKCONTRATADA_OSPREFERENCIAL";
         cmbContratada_OSPreferencial_Internalname = "CONTRATADA_OSPREFERENCIAL";
         lblContratada_ospreferencial_righttext_Internalname = "CONTRATADA_OSPREFERENCIAL_RIGHTTEXT";
         tblTablemergedcontratada_ospreferencial_Internalname = "TABLEMERGEDCONTRATADA_OSPREFERENCIAL";
         lblTextblockcontratada_cntpadrao_Internalname = "TEXTBLOCKCONTRATADA_CNTPADRAO";
         edtContratada_CntPadrao_Internalname = "CONTRATADA_CNTPADRAO";
         lblTextblockcontratada_ativo_Internalname = "TEXTBLOCKCONTRATADA_ATIVO";
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         edtavContratada_pessoacod_Internalname = "vCONTRATADA_PESSOACOD";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Contratada / Departamento";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contratada";
         lblContratada_ospreferencial_righttext_Visible = 1;
         cmbContratada_OSPreferencial_Jsonclick = "";
         cmbContratada_OSPreferencial.Enabled = 1;
         chkContratada_Ativo.Enabled = 1;
         edtContratada_CntPadrao_Jsonclick = "";
         edtContratada_CntPadrao_Enabled = 1;
         cmbContratada_UsaOSistema_Jsonclick = "";
         cmbContratada_UsaOSistema.Enabled = 1;
         cmbContratada_UsaOSistema.Visible = 1;
         cellContratada_usaosistema_cell_Class = "";
         cellTextblockcontratada_usaosistema_cell_Class = "";
         edtContratada_Lote_Jsonclick = "";
         edtContratada_Lote_Enabled = 1;
         edtContratada_OS_Jsonclick = "";
         edtContratada_OS_Enabled = 1;
         edtContratada_SS_Jsonclick = "";
         edtContratada_SS_Enabled = 1;
         edtavPessoa_fax_Jsonclick = "";
         edtavPessoa_fax_Enabled = 1;
         edtavPessoa_telefone_Jsonclick = "";
         edtavPessoa_telefone_Enabled = 1;
         dynContratada_MunicipioCod_Jsonclick = "";
         dynContratada_MunicipioCod.Enabled = 1;
         dynavContratada_uf_Jsonclick = "";
         dynavContratada_uf.Enabled = 1;
         edtavPessoa_cep_Jsonclick = "";
         edtavPessoa_cep_Enabled = 1;
         edtavPessoa_endereco_Jsonclick = "";
         edtavPessoa_endereco_Enabled = 1;
         edtavContratada_contacorrente_Jsonclick = "";
         edtavContratada_contacorrente_Enabled = 1;
         edtavContratada_agencianro_Jsonclick = "";
         edtavContratada_agencianro_Enabled = 1;
         edtavContratada_agencianome_Jsonclick = "";
         edtavContratada_agencianome_Enabled = 1;
         edtavContratada_banconro_Jsonclick = "";
         edtavContratada_banconro_Enabled = 1;
         edtavContratada_banconome_Jsonclick = "";
         edtavContratada_banconome_Enabled = 1;
         edtContratada_Sigla_Jsonclick = "";
         edtContratada_Sigla_Enabled = 1;
         edtavPessoa_ie_Jsonclick = "";
         edtavPessoa_ie_Enabled = 1;
         edtavPessoa_ie_Visible = 1;
         lblTextblockpessoa_ie_Visible = 1;
         edtavContratada_cnpj_Jsonclick = "";
         edtavContratada_cnpj_Enabled = 1;
         edtavContratada_cnpj_Visible = 1;
         lblTextblockcontratada_cnpj_Visible = 1;
         imgContratada_Logo_Enabled = 1;
         edtavContratada_razaosocial_Jsonclick = "";
         edtavContratada_razaosocial_Enabled = 1;
         cmbContratada_TipoFabrica_Jsonclick = "";
         cmbContratada_TipoFabrica.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbjava_Caption = "Java";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_PessoaCod_Enabled = 1;
         edtContratada_PessoaCod_Visible = 1;
         edtContratada_Codigo_Jsonclick = "";
         edtContratada_Codigo_Enabled = 0;
         edtContratada_Codigo_Visible = 1;
         edtavContratada_pessoacod_Jsonclick = "";
         edtavContratada_pessoacod_Enabled = 0;
         edtavContratada_pessoacod_Visible = 1;
         edtavContrato_codigo_Jsonclick = "";
         edtavContrato_codigo_Enabled = 0;
         edtavContrato_codigo_Visible = 1;
         chkContratada_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXACONTRATADA_MUNICIPIOCOD_html0C13( AV25Contratada_UF) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_UF0C13( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_UF_data0C13( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_UF_html0C13( )
      {
         String gxdynajaxvalue ;
         GXDLVvCONTRATADA_UF_data0C13( ) ;
         gxdynajaxindex = 1;
         dynavContratada_uf.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavContratada_uf.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvCONTRATADA_UF_data0C13( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000C30 */
         pr_default.execute(28);
         while ( (pr_default.getStatus(28) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T000C30_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000C30_A24Estado_Nome[0]));
            pr_default.readNext(28);
         }
         pr_default.close(28);
      }

      protected void GXDLACONTRATADA_MUNICIPIOCOD0C13( String AV25Contratada_UF )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATADA_MUNICIPIOCOD_data0C13( AV25Contratada_UF) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATADA_MUNICIPIOCOD_html0C13( String AV25Contratada_UF )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATADA_MUNICIPIOCOD_data0C13( AV25Contratada_UF) ;
         gxdynajaxindex = 1;
         dynContratada_MunicipioCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratada_MunicipioCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATADA_MUNICIPIOCOD_data0C13( String AV25Contratada_UF )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000C31 */
         pr_default.execute(29, new Object[] {AV25Contratada_UF});
         while ( (pr_default.getStatus(29) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000C31_A25Municipio_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000C31_A26Municipio_Nome[0]));
            pr_default.readNext(29);
         }
         pr_default.close(29);
      }

      protected void GX7ASACONTRATADA_PESSOACOD0C13( int AV11Insert_Contratada_PessoaCod )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratada_PessoaCod) )
         {
            A40Contratada_PessoaCod = AV11Insert_Contratada_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX23ASAISCONFIGURACOESVALIDAS0C13( String A40000Contratada_Logo_GXI ,
                                                        String A1664Contratada_Logo )
      {
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) && String.IsNullOrEmpty(StringUtil.RTrim( A40000Contratada_Logo_GXI)) ) )
         {
            GXt_char2 = AV37IsConfiguracoesValidas;
            new prc_validatamanhoimagem(context ).execute(  A1664Contratada_Logo, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1664Contratada_Logo", A1664Contratada_Logo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgContratada_Logo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A1664Contratada_Logo)) ? A40000Contratada_Logo_GXI : context.convertURL( context.PathToRelativeUrl( A1664Contratada_Logo))));
            AV37IsConfiguracoesValidas = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37IsConfiguracoesValidas", AV37IsConfiguracoesValidas);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( AV37IsConfiguracoesValidas)+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_52_0C13( wwpbaseobjects.SdtAuditingObject AV35AuditingObject ,
                                 int A39Contratada_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV35AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_53_0C13( wwpbaseobjects.SdtAuditingObject AV35AuditingObject ,
                                 int A39Contratada_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV35AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_54_0C13( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV35AuditingObject ,
                                 int A39Contratada_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV35AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_55_0C13( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV35AuditingObject ,
                                 int A39Contratada_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV35AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Validv_Contratada_uf( GXCombobox dynGX_Parm1 ,
                                        GXCombobox dynGX_Parm2 )
      {
         dynavContratada_uf = dynGX_Parm1;
         AV25Contratada_UF = dynavContratada_uf.CurrentValue;
         dynContratada_MunicipioCod = dynGX_Parm2;
         A349Contratada_MunicipioCod = (int)(NumberUtil.Val( dynContratada_MunicipioCod.CurrentValue, "."));
         n349Contratada_MunicipioCod = false;
         GXACONTRATADA_MUNICIPIOCOD_html0C13( AV25Contratada_UF) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynContratada_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0));
         if ( dynContratada_MunicipioCod.ItemCount > 0 )
         {
            A349Contratada_MunicipioCod = (int)(NumberUtil.Val( dynContratada_MunicipioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0))), "."));
            n349Contratada_MunicipioCod = false;
         }
         dynContratada_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0));
         isValidOutput.Add(dynContratada_MunicipioCod);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratada_municipiocod( String GX_Parm1 ,
                                                 GXCombobox dynGX_Parm2 ,
                                                 String GX_Parm3 ,
                                                 GXCombobox dynGX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         dynContratada_MunicipioCod = dynGX_Parm2;
         A349Contratada_MunicipioCod = (int)(NumberUtil.Val( dynContratada_MunicipioCod.CurrentValue, "."));
         n349Contratada_MunicipioCod = false;
         A350Contratada_UF = GX_Parm3;
         n350Contratada_UF = false;
         dynavContratada_uf = dynGX_Parm4;
         AV25Contratada_UF = dynavContratada_uf.CurrentValue;
         /* Using cursor T000C32 */
         pr_default.execute(30, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            if ( ! ( (0==A349Contratada_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratada_Municipio'.", "ForeignKeyNotFound", 1, "CONTRATADA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynContratada_MunicipioCod_Internalname;
            }
         }
         A350Contratada_UF = T000C32_A350Contratada_UF[0];
         n350Contratada_UF = T000C32_n350Contratada_UF[0];
         pr_default.close(30);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV25Contratada_UF)) )
         {
            AV25Contratada_UF = A350Contratada_UF;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A350Contratada_UF = "";
            n350Contratada_UF = false;
         }
         GXVvCONTRATADA_UF_html0C13( ) ;
         dynavContratada_uf.CurrentValue = AV25Contratada_UF;
         isValidOutput.Add(StringUtil.RTrim( A350Contratada_UF));
         if ( dynavContratada_uf.ItemCount > 0 )
         {
            AV25Contratada_UF = dynavContratada_uf.getValidValue(AV25Contratada_UF);
         }
         dynavContratada_uf.CurrentValue = AV25Contratada_UF;
         isValidOutput.Add(dynavContratada_uf);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratada_pessoacod( String GX_Parm1 ,
                                              int GX_Parm2 ,
                                              String GX_Parm3 ,
                                              String GX_Parm4 ,
                                              String GX_Parm5 ,
                                              String GX_Parm6 ,
                                              GXCombobox cmbGX_Parm7 ,
                                              String GX_Parm8 ,
                                              String GX_Parm9 ,
                                              String GX_Parm10 ,
                                              String GX_Parm11 ,
                                              String GX_Parm12 ,
                                              String GX_Parm13 ,
                                              String GX_Parm14 ,
                                              String GX_Parm15 ,
                                              String GX_Parm16 ,
                                              String GX_Parm17 ,
                                              int GX_Parm18 )
      {
         Gx_mode = GX_Parm1;
         A40Contratada_PessoaCod = GX_Parm2;
         A41Contratada_PessoaNom = GX_Parm3;
         n41Contratada_PessoaNom = false;
         AV24Contratada_RazaoSocial = GX_Parm4;
         A42Contratada_PessoaCNPJ = GX_Parm5;
         n42Contratada_PessoaCNPJ = false;
         AV21Contratada_CNPJ = GX_Parm6;
         cmbContratada_TipoFabrica = cmbGX_Parm7;
         A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.CurrentValue;
         A518Pessoa_IE = GX_Parm8;
         n518Pessoa_IE = false;
         AV32Pessoa_IE = GX_Parm9;
         A519Pessoa_Endereco = GX_Parm10;
         n519Pessoa_Endereco = false;
         AV30Pessoa_Endereco = GX_Parm11;
         A521Pessoa_CEP = GX_Parm12;
         n521Pessoa_CEP = false;
         AV29Pessoa_CEP = GX_Parm13;
         A522Pessoa_Telefone = GX_Parm14;
         n522Pessoa_Telefone = false;
         AV33Pessoa_Telefone = GX_Parm15;
         A523Pessoa_Fax = GX_Parm16;
         n523Pessoa_Fax = false;
         AV31Pessoa_Fax = GX_Parm17;
         AV23Contratada_PessoaCod = GX_Parm18;
         /* Using cursor T000C33 */
         pr_default.execute(31, new Object[] {A40Contratada_PessoaCod});
         A41Contratada_PessoaNom = T000C33_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T000C33_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000C33_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T000C33_n42Contratada_PessoaCNPJ[0];
         A518Pessoa_IE = T000C33_A518Pessoa_IE[0];
         n518Pessoa_IE = T000C33_n518Pessoa_IE[0];
         A519Pessoa_Endereco = T000C33_A519Pessoa_Endereco[0];
         n519Pessoa_Endereco = T000C33_n519Pessoa_Endereco[0];
         A521Pessoa_CEP = T000C33_A521Pessoa_CEP[0];
         n521Pessoa_CEP = T000C33_n521Pessoa_CEP[0];
         A522Pessoa_Telefone = T000C33_A522Pessoa_Telefone[0];
         n522Pessoa_Telefone = T000C33_n522Pessoa_Telefone[0];
         A523Pessoa_Fax = T000C33_A523Pessoa_Fax[0];
         n523Pessoa_Fax = T000C33_n523Pessoa_Fax[0];
         pr_default.close(31);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            AV24Contratada_RazaoSocial = StringUtil.Trim( A41Contratada_PessoaNom);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_RazaoSocial)) )
         {
            GX_msglist.addItem("Raz�o Social � obrigat�rio.", 1, "CONTRATADA_PESSOACOD");
            AnyError = 1;
            GX_FocusControl = edtContratada_PessoaCod_Internalname;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
         {
            AV21Contratada_CNPJ = A42Contratada_PessoaCNPJ;
         }
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) && String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratada_CNPJ)) )
         {
            GX_msglist.addItem("CNPJ � obrigat�rio.", 1, "CONTRATADA_PESSOACOD");
            AnyError = 1;
            GX_FocusControl = edtContratada_PessoaCod_Internalname;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV32Pessoa_IE)) )
         {
            AV32Pessoa_IE = A518Pessoa_IE;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV30Pessoa_Endereco)) )
         {
            AV30Pessoa_Endereco = A519Pessoa_Endereco;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV29Pessoa_CEP)) )
         {
            AV29Pessoa_CEP = A521Pessoa_CEP;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV33Pessoa_Telefone)) )
         {
            AV33Pessoa_Telefone = A522Pessoa_Telefone;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( AV31Pessoa_Fax)) )
         {
            AV31Pessoa_Fax = A523Pessoa_Fax;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==AV23Contratada_PessoaCod) )
         {
            AV23Contratada_PessoaCod = A40Contratada_PessoaCod;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A41Contratada_PessoaNom = "";
            n41Contratada_PessoaNom = false;
            A42Contratada_PessoaCNPJ = "";
            n42Contratada_PessoaCNPJ = false;
            A518Pessoa_IE = "";
            n518Pessoa_IE = false;
            A519Pessoa_Endereco = "";
            n519Pessoa_Endereco = false;
            A521Pessoa_CEP = "";
            n521Pessoa_CEP = false;
            A522Pessoa_Telefone = "";
            n522Pessoa_Telefone = false;
            A523Pessoa_Fax = "";
            n523Pessoa_Fax = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A41Contratada_PessoaNom));
         isValidOutput.Add(A42Contratada_PessoaCNPJ);
         isValidOutput.Add(StringUtil.RTrim( A518Pessoa_IE));
         isValidOutput.Add(A519Pessoa_Endereco);
         isValidOutput.Add(StringUtil.RTrim( A521Pessoa_CEP));
         isValidOutput.Add(StringUtil.RTrim( A522Pessoa_Telefone));
         isValidOutput.Add(StringUtil.RTrim( A523Pessoa_Fax));
         isValidOutput.Add(StringUtil.RTrim( AV24Contratada_RazaoSocial));
         isValidOutput.Add(AV21Contratada_CNPJ);
         isValidOutput.Add(StringUtil.RTrim( AV32Pessoa_IE));
         isValidOutput.Add(AV30Pessoa_Endereco);
         isValidOutput.Add(StringUtil.RTrim( AV29Pessoa_CEP));
         isValidOutput.Add(StringUtil.RTrim( AV33Pessoa_Telefone));
         isValidOutput.Add(StringUtil.RTrim( AV31Pessoa_Fax));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Contratada_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E130C2',iparms:[{av:'AV35AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A516Contratada_TipoFabrica',fld:'CONTRATADA_TIPOFABRICA',pic:'',nv:''},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Contratada_RazaoSocial',fld:'vCONTRATADA_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("VCONTRATADA_CNPJ.ISVALID","{handler:'E140C2',iparms:[{av:'AV21Contratada_CNPJ',fld:'vCONTRATADA_CNPJ',pic:'',nv:''},{av:'AV24Contratada_RazaoSocial',fld:'vCONTRATADA_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV19Contratada_BancoNome',fld:'vCONTRATADA_BANCONOME',pic:'@!',nv:''},{av:'AV20Contratada_BancoNro',fld:'vCONTRATADA_BANCONRO',pic:'',nv:''},{av:'AV17Contratada_AgenciaNome',fld:'vCONTRATADA_AGENCIANOME',pic:'@!',nv:''},{av:'AV18Contratada_AgenciaNro',fld:'vCONTRATADA_AGENCIANRO',pic:'',nv:''},{av:'AV22Contratada_ContaCorrente',fld:'vCONTRATADA_CONTACORRENTE',pic:'',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV32Pessoa_IE',fld:'vPESSOA_IE',pic:'',nv:''}],oparms:[{av:'AV23Contratada_PessoaCod',fld:'vCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONTRATADA_TIPOFABRICA.CLICK","{handler:'E110C13',iparms:[{av:'A516Contratada_TipoFabrica',fld:'CONTRATADA_TIPOFABRICA',pic:'',nv:''}],oparms:[{av:'AV21Contratada_CNPJ',fld:'vCONTRATADA_CNPJ',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(31);
         pr_default.close(17);
         pr_default.close(19);
         pr_default.close(30);
         pr_default.close(18);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         Z342Contratada_BancoNome = "";
         Z343Contratada_BancoNro = "";
         Z344Contratada_AgenciaNome = "";
         Z345Contratada_AgenciaNro = "";
         Z51Contratada_ContaCorrente = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV25Contratada_UF = "";
         A40000Contratada_Logo_GXI = "";
         A1664Contratada_Logo = "";
         GXKey = "";
         A516Contratada_TipoFabrica = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratada_tipofabrica_Jsonclick = "";
         lblTextblockcontratada_razaosocial_Jsonclick = "";
         AV24Contratada_RazaoSocial = "";
         lblTextblockcontratada_logo_Jsonclick = "";
         lblTextblockcontratada_cnpj_Jsonclick = "";
         AV21Contratada_CNPJ = "";
         lblTextblockpessoa_ie_Jsonclick = "";
         AV32Pessoa_IE = "";
         lblTextblockcontratada_sigla_Jsonclick = "";
         A438Contratada_Sigla = "";
         lblTextblockcontratada_banconome_Jsonclick = "";
         AV19Contratada_BancoNome = "";
         lblTextblockcontratada_banconro_Jsonclick = "";
         AV20Contratada_BancoNro = "";
         lblTextblockcontratada_agencianome_Jsonclick = "";
         AV17Contratada_AgenciaNome = "";
         lblTextblockcontratada_agencianro_Jsonclick = "";
         AV18Contratada_AgenciaNro = "";
         lblTextblockcontratada_contacorrente_Jsonclick = "";
         AV22Contratada_ContaCorrente = "";
         lblTextblockpessoa_endereco_Jsonclick = "";
         AV30Pessoa_Endereco = "";
         lblTextblockpessoa_cep_Jsonclick = "";
         AV29Pessoa_CEP = "";
         lblTextblockcontratada_uf_Jsonclick = "";
         lblTextblockcontratada_municipiocod_Jsonclick = "";
         lblTextblockpessoa_telefone_Jsonclick = "";
         AV33Pessoa_Telefone = "";
         lblTextblockpessoa_fax_Jsonclick = "";
         AV31Pessoa_Fax = "";
         lblTextblockcontratada_ss_Jsonclick = "";
         lblTextblockcontratada_os_Jsonclick = "";
         lblTextblockcontratada_lote_Jsonclick = "";
         lblTextblockcontratada_usaosistema_Jsonclick = "";
         lblTextblockcontratada_ospreferencial_Jsonclick = "";
         lblTextblockcontratada_cntpadrao_Jsonclick = "";
         lblTextblockcontratada_ativo_Jsonclick = "";
         lblContratada_ospreferencial_righttext_Jsonclick = "";
         A342Contratada_BancoNome = "";
         A343Contratada_BancoNro = "";
         A344Contratada_AgenciaNome = "";
         A345Contratada_AgenciaNro = "";
         A51Contratada_ContaCorrente = "";
         AV37IsConfiguracoesValidas = "";
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A350Contratada_UF = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         AV35AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A1127Contratada_LogoArquivo = "";
         A1129Contratada_LogoTipoArq = "";
         A1128Contratada_LogoNomeArq = "";
         A53Contratada_AreaTrabalhoDes = "";
         A1592Contratada_AreaTrbClcPFnl = "";
         AV39Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode13 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1127Contratada_LogoArquivo = "";
         Z1664Contratada_Logo = "";
         Z1129Contratada_LogoTipoArq = "";
         Z1128Contratada_LogoNomeArq = "";
         Z40000Contratada_Logo_GXI = "";
         Z53Contratada_AreaTrabalhoDes = "";
         Z1592Contratada_AreaTrbClcPFnl = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         Z518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         Z350Contratada_UF = "";
         T000C6_A350Contratada_UF = new String[] {""} ;
         T000C6_n350Contratada_UF = new bool[] {false} ;
         T000C4_A41Contratada_PessoaNom = new String[] {""} ;
         T000C4_n41Contratada_PessoaNom = new bool[] {false} ;
         T000C4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000C4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000C4_A518Pessoa_IE = new String[] {""} ;
         T000C4_n518Pessoa_IE = new bool[] {false} ;
         T000C4_A519Pessoa_Endereco = new String[] {""} ;
         T000C4_n519Pessoa_Endereco = new bool[] {false} ;
         T000C4_A521Pessoa_CEP = new String[] {""} ;
         T000C4_n521Pessoa_CEP = new bool[] {false} ;
         T000C4_A522Pessoa_Telefone = new String[] {""} ;
         T000C4_n522Pessoa_Telefone = new bool[] {false} ;
         T000C4_A523Pessoa_Fax = new String[] {""} ;
         T000C4_n523Pessoa_Fax = new bool[] {false} ;
         T000C5_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         T000C5_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         T000C5_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         T000C5_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         T000C5_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         T000C5_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         T000C7_A39Contratada_Codigo = new int[1] ;
         T000C7_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         T000C7_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         T000C7_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         T000C7_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         T000C7_A41Contratada_PessoaNom = new String[] {""} ;
         T000C7_n41Contratada_PessoaNom = new bool[] {false} ;
         T000C7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000C7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000C7_A518Pessoa_IE = new String[] {""} ;
         T000C7_n518Pessoa_IE = new bool[] {false} ;
         T000C7_A519Pessoa_Endereco = new String[] {""} ;
         T000C7_n519Pessoa_Endereco = new bool[] {false} ;
         T000C7_A521Pessoa_CEP = new String[] {""} ;
         T000C7_n521Pessoa_CEP = new bool[] {false} ;
         T000C7_A522Pessoa_Telefone = new String[] {""} ;
         T000C7_n522Pessoa_Telefone = new bool[] {false} ;
         T000C7_A523Pessoa_Fax = new String[] {""} ;
         T000C7_n523Pessoa_Fax = new bool[] {false} ;
         T000C7_A438Contratada_Sigla = new String[] {""} ;
         T000C7_A516Contratada_TipoFabrica = new String[] {""} ;
         T000C7_A342Contratada_BancoNome = new String[] {""} ;
         T000C7_n342Contratada_BancoNome = new bool[] {false} ;
         T000C7_A343Contratada_BancoNro = new String[] {""} ;
         T000C7_n343Contratada_BancoNro = new bool[] {false} ;
         T000C7_A344Contratada_AgenciaNome = new String[] {""} ;
         T000C7_n344Contratada_AgenciaNome = new bool[] {false} ;
         T000C7_A345Contratada_AgenciaNro = new String[] {""} ;
         T000C7_n345Contratada_AgenciaNro = new bool[] {false} ;
         T000C7_A51Contratada_ContaCorrente = new String[] {""} ;
         T000C7_n51Contratada_ContaCorrente = new bool[] {false} ;
         T000C7_A524Contratada_OS = new int[1] ;
         T000C7_n524Contratada_OS = new bool[] {false} ;
         T000C7_A1451Contratada_SS = new int[1] ;
         T000C7_n1451Contratada_SS = new bool[] {false} ;
         T000C7_A530Contratada_Lote = new short[1] ;
         T000C7_n530Contratada_Lote = new bool[] {false} ;
         T000C7_A1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C7_n1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C7_A1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C7_n1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C7_A1953Contratada_CntPadrao = new int[1] ;
         T000C7_n1953Contratada_CntPadrao = new bool[] {false} ;
         T000C7_A43Contratada_Ativo = new bool[] {false} ;
         T000C7_A1129Contratada_LogoTipoArq = new String[] {""} ;
         T000C7_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         T000C7_A1128Contratada_LogoNomeArq = new String[] {""} ;
         T000C7_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         T000C7_A40000Contratada_Logo_GXI = new String[] {""} ;
         T000C7_n40000Contratada_Logo_GXI = new bool[] {false} ;
         T000C7_A40Contratada_PessoaCod = new int[1] ;
         T000C7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T000C7_A349Contratada_MunicipioCod = new int[1] ;
         T000C7_n349Contratada_MunicipioCod = new bool[] {false} ;
         T000C7_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         T000C7_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         T000C7_A350Contratada_UF = new String[] {""} ;
         T000C7_n350Contratada_UF = new bool[] {false} ;
         T000C7_A1127Contratada_LogoArquivo = new String[] {""} ;
         T000C7_n1127Contratada_LogoArquivo = new bool[] {false} ;
         T000C7_A1664Contratada_Logo = new String[] {""} ;
         T000C7_n1664Contratada_Logo = new bool[] {false} ;
         A1127Contratada_LogoArquivo_Filetype = "";
         A1127Contratada_LogoArquivo_Filename = "";
         T000C8_A41Contratada_PessoaNom = new String[] {""} ;
         T000C8_n41Contratada_PessoaNom = new bool[] {false} ;
         T000C8_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000C8_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000C8_A518Pessoa_IE = new String[] {""} ;
         T000C8_n518Pessoa_IE = new bool[] {false} ;
         T000C8_A519Pessoa_Endereco = new String[] {""} ;
         T000C8_n519Pessoa_Endereco = new bool[] {false} ;
         T000C8_A521Pessoa_CEP = new String[] {""} ;
         T000C8_n521Pessoa_CEP = new bool[] {false} ;
         T000C8_A522Pessoa_Telefone = new String[] {""} ;
         T000C8_n522Pessoa_Telefone = new bool[] {false} ;
         T000C8_A523Pessoa_Fax = new String[] {""} ;
         T000C8_n523Pessoa_Fax = new bool[] {false} ;
         T000C9_A350Contratada_UF = new String[] {""} ;
         T000C9_n350Contratada_UF = new bool[] {false} ;
         T000C10_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         T000C10_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         T000C10_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         T000C10_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         T000C10_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         T000C10_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         T000C11_A39Contratada_Codigo = new int[1] ;
         T000C3_A39Contratada_Codigo = new int[1] ;
         T000C3_A438Contratada_Sigla = new String[] {""} ;
         T000C3_A516Contratada_TipoFabrica = new String[] {""} ;
         T000C3_A342Contratada_BancoNome = new String[] {""} ;
         T000C3_n342Contratada_BancoNome = new bool[] {false} ;
         T000C3_A343Contratada_BancoNro = new String[] {""} ;
         T000C3_n343Contratada_BancoNro = new bool[] {false} ;
         T000C3_A344Contratada_AgenciaNome = new String[] {""} ;
         T000C3_n344Contratada_AgenciaNome = new bool[] {false} ;
         T000C3_A345Contratada_AgenciaNro = new String[] {""} ;
         T000C3_n345Contratada_AgenciaNro = new bool[] {false} ;
         T000C3_A51Contratada_ContaCorrente = new String[] {""} ;
         T000C3_n51Contratada_ContaCorrente = new bool[] {false} ;
         T000C3_A524Contratada_OS = new int[1] ;
         T000C3_n524Contratada_OS = new bool[] {false} ;
         T000C3_A1451Contratada_SS = new int[1] ;
         T000C3_n1451Contratada_SS = new bool[] {false} ;
         T000C3_A530Contratada_Lote = new short[1] ;
         T000C3_n530Contratada_Lote = new bool[] {false} ;
         T000C3_A1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C3_n1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C3_A1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C3_n1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C3_A1953Contratada_CntPadrao = new int[1] ;
         T000C3_n1953Contratada_CntPadrao = new bool[] {false} ;
         T000C3_A43Contratada_Ativo = new bool[] {false} ;
         T000C3_A1129Contratada_LogoTipoArq = new String[] {""} ;
         T000C3_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         T000C3_A1128Contratada_LogoNomeArq = new String[] {""} ;
         T000C3_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         T000C3_A40000Contratada_Logo_GXI = new String[] {""} ;
         T000C3_n40000Contratada_Logo_GXI = new bool[] {false} ;
         T000C3_A40Contratada_PessoaCod = new int[1] ;
         T000C3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T000C3_A349Contratada_MunicipioCod = new int[1] ;
         T000C3_n349Contratada_MunicipioCod = new bool[] {false} ;
         T000C3_A1127Contratada_LogoArquivo = new String[] {""} ;
         T000C3_n1127Contratada_LogoArquivo = new bool[] {false} ;
         T000C3_A1664Contratada_Logo = new String[] {""} ;
         T000C3_n1664Contratada_Logo = new bool[] {false} ;
         T000C12_A39Contratada_Codigo = new int[1] ;
         T000C13_A39Contratada_Codigo = new int[1] ;
         T000C2_A39Contratada_Codigo = new int[1] ;
         T000C2_A438Contratada_Sigla = new String[] {""} ;
         T000C2_A516Contratada_TipoFabrica = new String[] {""} ;
         T000C2_A342Contratada_BancoNome = new String[] {""} ;
         T000C2_n342Contratada_BancoNome = new bool[] {false} ;
         T000C2_A343Contratada_BancoNro = new String[] {""} ;
         T000C2_n343Contratada_BancoNro = new bool[] {false} ;
         T000C2_A344Contratada_AgenciaNome = new String[] {""} ;
         T000C2_n344Contratada_AgenciaNome = new bool[] {false} ;
         T000C2_A345Contratada_AgenciaNro = new String[] {""} ;
         T000C2_n345Contratada_AgenciaNro = new bool[] {false} ;
         T000C2_A51Contratada_ContaCorrente = new String[] {""} ;
         T000C2_n51Contratada_ContaCorrente = new bool[] {false} ;
         T000C2_A524Contratada_OS = new int[1] ;
         T000C2_n524Contratada_OS = new bool[] {false} ;
         T000C2_A1451Contratada_SS = new int[1] ;
         T000C2_n1451Contratada_SS = new bool[] {false} ;
         T000C2_A530Contratada_Lote = new short[1] ;
         T000C2_n530Contratada_Lote = new bool[] {false} ;
         T000C2_A1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C2_n1481Contratada_UsaOSistema = new bool[] {false} ;
         T000C2_A1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C2_n1867Contratada_OSPreferencial = new bool[] {false} ;
         T000C2_A1953Contratada_CntPadrao = new int[1] ;
         T000C2_n1953Contratada_CntPadrao = new bool[] {false} ;
         T000C2_A43Contratada_Ativo = new bool[] {false} ;
         T000C2_A1129Contratada_LogoTipoArq = new String[] {""} ;
         T000C2_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         T000C2_A1128Contratada_LogoNomeArq = new String[] {""} ;
         T000C2_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         T000C2_A40000Contratada_Logo_GXI = new String[] {""} ;
         T000C2_n40000Contratada_Logo_GXI = new bool[] {false} ;
         T000C2_A40Contratada_PessoaCod = new int[1] ;
         T000C2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T000C2_A349Contratada_MunicipioCod = new int[1] ;
         T000C2_n349Contratada_MunicipioCod = new bool[] {false} ;
         T000C2_A1127Contratada_LogoArquivo = new String[] {""} ;
         T000C2_n1127Contratada_LogoArquivo = new bool[] {false} ;
         T000C2_A1664Contratada_Logo = new String[] {""} ;
         T000C2_n1664Contratada_Logo = new bool[] {false} ;
         T000C14_A39Contratada_Codigo = new int[1] ;
         T000C19_A41Contratada_PessoaNom = new String[] {""} ;
         T000C19_n41Contratada_PessoaNom = new bool[] {false} ;
         T000C19_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000C19_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000C19_A518Pessoa_IE = new String[] {""} ;
         T000C19_n518Pessoa_IE = new bool[] {false} ;
         T000C19_A519Pessoa_Endereco = new String[] {""} ;
         T000C19_n519Pessoa_Endereco = new bool[] {false} ;
         T000C19_A521Pessoa_CEP = new String[] {""} ;
         T000C19_n521Pessoa_CEP = new bool[] {false} ;
         T000C19_A522Pessoa_Telefone = new String[] {""} ;
         T000C19_n522Pessoa_Telefone = new bool[] {false} ;
         T000C19_A523Pessoa_Fax = new String[] {""} ;
         T000C19_n523Pessoa_Fax = new bool[] {false} ;
         T000C20_A350Contratada_UF = new String[] {""} ;
         T000C20_n350Contratada_UF = new bool[] {false} ;
         T000C21_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         T000C21_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         T000C21_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         T000C21_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         T000C21_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         T000C21_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         T000C22_A1653Tributo_Codigo = new int[1] ;
         T000C23_A1482Gestao_Codigo = new int[1] ;
         T000C24_A456ContagemResultado_Codigo = new int[1] ;
         T000C25_A456ContagemResultado_Codigo = new int[1] ;
         T000C26_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000C26_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000C27_A439Solicitacoes_Codigo = new int[1] ;
         T000C28_A74Contrato_Codigo = new int[1] ;
         T000C29_A39Contratada_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         i516Contratada_TipoFabrica = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000C30_A23Estado_UF = new String[] {""} ;
         T000C30_A24Estado_Nome = new String[] {""} ;
         T000C31_A25Municipio_Codigo = new int[1] ;
         T000C31_A26Municipio_Nome = new String[] {""} ;
         T000C31_A23Estado_UF = new String[] {""} ;
         GXt_char2 = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000C32_A350Contratada_UF = new String[] {""} ;
         T000C32_n350Contratada_UF = new bool[] {false} ;
         T000C33_A41Contratada_PessoaNom = new String[] {""} ;
         T000C33_n41Contratada_PessoaNom = new bool[] {false} ;
         T000C33_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000C33_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000C33_A518Pessoa_IE = new String[] {""} ;
         T000C33_n518Pessoa_IE = new bool[] {false} ;
         T000C33_A519Pessoa_Endereco = new String[] {""} ;
         T000C33_n519Pessoa_Endereco = new bool[] {false} ;
         T000C33_A521Pessoa_CEP = new String[] {""} ;
         T000C33_n521Pessoa_CEP = new bool[] {false} ;
         T000C33_A522Pessoa_Telefone = new String[] {""} ;
         T000C33_n522Pessoa_Telefone = new bool[] {false} ;
         T000C33_A523Pessoa_Fax = new String[] {""} ;
         T000C33_n523Pessoa_Fax = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratada__default(),
            new Object[][] {
                new Object[] {
               T000C2_A39Contratada_Codigo, T000C2_A438Contratada_Sigla, T000C2_A516Contratada_TipoFabrica, T000C2_A342Contratada_BancoNome, T000C2_n342Contratada_BancoNome, T000C2_A343Contratada_BancoNro, T000C2_n343Contratada_BancoNro, T000C2_A344Contratada_AgenciaNome, T000C2_n344Contratada_AgenciaNome, T000C2_A345Contratada_AgenciaNro,
               T000C2_n345Contratada_AgenciaNro, T000C2_A51Contratada_ContaCorrente, T000C2_n51Contratada_ContaCorrente, T000C2_A524Contratada_OS, T000C2_n524Contratada_OS, T000C2_A1451Contratada_SS, T000C2_n1451Contratada_SS, T000C2_A530Contratada_Lote, T000C2_n530Contratada_Lote, T000C2_A1481Contratada_UsaOSistema,
               T000C2_n1481Contratada_UsaOSistema, T000C2_A1867Contratada_OSPreferencial, T000C2_n1867Contratada_OSPreferencial, T000C2_A1953Contratada_CntPadrao, T000C2_n1953Contratada_CntPadrao, T000C2_A43Contratada_Ativo, T000C2_A1129Contratada_LogoTipoArq, T000C2_n1129Contratada_LogoTipoArq, T000C2_A1128Contratada_LogoNomeArq, T000C2_n1128Contratada_LogoNomeArq,
               T000C2_A40000Contratada_Logo_GXI, T000C2_n40000Contratada_Logo_GXI, T000C2_A40Contratada_PessoaCod, T000C2_A52Contratada_AreaTrabalhoCod, T000C2_A349Contratada_MunicipioCod, T000C2_n349Contratada_MunicipioCod, T000C2_A1127Contratada_LogoArquivo, T000C2_n1127Contratada_LogoArquivo, T000C2_A1664Contratada_Logo, T000C2_n1664Contratada_Logo
               }
               , new Object[] {
               T000C3_A39Contratada_Codigo, T000C3_A438Contratada_Sigla, T000C3_A516Contratada_TipoFabrica, T000C3_A342Contratada_BancoNome, T000C3_n342Contratada_BancoNome, T000C3_A343Contratada_BancoNro, T000C3_n343Contratada_BancoNro, T000C3_A344Contratada_AgenciaNome, T000C3_n344Contratada_AgenciaNome, T000C3_A345Contratada_AgenciaNro,
               T000C3_n345Contratada_AgenciaNro, T000C3_A51Contratada_ContaCorrente, T000C3_n51Contratada_ContaCorrente, T000C3_A524Contratada_OS, T000C3_n524Contratada_OS, T000C3_A1451Contratada_SS, T000C3_n1451Contratada_SS, T000C3_A530Contratada_Lote, T000C3_n530Contratada_Lote, T000C3_A1481Contratada_UsaOSistema,
               T000C3_n1481Contratada_UsaOSistema, T000C3_A1867Contratada_OSPreferencial, T000C3_n1867Contratada_OSPreferencial, T000C3_A1953Contratada_CntPadrao, T000C3_n1953Contratada_CntPadrao, T000C3_A43Contratada_Ativo, T000C3_A1129Contratada_LogoTipoArq, T000C3_n1129Contratada_LogoTipoArq, T000C3_A1128Contratada_LogoNomeArq, T000C3_n1128Contratada_LogoNomeArq,
               T000C3_A40000Contratada_Logo_GXI, T000C3_n40000Contratada_Logo_GXI, T000C3_A40Contratada_PessoaCod, T000C3_A52Contratada_AreaTrabalhoCod, T000C3_A349Contratada_MunicipioCod, T000C3_n349Contratada_MunicipioCod, T000C3_A1127Contratada_LogoArquivo, T000C3_n1127Contratada_LogoArquivo, T000C3_A1664Contratada_Logo, T000C3_n1664Contratada_Logo
               }
               , new Object[] {
               T000C4_A41Contratada_PessoaNom, T000C4_n41Contratada_PessoaNom, T000C4_A42Contratada_PessoaCNPJ, T000C4_n42Contratada_PessoaCNPJ, T000C4_A518Pessoa_IE, T000C4_n518Pessoa_IE, T000C4_A519Pessoa_Endereco, T000C4_n519Pessoa_Endereco, T000C4_A521Pessoa_CEP, T000C4_n521Pessoa_CEP,
               T000C4_A522Pessoa_Telefone, T000C4_n522Pessoa_Telefone, T000C4_A523Pessoa_Fax, T000C4_n523Pessoa_Fax
               }
               , new Object[] {
               T000C5_A53Contratada_AreaTrabalhoDes, T000C5_n53Contratada_AreaTrabalhoDes, T000C5_A1592Contratada_AreaTrbClcPFnl, T000C5_n1592Contratada_AreaTrbClcPFnl, T000C5_A1595Contratada_AreaTrbSrvPdr, T000C5_n1595Contratada_AreaTrbSrvPdr
               }
               , new Object[] {
               T000C6_A350Contratada_UF, T000C6_n350Contratada_UF
               }
               , new Object[] {
               T000C7_A39Contratada_Codigo, T000C7_A53Contratada_AreaTrabalhoDes, T000C7_n53Contratada_AreaTrabalhoDes, T000C7_A1592Contratada_AreaTrbClcPFnl, T000C7_n1592Contratada_AreaTrbClcPFnl, T000C7_A41Contratada_PessoaNom, T000C7_n41Contratada_PessoaNom, T000C7_A42Contratada_PessoaCNPJ, T000C7_n42Contratada_PessoaCNPJ, T000C7_A518Pessoa_IE,
               T000C7_n518Pessoa_IE, T000C7_A519Pessoa_Endereco, T000C7_n519Pessoa_Endereco, T000C7_A521Pessoa_CEP, T000C7_n521Pessoa_CEP, T000C7_A522Pessoa_Telefone, T000C7_n522Pessoa_Telefone, T000C7_A523Pessoa_Fax, T000C7_n523Pessoa_Fax, T000C7_A438Contratada_Sigla,
               T000C7_A516Contratada_TipoFabrica, T000C7_A342Contratada_BancoNome, T000C7_n342Contratada_BancoNome, T000C7_A343Contratada_BancoNro, T000C7_n343Contratada_BancoNro, T000C7_A344Contratada_AgenciaNome, T000C7_n344Contratada_AgenciaNome, T000C7_A345Contratada_AgenciaNro, T000C7_n345Contratada_AgenciaNro, T000C7_A51Contratada_ContaCorrente,
               T000C7_n51Contratada_ContaCorrente, T000C7_A524Contratada_OS, T000C7_n524Contratada_OS, T000C7_A1451Contratada_SS, T000C7_n1451Contratada_SS, T000C7_A530Contratada_Lote, T000C7_n530Contratada_Lote, T000C7_A1481Contratada_UsaOSistema, T000C7_n1481Contratada_UsaOSistema, T000C7_A1867Contratada_OSPreferencial,
               T000C7_n1867Contratada_OSPreferencial, T000C7_A1953Contratada_CntPadrao, T000C7_n1953Contratada_CntPadrao, T000C7_A43Contratada_Ativo, T000C7_A1129Contratada_LogoTipoArq, T000C7_n1129Contratada_LogoTipoArq, T000C7_A1128Contratada_LogoNomeArq, T000C7_n1128Contratada_LogoNomeArq, T000C7_A40000Contratada_Logo_GXI, T000C7_n40000Contratada_Logo_GXI,
               T000C7_A40Contratada_PessoaCod, T000C7_A52Contratada_AreaTrabalhoCod, T000C7_A349Contratada_MunicipioCod, T000C7_n349Contratada_MunicipioCod, T000C7_A1595Contratada_AreaTrbSrvPdr, T000C7_n1595Contratada_AreaTrbSrvPdr, T000C7_A350Contratada_UF, T000C7_n350Contratada_UF, T000C7_A1127Contratada_LogoArquivo, T000C7_n1127Contratada_LogoArquivo,
               T000C7_A1664Contratada_Logo, T000C7_n1664Contratada_Logo
               }
               , new Object[] {
               T000C8_A41Contratada_PessoaNom, T000C8_n41Contratada_PessoaNom, T000C8_A42Contratada_PessoaCNPJ, T000C8_n42Contratada_PessoaCNPJ, T000C8_A518Pessoa_IE, T000C8_n518Pessoa_IE, T000C8_A519Pessoa_Endereco, T000C8_n519Pessoa_Endereco, T000C8_A521Pessoa_CEP, T000C8_n521Pessoa_CEP,
               T000C8_A522Pessoa_Telefone, T000C8_n522Pessoa_Telefone, T000C8_A523Pessoa_Fax, T000C8_n523Pessoa_Fax
               }
               , new Object[] {
               T000C9_A350Contratada_UF, T000C9_n350Contratada_UF
               }
               , new Object[] {
               T000C10_A53Contratada_AreaTrabalhoDes, T000C10_n53Contratada_AreaTrabalhoDes, T000C10_A1592Contratada_AreaTrbClcPFnl, T000C10_n1592Contratada_AreaTrbClcPFnl, T000C10_A1595Contratada_AreaTrbSrvPdr, T000C10_n1595Contratada_AreaTrbSrvPdr
               }
               , new Object[] {
               T000C11_A39Contratada_Codigo
               }
               , new Object[] {
               T000C12_A39Contratada_Codigo
               }
               , new Object[] {
               T000C13_A39Contratada_Codigo
               }
               , new Object[] {
               T000C14_A39Contratada_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000C19_A41Contratada_PessoaNom, T000C19_n41Contratada_PessoaNom, T000C19_A42Contratada_PessoaCNPJ, T000C19_n42Contratada_PessoaCNPJ, T000C19_A518Pessoa_IE, T000C19_n518Pessoa_IE, T000C19_A519Pessoa_Endereco, T000C19_n519Pessoa_Endereco, T000C19_A521Pessoa_CEP, T000C19_n521Pessoa_CEP,
               T000C19_A522Pessoa_Telefone, T000C19_n522Pessoa_Telefone, T000C19_A523Pessoa_Fax, T000C19_n523Pessoa_Fax
               }
               , new Object[] {
               T000C20_A350Contratada_UF, T000C20_n350Contratada_UF
               }
               , new Object[] {
               T000C21_A53Contratada_AreaTrabalhoDes, T000C21_n53Contratada_AreaTrabalhoDes, T000C21_A1592Contratada_AreaTrbClcPFnl, T000C21_n1592Contratada_AreaTrbClcPFnl, T000C21_A1595Contratada_AreaTrbSrvPdr, T000C21_n1595Contratada_AreaTrbSrvPdr
               }
               , new Object[] {
               T000C22_A1653Tributo_Codigo
               }
               , new Object[] {
               T000C23_A1482Gestao_Codigo
               }
               , new Object[] {
               T000C24_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000C25_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000C26_A66ContratadaUsuario_ContratadaCod, T000C26_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               T000C27_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T000C28_A74Contrato_Codigo
               }
               , new Object[] {
               T000C29_A39Contratada_Codigo
               }
               , new Object[] {
               T000C30_A23Estado_UF, T000C30_A24Estado_Nome
               }
               , new Object[] {
               T000C31_A25Municipio_Codigo, T000C31_A26Municipio_Nome, T000C31_A23Estado_UF
               }
               , new Object[] {
               T000C32_A350Contratada_UF, T000C32_n350Contratada_UF
               }
               , new Object[] {
               T000C33_A41Contratada_PessoaNom, T000C33_n41Contratada_PessoaNom, T000C33_A42Contratada_PessoaCNPJ, T000C33_n42Contratada_PessoaCNPJ, T000C33_A518Pessoa_IE, T000C33_n518Pessoa_IE, T000C33_A519Pessoa_Endereco, T000C33_n519Pessoa_Endereco, T000C33_A521Pessoa_CEP, T000C33_n521Pessoa_CEP,
               T000C33_A522Pessoa_Telefone, T000C33_n522Pessoa_Telefone, T000C33_A523Pessoa_Fax, T000C33_n523Pessoa_Fax
               }
            }
         );
         Z43Contratada_Ativo = true;
         A43Contratada_Ativo = true;
         i43Contratada_Ativo = true;
         AV39Pgmname = "Contratada";
         Z516Contratada_TipoFabrica = "";
         A516Contratada_TipoFabrica = "";
         i516Contratada_TipoFabrica = "";
         Z52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z530Contratada_Lote ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A530Contratada_Lote ;
      private short Gx_BScreen ;
      private short RcdFound13 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Contratada_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z524Contratada_OS ;
      private int Z1451Contratada_SS ;
      private int Z1953Contratada_CntPadrao ;
      private int Z40Contratada_PessoaCod ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int Z349Contratada_MunicipioCod ;
      private int N52Contratada_AreaTrabalhoCod ;
      private int N40Contratada_PessoaCod ;
      private int N349Contratada_MunicipioCod ;
      private int AV11Insert_Contratada_PessoaCod ;
      private int A40Contratada_PessoaCod ;
      private int A349Contratada_MunicipioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV7Contratada_Codigo ;
      private int trnEnded ;
      private int AV36Contrato_Codigo ;
      private int edtavContrato_codigo_Enabled ;
      private int edtavContrato_codigo_Visible ;
      private int AV23Contratada_PessoaCod ;
      private int edtavContratada_pessoacod_Enabled ;
      private int edtavContratada_pessoacod_Visible ;
      private int A39Contratada_Codigo ;
      private int edtContratada_Codigo_Enabled ;
      private int edtContratada_Codigo_Visible ;
      private int edtContratada_PessoaCod_Visible ;
      private int edtContratada_PessoaCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtavContratada_razaosocial_Enabled ;
      private int imgContratada_Logo_Enabled ;
      private int lblTextblockcontratada_cnpj_Visible ;
      private int edtavContratada_cnpj_Visible ;
      private int edtavContratada_cnpj_Enabled ;
      private int lblTextblockpessoa_ie_Visible ;
      private int edtavPessoa_ie_Visible ;
      private int edtavPessoa_ie_Enabled ;
      private int edtContratada_Sigla_Enabled ;
      private int edtavContratada_banconome_Enabled ;
      private int edtavContratada_banconro_Enabled ;
      private int edtavContratada_agencianome_Enabled ;
      private int edtavContratada_agencianro_Enabled ;
      private int edtavContratada_contacorrente_Enabled ;
      private int edtavPessoa_endereco_Enabled ;
      private int edtavPessoa_cep_Enabled ;
      private int edtavPessoa_telefone_Enabled ;
      private int edtavPessoa_fax_Enabled ;
      private int A1451Contratada_SS ;
      private int edtContratada_SS_Enabled ;
      private int A524Contratada_OS ;
      private int edtContratada_OS_Enabled ;
      private int edtContratada_Lote_Enabled ;
      private int A1953Contratada_CntPadrao ;
      private int edtContratada_CntPadrao_Enabled ;
      private int lblContratada_ospreferencial_righttext_Visible ;
      private int AV14Insert_Contratada_AreaTrabalhoCod ;
      private int AV26Insert_Contratada_MunicipioCod ;
      private int AV28ok ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int AV40GXV1 ;
      private int Z1595Contratada_AreaTrbSrvPdr ;
      private int GXt_int1 ;
      private int i52Contratada_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z438Contratada_Sigla ;
      private String Z516Contratada_TipoFabrica ;
      private String Z342Contratada_BancoNome ;
      private String Z343Contratada_BancoNro ;
      private String Z344Contratada_AgenciaNome ;
      private String Z345Contratada_AgenciaNro ;
      private String Z51Contratada_ContaCorrente ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV25Contratada_UF ;
      private String imgContratada_Logo_Internalname ;
      private String Gx_mode ;
      private String GXKey ;
      private String A516Contratada_TipoFabrica ;
      private String chkContratada_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbContratada_TipoFabrica_Internalname ;
      private String edtavContrato_codigo_Internalname ;
      private String edtavContrato_codigo_Jsonclick ;
      private String edtavContratada_pessoacod_Internalname ;
      private String edtavContratada_pessoacod_Jsonclick ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtContratada_PessoaCod_Internalname ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_tipofabrica_Internalname ;
      private String lblTextblockcontratada_tipofabrica_Jsonclick ;
      private String cmbContratada_TipoFabrica_Jsonclick ;
      private String lblTextblockcontratada_razaosocial_Internalname ;
      private String lblTextblockcontratada_razaosocial_Jsonclick ;
      private String edtavContratada_razaosocial_Internalname ;
      private String AV24Contratada_RazaoSocial ;
      private String edtavContratada_razaosocial_Jsonclick ;
      private String lblTextblockcontratada_logo_Internalname ;
      private String lblTextblockcontratada_logo_Jsonclick ;
      private String lblTextblockcontratada_cnpj_Internalname ;
      private String lblTextblockcontratada_cnpj_Jsonclick ;
      private String edtavContratada_cnpj_Internalname ;
      private String edtavContratada_cnpj_Jsonclick ;
      private String lblTextblockpessoa_ie_Internalname ;
      private String lblTextblockpessoa_ie_Jsonclick ;
      private String edtavPessoa_ie_Internalname ;
      private String AV32Pessoa_IE ;
      private String edtavPessoa_ie_Jsonclick ;
      private String lblTextblockcontratada_sigla_Internalname ;
      private String lblTextblockcontratada_sigla_Jsonclick ;
      private String edtContratada_Sigla_Internalname ;
      private String A438Contratada_Sigla ;
      private String edtContratada_Sigla_Jsonclick ;
      private String lblTextblockcontratada_banconome_Internalname ;
      private String lblTextblockcontratada_banconome_Jsonclick ;
      private String edtavContratada_banconome_Internalname ;
      private String AV19Contratada_BancoNome ;
      private String edtavContratada_banconome_Jsonclick ;
      private String lblTextblockcontratada_banconro_Internalname ;
      private String lblTextblockcontratada_banconro_Jsonclick ;
      private String edtavContratada_banconro_Internalname ;
      private String AV20Contratada_BancoNro ;
      private String edtavContratada_banconro_Jsonclick ;
      private String lblTextblockcontratada_agencianome_Internalname ;
      private String lblTextblockcontratada_agencianome_Jsonclick ;
      private String edtavContratada_agencianome_Internalname ;
      private String AV17Contratada_AgenciaNome ;
      private String edtavContratada_agencianome_Jsonclick ;
      private String lblTextblockcontratada_agencianro_Internalname ;
      private String lblTextblockcontratada_agencianro_Jsonclick ;
      private String edtavContratada_agencianro_Internalname ;
      private String AV18Contratada_AgenciaNro ;
      private String edtavContratada_agencianro_Jsonclick ;
      private String lblTextblockcontratada_contacorrente_Internalname ;
      private String lblTextblockcontratada_contacorrente_Jsonclick ;
      private String edtavContratada_contacorrente_Internalname ;
      private String AV22Contratada_ContaCorrente ;
      private String edtavContratada_contacorrente_Jsonclick ;
      private String lblTextblockpessoa_endereco_Internalname ;
      private String lblTextblockpessoa_endereco_Jsonclick ;
      private String edtavPessoa_endereco_Internalname ;
      private String edtavPessoa_endereco_Jsonclick ;
      private String lblTextblockpessoa_cep_Internalname ;
      private String lblTextblockpessoa_cep_Jsonclick ;
      private String edtavPessoa_cep_Internalname ;
      private String AV29Pessoa_CEP ;
      private String edtavPessoa_cep_Jsonclick ;
      private String lblTextblockcontratada_uf_Internalname ;
      private String lblTextblockcontratada_uf_Jsonclick ;
      private String dynavContratada_uf_Internalname ;
      private String dynavContratada_uf_Jsonclick ;
      private String lblTextblockcontratada_municipiocod_Internalname ;
      private String lblTextblockcontratada_municipiocod_Jsonclick ;
      private String dynContratada_MunicipioCod_Internalname ;
      private String dynContratada_MunicipioCod_Jsonclick ;
      private String lblTextblockpessoa_telefone_Internalname ;
      private String lblTextblockpessoa_telefone_Jsonclick ;
      private String edtavPessoa_telefone_Internalname ;
      private String AV33Pessoa_Telefone ;
      private String edtavPessoa_telefone_Jsonclick ;
      private String lblTextblockpessoa_fax_Internalname ;
      private String lblTextblockpessoa_fax_Jsonclick ;
      private String edtavPessoa_fax_Internalname ;
      private String AV31Pessoa_Fax ;
      private String edtavPessoa_fax_Jsonclick ;
      private String lblTextblockcontratada_ss_Internalname ;
      private String lblTextblockcontratada_ss_Jsonclick ;
      private String edtContratada_SS_Internalname ;
      private String edtContratada_SS_Jsonclick ;
      private String lblTextblockcontratada_os_Internalname ;
      private String lblTextblockcontratada_os_Jsonclick ;
      private String edtContratada_OS_Internalname ;
      private String edtContratada_OS_Jsonclick ;
      private String lblTextblockcontratada_lote_Internalname ;
      private String lblTextblockcontratada_lote_Jsonclick ;
      private String edtContratada_Lote_Internalname ;
      private String edtContratada_Lote_Jsonclick ;
      private String cellTextblockcontratada_usaosistema_cell_Internalname ;
      private String cellTextblockcontratada_usaosistema_cell_Class ;
      private String lblTextblockcontratada_usaosistema_Internalname ;
      private String lblTextblockcontratada_usaosistema_Jsonclick ;
      private String cellContratada_usaosistema_cell_Internalname ;
      private String cellContratada_usaosistema_cell_Class ;
      private String cmbContratada_UsaOSistema_Internalname ;
      private String cmbContratada_UsaOSistema_Jsonclick ;
      private String lblTextblockcontratada_ospreferencial_Internalname ;
      private String lblTextblockcontratada_ospreferencial_Jsonclick ;
      private String lblTextblockcontratada_cntpadrao_Internalname ;
      private String lblTextblockcontratada_cntpadrao_Jsonclick ;
      private String edtContratada_CntPadrao_Internalname ;
      private String edtContratada_CntPadrao_Jsonclick ;
      private String lblTextblockcontratada_ativo_Internalname ;
      private String lblTextblockcontratada_ativo_Jsonclick ;
      private String tblTablemergedcontratada_ospreferencial_Internalname ;
      private String cmbContratada_OSPreferencial_Internalname ;
      private String cmbContratada_OSPreferencial_Jsonclick ;
      private String lblContratada_ospreferencial_righttext_Internalname ;
      private String lblContratada_ospreferencial_righttext_Jsonclick ;
      private String A342Contratada_BancoNome ;
      private String A343Contratada_BancoNro ;
      private String A344Contratada_AgenciaNome ;
      private String A345Contratada_AgenciaNro ;
      private String A51Contratada_ContaCorrente ;
      private String A41Contratada_PessoaNom ;
      private String A350Contratada_UF ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A1129Contratada_LogoTipoArq ;
      private String A1128Contratada_LogoNomeArq ;
      private String A1592Contratada_AreaTrbClcPFnl ;
      private String AV39Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode13 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Z1129Contratada_LogoTipoArq ;
      private String Z1128Contratada_LogoNomeArq ;
      private String Z1592Contratada_AreaTrbClcPFnl ;
      private String Z41Contratada_PessoaNom ;
      private String Z518Pessoa_IE ;
      private String Z521Pessoa_CEP ;
      private String Z522Pessoa_Telefone ;
      private String Z523Pessoa_Fax ;
      private String Z350Contratada_UF ;
      private String A1127Contratada_LogoArquivo_Filetype ;
      private String A1127Contratada_LogoArquivo_Filename ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String i516Contratada_TipoFabrica ;
      private String gxwrpcisep ;
      private String GXt_char2 ;
      private bool Z1481Contratada_UsaOSistema ;
      private bool Z1867Contratada_OSPreferencial ;
      private bool Z43Contratada_Ativo ;
      private bool entryPointCalled ;
      private bool n40000Contratada_Logo_GXI ;
      private bool n1664Contratada_Logo ;
      private bool n349Contratada_MunicipioCod ;
      private bool toggleJsOutput ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool A1867Contratada_OSPreferencial ;
      private bool n1867Contratada_OSPreferencial ;
      private bool wbErr ;
      private bool A1664Contratada_Logo_IsBlob ;
      private bool A43Contratada_Ativo ;
      private bool n1451Contratada_SS ;
      private bool n524Contratada_OS ;
      private bool n530Contratada_Lote ;
      private bool n1953Contratada_CntPadrao ;
      private bool n342Contratada_BancoNome ;
      private bool n343Contratada_BancoNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n345Contratada_AgenciaNro ;
      private bool n51Contratada_ContaCorrente ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n350Contratada_UF ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n1127Contratada_LogoArquivo ;
      private bool n1129Contratada_LogoTipoArq ;
      private bool n1128Contratada_LogoNomeArq ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n1592Contratada_AreaTrbClcPFnl ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i43Contratada_Ativo ;
      private String A40000Contratada_Logo_GXI ;
      private String AV21Contratada_CNPJ ;
      private String AV30Pessoa_Endereco ;
      private String AV37IsConfiguracoesValidas ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String Z40000Contratada_Logo_GXI ;
      private String Z53Contratada_AreaTrabalhoDes ;
      private String Z42Contratada_PessoaCNPJ ;
      private String Z519Pessoa_Endereco ;
      private String A1664Contratada_Logo ;
      private String Z1664Contratada_Logo ;
      private String A1127Contratada_LogoArquivo ;
      private String Z1127Contratada_LogoArquivo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratada_TipoFabrica ;
      private GXCombobox dynavContratada_uf ;
      private GXCombobox dynContratada_MunicipioCod ;
      private GXCombobox cmbContratada_UsaOSistema ;
      private GXCombobox cmbContratada_OSPreferencial ;
      private GXCheckbox chkContratada_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T000C6_A350Contratada_UF ;
      private bool[] T000C6_n350Contratada_UF ;
      private String[] T000C4_A41Contratada_PessoaNom ;
      private bool[] T000C4_n41Contratada_PessoaNom ;
      private String[] T000C4_A42Contratada_PessoaCNPJ ;
      private bool[] T000C4_n42Contratada_PessoaCNPJ ;
      private String[] T000C4_A518Pessoa_IE ;
      private bool[] T000C4_n518Pessoa_IE ;
      private String[] T000C4_A519Pessoa_Endereco ;
      private bool[] T000C4_n519Pessoa_Endereco ;
      private String[] T000C4_A521Pessoa_CEP ;
      private bool[] T000C4_n521Pessoa_CEP ;
      private String[] T000C4_A522Pessoa_Telefone ;
      private bool[] T000C4_n522Pessoa_Telefone ;
      private String[] T000C4_A523Pessoa_Fax ;
      private bool[] T000C4_n523Pessoa_Fax ;
      private String[] T000C5_A53Contratada_AreaTrabalhoDes ;
      private bool[] T000C5_n53Contratada_AreaTrabalhoDes ;
      private String[] T000C5_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] T000C5_n1592Contratada_AreaTrbClcPFnl ;
      private int[] T000C5_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] T000C5_n1595Contratada_AreaTrbSrvPdr ;
      private int[] T000C7_A39Contratada_Codigo ;
      private String[] T000C7_A53Contratada_AreaTrabalhoDes ;
      private bool[] T000C7_n53Contratada_AreaTrabalhoDes ;
      private String[] T000C7_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] T000C7_n1592Contratada_AreaTrbClcPFnl ;
      private String[] T000C7_A41Contratada_PessoaNom ;
      private bool[] T000C7_n41Contratada_PessoaNom ;
      private String[] T000C7_A42Contratada_PessoaCNPJ ;
      private bool[] T000C7_n42Contratada_PessoaCNPJ ;
      private String[] T000C7_A518Pessoa_IE ;
      private bool[] T000C7_n518Pessoa_IE ;
      private String[] T000C7_A519Pessoa_Endereco ;
      private bool[] T000C7_n519Pessoa_Endereco ;
      private String[] T000C7_A521Pessoa_CEP ;
      private bool[] T000C7_n521Pessoa_CEP ;
      private String[] T000C7_A522Pessoa_Telefone ;
      private bool[] T000C7_n522Pessoa_Telefone ;
      private String[] T000C7_A523Pessoa_Fax ;
      private bool[] T000C7_n523Pessoa_Fax ;
      private String[] T000C7_A438Contratada_Sigla ;
      private String[] T000C7_A516Contratada_TipoFabrica ;
      private String[] T000C7_A342Contratada_BancoNome ;
      private bool[] T000C7_n342Contratada_BancoNome ;
      private String[] T000C7_A343Contratada_BancoNro ;
      private bool[] T000C7_n343Contratada_BancoNro ;
      private String[] T000C7_A344Contratada_AgenciaNome ;
      private bool[] T000C7_n344Contratada_AgenciaNome ;
      private String[] T000C7_A345Contratada_AgenciaNro ;
      private bool[] T000C7_n345Contratada_AgenciaNro ;
      private String[] T000C7_A51Contratada_ContaCorrente ;
      private bool[] T000C7_n51Contratada_ContaCorrente ;
      private int[] T000C7_A524Contratada_OS ;
      private bool[] T000C7_n524Contratada_OS ;
      private int[] T000C7_A1451Contratada_SS ;
      private bool[] T000C7_n1451Contratada_SS ;
      private short[] T000C7_A530Contratada_Lote ;
      private bool[] T000C7_n530Contratada_Lote ;
      private bool[] T000C7_A1481Contratada_UsaOSistema ;
      private bool[] T000C7_n1481Contratada_UsaOSistema ;
      private bool[] T000C7_A1867Contratada_OSPreferencial ;
      private bool[] T000C7_n1867Contratada_OSPreferencial ;
      private int[] T000C7_A1953Contratada_CntPadrao ;
      private bool[] T000C7_n1953Contratada_CntPadrao ;
      private bool[] T000C7_A43Contratada_Ativo ;
      private String[] T000C7_A1129Contratada_LogoTipoArq ;
      private bool[] T000C7_n1129Contratada_LogoTipoArq ;
      private String[] T000C7_A1128Contratada_LogoNomeArq ;
      private bool[] T000C7_n1128Contratada_LogoNomeArq ;
      private String[] T000C7_A40000Contratada_Logo_GXI ;
      private bool[] T000C7_n40000Contratada_Logo_GXI ;
      private int[] T000C7_A40Contratada_PessoaCod ;
      private int[] T000C7_A52Contratada_AreaTrabalhoCod ;
      private int[] T000C7_A349Contratada_MunicipioCod ;
      private bool[] T000C7_n349Contratada_MunicipioCod ;
      private int[] T000C7_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] T000C7_n1595Contratada_AreaTrbSrvPdr ;
      private String[] T000C7_A350Contratada_UF ;
      private bool[] T000C7_n350Contratada_UF ;
      private String[] T000C7_A1127Contratada_LogoArquivo ;
      private bool[] T000C7_n1127Contratada_LogoArquivo ;
      private String[] T000C7_A1664Contratada_Logo ;
      private bool[] T000C7_n1664Contratada_Logo ;
      private String[] T000C8_A41Contratada_PessoaNom ;
      private bool[] T000C8_n41Contratada_PessoaNom ;
      private String[] T000C8_A42Contratada_PessoaCNPJ ;
      private bool[] T000C8_n42Contratada_PessoaCNPJ ;
      private String[] T000C8_A518Pessoa_IE ;
      private bool[] T000C8_n518Pessoa_IE ;
      private String[] T000C8_A519Pessoa_Endereco ;
      private bool[] T000C8_n519Pessoa_Endereco ;
      private String[] T000C8_A521Pessoa_CEP ;
      private bool[] T000C8_n521Pessoa_CEP ;
      private String[] T000C8_A522Pessoa_Telefone ;
      private bool[] T000C8_n522Pessoa_Telefone ;
      private String[] T000C8_A523Pessoa_Fax ;
      private bool[] T000C8_n523Pessoa_Fax ;
      private String[] T000C9_A350Contratada_UF ;
      private bool[] T000C9_n350Contratada_UF ;
      private String[] T000C10_A53Contratada_AreaTrabalhoDes ;
      private bool[] T000C10_n53Contratada_AreaTrabalhoDes ;
      private String[] T000C10_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] T000C10_n1592Contratada_AreaTrbClcPFnl ;
      private int[] T000C10_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] T000C10_n1595Contratada_AreaTrbSrvPdr ;
      private int[] T000C11_A39Contratada_Codigo ;
      private int[] T000C3_A39Contratada_Codigo ;
      private String[] T000C3_A438Contratada_Sigla ;
      private String[] T000C3_A516Contratada_TipoFabrica ;
      private String[] T000C3_A342Contratada_BancoNome ;
      private bool[] T000C3_n342Contratada_BancoNome ;
      private String[] T000C3_A343Contratada_BancoNro ;
      private bool[] T000C3_n343Contratada_BancoNro ;
      private String[] T000C3_A344Contratada_AgenciaNome ;
      private bool[] T000C3_n344Contratada_AgenciaNome ;
      private String[] T000C3_A345Contratada_AgenciaNro ;
      private bool[] T000C3_n345Contratada_AgenciaNro ;
      private String[] T000C3_A51Contratada_ContaCorrente ;
      private bool[] T000C3_n51Contratada_ContaCorrente ;
      private int[] T000C3_A524Contratada_OS ;
      private bool[] T000C3_n524Contratada_OS ;
      private int[] T000C3_A1451Contratada_SS ;
      private bool[] T000C3_n1451Contratada_SS ;
      private short[] T000C3_A530Contratada_Lote ;
      private bool[] T000C3_n530Contratada_Lote ;
      private bool[] T000C3_A1481Contratada_UsaOSistema ;
      private bool[] T000C3_n1481Contratada_UsaOSistema ;
      private bool[] T000C3_A1867Contratada_OSPreferencial ;
      private bool[] T000C3_n1867Contratada_OSPreferencial ;
      private int[] T000C3_A1953Contratada_CntPadrao ;
      private bool[] T000C3_n1953Contratada_CntPadrao ;
      private bool[] T000C3_A43Contratada_Ativo ;
      private String[] T000C3_A1129Contratada_LogoTipoArq ;
      private bool[] T000C3_n1129Contratada_LogoTipoArq ;
      private String[] T000C3_A1128Contratada_LogoNomeArq ;
      private bool[] T000C3_n1128Contratada_LogoNomeArq ;
      private String[] T000C3_A40000Contratada_Logo_GXI ;
      private bool[] T000C3_n40000Contratada_Logo_GXI ;
      private int[] T000C3_A40Contratada_PessoaCod ;
      private int[] T000C3_A52Contratada_AreaTrabalhoCod ;
      private int[] T000C3_A349Contratada_MunicipioCod ;
      private bool[] T000C3_n349Contratada_MunicipioCod ;
      private String[] T000C3_A1127Contratada_LogoArquivo ;
      private bool[] T000C3_n1127Contratada_LogoArquivo ;
      private String[] T000C3_A1664Contratada_Logo ;
      private bool[] T000C3_n1664Contratada_Logo ;
      private int[] T000C12_A39Contratada_Codigo ;
      private int[] T000C13_A39Contratada_Codigo ;
      private int[] T000C2_A39Contratada_Codigo ;
      private String[] T000C2_A438Contratada_Sigla ;
      private String[] T000C2_A516Contratada_TipoFabrica ;
      private String[] T000C2_A342Contratada_BancoNome ;
      private bool[] T000C2_n342Contratada_BancoNome ;
      private String[] T000C2_A343Contratada_BancoNro ;
      private bool[] T000C2_n343Contratada_BancoNro ;
      private String[] T000C2_A344Contratada_AgenciaNome ;
      private bool[] T000C2_n344Contratada_AgenciaNome ;
      private String[] T000C2_A345Contratada_AgenciaNro ;
      private bool[] T000C2_n345Contratada_AgenciaNro ;
      private String[] T000C2_A51Contratada_ContaCorrente ;
      private bool[] T000C2_n51Contratada_ContaCorrente ;
      private int[] T000C2_A524Contratada_OS ;
      private bool[] T000C2_n524Contratada_OS ;
      private int[] T000C2_A1451Contratada_SS ;
      private bool[] T000C2_n1451Contratada_SS ;
      private short[] T000C2_A530Contratada_Lote ;
      private bool[] T000C2_n530Contratada_Lote ;
      private bool[] T000C2_A1481Contratada_UsaOSistema ;
      private bool[] T000C2_n1481Contratada_UsaOSistema ;
      private bool[] T000C2_A1867Contratada_OSPreferencial ;
      private bool[] T000C2_n1867Contratada_OSPreferencial ;
      private int[] T000C2_A1953Contratada_CntPadrao ;
      private bool[] T000C2_n1953Contratada_CntPadrao ;
      private bool[] T000C2_A43Contratada_Ativo ;
      private String[] T000C2_A1129Contratada_LogoTipoArq ;
      private bool[] T000C2_n1129Contratada_LogoTipoArq ;
      private String[] T000C2_A1128Contratada_LogoNomeArq ;
      private bool[] T000C2_n1128Contratada_LogoNomeArq ;
      private String[] T000C2_A40000Contratada_Logo_GXI ;
      private bool[] T000C2_n40000Contratada_Logo_GXI ;
      private int[] T000C2_A40Contratada_PessoaCod ;
      private int[] T000C2_A52Contratada_AreaTrabalhoCod ;
      private int[] T000C2_A349Contratada_MunicipioCod ;
      private bool[] T000C2_n349Contratada_MunicipioCod ;
      private String[] T000C2_A1127Contratada_LogoArquivo ;
      private bool[] T000C2_n1127Contratada_LogoArquivo ;
      private String[] T000C2_A1664Contratada_Logo ;
      private bool[] T000C2_n1664Contratada_Logo ;
      private int[] T000C14_A39Contratada_Codigo ;
      private String[] T000C19_A41Contratada_PessoaNom ;
      private bool[] T000C19_n41Contratada_PessoaNom ;
      private String[] T000C19_A42Contratada_PessoaCNPJ ;
      private bool[] T000C19_n42Contratada_PessoaCNPJ ;
      private String[] T000C19_A518Pessoa_IE ;
      private bool[] T000C19_n518Pessoa_IE ;
      private String[] T000C19_A519Pessoa_Endereco ;
      private bool[] T000C19_n519Pessoa_Endereco ;
      private String[] T000C19_A521Pessoa_CEP ;
      private bool[] T000C19_n521Pessoa_CEP ;
      private String[] T000C19_A522Pessoa_Telefone ;
      private bool[] T000C19_n522Pessoa_Telefone ;
      private String[] T000C19_A523Pessoa_Fax ;
      private bool[] T000C19_n523Pessoa_Fax ;
      private String[] T000C20_A350Contratada_UF ;
      private bool[] T000C20_n350Contratada_UF ;
      private String[] T000C21_A53Contratada_AreaTrabalhoDes ;
      private bool[] T000C21_n53Contratada_AreaTrabalhoDes ;
      private String[] T000C21_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] T000C21_n1592Contratada_AreaTrbClcPFnl ;
      private int[] T000C21_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] T000C21_n1595Contratada_AreaTrbSrvPdr ;
      private int[] T000C22_A1653Tributo_Codigo ;
      private int[] T000C23_A1482Gestao_Codigo ;
      private int[] T000C24_A456ContagemResultado_Codigo ;
      private int[] T000C25_A456ContagemResultado_Codigo ;
      private int[] T000C26_A66ContratadaUsuario_ContratadaCod ;
      private int[] T000C26_A69ContratadaUsuario_UsuarioCod ;
      private int[] T000C27_A439Solicitacoes_Codigo ;
      private int[] T000C28_A74Contrato_Codigo ;
      private int[] T000C29_A39Contratada_Codigo ;
      private String[] T000C30_A23Estado_UF ;
      private String[] T000C30_A24Estado_Nome ;
      private int[] T000C31_A25Municipio_Codigo ;
      private String[] T000C31_A26Municipio_Nome ;
      private String[] T000C31_A23Estado_UF ;
      private String[] T000C32_A350Contratada_UF ;
      private bool[] T000C32_n350Contratada_UF ;
      private String[] T000C33_A41Contratada_PessoaNom ;
      private bool[] T000C33_n41Contratada_PessoaNom ;
      private String[] T000C33_A42Contratada_PessoaCNPJ ;
      private bool[] T000C33_n42Contratada_PessoaCNPJ ;
      private String[] T000C33_A518Pessoa_IE ;
      private bool[] T000C33_n518Pessoa_IE ;
      private String[] T000C33_A519Pessoa_Endereco ;
      private bool[] T000C33_n519Pessoa_Endereco ;
      private String[] T000C33_A521Pessoa_CEP ;
      private bool[] T000C33_n521Pessoa_CEP ;
      private String[] T000C33_A522Pessoa_Telefone ;
      private bool[] T000C33_n522Pessoa_Telefone ;
      private String[] T000C33_A523Pessoa_Fax ;
      private bool[] T000C33_n523Pessoa_Fax ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV35AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000C7 ;
          prmT000C7 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C4 ;
          prmT000C4 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C6 ;
          prmT000C6 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C5 ;
          prmT000C5 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C8 ;
          prmT000C8 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C9 ;
          prmT000C9 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C10 ;
          prmT000C10 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C11 ;
          prmT000C11 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C3 ;
          prmT000C3 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C12 ;
          prmT000C12 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C13 ;
          prmT000C13 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C2 ;
          prmT000C2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C14 ;
          prmT000C14 = new Object[] {
          new Object[] {"@Contratada_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratada_TipoFabrica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratada_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratada_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_OSPreferencial",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_CntPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_Logo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_Logo_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C15 ;
          prmT000C15 = new Object[] {
          new Object[] {"@Contratada_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratada_TipoFabrica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratada_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratada_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_OSPreferencial",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_CntPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C16 ;
          prmT000C16 = new Object[] {
          new Object[] {"@Contratada_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C17 ;
          prmT000C17 = new Object[] {
          new Object[] {"@Contratada_Logo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_Logo_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C18 ;
          prmT000C18 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C19 ;
          prmT000C19 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C20 ;
          prmT000C20 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C21 ;
          prmT000C21 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C22 ;
          prmT000C22 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C23 ;
          prmT000C23 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C24 ;
          prmT000C24 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C25 ;
          prmT000C25 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C26 ;
          prmT000C26 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C27 ;
          prmT000C27 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C28 ;
          prmT000C28 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C29 ;
          prmT000C29 = new Object[] {
          } ;
          Object[] prmT000C30 ;
          prmT000C30 = new Object[] {
          } ;
          Object[] prmT000C31 ;
          prmT000C31 = new Object[] {
          new Object[] {"@AV25Contratada_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000C32 ;
          prmT000C32 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000C33 ;
          prmT000C33 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000C2", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_Logo_GXI], [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod] AS Contratada_MunicipioCod, [Contratada_LogoArquivo], [Contratada_Logo] FROM [Contratada] WITH (UPDLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C2,1,0,true,false )
             ,new CursorDef("T000C3", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_Logo_GXI], [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod] AS Contratada_MunicipioCod, [Contratada_LogoArquivo], [Contratada_Logo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C3,1,0,true,false )
             ,new CursorDef("T000C4", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C4,1,0,true,false )
             ,new CursorDef("T000C5", "SELECT [AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, [AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, [AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C5,1,0,true,false )
             ,new CursorDef("T000C6", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C6,1,0,true,false )
             ,new CursorDef("T000C7", "SELECT TM1.[Contratada_Codigo], T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T2.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Pessoa_IE], T3.[Pessoa_Endereco], T3.[Pessoa_CEP], T3.[Pessoa_Telefone], T3.[Pessoa_Fax], TM1.[Contratada_Sigla], TM1.[Contratada_TipoFabrica], TM1.[Contratada_BancoNome], TM1.[Contratada_BancoNro], TM1.[Contratada_AgenciaNome], TM1.[Contratada_AgenciaNro], TM1.[Contratada_ContaCorrente], TM1.[Contratada_OS], TM1.[Contratada_SS], TM1.[Contratada_Lote], TM1.[Contratada_UsaOSistema], TM1.[Contratada_OSPreferencial], TM1.[Contratada_CntPadrao], TM1.[Contratada_Ativo], TM1.[Contratada_LogoTipoArq], TM1.[Contratada_LogoNomeArq], TM1.[Contratada_Logo_GXI], TM1.[Contratada_PessoaCod] AS Contratada_PessoaCod, TM1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, TM1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T4.[Estado_UF] AS Contratada_UF, TM1.[Contratada_LogoArquivo], TM1.[Contratada_Logo] FROM ((([Contratada] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Contratada_AreaTrabalhoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = TM1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = TM1.[Contratada_MunicipioCod]) WHERE TM1.[Contratada_Codigo] = @Contratada_Codigo ORDER BY TM1.[Contratada_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C7,100,0,true,false )
             ,new CursorDef("T000C8", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C8,1,0,true,false )
             ,new CursorDef("T000C9", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C9,1,0,true,false )
             ,new CursorDef("T000C10", "SELECT [AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, [AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, [AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C10,1,0,true,false )
             ,new CursorDef("T000C11", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C11,1,0,true,false )
             ,new CursorDef("T000C12", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ( [Contratada_Codigo] > @Contratada_Codigo) ORDER BY [Contratada_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C12,1,0,true,true )
             ,new CursorDef("T000C13", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ( [Contratada_Codigo] < @Contratada_Codigo) ORDER BY [Contratada_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C13,1,0,true,true )
             ,new CursorDef("T000C14", "INSERT INTO [Contratada]([Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_LogoArquivo], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_Logo], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_Logo_GXI], [Contratada_PessoaCod], [Contratada_AreaTrabalhoCod], [Contratada_MunicipioCod]) VALUES(@Contratada_Sigla, @Contratada_TipoFabrica, @Contratada_BancoNome, @Contratada_BancoNro, @Contratada_AgenciaNome, @Contratada_AgenciaNro, @Contratada_ContaCorrente, @Contratada_OS, @Contratada_SS, @Contratada_Lote, @Contratada_LogoArquivo, @Contratada_UsaOSistema, @Contratada_OSPreferencial, @Contratada_CntPadrao, @Contratada_Ativo, @Contratada_Logo, @Contratada_LogoTipoArq, @Contratada_LogoNomeArq, @Contratada_Logo_GXI, @Contratada_PessoaCod, @Contratada_AreaTrabalhoCod, @Contratada_MunicipioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000C14)
             ,new CursorDef("T000C15", "UPDATE [Contratada] SET [Contratada_Sigla]=@Contratada_Sigla, [Contratada_TipoFabrica]=@Contratada_TipoFabrica, [Contratada_BancoNome]=@Contratada_BancoNome, [Contratada_BancoNro]=@Contratada_BancoNro, [Contratada_AgenciaNome]=@Contratada_AgenciaNome, [Contratada_AgenciaNro]=@Contratada_AgenciaNro, [Contratada_ContaCorrente]=@Contratada_ContaCorrente, [Contratada_OS]=@Contratada_OS, [Contratada_SS]=@Contratada_SS, [Contratada_Lote]=@Contratada_Lote, [Contratada_UsaOSistema]=@Contratada_UsaOSistema, [Contratada_OSPreferencial]=@Contratada_OSPreferencial, [Contratada_CntPadrao]=@Contratada_CntPadrao, [Contratada_Ativo]=@Contratada_Ativo, [Contratada_LogoTipoArq]=@Contratada_LogoTipoArq, [Contratada_LogoNomeArq]=@Contratada_LogoNomeArq, [Contratada_PessoaCod]=@Contratada_PessoaCod, [Contratada_AreaTrabalhoCod]=@Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod]=@Contratada_MunicipioCod  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmT000C15)
             ,new CursorDef("T000C16", "UPDATE [Contratada] SET [Contratada_LogoArquivo]=@Contratada_LogoArquivo  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmT000C16)
             ,new CursorDef("T000C17", "UPDATE [Contratada] SET [Contratada_Logo]=@Contratada_Logo, [Contratada_Logo_GXI]=@Contratada_Logo_GXI  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmT000C17)
             ,new CursorDef("T000C18", "DELETE FROM [Contratada]  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmT000C18)
             ,new CursorDef("T000C19", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C19,1,0,true,false )
             ,new CursorDef("T000C20", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C20,1,0,true,false )
             ,new CursorDef("T000C21", "SELECT [AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, [AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, [AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C21,1,0,true,false )
             ,new CursorDef("T000C22", "SELECT TOP 1 [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) WHERE [Tributo_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C22,1,0,true,true )
             ,new CursorDef("T000C23", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C23,1,0,true,true )
             ,new CursorDef("T000C24", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContratadaOrigemCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C24,1,0,true,true )
             ,new CursorDef("T000C25", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C25,1,0,true,true )
             ,new CursorDef("T000C26", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C26,1,0,true,true )
             ,new CursorDef("T000C27", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C27,1,0,true,true )
             ,new CursorDef("T000C28", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C28,1,0,true,true )
             ,new CursorDef("T000C29", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) ORDER BY [Contratada_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C29,100,0,true,false )
             ,new CursorDef("T000C30", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C30,0,0,true,false )
             ,new CursorDef("T000C31", "SELECT [Municipio_Codigo], [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Estado_UF] = @AV25Contratada_UF ORDER BY [Municipio_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C31,0,0,true,false )
             ,new CursorDef("T000C32", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C32,1,0,true,false )
             ,new CursorDef("T000C33", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C33,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((String[]) buf[26])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getMultimediaUri(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getBLOBFile(22, rslt.getString(16, 10), rslt.getString(17, 50)) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getMultimediaFile(23, rslt.getVarchar(18)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((String[]) buf[26])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getMultimediaUri(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getBLOBFile(22, rslt.getString(16, 10), rslt.getString(17, 50)) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getMultimediaFile(23, rslt.getVarchar(18)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((short[]) buf[35])[0] = rslt.getShort(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((bool[]) buf[37])[0] = rslt.getBool(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((bool[]) buf[39])[0] = rslt.getBool(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((bool[]) buf[43])[0] = rslt.getBool(24) ;
                ((String[]) buf[44])[0] = rslt.getString(25, 10) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((String[]) buf[46])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((String[]) buf[48])[0] = rslt.getMultimediaUri(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((int[]) buf[51])[0] = rslt.getInt(29) ;
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((String[]) buf[56])[0] = rslt.getString(32, 2) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((String[]) buf[58])[0] = rslt.getBLOBFile(33, rslt.getString(25, 10), rslt.getString(26, 50)) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((String[]) buf[60])[0] = rslt.getMultimediaFile(34, rslt.getVarchar(27)) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(34);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                stmt.SetParameter(15, (bool)parms[26]);
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(19, (String)parms[34], (String)parms[28]);
                }
                stmt.SetParameter(20, (int)parms[35]);
                stmt.SetParameter(21, (int)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[23]);
                }
                stmt.SetParameter(14, (bool)parms[24]);
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[28]);
                }
                stmt.SetParameter(17, (int)parms[29]);
                stmt.SetParameter(18, (int)parms[30]);
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[32]);
                }
                stmt.SetParameter(20, (int)parms[33]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
