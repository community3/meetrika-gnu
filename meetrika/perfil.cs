/*
               File: Perfil
        Description: Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:14:5.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class perfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A7Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A7Perfil_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Perfil_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkPerfil_Ativo.Name = "PERFIL_ATIVO";
         chkPerfil_Ativo.WebTags = "";
         chkPerfil_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPerfil_Ativo_Internalname, "TitleCaption", chkPerfil_Ativo.Caption);
         chkPerfil_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPerfil_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public perfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public perfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Perfil_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Perfil_Codigo = aP1_Perfil_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkPerfil_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_032( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_032e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")), ((edtPerfil_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPerfil_Codigo_Visible, edtPerfil_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Perfil.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_032( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPerfiltitle_Internalname, "Perfil", "", "", lblPerfiltitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_032( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_032e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_032( true) ;
         }
         return  ;
      }

      protected void wb_table3_29_032e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_032e( true) ;
         }
         else
         {
            wb_table1_2_032e( false) ;
         }
      }

      protected void wb_table3_29_032( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_032e( true) ;
         }
         else
         {
            wb_table3_29_032e( false) ;
         }
      }

      protected void wb_table2_8_032( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_032( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_032e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_032e( true) ;
         }
         else
         {
            wb_table2_8_032e( false) ;
         }
      }

      protected void wb_table4_16_032( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_nome_Internalname, "Nome", "", "", lblTextblockperfil_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPerfil_Nome_Internalname, StringUtil.RTrim( A4Perfil_Nome), StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPerfil_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_ativo_Internalname, "Ativo", "", "", lblTextblockperfil_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockperfil_ativo_Visible, 1, 0, "HLP_Perfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkPerfil_Ativo_Internalname, StringUtil.BoolToStr( A276Perfil_Ativo), "", "", chkPerfil_Ativo.Visible, chkPerfil_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(26, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_032e( true) ;
         }
         else
         {
            wb_table4_16_032e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11032 */
         E11032 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
               A276Perfil_Ativo = StringUtil.StrToBool( cgiGet( chkPerfil_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
               A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               /* Read saved values. */
               Z3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z3Perfil_Codigo"), ",", "."));
               Z4Perfil_Nome = cgiGet( "Z4Perfil_Nome");
               Z275Perfil_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z275Perfil_Tipo"), ",", "."));
               Z328Perfil_GamGUID = cgiGet( "Z328Perfil_GamGUID");
               Z329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( "Z329Perfil_GamId"), ",", "."));
               Z276Perfil_Ativo = StringUtil.StrToBool( cgiGet( "Z276Perfil_Ativo"));
               Z7Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z7Perfil_AreaTrabalhoCod"), ",", "."));
               A275Perfil_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z275Perfil_Tipo"), ",", "."));
               A328Perfil_GamGUID = cgiGet( "Z328Perfil_GamGUID");
               A329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( "Z329Perfil_GamId"), ",", "."));
               A7Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z7Perfil_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N7Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N7Perfil_AreaTrabalhoCod"), ",", "."));
               AV7Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPERFIL_CODIGO"), ",", "."));
               AV11Insert_Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PERFIL_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A7Perfil_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "PERFIL_AREATRABALHOCOD"), ",", "."));
               A275Perfil_Tipo = (short)(context.localUtil.CToN( cgiGet( "PERFIL_TIPO"), ",", "."));
               A328Perfil_GamGUID = cgiGet( "PERFIL_GAMGUID");
               A329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( "PERFIL_GAMID"), ",", "."));
               A8Perfil_AreaTrabalhoDes = cgiGet( "PERFIL_AREATRABALHODES");
               n8Perfil_AreaTrabalhoDes = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Perfil";
               A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A7Perfil_AreaTrabalhoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A328Perfil_GamGUID, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A3Perfil_Codigo != Z3Perfil_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Perfil_Codigo:"+context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Perfil_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A7Perfil_AreaTrabalhoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Perfil_Tipo:"+context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9"));
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Perfil_GamGUID:"+StringUtil.RTrim( context.localUtil.Format( A328Perfil_GamGUID, "")));
                  GXUtil.WriteLog("perfil:[SecurityCheckFailed value for]"+"Perfil_GamId:"+context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode2 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode2;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound2 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_030( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PERFIL_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtPerfil_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11032 */
                           E11032 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12032 */
                           E12032 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12032 */
            E12032 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll032( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes032( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_030( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls032( ) ;
            }
            else
            {
               CheckExtendedTable032( ) ;
               CloseExtendedTableCursors032( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption030( )
      {
      }

      protected void E11032( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Perfil_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Perfil_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtPerfil_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Visible), 5, 0)));
      }

      protected void E12032( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwperfil.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         pr_default.close(1);
         pr_default.close(2);
         returnInSub = true;
         if (true) return;
      }

      protected void ZM032( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z4Perfil_Nome = T00033_A4Perfil_Nome[0];
               Z275Perfil_Tipo = T00033_A275Perfil_Tipo[0];
               Z328Perfil_GamGUID = T00033_A328Perfil_GamGUID[0];
               Z329Perfil_GamId = T00033_A329Perfil_GamId[0];
               Z276Perfil_Ativo = T00033_A276Perfil_Ativo[0];
               Z7Perfil_AreaTrabalhoCod = T00033_A7Perfil_AreaTrabalhoCod[0];
            }
            else
            {
               Z4Perfil_Nome = A4Perfil_Nome;
               Z275Perfil_Tipo = A275Perfil_Tipo;
               Z328Perfil_GamGUID = A328Perfil_GamGUID;
               Z329Perfil_GamId = A329Perfil_GamId;
               Z276Perfil_Ativo = A276Perfil_Ativo;
               Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -10 )
         {
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z328Perfil_GamGUID = A328Perfil_GamGUID;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
            Z8Perfil_AreaTrabalhoDes = A8Perfil_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtPerfil_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "Perfil";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtPerfil_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Perfil_Codigo) )
         {
            A3Perfil_Codigo = AV7Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkPerfil_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPerfil_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkPerfil_Ativo.Visible), 5, 0)));
         lblTextblockperfil_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockperfil_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockperfil_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Perfil_AreaTrabalhoCod) )
         {
            A7Perfil_AreaTrabalhoCod = AV11Insert_Perfil_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A7Perfil_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A276Perfil_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A276Perfil_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00034 */
            pr_default.execute(2, new Object[] {A7Perfil_AreaTrabalhoCod});
            A8Perfil_AreaTrabalhoDes = T00034_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = T00034_n8Perfil_AreaTrabalhoDes[0];
            pr_default.close(2);
         }
      }

      protected void Load032( )
      {
         /* Using cursor T00035 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound2 = 1;
            A4Perfil_Nome = T00035_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A8Perfil_AreaTrabalhoDes = T00035_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = T00035_n8Perfil_AreaTrabalhoDes[0];
            A275Perfil_Tipo = T00035_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = T00035_A328Perfil_GamGUID[0];
            A329Perfil_GamId = T00035_A329Perfil_GamId[0];
            A276Perfil_Ativo = T00035_A276Perfil_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
            A7Perfil_AreaTrabalhoCod = T00035_A7Perfil_AreaTrabalhoCod[0];
            ZM032( -10) ;
         }
         pr_default.close(3);
         OnLoadActions032( ) ;
      }

      protected void OnLoadActions032( )
      {
      }

      protected void CheckExtendedTable032( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00034 */
         pr_default.execute(2, new Object[] {A7Perfil_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Perfil Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A8Perfil_AreaTrabalhoDes = T00034_A8Perfil_AreaTrabalhoDes[0];
         n8Perfil_AreaTrabalhoDes = T00034_n8Perfil_AreaTrabalhoDes[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors032( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A7Perfil_AreaTrabalhoCod )
      {
         /* Using cursor T00036 */
         pr_default.execute(4, new Object[] {A7Perfil_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Perfil Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A8Perfil_AreaTrabalhoDes = T00036_A8Perfil_AreaTrabalhoDes[0];
         n8Perfil_AreaTrabalhoDes = T00036_n8Perfil_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A8Perfil_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey032( )
      {
         /* Using cursor T00037 */
         pr_default.execute(5, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00033 */
         pr_default.execute(1, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM032( 10) ;
            RcdFound2 = 1;
            A3Perfil_Codigo = T00033_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            A4Perfil_Nome = T00033_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A275Perfil_Tipo = T00033_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = T00033_A328Perfil_GamGUID[0];
            A329Perfil_GamId = T00033_A329Perfil_GamId[0];
            A276Perfil_Ativo = T00033_A276Perfil_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
            A7Perfil_AreaTrabalhoCod = T00033_A7Perfil_AreaTrabalhoCod[0];
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load032( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey032( ) ;
            }
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey032( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey032( ) ;
         if ( RcdFound2 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound2 = 0;
         /* Using cursor T00038 */
         pr_default.execute(6, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00038_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00038_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               A3Perfil_Codigo = T00038_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound2 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound2 = 0;
         /* Using cursor T00039 */
         pr_default.execute(7, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00039_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00039_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               A3Perfil_Codigo = T00039_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound2 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey032( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPerfil_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert032( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( A3Perfil_Codigo != Z3Perfil_Codigo )
               {
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PERFIL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPerfil_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPerfil_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update032( ) ;
                  GX_FocusControl = edtPerfil_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A3Perfil_Codigo != Z3Perfil_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtPerfil_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert032( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PERFIL_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtPerfil_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPerfil_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert032( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A3Perfil_Codigo != Z3Perfil_Codigo )
         {
            A3Perfil_Codigo = Z3Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPerfil_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency032( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00032 */
            pr_default.execute(0, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Perfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z4Perfil_Nome, T00032_A4Perfil_Nome[0]) != 0 ) || ( Z275Perfil_Tipo != T00032_A275Perfil_Tipo[0] ) || ( StringUtil.StrCmp(Z328Perfil_GamGUID, T00032_A328Perfil_GamGUID[0]) != 0 ) || ( Z329Perfil_GamId != T00032_A329Perfil_GamId[0] ) || ( Z276Perfil_Ativo != T00032_A276Perfil_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z7Perfil_AreaTrabalhoCod != T00032_A7Perfil_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z4Perfil_Nome, T00032_A4Perfil_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z4Perfil_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00032_A4Perfil_Nome[0]);
               }
               if ( Z275Perfil_Tipo != T00032_A275Perfil_Tipo[0] )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z275Perfil_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T00032_A275Perfil_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z328Perfil_GamGUID, T00032_A328Perfil_GamGUID[0]) != 0 )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_GamGUID");
                  GXUtil.WriteLogRaw("Old: ",Z328Perfil_GamGUID);
                  GXUtil.WriteLogRaw("Current: ",T00032_A328Perfil_GamGUID[0]);
               }
               if ( Z329Perfil_GamId != T00032_A329Perfil_GamId[0] )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_GamId");
                  GXUtil.WriteLogRaw("Old: ",Z329Perfil_GamId);
                  GXUtil.WriteLogRaw("Current: ",T00032_A329Perfil_GamId[0]);
               }
               if ( Z276Perfil_Ativo != T00032_A276Perfil_Ativo[0] )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z276Perfil_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00032_A276Perfil_Ativo[0]);
               }
               if ( Z7Perfil_AreaTrabalhoCod != T00032_A7Perfil_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("perfil:[seudo value changed for attri]"+"Perfil_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z7Perfil_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00032_A7Perfil_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Perfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert032( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable032( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM032( 0) ;
            CheckOptimisticConcurrency032( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm032( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert032( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000310 */
                     pr_default.execute(8, new Object[] {A4Perfil_Nome, A275Perfil_Tipo, A328Perfil_GamGUID, A329Perfil_GamId, A276Perfil_Ativo, A7Perfil_AreaTrabalhoCod});
                     A3Perfil_Codigo = T000310_A3Perfil_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption030( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load032( ) ;
            }
            EndLevel032( ) ;
         }
         CloseExtendedTableCursors032( ) ;
      }

      protected void Update032( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable032( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency032( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm032( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate032( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000311 */
                     pr_default.execute(9, new Object[] {A4Perfil_Nome, A275Perfil_Tipo, A328Perfil_GamGUID, A329Perfil_GamId, A276Perfil_Ativo, A7Perfil_AreaTrabalhoCod, A3Perfil_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Perfil"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate032( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel032( ) ;
         }
         CloseExtendedTableCursors032( ) ;
      }

      protected void DeferredUpdate032( )
      {
      }

      protected void delete( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency032( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls032( ) ;
            AfterConfirm032( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete032( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000312 */
                  pr_default.execute(10, new Object[] {A3Perfil_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel032( ) ;
         Gx_mode = sMode2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls032( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000313 */
            pr_default.execute(11, new Object[] {A7Perfil_AreaTrabalhoCod});
            A8Perfil_AreaTrabalhoDes = T000313_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = T000313_n8Perfil_AreaTrabalhoDes[0];
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000314 */
            pr_default.execute(12, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T000315 */
            pr_default.execute(13, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario x Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel032( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete032( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Perfil");
            if ( AnyError == 0 )
            {
               ConfirmValues030( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Perfil");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart032( )
      {
         /* Scan By routine */
         /* Using cursor T000316 */
         pr_default.execute(14);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound2 = 1;
            A3Perfil_Codigo = T000316_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext032( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound2 = 1;
            A3Perfil_Codigo = T000316_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd032( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm032( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert032( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate032( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete032( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete032( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate032( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes032( )
      {
         edtPerfil_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Nome_Enabled), 5, 0)));
         chkPerfil_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPerfil_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkPerfil_Ativo.Enabled), 5, 0)));
         edtPerfil_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues030( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311714672");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("perfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Perfil_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z3Perfil_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z4Perfil_Nome", StringUtil.RTrim( Z4Perfil_Nome));
         GxWebStd.gx_hidden_field( context, "Z275Perfil_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z275Perfil_Tipo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z328Perfil_GamGUID", StringUtil.RTrim( Z328Perfil_GamGUID));
         GxWebStd.gx_hidden_field( context, "Z329Perfil_GamId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z329Perfil_GamId), 12, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z276Perfil_Ativo", Z276Perfil_Ativo);
         GxWebStd.gx_hidden_field( context, "Z7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PERFIL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_GAMGUID", StringUtil.RTrim( A328Perfil_GamGUID));
         GxWebStd.gx_hidden_field( context, "PERFIL_GAMID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERFIL_AREATRABALHODES", A8Perfil_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Perfil";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A7Perfil_AreaTrabalhoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A328Perfil_GamGUID, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Perfil_Codigo:"+context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Perfil_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A7Perfil_AreaTrabalhoCod), "ZZZZZ9"));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Perfil_Tipo:"+context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9"));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Perfil_GamGUID:"+StringUtil.RTrim( context.localUtil.Format( A328Perfil_GamGUID, "")));
         GXUtil.WriteLog("perfil:[SendSecurityCheck value for]"+"Perfil_GamId:"+context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("perfil.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Perfil_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Perfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Perfil" ;
      }

      protected void InitializeNonKey032( )
      {
         A4Perfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         A8Perfil_AreaTrabalhoDes = "";
         n8Perfil_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Perfil_AreaTrabalhoDes", A8Perfil_AreaTrabalhoDes);
         A275Perfil_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         A328Perfil_GamGUID = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A328Perfil_GamGUID", A328Perfil_GamGUID);
         A329Perfil_GamId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
         A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
         A276Perfil_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
         Z4Perfil_Nome = "";
         Z275Perfil_Tipo = 0;
         Z328Perfil_GamGUID = "";
         Z329Perfil_GamId = 0;
         Z276Perfil_Ativo = false;
         Z7Perfil_AreaTrabalhoCod = 0;
      }

      protected void InitAll032( )
      {
         A3Perfil_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         InitializeNonKey032( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A7Perfil_AreaTrabalhoCod = i7Perfil_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A7Perfil_AreaTrabalhoCod), 6, 0)));
         A276Perfil_Ativo = i276Perfil_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A276Perfil_Ativo", A276Perfil_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031171471");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("perfil.js", "?202031171472");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblPerfiltitle_Internalname = "PERFILTITLE";
         lblTextblockperfil_nome_Internalname = "TEXTBLOCKPERFIL_NOME";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         lblTextblockperfil_ativo_Internalname = "TEXTBLOCKPERFIL_ATIVO";
         chkPerfil_Ativo_Internalname = "PERFIL_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Perfil";
         chkPerfil_Ativo.Enabled = 1;
         chkPerfil_Ativo.Visible = 1;
         lblTextblockperfil_ativo_Visible = 1;
         edtPerfil_Nome_Jsonclick = "";
         edtPerfil_Nome_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtPerfil_Codigo_Jsonclick = "";
         edtPerfil_Codigo_Enabled = 0;
         edtPerfil_Codigo_Visible = 1;
         chkPerfil_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12032',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z4Perfil_Nome = "";
         Z328Perfil_GamGUID = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblPerfiltitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockperfil_nome_Jsonclick = "";
         A4Perfil_Nome = "";
         lblTextblockperfil_ativo_Jsonclick = "";
         A328Perfil_GamGUID = "";
         A8Perfil_AreaTrabalhoDes = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode2 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z8Perfil_AreaTrabalhoDes = "";
         T00034_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         T00034_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         T00035_A3Perfil_Codigo = new int[1] ;
         T00035_A4Perfil_Nome = new String[] {""} ;
         T00035_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         T00035_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         T00035_A275Perfil_Tipo = new short[1] ;
         T00035_A328Perfil_GamGUID = new String[] {""} ;
         T00035_A329Perfil_GamId = new long[1] ;
         T00035_A276Perfil_Ativo = new bool[] {false} ;
         T00035_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T00036_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         T00036_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         T00037_A3Perfil_Codigo = new int[1] ;
         T00033_A3Perfil_Codigo = new int[1] ;
         T00033_A4Perfil_Nome = new String[] {""} ;
         T00033_A275Perfil_Tipo = new short[1] ;
         T00033_A328Perfil_GamGUID = new String[] {""} ;
         T00033_A329Perfil_GamId = new long[1] ;
         T00033_A276Perfil_Ativo = new bool[] {false} ;
         T00033_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T00038_A3Perfil_Codigo = new int[1] ;
         T00039_A3Perfil_Codigo = new int[1] ;
         T00032_A3Perfil_Codigo = new int[1] ;
         T00032_A4Perfil_Nome = new String[] {""} ;
         T00032_A275Perfil_Tipo = new short[1] ;
         T00032_A328Perfil_GamGUID = new String[] {""} ;
         T00032_A329Perfil_GamId = new long[1] ;
         T00032_A276Perfil_Ativo = new bool[] {false} ;
         T00032_A7Perfil_AreaTrabalhoCod = new int[1] ;
         T000310_A3Perfil_Codigo = new int[1] ;
         T000313_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         T000313_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         T000314_A277Menu_Codigo = new int[1] ;
         T000314_A3Perfil_Codigo = new int[1] ;
         T000315_A1Usuario_Codigo = new int[1] ;
         T000315_A3Perfil_Codigo = new int[1] ;
         T000316_A3Perfil_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.perfil__default(),
            new Object[][] {
                new Object[] {
               T00032_A3Perfil_Codigo, T00032_A4Perfil_Nome, T00032_A275Perfil_Tipo, T00032_A328Perfil_GamGUID, T00032_A329Perfil_GamId, T00032_A276Perfil_Ativo, T00032_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T00033_A3Perfil_Codigo, T00033_A4Perfil_Nome, T00033_A275Perfil_Tipo, T00033_A328Perfil_GamGUID, T00033_A329Perfil_GamId, T00033_A276Perfil_Ativo, T00033_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T00034_A8Perfil_AreaTrabalhoDes, T00034_n8Perfil_AreaTrabalhoDes
               }
               , new Object[] {
               T00035_A3Perfil_Codigo, T00035_A4Perfil_Nome, T00035_A8Perfil_AreaTrabalhoDes, T00035_n8Perfil_AreaTrabalhoDes, T00035_A275Perfil_Tipo, T00035_A328Perfil_GamGUID, T00035_A329Perfil_GamId, T00035_A276Perfil_Ativo, T00035_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               T00036_A8Perfil_AreaTrabalhoDes, T00036_n8Perfil_AreaTrabalhoDes
               }
               , new Object[] {
               T00037_A3Perfil_Codigo
               }
               , new Object[] {
               T00038_A3Perfil_Codigo
               }
               , new Object[] {
               T00039_A3Perfil_Codigo
               }
               , new Object[] {
               T000310_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000313_A8Perfil_AreaTrabalhoDes, T000313_n8Perfil_AreaTrabalhoDes
               }
               , new Object[] {
               T000314_A277Menu_Codigo, T000314_A3Perfil_Codigo
               }
               , new Object[] {
               T000315_A1Usuario_Codigo, T000315_A3Perfil_Codigo
               }
               , new Object[] {
               T000316_A3Perfil_Codigo
               }
            }
         );
         Z276Perfil_Ativo = true;
         A276Perfil_Ativo = true;
         i276Perfil_Ativo = true;
         AV14Pgmname = "Perfil";
         Z7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z275Perfil_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A275Perfil_Tipo ;
      private short Gx_BScreen ;
      private short RcdFound2 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Perfil_Codigo ;
      private int Z3Perfil_Codigo ;
      private int Z7Perfil_AreaTrabalhoCod ;
      private int N7Perfil_AreaTrabalhoCod ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int AV7Perfil_Codigo ;
      private int trnEnded ;
      private int A3Perfil_Codigo ;
      private int edtPerfil_Codigo_Enabled ;
      private int edtPerfil_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtPerfil_Nome_Enabled ;
      private int lblTextblockperfil_ativo_Visible ;
      private int AV11Insert_Perfil_AreaTrabalhoCod ;
      private int AV15GXV1 ;
      private int i7Perfil_AreaTrabalhoCod ;
      private int idxLst ;
      private long Z329Perfil_GamId ;
      private long A329Perfil_GamId ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z4Perfil_Nome ;
      private String Z328Perfil_GamGUID ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkPerfil_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPerfil_Nome_Internalname ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblPerfiltitle_Internalname ;
      private String lblPerfiltitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockperfil_nome_Internalname ;
      private String lblTextblockperfil_nome_Jsonclick ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Jsonclick ;
      private String lblTextblockperfil_ativo_Internalname ;
      private String lblTextblockperfil_ativo_Jsonclick ;
      private String A328Perfil_GamGUID ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode2 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z276Perfil_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A276Perfil_Ativo ;
      private bool n8Perfil_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i276Perfil_Ativo ;
      private String A8Perfil_AreaTrabalhoDes ;
      private String Z8Perfil_AreaTrabalhoDes ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkPerfil_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T00034_A8Perfil_AreaTrabalhoDes ;
      private bool[] T00034_n8Perfil_AreaTrabalhoDes ;
      private int[] T00035_A3Perfil_Codigo ;
      private String[] T00035_A4Perfil_Nome ;
      private String[] T00035_A8Perfil_AreaTrabalhoDes ;
      private bool[] T00035_n8Perfil_AreaTrabalhoDes ;
      private short[] T00035_A275Perfil_Tipo ;
      private String[] T00035_A328Perfil_GamGUID ;
      private long[] T00035_A329Perfil_GamId ;
      private bool[] T00035_A276Perfil_Ativo ;
      private int[] T00035_A7Perfil_AreaTrabalhoCod ;
      private String[] T00036_A8Perfil_AreaTrabalhoDes ;
      private bool[] T00036_n8Perfil_AreaTrabalhoDes ;
      private int[] T00037_A3Perfil_Codigo ;
      private int[] T00033_A3Perfil_Codigo ;
      private String[] T00033_A4Perfil_Nome ;
      private short[] T00033_A275Perfil_Tipo ;
      private String[] T00033_A328Perfil_GamGUID ;
      private long[] T00033_A329Perfil_GamId ;
      private bool[] T00033_A276Perfil_Ativo ;
      private int[] T00033_A7Perfil_AreaTrabalhoCod ;
      private int[] T00038_A3Perfil_Codigo ;
      private int[] T00039_A3Perfil_Codigo ;
      private int[] T00032_A3Perfil_Codigo ;
      private String[] T00032_A4Perfil_Nome ;
      private short[] T00032_A275Perfil_Tipo ;
      private String[] T00032_A328Perfil_GamGUID ;
      private long[] T00032_A329Perfil_GamId ;
      private bool[] T00032_A276Perfil_Ativo ;
      private int[] T00032_A7Perfil_AreaTrabalhoCod ;
      private int[] T000310_A3Perfil_Codigo ;
      private String[] T000313_A8Perfil_AreaTrabalhoDes ;
      private bool[] T000313_n8Perfil_AreaTrabalhoDes ;
      private int[] T000314_A277Menu_Codigo ;
      private int[] T000314_A3Perfil_Codigo ;
      private int[] T000315_A1Usuario_Codigo ;
      private int[] T000315_A3Perfil_Codigo ;
      private int[] T000316_A3Perfil_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class perfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00035 ;
          prmT00035 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00034 ;
          prmT00034 = new Object[] {
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00036 ;
          prmT00036 = new Object[] {
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00037 ;
          prmT00037 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00033 ;
          prmT00033 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00038 ;
          prmT00038 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00039 ;
          prmT00039 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00032 ;
          prmT00032 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000310 ;
          prmT000310 = new Object[] {
          new Object[] {"@Perfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Perfil_Tipo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Perfil_GamGUID",SqlDbType.Char,40,0} ,
          new Object[] {"@Perfil_GamId",SqlDbType.Decimal,12,0} ,
          new Object[] {"@Perfil_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000311 ;
          prmT000311 = new Object[] {
          new Object[] {"@Perfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Perfil_Tipo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Perfil_GamGUID",SqlDbType.Char,40,0} ,
          new Object[] {"@Perfil_GamId",SqlDbType.Decimal,12,0} ,
          new Object[] {"@Perfil_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000312 ;
          prmT000312 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000313 ;
          prmT000313 = new Object[] {
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000314 ;
          prmT000314 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000315 ;
          prmT000315 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000316 ;
          prmT000316 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00032", "SELECT [Perfil_Codigo], [Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM [Perfil] WITH (UPDLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00032,1,0,true,false )
             ,new CursorDef("T00033", "SELECT [Perfil_Codigo], [Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00033,1,0,true,false )
             ,new CursorDef("T00034", "SELECT [AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Perfil_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00034,1,0,true,false )
             ,new CursorDef("T00035", "SELECT TM1.[Perfil_Codigo], TM1.[Perfil_Nome], T2.[AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes, TM1.[Perfil_Tipo], TM1.[Perfil_GamGUID], TM1.[Perfil_GamId], TM1.[Perfil_Ativo], TM1.[Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM ([Perfil] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Perfil_AreaTrabalhoCod]) WHERE TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00035,100,0,true,false )
             ,new CursorDef("T00036", "SELECT [AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Perfil_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00036,1,0,true,false )
             ,new CursorDef("T00037", "SELECT [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00037,1,0,true,false )
             ,new CursorDef("T00038", "SELECT TOP 1 [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE ( [Perfil_Codigo] > @Perfil_Codigo) ORDER BY [Perfil_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00038,1,0,true,true )
             ,new CursorDef("T00039", "SELECT TOP 1 [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE ( [Perfil_Codigo] < @Perfil_Codigo) ORDER BY [Perfil_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00039,1,0,true,true )
             ,new CursorDef("T000310", "INSERT INTO [Perfil]([Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod]) VALUES(@Perfil_Nome, @Perfil_Tipo, @Perfil_GamGUID, @Perfil_GamId, @Perfil_Ativo, @Perfil_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000310)
             ,new CursorDef("T000311", "UPDATE [Perfil] SET [Perfil_Nome]=@Perfil_Nome, [Perfil_Tipo]=@Perfil_Tipo, [Perfil_GamGUID]=@Perfil_GamGUID, [Perfil_GamId]=@Perfil_GamId, [Perfil_Ativo]=@Perfil_Ativo, [Perfil_AreaTrabalhoCod]=@Perfil_AreaTrabalhoCod  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmT000311)
             ,new CursorDef("T000312", "DELETE FROM [Perfil]  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmT000312)
             ,new CursorDef("T000313", "SELECT [AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Perfil_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000313,1,0,true,false )
             ,new CursorDef("T000314", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000314,1,0,true,true )
             ,new CursorDef("T000315", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000315,1,0,true,true )
             ,new CursorDef("T000316", "SELECT [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) ORDER BY [Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000316,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 40) ;
                ((long[]) buf[4])[0] = rslt.getLong(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 40) ;
                ((long[]) buf[4])[0] = rslt.getLong(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 40) ;
                ((long[]) buf[6])[0] = rslt.getLong(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (long)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (long)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
