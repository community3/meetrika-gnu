/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:23:20.51
*/
gx.evt.autoSkip = false;
gx.define('wwsaldocontrato', false, function () {
   this.ServerClass =  "wwsaldocontrato" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV77Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV27DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV26DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
      this.A1560NotaEmpenho_Codigo=gx.fn.getIntegerValue("NOTAEMPENHO_CODIGO",'.') ;
   };
   this.Validv_Saldocontrato_vigenciainicio1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV16SaldoContrato_VigenciaInicio1)==0) || new gx.date.gxdate( this.AV16SaldoContrato_VigenciaInicio1 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Saldocontrato_vigenciainicio_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV17SaldoContrato_VigenciaInicio_To1)==0) || new gx.date.gxdate( this.AV17SaldoContrato_VigenciaInicio_To1 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Saldocontrato_vigenciainicio2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV20SaldoContrato_VigenciaInicio2)==0) || new gx.date.gxdate( this.AV20SaldoContrato_VigenciaInicio2 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Saldocontrato_vigenciainicio_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV21SaldoContrato_VigenciaInicio_To2)==0) || new gx.date.gxdate( this.AV21SaldoContrato_VigenciaInicio_To2 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Saldocontrato_vigenciainicio3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV24SaldoContrato_VigenciaInicio3)==0) || new gx.date.gxdate( this.AV24SaldoContrato_VigenciaInicio3 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Saldocontrato_vigenciainicio_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSALDOCONTRATO_VIGENCIAINICIO_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV25SaldoContrato_VigenciaInicio_To3)==0) || new gx.date.gxdate( this.AV25SaldoContrato_VigenciaInicio_To3 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Saldo Contrato_Vigencia Inicio_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Saldocontrato_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(94) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("SALDOCONTRATO_CODIGO", gx.fn.currentGridRowImpl(94));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfsaldocontrato_vigenciainicio=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFSALDOCONTRATO_VIGENCIAINICIO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV42TFSaldoContrato_VigenciaInicio)==0) || new gx.date.gxdate( this.AV42TFSaldoContrato_VigenciaInicio ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFSaldo Contrato_Vigencia Inicio fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfsaldocontrato_vigenciainicio_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFSALDOCONTRATO_VIGENCIAINICIO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV43TFSaldoContrato_VigenciaInicio_To)==0) || new gx.date.gxdate( this.AV43TFSaldoContrato_VigenciaInicio_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFSaldo Contrato_Vigencia Inicio_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_saldocontrato_vigenciainicioauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV44DDO_SaldoContrato_VigenciaInicioAuxDate)==0) || new gx.date.gxdate( this.AV44DDO_SaldoContrato_VigenciaInicioAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Saldo Contrato_Vigencia Inicio Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_saldocontrato_vigenciainicioauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo)==0) || new gx.date.gxdate( this.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Saldo Contrato_Vigencia Inicio Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfsaldocontrato_vigenciafim=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFSALDOCONTRATO_VIGENCIAFIM");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV48TFSaldoContrato_VigenciaFim)==0) || new gx.date.gxdate( this.AV48TFSaldoContrato_VigenciaFim ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFSaldo Contrato_Vigencia Fim fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfsaldocontrato_vigenciafim_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFSALDOCONTRATO_VIGENCIAFIM_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV49TFSaldoContrato_VigenciaFim_To)==0) || new gx.date.gxdate( this.AV49TFSaldoContrato_VigenciaFim_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFSaldo Contrato_Vigencia Fim_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_saldocontrato_vigenciafimauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV50DDO_SaldoContrato_VigenciaFimAuxDate)==0) || new gx.date.gxdate( this.AV50DDO_SaldoContrato_VigenciaFimAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Saldo Contrato_Vigencia Fim Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_saldocontrato_vigenciafimauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo)==0) || new gx.date.gxdate( this.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Saldo Contrato_Vigencia Fim Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "SALDOCONTRATO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible", false );
      if ( this.AV19DynamicFiltersSelector2 == "SALDOCONTRATO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible", false );
      if ( this.AV23DynamicFiltersSelector3 == "SALDOCONTRATO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV13OrderedBy == 2 )
      {
         this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 3 )
      {
         this.DDO_CONTRATO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 1 )
      {
         this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 4 )
      {
         this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 5 )
      {
         this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 6 )
      {
         this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 7 )
      {
         this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 8 )
      {
         this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_CONTRATO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV18DynamicFiltersEnabled2 =  false  ;
      this.AV19DynamicFiltersSelector2 =  "SALDOCONTRATO_VIGENCIAINICIO"  ;
      this.AV20SaldoContrato_VigenciaInicio2 =  ''  ;
      this.AV21SaldoContrato_VigenciaInicio_To2 =  ''  ;
      this.s122_client();
      this.AV22DynamicFiltersEnabled3 =  false  ;
      this.AV23DynamicFiltersSelector3 =  "SALDOCONTRATO_VIGENCIAINICIO"  ;
      this.AV24SaldoContrato_VigenciaInicio3 =  ''  ;
      this.AV25SaldoContrato_VigenciaInicio_To3 =  ''  ;
      this.s132_client();
   };
   this.e11m52_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13m52_client=function()
   {
      this.executeServerEvent("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e18m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e19m52_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e20m52_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e21m52_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e22m52_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e23m52_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e24m52_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e25m52_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e26m52_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e27m52_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e28m52_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e29m52_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e30m52_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e34m52_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e35m52_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,20,21,23,26,28,31,33,35,37,40,42,44,46,47,50,52,54,56,59,61,63,65,66,69,71,73,75,78,80,82,84,85,88,91,95,96,97,98,99,100,101,102,103,104,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,134,136,138,140,142,144,146,148];
   this.GXLastCtrlId =148;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",94,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwsaldocontrato",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",95,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",96,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(1561,97,"SALDOCONTRATO_CODIGO","","","SaldoContrato_Codigo","int",0,"px",6,6,"right",null,[],1561,"SaldoContrato_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(74,98,"CONTRATO_CODIGO","","","Contrato_Codigo","int",0,"px",6,6,"right",null,[],74,"Contrato_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1571,99,"SALDOCONTRATO_VIGENCIAINICIO","","","SaldoContrato_VigenciaInicio","date",0,"px",8,8,"right",null,[],1571,"SaldoContrato_VigenciaInicio",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1572,100,"SALDOCONTRATO_VIGENCIAFIM","","","SaldoContrato_VigenciaFim","date",0,"px",8,8,"right",null,[],1572,"SaldoContrato_VigenciaFim",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1573,101,"SALDOCONTRATO_CREDITO","","","SaldoContrato_Credito","decimal",0,"px",18,18,"right",null,[],1573,"SaldoContrato_Credito",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1574,102,"SALDOCONTRATO_RESERVADO","","","SaldoContrato_Reservado","decimal",0,"px",18,18,"right",null,[],1574,"SaldoContrato_Reservado",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1575,103,"SALDOCONTRATO_EXECUTADO","","","SaldoContrato_Executado","decimal",0,"px",18,18,"right",null,[],1575,"SaldoContrato_Executado",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1576,104,"SALDOCONTRATO_SALDO","","","SaldoContrato_Saldo","decimal",0,"px",18,18,"right",null,[],1576,"SaldoContrato_Saldo",true,5,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 108, 20, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_SALDOCONTRATO_CODIGOContainer = gx.uc.getNew(this, 133, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_CODIGOContainer", "Ddo_saldocontrato_codigo");
   var DDO_SALDOCONTRATO_CODIGOContainer = this.DDO_SALDOCONTRATO_CODIGOContainer;
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_CODIGOContainer.addV2CFunction('AV33SaldoContrato_CodigoTitleFilterData', "vSALDOCONTRATO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33SaldoContrato_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV33SaldoContrato_CodigoTitleFilterData); });
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e12m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_CODIGOContainer);
   this.DDO_CONTRATO_CODIGOContainer = gx.uc.getNew(this, 135, 20, "BootstrapDropDownOptions", "DDO_CONTRATO_CODIGOContainer", "Ddo_contrato_codigo");
   var DDO_CONTRATO_CODIGOContainer = this.DDO_CONTRATO_CODIGOContainer;
   DDO_CONTRATO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTRATO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTRATO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTRATO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTRATO_CODIGOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_CONTRATO_CODIGOContainer.addV2CFunction('AV37Contrato_CodigoTitleFilterData', "vCONTRATO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37Contrato_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTRATO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV37Contrato_CodigoTitleFilterData); });
   DDO_CONTRATO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTRATO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_CONTRATO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTRATO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e13m52_client);
   this.setUserControl(DDO_CONTRATO_CODIGOContainer);
   this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer = gx.uc.getNew(this, 137, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_VIGENCIAINICIOContainer", "Ddo_saldocontrato_vigenciainicio");
   var DDO_SALDOCONTRATO_VIGENCIAINICIOContainer = this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer;
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.addV2CFunction('AV41SaldoContrato_VigenciaInicioTitleFilterData', "vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV41SaldoContrato_VigenciaInicioTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA",UC.ParentObject.AV41SaldoContrato_VigenciaInicioTitleFilterData); });
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.addEventHandler("OnOptionClicked", this.e14m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_VIGENCIAINICIOContainer);
   this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer = gx.uc.getNew(this, 139, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_VIGENCIAFIMContainer", "Ddo_saldocontrato_vigenciafim");
   var DDO_SALDOCONTRATO_VIGENCIAFIMContainer = this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer;
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.addV2CFunction('AV47SaldoContrato_VigenciaFimTitleFilterData', "vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47SaldoContrato_VigenciaFimTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA",UC.ParentObject.AV47SaldoContrato_VigenciaFimTitleFilterData); });
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_VIGENCIAFIMContainer.addEventHandler("OnOptionClicked", this.e15m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_VIGENCIAFIMContainer);
   this.DDO_SALDOCONTRATO_CREDITOContainer = gx.uc.getNew(this, 141, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_CREDITOContainer", "Ddo_saldocontrato_credito");
   var DDO_SALDOCONTRATO_CREDITOContainer = this.DDO_SALDOCONTRATO_CREDITOContainer;
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_CREDITOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_CREDITOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_CREDITOContainer.addV2CFunction('AV53SaldoContrato_CreditoTitleFilterData', "vSALDOCONTRATO_CREDITOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_CREDITOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV53SaldoContrato_CreditoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_CREDITOTITLEFILTERDATA",UC.ParentObject.AV53SaldoContrato_CreditoTitleFilterData); });
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_CREDITOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_CREDITOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_CREDITOContainer.addEventHandler("OnOptionClicked", this.e16m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_CREDITOContainer);
   this.DDO_SALDOCONTRATO_RESERVADOContainer = gx.uc.getNew(this, 143, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_RESERVADOContainer", "Ddo_saldocontrato_reservado");
   var DDO_SALDOCONTRATO_RESERVADOContainer = this.DDO_SALDOCONTRATO_RESERVADOContainer;
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_RESERVADOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_RESERVADOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_RESERVADOContainer.addV2CFunction('AV57SaldoContrato_ReservadoTitleFilterData', "vSALDOCONTRATO_RESERVADOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_RESERVADOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV57SaldoContrato_ReservadoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_RESERVADOTITLEFILTERDATA",UC.ParentObject.AV57SaldoContrato_ReservadoTitleFilterData); });
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_RESERVADOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_RESERVADOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_RESERVADOContainer.addEventHandler("OnOptionClicked", this.e17m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_RESERVADOContainer);
   this.DDO_SALDOCONTRATO_EXECUTADOContainer = gx.uc.getNew(this, 145, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_EXECUTADOContainer", "Ddo_saldocontrato_executado");
   var DDO_SALDOCONTRATO_EXECUTADOContainer = this.DDO_SALDOCONTRATO_EXECUTADOContainer;
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_EXECUTADOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_EXECUTADOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_EXECUTADOContainer.addV2CFunction('AV61SaldoContrato_ExecutadoTitleFilterData', "vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_EXECUTADOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61SaldoContrato_ExecutadoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA",UC.ParentObject.AV61SaldoContrato_ExecutadoTitleFilterData); });
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_EXECUTADOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_EXECUTADOContainer.addEventHandler("OnOptionClicked", this.e18m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_EXECUTADOContainer);
   this.DDO_SALDOCONTRATO_SALDOContainer = gx.uc.getNew(this, 147, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_SALDOContainer", "Ddo_saldocontrato_saldo");
   var DDO_SALDOCONTRATO_SALDOContainer = this.DDO_SALDOCONTRATO_SALDOContainer;
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_SALDOContainer.addV2CFunction('AV69DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_SALDOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV69DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV69DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_SALDOContainer.addV2CFunction('AV65SaldoContrato_SaldoTitleFilterData', "vSALDOCONTRATO_SALDOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_SALDOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV65SaldoContrato_SaldoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_SALDOTITLEFILTERDATA",UC.ParentObject.AV65SaldoContrato_SaldoTitleFilterData); });
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_SALDOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_SALDOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_SALDOContainer.addEventHandler("OnOptionClicked", this.e19m52_client);
   this.setUserControl(DDO_SALDOCONTRATO_SALDOContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 107, 20, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV71GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV71GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV71GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV72GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV72GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV72GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11m52_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"SALDOCONTRATOTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[26]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[28]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[31]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[37]={fld:"TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1",grid:0};
   GXValidFnc[40]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio1,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO1",gxz:"ZV16SaldoContrato_VigenciaInicio1",gxold:"OV16SaldoContrato_VigenciaInicio1",gxvar:"AV16SaldoContrato_VigenciaInicio1",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[40],ip:[40],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV16SaldoContrato_VigenciaInicio1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16SaldoContrato_VigenciaInicio1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO1",gx.O.AV16SaldoContrato_VigenciaInicio1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16SaldoContrato_VigenciaInicio1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO1")},nac:gx.falseFn};
   GXValidFnc[42]={fld:"DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO_TO1",gxz:"ZV17SaldoContrato_VigenciaInicio_To1",gxold:"OV17SaldoContrato_VigenciaInicio_To1",gxvar:"AV17SaldoContrato_VigenciaInicio_To1",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[44],ip:[44],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17SaldoContrato_VigenciaInicio_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17SaldoContrato_VigenciaInicio_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO1",gx.O.AV17SaldoContrato_VigenciaInicio_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17SaldoContrato_VigenciaInicio_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO1")},nac:gx.falseFn};
   GXValidFnc[46]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[47]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[50]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[52]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV19DynamicFiltersSelector2",gxold:"OV19DynamicFiltersSelector2",gxvar:"AV19DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV19DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[54]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[56]={fld:"TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2",grid:0};
   GXValidFnc[59]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio2,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO2",gxz:"ZV20SaldoContrato_VigenciaInicio2",gxold:"OV20SaldoContrato_VigenciaInicio2",gxvar:"AV20SaldoContrato_VigenciaInicio2",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[59],ip:[59],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20SaldoContrato_VigenciaInicio2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20SaldoContrato_VigenciaInicio2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO2",gx.O.AV20SaldoContrato_VigenciaInicio2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20SaldoContrato_VigenciaInicio2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO2")},nac:gx.falseFn};
   GXValidFnc[61]={fld:"DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[63]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO_TO2",gxz:"ZV21SaldoContrato_VigenciaInicio_To2",gxold:"OV21SaldoContrato_VigenciaInicio_To2",gxvar:"AV21SaldoContrato_VigenciaInicio_To2",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[63],ip:[63],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21SaldoContrato_VigenciaInicio_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21SaldoContrato_VigenciaInicio_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO2",gx.O.AV21SaldoContrato_VigenciaInicio_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21SaldoContrato_VigenciaInicio_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO2")},nac:gx.falseFn};
   GXValidFnc[65]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[66]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[69]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[71]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV23DynamicFiltersSelector3",gxold:"OV23DynamicFiltersSelector3",gxvar:"AV23DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV23DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV23DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[73]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[75]={fld:"TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3",grid:0};
   GXValidFnc[78]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio3,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO3",gxz:"ZV24SaldoContrato_VigenciaInicio3",gxold:"OV24SaldoContrato_VigenciaInicio3",gxvar:"AV24SaldoContrato_VigenciaInicio3",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[78],ip:[78],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24SaldoContrato_VigenciaInicio3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24SaldoContrato_VigenciaInicio3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO3",gx.O.AV24SaldoContrato_VigenciaInicio3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24SaldoContrato_VigenciaInicio3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO3")},nac:gx.falseFn};
   GXValidFnc[80]={fld:"DYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[82]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Saldocontrato_vigenciainicio_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_VIGENCIAINICIO_TO3",gxz:"ZV25SaldoContrato_VigenciaInicio_To3",gxold:"OV25SaldoContrato_VigenciaInicio_To3",gxvar:"AV25SaldoContrato_VigenciaInicio_To3",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[82],ip:[82],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25SaldoContrato_VigenciaInicio_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25SaldoContrato_VigenciaInicio_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO3",gx.O.AV25SaldoContrato_VigenciaInicio_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25SaldoContrato_VigenciaInicio_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vSALDOCONTRATO_VIGENCIAINICIO_TO3")},nac:gx.falseFn};
   GXValidFnc[84]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[85]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[88]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[91]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[95]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV28Update",gxold:"OV28Update",gxvar:"AV28Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV28Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(94),gx.O.AV28Update,gx.O.AV75Update_GXI)},c2v:function(){gx.O.AV75Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV28Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(94))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(94))}, gxvar_GXI:'AV75Update_GXI',nac:gx.falseFn};
   GXValidFnc[96]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV29Delete",gxold:"OV29Delete",gxvar:"AV29Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV29Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(94),gx.O.AV29Delete,gx.O.AV76Delete_GXI)},c2v:function(){gx.O.AV76Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV29Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(94))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(94))}, gxvar_GXI:'AV76Delete_GXI',nac:gx.falseFn};
   GXValidFnc[97]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:this.Valid_Saldocontrato_codigo,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_CODIGO",gxz:"Z1561SaldoContrato_Codigo",gxold:"O1561SaldoContrato_Codigo",gxvar:"A1561SaldoContrato_Codigo",ucs:[],op:[98,99,100,101,102,103,104],ip:[98,99,100,101,102,103,104,97],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1561SaldoContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1561SaldoContrato_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SALDOCONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),gx.O.A1561SaldoContrato_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1561SaldoContrato_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SALDOCONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[98]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_CODIGO",gxz:"Z74Contrato_Codigo",gxold:"O74Contrato_Codigo",gxvar:"A74Contrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z74Contrato_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),gx.O.A74Contrato_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[99]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_VIGENCIAINICIO",gxz:"Z1571SaldoContrato_VigenciaInicio",gxold:"O1571SaldoContrato_VigenciaInicio",gxvar:"A1571SaldoContrato_VigenciaInicio",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1571SaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1571SaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("SALDOCONTRATO_VIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94),gx.O.A1571SaldoContrato_VigenciaInicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1571SaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("SALDOCONTRATO_VIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[100]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_VIGENCIAFIM",gxz:"Z1572SaldoContrato_VigenciaFim",gxold:"O1572SaldoContrato_VigenciaFim",gxvar:"A1572SaldoContrato_VigenciaFim",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1572SaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1572SaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("SALDOCONTRATO_VIGENCIAFIM",row || gx.fn.currentGridRowImpl(94),gx.O.A1572SaldoContrato_VigenciaFim,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1572SaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("SALDOCONTRATO_VIGENCIAFIM",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[101]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_CREDITO",gxz:"Z1573SaldoContrato_Credito",gxold:"O1573SaldoContrato_Credito",gxvar:"A1573SaldoContrato_Credito",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1573SaldoContrato_Credito=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1573SaldoContrato_Credito=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("SALDOCONTRATO_CREDITO",row || gx.fn.currentGridRowImpl(94),gx.O.A1573SaldoContrato_Credito,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1573SaldoContrato_Credito=this.val()},val:function(row){return gx.fn.getGridDecimalValue("SALDOCONTRATO_CREDITO",row || gx.fn.currentGridRowImpl(94),'.',',')},nac:gx.falseFn};
   GXValidFnc[102]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_RESERVADO",gxz:"Z1574SaldoContrato_Reservado",gxold:"O1574SaldoContrato_Reservado",gxvar:"A1574SaldoContrato_Reservado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1574SaldoContrato_Reservado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1574SaldoContrato_Reservado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("SALDOCONTRATO_RESERVADO",row || gx.fn.currentGridRowImpl(94),gx.O.A1574SaldoContrato_Reservado,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1574SaldoContrato_Reservado=this.val()},val:function(row){return gx.fn.getGridDecimalValue("SALDOCONTRATO_RESERVADO",row || gx.fn.currentGridRowImpl(94),'.',',')},nac:gx.falseFn};
   GXValidFnc[103]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_EXECUTADO",gxz:"Z1575SaldoContrato_Executado",gxold:"O1575SaldoContrato_Executado",gxvar:"A1575SaldoContrato_Executado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1575SaldoContrato_Executado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1575SaldoContrato_Executado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("SALDOCONTRATO_EXECUTADO",row || gx.fn.currentGridRowImpl(94),gx.O.A1575SaldoContrato_Executado,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1575SaldoContrato_Executado=this.val()},val:function(row){return gx.fn.getGridDecimalValue("SALDOCONTRATO_EXECUTADO",row || gx.fn.currentGridRowImpl(94),'.',',')},nac:gx.falseFn};
   GXValidFnc[104]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_SALDO",gxz:"Z1576SaldoContrato_Saldo",gxold:"O1576SaldoContrato_Saldo",gxvar:"A1576SaldoContrato_Saldo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1576SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1576SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("SALDOCONTRATO_SALDO",row || gx.fn.currentGridRowImpl(94),gx.O.A1576SaldoContrato_Saldo,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1576SaldoContrato_Saldo=this.val()},val:function(row){return gx.fn.getGridDecimalValue("SALDOCONTRATO_SALDO",row || gx.fn.currentGridRowImpl(94),'.',',')},nac:gx.falseFn};
   GXValidFnc[109]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV18DynamicFiltersEnabled2",gxold:"OV18DynamicFiltersEnabled2",gxvar:"AV18DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV18DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[110]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV22DynamicFiltersEnabled3",gxold:"OV22DynamicFiltersEnabled3",gxvar:"AV22DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV22DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[111]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CODIGO",gxz:"ZV34TFSaldoContrato_Codigo",gxold:"OV34TFSaldoContrato_Codigo",gxvar:"AV34TFSaldoContrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV34TFSaldoContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV34TFSaldoContrato_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_CODIGO",gx.O.AV34TFSaldoContrato_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV34TFSaldoContrato_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFSALDOCONTRATO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[112]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CODIGO_TO",gxz:"ZV35TFSaldoContrato_Codigo_To",gxold:"OV35TFSaldoContrato_Codigo_To",gxvar:"AV35TFSaldoContrato_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35TFSaldoContrato_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV35TFSaldoContrato_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_CODIGO_TO",gx.O.AV35TFSaldoContrato_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35TFSaldoContrato_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFSALDOCONTRATO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[113]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATO_CODIGO",gxz:"ZV38TFContrato_Codigo",gxold:"OV38TFContrato_Codigo",gxvar:"AV38TFContrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38TFContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38TFContrato_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTRATO_CODIGO",gx.O.AV38TFContrato_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38TFContrato_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTRATO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[114]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATO_CODIGO_TO",gxz:"ZV39TFContrato_Codigo_To",gxold:"OV39TFContrato_Codigo_To",gxvar:"AV39TFContrato_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39TFContrato_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV39TFContrato_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTRATO_CODIGO_TO",gx.O.AV39TFContrato_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39TFContrato_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTRATO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[115]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfsaldocontrato_vigenciainicio,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_VIGENCIAINICIO",gxz:"ZV42TFSaldoContrato_VigenciaInicio",gxold:"OV42TFSaldoContrato_VigenciaInicio",gxvar:"AV42TFSaldoContrato_VigenciaInicio",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[115],ip:[115],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42TFSaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV42TFSaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_VIGENCIAINICIO",gx.O.AV42TFSaldoContrato_VigenciaInicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42TFSaldoContrato_VigenciaInicio=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFSALDOCONTRATO_VIGENCIAINICIO")},nac:gx.falseFn};
   GXValidFnc[116]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfsaldocontrato_vigenciainicio_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_VIGENCIAINICIO_TO",gxz:"ZV43TFSaldoContrato_VigenciaInicio_To",gxold:"OV43TFSaldoContrato_VigenciaInicio_To",gxvar:"AV43TFSaldoContrato_VigenciaInicio_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[116],ip:[116],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43TFSaldoContrato_VigenciaInicio_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV43TFSaldoContrato_VigenciaInicio_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_VIGENCIAINICIO_TO",gx.O.AV43TFSaldoContrato_VigenciaInicio_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43TFSaldoContrato_VigenciaInicio_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFSALDOCONTRATO_VIGENCIAINICIO_TO")},nac:gx.falseFn};
   GXValidFnc[117]={fld:"DDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATES",grid:0};
   GXValidFnc[118]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_saldocontrato_vigenciainicioauxdate,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE",gxz:"ZV44DDO_SaldoContrato_VigenciaInicioAuxDate",gxold:"OV44DDO_SaldoContrato_VigenciaInicioAuxDate",gxvar:"AV44DDO_SaldoContrato_VigenciaInicioAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[118],ip:[118],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44DDO_SaldoContrato_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV44DDO_SaldoContrato_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE",gx.O.AV44DDO_SaldoContrato_VigenciaInicioAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44DDO_SaldoContrato_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[119]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_saldocontrato_vigenciainicioauxdateto,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO",gxz:"ZV45DDO_SaldoContrato_VigenciaInicioAuxDateTo",gxold:"OV45DDO_SaldoContrato_VigenciaInicioAuxDateTo",gxvar:"AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[119],ip:[119],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV45DDO_SaldoContrato_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO",gx.O.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[120]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfsaldocontrato_vigenciafim,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_VIGENCIAFIM",gxz:"ZV48TFSaldoContrato_VigenciaFim",gxold:"OV48TFSaldoContrato_VigenciaFim",gxvar:"AV48TFSaldoContrato_VigenciaFim",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[120],ip:[120],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV48TFSaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV48TFSaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_VIGENCIAFIM",gx.O.AV48TFSaldoContrato_VigenciaFim,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV48TFSaldoContrato_VigenciaFim=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFSALDOCONTRATO_VIGENCIAFIM")},nac:gx.falseFn};
   GXValidFnc[121]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfsaldocontrato_vigenciafim_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_VIGENCIAFIM_TO",gxz:"ZV49TFSaldoContrato_VigenciaFim_To",gxold:"OV49TFSaldoContrato_VigenciaFim_To",gxvar:"AV49TFSaldoContrato_VigenciaFim_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[121],ip:[121],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV49TFSaldoContrato_VigenciaFim_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV49TFSaldoContrato_VigenciaFim_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_VIGENCIAFIM_TO",gx.O.AV49TFSaldoContrato_VigenciaFim_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV49TFSaldoContrato_VigenciaFim_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFSALDOCONTRATO_VIGENCIAFIM_TO")},nac:gx.falseFn};
   GXValidFnc[122]={fld:"DDO_SALDOCONTRATO_VIGENCIAFIMAUXDATES",grid:0};
   GXValidFnc[123]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_saldocontrato_vigenciafimauxdate,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE",gxz:"ZV50DDO_SaldoContrato_VigenciaFimAuxDate",gxold:"OV50DDO_SaldoContrato_VigenciaFimAuxDate",gxvar:"AV50DDO_SaldoContrato_VigenciaFimAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[123],ip:[123],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV50DDO_SaldoContrato_VigenciaFimAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV50DDO_SaldoContrato_VigenciaFimAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE",gx.O.AV50DDO_SaldoContrato_VigenciaFimAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV50DDO_SaldoContrato_VigenciaFimAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATE")},nac:gx.falseFn};
   GXValidFnc[124]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_saldocontrato_vigenciafimauxdateto,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO",gxz:"ZV51DDO_SaldoContrato_VigenciaFimAuxDateTo",gxold:"OV51DDO_SaldoContrato_VigenciaFimAuxDateTo",gxvar:"AV51DDO_SaldoContrato_VigenciaFimAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[124],ip:[124],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV51DDO_SaldoContrato_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO",gx.O.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[125]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CREDITO",gxz:"ZV54TFSaldoContrato_Credito",gxold:"OV54TFSaldoContrato_Credito",gxvar:"AV54TFSaldoContrato_Credito",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV54TFSaldoContrato_Credito=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV54TFSaldoContrato_Credito=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_CREDITO",gx.O.AV54TFSaldoContrato_Credito,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV54TFSaldoContrato_Credito=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_CREDITO",'.',',')},nac:gx.falseFn};
   GXValidFnc[126]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CREDITO_TO",gxz:"ZV55TFSaldoContrato_Credito_To",gxold:"OV55TFSaldoContrato_Credito_To",gxvar:"AV55TFSaldoContrato_Credito_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV55TFSaldoContrato_Credito_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV55TFSaldoContrato_Credito_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_CREDITO_TO",gx.O.AV55TFSaldoContrato_Credito_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV55TFSaldoContrato_Credito_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_CREDITO_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[127]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_RESERVADO",gxz:"ZV58TFSaldoContrato_Reservado",gxold:"OV58TFSaldoContrato_Reservado",gxvar:"AV58TFSaldoContrato_Reservado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV58TFSaldoContrato_Reservado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV58TFSaldoContrato_Reservado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_RESERVADO",gx.O.AV58TFSaldoContrato_Reservado,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV58TFSaldoContrato_Reservado=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_RESERVADO",'.',',')},nac:gx.falseFn};
   GXValidFnc[128]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_RESERVADO_TO",gxz:"ZV59TFSaldoContrato_Reservado_To",gxold:"OV59TFSaldoContrato_Reservado_To",gxvar:"AV59TFSaldoContrato_Reservado_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV59TFSaldoContrato_Reservado_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV59TFSaldoContrato_Reservado_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_RESERVADO_TO",gx.O.AV59TFSaldoContrato_Reservado_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV59TFSaldoContrato_Reservado_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_RESERVADO_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[129]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_EXECUTADO",gxz:"ZV62TFSaldoContrato_Executado",gxold:"OV62TFSaldoContrato_Executado",gxvar:"AV62TFSaldoContrato_Executado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV62TFSaldoContrato_Executado=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV62TFSaldoContrato_Executado=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_EXECUTADO",gx.O.AV62TFSaldoContrato_Executado,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV62TFSaldoContrato_Executado=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_EXECUTADO",'.',',')},nac:gx.falseFn};
   GXValidFnc[130]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_EXECUTADO_TO",gxz:"ZV63TFSaldoContrato_Executado_To",gxold:"OV63TFSaldoContrato_Executado_To",gxvar:"AV63TFSaldoContrato_Executado_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV63TFSaldoContrato_Executado_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV63TFSaldoContrato_Executado_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_EXECUTADO_TO",gx.O.AV63TFSaldoContrato_Executado_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV63TFSaldoContrato_Executado_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_EXECUTADO_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[131]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_SALDO",gxz:"ZV66TFSaldoContrato_Saldo",gxold:"OV66TFSaldoContrato_Saldo",gxvar:"AV66TFSaldoContrato_Saldo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV66TFSaldoContrato_Saldo=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV66TFSaldoContrato_Saldo=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_SALDO",gx.O.AV66TFSaldoContrato_Saldo,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV66TFSaldoContrato_Saldo=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_SALDO",'.',',')},nac:gx.falseFn};
   GXValidFnc[132]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_SALDO_TO",gxz:"ZV67TFSaldoContrato_Saldo_To",gxold:"OV67TFSaldoContrato_Saldo_To",gxvar:"AV67TFSaldoContrato_Saldo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV67TFSaldoContrato_Saldo_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV67TFSaldoContrato_Saldo_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFSALDOCONTRATO_SALDO_TO",gx.O.AV67TFSaldoContrato_Saldo_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV67TFSaldoContrato_Saldo_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFSALDOCONTRATO_SALDO_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[134]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV36ddo_SaldoContrato_CodigoTitleControlIdToReplace",gxold:"OV36ddo_SaldoContrato_CodigoTitleControlIdToReplace",gxvar:"AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV36ddo_SaldoContrato_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[136]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV40ddo_Contrato_CodigoTitleControlIdToReplace",gxold:"OV40ddo_Contrato_CodigoTitleControlIdToReplace",gxvar:"AV40ddo_Contrato_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40ddo_Contrato_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[138]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE",gxz:"ZV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace",gxold:"OV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace",gxvar:"AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE",gx.O.AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[140]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE",gxz:"ZV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace",gxold:"OV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace",gxvar:"AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE",gx.O.AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[142]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE",gxz:"ZV56ddo_SaldoContrato_CreditoTitleControlIdToReplace",gxold:"OV56ddo_SaldoContrato_CreditoTitleControlIdToReplace",gxvar:"AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV56ddo_SaldoContrato_CreditoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE",gx.O.AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[144]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE",gxz:"ZV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace",gxold:"OV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace",gxvar:"AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE",gx.O.AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[146]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE",gxz:"ZV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace",gxold:"OV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace",gxvar:"AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE",gx.O.AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[148]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE",gxz:"ZV68ddo_SaldoContrato_SaldoTitleControlIdToReplace",gxold:"OV68ddo_SaldoContrato_SaldoTitleControlIdToReplace",gxvar:"AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV68ddo_SaldoContrato_SaldoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE",gx.O.AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16SaldoContrato_VigenciaInicio1 = gx.date.nullDate() ;
   this.ZV16SaldoContrato_VigenciaInicio1 = gx.date.nullDate() ;
   this.OV16SaldoContrato_VigenciaInicio1 = gx.date.nullDate() ;
   this.AV17SaldoContrato_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.ZV17SaldoContrato_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.OV17SaldoContrato_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.ZV19DynamicFiltersSelector2 = "" ;
   this.OV19DynamicFiltersSelector2 = "" ;
   this.AV20SaldoContrato_VigenciaInicio2 = gx.date.nullDate() ;
   this.ZV20SaldoContrato_VigenciaInicio2 = gx.date.nullDate() ;
   this.OV20SaldoContrato_VigenciaInicio2 = gx.date.nullDate() ;
   this.AV21SaldoContrato_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.ZV21SaldoContrato_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.OV21SaldoContrato_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.ZV23DynamicFiltersSelector3 = "" ;
   this.OV23DynamicFiltersSelector3 = "" ;
   this.AV24SaldoContrato_VigenciaInicio3 = gx.date.nullDate() ;
   this.ZV24SaldoContrato_VigenciaInicio3 = gx.date.nullDate() ;
   this.OV24SaldoContrato_VigenciaInicio3 = gx.date.nullDate() ;
   this.AV25SaldoContrato_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.ZV25SaldoContrato_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.OV25SaldoContrato_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.ZV28Update = "" ;
   this.OV28Update = "" ;
   this.ZV29Delete = "" ;
   this.OV29Delete = "" ;
   this.Z1561SaldoContrato_Codigo = 0 ;
   this.O1561SaldoContrato_Codigo = 0 ;
   this.Z74Contrato_Codigo = 0 ;
   this.O74Contrato_Codigo = 0 ;
   this.Z1571SaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.O1571SaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.Z1572SaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.O1572SaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.Z1573SaldoContrato_Credito = 0 ;
   this.O1573SaldoContrato_Credito = 0 ;
   this.Z1574SaldoContrato_Reservado = 0 ;
   this.O1574SaldoContrato_Reservado = 0 ;
   this.Z1575SaldoContrato_Executado = 0 ;
   this.O1575SaldoContrato_Executado = 0 ;
   this.Z1576SaldoContrato_Saldo = 0 ;
   this.O1576SaldoContrato_Saldo = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.ZV18DynamicFiltersEnabled2 = false ;
   this.OV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.ZV22DynamicFiltersEnabled3 = false ;
   this.OV22DynamicFiltersEnabled3 = false ;
   this.AV34TFSaldoContrato_Codigo = 0 ;
   this.ZV34TFSaldoContrato_Codigo = 0 ;
   this.OV34TFSaldoContrato_Codigo = 0 ;
   this.AV35TFSaldoContrato_Codigo_To = 0 ;
   this.ZV35TFSaldoContrato_Codigo_To = 0 ;
   this.OV35TFSaldoContrato_Codigo_To = 0 ;
   this.AV38TFContrato_Codigo = 0 ;
   this.ZV38TFContrato_Codigo = 0 ;
   this.OV38TFContrato_Codigo = 0 ;
   this.AV39TFContrato_Codigo_To = 0 ;
   this.ZV39TFContrato_Codigo_To = 0 ;
   this.OV39TFContrato_Codigo_To = 0 ;
   this.AV42TFSaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.ZV42TFSaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.OV42TFSaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.AV43TFSaldoContrato_VigenciaInicio_To = gx.date.nullDate() ;
   this.ZV43TFSaldoContrato_VigenciaInicio_To = gx.date.nullDate() ;
   this.OV43TFSaldoContrato_VigenciaInicio_To = gx.date.nullDate() ;
   this.AV44DDO_SaldoContrato_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.ZV44DDO_SaldoContrato_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.OV44DDO_SaldoContrato_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.ZV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.OV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.AV48TFSaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.ZV48TFSaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.OV48TFSaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.AV49TFSaldoContrato_VigenciaFim_To = gx.date.nullDate() ;
   this.ZV49TFSaldoContrato_VigenciaFim_To = gx.date.nullDate() ;
   this.OV49TFSaldoContrato_VigenciaFim_To = gx.date.nullDate() ;
   this.AV50DDO_SaldoContrato_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.ZV50DDO_SaldoContrato_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.OV50DDO_SaldoContrato_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.ZV51DDO_SaldoContrato_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.OV51DDO_SaldoContrato_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.AV54TFSaldoContrato_Credito = 0 ;
   this.ZV54TFSaldoContrato_Credito = 0 ;
   this.OV54TFSaldoContrato_Credito = 0 ;
   this.AV55TFSaldoContrato_Credito_To = 0 ;
   this.ZV55TFSaldoContrato_Credito_To = 0 ;
   this.OV55TFSaldoContrato_Credito_To = 0 ;
   this.AV58TFSaldoContrato_Reservado = 0 ;
   this.ZV58TFSaldoContrato_Reservado = 0 ;
   this.OV58TFSaldoContrato_Reservado = 0 ;
   this.AV59TFSaldoContrato_Reservado_To = 0 ;
   this.ZV59TFSaldoContrato_Reservado_To = 0 ;
   this.OV59TFSaldoContrato_Reservado_To = 0 ;
   this.AV62TFSaldoContrato_Executado = 0 ;
   this.ZV62TFSaldoContrato_Executado = 0 ;
   this.OV62TFSaldoContrato_Executado = 0 ;
   this.AV63TFSaldoContrato_Executado_To = 0 ;
   this.ZV63TFSaldoContrato_Executado_To = 0 ;
   this.OV63TFSaldoContrato_Executado_To = 0 ;
   this.AV66TFSaldoContrato_Saldo = 0 ;
   this.ZV66TFSaldoContrato_Saldo = 0 ;
   this.OV66TFSaldoContrato_Saldo = 0 ;
   this.AV67TFSaldoContrato_Saldo_To = 0 ;
   this.ZV67TFSaldoContrato_Saldo_To = 0 ;
   this.OV67TFSaldoContrato_Saldo_To = 0 ;
   this.AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.ZV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.OV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.ZV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.OV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "" ;
   this.ZV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "" ;
   this.OV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "" ;
   this.AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "" ;
   this.ZV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "" ;
   this.OV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "" ;
   this.AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = "" ;
   this.ZV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = "" ;
   this.OV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = "" ;
   this.AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "" ;
   this.ZV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "" ;
   this.OV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "" ;
   this.AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "" ;
   this.ZV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "" ;
   this.OV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "" ;
   this.AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = "" ;
   this.ZV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = "" ;
   this.OV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = "" ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16SaldoContrato_VigenciaInicio1 = gx.date.nullDate() ;
   this.AV17SaldoContrato_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.AV20SaldoContrato_VigenciaInicio2 = gx.date.nullDate() ;
   this.AV21SaldoContrato_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.AV24SaldoContrato_VigenciaInicio3 = gx.date.nullDate() ;
   this.AV25SaldoContrato_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.AV71GridCurrentPage = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.AV34TFSaldoContrato_Codigo = 0 ;
   this.AV35TFSaldoContrato_Codigo_To = 0 ;
   this.AV38TFContrato_Codigo = 0 ;
   this.AV39TFContrato_Codigo_To = 0 ;
   this.AV42TFSaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.AV43TFSaldoContrato_VigenciaInicio_To = gx.date.nullDate() ;
   this.AV44DDO_SaldoContrato_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.AV45DDO_SaldoContrato_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.AV48TFSaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.AV49TFSaldoContrato_VigenciaFim_To = gx.date.nullDate() ;
   this.AV50DDO_SaldoContrato_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.AV51DDO_SaldoContrato_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.AV54TFSaldoContrato_Credito = 0 ;
   this.AV55TFSaldoContrato_Credito_To = 0 ;
   this.AV58TFSaldoContrato_Reservado = 0 ;
   this.AV59TFSaldoContrato_Reservado_To = 0 ;
   this.AV62TFSaldoContrato_Executado = 0 ;
   this.AV63TFSaldoContrato_Executado_To = 0 ;
   this.AV66TFSaldoContrato_Saldo = 0 ;
   this.AV67TFSaldoContrato_Saldo_To = 0 ;
   this.AV69DDO_TitleSettingsIcons = {} ;
   this.AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace = "" ;
   this.AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace = "" ;
   this.AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace = "" ;
   this.AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace = "" ;
   this.AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace = "" ;
   this.AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace = "" ;
   this.A1560NotaEmpenho_Codigo = 0 ;
   this.AV28Update = "" ;
   this.AV29Delete = "" ;
   this.A1561SaldoContrato_Codigo = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.A1571SaldoContrato_VigenciaInicio = gx.date.nullDate() ;
   this.A1572SaldoContrato_VigenciaFim = gx.date.nullDate() ;
   this.A1573SaldoContrato_Credito = 0 ;
   this.A1574SaldoContrato_Reservado = 0 ;
   this.A1575SaldoContrato_Executado = 0 ;
   this.A1576SaldoContrato_Saldo = 0 ;
   this.AV77Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV27DynamicFiltersIgnoreFirst = false ;
   this.AV26DynamicFiltersRemoving = false ;
   this.Events = {"e11m52_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12m52_client": ["DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED", true] ,"e13m52_client": ["DDO_CONTRATO_CODIGO.ONOPTIONCLICKED", true] ,"e14m52_client": ["DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED", true] ,"e15m52_client": ["DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED", true] ,"e16m52_client": ["DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED", true] ,"e17m52_client": ["DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED", true] ,"e18m52_client": ["DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED", true] ,"e19m52_client": ["DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED", true] ,"e20m52_client": ["VORDEREDBY.CLICK", true] ,"e21m52_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e22m52_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e23m52_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e24m52_client": ["'DOCLEANFILTERS'", true] ,"e25m52_client": ["'DOINSERT'", true] ,"e26m52_client": ["'ADDDYNAMICFILTERS1'", true] ,"e27m52_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e28m52_client": ["'ADDDYNAMICFILTERS2'", true] ,"e29m52_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e30m52_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e34m52_client": ["ENTER", true] ,"e35m52_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''}],[{av:'AV33SaldoContrato_CodigoTitleFilterData',fld:'vSALDOCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41SaldoContrato_VigenciaInicioTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47SaldoContrato_VigenciaFimTitleFilterData',fld:'vSALDOCONTRATO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53SaldoContrato_CreditoTitleFilterData',fld:'vSALDOCONTRATO_CREDITOTITLEFILTERDATA',pic:'',nv:null},{av:'AV57SaldoContrato_ReservadoTitleFilterData',fld:'vSALDOCONTRATO_RESERVADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV61SaldoContrato_ExecutadoTitleFilterData',fld:'vSALDOCONTRATO_EXECUTADOTITLEFILTERDATA',pic:'',nv:null},{av:'AV65SaldoContrato_SaldoTitleFilterData',fld:'vSALDOCONTRATO_SALDOTITLEFILTERDATA',pic:'',nv:null},{ctrl:'SALDOCONTRATO_CODIGO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_CODIGO","Title")',ctrl:'SALDOCONTRATO_CODIGO',prop:'Title'},{ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATO_CODIGO","Title")',ctrl:'CONTRATO_CODIGO',prop:'Title'},{ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_VIGENCIAINICIO","Title")',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Title'},{ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_VIGENCIAFIM","Title")',ctrl:'SALDOCONTRATO_VIGENCIAFIM',prop:'Title'},{ctrl:'SALDOCONTRATO_CREDITO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_CREDITO","Title")',ctrl:'SALDOCONTRATO_CREDITO',prop:'Title'},{ctrl:'SALDOCONTRATO_RESERVADO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_RESERVADO","Title")',ctrl:'SALDOCONTRATO_RESERVADO',prop:'Title'},{ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_EXECUTADO","Title")',ctrl:'SALDOCONTRATO_EXECUTADO',prop:'Title'},{ctrl:'SALDOCONTRATO_SALDO',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_SALDO","Title")',ctrl:'SALDOCONTRATO_SALDO',prop:'Title'},{av:'AV71GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV72GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_CONTRATO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredText_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_VIGENCIAINICIO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_VIGENCIAFIM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_CREDITO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_RESERVADO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_EXECUTADO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_SALDO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'SortedStatus'},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_CODIGO","Link")',ctrl:'SALDOCONTRATO_CODIGO',prop:'Link'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_VIGENCIAINICIO","Link")',ctrl:'SALDOCONTRATO_VIGENCIAINICIO',prop:'Link'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV36ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_SaldoContrato_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_SaldoContrato_VigenciaFimTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_SaldoContrato_CreditoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CREDITOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_SaldoContrato_ReservadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_RESERVADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_SaldoContrato_ExecutadoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_EXECUTADOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_SaldoContrato_SaldoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_SALDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV34TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredText_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFSaldoContrato_VigenciaInicio',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredText_set'},{av:'AV43TFSaldoContrato_VigenciaInicio_To',fld:'vTFSALDOCONTRATO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_VIGENCIAINICIOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAINICIO',prop:'FilteredTextTo_set'},{av:'AV48TFSaldoContrato_VigenciaFim',fld:'vTFSALDOCONTRATO_VIGENCIAFIM',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredText_set'},{av:'AV49TFSaldoContrato_VigenciaFim_To',fld:'vTFSALDOCONTRATO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'this.DDO_SALDOCONTRATO_VIGENCIAFIMContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_VIGENCIAFIM',prop:'FilteredTextTo_set'},{av:'AV54TFSaldoContrato_Credito',fld:'vTFSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredText_set'},{av:'AV55TFSaldoContrato_Credito_To',fld:'vTFSALDOCONTRATO_CREDITO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_CREDITOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_CREDITO',prop:'FilteredTextTo_set'},{av:'AV58TFSaldoContrato_Reservado',fld:'vTFSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredText_set'},{av:'AV59TFSaldoContrato_Reservado_To',fld:'vTFSALDOCONTRATO_RESERVADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_RESERVADOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_RESERVADO',prop:'FilteredTextTo_set'},{av:'AV62TFSaldoContrato_Executado',fld:'vTFSALDOCONTRATO_EXECUTADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredText_set'},{av:'AV63TFSaldoContrato_Executado_To',fld:'vTFSALDOCONTRATO_EXECUTADO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_EXECUTADOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_EXECUTADO',prop:'FilteredTextTo_set'},{av:'AV66TFSaldoContrato_Saldo',fld:'vTFSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredText_set'},{av:'AV67TFSaldoContrato_Saldo_To',fld:'vTFSALDOCONTRATO_SALDO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_SALDOCONTRATO_SALDOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_SALDO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SaldoContrato_VigenciaInicio1',fld:'vSALDOCONTRATO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17SaldoContrato_VigenciaInicio_To1',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SaldoContrato_VigenciaInicio2',fld:'vSALDOCONTRATO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21SaldoContrato_VigenciaInicio_To2',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SaldoContrato_VigenciaInicio3',fld:'vSALDOCONTRATO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25SaldoContrato_VigenciaInicio_To3',fld:'vSALDOCONTRATO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSSALDOCONTRATO_VIGENCIAINICIO3',prop:'Visible'}]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.setVCMap("AV77Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A1560NotaEmpenho_Codigo", "NOTAEMPENHO_CODIGO", 0, "int");
   this.setVCMap("AV77Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A1560NotaEmpenho_Codigo", "NOTAEMPENHO_CODIGO", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[20]);
   GridContainer.addRefreshingVar(this.GXValidFnc[21]);
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[44]);
   GridContainer.addRefreshingVar(this.GXValidFnc[52]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[63]);
   GridContainer.addRefreshingVar(this.GXValidFnc[71]);
   GridContainer.addRefreshingVar(this.GXValidFnc[78]);
   GridContainer.addRefreshingVar(this.GXValidFnc[82]);
   GridContainer.addRefreshingVar(this.GXValidFnc[109]);
   GridContainer.addRefreshingVar(this.GXValidFnc[110]);
   GridContainer.addRefreshingVar(this.GXValidFnc[111]);
   GridContainer.addRefreshingVar(this.GXValidFnc[112]);
   GridContainer.addRefreshingVar(this.GXValidFnc[113]);
   GridContainer.addRefreshingVar(this.GXValidFnc[114]);
   GridContainer.addRefreshingVar(this.GXValidFnc[115]);
   GridContainer.addRefreshingVar(this.GXValidFnc[116]);
   GridContainer.addRefreshingVar(this.GXValidFnc[120]);
   GridContainer.addRefreshingVar(this.GXValidFnc[121]);
   GridContainer.addRefreshingVar(this.GXValidFnc[125]);
   GridContainer.addRefreshingVar(this.GXValidFnc[126]);
   GridContainer.addRefreshingVar(this.GXValidFnc[127]);
   GridContainer.addRefreshingVar(this.GXValidFnc[128]);
   GridContainer.addRefreshingVar(this.GXValidFnc[129]);
   GridContainer.addRefreshingVar(this.GXValidFnc[130]);
   GridContainer.addRefreshingVar(this.GXValidFnc[131]);
   GridContainer.addRefreshingVar(this.GXValidFnc[132]);
   GridContainer.addRefreshingVar(this.GXValidFnc[134]);
   GridContainer.addRefreshingVar(this.GXValidFnc[136]);
   GridContainer.addRefreshingVar(this.GXValidFnc[138]);
   GridContainer.addRefreshingVar(this.GXValidFnc[140]);
   GridContainer.addRefreshingVar(this.GXValidFnc[142]);
   GridContainer.addRefreshingVar(this.GXValidFnc[144]);
   GridContainer.addRefreshingVar(this.GXValidFnc[146]);
   GridContainer.addRefreshingVar(this.GXValidFnc[148]);
   GridContainer.addRefreshingVar({rfrVar:"AV77Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV27DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV26DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A1561SaldoContrato_Codigo", rfrProp:"Value", gxAttId:"1561"});
   GridContainer.addRefreshingVar({rfrVar:"A1560NotaEmpenho_Codigo"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwsaldocontrato);
