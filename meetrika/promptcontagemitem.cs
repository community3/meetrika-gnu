/*
               File: PromptContagemItem
        Description: Selecione Contagem Item
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:34:36.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemitem : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagemItem_Lancamento ,
                           ref int aP1_InOutContagem_FabricaSoftwareCod ,
                           ref short aP2_InOutContagemItem_Sequencial )
      {
         this.AV7InOutContagemItem_Lancamento = aP0_InOutContagemItem_Lancamento;
         this.AV172InOutContagem_FabricaSoftwareCod = aP1_InOutContagem_FabricaSoftwareCod;
         this.AV8InOutContagemItem_Sequencial = aP2_InOutContagemItem_Sequencial;
         executePrivate();
         aP0_InOutContagemItem_Lancamento=this.AV7InOutContagemItem_Lancamento;
         aP1_InOutContagem_FabricaSoftwareCod=this.AV172InOutContagem_FabricaSoftwareCod;
         aP2_InOutContagemItem_Sequencial=this.AV8InOutContagemItem_Sequencial;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbContagemItem_TipoUnidade = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_116 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_116_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_116_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV173Contagem_FabricaSoftwareCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0)));
               AV17ContagemItem_Sequencial1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemItem_Sequencial1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemItem_Sequencial1), 3, 0)));
               AV54Contagem_DataCriacao1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Contagem_DataCriacao1", context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"));
               AV55Contagem_DataCriacao_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Contagem_DataCriacao_To1", context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"));
               AV18FuncaoAPF_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
               AV56ContagemItem_FSReferenciaTecnicaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
               AV57ContagemItem_FMReferenciaTecnicaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV174Contagem_FabricaSoftwareCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0)));
               AV22ContagemItem_Sequencial2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemItem_Sequencial2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemItem_Sequencial2), 3, 0)));
               AV58Contagem_DataCriacao2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Contagem_DataCriacao2", context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"));
               AV59Contagem_DataCriacao_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contagem_DataCriacao_To2", context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"));
               AV23FuncaoAPF_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
               AV60ContagemItem_FSReferenciaTecnicaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
               AV61ContagemItem_FMReferenciaTecnicaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV175Contagem_FabricaSoftwareCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0)));
               AV27ContagemItem_Sequencial3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemItem_Sequencial3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContagemItem_Sequencial3), 3, 0)));
               AV62Contagem_DataCriacao3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Contagem_DataCriacao3", context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"));
               AV63Contagem_DataCriacao_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Contagem_DataCriacao_To3", context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"));
               AV28FuncaoAPF_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
               AV64ContagemItem_FSReferenciaTecnicaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
               AV65ContagemItem_FMReferenciaTecnicaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagemItem_Lancamento = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemItem_Lancamento), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV172InOutContagem_FabricaSoftwareCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172InOutContagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172InOutContagem_FabricaSoftwareCod), 6, 0)));
                  AV8InOutContagemItem_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemItem_Sequencial), 3, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA542( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS542( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE542( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117343748");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemitem.aspx") + "?" + UrlEncode("" +AV7InOutContagemItem_Lancamento) + "," + UrlEncode("" +AV172InOutContagem_FabricaSoftwareCod) + "," + UrlEncode("" +AV8InOutContagemItem_Sequencial)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_FABRICASOFTWARECOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_SEQUENCIAL1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContagemItem_Sequencial1), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO1", context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO_TO1", context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME1", AV18FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1", StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1", StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_FABRICASOFTWARECOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_SEQUENCIAL2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ContagemItem_Sequencial2), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO2", context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO_TO2", context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME2", AV23FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2", StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2", StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_FABRICASOFTWARECOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_SEQUENCIAL3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27ContagemItem_Sequencial3), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO3", context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_DATACRIACAO_TO3", context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME3", AV28FuncaoAPF_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3", StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3", StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_116", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_116), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV170GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV171GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEM_FABRICASOFTWARECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV172InOutContagem_FabricaSoftwareCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMITEM_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContagemItem_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm542( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemItem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contagem Item" ;
      }

      protected void WB540( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_542( true) ;
         }
         else
         {
            wb_table1_2_542( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_542e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(147, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(148, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"");
         }
         wbLoad = true;
      }

      protected void START542( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contagem Item", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP540( ) ;
      }

      protected void WS542( )
      {
         START542( ) ;
         EVT542( ) ;
      }

      protected void EVT542( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11542 */
                           E11542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12542 */
                           E12542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13542 */
                           E13542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14542 */
                           E14542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15542 */
                           E15542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16542 */
                           E16542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17542 */
                           E17542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18542 */
                           E18542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19542 */
                           E19542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20542 */
                           E20542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21542 */
                           E21542 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_116_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1162( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV178Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
                           A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                           A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
                           A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
                           A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
                           n194Contagem_AreaTrabalhoDes = false;
                           cmbContagem_Tecnica.Name = cmbContagem_Tecnica_Internalname;
                           cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
                           A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
                           n195Contagem_Tecnica = false;
                           cmbContagem_Tipo.Name = cmbContagem_Tipo_Internalname;
                           cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
                           A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
                           n196Contagem_Tipo = false;
                           A207Contagem_FabricaSoftwareCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", "."));
                           A208Contagem_FabricaSoftwarePessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", "."));
                           A209Contagem_FabricaSoftwarePessoaNom = StringUtil.Upper( cgiGet( edtContagem_FabricaSoftwarePessoaNom_Internalname));
                           A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
                           n213Contagem_UsuarioContadorCod = false;
                           A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
                           n214Contagem_UsuarioContadorPessoaCod = false;
                           A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
                           n215Contagem_UsuarioContadorPessoaNom = false;
                           A216Contagem_UsuarioAuditorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", "."));
                           A217Contagem_UsuarioAuditorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", "."));
                           A218Contagem_UsuarioAuditorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioAuditorPessoaNom_Internalname));
                           A225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Sequencial_Internalname), ",", "."));
                           n225ContagemItem_Sequencial = false;
                           A229ContagemItem_Requisito = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", "."));
                           n229ContagemItem_Requisito = false;
                           A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                           n165FuncaoAPF_Codigo = false;
                           A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
                           n950ContagemItem_PFB = false;
                           A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
                           n951ContagemItem_PFL = false;
                           A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                           cmbFuncaoAPF_Tipo.Name = cmbFuncaoAPF_Tipo_Internalname;
                           cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                           A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                           cmbContagemItem_TipoUnidade.Name = cmbContagemItem_TipoUnidade_Internalname;
                           cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                           A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                           A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
                           n953ContagemItem_Evidencias = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22542 */
                                 E22542 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23542 */
                                 E23542 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24542 */
                                 E24542 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_fabricasoftwarecod1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD1"), ",", ".") != Convert.ToDecimal( AV173Contagem_FabricaSoftwareCod1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_sequencial1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL1"), ",", ".") != Convert.ToDecimal( AV17ContagemItem_Sequencial1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO1"), 0) != AV54Contagem_DataCriacao1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO1"), 0) != AV55Contagem_DataCriacao_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV18FuncaoAPF_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fsreferenciatecnicanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1"), AV56ContagemItem_FSReferenciaTecnicaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fmreferenciatecnicanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1"), AV57ContagemItem_FMReferenciaTecnicaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_fabricasoftwarecod2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD2"), ",", ".") != Convert.ToDecimal( AV174Contagem_FabricaSoftwareCod2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_sequencial2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL2"), ",", ".") != Convert.ToDecimal( AV22ContagemItem_Sequencial2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO2"), 0) != AV58Contagem_DataCriacao2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO2"), 0) != AV59Contagem_DataCriacao_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV23FuncaoAPF_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fsreferenciatecnicanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2"), AV60ContagemItem_FSReferenciaTecnicaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fmreferenciatecnicanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2"), AV61ContagemItem_FMReferenciaTecnicaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_fabricasoftwarecod3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD3"), ",", ".") != Convert.ToDecimal( AV175Contagem_FabricaSoftwareCod3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_sequencial3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL3"), ",", ".") != Convert.ToDecimal( AV27ContagemItem_Sequencial3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO3"), 0) != AV62Contagem_DataCriacao3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_datacriacao_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO3"), 0) != AV63Contagem_DataCriacao_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV28FuncaoAPF_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fsreferenciatecnicanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3"), AV64ContagemItem_FSReferenciaTecnicaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemitem_fmreferenciatecnicanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3"), AV65ContagemItem_FMReferenciaTecnicaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25542 */
                                       E25542 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE542( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm542( ) ;
            }
         }
      }

      protected void PA542( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEM_SEQUENCIAL", "Sequencial", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_DATACRIACAO", "Data Cria��o", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEM_SEQUENCIAL", "Sequencial", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_DATACRIACAO", "Data Cria��o", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEM_SEQUENCIAL", "Sequencial", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_DATACRIACAO", "Data Cria��o", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTAGEM_TECNICA_" + sGXsfl_116_idx;
            cmbContagem_Tecnica.Name = GXCCtl;
            cmbContagem_Tecnica.WebTags = "";
            cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
            cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
            cmbContagem_Tecnica.addItem("2", "Estimada", 0);
            cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
            if ( cmbContagem_Tecnica.ItemCount > 0 )
            {
               A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
               n195Contagem_Tecnica = false;
            }
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_116_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_116_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_116_idx;
            cmbContagemItem_TipoUnidade.Name = GXCCtl;
            cmbContagemItem_TipoUnidade.WebTags = "";
            if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
            {
               A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1162( ) ;
         while ( nGXsfl_116_idx <= nRC_GXsfl_116 )
         {
            sendrow_1162( ) ;
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       int AV173Contagem_FabricaSoftwareCod1 ,
                                       short AV17ContagemItem_Sequencial1 ,
                                       DateTime AV54Contagem_DataCriacao1 ,
                                       DateTime AV55Contagem_DataCriacao_To1 ,
                                       String AV18FuncaoAPF_Nome1 ,
                                       String AV56ContagemItem_FSReferenciaTecnicaNom1 ,
                                       String AV57ContagemItem_FMReferenciaTecnicaNom1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       int AV174Contagem_FabricaSoftwareCod2 ,
                                       short AV22ContagemItem_Sequencial2 ,
                                       DateTime AV58Contagem_DataCriacao2 ,
                                       DateTime AV59Contagem_DataCriacao_To2 ,
                                       String AV23FuncaoAPF_Nome2 ,
                                       String AV60ContagemItem_FSReferenciaTecnicaNom2 ,
                                       String AV61ContagemItem_FMReferenciaTecnicaNom2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       int AV175Contagem_FabricaSoftwareCod3 ,
                                       short AV27ContagemItem_Sequencial3 ,
                                       DateTime AV62Contagem_DataCriacao3 ,
                                       DateTime AV63Contagem_DataCriacao_To3 ,
                                       String AV28FuncaoAPF_Nome3 ,
                                       String AV64ContagemItem_FSReferenciaTecnicaNom3 ,
                                       String AV65ContagemItem_FMReferenciaTecnicaNom3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF542( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_LANCAMENTO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWARECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FABRICASOFTWARECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FABRICASOFTWAREPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FABRICASOFTWAREPESSOANOM", StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_USUARIOAUDITORCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_USUARIOAUDITORPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_USUARIOAUDITORPESSOANOM", StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_REQUISITO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_REQUISITO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( "", context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFB", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( "", context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFL", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_TIPOUNIDADE", A952ContagemItem_TipoUnidade);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( "", A953ContagemItem_Evidencias));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_EVIDENCIAS", A953ContagemItem_Evidencias);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF542( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF542( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 116;
         /* Execute user event: E23542 */
         E23542 ();
         nGXsfl_116_idx = 1;
         sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
         SubsflControlProps_1162( ) ;
         nGXsfl_116_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1162( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV173Contagem_FabricaSoftwareCod1 ,
                                                 AV17ContagemItem_Sequencial1 ,
                                                 AV54Contagem_DataCriacao1 ,
                                                 AV55Contagem_DataCriacao_To1 ,
                                                 AV18FuncaoAPF_Nome1 ,
                                                 AV56ContagemItem_FSReferenciaTecnicaNom1 ,
                                                 AV57ContagemItem_FMReferenciaTecnicaNom1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV174Contagem_FabricaSoftwareCod2 ,
                                                 AV22ContagemItem_Sequencial2 ,
                                                 AV58Contagem_DataCriacao2 ,
                                                 AV59Contagem_DataCriacao_To2 ,
                                                 AV23FuncaoAPF_Nome2 ,
                                                 AV60ContagemItem_FSReferenciaTecnicaNom2 ,
                                                 AV61ContagemItem_FMReferenciaTecnicaNom2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV175Contagem_FabricaSoftwareCod3 ,
                                                 AV27ContagemItem_Sequencial3 ,
                                                 AV62Contagem_DataCriacao3 ,
                                                 AV63Contagem_DataCriacao_To3 ,
                                                 AV28FuncaoAPF_Nome3 ,
                                                 AV64ContagemItem_FSReferenciaTecnicaNom3 ,
                                                 AV65ContagemItem_FMReferenciaTecnicaNom3 ,
                                                 A207Contagem_FabricaSoftwareCod ,
                                                 A225ContagemItem_Sequencial ,
                                                 A197Contagem_DataCriacao ,
                                                 A166FuncaoAPF_Nome ,
                                                 A269ContagemItem_FSReferenciaTecnicaNom ,
                                                 A257ContagemItem_FMReferenciaTecnicaNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            lV56ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
            lV56ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
            lV57ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
            lV57ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
            lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            lV60ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
            lV60ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
            lV61ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
            lV61ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
            lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            lV64ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
            lV64ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
            lV65ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
            lV65ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
            /* Using cursor H00542 */
            pr_default.execute(0, new Object[] {AV173Contagem_FabricaSoftwareCod1, AV173Contagem_FabricaSoftwareCod1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV17ContagemItem_Sequencial1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, lV18FuncaoAPF_Nome1, lV18FuncaoAPF_Nome1, lV56ContagemItem_FSReferenciaTecnicaNom1, lV56ContagemItem_FSReferenciaTecnicaNom1, lV57ContagemItem_FMReferenciaTecnicaNom1, lV57ContagemItem_FMReferenciaTecnicaNom1, AV174Contagem_FabricaSoftwareCod2, AV174Contagem_FabricaSoftwareCod2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV22ContagemItem_Sequencial2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, lV23FuncaoAPF_Nome2, lV23FuncaoAPF_Nome2, lV60ContagemItem_FSReferenciaTecnicaNom2, lV60ContagemItem_FSReferenciaTecnicaNom2, lV61ContagemItem_FMReferenciaTecnicaNom2, lV61ContagemItem_FMReferenciaTecnicaNom2, AV175Contagem_FabricaSoftwareCod3, AV175Contagem_FabricaSoftwareCod3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV27ContagemItem_Sequencial3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, lV28FuncaoAPF_Nome3, lV28FuncaoAPF_Nome3, lV64ContagemItem_FSReferenciaTecnicaNom3, lV64ContagemItem_FSReferenciaTecnicaNom3, lV65ContagemItem_FMReferenciaTecnicaNom3, lV65ContagemItem_FMReferenciaTecnicaNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_116_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A226ContagemItem_FSReferenciaTecnicaCod = H00542_A226ContagemItem_FSReferenciaTecnicaCod[0];
               n226ContagemItem_FSReferenciaTecnicaCod = H00542_n226ContagemItem_FSReferenciaTecnicaCod[0];
               A255ContagemItem_FMReferenciaTecnicaCod = H00542_A255ContagemItem_FMReferenciaTecnicaCod[0];
               n255ContagemItem_FMReferenciaTecnicaCod = H00542_n255ContagemItem_FMReferenciaTecnicaCod[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00542_A257ContagemItem_FMReferenciaTecnicaNom[0];
               n257ContagemItem_FMReferenciaTecnicaNom = H00542_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00542_A269ContagemItem_FSReferenciaTecnicaNom[0];
               n269ContagemItem_FSReferenciaTecnicaNom = H00542_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A953ContagemItem_Evidencias = H00542_A953ContagemItem_Evidencias[0];
               n953ContagemItem_Evidencias = H00542_n953ContagemItem_Evidencias[0];
               A952ContagemItem_TipoUnidade = H00542_A952ContagemItem_TipoUnidade[0];
               A184FuncaoAPF_Tipo = H00542_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00542_A166FuncaoAPF_Nome[0];
               A951ContagemItem_PFL = H00542_A951ContagemItem_PFL[0];
               n951ContagemItem_PFL = H00542_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = H00542_A950ContagemItem_PFB[0];
               n950ContagemItem_PFB = H00542_n950ContagemItem_PFB[0];
               A165FuncaoAPF_Codigo = H00542_A165FuncaoAPF_Codigo[0];
               n165FuncaoAPF_Codigo = H00542_n165FuncaoAPF_Codigo[0];
               A229ContagemItem_Requisito = H00542_A229ContagemItem_Requisito[0];
               n229ContagemItem_Requisito = H00542_n229ContagemItem_Requisito[0];
               A225ContagemItem_Sequencial = H00542_A225ContagemItem_Sequencial[0];
               n225ContagemItem_Sequencial = H00542_n225ContagemItem_Sequencial[0];
               A218Contagem_UsuarioAuditorPessoaNom = H00542_A218Contagem_UsuarioAuditorPessoaNom[0];
               A217Contagem_UsuarioAuditorPessoaCod = H00542_A217Contagem_UsuarioAuditorPessoaCod[0];
               A216Contagem_UsuarioAuditorCod = H00542_A216Contagem_UsuarioAuditorCod[0];
               A215Contagem_UsuarioContadorPessoaNom = H00542_A215Contagem_UsuarioContadorPessoaNom[0];
               n215Contagem_UsuarioContadorPessoaNom = H00542_n215Contagem_UsuarioContadorPessoaNom[0];
               A214Contagem_UsuarioContadorPessoaCod = H00542_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = H00542_n214Contagem_UsuarioContadorPessoaCod[0];
               A213Contagem_UsuarioContadorCod = H00542_A213Contagem_UsuarioContadorCod[0];
               n213Contagem_UsuarioContadorCod = H00542_n213Contagem_UsuarioContadorCod[0];
               A209Contagem_FabricaSoftwarePessoaNom = H00542_A209Contagem_FabricaSoftwarePessoaNom[0];
               A208Contagem_FabricaSoftwarePessoaCod = H00542_A208Contagem_FabricaSoftwarePessoaCod[0];
               A207Contagem_FabricaSoftwareCod = H00542_A207Contagem_FabricaSoftwareCod[0];
               A196Contagem_Tipo = H00542_A196Contagem_Tipo[0];
               n196Contagem_Tipo = H00542_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00542_A195Contagem_Tecnica[0];
               n195Contagem_Tecnica = H00542_n195Contagem_Tecnica[0];
               A194Contagem_AreaTrabalhoDes = H00542_A194Contagem_AreaTrabalhoDes[0];
               n194Contagem_AreaTrabalhoDes = H00542_n194Contagem_AreaTrabalhoDes[0];
               A193Contagem_AreaTrabalhoCod = H00542_A193Contagem_AreaTrabalhoCod[0];
               A197Contagem_DataCriacao = H00542_A197Contagem_DataCriacao[0];
               A192Contagem_Codigo = H00542_A192Contagem_Codigo[0];
               A224ContagemItem_Lancamento = H00542_A224ContagemItem_Lancamento[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00542_A269ContagemItem_FSReferenciaTecnicaNom[0];
               n269ContagemItem_FSReferenciaTecnicaNom = H00542_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00542_A257ContagemItem_FMReferenciaTecnicaNom[0];
               n257ContagemItem_FMReferenciaTecnicaNom = H00542_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A184FuncaoAPF_Tipo = H00542_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00542_A166FuncaoAPF_Nome[0];
               A213Contagem_UsuarioContadorCod = H00542_A213Contagem_UsuarioContadorCod[0];
               n213Contagem_UsuarioContadorCod = H00542_n213Contagem_UsuarioContadorCod[0];
               A196Contagem_Tipo = H00542_A196Contagem_Tipo[0];
               n196Contagem_Tipo = H00542_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00542_A195Contagem_Tecnica[0];
               n195Contagem_Tecnica = H00542_n195Contagem_Tecnica[0];
               A193Contagem_AreaTrabalhoCod = H00542_A193Contagem_AreaTrabalhoCod[0];
               A197Contagem_DataCriacao = H00542_A197Contagem_DataCriacao[0];
               A214Contagem_UsuarioContadorPessoaCod = H00542_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = H00542_n214Contagem_UsuarioContadorPessoaCod[0];
               A215Contagem_UsuarioContadorPessoaNom = H00542_A215Contagem_UsuarioContadorPessoaNom[0];
               n215Contagem_UsuarioContadorPessoaNom = H00542_n215Contagem_UsuarioContadorPessoaNom[0];
               A194Contagem_AreaTrabalhoDes = H00542_A194Contagem_AreaTrabalhoDes[0];
               n194Contagem_AreaTrabalhoDes = H00542_n194Contagem_AreaTrabalhoDes[0];
               /* Execute user event: E24542 */
               E24542 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 116;
            WB540( ) ;
         }
         nGXsfl_116_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV173Contagem_FabricaSoftwareCod1 ,
                                              AV17ContagemItem_Sequencial1 ,
                                              AV54Contagem_DataCriacao1 ,
                                              AV55Contagem_DataCriacao_To1 ,
                                              AV18FuncaoAPF_Nome1 ,
                                              AV56ContagemItem_FSReferenciaTecnicaNom1 ,
                                              AV57ContagemItem_FMReferenciaTecnicaNom1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV174Contagem_FabricaSoftwareCod2 ,
                                              AV22ContagemItem_Sequencial2 ,
                                              AV58Contagem_DataCriacao2 ,
                                              AV59Contagem_DataCriacao_To2 ,
                                              AV23FuncaoAPF_Nome2 ,
                                              AV60ContagemItem_FSReferenciaTecnicaNom2 ,
                                              AV61ContagemItem_FMReferenciaTecnicaNom2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV26DynamicFiltersOperator3 ,
                                              AV175Contagem_FabricaSoftwareCod3 ,
                                              AV27ContagemItem_Sequencial3 ,
                                              AV62Contagem_DataCriacao3 ,
                                              AV63Contagem_DataCriacao_To3 ,
                                              AV28FuncaoAPF_Nome3 ,
                                              AV64ContagemItem_FSReferenciaTecnicaNom3 ,
                                              AV65ContagemItem_FMReferenciaTecnicaNom3 ,
                                              A207Contagem_FabricaSoftwareCod ,
                                              A225ContagemItem_Sequencial ,
                                              A197Contagem_DataCriacao ,
                                              A166FuncaoAPF_Nome ,
                                              A269ContagemItem_FSReferenciaTecnicaNom ,
                                              A257ContagemItem_FMReferenciaTecnicaNom ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
         lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
         lV56ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
         lV56ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
         lV57ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
         lV57ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
         lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
         lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
         lV60ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
         lV60ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
         lV61ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
         lV61ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
         lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
         lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
         lV64ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
         lV64ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
         lV65ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
         lV65ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
         /* Using cursor H00543 */
         pr_default.execute(1, new Object[] {AV173Contagem_FabricaSoftwareCod1, AV173Contagem_FabricaSoftwareCod1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV17ContagemItem_Sequencial1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, lV18FuncaoAPF_Nome1, lV18FuncaoAPF_Nome1, lV56ContagemItem_FSReferenciaTecnicaNom1, lV56ContagemItem_FSReferenciaTecnicaNom1, lV57ContagemItem_FMReferenciaTecnicaNom1, lV57ContagemItem_FMReferenciaTecnicaNom1, AV174Contagem_FabricaSoftwareCod2, AV174Contagem_FabricaSoftwareCod2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV22ContagemItem_Sequencial2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, lV23FuncaoAPF_Nome2, lV23FuncaoAPF_Nome2, lV60ContagemItem_FSReferenciaTecnicaNom2, lV60ContagemItem_FSReferenciaTecnicaNom2, lV61ContagemItem_FMReferenciaTecnicaNom2, lV61ContagemItem_FMReferenciaTecnicaNom2, AV175Contagem_FabricaSoftwareCod3, AV175Contagem_FabricaSoftwareCod3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV27ContagemItem_Sequencial3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, lV28FuncaoAPF_Nome3, lV28FuncaoAPF_Nome3, lV64ContagemItem_FSReferenciaTecnicaNom3, lV64ContagemItem_FSReferenciaTecnicaNom3, lV65ContagemItem_FMReferenciaTecnicaNom3, lV65ContagemItem_FMReferenciaTecnicaNom3});
         GRID_nRecordCount = H00543_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         }
         return (int)(0) ;
      }

      protected void STRUP540( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22542 */
         E22542 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD1");
               GX_FocusControl = edtavContagem_fabricasoftwarecod1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV173Contagem_FabricaSoftwareCod1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            else
            {
               AV173Contagem_FabricaSoftwareCod1 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial1_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMITEM_SEQUENCIAL1");
               GX_FocusControl = edtavContagemitem_sequencial1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemItem_Sequencial1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemItem_Sequencial1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemItem_Sequencial1), 3, 0)));
            }
            else
            {
               AV17ContagemItem_Sequencial1 = (short)(context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemItem_Sequencial1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemItem_Sequencial1), 3, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao1"}), 1, "vCONTAGEM_DATACRIACAO1");
               GX_FocusControl = edtavContagem_datacriacao1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54Contagem_DataCriacao1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Contagem_DataCriacao1", context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"));
            }
            else
            {
               AV54Contagem_DataCriacao1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Contagem_DataCriacao1", context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao_To1"}), 1, "vCONTAGEM_DATACRIACAO_TO1");
               GX_FocusControl = edtavContagem_datacriacao_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55Contagem_DataCriacao_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Contagem_DataCriacao_To1", context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"));
            }
            else
            {
               AV55Contagem_DataCriacao_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Contagem_DataCriacao_To1", context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"));
            }
            AV18FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            AV56ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
            AV57ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD2");
               GX_FocusControl = edtavContagem_fabricasoftwarecod2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV174Contagem_FabricaSoftwareCod2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0)));
            }
            else
            {
               AV174Contagem_FabricaSoftwareCod2 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial2_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMITEM_SEQUENCIAL2");
               GX_FocusControl = edtavContagemitem_sequencial2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22ContagemItem_Sequencial2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemItem_Sequencial2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemItem_Sequencial2), 3, 0)));
            }
            else
            {
               AV22ContagemItem_Sequencial2 = (short)(context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemItem_Sequencial2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemItem_Sequencial2), 3, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao2"}), 1, "vCONTAGEM_DATACRIACAO2");
               GX_FocusControl = edtavContagem_datacriacao2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58Contagem_DataCriacao2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Contagem_DataCriacao2", context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"));
            }
            else
            {
               AV58Contagem_DataCriacao2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Contagem_DataCriacao2", context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao_To2"}), 1, "vCONTAGEM_DATACRIACAO_TO2");
               GX_FocusControl = edtavContagem_datacriacao_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59Contagem_DataCriacao_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contagem_DataCriacao_To2", context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"));
            }
            else
            {
               AV59Contagem_DataCriacao_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contagem_DataCriacao_To2", context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"));
            }
            AV23FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            AV60ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
            AV61ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD3");
               GX_FocusControl = edtavContagem_fabricasoftwarecod3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV175Contagem_FabricaSoftwareCod3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0)));
            }
            else
            {
               AV175Contagem_FabricaSoftwareCod3 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial3_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMITEM_SEQUENCIAL3");
               GX_FocusControl = edtavContagemitem_sequencial3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27ContagemItem_Sequencial3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemItem_Sequencial3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContagemItem_Sequencial3), 3, 0)));
            }
            else
            {
               AV27ContagemItem_Sequencial3 = (short)(context.localUtil.CToN( cgiGet( edtavContagemitem_sequencial3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemItem_Sequencial3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContagemItem_Sequencial3), 3, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao3"}), 1, "vCONTAGEM_DATACRIACAO3");
               GX_FocusControl = edtavContagem_datacriacao3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62Contagem_DataCriacao3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Contagem_DataCriacao3", context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"));
            }
            else
            {
               AV62Contagem_DataCriacao3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Contagem_DataCriacao3", context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagem_datacriacao_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem_Data Criacao_To3"}), 1, "vCONTAGEM_DATACRIACAO_TO3");
               GX_FocusControl = edtavContagem_datacriacao_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63Contagem_DataCriacao_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Contagem_DataCriacao_To3", context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"));
            }
            else
            {
               AV63Contagem_DataCriacao_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagem_datacriacao_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Contagem_DataCriacao_To3", context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"));
            }
            AV28FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            AV64ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
            AV65ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_116 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_116"), ",", "."));
            AV170GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV171GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD1"), ",", ".") != Convert.ToDecimal( AV173Contagem_FabricaSoftwareCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL1"), ",", ".") != Convert.ToDecimal( AV17ContagemItem_Sequencial1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO1"), 0) != AV54Contagem_DataCriacao1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO1"), 0) != AV55Contagem_DataCriacao_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV18FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1"), AV56ContagemItem_FSReferenciaTecnicaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1"), AV57ContagemItem_FMReferenciaTecnicaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD2"), ",", ".") != Convert.ToDecimal( AV174Contagem_FabricaSoftwareCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL2"), ",", ".") != Convert.ToDecimal( AV22ContagemItem_Sequencial2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO2"), 0) != AV58Contagem_DataCriacao2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO2"), 0) != AV59Contagem_DataCriacao_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV23FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2"), AV60ContagemItem_FSReferenciaTecnicaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2"), AV61ContagemItem_FMReferenciaTecnicaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_FABRICASOFTWARECOD3"), ",", ".") != Convert.ToDecimal( AV175Contagem_FabricaSoftwareCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMITEM_SEQUENCIAL3"), ",", ".") != Convert.ToDecimal( AV27ContagemItem_Sequencial3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO3"), 0) != AV62Contagem_DataCriacao3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEM_DATACRIACAO_TO3"), 0) != AV63Contagem_DataCriacao_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV28FuncaoAPF_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3"), AV64ContagemItem_FSReferenciaTecnicaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3"), AV65ContagemItem_FMReferenciaTecnicaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22542 */
         E22542 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22542( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Select Contagem Item";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Contratada F�brica", 0);
         cmbavOrderedby.addItem("2", "Sequencial", 0);
         cmbavOrderedby.addItem("3", "Item do Lan�amento", 0);
         cmbavOrderedby.addItem("4", "C�digo", 0);
         cmbavOrderedby.addItem("5", "Data Cria��o", 0);
         cmbavOrderedby.addItem("6", "C�d. �rea de Trabalho", 0);
         cmbavOrderedby.addItem("7", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("8", "T�cnica", 0);
         cmbavOrderedby.addItem("9", "Tipo", 0);
         cmbavOrderedby.addItem("10", "Pessoa Contratada", 0);
         cmbavOrderedby.addItem("11", "F�brica )", 0);
         cmbavOrderedby.addItem("12", "C�d. Usu�rio Contador", 0);
         cmbavOrderedby.addItem("13", "C�d. Pessoa Contador", 0);
         cmbavOrderedby.addItem("14", "Nome do Contador", 0);
         cmbavOrderedby.addItem("15", "Usu�rio Auditor", 0);
         cmbavOrderedby.addItem("16", "Pessoa Auditor", 0);
         cmbavOrderedby.addItem("17", "do Auditor", 0);
         cmbavOrderedby.addItem("18", "de Software)", 0);
         cmbavOrderedby.addItem("19", "Fun��o de Transa��o", 0);
         cmbavOrderedby.addItem("20", "PFB", 0);
         cmbavOrderedby.addItem("21", "PFL", 0);
         cmbavOrderedby.addItem("22", "Fun��o de Transa��o", 0);
         cmbavOrderedby.addItem("23", "Tipo", 0);
         cmbavOrderedby.addItem("24", "Tipo", 0);
         cmbavOrderedby.addItem("25", "Evidencias", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E23542( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         edtContagemItem_Lancamento_Titleformat = 2;
         edtContagemItem_Lancamento_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Item do Lan�amento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Title", edtContagemItem_Lancamento_Title);
         edtContagem_Codigo_Titleformat = 2;
         edtContagem_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Title", edtContagem_Codigo_Title);
         edtContagem_DataCriacao_Titleformat = 2;
         edtContagem_DataCriacao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Data Cria��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_DataCriacao_Internalname, "Title", edtContagem_DataCriacao_Title);
         edtContagem_AreaTrabalhoCod_Titleformat = 2;
         edtContagem_AreaTrabalhoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�d. �rea de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Title", edtContagem_AreaTrabalhoCod_Title);
         edtContagem_AreaTrabalhoDes_Titleformat = 2;
         edtContagem_AreaTrabalhoDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "�rea de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoDes_Internalname, "Title", edtContagem_AreaTrabalhoDes_Title);
         cmbContagem_Tecnica_Titleformat = 2;
         cmbContagem_Tecnica.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "T�cnica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Title", cmbContagem_Tecnica.Title.Text);
         cmbContagem_Tipo_Titleformat = 2;
         cmbContagem_Tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Title", cmbContagem_Tipo.Title.Text);
         edtContagem_FabricaSoftwareCod_Titleformat = 2;
         edtContagem_FabricaSoftwareCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratada F�brica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwareCod_Internalname, "Title", edtContagem_FabricaSoftwareCod_Title);
         edtContagem_FabricaSoftwarePessoaCod_Titleformat = 2;
         edtContagem_FabricaSoftwarePessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Contratada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwarePessoaCod_Internalname, "Title", edtContagem_FabricaSoftwarePessoaCod_Title);
         edtContagem_FabricaSoftwarePessoaNom_Titleformat = 2;
         edtContagem_FabricaSoftwarePessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "F�brica )", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_FabricaSoftwarePessoaNom_Internalname, "Title", edtContagem_FabricaSoftwarePessoaNom_Title);
         edtContagem_UsuarioContadorCod_Titleformat = 2;
         edtContagem_UsuarioContadorCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�d. Usu�rio Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Title", edtContagem_UsuarioContadorCod_Title);
         edtContagem_UsuarioContadorPessoaCod_Titleformat = 2;
         edtContagem_UsuarioContadorPessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�d. Pessoa Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Title", edtContagem_UsuarioContadorPessoaCod_Title);
         edtContagem_UsuarioContadorPessoaNom_Titleformat = 2;
         edtContagem_UsuarioContadorPessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome do Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaNom_Internalname, "Title", edtContagem_UsuarioContadorPessoaNom_Title);
         edtContagem_UsuarioAuditorCod_Titleformat = 2;
         edtContagem_UsuarioAuditorCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV13OrderedBy==15) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorCod_Internalname, "Title", edtContagem_UsuarioAuditorCod_Title);
         edtContagem_UsuarioAuditorPessoaCod_Titleformat = 2;
         edtContagem_UsuarioAuditorPessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV13OrderedBy==16) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorPessoaCod_Internalname, "Title", edtContagem_UsuarioAuditorPessoaCod_Title);
         edtContagem_UsuarioAuditorPessoaNom_Titleformat = 2;
         edtContagem_UsuarioAuditorPessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV13OrderedBy==17) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "do Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioAuditorPessoaNom_Internalname, "Title", edtContagem_UsuarioAuditorPessoaNom_Title);
         edtContagemItem_Sequencial_Titleformat = 2;
         edtContagemItem_Sequencial_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sequencial", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Sequencial_Internalname, "Title", edtContagemItem_Sequencial_Title);
         edtContagemItem_Requisito_Titleformat = 2;
         edtContagemItem_Requisito_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV13OrderedBy==18) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Software)", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Requisito_Internalname, "Title", edtContagemItem_Requisito_Title);
         edtFuncaoAPF_Codigo_Titleformat = 2;
         edtFuncaoAPF_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 19);' >%5</span>", ((AV13OrderedBy==19) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fun��o de Transa��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Title", edtFuncaoAPF_Codigo_Title);
         edtContagemItem_PFB_Titleformat = 2;
         edtContagemItem_PFB_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 20);' >%5</span>", ((AV13OrderedBy==20) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFB", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFB_Internalname, "Title", edtContagemItem_PFB_Title);
         edtContagemItem_PFL_Titleformat = 2;
         edtContagemItem_PFL_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 21);' >%5</span>", ((AV13OrderedBy==21) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFL", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFL_Internalname, "Title", edtContagemItem_PFL_Title);
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 22);' >%5</span>", ((AV13OrderedBy==22) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fun��o de Transa��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Tipo_Titleformat = 2;
         cmbFuncaoAPF_Tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 23);' >%5</span>", ((AV13OrderedBy==23) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Title", cmbFuncaoAPF_Tipo.Title.Text);
         cmbContagemItem_TipoUnidade_Titleformat = 2;
         cmbContagemItem_TipoUnidade.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 24);' >%5</span>", ((AV13OrderedBy==24) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Title", cmbContagemItem_TipoUnidade.Title.Text);
         edtContagemItem_Evidencias_Titleformat = 2;
         edtContagemItem_Evidencias_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 25);' >%5</span>", ((AV13OrderedBy==25) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Evidencias", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Evidencias_Internalname, "Title", edtContagemItem_Evidencias_Title);
         AV170GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV170GridCurrentPage), 10, 0)));
         AV171GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV171GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11542( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV169PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV169PageToGo) ;
         }
      }

      private void E24542( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV178Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 116;
         }
         sendrow_1162( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_116_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(116, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25542 */
         E25542 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25542( )
      {
         /* Enter Routine */
         AV7InOutContagemItem_Lancamento = A224ContagemItem_Lancamento;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemItem_Lancamento), 6, 0)));
         AV172InOutContagem_FabricaSoftwareCod = A207Contagem_FabricaSoftwareCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172InOutContagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172InOutContagem_FabricaSoftwareCod), 6, 0)));
         AV8InOutContagemItem_Sequencial = A225ContagemItem_Sequencial;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemItem_Sequencial), 3, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContagemItem_Lancamento,(int)AV172InOutContagem_FabricaSoftwareCod,(short)AV8InOutContagemItem_Sequencial});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E12542( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E17542( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
      }

      protected void E13542( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18542( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19542( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
      }

      protected void E14542( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20542( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15542( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV173Contagem_FabricaSoftwareCod1, AV17ContagemItem_Sequencial1, AV54Contagem_DataCriacao1, AV55Contagem_DataCriacao_To1, AV18FuncaoAPF_Nome1, AV56ContagemItem_FSReferenciaTecnicaNom1, AV57ContagemItem_FMReferenciaTecnicaNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV174Contagem_FabricaSoftwareCod2, AV22ContagemItem_Sequencial2, AV58Contagem_DataCriacao2, AV59Contagem_DataCriacao_To2, AV23FuncaoAPF_Nome2, AV60ContagemItem_FSReferenciaTecnicaNom2, AV61ContagemItem_FMReferenciaTecnicaNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV175Contagem_FabricaSoftwareCod3, AV27ContagemItem_Sequencial3, AV62Contagem_DataCriacao3, AV63Contagem_DataCriacao_To3, AV28FuncaoAPF_Nome3, AV64ContagemItem_FSReferenciaTecnicaNom3, AV65ContagemItem_FMReferenciaTecnicaNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21542( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16542( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagem_fabricasoftwarecod1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod1_Visible), 5, 0)));
         edtavContagemitem_sequencial1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagem_datacriacao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao1_Visible), 5, 0)));
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom1_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 )
         {
            edtavContagemitem_sequencial1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 )
         {
            tblTablemergeddynamicfilterscontagem_datacriacao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagem_fabricasoftwarecod2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod2_Visible), 5, 0)));
         edtavContagemitem_sequencial2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagem_datacriacao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao2_Visible), 5, 0)));
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom2_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 )
         {
            edtavContagemitem_sequencial2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 )
         {
            tblTablemergeddynamicfilterscontagem_datacriacao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagem_fabricasoftwarecod3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod3_Visible), 5, 0)));
         edtavContagemitem_sequencial3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagem_datacriacao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao3_Visible), 5, 0)));
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom3_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_fabricasoftwarecod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 )
         {
            edtavContagemitem_sequencial3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_sequencial3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_sequencial3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 )
         {
            tblTablemergeddynamicfilterscontagem_datacriacao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagem_datacriacao3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fsreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemitem_fmreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV174Contagem_FabricaSoftwareCod2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV175Contagem_FabricaSoftwareCod3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV173Contagem_FabricaSoftwareCod1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV173Contagem_FabricaSoftwareCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContagemItem_Sequencial1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemItem_Sequencial1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContagemItem_Sequencial1), 3, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 )
            {
               AV54Contagem_DataCriacao1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Contagem_DataCriacao1", context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"));
               AV55Contagem_DataCriacao_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Contagem_DataCriacao_To1", context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18FuncaoAPF_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV56ContagemItem_FSReferenciaTecnicaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemItem_FSReferenciaTecnicaNom1", AV56ContagemItem_FSReferenciaTecnicaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV57ContagemItem_FMReferenciaTecnicaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemItem_FMReferenciaTecnicaNom1", AV57ContagemItem_FMReferenciaTecnicaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV174Contagem_FabricaSoftwareCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ContagemItem_Sequencial2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemItem_Sequencial2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemItem_Sequencial2), 3, 0)));
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 )
               {
                  AV58Contagem_DataCriacao2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Contagem_DataCriacao2", context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"));
                  AV59Contagem_DataCriacao_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contagem_DataCriacao_To2", context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23FuncaoAPF_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV60ContagemItem_FSReferenciaTecnicaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ContagemItem_FSReferenciaTecnicaNom2", AV60ContagemItem_FSReferenciaTecnicaNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV61ContagemItem_FMReferenciaTecnicaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContagemItem_FMReferenciaTecnicaNom2", AV61ContagemItem_FMReferenciaTecnicaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV175Contagem_FabricaSoftwareCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV175Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27ContagemItem_Sequencial3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ContagemItem_Sequencial3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ContagemItem_Sequencial3), 3, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 )
                  {
                     AV62Contagem_DataCriacao3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Contagem_DataCriacao3", context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"));
                     AV63Contagem_DataCriacao_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Contagem_DataCriacao_To3", context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28FuncaoAPF_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV64ContagemItem_FSReferenciaTecnicaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ContagemItem_FSReferenciaTecnicaNom3", AV64ContagemItem_FSReferenciaTecnicaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV65ContagemItem_FMReferenciaTecnicaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemItem_FMReferenciaTecnicaNom3", AV65ContagemItem_FMReferenciaTecnicaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV173Contagem_FabricaSoftwareCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ! (0==AV17ContagemItem_Sequencial1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17ContagemItem_Sequencial1), 3, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 ) && ! ( (DateTime.MinValue==AV54Contagem_DataCriacao1) && (DateTime.MinValue==AV55Contagem_DataCriacao_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV54Contagem_DataCriacao1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV55Contagem_DataCriacao_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18FuncaoAPF_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56ContagemItem_FSReferenciaTecnicaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57ContagemItem_FMReferenciaTecnicaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV174Contagem_FabricaSoftwareCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ! (0==AV22ContagemItem_Sequencial2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV22ContagemItem_Sequencial2), 3, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 ) && ! ( (DateTime.MinValue==AV58Contagem_DataCriacao2) && (DateTime.MinValue==AV59Contagem_DataCriacao_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV58Contagem_DataCriacao2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV59Contagem_DataCriacao_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23FuncaoAPF_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV60ContagemItem_FSReferenciaTecnicaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV61ContagemItem_FMReferenciaTecnicaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV175Contagem_FabricaSoftwareCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ! (0==AV27ContagemItem_Sequencial3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV27ContagemItem_Sequencial3), 3, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 ) && ! ( (DateTime.MinValue==AV62Contagem_DataCriacao3) && (DateTime.MinValue==AV63Contagem_DataCriacao_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV62Contagem_DataCriacao3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV63Contagem_DataCriacao_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28FuncaoAPF_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV64ContagemItem_FSReferenciaTecnicaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV65ContagemItem_FMReferenciaTecnicaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_542( true) ;
         }
         else
         {
            wb_table2_5_542( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_110_542( true) ;
         }
         else
         {
            wb_table3_110_542( false) ;
         }
         return  ;
      }

      protected void wb_table3_110_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_542e( true) ;
         }
         else
         {
            wb_table1_2_542e( false) ;
         }
      }

      protected void wb_table3_110_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_113_542( true) ;
         }
         else
         {
            wb_table4_113_542( false) ;
         }
         return  ;
      }

      protected void wb_table4_113_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_110_542e( true) ;
         }
         else
         {
            wb_table3_110_542e( false) ;
         }
      }

      protected void wb_table4_113_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"116\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Lancamento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Lancamento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Lancamento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_DataCriacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_DataCriacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_DataCriacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_AreaTrabalhoDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_AreaTrabalhoDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_AreaTrabalhoDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Tecnica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Tecnica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Tecnica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwareCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwareCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwareCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwarePessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwarePessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwarePessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwarePessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwarePessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwarePessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Sequencial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Sequencial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Sequencial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Requisito_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Requisito_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Requisito_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFL_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFL_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFL_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemItem_TipoUnidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Evidencias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Evidencias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Evidencias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Lancamento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Lancamento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_DataCriacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_DataCriacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A194Contagem_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_AreaTrabalhoDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_AreaTrabalhoDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A195Contagem_Tecnica));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Tecnica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Tecnica_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A196Contagem_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwareCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwareCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwarePessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwarePessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwarePessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwarePessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Sequencial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Sequencial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Requisito_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Requisito_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFL_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFL_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A184FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A952ContagemItem_TipoUnidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemItem_TipoUnidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemItem_TipoUnidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A953ContagemItem_Evidencias);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Evidencias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Evidencias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 116 )
         {
            wbEnd = 0;
            nRC_GXsfl_116 = (short)(nGXsfl_116_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_113_542e( true) ;
         }
         else
         {
            wb_table4_113_542e( false) ;
         }
      }

      protected void wb_table2_5_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_542( true) ;
         }
         else
         {
            wb_table5_14_542( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_542e( true) ;
         }
         else
         {
            wb_table2_5_542e( false) ;
         }
      }

      protected void wb_table5_14_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_542( true) ;
         }
         else
         {
            wb_table6_19_542( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_542e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_542e( true) ;
         }
         else
         {
            wb_table5_14_542e( false) ;
         }
      }

      protected void wb_table6_19_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_542( true) ;
         }
         else
         {
            wb_table7_28_542( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_57_542( true) ;
         }
         else
         {
            wb_table8_57_542( false) ;
         }
         return  ;
      }

      protected void wb_table8_57_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_86_542( true) ;
         }
         else
         {
            wb_table9_86_542( false) ;
         }
         return  ;
      }

      protected void wb_table9_86_542e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_542e( true) ;
         }
         else
         {
            wb_table6_19_542e( false) ;
         }
      }

      protected void wb_table9_86_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV175Contagem_FabricaSoftwareCod3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV175Contagem_FabricaSoftwareCod3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_sequencial3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27ContagemItem_Sequencial3), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27ContagemItem_Sequencial3), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_sequencial3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_sequencial3_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            wb_table10_93_542( true) ;
         }
         else
         {
            wb_table10_93_542( false) ;
         }
         return  ;
      }

      protected void wb_table10_93_542e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome3_Internalname, AV28FuncaoAPF_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavFuncaoapf_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom3_Internalname, StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3), StringUtil.RTrim( context.localUtil.Format( AV64ContagemItem_FSReferenciaTecnicaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom3_Internalname, StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3), StringUtil.RTrim( context.localUtil.Format( AV65ContagemItem_FMReferenciaTecnicaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_86_542e( true) ;
         }
         else
         {
            wb_table9_86_542e( false) ;
         }
      }

      protected void wb_table10_93_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagem_datacriacao3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname, tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao3_Internalname, context.localUtil.Format(AV62Contagem_DataCriacao3, "99/99/99"), context.localUtil.Format( AV62Contagem_DataCriacao3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao_to3_Internalname, context.localUtil.Format(AV63Contagem_DataCriacao_To3, "99/99/99"), context.localUtil.Format( AV63Contagem_DataCriacao_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_93_542e( true) ;
         }
         else
         {
            wb_table10_93_542e( false) ;
         }
      }

      protected void wb_table8_57_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV174Contagem_FabricaSoftwareCod2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV174Contagem_FabricaSoftwareCod2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_sequencial2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ContagemItem_Sequencial2), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22ContagemItem_Sequencial2), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_sequencial2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_sequencial2_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            wb_table11_64_542( true) ;
         }
         else
         {
            wb_table11_64_542( false) ;
         }
         return  ;
      }

      protected void wb_table11_64_542e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV23FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom2_Internalname, StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2), StringUtil.RTrim( context.localUtil.Format( AV60ContagemItem_FSReferenciaTecnicaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom2_Internalname, StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2), StringUtil.RTrim( context.localUtil.Format( AV61ContagemItem_FMReferenciaTecnicaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_57_542e( true) ;
         }
         else
         {
            wb_table8_57_542e( false) ;
         }
      }

      protected void wb_table11_64_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagem_datacriacao2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname, tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao2_Internalname, context.localUtil.Format(AV58Contagem_DataCriacao2, "99/99/99"), context.localUtil.Format( AV58Contagem_DataCriacao2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao_to2_Internalname, context.localUtil.Format(AV59Contagem_DataCriacao_To2, "99/99/99"), context.localUtil.Format( AV59Contagem_DataCriacao_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_64_542e( true) ;
         }
         else
         {
            wb_table11_64_542e( false) ;
         }
      }

      protected void wb_table7_28_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContagemItem.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV173Contagem_FabricaSoftwareCod1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV173Contagem_FabricaSoftwareCod1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_sequencial1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContagemItem_Sequencial1), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17ContagemItem_Sequencial1), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_sequencial1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_sequencial1_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            wb_table12_35_542( true) ;
         }
         else
         {
            wb_table12_35_542( false) ;
         }
         return  ;
      }

      protected void wb_table12_35_542e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV18FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom1_Internalname, StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1), StringUtil.RTrim( context.localUtil.Format( AV56ContagemItem_FSReferenciaTecnicaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom1_Internalname, StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1), StringUtil.RTrim( context.localUtil.Format( AV57ContagemItem_FMReferenciaTecnicaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_542e( true) ;
         }
         else
         {
            wb_table7_28_542e( false) ;
         }
      }

      protected void wb_table12_35_542( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagem_datacriacao1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname, tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao1_Internalname, context.localUtil.Format(AV54Contagem_DataCriacao1, "99/99/99"), context.localUtil.Format( AV54Contagem_DataCriacao1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagem_datacriacao_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagem_datacriacao_to1_Internalname, context.localUtil.Format(AV55Contagem_DataCriacao_To1, "99/99/99"), context.localUtil.Format( AV55Contagem_DataCriacao_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_datacriacao_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemItem.htm");
            GxWebStd.gx_bitmap( context, edtavContagem_datacriacao_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_35_542e( true) ;
         }
         else
         {
            wb_table12_35_542e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagemItem_Lancamento = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemItem_Lancamento), 6, 0)));
         AV172InOutContagem_FabricaSoftwareCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172InOutContagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172InOutContagem_FabricaSoftwareCod), 6, 0)));
         AV8InOutContagemItem_Sequencial = Convert.ToInt16(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemItem_Sequencial), 3, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA542( ) ;
         WS542( ) ;
         WE542( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117344461");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemitem.js", "?20203117344461");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1162( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_116_idx;
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_116_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_116_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_116_idx;
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD_"+sGXsfl_116_idx;
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES_"+sGXsfl_116_idx;
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA_"+sGXsfl_116_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_116_idx;
         edtContagem_FabricaSoftwareCod_Internalname = "CONTAGEM_FABRICASOFTWARECOD_"+sGXsfl_116_idx;
         edtContagem_FabricaSoftwarePessoaCod_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOACOD_"+sGXsfl_116_idx;
         edtContagem_FabricaSoftwarePessoaNom_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOANOM_"+sGXsfl_116_idx;
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD_"+sGXsfl_116_idx;
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD_"+sGXsfl_116_idx;
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM_"+sGXsfl_116_idx;
         edtContagem_UsuarioAuditorCod_Internalname = "CONTAGEM_USUARIOAUDITORCOD_"+sGXsfl_116_idx;
         edtContagem_UsuarioAuditorPessoaCod_Internalname = "CONTAGEM_USUARIOAUDITORPESSOACOD_"+sGXsfl_116_idx;
         edtContagem_UsuarioAuditorPessoaNom_Internalname = "CONTAGEM_USUARIOAUDITORPESSOANOM_"+sGXsfl_116_idx;
         edtContagemItem_Sequencial_Internalname = "CONTAGEMITEM_SEQUENCIAL_"+sGXsfl_116_idx;
         edtContagemItem_Requisito_Internalname = "CONTAGEMITEM_REQUISITO_"+sGXsfl_116_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_116_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_116_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_116_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_116_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_116_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_116_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_116_idx;
      }

      protected void SubsflControlProps_fel_1162( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_116_fel_idx;
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO_"+sGXsfl_116_fel_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_116_fel_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_116_fel_idx;
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD_"+sGXsfl_116_fel_idx;
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES_"+sGXsfl_116_fel_idx;
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA_"+sGXsfl_116_fel_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_116_fel_idx;
         edtContagem_FabricaSoftwareCod_Internalname = "CONTAGEM_FABRICASOFTWARECOD_"+sGXsfl_116_fel_idx;
         edtContagem_FabricaSoftwarePessoaCod_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOACOD_"+sGXsfl_116_fel_idx;
         edtContagem_FabricaSoftwarePessoaNom_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOANOM_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioAuditorCod_Internalname = "CONTAGEM_USUARIOAUDITORCOD_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioAuditorPessoaCod_Internalname = "CONTAGEM_USUARIOAUDITORPESSOACOD_"+sGXsfl_116_fel_idx;
         edtContagem_UsuarioAuditorPessoaNom_Internalname = "CONTAGEM_USUARIOAUDITORPESSOANOM_"+sGXsfl_116_fel_idx;
         edtContagemItem_Sequencial_Internalname = "CONTAGEMITEM_SEQUENCIAL_"+sGXsfl_116_fel_idx;
         edtContagemItem_Requisito_Internalname = "CONTAGEMITEM_REQUISITO_"+sGXsfl_116_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_116_fel_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_116_fel_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_116_fel_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_116_fel_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_116_fel_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_116_fel_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_116_fel_idx;
      }

      protected void sendrow_1162( )
      {
         SubsflControlProps_1162( ) ;
         WB540( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_116_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_116_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_116_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 117,'',false,'',116)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV178Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV178Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_116_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Lancamento_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Lancamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_DataCriacao_Internalname,context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"),context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_DataCriacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_AreaTrabalhoDes_Internalname,(String)A194Contagem_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTAGEM_TECNICA_" + sGXsfl_116_idx;
            cmbContagem_Tecnica.Name = GXCCtl;
            cmbContagem_Tecnica.WebTags = "";
            cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
            cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
            cmbContagem_Tecnica.addItem("2", "Estimada", 0);
            cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
            if ( cmbContagem_Tecnica.ItemCount > 0 )
            {
               A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
               n195Contagem_Tecnica = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tecnica,(String)cmbContagem_Tecnica_Internalname,StringUtil.RTrim( A195Contagem_Tecnica),(short)1,(String)cmbContagem_Tecnica_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_116_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tipo,(String)cmbContagem_Tipo_Internalname,StringUtil.RTrim( A196Contagem_Tipo),(short)1,(String)cmbContagem_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwareCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwareCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwarePessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwarePessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwarePessoaNom_Internalname,StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom),StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwarePessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorPessoaNom_Internalname,StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom),StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorPessoaNom_Internalname,StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom),StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Sequencial",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Requisito_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Requisito_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Requisito",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")),context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")),context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_116_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Tipo,(String)cmbFuncaoAPF_Tipo_Internalname,StringUtil.RTrim( A184FuncaoAPF_Tipo),(short)1,(String)cmbFuncaoAPF_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_116_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_116_idx;
               cmbContagemItem_TipoUnidade.Name = GXCCtl;
               cmbContagemItem_TipoUnidade.WebTags = "";
               if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
               {
                  A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_TipoUnidade,(String)cmbContagemItem_TipoUnidade_Internalname,StringUtil.RTrim( A952ContagemItem_TipoUnidade),(short)1,(String)cmbContagemItem_TipoUnidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Evidencias_Internalname,(String)A953ContagemItem_Evidencias,(String)A953ContagemItem_Evidencias,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Evidencias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5000,(short)0,(short)0,(short)116,(short)1,(short)0,(short)-1,(bool)true,(String)"Evidencias",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_LANCAMENTO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWARECOD"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORCOD"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_SEQUENCIAL"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_REQUISITO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, A953ContagemItem_Evidencias));
            GridContainer.AddRow(GridRow);
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         /* End function sendrow_1162 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagem_fabricasoftwarecod1_Internalname = "vCONTAGEM_FABRICASOFTWARECOD1";
         edtavContagemitem_sequencial1_Internalname = "vCONTAGEMITEM_SEQUENCIAL1";
         edtavContagem_datacriacao1_Internalname = "vCONTAGEM_DATACRIACAO1";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEM_DATACRIACAO_RANGEMIDDLETEXT1";
         edtavContagem_datacriacao_to1_Internalname = "vCONTAGEM_DATACRIACAO_TO1";
         tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1";
         edtavFuncaoapf_nome1_Internalname = "vFUNCAOAPF_NOME1";
         edtavContagemitem_fsreferenciatecnicanom1_Internalname = "vCONTAGEMITEM_FSREFERENCIATECNICANOM1";
         edtavContagemitem_fmreferenciatecnicanom1_Internalname = "vCONTAGEMITEM_FMREFERENCIATECNICANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagem_fabricasoftwarecod2_Internalname = "vCONTAGEM_FABRICASOFTWARECOD2";
         edtavContagemitem_sequencial2_Internalname = "vCONTAGEMITEM_SEQUENCIAL2";
         edtavContagem_datacriacao2_Internalname = "vCONTAGEM_DATACRIACAO2";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEM_DATACRIACAO_RANGEMIDDLETEXT2";
         edtavContagem_datacriacao_to2_Internalname = "vCONTAGEM_DATACRIACAO_TO2";
         tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2";
         edtavFuncaoapf_nome2_Internalname = "vFUNCAOAPF_NOME2";
         edtavContagemitem_fsreferenciatecnicanom2_Internalname = "vCONTAGEMITEM_FSREFERENCIATECNICANOM2";
         edtavContagemitem_fmreferenciatecnicanom2_Internalname = "vCONTAGEMITEM_FMREFERENCIATECNICANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagem_fabricasoftwarecod3_Internalname = "vCONTAGEM_FABRICASOFTWARECOD3";
         edtavContagemitem_sequencial3_Internalname = "vCONTAGEMITEM_SEQUENCIAL3";
         edtavContagem_datacriacao3_Internalname = "vCONTAGEM_DATACRIACAO3";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEM_DATACRIACAO_RANGEMIDDLETEXT3";
         edtavContagem_datacriacao_to3_Internalname = "vCONTAGEM_DATACRIACAO_TO3";
         tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3";
         edtavFuncaoapf_nome3_Internalname = "vFUNCAOAPF_NOME3";
         edtavContagemitem_fsreferenciatecnicanom3_Internalname = "vCONTAGEMITEM_FSREFERENCIATECNICANOM3";
         edtavContagemitem_fmreferenciatecnicanom3_Internalname = "vCONTAGEMITEM_FMREFERENCIATECNICANOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD";
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES";
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA";
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO";
         edtContagem_FabricaSoftwareCod_Internalname = "CONTAGEM_FABRICASOFTWARECOD";
         edtContagem_FabricaSoftwarePessoaCod_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOACOD";
         edtContagem_FabricaSoftwarePessoaNom_Internalname = "CONTAGEM_FABRICASOFTWAREPESSOANOM";
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD";
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM";
         edtContagem_UsuarioAuditorCod_Internalname = "CONTAGEM_USUARIOAUDITORCOD";
         edtContagem_UsuarioAuditorPessoaCod_Internalname = "CONTAGEM_USUARIOAUDITORPESSOACOD";
         edtContagem_UsuarioAuditorPessoaNom_Internalname = "CONTAGEM_USUARIOAUDITORPESSOANOM";
         edtContagemItem_Sequencial_Internalname = "CONTAGEMITEM_SEQUENCIAL";
         edtContagemItem_Requisito_Internalname = "CONTAGEMITEM_REQUISITO";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB";
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE";
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemItem_Evidencias_Jsonclick = "";
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFB_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtContagemItem_Requisito_Jsonclick = "";
         edtContagemItem_Sequencial_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioAuditorCod_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaNom_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaCod_Jsonclick = "";
         edtContagem_FabricaSoftwareCod_Jsonclick = "";
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tecnica_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagem_datacriacao_to1_Jsonclick = "";
         edtavContagem_datacriacao1_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom1_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom1_Jsonclick = "";
         edtavContagemitem_sequencial1_Jsonclick = "";
         edtavContagem_fabricasoftwarecod1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagem_datacriacao_to2_Jsonclick = "";
         edtavContagem_datacriacao2_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom2_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom2_Jsonclick = "";
         edtavContagemitem_sequencial2_Jsonclick = "";
         edtavContagem_fabricasoftwarecod2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagem_datacriacao_to3_Jsonclick = "";
         edtavContagem_datacriacao3_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom3_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom3_Jsonclick = "";
         edtavContagemitem_sequencial3_Jsonclick = "";
         edtavContagem_fabricasoftwarecod3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagemItem_Evidencias_Titleformat = 0;
         cmbContagemItem_TipoUnidade_Titleformat = 0;
         cmbFuncaoAPF_Tipo_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         edtContagemItem_PFL_Titleformat = 0;
         edtContagemItem_PFB_Titleformat = 0;
         edtFuncaoAPF_Codigo_Titleformat = 0;
         edtContagemItem_Requisito_Titleformat = 0;
         edtContagemItem_Sequencial_Titleformat = 0;
         edtContagem_UsuarioAuditorPessoaNom_Titleformat = 0;
         edtContagem_UsuarioAuditorPessoaCod_Titleformat = 0;
         edtContagem_UsuarioAuditorCod_Titleformat = 0;
         edtContagem_UsuarioContadorPessoaNom_Titleformat = 0;
         edtContagem_UsuarioContadorPessoaCod_Titleformat = 0;
         edtContagem_UsuarioContadorCod_Titleformat = 0;
         edtContagem_FabricaSoftwarePessoaNom_Titleformat = 0;
         edtContagem_FabricaSoftwarePessoaCod_Titleformat = 0;
         edtContagem_FabricaSoftwareCod_Titleformat = 0;
         cmbContagem_Tipo_Titleformat = 0;
         cmbContagem_Tecnica_Titleformat = 0;
         edtContagem_AreaTrabalhoDes_Titleformat = 0;
         edtContagem_AreaTrabalhoCod_Titleformat = 0;
         edtContagem_DataCriacao_Titleformat = 0;
         edtContagem_Codigo_Titleformat = 0;
         edtContagemItem_Lancamento_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom3_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom3_Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         tblTablemergeddynamicfilterscontagem_datacriacao3_Visible = 1;
         edtavContagemitem_sequencial3_Visible = 1;
         edtavContagem_fabricasoftwarecod3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom2_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom2_Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         tblTablemergeddynamicfilterscontagem_datacriacao2_Visible = 1;
         edtavContagemitem_sequencial2_Visible = 1;
         edtavContagem_fabricasoftwarecod2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom1_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom1_Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         tblTablemergeddynamicfilterscontagem_datacriacao1_Visible = 1;
         edtavContagemitem_sequencial1_Visible = 1;
         edtavContagem_fabricasoftwarecod1_Visible = 1;
         edtContagemItem_Evidencias_Title = "Evidencias";
         cmbContagemItem_TipoUnidade.Title.Text = "Tipo";
         cmbFuncaoAPF_Tipo.Title.Text = "Tipo";
         edtFuncaoAPF_Nome_Title = "Fun��o de Transa��o";
         edtContagemItem_PFL_Title = "PFL";
         edtContagemItem_PFB_Title = "PFB";
         edtFuncaoAPF_Codigo_Title = "Fun��o de Transa��o";
         edtContagemItem_Requisito_Title = "de Software)";
         edtContagemItem_Sequencial_Title = "Sequencial";
         edtContagem_UsuarioAuditorPessoaNom_Title = "do Auditor";
         edtContagem_UsuarioAuditorPessoaCod_Title = "Pessoa Auditor";
         edtContagem_UsuarioAuditorCod_Title = "Usu�rio Auditor";
         edtContagem_UsuarioContadorPessoaNom_Title = "Nome do Contador";
         edtContagem_UsuarioContadorPessoaCod_Title = "C�d. Pessoa Contador";
         edtContagem_UsuarioContadorCod_Title = "C�d. Usu�rio Contador";
         edtContagem_FabricaSoftwarePessoaNom_Title = "F�brica )";
         edtContagem_FabricaSoftwarePessoaCod_Title = "Pessoa Contratada";
         edtContagem_FabricaSoftwareCod_Title = "Contratada F�brica";
         cmbContagem_Tipo.Title.Text = "Tipo";
         cmbContagem_Tecnica.Title.Text = "T�cnica";
         edtContagem_AreaTrabalhoDes_Title = "�rea de Trabalho";
         edtContagem_AreaTrabalhoCod_Title = "C�d. �rea de Trabalho";
         edtContagem_DataCriacao_Title = "Data Cria��o";
         edtContagem_Codigo_Title = "C�digo";
         edtContagemItem_Lancamento_Title = "Item do Lan�amento";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contagem Item";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemItem_Lancamento_Titleformat',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Titleformat'},{av:'edtContagemItem_Lancamento_Title',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Title'},{av:'edtContagem_Codigo_Titleformat',ctrl:'CONTAGEM_CODIGO',prop:'Titleformat'},{av:'edtContagem_Codigo_Title',ctrl:'CONTAGEM_CODIGO',prop:'Title'},{av:'edtContagem_DataCriacao_Titleformat',ctrl:'CONTAGEM_DATACRIACAO',prop:'Titleformat'},{av:'edtContagem_DataCriacao_Title',ctrl:'CONTAGEM_DATACRIACAO',prop:'Title'},{av:'edtContagem_AreaTrabalhoCod_Titleformat',ctrl:'CONTAGEM_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtContagem_AreaTrabalhoCod_Title',ctrl:'CONTAGEM_AREATRABALHOCOD',prop:'Title'},{av:'edtContagem_AreaTrabalhoDes_Titleformat',ctrl:'CONTAGEM_AREATRABALHODES',prop:'Titleformat'},{av:'edtContagem_AreaTrabalhoDes_Title',ctrl:'CONTAGEM_AREATRABALHODES',prop:'Title'},{av:'cmbContagem_Tecnica'},{av:'cmbContagem_Tipo'},{av:'edtContagem_FabricaSoftwareCod_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWARECOD',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwareCod_Title',ctrl:'CONTAGEM_FABRICASOFTWARECOD',prop:'Title'},{av:'edtContagem_FabricaSoftwarePessoaCod_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOACOD',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwarePessoaCod_Title',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOACOD',prop:'Title'},{av:'edtContagem_FabricaSoftwarePessoaNom_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOANOM',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwarePessoaNom_Title',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOANOM',prop:'Title'},{av:'edtContagem_UsuarioContadorCod_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORCOD',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorCod_Title',ctrl:'CONTAGEM_USUARIOCONTADORCOD',prop:'Title'},{av:'edtContagem_UsuarioContadorPessoaCod_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORPESSOACOD',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorPessoaCod_Title',ctrl:'CONTAGEM_USUARIOCONTADORPESSOACOD',prop:'Title'},{av:'edtContagem_UsuarioContadorPessoaNom_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORPESSOANOM',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorPessoaNom_Title',ctrl:'CONTAGEM_USUARIOCONTADORPESSOANOM',prop:'Title'},{av:'edtContagem_UsuarioAuditorCod_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORCOD',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorCod_Title',ctrl:'CONTAGEM_USUARIOAUDITORCOD',prop:'Title'},{av:'edtContagem_UsuarioAuditorPessoaCod_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORPESSOACOD',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorPessoaCod_Title',ctrl:'CONTAGEM_USUARIOAUDITORPESSOACOD',prop:'Title'},{av:'edtContagem_UsuarioAuditorPessoaNom_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORPESSOANOM',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorPessoaNom_Title',ctrl:'CONTAGEM_USUARIOAUDITORPESSOANOM',prop:'Title'},{av:'edtContagemItem_Sequencial_Titleformat',ctrl:'CONTAGEMITEM_SEQUENCIAL',prop:'Titleformat'},{av:'edtContagemItem_Sequencial_Title',ctrl:'CONTAGEMITEM_SEQUENCIAL',prop:'Title'},{av:'edtContagemItem_Requisito_Titleformat',ctrl:'CONTAGEMITEM_REQUISITO',prop:'Titleformat'},{av:'edtContagemItem_Requisito_Title',ctrl:'CONTAGEMITEM_REQUISITO',prop:'Title'},{av:'edtFuncaoAPF_Codigo_Titleformat',ctrl:'FUNCAOAPF_CODIGO',prop:'Titleformat'},{av:'edtFuncaoAPF_Codigo_Title',ctrl:'FUNCAOAPF_CODIGO',prop:'Title'},{av:'edtContagemItem_PFB_Titleformat',ctrl:'CONTAGEMITEM_PFB',prop:'Titleformat'},{av:'edtContagemItem_PFB_Title',ctrl:'CONTAGEMITEM_PFB',prop:'Title'},{av:'edtContagemItem_PFL_Titleformat',ctrl:'CONTAGEMITEM_PFL',prop:'Titleformat'},{av:'edtContagemItem_PFL_Title',ctrl:'CONTAGEMITEM_PFL',prop:'Title'},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'cmbContagemItem_TipoUnidade'},{av:'edtContagemItem_Evidencias_Titleformat',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Titleformat'},{av:'edtContagemItem_Evidencias_Title',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Title'},{av:'AV170GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV171GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24542',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25542',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A207Contagem_FabricaSoftwareCod',fld:'CONTAGEM_FABRICASOFTWARECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A225ContagemItem_Sequencial',fld:'CONTAGEMITEM_SEQUENCIAL',pic:'ZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContagemItem_Lancamento',fld:'vINOUTCONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'AV172InOutContagem_FabricaSoftwareCod',fld:'vINOUTCONTAGEM_FABRICASOFTWARECOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContagemItem_Sequencial',fld:'vINOUTCONTAGEMITEM_SEQUENCIAL',pic:'ZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavContagemitem_sequencial2_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavContagemitem_sequencial3_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavContagemitem_sequencial1_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18542',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavContagemitem_sequencial1_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavContagemitem_sequencial2_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavContagemitem_sequencial3_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavContagemitem_sequencial1_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20542',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavContagemitem_sequencial2_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavContagemitem_sequencial2_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavContagemitem_sequencial3_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavContagemitem_sequencial1_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21542',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavContagemitem_sequencial3_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16542',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV173Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavContagemitem_sequencial1_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV174Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV175Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV57ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV56ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV54Contagem_DataCriacao1',fld:'vCONTAGEM_DATACRIACAO1',pic:'',nv:''},{av:'AV55Contagem_DataCriacao_To1',fld:'vCONTAGEM_DATACRIACAO_TO1',pic:'',nv:''},{av:'AV17ContagemItem_Sequencial1',fld:'vCONTAGEMITEM_SEQUENCIAL1',pic:'ZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV61ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV58Contagem_DataCriacao2',fld:'vCONTAGEM_DATACRIACAO2',pic:'',nv:''},{av:'AV59Contagem_DataCriacao_To2',fld:'vCONTAGEM_DATACRIACAO_TO2',pic:'',nv:''},{av:'AV22ContagemItem_Sequencial2',fld:'vCONTAGEMITEM_SEQUENCIAL2',pic:'ZZ9',nv:0},{av:'AV65ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV64ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV62Contagem_DataCriacao3',fld:'vCONTAGEM_DATACRIACAO3',pic:'',nv:''},{av:'AV63Contagem_DataCriacao_To3',fld:'vCONTAGEM_DATACRIACAO_TO3',pic:'',nv:''},{av:'AV27ContagemItem_Sequencial3',fld:'vCONTAGEMITEM_SEQUENCIAL3',pic:'ZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavContagemitem_sequencial2_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavContagemitem_sequencial3_Visible',ctrl:'vCONTAGEMITEM_SEQUENCIAL3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagem_datacriacao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEM_DATACRIACAO3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV54Contagem_DataCriacao1 = DateTime.MinValue;
         AV55Contagem_DataCriacao_To1 = DateTime.MinValue;
         AV18FuncaoAPF_Nome1 = "";
         AV56ContagemItem_FSReferenciaTecnicaNom1 = "";
         AV57ContagemItem_FMReferenciaTecnicaNom1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV58Contagem_DataCriacao2 = DateTime.MinValue;
         AV59Contagem_DataCriacao_To2 = DateTime.MinValue;
         AV23FuncaoAPF_Nome2 = "";
         AV60ContagemItem_FSReferenciaTecnicaNom2 = "";
         AV61ContagemItem_FMReferenciaTecnicaNom2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV62Contagem_DataCriacao3 = DateTime.MinValue;
         AV63Contagem_DataCriacao_To3 = DateTime.MinValue;
         AV28FuncaoAPF_Nome3 = "";
         AV64ContagemItem_FSReferenciaTecnicaNom3 = "";
         AV65ContagemItem_FMReferenciaTecnicaNom3 = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV178Select_GXI = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A194Contagem_AreaTrabalhoDes = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A209Contagem_FabricaSoftwarePessoaNom = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         A218Contagem_UsuarioAuditorPessoaNom = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A952ContagemItem_TipoUnidade = "";
         A953ContagemItem_Evidencias = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18FuncaoAPF_Nome1 = "";
         lV56ContagemItem_FSReferenciaTecnicaNom1 = "";
         lV57ContagemItem_FMReferenciaTecnicaNom1 = "";
         lV23FuncaoAPF_Nome2 = "";
         lV60ContagemItem_FSReferenciaTecnicaNom2 = "";
         lV61ContagemItem_FMReferenciaTecnicaNom2 = "";
         lV28FuncaoAPF_Nome3 = "";
         lV64ContagemItem_FSReferenciaTecnicaNom3 = "";
         lV65ContagemItem_FMReferenciaTecnicaNom3 = "";
         A269ContagemItem_FSReferenciaTecnicaNom = "";
         A257ContagemItem_FMReferenciaTecnicaNom = "";
         H00542_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         H00542_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         H00542_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         H00542_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         H00542_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         H00542_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         H00542_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         H00542_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         H00542_A953ContagemItem_Evidencias = new String[] {""} ;
         H00542_n953ContagemItem_Evidencias = new bool[] {false} ;
         H00542_A952ContagemItem_TipoUnidade = new String[] {""} ;
         H00542_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00542_A166FuncaoAPF_Nome = new String[] {""} ;
         H00542_A951ContagemItem_PFL = new decimal[1] ;
         H00542_n951ContagemItem_PFL = new bool[] {false} ;
         H00542_A950ContagemItem_PFB = new decimal[1] ;
         H00542_n950ContagemItem_PFB = new bool[] {false} ;
         H00542_A165FuncaoAPF_Codigo = new int[1] ;
         H00542_n165FuncaoAPF_Codigo = new bool[] {false} ;
         H00542_A229ContagemItem_Requisito = new short[1] ;
         H00542_n229ContagemItem_Requisito = new bool[] {false} ;
         H00542_A225ContagemItem_Sequencial = new short[1] ;
         H00542_n225ContagemItem_Sequencial = new bool[] {false} ;
         H00542_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         H00542_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         H00542_A216Contagem_UsuarioAuditorCod = new int[1] ;
         H00542_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         H00542_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         H00542_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         H00542_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         H00542_A213Contagem_UsuarioContadorCod = new int[1] ;
         H00542_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         H00542_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         H00542_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         H00542_A207Contagem_FabricaSoftwareCod = new int[1] ;
         H00542_A196Contagem_Tipo = new String[] {""} ;
         H00542_n196Contagem_Tipo = new bool[] {false} ;
         H00542_A195Contagem_Tecnica = new String[] {""} ;
         H00542_n195Contagem_Tecnica = new bool[] {false} ;
         H00542_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         H00542_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         H00542_A193Contagem_AreaTrabalhoCod = new int[1] ;
         H00542_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00542_A192Contagem_Codigo = new int[1] ;
         H00542_A224ContagemItem_Lancamento = new int[1] ;
         H00543_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemitem__default(),
            new Object[][] {
                new Object[] {
               H00542_A226ContagemItem_FSReferenciaTecnicaCod, H00542_n226ContagemItem_FSReferenciaTecnicaCod, H00542_A255ContagemItem_FMReferenciaTecnicaCod, H00542_n255ContagemItem_FMReferenciaTecnicaCod, H00542_A257ContagemItem_FMReferenciaTecnicaNom, H00542_n257ContagemItem_FMReferenciaTecnicaNom, H00542_A269ContagemItem_FSReferenciaTecnicaNom, H00542_n269ContagemItem_FSReferenciaTecnicaNom, H00542_A953ContagemItem_Evidencias, H00542_n953ContagemItem_Evidencias,
               H00542_A952ContagemItem_TipoUnidade, H00542_A184FuncaoAPF_Tipo, H00542_A166FuncaoAPF_Nome, H00542_A951ContagemItem_PFL, H00542_n951ContagemItem_PFL, H00542_A950ContagemItem_PFB, H00542_n950ContagemItem_PFB, H00542_A165FuncaoAPF_Codigo, H00542_n165FuncaoAPF_Codigo, H00542_A229ContagemItem_Requisito,
               H00542_n229ContagemItem_Requisito, H00542_A225ContagemItem_Sequencial, H00542_n225ContagemItem_Sequencial, H00542_A218Contagem_UsuarioAuditorPessoaNom, H00542_A217Contagem_UsuarioAuditorPessoaCod, H00542_A216Contagem_UsuarioAuditorCod, H00542_A215Contagem_UsuarioContadorPessoaNom, H00542_n215Contagem_UsuarioContadorPessoaNom, H00542_A214Contagem_UsuarioContadorPessoaCod, H00542_n214Contagem_UsuarioContadorPessoaCod,
               H00542_A213Contagem_UsuarioContadorCod, H00542_n213Contagem_UsuarioContadorCod, H00542_A209Contagem_FabricaSoftwarePessoaNom, H00542_A208Contagem_FabricaSoftwarePessoaCod, H00542_A207Contagem_FabricaSoftwareCod, H00542_A196Contagem_Tipo, H00542_n196Contagem_Tipo, H00542_A195Contagem_Tecnica, H00542_n195Contagem_Tecnica, H00542_A194Contagem_AreaTrabalhoDes,
               H00542_n194Contagem_AreaTrabalhoDes, H00542_A193Contagem_AreaTrabalhoCod, H00542_A197Contagem_DataCriacao, H00542_A192Contagem_Codigo, H00542_A224ContagemItem_Lancamento
               }
               , new Object[] {
               H00543_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8InOutContagemItem_Sequencial ;
      private short wcpOAV8InOutContagemItem_Sequencial ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_116 ;
      private short nGXsfl_116_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV17ContagemItem_Sequencial1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV22ContagemItem_Sequencial2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV27ContagemItem_Sequencial3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A225ContagemItem_Sequencial ;
      private short A229ContagemItem_Requisito ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_116_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemItem_Lancamento_Titleformat ;
      private short edtContagem_Codigo_Titleformat ;
      private short edtContagem_DataCriacao_Titleformat ;
      private short edtContagem_AreaTrabalhoCod_Titleformat ;
      private short edtContagem_AreaTrabalhoDes_Titleformat ;
      private short cmbContagem_Tecnica_Titleformat ;
      private short cmbContagem_Tipo_Titleformat ;
      private short edtContagem_FabricaSoftwareCod_Titleformat ;
      private short edtContagem_FabricaSoftwarePessoaCod_Titleformat ;
      private short edtContagem_FabricaSoftwarePessoaNom_Titleformat ;
      private short edtContagem_UsuarioContadorCod_Titleformat ;
      private short edtContagem_UsuarioContadorPessoaCod_Titleformat ;
      private short edtContagem_UsuarioContadorPessoaNom_Titleformat ;
      private short edtContagem_UsuarioAuditorCod_Titleformat ;
      private short edtContagem_UsuarioAuditorPessoaCod_Titleformat ;
      private short edtContagem_UsuarioAuditorPessoaNom_Titleformat ;
      private short edtContagemItem_Sequencial_Titleformat ;
      private short edtContagemItem_Requisito_Titleformat ;
      private short edtFuncaoAPF_Codigo_Titleformat ;
      private short edtContagemItem_PFB_Titleformat ;
      private short edtContagemItem_PFL_Titleformat ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Tipo_Titleformat ;
      private short cmbContagemItem_TipoUnidade_Titleformat ;
      private short edtContagemItem_Evidencias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContagemItem_Lancamento ;
      private int AV172InOutContagem_FabricaSoftwareCod ;
      private int wcpOAV7InOutContagemItem_Lancamento ;
      private int wcpOAV172InOutContagem_FabricaSoftwareCod ;
      private int subGrid_Rows ;
      private int AV173Contagem_FabricaSoftwareCod1 ;
      private int AV174Contagem_FabricaSoftwareCod2 ;
      private int AV175Contagem_FabricaSoftwareCod3 ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A224ContagemItem_Lancamento ;
      private int A192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A207Contagem_FabricaSoftwareCod ;
      private int A208Contagem_FabricaSoftwarePessoaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int A216Contagem_UsuarioAuditorCod ;
      private int A217Contagem_UsuarioAuditorPessoaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A226ContagemItem_FSReferenciaTecnicaCod ;
      private int A255ContagemItem_FMReferenciaTecnicaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV169PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagem_fabricasoftwarecod1_Visible ;
      private int edtavContagemitem_sequencial1_Visible ;
      private int tblTablemergeddynamicfilterscontagem_datacriacao1_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom1_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom1_Visible ;
      private int edtavContagem_fabricasoftwarecod2_Visible ;
      private int edtavContagemitem_sequencial2_Visible ;
      private int tblTablemergeddynamicfilterscontagem_datacriacao2_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom2_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom2_Visible ;
      private int edtavContagem_fabricasoftwarecod3_Visible ;
      private int edtavContagemitem_sequencial3_Visible ;
      private int tblTablemergeddynamicfilterscontagem_datacriacao3_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom3_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV170GridCurrentPage ;
      private long AV171GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_116_idx="0001" ;
      private String AV56ContagemItem_FSReferenciaTecnicaNom1 ;
      private String AV57ContagemItem_FMReferenciaTecnicaNom1 ;
      private String AV60ContagemItem_FSReferenciaTecnicaNom2 ;
      private String AV61ContagemItem_FMReferenciaTecnicaNom2 ;
      private String AV64ContagemItem_FSReferenciaTecnicaNom3 ;
      private String AV65ContagemItem_FMReferenciaTecnicaNom3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String cmbContagem_Tecnica_Internalname ;
      private String A195Contagem_Tecnica ;
      private String cmbContagem_Tipo_Internalname ;
      private String A196Contagem_Tipo ;
      private String edtContagem_FabricaSoftwareCod_Internalname ;
      private String edtContagem_FabricaSoftwarePessoaCod_Internalname ;
      private String A209Contagem_FabricaSoftwarePessoaNom ;
      private String edtContagem_FabricaSoftwarePessoaNom_Internalname ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String edtContagem_UsuarioAuditorCod_Internalname ;
      private String edtContagem_UsuarioAuditorPessoaCod_Internalname ;
      private String A218Contagem_UsuarioAuditorPessoaNom ;
      private String edtContagem_UsuarioAuditorPessoaNom_Internalname ;
      private String edtContagemItem_Sequencial_Internalname ;
      private String edtContagemItem_Requisito_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtContagemItem_PFB_Internalname ;
      private String edtContagemItem_PFL_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String A184FuncaoAPF_Tipo ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV56ContagemItem_FSReferenciaTecnicaNom1 ;
      private String lV57ContagemItem_FMReferenciaTecnicaNom1 ;
      private String lV60ContagemItem_FSReferenciaTecnicaNom2 ;
      private String lV61ContagemItem_FMReferenciaTecnicaNom2 ;
      private String lV64ContagemItem_FSReferenciaTecnicaNom3 ;
      private String lV65ContagemItem_FMReferenciaTecnicaNom3 ;
      private String A269ContagemItem_FSReferenciaTecnicaNom ;
      private String A257ContagemItem_FMReferenciaTecnicaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagem_fabricasoftwarecod1_Internalname ;
      private String edtavContagemitem_sequencial1_Internalname ;
      private String edtavContagem_datacriacao1_Internalname ;
      private String edtavContagem_datacriacao_to1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom1_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagem_fabricasoftwarecod2_Internalname ;
      private String edtavContagemitem_sequencial2_Internalname ;
      private String edtavContagem_datacriacao2_Internalname ;
      private String edtavContagem_datacriacao_to2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom2_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagem_fabricasoftwarecod3_Internalname ;
      private String edtavContagemitem_sequencial3_Internalname ;
      private String edtavContagem_datacriacao3_Internalname ;
      private String edtavContagem_datacriacao_to3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom3_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemItem_Lancamento_Title ;
      private String edtContagem_Codigo_Title ;
      private String edtContagem_DataCriacao_Title ;
      private String edtContagem_AreaTrabalhoCod_Title ;
      private String edtContagem_AreaTrabalhoDes_Title ;
      private String edtContagem_FabricaSoftwareCod_Title ;
      private String edtContagem_FabricaSoftwarePessoaCod_Title ;
      private String edtContagem_FabricaSoftwarePessoaNom_Title ;
      private String edtContagem_UsuarioContadorCod_Title ;
      private String edtContagem_UsuarioContadorPessoaCod_Title ;
      private String edtContagem_UsuarioContadorPessoaNom_Title ;
      private String edtContagem_UsuarioAuditorCod_Title ;
      private String edtContagem_UsuarioAuditorPessoaCod_Title ;
      private String edtContagem_UsuarioAuditorPessoaNom_Title ;
      private String edtContagemItem_Sequencial_Title ;
      private String edtContagemItem_Requisito_Title ;
      private String edtFuncaoAPF_Codigo_Title ;
      private String edtContagemItem_PFB_Title ;
      private String edtContagemItem_PFL_Title ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtContagemItem_Evidencias_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontagem_datacriacao1_Internalname ;
      private String tblTablemergeddynamicfilterscontagem_datacriacao2_Internalname ;
      private String tblTablemergeddynamicfilterscontagem_datacriacao3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod3_Jsonclick ;
      private String edtavContagemitem_sequencial3_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom3_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom3_Jsonclick ;
      private String edtavContagem_datacriacao3_Jsonclick ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext3_Jsonclick ;
      private String edtavContagem_datacriacao_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod2_Jsonclick ;
      private String edtavContagemitem_sequencial2_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom2_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom2_Jsonclick ;
      private String edtavContagem_datacriacao2_Jsonclick ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext2_Jsonclick ;
      private String edtavContagem_datacriacao_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod1_Jsonclick ;
      private String edtavContagemitem_sequencial1_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom1_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom1_Jsonclick ;
      private String edtavContagem_datacriacao1_Jsonclick ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagem_datacriacao_rangemiddletext1_Jsonclick ;
      private String edtavContagem_datacriacao_to1_Jsonclick ;
      private String sGXsfl_116_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String edtContagem_Codigo_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String edtContagem_FabricaSoftwareCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaNom_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String edtContagem_UsuarioAuditorCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaNom_Jsonclick ;
      private String edtContagemItem_Sequencial_Jsonclick ;
      private String edtContagemItem_Requisito_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String edtContagemItem_Evidencias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV54Contagem_DataCriacao1 ;
      private DateTime AV55Contagem_DataCriacao_To1 ;
      private DateTime AV58Contagem_DataCriacao2 ;
      private DateTime AV59Contagem_DataCriacao_To2 ;
      private DateTime AV62Contagem_DataCriacao3 ;
      private DateTime AV63Contagem_DataCriacao_To3 ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n225ContagemItem_Sequencial ;
      private bool n229ContagemItem_Requisito ;
      private bool n165FuncaoAPF_Codigo ;
      private bool n950ContagemItem_PFB ;
      private bool n951ContagemItem_PFL ;
      private bool n953ContagemItem_Evidencias ;
      private bool n226ContagemItem_FSReferenciaTecnicaCod ;
      private bool n255ContagemItem_FMReferenciaTecnicaCod ;
      private bool n257ContagemItem_FMReferenciaTecnicaNom ;
      private bool n269ContagemItem_FSReferenciaTecnicaNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String A953ContagemItem_Evidencias ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18FuncaoAPF_Nome1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV23FuncaoAPF_Nome2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV28FuncaoAPF_Nome3 ;
      private String AV178Select_GXI ;
      private String A194Contagem_AreaTrabalhoDes ;
      private String A166FuncaoAPF_Nome ;
      private String A952ContagemItem_TipoUnidade ;
      private String lV18FuncaoAPF_Nome1 ;
      private String lV23FuncaoAPF_Nome2 ;
      private String lV28FuncaoAPF_Nome3 ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagemItem_Lancamento ;
      private int aP1_InOutContagem_FabricaSoftwareCod ;
      private short aP2_InOutContagemItem_Sequencial ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00542_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] H00542_n226ContagemItem_FSReferenciaTecnicaCod ;
      private int[] H00542_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] H00542_n255ContagemItem_FMReferenciaTecnicaCod ;
      private String[] H00542_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] H00542_n257ContagemItem_FMReferenciaTecnicaNom ;
      private String[] H00542_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] H00542_n269ContagemItem_FSReferenciaTecnicaNom ;
      private String[] H00542_A953ContagemItem_Evidencias ;
      private bool[] H00542_n953ContagemItem_Evidencias ;
      private String[] H00542_A952ContagemItem_TipoUnidade ;
      private String[] H00542_A184FuncaoAPF_Tipo ;
      private String[] H00542_A166FuncaoAPF_Nome ;
      private decimal[] H00542_A951ContagemItem_PFL ;
      private bool[] H00542_n951ContagemItem_PFL ;
      private decimal[] H00542_A950ContagemItem_PFB ;
      private bool[] H00542_n950ContagemItem_PFB ;
      private int[] H00542_A165FuncaoAPF_Codigo ;
      private bool[] H00542_n165FuncaoAPF_Codigo ;
      private short[] H00542_A229ContagemItem_Requisito ;
      private bool[] H00542_n229ContagemItem_Requisito ;
      private short[] H00542_A225ContagemItem_Sequencial ;
      private bool[] H00542_n225ContagemItem_Sequencial ;
      private String[] H00542_A218Contagem_UsuarioAuditorPessoaNom ;
      private int[] H00542_A217Contagem_UsuarioAuditorPessoaCod ;
      private int[] H00542_A216Contagem_UsuarioAuditorCod ;
      private String[] H00542_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] H00542_n215Contagem_UsuarioContadorPessoaNom ;
      private int[] H00542_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] H00542_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] H00542_A213Contagem_UsuarioContadorCod ;
      private bool[] H00542_n213Contagem_UsuarioContadorCod ;
      private String[] H00542_A209Contagem_FabricaSoftwarePessoaNom ;
      private int[] H00542_A208Contagem_FabricaSoftwarePessoaCod ;
      private int[] H00542_A207Contagem_FabricaSoftwareCod ;
      private String[] H00542_A196Contagem_Tipo ;
      private bool[] H00542_n196Contagem_Tipo ;
      private String[] H00542_A195Contagem_Tecnica ;
      private bool[] H00542_n195Contagem_Tecnica ;
      private String[] H00542_A194Contagem_AreaTrabalhoDes ;
      private bool[] H00542_n194Contagem_AreaTrabalhoDes ;
      private int[] H00542_A193Contagem_AreaTrabalhoCod ;
      private DateTime[] H00542_A197Contagem_DataCriacao ;
      private int[] H00542_A192Contagem_Codigo ;
      private int[] H00542_A224ContagemItem_Lancamento ;
      private long[] H00543_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class promptcontagemitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00542( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV173Contagem_FabricaSoftwareCod1 ,
                                             short AV17ContagemItem_Sequencial1 ,
                                             DateTime AV54Contagem_DataCriacao1 ,
                                             DateTime AV55Contagem_DataCriacao_To1 ,
                                             String AV18FuncaoAPF_Nome1 ,
                                             String AV56ContagemItem_FSReferenciaTecnicaNom1 ,
                                             String AV57ContagemItem_FMReferenciaTecnicaNom1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             int AV174Contagem_FabricaSoftwareCod2 ,
                                             short AV22ContagemItem_Sequencial2 ,
                                             DateTime AV58Contagem_DataCriacao2 ,
                                             DateTime AV59Contagem_DataCriacao_To2 ,
                                             String AV23FuncaoAPF_Nome2 ,
                                             String AV60ContagemItem_FSReferenciaTecnicaNom2 ,
                                             String AV61ContagemItem_FMReferenciaTecnicaNom2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             int AV175Contagem_FabricaSoftwareCod3 ,
                                             short AV27ContagemItem_Sequencial3 ,
                                             DateTime AV62Contagem_DataCriacao3 ,
                                             DateTime AV63Contagem_DataCriacao_To3 ,
                                             String AV28FuncaoAPF_Nome3 ,
                                             String AV64ContagemItem_FSReferenciaTecnicaNom3 ,
                                             String AV65ContagemItem_FMReferenciaTecnicaNom3 ,
                                             int A207Contagem_FabricaSoftwareCod ,
                                             short A225ContagemItem_Sequencial ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A269ContagemItem_FSReferenciaTecnicaNom ,
                                             String A257ContagemItem_FMReferenciaTecnicaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [47] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, T1.[ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni, T3.[ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, T2.[ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, T1.[ContagemItem_Evidencias], T1.[ContagemItem_TipoUnidade], T4.[FuncaoAPF_Tipo], T4.[FuncaoAPF_Nome], T1.[ContagemItem_PFL], T1.[ContagemItem_PFB], T1.[FuncaoAPF_Codigo], T1.[ContagemItem_Requisito], T1.[ContagemItem_Sequencial], T1.[Contagem_UsuarioAuditorPessoaNom], T1.[Contagem_UsuarioAuditorPessoaCod], T1.[Contagem_UsuarioAuditorCod], T7.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T6.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T5.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_FabricaSoftwarePessoaNom], T1.[Contagem_FabricaSoftwarePessoaCod], T1.[Contagem_FabricaSoftwareCod], T5.[Contagem_Tipo], T5.[Contagem_Tecnica], T8.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T5.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T5.[Contagem_DataCriacao], T1.[Contagem_Codigo], T1.[ContagemItem_Lancamento]";
         sFromString = " FROM ((((((([ContagemItem] T1 WITH (NOLOCK) LEFT JOIN [ReferenciaTecnica] T2 WITH (NOLOCK) ON T2.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T3 WITH (NOLOCK) ON T3.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FMReferenciaTecnicaCod]) LEFT JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) INNER JOIN [Contagem] T5 WITH (NOLOCK) ON T5.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN [Usuario] T6 WITH (NOLOCK) ON T6.[Usuario_Codigo] = T5.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T8 WITH (NOLOCK) ON T8.[AreaTrabalho_Codigo] = T5.[Contagem_AreaTrabalhoCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV54Contagem_DataCriacao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] >= @AV54Contagem_DataCriacao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] >= @AV54Contagem_DataCriacao1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV55Contagem_DataCriacao_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] <= @AV55Contagem_DataCriacao_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] <= @AV55Contagem_DataCriacao_To1)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like '%' + @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like '%' + @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV58Contagem_DataCriacao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] >= @AV58Contagem_DataCriacao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] >= @AV58Contagem_DataCriacao2)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV59Contagem_DataCriacao_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] <= @AV59Contagem_DataCriacao_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] <= @AV59Contagem_DataCriacao_To2)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 2 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 2 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV62Contagem_DataCriacao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] >= @AV62Contagem_DataCriacao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] >= @AV62Contagem_DataCriacao3)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV63Contagem_DataCriacao_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Contagem_DataCriacao] <= @AV63Contagem_DataCriacao_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Contagem_DataCriacao] <= @AV63Contagem_DataCriacao_To3)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaTecnica_Nome] like '%' + @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwareCod]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwareCod] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Sequencial]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Sequencial] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_DataCriacao]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_DataCriacao] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T8.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T8.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_Tecnica]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_Tecnica] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_Tipo]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwarePessoaCod]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwarePessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwarePessoaNom]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_FabricaSoftwarePessoaNom] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_UsuarioContadorCod]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Contagem_UsuarioContadorCod] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T6.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T7.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T7.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorCod]";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorCod] DESC";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorPessoaCod]";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorPessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorPessoaNom]";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioAuditorPessoaNom] DESC";
         }
         else if ( ( AV13OrderedBy == 18 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Requisito]";
         }
         else if ( ( AV13OrderedBy == 18 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Requisito] DESC";
         }
         else if ( ( AV13OrderedBy == 19 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo]";
         }
         else if ( ( AV13OrderedBy == 19 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 20 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_PFB]";
         }
         else if ( ( AV13OrderedBy == 20 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_PFB] DESC";
         }
         else if ( ( AV13OrderedBy == 21 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_PFL]";
         }
         else if ( ( AV13OrderedBy == 21 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_PFL] DESC";
         }
         else if ( ( AV13OrderedBy == 22 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 22 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 23 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Tipo]";
         }
         else if ( ( AV13OrderedBy == 23 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 24 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_TipoUnidade]";
         }
         else if ( ( AV13OrderedBy == 24 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_TipoUnidade] DESC";
         }
         else if ( ( AV13OrderedBy == 25 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Evidencias]";
         }
         else if ( ( AV13OrderedBy == 25 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Evidencias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00543( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV173Contagem_FabricaSoftwareCod1 ,
                                             short AV17ContagemItem_Sequencial1 ,
                                             DateTime AV54Contagem_DataCriacao1 ,
                                             DateTime AV55Contagem_DataCriacao_To1 ,
                                             String AV18FuncaoAPF_Nome1 ,
                                             String AV56ContagemItem_FSReferenciaTecnicaNom1 ,
                                             String AV57ContagemItem_FMReferenciaTecnicaNom1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             int AV174Contagem_FabricaSoftwareCod2 ,
                                             short AV22ContagemItem_Sequencial2 ,
                                             DateTime AV58Contagem_DataCriacao2 ,
                                             DateTime AV59Contagem_DataCriacao_To2 ,
                                             String AV23FuncaoAPF_Nome2 ,
                                             String AV60ContagemItem_FSReferenciaTecnicaNom2 ,
                                             String AV61ContagemItem_FMReferenciaTecnicaNom2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             int AV175Contagem_FabricaSoftwareCod3 ,
                                             short AV27ContagemItem_Sequencial3 ,
                                             DateTime AV62Contagem_DataCriacao3 ,
                                             DateTime AV63Contagem_DataCriacao_To3 ,
                                             String AV28FuncaoAPF_Nome3 ,
                                             String AV64ContagemItem_FSReferenciaTecnicaNom3 ,
                                             String AV65ContagemItem_FMReferenciaTecnicaNom3 ,
                                             int A207Contagem_FabricaSoftwareCod ,
                                             short A225ContagemItem_Sequencial ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A269ContagemItem_FSReferenciaTecnicaNom ,
                                             String A257ContagemItem_FMReferenciaTecnicaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [42] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((((([ContagemItem] T1 WITH (NOLOCK) LEFT JOIN [ReferenciaTecnica] T7 WITH (NOLOCK) ON T7.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T8 WITH (NOLOCK) ON T8.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FMReferenciaTecnicaCod]) LEFT JOIN [FuncoesAPF] T6 WITH (NOLOCK) ON T6.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T2.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV173Contagem_FabricaSoftwareCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV173Contagem_FabricaSoftwareCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV173Contagem_FabricaSoftwareCod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17ContagemItem_Sequencial1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV17ContagemItem_Sequencial1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV17ContagemItem_Sequencial1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV54Contagem_DataCriacao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] >= @AV54Contagem_DataCriacao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] >= @AV54Contagem_DataCriacao1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV55Contagem_DataCriacao_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] <= @AV55Contagem_DataCriacao_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] <= @AV55Contagem_DataCriacao_To1)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like '%' + @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like '%' + @lV56ContagemItem_FSReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like '%' + @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like '%' + @lV57ContagemItem_FMReferenciaTecnicaNom1)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV174Contagem_FabricaSoftwareCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV174Contagem_FabricaSoftwareCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV174Contagem_FabricaSoftwareCod2)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (0==AV22ContagemItem_Sequencial2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV22ContagemItem_Sequencial2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV22ContagemItem_Sequencial2)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV58Contagem_DataCriacao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] >= @AV58Contagem_DataCriacao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] >= @AV58Contagem_DataCriacao2)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV59Contagem_DataCriacao_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] <= @AV59Contagem_DataCriacao_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] <= @AV59Contagem_DataCriacao_To2)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom2)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] < @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] = @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( AV26DynamicFiltersOperator3 == 2 ) && ( ! (0==AV175Contagem_FabricaSoftwareCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV175Contagem_FabricaSoftwareCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_FabricaSoftwareCod] > @AV175Contagem_FabricaSoftwareCod3)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] < @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] < @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] = @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] = @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_SEQUENCIAL") == 0 ) && ( AV26DynamicFiltersOperator3 == 2 ) && ( ! (0==AV27ContagemItem_Sequencial3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemItem_Sequencial] > @AV27ContagemItem_Sequencial3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemItem_Sequencial] > @AV27ContagemItem_Sequencial3)";
            }
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV62Contagem_DataCriacao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] >= @AV62Contagem_DataCriacao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] >= @AV62Contagem_DataCriacao3)";
            }
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEM_DATACRIACAO") == 0 ) && ( ! (DateTime.MinValue==AV63Contagem_DataCriacao_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contagem_DataCriacao] <= @AV63Contagem_DataCriacao_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contagem_DataCriacao] <= @AV63Contagem_DataCriacao_To3)";
            }
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T7.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T7.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FSReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T8.[ReferenciaTecnica_Nome] like '%' + @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T8.[ReferenciaTecnica_Nome] like '%' + @lV65ContagemItem_FMReferenciaTecnicaNom3)";
            }
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 18 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 18 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 19 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 19 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 20 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 20 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 21 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 21 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 22 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 22 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 23 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 23 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 24 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 24 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 25 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 25 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00542(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (short)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (DateTime)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H00543(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (short)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (short)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (DateTime)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00542 ;
          prmH00542 = new Object[] {
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV54Contagem_DataCriacao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55Contagem_DataCriacao_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV58Contagem_DataCriacao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59Contagem_DataCriacao_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV62Contagem_DataCriacao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63Contagem_DataCriacao_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00543 ;
          prmH00543 = new Object[] {
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV17ContagemItem_Sequencial1",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV54Contagem_DataCriacao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55Contagem_DataCriacao_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV56ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV174Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV22ContagemItem_Sequencial2",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV58Contagem_DataCriacao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59Contagem_DataCriacao_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV175Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV27ContagemItem_Sequencial3",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV62Contagem_DataCriacao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63Contagem_DataCriacao_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV64ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00542", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00542,11,0,true,false )
             ,new CursorDef("H00543", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00543,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 3) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 50) ;
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((int[]) buf[25])[0] = rslt.getInt(16) ;
                ((String[]) buf[26])[0] = rslt.getString(17, 100) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((int[]) buf[28])[0] = rslt.getInt(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((String[]) buf[32])[0] = rslt.getString(20, 50) ;
                ((int[]) buf[33])[0] = rslt.getInt(21) ;
                ((int[]) buf[34])[0] = rslt.getInt(22) ;
                ((String[]) buf[35])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((String[]) buf[37])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((String[]) buf[39])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((int[]) buf[41])[0] = rslt.getInt(26) ;
                ((DateTime[]) buf[42])[0] = rslt.getGXDate(27) ;
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                ((int[]) buf[44])[0] = rslt.getInt(29) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[64]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[65]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[66]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[78]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[79]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[80]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[81]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[82]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[89]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[90]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[91]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[92]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[93]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[47]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[60]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[61]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[73]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[74]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[75]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[76]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                return;
       }
    }

 }

}
