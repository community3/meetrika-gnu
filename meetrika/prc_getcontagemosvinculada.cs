/*
               File: PRC_GETContagemOSVinculada
        Description: GET Contagem OS Vinculada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:17.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getcontagemosvinculada : GXProcedure
   {
      public prc_getcontagemosvinculada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getcontagemosvinculada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out decimal aP1_ContagemResultado_PFBFS ,
                           out decimal aP2_ContagemResultado_PFLFS ,
                           out decimal aP3_ContagemResultado_PFBFM ,
                           out decimal aP4_ContagemResultado_PFLFM ,
                           out DateTime aP5_ContagemResultado_DataCnt ,
                           out int aP6_ContagemResultado_ContadorFMCod )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_PFBFS = 0 ;
         this.AV10ContagemResultado_PFLFS = 0 ;
         this.AV12ContagemResultado_PFBFM = 0 ;
         this.AV11ContagemResultado_PFLFM = 0 ;
         this.AV13ContagemResultado_DataCnt = DateTime.MinValue ;
         this.AV9ContagemResultado_ContadorFMCod = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_PFBFS=this.AV8ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV10ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV12ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV11ContagemResultado_PFLFM;
         aP5_ContagemResultado_DataCnt=this.AV13ContagemResultado_DataCnt;
         aP6_ContagemResultado_ContadorFMCod=this.AV9ContagemResultado_ContadorFMCod;
      }

      public int executeUdp( ref int aP0_ContagemResultado_Codigo ,
                             out decimal aP1_ContagemResultado_PFBFS ,
                             out decimal aP2_ContagemResultado_PFLFS ,
                             out decimal aP3_ContagemResultado_PFBFM ,
                             out decimal aP4_ContagemResultado_PFLFM ,
                             out DateTime aP5_ContagemResultado_DataCnt )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_PFBFS = 0 ;
         this.AV10ContagemResultado_PFLFS = 0 ;
         this.AV12ContagemResultado_PFBFM = 0 ;
         this.AV11ContagemResultado_PFLFM = 0 ;
         this.AV13ContagemResultado_DataCnt = DateTime.MinValue ;
         this.AV9ContagemResultado_ContadorFMCod = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_PFBFS=this.AV8ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV10ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV12ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV11ContagemResultado_PFLFM;
         aP5_ContagemResultado_DataCnt=this.AV13ContagemResultado_DataCnt;
         aP6_ContagemResultado_ContadorFMCod=this.AV9ContagemResultado_ContadorFMCod;
         return AV9ContagemResultado_ContadorFMCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out decimal aP1_ContagemResultado_PFBFS ,
                                 out decimal aP2_ContagemResultado_PFLFS ,
                                 out decimal aP3_ContagemResultado_PFBFM ,
                                 out decimal aP4_ContagemResultado_PFLFM ,
                                 out DateTime aP5_ContagemResultado_DataCnt ,
                                 out int aP6_ContagemResultado_ContadorFMCod )
      {
         prc_getcontagemosvinculada objprc_getcontagemosvinculada;
         objprc_getcontagemosvinculada = new prc_getcontagemosvinculada();
         objprc_getcontagemosvinculada.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_getcontagemosvinculada.AV8ContagemResultado_PFBFS = 0 ;
         objprc_getcontagemosvinculada.AV10ContagemResultado_PFLFS = 0 ;
         objprc_getcontagemosvinculada.AV12ContagemResultado_PFBFM = 0 ;
         objprc_getcontagemosvinculada.AV11ContagemResultado_PFLFM = 0 ;
         objprc_getcontagemosvinculada.AV13ContagemResultado_DataCnt = DateTime.MinValue ;
         objprc_getcontagemosvinculada.AV9ContagemResultado_ContadorFMCod = 0 ;
         objprc_getcontagemosvinculada.context.SetSubmitInitialConfig(context);
         objprc_getcontagemosvinculada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getcontagemosvinculada);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_PFBFS=this.AV8ContagemResultado_PFBFS;
         aP2_ContagemResultado_PFLFS=this.AV10ContagemResultado_PFLFS;
         aP3_ContagemResultado_PFBFM=this.AV12ContagemResultado_PFBFM;
         aP4_ContagemResultado_PFLFM=this.AV11ContagemResultado_PFLFM;
         aP5_ContagemResultado_DataCnt=this.AV13ContagemResultado_DataCnt;
         aP6_ContagemResultado_ContadorFMCod=this.AV9ContagemResultado_ContadorFMCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getcontagemosvinculada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004L2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A517ContagemResultado_Ultima = P004L2_A517ContagemResultado_Ultima[0];
            A458ContagemResultado_PFBFS = P004L2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P004L2_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = P004L2_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P004L2_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = P004L2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P004L2_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = P004L2_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P004L2_n461ContagemResultado_PFLFM[0];
            A473ContagemResultado_DataCnt = P004L2_A473ContagemResultado_DataCnt[0];
            A470ContagemResultado_ContadorFMCod = P004L2_A470ContagemResultado_ContadorFMCod[0];
            A511ContagemResultado_HoraCnt = P004L2_A511ContagemResultado_HoraCnt[0];
            AV8ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            AV10ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            AV12ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            AV11ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            AV13ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            AV9ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004L2_A456ContagemResultado_Codigo = new int[1] ;
         P004L2_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004L2_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004L2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004L2_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004L2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004L2_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004L2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004L2_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004L2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004L2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004L2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004L2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getcontagemosvinculada__default(),
            new Object[][] {
                new Object[] {
               P004L2_A456ContagemResultado_Codigo, P004L2_A517ContagemResultado_Ultima, P004L2_A458ContagemResultado_PFBFS, P004L2_n458ContagemResultado_PFBFS, P004L2_A459ContagemResultado_PFLFS, P004L2_n459ContagemResultado_PFLFS, P004L2_A460ContagemResultado_PFBFM, P004L2_n460ContagemResultado_PFBFM, P004L2_A461ContagemResultado_PFLFM, P004L2_n461ContagemResultado_PFLFM,
               P004L2_A473ContagemResultado_DataCnt, P004L2_A470ContagemResultado_ContadorFMCod, P004L2_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV9ContagemResultado_ContadorFMCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal AV8ContagemResultado_PFBFS ;
      private decimal AV10ContagemResultado_PFLFS ;
      private decimal AV12ContagemResultado_PFBFM ;
      private decimal AV11ContagemResultado_PFLFM ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV13ContagemResultado_DataCnt ;
      private bool A517ContagemResultado_Ultima ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004L2_A456ContagemResultado_Codigo ;
      private bool[] P004L2_A517ContagemResultado_Ultima ;
      private decimal[] P004L2_A458ContagemResultado_PFBFS ;
      private bool[] P004L2_n458ContagemResultado_PFBFS ;
      private decimal[] P004L2_A459ContagemResultado_PFLFS ;
      private bool[] P004L2_n459ContagemResultado_PFLFS ;
      private decimal[] P004L2_A460ContagemResultado_PFBFM ;
      private bool[] P004L2_n460ContagemResultado_PFBFM ;
      private decimal[] P004L2_A461ContagemResultado_PFLFM ;
      private bool[] P004L2_n461ContagemResultado_PFLFM ;
      private DateTime[] P004L2_A473ContagemResultado_DataCnt ;
      private int[] P004L2_A470ContagemResultado_ContadorFMCod ;
      private String[] P004L2_A511ContagemResultado_HoraCnt ;
      private decimal aP1_ContagemResultado_PFBFS ;
      private decimal aP2_ContagemResultado_PFLFS ;
      private decimal aP3_ContagemResultado_PFBFM ;
      private decimal aP4_ContagemResultado_PFLFM ;
      private DateTime aP5_ContagemResultado_DataCnt ;
      private int aP6_ContagemResultado_ContadorFMCod ;
   }

   public class prc_getcontagemosvinculada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004L2 ;
          prmP004L2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004L2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_DataCnt], [ContagemResultado_ContadorFMCod], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004L2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
