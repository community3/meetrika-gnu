/*
               File: GetPromptContratoClausulasFilterData
        Description: Get Prompt Contrato Clausulas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:33.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoclausulasfilterdata : GXProcedure
   {
      public getpromptcontratoclausulasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoclausulasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoclausulasfilterdata objgetpromptcontratoclausulasfilterdata;
         objgetpromptcontratoclausulasfilterdata = new getpromptcontratoclausulasfilterdata();
         objgetpromptcontratoclausulasfilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptcontratoclausulasfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoclausulasfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoclausulasfilterdata.AV22OptionsJson = "" ;
         objgetpromptcontratoclausulasfilterdata.AV25OptionsDescJson = "" ;
         objgetpromptcontratoclausulasfilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptcontratoclausulasfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoclausulasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoclausulasfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoclausulasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOCLAUSULAS_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOCLAUSULAS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptContratoClausulasGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoClausulasGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptContratoClausulasGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM") == 0 )
            {
               AV12TFContratoClausulas_Item = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM_SEL") == 0 )
            {
               AV13TFContratoClausulas_Item_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO") == 0 )
            {
               AV14TFContratoClausulas_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoClausulas_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36ContratoClausulas_Item1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 )
               {
                  AV39DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV40ContratoClausulas_Item2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV44ContratoClausulas_Item3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV16SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36ContratoClausulas_Item1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39DynamicFiltersOperator2 ,
                                              AV40ContratoClausulas_Item2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44ContratoClausulas_Item3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContratoClausulas_Item_Sel ,
                                              AV12TFContratoClausulas_Item ,
                                              AV15TFContratoClausulas_Descricao_Sel ,
                                              AV14TFContratoClausulas_Descricao ,
                                              A153ContratoClausulas_Item ,
                                              A77Contrato_Numero ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContratoClausulas_Item = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoClausulas_Item), 10, "%");
         lV14TFContratoClausulas_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoClausulas_Descricao), "%", "");
         /* Using cursor P00UK2 */
         pr_default.execute(0, new Object[] {lV36ContratoClausulas_Item1, lV36ContratoClausulas_Item1, lV40ContratoClausulas_Item2, lV40ContratoClausulas_Item2, lV44ContratoClausulas_Item3, lV44ContratoClausulas_Item3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContratoClausulas_Item, AV13TFContratoClausulas_Item_Sel, lV14TFContratoClausulas_Descricao, AV15TFContratoClausulas_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUK2 = false;
            A74Contrato_Codigo = P00UK2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00UK2_A77Contrato_Numero[0];
            A154ContratoClausulas_Descricao = P00UK2_A154ContratoClausulas_Descricao[0];
            A153ContratoClausulas_Item = P00UK2_A153ContratoClausulas_Item[0];
            A152ContratoClausulas_Codigo = P00UK2_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00UK2_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UK2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKUK2 = false;
               A74Contrato_Codigo = P00UK2_A74Contrato_Codigo[0];
               A152ContratoClausulas_Codigo = P00UK2_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUK2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV20Option = A77Contrato_Numero;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUK2 )
            {
               BRKUK2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' Routine */
         AV12TFContratoClausulas_Item = AV16SearchTxt;
         AV13TFContratoClausulas_Item_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36ContratoClausulas_Item1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39DynamicFiltersOperator2 ,
                                              AV40ContratoClausulas_Item2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44ContratoClausulas_Item3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContratoClausulas_Item_Sel ,
                                              AV12TFContratoClausulas_Item ,
                                              AV15TFContratoClausulas_Descricao_Sel ,
                                              AV14TFContratoClausulas_Descricao ,
                                              A153ContratoClausulas_Item ,
                                              A77Contrato_Numero ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContratoClausulas_Item = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoClausulas_Item), 10, "%");
         lV14TFContratoClausulas_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoClausulas_Descricao), "%", "");
         /* Using cursor P00UK3 */
         pr_default.execute(1, new Object[] {lV36ContratoClausulas_Item1, lV36ContratoClausulas_Item1, lV40ContratoClausulas_Item2, lV40ContratoClausulas_Item2, lV44ContratoClausulas_Item3, lV44ContratoClausulas_Item3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContratoClausulas_Item, AV13TFContratoClausulas_Item_Sel, lV14TFContratoClausulas_Descricao, AV15TFContratoClausulas_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUK4 = false;
            A74Contrato_Codigo = P00UK3_A74Contrato_Codigo[0];
            A153ContratoClausulas_Item = P00UK3_A153ContratoClausulas_Item[0];
            A154ContratoClausulas_Descricao = P00UK3_A154ContratoClausulas_Descricao[0];
            A77Contrato_Numero = P00UK3_A77Contrato_Numero[0];
            A152ContratoClausulas_Codigo = P00UK3_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00UK3_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UK3_A153ContratoClausulas_Item[0], A153ContratoClausulas_Item) == 0 ) )
            {
               BRKUK4 = false;
               A152ContratoClausulas_Codigo = P00UK3_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUK4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A153ContratoClausulas_Item)) )
            {
               AV20Option = A153ContratoClausulas_Item;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUK4 )
            {
               BRKUK4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' Routine */
         AV14TFContratoClausulas_Descricao = AV16SearchTxt;
         AV15TFContratoClausulas_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36ContratoClausulas_Item1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39DynamicFiltersOperator2 ,
                                              AV40ContratoClausulas_Item2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44ContratoClausulas_Item3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContratoClausulas_Item_Sel ,
                                              AV12TFContratoClausulas_Item ,
                                              AV15TFContratoClausulas_Descricao_Sel ,
                                              AV14TFContratoClausulas_Descricao ,
                                              A153ContratoClausulas_Item ,
                                              A77Contrato_Numero ,
                                              A154ContratoClausulas_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV36ContratoClausulas_Item1 = StringUtil.PadR( StringUtil.RTrim( AV36ContratoClausulas_Item1), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV40ContratoClausulas_Item2 = StringUtil.PadR( StringUtil.RTrim( AV40ContratoClausulas_Item2), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV44ContratoClausulas_Item3 = StringUtil.PadR( StringUtil.RTrim( AV44ContratoClausulas_Item3), 10, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContratoClausulas_Item = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoClausulas_Item), 10, "%");
         lV14TFContratoClausulas_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoClausulas_Descricao), "%", "");
         /* Using cursor P00UK4 */
         pr_default.execute(2, new Object[] {lV36ContratoClausulas_Item1, lV36ContratoClausulas_Item1, lV40ContratoClausulas_Item2, lV40ContratoClausulas_Item2, lV44ContratoClausulas_Item3, lV44ContratoClausulas_Item3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContratoClausulas_Item, AV13TFContratoClausulas_Item_Sel, lV14TFContratoClausulas_Descricao, AV15TFContratoClausulas_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUK6 = false;
            A74Contrato_Codigo = P00UK4_A74Contrato_Codigo[0];
            A154ContratoClausulas_Descricao = P00UK4_A154ContratoClausulas_Descricao[0];
            A77Contrato_Numero = P00UK4_A77Contrato_Numero[0];
            A153ContratoClausulas_Item = P00UK4_A153ContratoClausulas_Item[0];
            A152ContratoClausulas_Codigo = P00UK4_A152ContratoClausulas_Codigo[0];
            A77Contrato_Numero = P00UK4_A77Contrato_Numero[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00UK4_A154ContratoClausulas_Descricao[0], A154ContratoClausulas_Descricao) == 0 ) )
            {
               BRKUK6 = false;
               A152ContratoClausulas_Codigo = P00UK4_A152ContratoClausulas_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUK6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A154ContratoClausulas_Descricao)) )
            {
               AV20Option = A154ContratoClausulas_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUK6 )
            {
               BRKUK6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoClausulas_Item = "";
         AV13TFContratoClausulas_Item_Sel = "";
         AV14TFContratoClausulas_Descricao = "";
         AV15TFContratoClausulas_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36ContratoClausulas_Item1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV40ContratoClausulas_Item2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44ContratoClausulas_Item3 = "";
         scmdbuf = "";
         lV36ContratoClausulas_Item1 = "";
         lV40ContratoClausulas_Item2 = "";
         lV44ContratoClausulas_Item3 = "";
         lV10TFContrato_Numero = "";
         lV12TFContratoClausulas_Item = "";
         lV14TFContratoClausulas_Descricao = "";
         A153ContratoClausulas_Item = "";
         A77Contrato_Numero = "";
         A154ContratoClausulas_Descricao = "";
         P00UK2_A74Contrato_Codigo = new int[1] ;
         P00UK2_A77Contrato_Numero = new String[] {""} ;
         P00UK2_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00UK2_A153ContratoClausulas_Item = new String[] {""} ;
         P00UK2_A152ContratoClausulas_Codigo = new int[1] ;
         AV20Option = "";
         P00UK3_A74Contrato_Codigo = new int[1] ;
         P00UK3_A153ContratoClausulas_Item = new String[] {""} ;
         P00UK3_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00UK3_A77Contrato_Numero = new String[] {""} ;
         P00UK3_A152ContratoClausulas_Codigo = new int[1] ;
         P00UK4_A74Contrato_Codigo = new int[1] ;
         P00UK4_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00UK4_A77Contrato_Numero = new String[] {""} ;
         P00UK4_A153ContratoClausulas_Item = new String[] {""} ;
         P00UK4_A152ContratoClausulas_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoclausulasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UK2_A74Contrato_Codigo, P00UK2_A77Contrato_Numero, P00UK2_A154ContratoClausulas_Descricao, P00UK2_A153ContratoClausulas_Item, P00UK2_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00UK3_A74Contrato_Codigo, P00UK3_A153ContratoClausulas_Item, P00UK3_A154ContratoClausulas_Descricao, P00UK3_A77Contrato_Numero, P00UK3_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00UK4_A74Contrato_Codigo, P00UK4_A154ContratoClausulas_Descricao, P00UK4_A77Contrato_Numero, P00UK4_A153ContratoClausulas_Item, P00UK4_A152ContratoClausulas_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV39DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private int AV47GXV1 ;
      private int A74Contrato_Codigo ;
      private int A152ContratoClausulas_Codigo ;
      private long AV28count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContratoClausulas_Item ;
      private String AV13TFContratoClausulas_Item_Sel ;
      private String AV36ContratoClausulas_Item1 ;
      private String AV40ContratoClausulas_Item2 ;
      private String AV44ContratoClausulas_Item3 ;
      private String scmdbuf ;
      private String lV36ContratoClausulas_Item1 ;
      private String lV40ContratoClausulas_Item2 ;
      private String lV44ContratoClausulas_Item3 ;
      private String lV10TFContrato_Numero ;
      private String lV12TFContratoClausulas_Item ;
      private String A153ContratoClausulas_Item ;
      private String A77Contrato_Numero ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool BRKUK2 ;
      private bool BRKUK4 ;
      private bool BRKUK6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A154ContratoClausulas_Descricao ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFContratoClausulas_Descricao ;
      private String AV15TFContratoClausulas_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String lV14TFContratoClausulas_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UK2_A74Contrato_Codigo ;
      private String[] P00UK2_A77Contrato_Numero ;
      private String[] P00UK2_A154ContratoClausulas_Descricao ;
      private String[] P00UK2_A153ContratoClausulas_Item ;
      private int[] P00UK2_A152ContratoClausulas_Codigo ;
      private int[] P00UK3_A74Contrato_Codigo ;
      private String[] P00UK3_A153ContratoClausulas_Item ;
      private String[] P00UK3_A154ContratoClausulas_Descricao ;
      private String[] P00UK3_A77Contrato_Numero ;
      private int[] P00UK3_A152ContratoClausulas_Codigo ;
      private int[] P00UK4_A74Contrato_Codigo ;
      private String[] P00UK4_A154ContratoClausulas_Descricao ;
      private String[] P00UK4_A77Contrato_Numero ;
      private String[] P00UK4_A153ContratoClausulas_Item ;
      private int[] P00UK4_A152ContratoClausulas_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptcontratoclausulasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UK2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36ContratoClausulas_Item1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             short AV39DynamicFiltersOperator2 ,
                                             String AV40ContratoClausulas_Item2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44ContratoClausulas_Item3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContratoClausulas_Item_Sel ,
                                             String AV12TFContratoClausulas_Item ,
                                             String AV15TFContratoClausulas_Descricao_Sel ,
                                             String AV14TFContratoClausulas_Descricao ,
                                             String A153ContratoClausulas_Item ,
                                             String A77Contrato_Numero ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoClausulas_Descricao], T1.[ContratoClausulas_Item], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoClausulas_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoClausulas_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UK3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36ContratoClausulas_Item1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             short AV39DynamicFiltersOperator2 ,
                                             String AV40ContratoClausulas_Item2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44ContratoClausulas_Item3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContratoClausulas_Item_Sel ,
                                             String AV12TFContratoClausulas_Item ,
                                             String AV15TFContratoClausulas_Descricao_Sel ,
                                             String AV14TFContratoClausulas_Descricao ,
                                             String A153ContratoClausulas_Item ,
                                             String A77Contrato_Numero ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoClausulas_Item], T1.[ContratoClausulas_Descricao], T2.[Contrato_Numero], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoClausulas_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoClausulas_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoClausulas_Item]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UK4( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36ContratoClausulas_Item1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             short AV39DynamicFiltersOperator2 ,
                                             String AV40ContratoClausulas_Item2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44ContratoClausulas_Item3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContratoClausulas_Item_Sel ,
                                             String AV12TFContratoClausulas_Item ,
                                             String AV15TFContratoClausulas_Descricao_Sel ,
                                             String AV14TFContratoClausulas_Descricao ,
                                             String A153ContratoClausulas_Item ,
                                             String A77Contrato_Numero ,
                                             String A154ContratoClausulas_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoClausulas_Descricao], T2.[Contrato_Numero], T1.[ContratoClausulas_Item], T1.[ContratoClausulas_Codigo] FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContratoClausulas_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV36ContratoClausulas_Item1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV39DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContratoClausulas_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV40ContratoClausulas_Item2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "CONTRATOCLAUSULAS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContratoClausulas_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like '%' + @lV44ContratoClausulas_Item3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoClausulas_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV12TFContratoClausulas_Item)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV13TFContratoClausulas_Item_Sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoClausulas_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV14TFContratoClausulas_Descricao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoClausulas_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV15TFContratoClausulas_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoClausulas_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UK2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_P00UK3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
               case 2 :
                     return conditional_P00UK4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UK2 ;
          prmP00UK2 = new Object[] {
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContratoClausulas_Item_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV14TFContratoClausulas_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFContratoClausulas_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00UK3 ;
          prmP00UK3 = new Object[] {
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContratoClausulas_Item_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV14TFContratoClausulas_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFContratoClausulas_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00UK4 ;
          prmP00UK4 = new Object[] {
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV36ContratoClausulas_Item1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV40ContratoClausulas_Item2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44ContratoClausulas_Item3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContratoClausulas_Item_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV14TFContratoClausulas_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFContratoClausulas_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UK2,100,0,true,false )
             ,new CursorDef("P00UK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UK3,100,0,true,false )
             ,new CursorDef("P00UK4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UK4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoclausulasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoclausulasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoclausulasfilterdata") )
          {
             return  ;
          }
          getpromptcontratoclausulasfilterdata worker = new getpromptcontratoclausulasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
