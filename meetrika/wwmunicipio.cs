/*
               File: WWMunicipio
        Description:  Municipio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:31:23.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwmunicipio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwmunicipio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwmunicipio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Municipio_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Municipio_Nome1", AV17Municipio_Nome1);
               AV18Estado_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Estado_Nome1", AV18Estado_Nome1);
               AV19Pais_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pais_Nome1", AV19Pais_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23Municipio_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Municipio_Nome2", AV23Municipio_Nome2);
               AV24Estado_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_Nome2", AV24Estado_Nome2);
               AV25Pais_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome2", AV25Pais_Nome2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29Municipio_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Municipio_Nome3", AV29Municipio_Nome3);
               AV30Estado_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Estado_Nome3", AV30Estado_Nome3);
               AV31Pais_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pais_Nome3", AV31Pais_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               AV61TFMunicipio_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFMunicipio_Nome", AV61TFMunicipio_Nome);
               AV62TFMunicipio_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFMunicipio_Nome_Sel", AV62TFMunicipio_Nome_Sel);
               AV69TFEstado_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFEstado_Nome", AV69TFEstado_Nome);
               AV70TFEstado_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFEstado_Nome_Sel", AV70TFEstado_Nome_Sel);
               AV73TFPais_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFPais_Nome", AV73TFPais_Nome);
               AV74TFPais_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFPais_Nome_Sel", AV74TFPais_Nome_Sel);
               AV81ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
               AV63ddo_Municipio_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Municipio_NomeTitleControlIdToReplace", AV63ddo_Municipio_NomeTitleControlIdToReplace);
               AV71ddo_Estado_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Estado_NomeTitleControlIdToReplace", AV71ddo_Estado_NomeTitleControlIdToReplace);
               AV75ddo_Pais_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Pais_NomeTitleControlIdToReplace", AV75ddo_Pais_NomeTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV115Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A23Estado_UF = GetNextPar( );
               A21Pais_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1C2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1C2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311731245");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwmunicipio.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME1", StringUtil.RTrim( AV17Municipio_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME1", StringUtil.RTrim( AV18Estado_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME1", StringUtil.RTrim( AV19Pais_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME2", StringUtil.RTrim( AV23Municipio_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME2", StringUtil.RTrim( AV24Estado_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME2", StringUtil.RTrim( AV25Pais_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME3", StringUtil.RTrim( AV29Municipio_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME3", StringUtil.RTrim( AV30Estado_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vPAIS_NOME3", StringUtil.RTrim( AV31Pais_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME", StringUtil.RTrim( AV61TFMunicipio_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME_SEL", StringUtil.RTrim( AV62TFMunicipio_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_NOME", StringUtil.RTrim( AV69TFEstado_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_NOME_SEL", StringUtil.RTrim( AV70TFEstado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_NOME", StringUtil.RTrim( AV73TFPais_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPAIS_NOME_SEL", StringUtil.RTrim( AV74TFPais_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV85ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV85ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMUNICIPIO_NOMETITLEFILTERDATA", AV60Municipio_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMUNICIPIO_NOMETITLEFILTERDATA", AV60Municipio_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_NOMETITLEFILTERDATA", AV68Estado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_NOMETITLEFILTERDATA", AV68Estado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPAIS_NOMETITLEFILTERDATA", AV72Pais_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPAIS_NOMETITLEFILTERDATA", AV72Pais_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV115Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Caption", StringUtil.RTrim( Ddo_municipio_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Tooltip", StringUtil.RTrim( Ddo_municipio_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cls", StringUtil.RTrim( Ddo_municipio_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_municipio_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_municipio_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_municipio_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_municipio_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filtertype", StringUtil.RTrim( Ddo_municipio_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_municipio_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_municipio_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_municipio_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_municipio_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_municipio_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortasc", StringUtil.RTrim( Ddo_municipio_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_municipio_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_municipio_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_municipio_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_municipio_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_municipio_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Caption", StringUtil.RTrim( Ddo_estado_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Tooltip", StringUtil.RTrim( Ddo_estado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Cls", StringUtil.RTrim( Ddo_estado_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_estado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_estado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_estado_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_estado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filtertype", StringUtil.RTrim( Ddo_estado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_estado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_estado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_estado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_estado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortasc", StringUtil.RTrim( Ddo_estado_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortdsc", StringUtil.RTrim( Ddo_estado_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_estado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_estado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_estado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_estado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Caption", StringUtil.RTrim( Ddo_pais_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Tooltip", StringUtil.RTrim( Ddo_pais_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Cls", StringUtil.RTrim( Ddo_pais_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_pais_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_pais_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_pais_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pais_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_pais_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_pais_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_pais_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_pais_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filtertype", StringUtil.RTrim( Ddo_pais_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_pais_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_pais_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalisttype", StringUtil.RTrim( Ddo_pais_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalistproc", StringUtil.RTrim( Ddo_pais_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pais_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortasc", StringUtil.RTrim( Ddo_pais_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Sortdsc", StringUtil.RTrim( Ddo_pais_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Loadingdata", StringUtil.RTrim( Ddo_pais_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_pais_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_pais_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_pais_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_municipio_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_estado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_estado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_pais_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_pais_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PAIS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_pais_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1C2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1C2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwmunicipio.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWMunicipio" ;
      }

      public override String GetPgmdesc( )
      {
         return " Municipio" ;
      }

      protected void WB1C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_1C2( true) ;
         }
         else
         {
            wb_table1_2_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_Internalname, StringUtil.RTrim( AV61TFMunicipio_Nome), StringUtil.RTrim( context.localUtil.Format( AV61TFMunicipio_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_sel_Internalname, StringUtil.RTrim( AV62TFMunicipio_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV62TFMunicipio_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_nome_Internalname, StringUtil.RTrim( AV69TFEstado_Nome), StringUtil.RTrim( context.localUtil.Format( AV69TFEstado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_nome_sel_Internalname, StringUtil.RTrim( AV70TFEstado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV70TFEstado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_nome_Internalname, StringUtil.RTrim( AV73TFPais_Nome), StringUtil.RTrim( context.localUtil.Format( AV73TFPais_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpais_nome_sel_Internalname, StringUtil.RTrim( AV74TFPais_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV74TFPais_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpais_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpais_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MUNICIPIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, AV63ddo_Municipio_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_municipio_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMunicipio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_nometitlecontrolidtoreplace_Internalname, AV71ddo_Estado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_estado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMunicipio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PAIS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pais_nometitlecontrolidtoreplace_Internalname, AV75ddo_Pais_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_pais_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMunicipio.htm");
         }
         wbLoad = true;
      }

      protected void START1C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Municipio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1C0( ) ;
      }

      protected void WS1C2( )
      {
         START1C2( ) ;
         EVT1C2( ) ;
      }

      protected void EVT1C2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E111C2 */
                              E111C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E121C2 */
                              E121C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MUNICIPIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E131C2 */
                              E131C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E141C2 */
                              E141C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PAIS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E151C2 */
                              E151C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E161C2 */
                              E161C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E171C2 */
                              E171C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E181C2 */
                              E181C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E191C2 */
                              E191C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E201C2 */
                              E201C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E211C2 */
                              E211C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E221C2 */
                              E221C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E231C2 */
                              E231C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E241C2 */
                              E241C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E251C2 */
                              E251C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              AV34Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV112Update_GXI : context.convertURL( context.PathToRelativeUrl( AV34Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV113Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              AV80Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV80Display)) ? AV114Display_GXI : context.convertURL( context.PathToRelativeUrl( AV80Display))));
                              A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
                              A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              A24Estado_Nome = StringUtil.Upper( cgiGet( edtEstado_Nome_Internalname));
                              A21Pais_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPais_Codigo_Internalname), ",", "."));
                              A22Pais_Nome = StringUtil.Upper( cgiGet( edtPais_Nome_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E261C2 */
                                    E261C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E271C2 */
                                    E271C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E281C2 */
                                    E281C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV17Municipio_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME1"), AV18Estado_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME1"), AV19Pais_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV23Municipio_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME2"), AV24Estado_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME2"), AV25Pais_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV29Municipio_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME3"), AV30Estado_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pais_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME3"), AV31Pais_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV61TFMunicipio_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV62TFMunicipio_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME"), AV69TFEstado_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME_SEL"), AV70TFEstado_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME"), AV73TFPais_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpais_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME_SEL"), AV74TFPais_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("MUNICIPIO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("ESTADO_NOME", "", 0);
            cmbavDynamicfiltersselector1.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("MUNICIPIO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("ESTADO_NOME", "", 0);
            cmbavDynamicfiltersselector2.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("MUNICIPIO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("ESTADO_NOME", "", 0);
            cmbavDynamicfiltersselector3.addItem("PAIS_NOME", "Pa�s", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Municipio_Nome1 ,
                                       String AV18Estado_Nome1 ,
                                       String AV19Pais_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23Municipio_Nome2 ,
                                       String AV24Estado_Nome2 ,
                                       String AV25Pais_Nome2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       String AV29Municipio_Nome3 ,
                                       String AV30Estado_Nome3 ,
                                       String AV31Pais_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 ,
                                       String AV61TFMunicipio_Nome ,
                                       String AV62TFMunicipio_Nome_Sel ,
                                       String AV69TFEstado_Nome ,
                                       String AV70TFEstado_Nome_Sel ,
                                       String AV73TFPais_Nome ,
                                       String AV74TFPais_Nome_Sel ,
                                       short AV81ManageFiltersExecutionStep ,
                                       String AV63ddo_Municipio_NomeTitleControlIdToReplace ,
                                       String AV71ddo_Estado_NomeTitleControlIdToReplace ,
                                       String AV75ddo_Pais_NomeTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV115Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A25Municipio_Codigo ,
                                       String A23Estado_UF ,
                                       int A21Pais_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF1C2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_NOME", StringUtil.RTrim( A26Municipio_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "gxhash_PAIS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PAIS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV115Pgmname = "WWMunicipio";
         context.Gx_err = 0;
      }

      protected void RF1C2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E271C2 */
         E271C2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV89WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                                 AV90WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                                 AV91WWMunicipioDS_3_Municipio_nome1 ,
                                                 AV92WWMunicipioDS_4_Estado_nome1 ,
                                                 AV93WWMunicipioDS_5_Pais_nome1 ,
                                                 AV94WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                                 AV95WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                                 AV96WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                                 AV97WWMunicipioDS_9_Municipio_nome2 ,
                                                 AV98WWMunicipioDS_10_Estado_nome2 ,
                                                 AV99WWMunicipioDS_11_Pais_nome2 ,
                                                 AV100WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                                 AV101WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                                 AV102WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                                 AV103WWMunicipioDS_15_Municipio_nome3 ,
                                                 AV104WWMunicipioDS_16_Estado_nome3 ,
                                                 AV105WWMunicipioDS_17_Pais_nome3 ,
                                                 AV107WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                                 AV106WWMunicipioDS_18_Tfmunicipio_nome ,
                                                 AV109WWMunicipioDS_21_Tfestado_nome_sel ,
                                                 AV108WWMunicipioDS_20_Tfestado_nome ,
                                                 AV111WWMunicipioDS_23_Tfpais_nome_sel ,
                                                 AV110WWMunicipioDS_22_Tfpais_nome ,
                                                 A26Municipio_Nome ,
                                                 A24Estado_Nome ,
                                                 A22Pais_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV91WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1), 50, "%");
            lV91WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1), 50, "%");
            lV92WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1), 50, "%");
            lV92WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1), 50, "%");
            lV93WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1), 50, "%");
            lV93WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1), 50, "%");
            lV97WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2), 50, "%");
            lV97WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2), 50, "%");
            lV98WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2), 50, "%");
            lV98WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2), 50, "%");
            lV99WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2), 50, "%");
            lV99WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2), 50, "%");
            lV103WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3), 50, "%");
            lV103WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3), 50, "%");
            lV104WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3), 50, "%");
            lV104WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3), 50, "%");
            lV105WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3), 50, "%");
            lV105WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3), 50, "%");
            lV106WWMunicipioDS_18_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV106WWMunicipioDS_18_Tfmunicipio_nome), 50, "%");
            lV108WWMunicipioDS_20_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV108WWMunicipioDS_20_Tfestado_nome), 50, "%");
            lV110WWMunicipioDS_22_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV110WWMunicipioDS_22_Tfpais_nome), 50, "%");
            /* Using cursor H001C2 */
            pr_default.execute(0, new Object[] {lV91WWMunicipioDS_3_Municipio_nome1, lV91WWMunicipioDS_3_Municipio_nome1, lV92WWMunicipioDS_4_Estado_nome1, lV92WWMunicipioDS_4_Estado_nome1, lV93WWMunicipioDS_5_Pais_nome1, lV93WWMunicipioDS_5_Pais_nome1, lV97WWMunicipioDS_9_Municipio_nome2, lV97WWMunicipioDS_9_Municipio_nome2, lV98WWMunicipioDS_10_Estado_nome2, lV98WWMunicipioDS_10_Estado_nome2, lV99WWMunicipioDS_11_Pais_nome2, lV99WWMunicipioDS_11_Pais_nome2, lV103WWMunicipioDS_15_Municipio_nome3, lV103WWMunicipioDS_15_Municipio_nome3, lV104WWMunicipioDS_16_Estado_nome3, lV104WWMunicipioDS_16_Estado_nome3, lV105WWMunicipioDS_17_Pais_nome3, lV105WWMunicipioDS_17_Pais_nome3, lV106WWMunicipioDS_18_Tfmunicipio_nome, AV107WWMunicipioDS_19_Tfmunicipio_nome_sel, lV108WWMunicipioDS_20_Tfestado_nome, AV109WWMunicipioDS_21_Tfestado_nome_sel, lV110WWMunicipioDS_22_Tfpais_nome, AV111WWMunicipioDS_23_Tfpais_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_94_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A22Pais_Nome = H001C2_A22Pais_Nome[0];
               A21Pais_Codigo = H001C2_A21Pais_Codigo[0];
               A24Estado_Nome = H001C2_A24Estado_Nome[0];
               A23Estado_UF = H001C2_A23Estado_UF[0];
               A26Municipio_Nome = H001C2_A26Municipio_Nome[0];
               A25Municipio_Codigo = H001C2_A25Municipio_Codigo[0];
               A22Pais_Nome = H001C2_A22Pais_Nome[0];
               A24Estado_Nome = H001C2_A24Estado_Nome[0];
               /* Execute user event: E281C2 */
               E281C2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WB1C0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV89WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                              AV90WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                              AV91WWMunicipioDS_3_Municipio_nome1 ,
                                              AV92WWMunicipioDS_4_Estado_nome1 ,
                                              AV93WWMunicipioDS_5_Pais_nome1 ,
                                              AV94WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                              AV95WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                              AV96WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                              AV97WWMunicipioDS_9_Municipio_nome2 ,
                                              AV98WWMunicipioDS_10_Estado_nome2 ,
                                              AV99WWMunicipioDS_11_Pais_nome2 ,
                                              AV100WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                              AV101WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                              AV102WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                              AV103WWMunicipioDS_15_Municipio_nome3 ,
                                              AV104WWMunicipioDS_16_Estado_nome3 ,
                                              AV105WWMunicipioDS_17_Pais_nome3 ,
                                              AV107WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                              AV106WWMunicipioDS_18_Tfmunicipio_nome ,
                                              AV109WWMunicipioDS_21_Tfestado_nome_sel ,
                                              AV108WWMunicipioDS_20_Tfestado_nome ,
                                              AV111WWMunicipioDS_23_Tfpais_nome_sel ,
                                              AV110WWMunicipioDS_22_Tfpais_nome ,
                                              A26Municipio_Nome ,
                                              A24Estado_Nome ,
                                              A22Pais_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV91WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV91WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV92WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV92WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV93WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV93WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV97WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV97WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV98WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV98WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV99WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV99WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV103WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV103WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV104WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV104WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV105WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV105WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV106WWMunicipioDS_18_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV106WWMunicipioDS_18_Tfmunicipio_nome), 50, "%");
         lV108WWMunicipioDS_20_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV108WWMunicipioDS_20_Tfestado_nome), 50, "%");
         lV110WWMunicipioDS_22_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV110WWMunicipioDS_22_Tfpais_nome), 50, "%");
         /* Using cursor H001C3 */
         pr_default.execute(1, new Object[] {lV91WWMunicipioDS_3_Municipio_nome1, lV91WWMunicipioDS_3_Municipio_nome1, lV92WWMunicipioDS_4_Estado_nome1, lV92WWMunicipioDS_4_Estado_nome1, lV93WWMunicipioDS_5_Pais_nome1, lV93WWMunicipioDS_5_Pais_nome1, lV97WWMunicipioDS_9_Municipio_nome2, lV97WWMunicipioDS_9_Municipio_nome2, lV98WWMunicipioDS_10_Estado_nome2, lV98WWMunicipioDS_10_Estado_nome2, lV99WWMunicipioDS_11_Pais_nome2, lV99WWMunicipioDS_11_Pais_nome2, lV103WWMunicipioDS_15_Municipio_nome3, lV103WWMunicipioDS_15_Municipio_nome3, lV104WWMunicipioDS_16_Estado_nome3, lV104WWMunicipioDS_16_Estado_nome3, lV105WWMunicipioDS_17_Pais_nome3, lV105WWMunicipioDS_17_Pais_nome3, lV106WWMunicipioDS_18_Tfmunicipio_nome, AV107WWMunicipioDS_19_Tfmunicipio_nome_sel, lV108WWMunicipioDS_20_Tfestado_nome, AV109WWMunicipioDS_21_Tfestado_nome_sel, lV110WWMunicipioDS_22_Tfpais_nome, AV111WWMunicipioDS_23_Tfpais_nome_sel});
         GRID_nRecordCount = H001C3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP1C0( )
      {
         /* Before Start, stand alone formulas. */
         AV115Pgmname = "WWMunicipio";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E261C2 */
         E261C2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV85ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV76DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vMUNICIPIO_NOMETITLEFILTERDATA"), AV60Municipio_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_NOMETITLEFILTERDATA"), AV68Estado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPAIS_NOMETITLEFILTERDATA"), AV72Pais_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Municipio_Nome1 = StringUtil.Upper( cgiGet( edtavMunicipio_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Municipio_Nome1", AV17Municipio_Nome1);
            AV18Estado_Nome1 = StringUtil.Upper( cgiGet( edtavEstado_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Estado_Nome1", AV18Estado_Nome1);
            AV19Pais_Nome1 = StringUtil.Upper( cgiGet( edtavPais_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pais_Nome1", AV19Pais_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23Municipio_Nome2 = StringUtil.Upper( cgiGet( edtavMunicipio_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Municipio_Nome2", AV23Municipio_Nome2);
            AV24Estado_Nome2 = StringUtil.Upper( cgiGet( edtavEstado_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_Nome2", AV24Estado_Nome2);
            AV25Pais_Nome2 = StringUtil.Upper( cgiGet( edtavPais_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome2", AV25Pais_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            AV29Municipio_Nome3 = StringUtil.Upper( cgiGet( edtavMunicipio_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Municipio_Nome3", AV29Municipio_Nome3);
            AV30Estado_Nome3 = StringUtil.Upper( cgiGet( edtavEstado_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Estado_Nome3", AV30Estado_Nome3);
            AV31Pais_Nome3 = StringUtil.Upper( cgiGet( edtavPais_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pais_Nome3", AV31Pais_Nome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV81ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
            }
            AV61TFMunicipio_Nome = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFMunicipio_Nome", AV61TFMunicipio_Nome);
            AV62TFMunicipio_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFMunicipio_Nome_Sel", AV62TFMunicipio_Nome_Sel);
            AV69TFEstado_Nome = StringUtil.Upper( cgiGet( edtavTfestado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFEstado_Nome", AV69TFEstado_Nome);
            AV70TFEstado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfestado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFEstado_Nome_Sel", AV70TFEstado_Nome_Sel);
            AV73TFPais_Nome = StringUtil.Upper( cgiGet( edtavTfpais_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFPais_Nome", AV73TFPais_Nome);
            AV74TFPais_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfpais_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFPais_Nome_Sel", AV74TFPais_Nome_Sel);
            AV63ddo_Municipio_NomeTitleControlIdToReplace = cgiGet( edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Municipio_NomeTitleControlIdToReplace", AV63ddo_Municipio_NomeTitleControlIdToReplace);
            AV71ddo_Estado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_estado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Estado_NomeTitleControlIdToReplace", AV71ddo_Estado_NomeTitleControlIdToReplace);
            AV75ddo_Pais_NomeTitleControlIdToReplace = cgiGet( edtavDdo_pais_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Pais_NomeTitleControlIdToReplace", AV75ddo_Pais_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV78GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV79GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_municipio_nome_Caption = cgiGet( "DDO_MUNICIPIO_NOME_Caption");
            Ddo_municipio_nome_Tooltip = cgiGet( "DDO_MUNICIPIO_NOME_Tooltip");
            Ddo_municipio_nome_Cls = cgiGet( "DDO_MUNICIPIO_NOME_Cls");
            Ddo_municipio_nome_Filteredtext_set = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_set");
            Ddo_municipio_nome_Selectedvalue_set = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_set");
            Ddo_municipio_nome_Dropdownoptionstype = cgiGet( "DDO_MUNICIPIO_NOME_Dropdownoptionstype");
            Ddo_municipio_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace");
            Ddo_municipio_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortasc"));
            Ddo_municipio_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortdsc"));
            Ddo_municipio_nome_Sortedstatus = cgiGet( "DDO_MUNICIPIO_NOME_Sortedstatus");
            Ddo_municipio_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includefilter"));
            Ddo_municipio_nome_Filtertype = cgiGet( "DDO_MUNICIPIO_NOME_Filtertype");
            Ddo_municipio_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Filterisrange"));
            Ddo_municipio_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includedatalist"));
            Ddo_municipio_nome_Datalisttype = cgiGet( "DDO_MUNICIPIO_NOME_Datalisttype");
            Ddo_municipio_nome_Datalistproc = cgiGet( "DDO_MUNICIPIO_NOME_Datalistproc");
            Ddo_municipio_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_municipio_nome_Sortasc = cgiGet( "DDO_MUNICIPIO_NOME_Sortasc");
            Ddo_municipio_nome_Sortdsc = cgiGet( "DDO_MUNICIPIO_NOME_Sortdsc");
            Ddo_municipio_nome_Loadingdata = cgiGet( "DDO_MUNICIPIO_NOME_Loadingdata");
            Ddo_municipio_nome_Cleanfilter = cgiGet( "DDO_MUNICIPIO_NOME_Cleanfilter");
            Ddo_municipio_nome_Noresultsfound = cgiGet( "DDO_MUNICIPIO_NOME_Noresultsfound");
            Ddo_municipio_nome_Searchbuttontext = cgiGet( "DDO_MUNICIPIO_NOME_Searchbuttontext");
            Ddo_estado_nome_Caption = cgiGet( "DDO_ESTADO_NOME_Caption");
            Ddo_estado_nome_Tooltip = cgiGet( "DDO_ESTADO_NOME_Tooltip");
            Ddo_estado_nome_Cls = cgiGet( "DDO_ESTADO_NOME_Cls");
            Ddo_estado_nome_Filteredtext_set = cgiGet( "DDO_ESTADO_NOME_Filteredtext_set");
            Ddo_estado_nome_Selectedvalue_set = cgiGet( "DDO_ESTADO_NOME_Selectedvalue_set");
            Ddo_estado_nome_Dropdownoptionstype = cgiGet( "DDO_ESTADO_NOME_Dropdownoptionstype");
            Ddo_estado_nome_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_NOME_Titlecontrolidtoreplace");
            Ddo_estado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includesortasc"));
            Ddo_estado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includesortdsc"));
            Ddo_estado_nome_Sortedstatus = cgiGet( "DDO_ESTADO_NOME_Sortedstatus");
            Ddo_estado_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includefilter"));
            Ddo_estado_nome_Filtertype = cgiGet( "DDO_ESTADO_NOME_Filtertype");
            Ddo_estado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Filterisrange"));
            Ddo_estado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includedatalist"));
            Ddo_estado_nome_Datalisttype = cgiGet( "DDO_ESTADO_NOME_Datalisttype");
            Ddo_estado_nome_Datalistproc = cgiGet( "DDO_ESTADO_NOME_Datalistproc");
            Ddo_estado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ESTADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_nome_Sortasc = cgiGet( "DDO_ESTADO_NOME_Sortasc");
            Ddo_estado_nome_Sortdsc = cgiGet( "DDO_ESTADO_NOME_Sortdsc");
            Ddo_estado_nome_Loadingdata = cgiGet( "DDO_ESTADO_NOME_Loadingdata");
            Ddo_estado_nome_Cleanfilter = cgiGet( "DDO_ESTADO_NOME_Cleanfilter");
            Ddo_estado_nome_Noresultsfound = cgiGet( "DDO_ESTADO_NOME_Noresultsfound");
            Ddo_estado_nome_Searchbuttontext = cgiGet( "DDO_ESTADO_NOME_Searchbuttontext");
            Ddo_pais_nome_Caption = cgiGet( "DDO_PAIS_NOME_Caption");
            Ddo_pais_nome_Tooltip = cgiGet( "DDO_PAIS_NOME_Tooltip");
            Ddo_pais_nome_Cls = cgiGet( "DDO_PAIS_NOME_Cls");
            Ddo_pais_nome_Filteredtext_set = cgiGet( "DDO_PAIS_NOME_Filteredtext_set");
            Ddo_pais_nome_Selectedvalue_set = cgiGet( "DDO_PAIS_NOME_Selectedvalue_set");
            Ddo_pais_nome_Dropdownoptionstype = cgiGet( "DDO_PAIS_NOME_Dropdownoptionstype");
            Ddo_pais_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PAIS_NOME_Titlecontrolidtoreplace");
            Ddo_pais_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includesortasc"));
            Ddo_pais_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includesortdsc"));
            Ddo_pais_nome_Sortedstatus = cgiGet( "DDO_PAIS_NOME_Sortedstatus");
            Ddo_pais_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includefilter"));
            Ddo_pais_nome_Filtertype = cgiGet( "DDO_PAIS_NOME_Filtertype");
            Ddo_pais_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Filterisrange"));
            Ddo_pais_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PAIS_NOME_Includedatalist"));
            Ddo_pais_nome_Datalisttype = cgiGet( "DDO_PAIS_NOME_Datalisttype");
            Ddo_pais_nome_Datalistproc = cgiGet( "DDO_PAIS_NOME_Datalistproc");
            Ddo_pais_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PAIS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pais_nome_Sortasc = cgiGet( "DDO_PAIS_NOME_Sortasc");
            Ddo_pais_nome_Sortdsc = cgiGet( "DDO_PAIS_NOME_Sortdsc");
            Ddo_pais_nome_Loadingdata = cgiGet( "DDO_PAIS_NOME_Loadingdata");
            Ddo_pais_nome_Cleanfilter = cgiGet( "DDO_PAIS_NOME_Cleanfilter");
            Ddo_pais_nome_Noresultsfound = cgiGet( "DDO_PAIS_NOME_Noresultsfound");
            Ddo_pais_nome_Searchbuttontext = cgiGet( "DDO_PAIS_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_municipio_nome_Activeeventkey = cgiGet( "DDO_MUNICIPIO_NOME_Activeeventkey");
            Ddo_municipio_nome_Filteredtext_get = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_get");
            Ddo_municipio_nome_Selectedvalue_get = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_get");
            Ddo_estado_nome_Activeeventkey = cgiGet( "DDO_ESTADO_NOME_Activeeventkey");
            Ddo_estado_nome_Filteredtext_get = cgiGet( "DDO_ESTADO_NOME_Filteredtext_get");
            Ddo_estado_nome_Selectedvalue_get = cgiGet( "DDO_ESTADO_NOME_Selectedvalue_get");
            Ddo_pais_nome_Activeeventkey = cgiGet( "DDO_PAIS_NOME_Activeeventkey");
            Ddo_pais_nome_Filteredtext_get = cgiGet( "DDO_PAIS_NOME_Filteredtext_get");
            Ddo_pais_nome_Selectedvalue_get = cgiGet( "DDO_PAIS_NOME_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV17Municipio_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME1"), AV18Estado_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME1"), AV19Pais_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV23Municipio_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME2"), AV24Estado_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME2"), AV25Pais_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV29Municipio_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME3"), AV30Estado_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPAIS_NOME3"), AV31Pais_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV61TFMunicipio_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV62TFMunicipio_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME"), AV69TFEstado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME_SEL"), AV70TFEstado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME"), AV73TFPais_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPAIS_NOME_SEL"), AV74TFPais_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E261C2 */
         E261C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E261C2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfmunicipio_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_Visible), 5, 0)));
         edtavTfmunicipio_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_sel_Visible), 5, 0)));
         edtavTfestado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_nome_Visible), 5, 0)));
         edtavTfestado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_nome_sel_Visible), 5, 0)));
         edtavTfpais_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_nome_Visible), 5, 0)));
         edtavTfpais_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpais_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpais_nome_sel_Visible), 5, 0)));
         Ddo_municipio_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Municipio_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "TitleControlIdToReplace", Ddo_municipio_nome_Titlecontrolidtoreplace);
         AV63ddo_Municipio_NomeTitleControlIdToReplace = Ddo_municipio_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Municipio_NomeTitleControlIdToReplace", AV63ddo_Municipio_NomeTitleControlIdToReplace);
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_municipio_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "TitleControlIdToReplace", Ddo_estado_nome_Titlecontrolidtoreplace);
         AV71ddo_Estado_NomeTitleControlIdToReplace = Ddo_estado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Estado_NomeTitleControlIdToReplace", AV71ddo_Estado_NomeTitleControlIdToReplace);
         edtavDdo_estado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pais_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Pais_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "TitleControlIdToReplace", Ddo_pais_nome_Titlecontrolidtoreplace);
         AV75ddo_Pais_NomeTitleControlIdToReplace = Ddo_pais_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Pais_NomeTitleControlIdToReplace", AV75ddo_Pais_NomeTitleControlIdToReplace);
         edtavDdo_pais_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pais_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pais_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Municipio";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Munic�pio", 0);
         cmbavOrderedby.addItem("2", "", 0);
         cmbavOrderedby.addItem("3", "Pa�s", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV76DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV76DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E271C2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Estado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Pais_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV81ManageFiltersExecutionStep == 1 )
         {
            AV81ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV81ManageFiltersExecutionStep == 2 )
         {
            AV81ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PAIS_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "PAIS_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV63ddo_Municipio_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         edtEstado_Nome_Titleformat = 2;
         edtEstado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "", AV71ddo_Estado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_Nome_Internalname, "Title", edtEstado_Nome_Title);
         edtPais_Nome_Titleformat = 2;
         edtPais_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pa�s", AV75ddo_Pais_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPais_Nome_Internalname, "Title", edtPais_Nome_Title);
         AV78GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78GridCurrentPage), 10, 0)));
         AV79GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridPageCount), 10, 0)));
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV90WWMunicipioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV91WWMunicipioDS_3_Municipio_nome1 = AV17Municipio_Nome1;
         AV92WWMunicipioDS_4_Estado_nome1 = AV18Estado_Nome1;
         AV93WWMunicipioDS_5_Pais_nome1 = AV19Pais_Nome1;
         AV94WWMunicipioDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV96WWMunicipioDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV97WWMunicipioDS_9_Municipio_nome2 = AV23Municipio_Nome2;
         AV98WWMunicipioDS_10_Estado_nome2 = AV24Estado_Nome2;
         AV99WWMunicipioDS_11_Pais_nome2 = AV25Pais_Nome2;
         AV100WWMunicipioDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV102WWMunicipioDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV103WWMunicipioDS_15_Municipio_nome3 = AV29Municipio_Nome3;
         AV104WWMunicipioDS_16_Estado_nome3 = AV30Estado_Nome3;
         AV105WWMunicipioDS_17_Pais_nome3 = AV31Pais_Nome3;
         AV106WWMunicipioDS_18_Tfmunicipio_nome = AV61TFMunicipio_Nome;
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = AV62TFMunicipio_Nome_Sel;
         AV108WWMunicipioDS_20_Tfestado_nome = AV69TFEstado_Nome;
         AV109WWMunicipioDS_21_Tfestado_nome_sel = AV70TFEstado_Nome_Sel;
         AV110WWMunicipioDS_22_Tfpais_nome = AV73TFPais_Nome;
         AV111WWMunicipioDS_23_Tfpais_nome_sel = AV74TFPais_Nome_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60Municipio_NomeTitleFilterData", AV60Municipio_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68Estado_NomeTitleFilterData", AV68Estado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72Pais_NomeTitleFilterData", AV72Pais_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85ManageFiltersData", AV85ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E121C2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV77PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV77PageToGo) ;
         }
      }

      protected void E131C2( )
      {
         /* Ddo_municipio_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFMunicipio_Nome = Ddo_municipio_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFMunicipio_Nome", AV61TFMunicipio_Nome);
            AV62TFMunicipio_Nome_Sel = Ddo_municipio_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFMunicipio_Nome_Sel", AV62TFMunicipio_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E141C2( )
      {
         /* Ddo_estado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFEstado_Nome = Ddo_estado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFEstado_Nome", AV69TFEstado_Nome);
            AV70TFEstado_Nome_Sel = Ddo_estado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFEstado_Nome_Sel", AV70TFEstado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E151C2( )
      {
         /* Ddo_pais_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pais_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pais_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFPais_Nome = Ddo_pais_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFPais_Nome", AV73TFPais_Nome);
            AV74TFPais_Nome_Sel = Ddo_pais_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFPais_Nome_Sel", AV74TFPais_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E281C2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A25Municipio_Codigo);
            AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
            AV112Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV34Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
            AV112Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A25Municipio_Codigo);
            AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
            AV113Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV35Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
            AV113Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV80Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV80Display);
            AV114Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV80Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV80Display);
            AV114Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtEstado_Nome_Link = formatLink("viewestado.aspx") + "?" + UrlEncode(StringUtil.RTrim(A23Estado_UF)) + "," + UrlEncode(StringUtil.RTrim(""));
         edtPais_Nome_Link = formatLink("viewpais.aspx") + "?" + UrlEncode("" +A21Pais_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         sendrow_942( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E161C2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E211C2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
      }

      protected void E171C2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E221C2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E231C2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
      }

      protected void E181C2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E241C2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E191C2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Municipio_Nome1, AV18Estado_Nome1, AV19Pais_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Municipio_Nome2, AV24Estado_Nome2, AV25Pais_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Municipio_Nome3, AV30Estado_Nome3, AV31Pais_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFMunicipio_Nome, AV62TFMunicipio_Nome_Sel, AV69TFEstado_Nome, AV70TFEstado_Nome_Sel, AV73TFPais_Nome, AV74TFPais_Nome_Sel, AV81ManageFiltersExecutionStep, AV63ddo_Municipio_NomeTitleControlIdToReplace, AV71ddo_Estado_NomeTitleControlIdToReplace, AV75ddo_Pais_NomeTitleControlIdToReplace, AV6WWPContext, AV115Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A25Municipio_Codigo, A23Estado_UF, A21Pais_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E251C2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E111C2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWMunicipioFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV81ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWMunicipioFilters")), new Object[] {});
            AV81ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV81ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV82ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWMunicipioFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV82ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV115Pgmname+"GridState",  AV82ManageFiltersXml) ;
               AV10GridState.FromXml(AV82ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E201C2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_municipio_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         Ddo_estado_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
         Ddo_pais_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_municipio_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_estado_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_pais_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SortedStatus", Ddo_pais_nome_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavMunicipio_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
         edtavEstado_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome1_Visible), 5, 0)));
         edtavPais_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 )
         {
            edtavPais_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavMunicipio_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
         edtavEstado_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome2_Visible), 5, 0)));
         edtavPais_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PAIS_NOME") == 0 )
         {
            edtavPais_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavMunicipio_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
         edtavEstado_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome3_Visible), 5, 0)));
         edtavPais_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "PAIS_NOME") == 0 )
         {
            edtavPais_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPais_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPais_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Municipio_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Municipio_Nome2", AV23Municipio_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29Municipio_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Municipio_Nome3", AV29Municipio_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV85ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV86ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV86ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV86ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV86ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV86ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
         AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV86ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV86ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV86ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV86ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
         AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV86ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
         AV83ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWMunicipioFilters"), "");
         AV116GXV1 = 1;
         while ( AV116GXV1 <= AV83ManageFiltersItems.Count )
         {
            AV84ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV83ManageFiltersItems.Item(AV116GXV1));
            AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV86ManageFiltersDataItem.gxTpr_Title = AV84ManageFiltersItem.gxTpr_Title;
            AV86ManageFiltersDataItem.gxTpr_Eventkey = AV84ManageFiltersItem.gxTpr_Title;
            AV86ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV86ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
            if ( AV85ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV116GXV1 = (int)(AV116GXV1+1);
         }
         if ( AV85ManageFiltersData.Count > 3 )
         {
            AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV86ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
            AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV86ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV86ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV86ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV86ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV86ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV85ManageFiltersData.Add(AV86ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV61TFMunicipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFMunicipio_Nome", AV61TFMunicipio_Nome);
         Ddo_municipio_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
         AV62TFMunicipio_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFMunicipio_Nome_Sel", AV62TFMunicipio_Nome_Sel);
         Ddo_municipio_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
         AV69TFEstado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFEstado_Nome", AV69TFEstado_Nome);
         Ddo_estado_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "FilteredText_set", Ddo_estado_nome_Filteredtext_set);
         AV70TFEstado_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFEstado_Nome_Sel", AV70TFEstado_Nome_Sel);
         Ddo_estado_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SelectedValue_set", Ddo_estado_nome_Selectedvalue_set);
         AV73TFPais_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFPais_Nome", AV73TFPais_Nome);
         Ddo_pais_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "FilteredText_set", Ddo_pais_nome_Filteredtext_set);
         AV74TFPais_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFPais_Nome_Sel", AV74TFPais_Nome_Sel);
         Ddo_pais_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SelectedValue_set", Ddo_pais_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "MUNICIPIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Municipio_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Municipio_Nome1", AV17Municipio_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get(AV115Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV115Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV38Session.Get(AV115Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV117GXV2 = 1;
         while ( AV117GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV117GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV61TFMunicipio_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFMunicipio_Nome", AV61TFMunicipio_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFMunicipio_Nome)) )
               {
                  Ddo_municipio_nome_Filteredtext_set = AV61TFMunicipio_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV62TFMunicipio_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFMunicipio_Nome_Sel", AV62TFMunicipio_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFMunicipio_Nome_Sel)) )
               {
                  Ddo_municipio_nome_Selectedvalue_set = AV62TFMunicipio_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME") == 0 )
            {
               AV69TFEstado_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFEstado_Nome", AV69TFEstado_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFEstado_Nome)) )
               {
                  Ddo_estado_nome_Filteredtext_set = AV69TFEstado_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "FilteredText_set", Ddo_estado_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME_SEL") == 0 )
            {
               AV70TFEstado_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFEstado_Nome_Sel", AV70TFEstado_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFEstado_Nome_Sel)) )
               {
                  Ddo_estado_nome_Selectedvalue_set = AV70TFEstado_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SelectedValue_set", Ddo_estado_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME") == 0 )
            {
               AV73TFPais_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFPais_Nome", AV73TFPais_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFPais_Nome)) )
               {
                  Ddo_pais_nome_Filteredtext_set = AV73TFPais_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "FilteredText_set", Ddo_pais_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME_SEL") == 0 )
            {
               AV74TFPais_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFPais_Nome_Sel", AV74TFPais_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFPais_Nome_Sel)) )
               {
                  Ddo_pais_nome_Selectedvalue_set = AV74TFPais_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pais_nome_Internalname, "SelectedValue_set", Ddo_pais_nome_Selectedvalue_set);
               }
            }
            AV117GXV2 = (int)(AV117GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Municipio_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Municipio_Nome1", AV17Municipio_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Estado_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Estado_Nome1", AV18Estado_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19Pais_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pais_Nome1", AV19Pais_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Municipio_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Municipio_Nome2", AV23Municipio_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Estado_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_Nome2", AV24Estado_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PAIS_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25Pais_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pais_Nome2", AV25Pais_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29Municipio_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Municipio_Nome3", AV29Municipio_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV30Estado_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Estado_Nome3", AV30Estado_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "PAIS_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31Pais_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Pais_Nome3", AV31Pais_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV38Session.Get(AV115Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFMunicipio_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFMunicipio_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFMunicipio_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFMunicipio_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFEstado_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV69TFEstado_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFEstado_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFEstado_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFPais_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPAIS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFPais_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFPais_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPAIS_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFPais_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV115Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Municipio_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Municipio_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Estado_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Estado_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Pais_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Pais_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Municipio_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Municipio_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Estado_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Estado_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Pais_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Pais_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Municipio_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29Municipio_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Estado_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30Estado_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "PAIS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Pais_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Pais_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV115Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Municipio";
         AV38Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1C2( true) ;
         }
         else
         {
            wb_table2_8_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_1C2( true) ;
         }
         else
         {
            wb_table3_88_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1C2e( true) ;
         }
         else
         {
            wb_table1_2_1C2e( false) ;
         }
      }

      protected void wb_table3_88_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_1C2( true) ;
         }
         else
         {
            wb_table4_91_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_1C2e( true) ;
         }
         else
         {
            wb_table3_88_1C2e( false) ;
         }
      }

      protected void wb_table4_91_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. do Munic�pio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "UF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. do Pa�s") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPais_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPais_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPais_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV80Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMunicipio_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A24Estado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtEstado_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A22Pais_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPais_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPais_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtPais_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_1C2e( true) ;
         }
         else
         {
            wb_table4_91_1C2e( false) ;
         }
      }

      protected void wb_table2_8_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_1C2( true) ;
         }
         else
         {
            wb_table5_11_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_1C2( true) ;
         }
         else
         {
            wb_table6_23_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1C2e( true) ;
         }
         else
         {
            wb_table2_8_1C2e( false) ;
         }
      }

      protected void wb_table6_23_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_1C2( true) ;
         }
         else
         {
            wb_table7_28_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_1C2e( true) ;
         }
         else
         {
            wb_table6_23_1C2e( false) ;
         }
      }

      protected void wb_table7_28_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_1C2( true) ;
         }
         else
         {
            wb_table8_37_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_56_1C2( true) ;
         }
         else
         {
            wb_table9_56_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_1C2( true) ;
         }
         else
         {
            wb_table10_75_1C2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_1C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_1C2e( true) ;
         }
         else
         {
            wb_table7_28_1C2e( false) ;
         }
      }

      protected void wb_table10_75_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome3_Internalname, StringUtil.RTrim( AV29Municipio_Nome3), StringUtil.RTrim( context.localUtil.Format( AV29Municipio_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome3_Internalname, StringUtil.RTrim( AV30Estado_Nome3), StringUtil.RTrim( context.localUtil.Format( AV30Estado_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome3_Internalname, StringUtil.RTrim( AV31Pais_Nome3), StringUtil.RTrim( context.localUtil.Format( AV31Pais_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_1C2e( true) ;
         }
         else
         {
            wb_table10_75_1C2e( false) ;
         }
      }

      protected void wb_table9_56_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome2_Internalname, StringUtil.RTrim( AV23Municipio_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Municipio_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome2_Internalname, StringUtil.RTrim( AV24Estado_Nome2), StringUtil.RTrim( context.localUtil.Format( AV24Estado_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome2_Internalname, StringUtil.RTrim( AV25Pais_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25Pais_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_1C2e( true) ;
         }
         else
         {
            wb_table9_56_1C2e( false) ;
         }
      }

      protected void wb_table8_37_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome1_Internalname, StringUtil.RTrim( AV17Municipio_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Municipio_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome1_Internalname, StringUtil.RTrim( AV18Estado_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Estado_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPais_nome1_Internalname, StringUtil.RTrim( AV19Pais_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19Pais_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPais_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPais_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_1C2e( true) ;
         }
         else
         {
            wb_table8_37_1C2e( false) ;
         }
      }

      protected void wb_table5_11_1C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMunicipiotitle_Internalname, "Municipios", "", "", lblMunicipiotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWMunicipio.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWMunicipio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_1C2e( true) ;
         }
         else
         {
            wb_table5_11_1C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1C2( ) ;
         WS1C2( ) ;
         WE1C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117313166");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwmunicipio.js", "?20203117313166");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_94_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_94_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_94_idx;
         edtPais_Codigo_Internalname = "PAIS_CODIGO_"+sGXsfl_94_idx;
         edtPais_Nome_Internalname = "PAIS_NOME_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_fel_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_94_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_94_fel_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_94_fel_idx;
         edtPais_Codigo_Internalname = "PAIS_CODIGO_"+sGXsfl_94_fel_idx;
         edtPais_Nome_Internalname = "PAIS_NOME_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WB1C0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV112Update_GXI : context.PathToRelativeUrl( AV34Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV113Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV80Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV80Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV114Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV80Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV80Display)) ? AV114Display_GXI : context.PathToRelativeUrl( AV80Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV80Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMunicipio_Nome_Link,(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_Nome_Internalname,StringUtil.RTrim( A24Estado_Nome),StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtEstado_Nome_Link,(String)"",(String)"",(String)"",(String)edtEstado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPais_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A21Pais_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPais_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPais_Nome_Internalname,StringUtil.RTrim( A22Pais_Nome),StringUtil.RTrim( context.localUtil.Format( A22Pais_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtPais_Nome_Link,(String)"",(String)"",(String)"",(String)edtPais_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_NOME"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PAIS_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A21Pais_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblMunicipiotitle_Internalname = "MUNICIPIOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavMunicipio_nome1_Internalname = "vMUNICIPIO_NOME1";
         edtavEstado_nome1_Internalname = "vESTADO_NOME1";
         edtavPais_nome1_Internalname = "vPAIS_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavMunicipio_nome2_Internalname = "vMUNICIPIO_NOME2";
         edtavEstado_nome2_Internalname = "vESTADO_NOME2";
         edtavPais_nome2_Internalname = "vPAIS_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavMunicipio_nome3_Internalname = "vMUNICIPIO_NOME3";
         edtavEstado_nome3_Internalname = "vESTADO_NOME3";
         edtavPais_nome3_Internalname = "vPAIS_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         edtEstado_UF_Internalname = "ESTADO_UF";
         edtEstado_Nome_Internalname = "ESTADO_NOME";
         edtPais_Codigo_Internalname = "PAIS_CODIGO";
         edtPais_Nome_Internalname = "PAIS_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfmunicipio_nome_Internalname = "vTFMUNICIPIO_NOME";
         edtavTfmunicipio_nome_sel_Internalname = "vTFMUNICIPIO_NOME_SEL";
         edtavTfestado_nome_Internalname = "vTFESTADO_NOME";
         edtavTfestado_nome_sel_Internalname = "vTFESTADO_NOME_SEL";
         edtavTfpais_nome_Internalname = "vTFPAIS_NOME";
         edtavTfpais_nome_sel_Internalname = "vTFPAIS_NOME_SEL";
         Ddo_municipio_nome_Internalname = "DDO_MUNICIPIO_NOME";
         edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname = "vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_estado_nome_Internalname = "DDO_ESTADO_NOME";
         edtavDdo_estado_nometitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_pais_nome_Internalname = "DDO_PAIS_NOME";
         edtavDdo_pais_nometitlecontrolidtoreplace_Internalname = "vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtPais_Nome_Jsonclick = "";
         edtPais_Codigo_Jsonclick = "";
         edtEstado_Nome_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavPais_nome1_Jsonclick = "";
         edtavEstado_nome1_Jsonclick = "";
         edtavMunicipio_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavPais_nome2_Jsonclick = "";
         edtavEstado_nome2_Jsonclick = "";
         edtavMunicipio_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavPais_nome3_Jsonclick = "";
         edtavEstado_nome3_Jsonclick = "";
         edtavMunicipio_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtPais_Nome_Link = "";
         edtEstado_Nome_Link = "";
         edtMunicipio_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtPais_Nome_Titleformat = 0;
         edtEstado_Nome_Titleformat = 0;
         edtMunicipio_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavPais_nome3_Visible = 1;
         edtavEstado_nome3_Visible = 1;
         edtavMunicipio_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavPais_nome2_Visible = 1;
         edtavEstado_nome2_Visible = 1;
         edtavMunicipio_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavPais_nome1_Visible = 1;
         edtavEstado_nome1_Visible = 1;
         edtavMunicipio_nome1_Visible = 1;
         edtPais_Nome_Title = "Pa�s";
         edtEstado_Nome_Title = "";
         edtMunicipio_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_pais_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfpais_nome_sel_Jsonclick = "";
         edtavTfpais_nome_sel_Visible = 1;
         edtavTfpais_nome_Jsonclick = "";
         edtavTfpais_nome_Visible = 1;
         edtavTfestado_nome_sel_Jsonclick = "";
         edtavTfestado_nome_sel_Visible = 1;
         edtavTfestado_nome_Jsonclick = "";
         edtavTfestado_nome_Visible = 1;
         edtavTfmunicipio_nome_sel_Jsonclick = "";
         edtavTfmunicipio_nome_sel_Visible = 1;
         edtavTfmunicipio_nome_Jsonclick = "";
         edtavTfmunicipio_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_pais_nome_Searchbuttontext = "Pesquisar";
         Ddo_pais_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pais_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_pais_nome_Loadingdata = "Carregando dados...";
         Ddo_pais_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_pais_nome_Sortasc = "Ordenar de A � Z";
         Ddo_pais_nome_Datalistupdateminimumcharacters = 0;
         Ddo_pais_nome_Datalistproc = "GetWWMunicipioFilterData";
         Ddo_pais_nome_Datalisttype = "Dynamic";
         Ddo_pais_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pais_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pais_nome_Filtertype = "Character";
         Ddo_pais_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_pais_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pais_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pais_nome_Titlecontrolidtoreplace = "";
         Ddo_pais_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pais_nome_Cls = "ColumnSettings";
         Ddo_pais_nome_Tooltip = "Op��es";
         Ddo_pais_nome_Caption = "";
         Ddo_estado_nome_Searchbuttontext = "Pesquisar";
         Ddo_estado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_nome_Loadingdata = "Carregando dados...";
         Ddo_estado_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_nome_Sortasc = "Ordenar de A � Z";
         Ddo_estado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_estado_nome_Datalistproc = "GetWWMunicipioFilterData";
         Ddo_estado_nome_Datalisttype = "Dynamic";
         Ddo_estado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_nome_Filtertype = "Character";
         Ddo_estado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_nome_Titlecontrolidtoreplace = "";
         Ddo_estado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_nome_Cls = "ColumnSettings";
         Ddo_estado_nome_Tooltip = "Op��es";
         Ddo_estado_nome_Caption = "";
         Ddo_municipio_nome_Searchbuttontext = "Pesquisar";
         Ddo_municipio_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_municipio_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_municipio_nome_Loadingdata = "Carregando dados...";
         Ddo_municipio_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_municipio_nome_Sortasc = "Ordenar de A � Z";
         Ddo_municipio_nome_Datalistupdateminimumcharacters = 0;
         Ddo_municipio_nome_Datalistproc = "GetWWMunicipioFilterData";
         Ddo_municipio_nome_Datalisttype = "Dynamic";
         Ddo_municipio_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_municipio_nome_Filtertype = "Character";
         Ddo_municipio_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Titlecontrolidtoreplace = "";
         Ddo_municipio_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_municipio_nome_Cls = "ColumnSettings";
         Ddo_municipio_nome_Tooltip = "Op��es";
         Ddo_municipio_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Municipio";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV60Municipio_NomeTitleFilterData',fld:'vMUNICIPIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV68Estado_NomeTitleFilterData',fld:'vESTADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV72Pais_NomeTitleFilterData',fld:'vPAIS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'edtEstado_Nome_Titleformat',ctrl:'ESTADO_NOME',prop:'Titleformat'},{av:'edtEstado_Nome_Title',ctrl:'ESTADO_NOME',prop:'Title'},{av:'edtPais_Nome_Titleformat',ctrl:'PAIS_NOME',prop:'Titleformat'},{av:'edtPais_Nome_Title',ctrl:'PAIS_NOME',prop:'Title'},{av:'AV78GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV79GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV85ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E121C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_MUNICIPIO_NOME.ONOPTIONCLICKED","{handler:'E131C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_municipio_nome_Activeeventkey',ctrl:'DDO_MUNICIPIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_municipio_nome_Filteredtext_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_get'},{av:'Ddo_municipio_nome_Selectedvalue_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_NOME.ONOPTIONCLICKED","{handler:'E141C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_estado_nome_Activeeventkey',ctrl:'DDO_ESTADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_estado_nome_Filteredtext_get',ctrl:'DDO_ESTADO_NOME',prop:'FilteredText_get'},{av:'Ddo_estado_nome_Selectedvalue_get',ctrl:'DDO_ESTADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PAIS_NOME.ONOPTIONCLICKED","{handler:'E151C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_pais_nome_Activeeventkey',ctrl:'DDO_PAIS_NOME',prop:'ActiveEventKey'},{av:'Ddo_pais_nome_Filteredtext_get',ctrl:'DDO_PAIS_NOME',prop:'FilteredText_get'},{av:'Ddo_pais_nome_Selectedvalue_get',ctrl:'DDO_PAIS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E281C2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV34Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV80Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtMunicipio_Nome_Link',ctrl:'MUNICIPIO_NOME',prop:'Link'},{av:'edtEstado_Nome_Link',ctrl:'ESTADO_NOME',prop:'Link'},{av:'edtPais_Nome_Link',ctrl:'PAIS_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E161C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E211C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E171C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E221C2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E231C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E181C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E241C2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E191C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E251C2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E111C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Pais_NomeTitleControlIdToReplace',fld:'vDDO_PAIS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'A21Pais_Codigo',fld:'PAIS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV81ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Filteredtext_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_set'},{av:'AV62TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Selectedvalue_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_set'},{av:'AV69TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'Ddo_estado_nome_Filteredtext_set',ctrl:'DDO_ESTADO_NOME',prop:'FilteredText_set'},{av:'AV70TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_estado_nome_Selectedvalue_set',ctrl:'DDO_ESTADO_NOME',prop:'SelectedValue_set'},{av:'AV73TFPais_Nome',fld:'vTFPAIS_NOME',pic:'@!',nv:''},{av:'Ddo_pais_nome_Filteredtext_set',ctrl:'DDO_PAIS_NOME',prop:'FilteredText_set'},{av:'AV74TFPais_Nome_Sel',fld:'vTFPAIS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pais_nome_Selectedvalue_set',ctrl:'DDO_PAIS_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'},{av:'Ddo_pais_nome_Sortedstatus',ctrl:'DDO_PAIS_NOME',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19Pais_Nome1',fld:'vPAIS_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV24Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Pais_Nome2',fld:'vPAIS_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV30Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV31Pais_Nome3',fld:'vPAIS_NOME3',pic:'@!',nv:''},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'edtavPais_nome1_Visible',ctrl:'vPAIS_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'edtavPais_nome2_Visible',ctrl:'vPAIS_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'edtavPais_nome3_Visible',ctrl:'vPAIS_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E201C2',iparms:[{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_municipio_nome_Activeeventkey = "";
         Ddo_municipio_nome_Filteredtext_get = "";
         Ddo_municipio_nome_Selectedvalue_get = "";
         Ddo_estado_nome_Activeeventkey = "";
         Ddo_estado_nome_Filteredtext_get = "";
         Ddo_estado_nome_Selectedvalue_get = "";
         Ddo_pais_nome_Activeeventkey = "";
         Ddo_pais_nome_Filteredtext_get = "";
         Ddo_pais_nome_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Municipio_Nome1 = "";
         AV18Estado_Nome1 = "";
         AV19Pais_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Municipio_Nome2 = "";
         AV24Estado_Nome2 = "";
         AV25Pais_Nome2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29Municipio_Nome3 = "";
         AV30Estado_Nome3 = "";
         AV31Pais_Nome3 = "";
         AV61TFMunicipio_Nome = "";
         AV62TFMunicipio_Nome_Sel = "";
         AV69TFEstado_Nome = "";
         AV70TFEstado_Nome_Sel = "";
         AV73TFPais_Nome = "";
         AV74TFPais_Nome_Sel = "";
         AV63ddo_Municipio_NomeTitleControlIdToReplace = "";
         AV71ddo_Estado_NomeTitleControlIdToReplace = "";
         AV75ddo_Pais_NomeTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV115Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A23Estado_UF = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV85ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Estado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Pais_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_municipio_nome_Filteredtext_set = "";
         Ddo_municipio_nome_Selectedvalue_set = "";
         Ddo_municipio_nome_Sortedstatus = "";
         Ddo_estado_nome_Filteredtext_set = "";
         Ddo_estado_nome_Selectedvalue_set = "";
         Ddo_estado_nome_Sortedstatus = "";
         Ddo_pais_nome_Filteredtext_set = "";
         Ddo_pais_nome_Selectedvalue_set = "";
         Ddo_pais_nome_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Update = "";
         AV112Update_GXI = "";
         AV35Delete = "";
         AV113Delete_GXI = "";
         AV80Display = "";
         AV114Display_GXI = "";
         A26Municipio_Nome = "";
         A24Estado_Nome = "";
         A22Pais_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV91WWMunicipioDS_3_Municipio_nome1 = "";
         lV92WWMunicipioDS_4_Estado_nome1 = "";
         lV93WWMunicipioDS_5_Pais_nome1 = "";
         lV97WWMunicipioDS_9_Municipio_nome2 = "";
         lV98WWMunicipioDS_10_Estado_nome2 = "";
         lV99WWMunicipioDS_11_Pais_nome2 = "";
         lV103WWMunicipioDS_15_Municipio_nome3 = "";
         lV104WWMunicipioDS_16_Estado_nome3 = "";
         lV105WWMunicipioDS_17_Pais_nome3 = "";
         lV106WWMunicipioDS_18_Tfmunicipio_nome = "";
         lV108WWMunicipioDS_20_Tfestado_nome = "";
         lV110WWMunicipioDS_22_Tfpais_nome = "";
         AV89WWMunicipioDS_1_Dynamicfiltersselector1 = "";
         AV91WWMunicipioDS_3_Municipio_nome1 = "";
         AV92WWMunicipioDS_4_Estado_nome1 = "";
         AV93WWMunicipioDS_5_Pais_nome1 = "";
         AV95WWMunicipioDS_7_Dynamicfiltersselector2 = "";
         AV97WWMunicipioDS_9_Municipio_nome2 = "";
         AV98WWMunicipioDS_10_Estado_nome2 = "";
         AV99WWMunicipioDS_11_Pais_nome2 = "";
         AV101WWMunicipioDS_13_Dynamicfiltersselector3 = "";
         AV103WWMunicipioDS_15_Municipio_nome3 = "";
         AV104WWMunicipioDS_16_Estado_nome3 = "";
         AV105WWMunicipioDS_17_Pais_nome3 = "";
         AV107WWMunicipioDS_19_Tfmunicipio_nome_sel = "";
         AV106WWMunicipioDS_18_Tfmunicipio_nome = "";
         AV109WWMunicipioDS_21_Tfestado_nome_sel = "";
         AV108WWMunicipioDS_20_Tfestado_nome = "";
         AV111WWMunicipioDS_23_Tfpais_nome_sel = "";
         AV110WWMunicipioDS_22_Tfpais_nome = "";
         H001C2_A22Pais_Nome = new String[] {""} ;
         H001C2_A21Pais_Codigo = new int[1] ;
         H001C2_A24Estado_Nome = new String[] {""} ;
         H001C2_A23Estado_UF = new String[] {""} ;
         H001C2_A26Municipio_Nome = new String[] {""} ;
         H001C2_A25Municipio_Codigo = new int[1] ;
         H001C3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV82ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV86ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV83ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV84ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV38Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblMunicipiotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwmunicipio__default(),
            new Object[][] {
                new Object[] {
               H001C2_A22Pais_Nome, H001C2_A21Pais_Codigo, H001C2_A24Estado_Nome, H001C2_A23Estado_UF, H001C2_A26Municipio_Nome, H001C2_A25Municipio_Codigo
               }
               , new Object[] {
               H001C3_AGRID_nRecordCount
               }
            }
         );
         AV115Pgmname = "WWMunicipio";
         /* GeneXus formulas. */
         AV115Pgmname = "WWMunicipio";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short AV81ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV90WWMunicipioDS_2_Dynamicfiltersoperator1 ;
      private short AV96WWMunicipioDS_8_Dynamicfiltersoperator2 ;
      private short AV102WWMunicipioDS_14_Dynamicfiltersoperator3 ;
      private short edtMunicipio_Nome_Titleformat ;
      private short edtEstado_Nome_Titleformat ;
      private short edtPais_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A25Municipio_Codigo ;
      private int A21Pais_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_municipio_nome_Datalistupdateminimumcharacters ;
      private int Ddo_estado_nome_Datalistupdateminimumcharacters ;
      private int Ddo_pais_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfmunicipio_nome_Visible ;
      private int edtavTfmunicipio_nome_sel_Visible ;
      private int edtavTfestado_nome_Visible ;
      private int edtavTfestado_nome_sel_Visible ;
      private int edtavTfpais_nome_Visible ;
      private int edtavTfpais_nome_sel_Visible ;
      private int edtavDdo_municipio_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pais_nometitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV77PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavMunicipio_nome1_Visible ;
      private int edtavEstado_nome1_Visible ;
      private int edtavPais_nome1_Visible ;
      private int edtavMunicipio_nome2_Visible ;
      private int edtavEstado_nome2_Visible ;
      private int edtavPais_nome2_Visible ;
      private int edtavMunicipio_nome3_Visible ;
      private int edtavEstado_nome3_Visible ;
      private int edtavPais_nome3_Visible ;
      private int AV116GXV1 ;
      private int AV117GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV78GridCurrentPage ;
      private long AV79GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_municipio_nome_Activeeventkey ;
      private String Ddo_municipio_nome_Filteredtext_get ;
      private String Ddo_municipio_nome_Selectedvalue_get ;
      private String Ddo_estado_nome_Activeeventkey ;
      private String Ddo_estado_nome_Filteredtext_get ;
      private String Ddo_estado_nome_Selectedvalue_get ;
      private String Ddo_pais_nome_Activeeventkey ;
      private String Ddo_pais_nome_Filteredtext_get ;
      private String Ddo_pais_nome_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV17Municipio_Nome1 ;
      private String AV18Estado_Nome1 ;
      private String AV19Pais_Nome1 ;
      private String AV23Municipio_Nome2 ;
      private String AV24Estado_Nome2 ;
      private String AV25Pais_Nome2 ;
      private String AV29Municipio_Nome3 ;
      private String AV30Estado_Nome3 ;
      private String AV31Pais_Nome3 ;
      private String AV61TFMunicipio_Nome ;
      private String AV62TFMunicipio_Nome_Sel ;
      private String AV69TFEstado_Nome ;
      private String AV70TFEstado_Nome_Sel ;
      private String AV73TFPais_Nome ;
      private String AV74TFPais_Nome_Sel ;
      private String AV115Pgmname ;
      private String A23Estado_UF ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_municipio_nome_Caption ;
      private String Ddo_municipio_nome_Tooltip ;
      private String Ddo_municipio_nome_Cls ;
      private String Ddo_municipio_nome_Filteredtext_set ;
      private String Ddo_municipio_nome_Selectedvalue_set ;
      private String Ddo_municipio_nome_Dropdownoptionstype ;
      private String Ddo_municipio_nome_Titlecontrolidtoreplace ;
      private String Ddo_municipio_nome_Sortedstatus ;
      private String Ddo_municipio_nome_Filtertype ;
      private String Ddo_municipio_nome_Datalisttype ;
      private String Ddo_municipio_nome_Datalistproc ;
      private String Ddo_municipio_nome_Sortasc ;
      private String Ddo_municipio_nome_Sortdsc ;
      private String Ddo_municipio_nome_Loadingdata ;
      private String Ddo_municipio_nome_Cleanfilter ;
      private String Ddo_municipio_nome_Noresultsfound ;
      private String Ddo_municipio_nome_Searchbuttontext ;
      private String Ddo_estado_nome_Caption ;
      private String Ddo_estado_nome_Tooltip ;
      private String Ddo_estado_nome_Cls ;
      private String Ddo_estado_nome_Filteredtext_set ;
      private String Ddo_estado_nome_Selectedvalue_set ;
      private String Ddo_estado_nome_Dropdownoptionstype ;
      private String Ddo_estado_nome_Titlecontrolidtoreplace ;
      private String Ddo_estado_nome_Sortedstatus ;
      private String Ddo_estado_nome_Filtertype ;
      private String Ddo_estado_nome_Datalisttype ;
      private String Ddo_estado_nome_Datalistproc ;
      private String Ddo_estado_nome_Sortasc ;
      private String Ddo_estado_nome_Sortdsc ;
      private String Ddo_estado_nome_Loadingdata ;
      private String Ddo_estado_nome_Cleanfilter ;
      private String Ddo_estado_nome_Noresultsfound ;
      private String Ddo_estado_nome_Searchbuttontext ;
      private String Ddo_pais_nome_Caption ;
      private String Ddo_pais_nome_Tooltip ;
      private String Ddo_pais_nome_Cls ;
      private String Ddo_pais_nome_Filteredtext_set ;
      private String Ddo_pais_nome_Selectedvalue_set ;
      private String Ddo_pais_nome_Dropdownoptionstype ;
      private String Ddo_pais_nome_Titlecontrolidtoreplace ;
      private String Ddo_pais_nome_Sortedstatus ;
      private String Ddo_pais_nome_Filtertype ;
      private String Ddo_pais_nome_Datalisttype ;
      private String Ddo_pais_nome_Datalistproc ;
      private String Ddo_pais_nome_Sortasc ;
      private String Ddo_pais_nome_Sortdsc ;
      private String Ddo_pais_nome_Loadingdata ;
      private String Ddo_pais_nome_Cleanfilter ;
      private String Ddo_pais_nome_Noresultsfound ;
      private String Ddo_pais_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfmunicipio_nome_Internalname ;
      private String edtavTfmunicipio_nome_Jsonclick ;
      private String edtavTfmunicipio_nome_sel_Internalname ;
      private String edtavTfmunicipio_nome_sel_Jsonclick ;
      private String edtavTfestado_nome_Internalname ;
      private String edtavTfestado_nome_Jsonclick ;
      private String edtavTfestado_nome_sel_Internalname ;
      private String edtavTfestado_nome_sel_Jsonclick ;
      private String edtavTfpais_nome_Internalname ;
      private String edtavTfpais_nome_Jsonclick ;
      private String edtavTfpais_nome_sel_Internalname ;
      private String edtavTfpais_nome_sel_Jsonclick ;
      private String edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pais_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String edtEstado_UF_Internalname ;
      private String A24Estado_Nome ;
      private String edtEstado_Nome_Internalname ;
      private String edtPais_Codigo_Internalname ;
      private String A22Pais_Nome ;
      private String edtPais_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV91WWMunicipioDS_3_Municipio_nome1 ;
      private String lV92WWMunicipioDS_4_Estado_nome1 ;
      private String lV93WWMunicipioDS_5_Pais_nome1 ;
      private String lV97WWMunicipioDS_9_Municipio_nome2 ;
      private String lV98WWMunicipioDS_10_Estado_nome2 ;
      private String lV99WWMunicipioDS_11_Pais_nome2 ;
      private String lV103WWMunicipioDS_15_Municipio_nome3 ;
      private String lV104WWMunicipioDS_16_Estado_nome3 ;
      private String lV105WWMunicipioDS_17_Pais_nome3 ;
      private String lV106WWMunicipioDS_18_Tfmunicipio_nome ;
      private String lV108WWMunicipioDS_20_Tfestado_nome ;
      private String lV110WWMunicipioDS_22_Tfpais_nome ;
      private String AV91WWMunicipioDS_3_Municipio_nome1 ;
      private String AV92WWMunicipioDS_4_Estado_nome1 ;
      private String AV93WWMunicipioDS_5_Pais_nome1 ;
      private String AV97WWMunicipioDS_9_Municipio_nome2 ;
      private String AV98WWMunicipioDS_10_Estado_nome2 ;
      private String AV99WWMunicipioDS_11_Pais_nome2 ;
      private String AV103WWMunicipioDS_15_Municipio_nome3 ;
      private String AV104WWMunicipioDS_16_Estado_nome3 ;
      private String AV105WWMunicipioDS_17_Pais_nome3 ;
      private String AV107WWMunicipioDS_19_Tfmunicipio_nome_sel ;
      private String AV106WWMunicipioDS_18_Tfmunicipio_nome ;
      private String AV109WWMunicipioDS_21_Tfestado_nome_sel ;
      private String AV108WWMunicipioDS_20_Tfestado_nome ;
      private String AV111WWMunicipioDS_23_Tfpais_nome_sel ;
      private String AV110WWMunicipioDS_22_Tfpais_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavMunicipio_nome1_Internalname ;
      private String edtavEstado_nome1_Internalname ;
      private String edtavPais_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavMunicipio_nome2_Internalname ;
      private String edtavEstado_nome2_Internalname ;
      private String edtavPais_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavMunicipio_nome3_Internalname ;
      private String edtavEstado_nome3_Internalname ;
      private String edtavPais_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_municipio_nome_Internalname ;
      private String Ddo_estado_nome_Internalname ;
      private String Ddo_pais_nome_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtMunicipio_Nome_Title ;
      private String edtEstado_Nome_Title ;
      private String edtPais_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtMunicipio_Nome_Link ;
      private String edtEstado_Nome_Link ;
      private String edtPais_Nome_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavMunicipio_nome3_Jsonclick ;
      private String edtavEstado_nome3_Jsonclick ;
      private String edtavPais_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavMunicipio_nome2_Jsonclick ;
      private String edtavEstado_nome2_Jsonclick ;
      private String edtavPais_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavMunicipio_nome1_Jsonclick ;
      private String edtavEstado_nome1_Jsonclick ;
      private String edtavPais_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblMunicipiotitle_Internalname ;
      private String lblMunicipiotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String edtEstado_Nome_Jsonclick ;
      private String edtPais_Codigo_Jsonclick ;
      private String edtPais_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_municipio_nome_Includesortasc ;
      private bool Ddo_municipio_nome_Includesortdsc ;
      private bool Ddo_municipio_nome_Includefilter ;
      private bool Ddo_municipio_nome_Filterisrange ;
      private bool Ddo_municipio_nome_Includedatalist ;
      private bool Ddo_estado_nome_Includesortasc ;
      private bool Ddo_estado_nome_Includesortdsc ;
      private bool Ddo_estado_nome_Includefilter ;
      private bool Ddo_estado_nome_Filterisrange ;
      private bool Ddo_estado_nome_Includedatalist ;
      private bool Ddo_pais_nome_Includesortasc ;
      private bool Ddo_pais_nome_Includesortdsc ;
      private bool Ddo_pais_nome_Includefilter ;
      private bool Ddo_pais_nome_Filterisrange ;
      private bool Ddo_pais_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV94WWMunicipioDS_6_Dynamicfiltersenabled2 ;
      private bool AV100WWMunicipioDS_12_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private bool AV80Display_IsBlob ;
      private String AV82ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV63ddo_Municipio_NomeTitleControlIdToReplace ;
      private String AV71ddo_Estado_NomeTitleControlIdToReplace ;
      private String AV75ddo_Pais_NomeTitleControlIdToReplace ;
      private String AV112Update_GXI ;
      private String AV113Delete_GXI ;
      private String AV114Display_GXI ;
      private String AV89WWMunicipioDS_1_Dynamicfiltersselector1 ;
      private String AV95WWMunicipioDS_7_Dynamicfiltersselector2 ;
      private String AV101WWMunicipioDS_13_Dynamicfiltersselector3 ;
      private String AV34Update ;
      private String AV35Delete ;
      private String AV80Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV38Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H001C2_A22Pais_Nome ;
      private int[] H001C2_A21Pais_Codigo ;
      private String[] H001C2_A24Estado_Nome ;
      private String[] H001C2_A23Estado_UF ;
      private String[] H001C2_A26Municipio_Nome ;
      private int[] H001C2_A25Municipio_Codigo ;
      private long[] H001C3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV85ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60Municipio_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68Estado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72Pais_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV83ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV86ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV76DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV84ManageFiltersItem ;
   }

   public class wwmunicipio__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H001C2( IGxContext context ,
                                             String AV89WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                             short AV90WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                             String AV91WWMunicipioDS_3_Municipio_nome1 ,
                                             String AV92WWMunicipioDS_4_Estado_nome1 ,
                                             String AV93WWMunicipioDS_5_Pais_nome1 ,
                                             bool AV94WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                             short AV96WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                             String AV97WWMunicipioDS_9_Municipio_nome2 ,
                                             String AV98WWMunicipioDS_10_Estado_nome2 ,
                                             String AV99WWMunicipioDS_11_Pais_nome2 ,
                                             bool AV100WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                             short AV102WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                             String AV103WWMunicipioDS_15_Municipio_nome3 ,
                                             String AV104WWMunicipioDS_16_Estado_nome3 ,
                                             String AV105WWMunicipioDS_17_Pais_nome3 ,
                                             String AV107WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                             String AV106WWMunicipioDS_18_Tfmunicipio_nome ,
                                             String AV109WWMunicipioDS_21_Tfestado_nome_sel ,
                                             String AV108WWMunicipioDS_20_Tfestado_nome ,
                                             String AV111WWMunicipioDS_23_Tfpais_nome_sel ,
                                             String AV110WWMunicipioDS_22_Tfpais_nome ,
                                             String A26Municipio_Nome ,
                                             String A24Estado_Nome ,
                                             String A22Pais_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [29] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Pais_Nome], T1.[Pais_Codigo], T3.[Estado_Nome], T1.[Estado_UF], T1.[Municipio_Nome], T1.[Municipio_Codigo]";
         sFromString = " FROM (([Municipio] T1 WITH (NOLOCK) INNER JOIN [Pais] T2 WITH (NOLOCK) ON T2.[Pais_Codigo] = T1.[Pais_Codigo]) INNER JOIN [Estado] T3 WITH (NOLOCK) ON T3.[Estado_UF] = T1.[Estado_UF])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV92WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV92WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV92WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV92WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV93WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV93WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV93WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV93WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV98WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV98WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV98WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV98WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV99WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV99WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV99WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV99WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV104WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV104WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV104WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV104WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV105WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV105WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV105WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV105WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWMunicipioDS_18_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV106WWMunicipioDS_18_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV106WWMunicipioDS_18_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] = @AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] = @AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMunicipioDS_21_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMunicipioDS_20_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV108WWMunicipioDS_20_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV108WWMunicipioDS_20_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMunicipioDS_21_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] = @AV109WWMunicipioDS_21_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] = @AV109WWMunicipioDS_21_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWMunicipioDS_23_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMunicipioDS_22_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV110WWMunicipioDS_22_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV110WWMunicipioDS_22_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWMunicipioDS_23_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] = @AV111WWMunicipioDS_23_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] = @AV111WWMunicipioDS_23_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Estado_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Estado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pais_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pais_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H001C3( IGxContext context ,
                                             String AV89WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                             short AV90WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                             String AV91WWMunicipioDS_3_Municipio_nome1 ,
                                             String AV92WWMunicipioDS_4_Estado_nome1 ,
                                             String AV93WWMunicipioDS_5_Pais_nome1 ,
                                             bool AV94WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                             String AV95WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                             short AV96WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                             String AV97WWMunicipioDS_9_Municipio_nome2 ,
                                             String AV98WWMunicipioDS_10_Estado_nome2 ,
                                             String AV99WWMunicipioDS_11_Pais_nome2 ,
                                             bool AV100WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                             String AV101WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                             short AV102WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                             String AV103WWMunicipioDS_15_Municipio_nome3 ,
                                             String AV104WWMunicipioDS_16_Estado_nome3 ,
                                             String AV105WWMunicipioDS_17_Pais_nome3 ,
                                             String AV107WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                             String AV106WWMunicipioDS_18_Tfmunicipio_nome ,
                                             String AV109WWMunicipioDS_21_Tfestado_nome_sel ,
                                             String AV108WWMunicipioDS_20_Tfestado_nome ,
                                             String AV111WWMunicipioDS_23_Tfpais_nome_sel ,
                                             String AV110WWMunicipioDS_22_Tfpais_nome ,
                                             String A26Municipio_Nome ,
                                             String A24Estado_Nome ,
                                             String A22Pais_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Municipio] T1 WITH (NOLOCK) INNER JOIN [Pais] T3 WITH (NOLOCK) ON T3.[Pais_Codigo] = T1.[Pais_Codigo]) INNER JOIN [Estado] T2 WITH (NOLOCK) ON T2.[Estado_UF] = T1.[Estado_UF])";
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV91WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV92WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV92WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV92WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV92WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV93WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV93WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV89WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV90WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV93WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV93WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV97WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV98WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV98WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV98WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV98WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV99WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV99WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV94WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV96WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV99WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV99WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV103WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV104WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV104WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV104WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV104WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV105WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV105WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV100WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV101WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV102WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV105WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV105WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWMunicipioDS_18_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV106WWMunicipioDS_18_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV106WWMunicipioDS_18_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] = @AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] = @AV107WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMunicipioDS_21_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMunicipioDS_20_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV108WWMunicipioDS_20_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV108WWMunicipioDS_20_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMunicipioDS_21_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] = @AV109WWMunicipioDS_21_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] = @AV109WWMunicipioDS_21_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWMunicipioDS_23_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMunicipioDS_22_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV110WWMunicipioDS_22_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV110WWMunicipioDS_22_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWMunicipioDS_23_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] = @AV111WWMunicipioDS_23_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] = @AV111WWMunicipioDS_23_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H001C2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H001C3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001C2 ;
          prmH001C2 = new Object[] {
          new Object[] {"@lV91WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV91WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV105WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV105WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWMunicipioDS_18_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV107WWMunicipioDS_19_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV108WWMunicipioDS_20_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV109WWMunicipioDS_21_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV110WWMunicipioDS_22_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV111WWMunicipioDS_23_Tfpais_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH001C3 ;
          prmH001C3 = new Object[] {
          new Object[] {"@lV91WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV91WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV98WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV105WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV105WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV106WWMunicipioDS_18_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV107WWMunicipioDS_19_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV108WWMunicipioDS_20_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV109WWMunicipioDS_21_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV110WWMunicipioDS_22_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV111WWMunicipioDS_23_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001C2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001C2,11,0,true,false )
             ,new CursorDef("H001C3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001C3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

}
