/*
               File: AmbienteTecnologicoSistemas
        Description: Ambiente Tecnologico Sistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:44.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ambientetecnologicosistemas : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public ambientetecnologicosistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public ambientetecnologicosistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AmbienteTecnologico_Codigo )
      {
         this.AV7AmbienteTecnologico_Codigo = aP0_AmbienteTecnologico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavSistema_ativo = new GXCheckbox();
         cmbavSistema_tipo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7AmbienteTecnologico_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_75 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_75_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_75_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV18DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
                  AV19Sistema_Sigla1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
                  AV20Sistema_Coordenacao1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Sistema_Coordenacao1", AV20Sistema_Coordenacao1);
                  AV22DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
                  AV23Sistema_Sigla2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
                  AV24Sistema_Coordenacao2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Sistema_Coordenacao2", AV24Sistema_Coordenacao2);
                  AV26DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  AV27Sistema_Sigla3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
                  AV28Sistema_Coordenacao3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Sistema_Coordenacao3", AV28Sistema_Coordenacao3);
                  AV21DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
                  AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV35TFSistema_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
                  AV36TFSistema_Sigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSistema_Sigla_Sel", AV36TFSistema_Sigla_Sel);
                  AV39TFSistema_Coordenacao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
                  AV40TFSistema_Coordenacao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSistema_Coordenacao_Sel", AV40TFSistema_Coordenacao_Sel);
                  AV7AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0)));
                  AV37ddo_Sistema_SiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_Sistema_SiglaTitleControlIdToReplace", AV37ddo_Sistema_SiglaTitleControlIdToReplace);
                  AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace);
                  AV48Pgmname = GetNextPar( );
                  AV16Sistema_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Sistema_Ativo", AV16Sistema_Ativo);
                  AV17Sistema_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
                  AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV48Pgmname = "AmbienteTecnologicoSistemas";
               context.Gx_err = 0;
               WSGN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Ambiente Tecnologico Sistemas") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311764520");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("ambientetecnologicosistemas.aspx") + "?" + UrlEncode("" +AV7AmbienteTecnologico_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV18DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_SIGLA1", StringUtil.RTrim( AV19Sistema_Sigla1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_COORDENACAO1", AV20Sistema_Coordenacao1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV22DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_SIGLA2", StringUtil.RTrim( AV23Sistema_Sigla2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_COORDENACAO2", AV24Sistema_Coordenacao2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_SIGLA3", StringUtil.RTrim( AV27Sistema_Sigla3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSISTEMA_COORDENACAO3", AV28Sistema_Coordenacao3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV21DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMA_SIGLA", StringUtil.RTrim( AV35TFSistema_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMA_SIGLA_SEL", StringUtil.RTrim( AV36TFSistema_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMA_COORDENACAO", AV39TFSistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMA_COORDENACAO_SEL", AV40TFSistema_Coordenacao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_75", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_75), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSISTEMA_SIGLATITLEFILTERDATA", AV34Sistema_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSISTEMA_SIGLATITLEFILTERDATA", AV34Sistema_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSISTEMA_COORDENACAOTITLEFILTERDATA", AV38Sistema_CoordenacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSISTEMA_COORDENACAOTITLEFILTERDATA", AV38Sistema_CoordenacaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vAMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV48Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Caption", StringUtil.RTrim( Ddo_sistema_sigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Tooltip", StringUtil.RTrim( Ddo_sistema_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Cls", StringUtil.RTrim( Ddo_sistema_sigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_sistema_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_sistema_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Filtertype", StringUtil.RTrim( Ddo_sistema_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_sistema_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_sistema_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Sortasc", StringUtil.RTrim( Ddo_sistema_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_sistema_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_sistema_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_sistema_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_sistema_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Caption", StringUtil.RTrim( Ddo_sistema_coordenacao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Tooltip", StringUtil.RTrim( Ddo_sistema_coordenacao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Cls", StringUtil.RTrim( Ddo_sistema_coordenacao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_coordenacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_coordenacao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_coordenacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_coordenacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Sortedstatus", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Includefilter", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Filtertype", StringUtil.RTrim( Ddo_sistema_coordenacao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Datalisttype", StringUtil.RTrim( Ddo_sistema_coordenacao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Datalistproc", StringUtil.RTrim( Ddo_sistema_coordenacao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_coordenacao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Sortasc", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Sortdsc", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Loadingdata", StringUtil.RTrim( Ddo_sistema_coordenacao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Cleanfilter", StringUtil.RTrim( Ddo_sistema_coordenacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Noresultsfound", StringUtil.RTrim( Ddo_sistema_coordenacao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_coordenacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_sistema_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Activeeventkey", StringUtil.RTrim( Ddo_sistema_coordenacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_coordenacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMA_COORDENACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_coordenacao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGN2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("ambientetecnologicosistemas.js", "?2020311764592");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AmbienteTecnologicoSistemas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Ambiente Tecnologico Sistemas" ;
      }

      protected void WBGN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "ambientetecnologicosistemas.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GN2( true) ;
         }
         else
         {
            wb_table1_2_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAmbienteTecnologico_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AmbienteTecnologicoSistemas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(85, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_Internalname, StringUtil.RTrim( AV35TFSistema_Sigla), StringUtil.RTrim( context.localUtil.Format( AV35TFSistema_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_sel_Internalname, StringUtil.RTrim( AV36TFSistema_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV36TFSistema_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_sel_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_coordenacao_Internalname, AV39TFSistema_Coordenacao, StringUtil.RTrim( context.localUtil.Format( AV39TFSistema_Coordenacao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_coordenacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_coordenacao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_coordenacao_sel_Internalname, AV40TFSistema_Coordenacao_Sel, StringUtil.RTrim( context.localUtil.Format( AV40TFSistema_Coordenacao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_coordenacao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_coordenacao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SISTEMA_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, AV37ddo_Sistema_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", 0, edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AmbienteTecnologicoSistemas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SISTEMA_COORDENACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", 0, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AmbienteTecnologicoSistemas.htm");
         }
         wbLoad = true;
      }

      protected void STARTGN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Ambiente Tecnologico Sistemas", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGN0( ) ;
            }
         }
      }

      protected void WSGN2( )
      {
         STARTGN2( ) ;
         EVTGN2( ) ;
      }

      protected void EVTGN2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GN2 */
                                    E11GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GN2 */
                                    E12GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_COORDENACAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GN2 */
                                    E13GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GN2 */
                                    E14GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15GN2 */
                                    E15GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16GN2 */
                                    E16GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17GN2 */
                                    E17GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18GN2 */
                                    E18GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19GN2 */
                                    E19GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20GN2 */
                                    E20GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21GN2 */
                                    E21GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22GN2 */
                                    E22GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23GN2 */
                                    E23GN2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGN0( ) ;
                              }
                              nGXsfl_75_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
                              SubsflControlProps_752( ) ;
                              A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                              A129Sistema_Sigla = StringUtil.Upper( cgiGet( edtSistema_Sigla_Internalname));
                              A513Sistema_Coordenacao = StringUtil.Upper( cgiGet( edtSistema_Coordenacao_Internalname));
                              n513Sistema_Coordenacao = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24GN2 */
                                          E24GN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25GN2 */
                                          E25GN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26GN2 */
                                          E26GN2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV18DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_sigla1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA1"), AV19Sistema_Sigla1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_coordenacao1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO1"), AV20Sistema_Coordenacao1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_sigla2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA2"), AV23Sistema_Sigla2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_coordenacao2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO2"), AV24Sistema_Coordenacao2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_sigla3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA3"), AV27Sistema_Sigla3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Sistema_coordenacao3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO3"), AV28Sistema_Coordenacao3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistema_sigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_SIGLA"), AV35TFSistema_Sigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistema_sigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_SIGLA_SEL"), AV36TFSistema_Sigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistema_coordenacao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_COORDENACAO"), AV39TFSistema_Coordenacao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistema_coordenacao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_COORDENACAO_SEL"), AV40TFSistema_Coordenacao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPGN0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGN2( ) ;
            }
         }
      }

      protected void PAGN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            chkavSistema_ativo.Name = "vSISTEMA_ATIVO";
            chkavSistema_ativo.WebTags = "";
            chkavSistema_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavSistema_ativo_Internalname, "TitleCaption", chkavSistema_ativo.Caption);
            chkavSistema_ativo.CheckedValue = "false";
            cmbavSistema_tipo.Name = "vSISTEMA_TIPO";
            cmbavSistema_tipo.WebTags = "";
            cmbavSistema_tipo.addItem("", "Todos", 0);
            cmbavSistema_tipo.addItem("D", "Desenvolvimento", 0);
            cmbavSistema_tipo.addItem("M", "Melhoria", 0);
            cmbavSistema_tipo.addItem("A", "Aplica��o", 0);
            if ( cmbavSistema_tipo.ItemCount > 0 )
            {
               AV17Sistema_Tipo = cmbavSistema_tipo.getValidValue(AV17Sistema_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SISTEMA_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_COORDENACAO", "�rea gestora", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV18DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SISTEMA_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_COORDENACAO", "�rea gestora", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SISTEMA_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("SISTEMA_COORDENACAO", "�rea gestora", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_752( ) ;
         while ( nGXsfl_75_idx <= nRC_GXsfl_75 )
         {
            sendrow_752( ) ;
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV18DynamicFiltersSelector1 ,
                                       String AV19Sistema_Sigla1 ,
                                       String AV20Sistema_Coordenacao1 ,
                                       String AV22DynamicFiltersSelector2 ,
                                       String AV23Sistema_Sigla2 ,
                                       String AV24Sistema_Coordenacao2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       String AV27Sistema_Sigla3 ,
                                       String AV28Sistema_Coordenacao3 ,
                                       bool AV21DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       String AV35TFSistema_Sigla ,
                                       String AV36TFSistema_Sigla_Sel ,
                                       String AV39TFSistema_Coordenacao ,
                                       String AV40TFSistema_Coordenacao_Sel ,
                                       int AV7AmbienteTecnologico_Codigo ,
                                       String AV37ddo_Sistema_SiglaTitleControlIdToReplace ,
                                       String AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace ,
                                       String AV48Pgmname ,
                                       bool AV16Sistema_Ativo ,
                                       String AV17Sistema_Tipo ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGN2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_COORDENACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMA_COORDENACAO", A513Sistema_Coordenacao);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavSistema_tipo.ItemCount > 0 )
         {
            AV17Sistema_Tipo = cmbavSistema_tipo.getValidValue(AV17Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV18DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV48Pgmname = "AmbienteTecnologicoSistemas";
         context.Gx_err = 0;
      }

      protected void RFGN2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 75;
         /* Execute user event: E25GN2 */
         E25GN2 ();
         nGXsfl_75_idx = 1;
         sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
         SubsflControlProps_752( ) ;
         nGXsfl_75_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_752( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV18DynamicFiltersSelector1 ,
                                                 AV19Sistema_Sigla1 ,
                                                 AV20Sistema_Coordenacao1 ,
                                                 AV21DynamicFiltersEnabled2 ,
                                                 AV22DynamicFiltersSelector2 ,
                                                 AV23Sistema_Sigla2 ,
                                                 AV24Sistema_Coordenacao2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27Sistema_Sigla3 ,
                                                 AV28Sistema_Coordenacao3 ,
                                                 AV36TFSistema_Sigla_Sel ,
                                                 AV35TFSistema_Sigla ,
                                                 AV40TFSistema_Coordenacao_Sel ,
                                                 AV39TFSistema_Coordenacao ,
                                                 A129Sistema_Sigla ,
                                                 A513Sistema_Coordenacao ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A351AmbienteTecnologico_Codigo ,
                                                 AV7AmbienteTecnologico_Codigo ,
                                                 A130Sistema_Ativo ,
                                                 A699Sistema_Tipo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            lV19Sistema_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV19Sistema_Sigla1), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
            lV20Sistema_Coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV20Sistema_Coordenacao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Sistema_Coordenacao1", AV20Sistema_Coordenacao1);
            lV23Sistema_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV23Sistema_Sigla2), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
            lV24Sistema_Coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV24Sistema_Coordenacao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Sistema_Coordenacao2", AV24Sistema_Coordenacao2);
            lV27Sistema_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV27Sistema_Sigla3), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
            lV28Sistema_Coordenacao3 = StringUtil.Concat( StringUtil.RTrim( AV28Sistema_Coordenacao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Sistema_Coordenacao3", AV28Sistema_Coordenacao3);
            lV35TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV35TFSistema_Sigla), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
            lV39TFSistema_Coordenacao = StringUtil.Concat( StringUtil.RTrim( AV39TFSistema_Coordenacao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
            /* Using cursor H00GN2 */
            pr_default.execute(0, new Object[] {AV7AmbienteTecnologico_Codigo, lV19Sistema_Sigla1, lV20Sistema_Coordenacao1, lV23Sistema_Sigla2, lV24Sistema_Coordenacao2, lV27Sistema_Sigla3, lV28Sistema_Coordenacao3, lV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, lV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_75_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A130Sistema_Ativo = H00GN2_A130Sistema_Ativo[0];
               A699Sistema_Tipo = H00GN2_A699Sistema_Tipo[0];
               n699Sistema_Tipo = H00GN2_n699Sistema_Tipo[0];
               A351AmbienteTecnologico_Codigo = H00GN2_A351AmbienteTecnologico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               n351AmbienteTecnologico_Codigo = H00GN2_n351AmbienteTecnologico_Codigo[0];
               A513Sistema_Coordenacao = H00GN2_A513Sistema_Coordenacao[0];
               n513Sistema_Coordenacao = H00GN2_n513Sistema_Coordenacao[0];
               A129Sistema_Sigla = H00GN2_A129Sistema_Sigla[0];
               A127Sistema_Codigo = H00GN2_A127Sistema_Codigo[0];
               /* Execute user event: E26GN2 */
               E26GN2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 75;
            WBGN0( ) ;
         }
         nGXsfl_75_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV18DynamicFiltersSelector1 ,
                                              AV19Sistema_Sigla1 ,
                                              AV20Sistema_Coordenacao1 ,
                                              AV21DynamicFiltersEnabled2 ,
                                              AV22DynamicFiltersSelector2 ,
                                              AV23Sistema_Sigla2 ,
                                              AV24Sistema_Coordenacao2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27Sistema_Sigla3 ,
                                              AV28Sistema_Coordenacao3 ,
                                              AV36TFSistema_Sigla_Sel ,
                                              AV35TFSistema_Sigla ,
                                              AV40TFSistema_Coordenacao_Sel ,
                                              AV39TFSistema_Coordenacao ,
                                              A129Sistema_Sigla ,
                                              A513Sistema_Coordenacao ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A351AmbienteTecnologico_Codigo ,
                                              AV7AmbienteTecnologico_Codigo ,
                                              A130Sistema_Ativo ,
                                              A699Sistema_Tipo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV19Sistema_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV19Sistema_Sigla1), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
         lV20Sistema_Coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV20Sistema_Coordenacao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Sistema_Coordenacao1", AV20Sistema_Coordenacao1);
         lV23Sistema_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV23Sistema_Sigla2), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
         lV24Sistema_Coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV24Sistema_Coordenacao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Sistema_Coordenacao2", AV24Sistema_Coordenacao2);
         lV27Sistema_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV27Sistema_Sigla3), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
         lV28Sistema_Coordenacao3 = StringUtil.Concat( StringUtil.RTrim( AV28Sistema_Coordenacao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Sistema_Coordenacao3", AV28Sistema_Coordenacao3);
         lV35TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV35TFSistema_Sigla), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
         lV39TFSistema_Coordenacao = StringUtil.Concat( StringUtil.RTrim( AV39TFSistema_Coordenacao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
         /* Using cursor H00GN3 */
         pr_default.execute(1, new Object[] {AV7AmbienteTecnologico_Codigo, lV19Sistema_Sigla1, lV20Sistema_Coordenacao1, lV23Sistema_Sigla2, lV24Sistema_Coordenacao2, lV27Sistema_Sigla3, lV28Sistema_Coordenacao3, lV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, lV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel});
         GRID_nRecordCount = H00GN3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGN0( )
      {
         /* Before Start, stand alone formulas. */
         AV48Pgmname = "AmbienteTecnologicoSistemas";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24GN2 */
         E24GN2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV42DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSISTEMA_SIGLATITLEFILTERDATA"), AV34Sistema_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSISTEMA_COORDENACAOTITLEFILTERDATA"), AV38Sistema_CoordenacaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV16Sistema_Ativo = StringUtil.StrToBool( cgiGet( chkavSistema_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Sistema_Ativo", AV16Sistema_Ativo);
            cmbavSistema_tipo.Name = cmbavSistema_tipo_Internalname;
            cmbavSistema_tipo.CurrentValue = cgiGet( cmbavSistema_tipo_Internalname);
            AV17Sistema_Tipo = cgiGet( cmbavSistema_tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV18DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            AV19Sistema_Sigla1 = StringUtil.Upper( cgiGet( edtavSistema_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
            AV20Sistema_Coordenacao1 = StringUtil.Upper( cgiGet( edtavSistema_coordenacao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Sistema_Coordenacao1", AV20Sistema_Coordenacao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV22DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            AV23Sistema_Sigla2 = StringUtil.Upper( cgiGet( edtavSistema_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
            AV24Sistema_Coordenacao2 = StringUtil.Upper( cgiGet( edtavSistema_coordenacao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Sistema_Coordenacao2", AV24Sistema_Coordenacao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            AV27Sistema_Sigla3 = StringUtil.Upper( cgiGet( edtavSistema_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
            AV28Sistema_Coordenacao3 = StringUtil.Upper( cgiGet( edtavSistema_coordenacao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Sistema_Coordenacao3", AV28Sistema_Coordenacao3);
            A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            AV21DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            AV35TFSistema_Sigla = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
            AV36TFSistema_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSistema_Sigla_Sel", AV36TFSistema_Sigla_Sel);
            AV39TFSistema_Coordenacao = StringUtil.Upper( cgiGet( edtavTfsistema_coordenacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
            AV40TFSistema_Coordenacao_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_coordenacao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSistema_Coordenacao_Sel", AV40TFSistema_Coordenacao_Sel);
            AV37ddo_Sistema_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_Sistema_SiglaTitleControlIdToReplace", AV37ddo_Sistema_SiglaTitleControlIdToReplace);
            AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace = cgiGet( edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_75 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_75"), ",", "."));
            AV44GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV45GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AmbienteTecnologico_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_sistema_sigla_Caption = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Caption");
            Ddo_sistema_sigla_Tooltip = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Tooltip");
            Ddo_sistema_sigla_Cls = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Cls");
            Ddo_sistema_sigla_Filteredtext_set = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Filteredtext_set");
            Ddo_sistema_sigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Selectedvalue_set");
            Ddo_sistema_sigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Dropdownoptionstype");
            Ddo_sistema_sigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace");
            Ddo_sistema_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Includesortasc"));
            Ddo_sistema_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Includesortdsc"));
            Ddo_sistema_sigla_Sortedstatus = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Sortedstatus");
            Ddo_sistema_sigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Includefilter"));
            Ddo_sistema_sigla_Filtertype = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Filtertype");
            Ddo_sistema_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Filterisrange"));
            Ddo_sistema_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Includedatalist"));
            Ddo_sistema_sigla_Datalisttype = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Datalisttype");
            Ddo_sistema_sigla_Datalistproc = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Datalistproc");
            Ddo_sistema_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_sigla_Sortasc = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Sortasc");
            Ddo_sistema_sigla_Sortdsc = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Sortdsc");
            Ddo_sistema_sigla_Loadingdata = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Loadingdata");
            Ddo_sistema_sigla_Cleanfilter = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Cleanfilter");
            Ddo_sistema_sigla_Noresultsfound = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Noresultsfound");
            Ddo_sistema_sigla_Searchbuttontext = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Searchbuttontext");
            Ddo_sistema_coordenacao_Caption = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Caption");
            Ddo_sistema_coordenacao_Tooltip = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Tooltip");
            Ddo_sistema_coordenacao_Cls = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Cls");
            Ddo_sistema_coordenacao_Filteredtext_set = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Filteredtext_set");
            Ddo_sistema_coordenacao_Selectedvalue_set = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Selectedvalue_set");
            Ddo_sistema_coordenacao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Dropdownoptionstype");
            Ddo_sistema_coordenacao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Titlecontrolidtoreplace");
            Ddo_sistema_coordenacao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Includesortasc"));
            Ddo_sistema_coordenacao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Includesortdsc"));
            Ddo_sistema_coordenacao_Sortedstatus = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Sortedstatus");
            Ddo_sistema_coordenacao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Includefilter"));
            Ddo_sistema_coordenacao_Filtertype = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Filtertype");
            Ddo_sistema_coordenacao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Filterisrange"));
            Ddo_sistema_coordenacao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Includedatalist"));
            Ddo_sistema_coordenacao_Datalisttype = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Datalisttype");
            Ddo_sistema_coordenacao_Datalistproc = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Datalistproc");
            Ddo_sistema_coordenacao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_coordenacao_Sortasc = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Sortasc");
            Ddo_sistema_coordenacao_Sortdsc = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Sortdsc");
            Ddo_sistema_coordenacao_Loadingdata = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Loadingdata");
            Ddo_sistema_coordenacao_Cleanfilter = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Cleanfilter");
            Ddo_sistema_coordenacao_Noresultsfound = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Noresultsfound");
            Ddo_sistema_coordenacao_Searchbuttontext = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_sistema_sigla_Activeeventkey = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Activeeventkey");
            Ddo_sistema_sigla_Filteredtext_get = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Filteredtext_get");
            Ddo_sistema_sigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_SISTEMA_SIGLA_Selectedvalue_get");
            Ddo_sistema_coordenacao_Activeeventkey = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Activeeventkey");
            Ddo_sistema_coordenacao_Filteredtext_get = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Filteredtext_get");
            Ddo_sistema_coordenacao_Selectedvalue_get = cgiGet( sPrefix+"DDO_SISTEMA_COORDENACAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV18DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA1"), AV19Sistema_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO1"), AV20Sistema_Coordenacao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA2"), AV23Sistema_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO2"), AV24Sistema_Coordenacao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_SIGLA3"), AV27Sistema_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSISTEMA_COORDENACAO3"), AV28Sistema_Coordenacao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_SIGLA"), AV35TFSistema_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_SIGLA_SEL"), AV36TFSistema_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_COORDENACAO"), AV39TFSistema_Coordenacao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMA_COORDENACAO_SEL"), AV40TFSistema_Coordenacao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24GN2 */
         E24GN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24GN2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV18DynamicFiltersSelector1 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector2 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         AV17Sistema_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
         edtavTfsistema_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistema_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_Visible), 5, 0)));
         edtavTfsistema_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistema_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_sel_Visible), 5, 0)));
         edtavTfsistema_coordenacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistema_coordenacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_coordenacao_Visible), 5, 0)));
         edtavTfsistema_coordenacao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistema_coordenacao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_coordenacao_sel_Visible), 5, 0)));
         Ddo_sistema_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "TitleControlIdToReplace", Ddo_sistema_sigla_Titlecontrolidtoreplace);
         AV37ddo_Sistema_SiglaTitleControlIdToReplace = Ddo_sistema_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_Sistema_SiglaTitleControlIdToReplace", AV37ddo_Sistema_SiglaTitleControlIdToReplace);
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_coordenacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Coordenacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "TitleControlIdToReplace", Ddo_sistema_coordenacao_Titlecontrolidtoreplace);
         AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace = Ddo_sistema_coordenacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace);
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible), 5, 0)));
         edtAmbienteTecnologico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAmbienteTecnologico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Sigla", 0);
         cmbavOrderedby.addItem("2", "�rea gestora", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV42DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV42DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25GN2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Sistema_CoordenacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSistema_Sigla_Titleformat = 2;
         edtSistema_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV37ddo_Sistema_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_Sigla_Internalname, "Title", edtSistema_Sigla_Title);
         edtSistema_Coordenacao_Titleformat = 2;
         edtSistema_Coordenacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�rea gestora", AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_Coordenacao_Internalname, "Title", edtSistema_Coordenacao_Title);
         AV44GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridCurrentPage), 10, 0)));
         AV45GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34Sistema_SiglaTitleFilterData", AV34Sistema_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38Sistema_CoordenacaoTitleFilterData", AV38Sistema_CoordenacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11GN2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV43PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV43PageToGo) ;
         }
      }

      protected void E12GN2( )
      {
         /* Ddo_sistema_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFSistema_Sigla = Ddo_sistema_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
            AV36TFSistema_Sigla_Sel = Ddo_sistema_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSistema_Sigla_Sel", AV36TFSistema_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GN2( )
      {
         /* Ddo_sistema_coordenacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_sistema_coordenacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_sistema_coordenacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFSistema_Coordenacao = Ddo_sistema_coordenacao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
            AV40TFSistema_Coordenacao_Sel = Ddo_sistema_coordenacao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSistema_Coordenacao_Sel", AV40TFSistema_Coordenacao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26GN2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 75;
         }
         sendrow_752( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_75_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(75, GridRow);
         }
      }

      protected void E14GN2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E19GN2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV21DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15GN2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20GN2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21GN2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16GN2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22GN2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17GN2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV18DynamicFiltersSelector1, AV19Sistema_Sigla1, AV20Sistema_Coordenacao1, AV22DynamicFiltersSelector2, AV23Sistema_Sigla2, AV24Sistema_Coordenacao2, AV26DynamicFiltersSelector3, AV27Sistema_Sigla3, AV28Sistema_Coordenacao3, AV21DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFSistema_Sigla, AV36TFSistema_Sigla_Sel, AV39TFSistema_Coordenacao, AV40TFSistema_Coordenacao_Sel, AV7AmbienteTecnologico_Codigo, AV37ddo_Sistema_SiglaTitleControlIdToReplace, AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV48Pgmname, AV16Sistema_Ativo, AV17Sistema_Tipo, AV11GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23GN2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18GN2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavSistema_tipo.CurrentValue = StringUtil.RTrim( AV17Sistema_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavSistema_tipo_Internalname, "Values", cmbavSistema_tipo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_sistema_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         Ddo_sistema_coordenacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_sistema_sigla_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_sistema_coordenacao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSistema_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla1_Visible), 5, 0)));
         edtavSistema_coordenacao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
         {
            edtavSistema_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
         {
            edtavSistema_coordenacao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavSistema_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla2_Visible), 5, 0)));
         edtavSistema_coordenacao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
         {
            edtavSistema_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
         {
            edtavSistema_coordenacao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavSistema_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla3_Visible), 5, 0)));
         edtavSistema_coordenacao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 )
         {
            edtavSistema_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 )
         {
            edtavSistema_coordenacao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_coordenacao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         AV22DynamicFiltersSelector2 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         AV23Sistema_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27Sistema_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16Sistema_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Sistema_Ativo", AV16Sistema_Ativo);
         AV17Sistema_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
         AV35TFSistema_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
         Ddo_sistema_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "FilteredText_set", Ddo_sistema_sigla_Filteredtext_set);
         AV36TFSistema_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSistema_Sigla_Sel", AV36TFSistema_Sigla_Sel);
         Ddo_sistema_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SelectedValue_set", Ddo_sistema_sigla_Selectedvalue_set);
         AV39TFSistema_Coordenacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
         Ddo_sistema_coordenacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "FilteredText_set", Ddo_sistema_coordenacao_Filteredtext_set);
         AV40TFSistema_Coordenacao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSistema_Coordenacao_Sel", AV40TFSistema_Coordenacao_Sel);
         Ddo_sistema_coordenacao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SelectedValue_set", Ddo_sistema_coordenacao_Selectedvalue_set);
         AV18DynamicFiltersSelector1 = "SISTEMA_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         AV19Sistema_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV48Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV48Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV48Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "SISTEMA_ATIVO") == 0 )
            {
               AV16Sistema_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Sistema_Ativo", AV16Sistema_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "SISTEMA_TIPO") == 0 )
            {
               AV17Sistema_Tipo = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA") == 0 )
            {
               AV35TFSistema_Sigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFSistema_Sigla", AV35TFSistema_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFSistema_Sigla)) )
               {
                  Ddo_sistema_sigla_Filteredtext_set = AV35TFSistema_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "FilteredText_set", Ddo_sistema_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA_SEL") == 0 )
            {
               AV36TFSistema_Sigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFSistema_Sigla_Sel", AV36TFSistema_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) )
               {
                  Ddo_sistema_sigla_Selectedvalue_set = AV36TFSistema_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_sigla_Internalname, "SelectedValue_set", Ddo_sistema_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO") == 0 )
            {
               AV39TFSistema_Coordenacao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFSistema_Coordenacao", AV39TFSistema_Coordenacao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSistema_Coordenacao)) )
               {
                  Ddo_sistema_coordenacao_Filteredtext_set = AV39TFSistema_Coordenacao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "FilteredText_set", Ddo_sistema_coordenacao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO_SEL") == 0 )
            {
               AV40TFSistema_Coordenacao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFSistema_Coordenacao_Sel", AV40TFSistema_Coordenacao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) )
               {
                  Ddo_sistema_coordenacao_Selectedvalue_set = AV40TFSistema_Coordenacao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistema_coordenacao_Internalname, "SelectedValue_set", Ddo_sistema_coordenacao_Selectedvalue_set);
               }
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV18DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
            {
               AV19Sistema_Sigla1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Sistema_Sigla1", AV19Sistema_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
            {
               AV20Sistema_Coordenacao1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Sistema_Coordenacao1", AV20Sistema_Coordenacao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV21DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV22DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
               {
                  AV23Sistema_Sigla2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Sistema_Sigla2", AV23Sistema_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
               {
                  AV24Sistema_Coordenacao2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Sistema_Coordenacao2", AV24Sistema_Coordenacao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 )
                  {
                     AV27Sistema_Sigla3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Sistema_Sigla3", AV27Sistema_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 )
                  {
                     AV28Sistema_Coordenacao3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Sistema_Coordenacao3", AV28Sistema_Coordenacao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV48Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV16Sistema_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "SISTEMA_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV16Sistema_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Sistema_Tipo)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "SISTEMA_TIPO";
            AV12GridStateFilterValue.gxTpr_Value = AV17Sistema_Tipo;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFSistema_Sigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV35TFSistema_Sigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFSistema_Sigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSistema_Coordenacao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMA_COORDENACAO";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFSistema_Coordenacao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMA_COORDENACAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFSistema_Coordenacao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7AmbienteTecnologico_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&AMBIENTETECNOLOGICO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV48Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Sistema_Sigla1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Sistema_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Sistema_Coordenacao1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV20Sistema_Coordenacao1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Sistema_Sigla2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23Sistema_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Sistema_Coordenacao2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Sistema_Coordenacao2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Sistema_Sigla3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV27Sistema_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Sistema_Coordenacao3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28Sistema_Coordenacao3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV48Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Sistema";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "AmbienteTecnologico_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_5_GN2( true) ;
         }
         else
         {
            wb_table2_5_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_72_GN2( true) ;
         }
         else
         {
            wb_table3_72_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_72_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GN2e( true) ;
         }
         else
         {
            wb_table1_2_GN2e( false) ;
         }
      }

      protected void wb_table3_72_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"75\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Coordenacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Coordenacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Coordenacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A129Sistema_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A513Sistema_Coordenacao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Coordenacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Coordenacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 75 )
         {
            wbEnd = 0;
            nRC_GXsfl_75 = (short)(nGXsfl_75_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_72_GN2e( true) ;
         }
         else
         {
            wb_table3_72_GN2e( false) ;
         }
      }

      protected void wb_table2_5_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_8_GN2( true) ;
         }
         else
         {
            wb_table4_8_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_8_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_AmbienteTecnologicoSistemas.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_17_GN2( true) ;
         }
         else
         {
            wb_table5_17_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_17_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GN2e( true) ;
         }
         else
         {
            wb_table2_5_GN2e( false) ;
         }
      }

      protected void wb_table5_17_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_ativo_Internalname, "Ativo", "", "", lblFiltertextsistema_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSistema_ativo_Internalname, StringUtil.BoolToStr( AV16Sistema_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(24, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_tipo_Internalname, "Tipo", "", "", lblFiltertextsistema_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tipo, cmbavSistema_tipo_Internalname, StringUtil.RTrim( AV17Sistema_Tipo), 1, cmbavSistema_tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_AmbienteTecnologicoSistemas.htm");
            cmbavSistema_tipo.CurrentValue = StringUtil.RTrim( AV17Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavSistema_tipo_Internalname, "Values", (String)(cmbavSistema_tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_30_GN2( true) ;
         }
         else
         {
            wb_table6_30_GN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_30_GN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_GN2e( true) ;
         }
         else
         {
            wb_table5_17_GN2e( false) ;
         }
      }

      protected void wb_table6_30_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_AmbienteTecnologicoSistemas.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla1_Internalname, StringUtil.RTrim( AV19Sistema_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV19Sistema_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_sigla1_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_coordenacao1_Internalname, AV20Sistema_Coordenacao1, StringUtil.RTrim( context.localUtil.Format( AV20Sistema_Coordenacao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_coordenacao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_coordenacao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_AmbienteTecnologicoSistemas.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla2_Internalname, StringUtil.RTrim( AV23Sistema_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV23Sistema_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_sigla2_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_coordenacao2_Internalname, AV24Sistema_Coordenacao2, StringUtil.RTrim( context.localUtil.Format( AV24Sistema_Coordenacao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_coordenacao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_coordenacao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_AmbienteTecnologicoSistemas.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla3_Internalname, StringUtil.RTrim( AV27Sistema_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV27Sistema_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_sigla3_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_coordenacao3_Internalname, AV28Sistema_Coordenacao3, StringUtil.RTrim( context.localUtil.Format( AV28Sistema_Coordenacao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_coordenacao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_coordenacao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AmbienteTecnologicoSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_GN2e( true) ;
         }
         else
         {
            wb_table6_30_GN2e( false) ;
         }
      }

      protected void wb_table4_8_GN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_8_GN2e( true) ;
         }
         else
         {
            wb_table4_8_GN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7AmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGN2( ) ;
         WSGN2( ) ;
         WEGN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7AmbienteTecnologico_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGN2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "ambientetecnologicosistemas");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGN2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7AmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0)));
         }
         wcpOAV7AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7AmbienteTecnologico_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7AmbienteTecnologico_Codigo != wcpOAV7AmbienteTecnologico_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7AmbienteTecnologico_Codigo = AV7AmbienteTecnologico_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7AmbienteTecnologico_Codigo = cgiGet( sPrefix+"AV7AmbienteTecnologico_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7AmbienteTecnologico_Codigo) > 0 )
         {
            AV7AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7AmbienteTecnologico_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0)));
         }
         else
         {
            AV7AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7AmbienteTecnologico_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGN2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGN2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7AmbienteTecnologico_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7AmbienteTecnologico_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7AmbienteTecnologico_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7AmbienteTecnologico_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGN2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031176495");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("ambientetecnologicosistemas.js", "?202031176495");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_752( )
      {
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO_"+sGXsfl_75_idx;
         edtSistema_Sigla_Internalname = sPrefix+"SISTEMA_SIGLA_"+sGXsfl_75_idx;
         edtSistema_Coordenacao_Internalname = sPrefix+"SISTEMA_COORDENACAO_"+sGXsfl_75_idx;
      }

      protected void SubsflControlProps_fel_752( )
      {
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO_"+sGXsfl_75_fel_idx;
         edtSistema_Sigla_Internalname = sPrefix+"SISTEMA_SIGLA_"+sGXsfl_75_fel_idx;
         edtSistema_Coordenacao_Internalname = sPrefix+"SISTEMA_COORDENACAO_"+sGXsfl_75_fel_idx;
      }

      protected void sendrow_752( )
      {
         SubsflControlProps_752( ) ;
         WBGN0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_75_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_75_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_75_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Sigla_Internalname,StringUtil.RTrim( A129Sistema_Sigla),StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Coordenacao_Internalname,(String)A513Sistema_Coordenacao,StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Coordenacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sPrefix+sGXsfl_75_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_SIGLA"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sPrefix+sGXsfl_75_idx, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_COORDENACAO"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sPrefix+sGXsfl_75_idx, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         /* End function sendrow_752 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextsistema_ativo_Internalname = sPrefix+"FILTERTEXTSISTEMA_ATIVO";
         chkavSistema_ativo_Internalname = sPrefix+"vSISTEMA_ATIVO";
         lblFiltertextsistema_tipo_Internalname = sPrefix+"FILTERTEXTSISTEMA_TIPO";
         cmbavSistema_tipo_Internalname = sPrefix+"vSISTEMA_TIPO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavSistema_sigla1_Internalname = sPrefix+"vSISTEMA_SIGLA1";
         edtavSistema_coordenacao1_Internalname = sPrefix+"vSISTEMA_COORDENACAO1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavSistema_sigla2_Internalname = sPrefix+"vSISTEMA_SIGLA2";
         edtavSistema_coordenacao2_Internalname = sPrefix+"vSISTEMA_COORDENACAO2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavSistema_sigla3_Internalname = sPrefix+"vSISTEMA_SIGLA3";
         edtavSistema_coordenacao3_Internalname = sPrefix+"vSISTEMA_COORDENACAO3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO";
         edtSistema_Sigla_Internalname = sPrefix+"SISTEMA_SIGLA";
         edtSistema_Coordenacao_Internalname = sPrefix+"SISTEMA_COORDENACAO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtAmbienteTecnologico_Codigo_Internalname = sPrefix+"AMBIENTETECNOLOGICO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavTfsistema_sigla_Internalname = sPrefix+"vTFSISTEMA_SIGLA";
         edtavTfsistema_sigla_sel_Internalname = sPrefix+"vTFSISTEMA_SIGLA_SEL";
         edtavTfsistema_coordenacao_Internalname = sPrefix+"vTFSISTEMA_COORDENACAO";
         edtavTfsistema_coordenacao_sel_Internalname = sPrefix+"vTFSISTEMA_COORDENACAO_SEL";
         Ddo_sistema_sigla_Internalname = sPrefix+"DDO_SISTEMA_SIGLA";
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_sistema_coordenacao_Internalname = sPrefix+"DDO_SISTEMA_COORDENACAO";
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtSistema_Coordenacao_Jsonclick = "";
         edtSistema_Sigla_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtavSistema_coordenacao3_Jsonclick = "";
         edtavSistema_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavSistema_coordenacao2_Jsonclick = "";
         edtavSistema_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavSistema_coordenacao1_Jsonclick = "";
         edtavSistema_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         cmbavSistema_tipo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtSistema_Coordenacao_Titleformat = 0;
         edtSistema_Sigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavSistema_coordenacao3_Visible = 1;
         edtavSistema_sigla3_Visible = 1;
         edtavSistema_coordenacao2_Visible = 1;
         edtavSistema_sigla2_Visible = 1;
         edtavSistema_coordenacao1_Visible = 1;
         edtavSistema_sigla1_Visible = 1;
         edtSistema_Coordenacao_Title = "�rea gestora";
         edtSistema_Sigla_Title = "Sigla";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavSistema_ativo.Caption = "";
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 1;
         edtavTfsistema_coordenacao_sel_Jsonclick = "";
         edtavTfsistema_coordenacao_sel_Visible = 1;
         edtavTfsistema_coordenacao_Jsonclick = "";
         edtavTfsistema_coordenacao_Visible = 1;
         edtavTfsistema_sigla_sel_Jsonclick = "";
         edtavTfsistema_sigla_sel_Visible = 1;
         edtavTfsistema_sigla_Jsonclick = "";
         edtavTfsistema_sigla_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Visible = 1;
         Ddo_sistema_coordenacao_Searchbuttontext = "Pesquisar";
         Ddo_sistema_coordenacao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_coordenacao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_coordenacao_Loadingdata = "Carregando dados...";
         Ddo_sistema_coordenacao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_coordenacao_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_coordenacao_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_coordenacao_Datalistproc = "GetAmbienteTecnologicoSistemasFilterData";
         Ddo_sistema_coordenacao_Datalisttype = "Dynamic";
         Ddo_sistema_coordenacao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_coordenacao_Filtertype = "Character";
         Ddo_sistema_coordenacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Titlecontrolidtoreplace = "";
         Ddo_sistema_coordenacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_coordenacao_Cls = "ColumnSettings";
         Ddo_sistema_coordenacao_Tooltip = "Op��es";
         Ddo_sistema_coordenacao_Caption = "";
         Ddo_sistema_sigla_Searchbuttontext = "Pesquisar";
         Ddo_sistema_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_sigla_Loadingdata = "Carregando dados...";
         Ddo_sistema_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_sigla_Datalistproc = "GetAmbienteTecnologicoSistemasFilterData";
         Ddo_sistema_sigla_Datalisttype = "Dynamic";
         Ddo_sistema_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_sigla_Filtertype = "Character";
         Ddo_sistema_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Titlecontrolidtoreplace = "";
         Ddo_sistema_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_sigla_Cls = "ColumnSettings";
         Ddo_sistema_sigla_Tooltip = "Op��es";
         Ddo_sistema_sigla_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''}],oparms:[{av:'AV34Sistema_SiglaTitleFilterData',fld:'vSISTEMA_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV38Sistema_CoordenacaoTitleFilterData',fld:'vSISTEMA_COORDENACAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtSistema_Sigla_Titleformat',ctrl:'SISTEMA_SIGLA',prop:'Titleformat'},{av:'edtSistema_Sigla_Title',ctrl:'SISTEMA_SIGLA',prop:'Title'},{av:'edtSistema_Coordenacao_Titleformat',ctrl:'SISTEMA_COORDENACAO',prop:'Titleformat'},{av:'edtSistema_Coordenacao_Title',ctrl:'SISTEMA_COORDENACAO',prop:'Title'},{av:'AV44GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SISTEMA_SIGLA.ONOPTIONCLICKED","{handler:'E12GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_sistema_sigla_Activeeventkey',ctrl:'DDO_SISTEMA_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_sistema_sigla_Filteredtext_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_get'},{av:'Ddo_sistema_sigla_Selectedvalue_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_COORDENACAO.ONOPTIONCLICKED","{handler:'E13GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''},{av:'Ddo_sistema_coordenacao_Activeeventkey',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'ActiveEventKey'},{av:'Ddo_sistema_coordenacao_Filteredtext_get',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'FilteredText_get'},{av:'Ddo_sistema_coordenacao_Selectedvalue_get',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26GN2',iparms:[],oparms:[]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19GN2',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_sigla3_Visible',ctrl:'vSISTEMA_SIGLA3',prop:'Visible'},{av:'edtavSistema_coordenacao3_Visible',ctrl:'vSISTEMA_COORDENACAO3',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20GN2',iparms:[{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E21GN2',iparms:[],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_sigla3_Visible',ctrl:'vSISTEMA_SIGLA3',prop:'Visible'},{av:'edtavSistema_coordenacao3_Visible',ctrl:'vSISTEMA_COORDENACAO3',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22GN2',iparms:[{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_sigla3_Visible',ctrl:'vSISTEMA_SIGLA3',prop:'Visible'},{av:'edtavSistema_coordenacao3_Visible',ctrl:'vSISTEMA_COORDENACAO3',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23GN2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavSistema_sigla3_Visible',ctrl:'vSISTEMA_SIGLA3',prop:'Visible'},{av:'edtavSistema_coordenacao3_Visible',ctrl:'vSISTEMA_COORDENACAO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18GN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV7AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV16Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV35TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Filteredtext_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_set'},{av:'AV36TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Selectedvalue_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_set'},{av:'AV39TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'Ddo_sistema_coordenacao_Filteredtext_set',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'FilteredText_set'},{av:'AV40TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_coordenacao_Selectedvalue_set',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SelectedValue_set'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27Sistema_Sigla3',fld:'vSISTEMA_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV20Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV28Sistema_Coordenacao3',fld:'vSISTEMA_COORDENACAO3',pic:'@!',nv:''},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_sigla3_Visible',ctrl:'vSISTEMA_SIGLA3',prop:'Visible'},{av:'edtavSistema_coordenacao3_Visible',ctrl:'vSISTEMA_COORDENACAO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_sistema_sigla_Activeeventkey = "";
         Ddo_sistema_sigla_Filteredtext_get = "";
         Ddo_sistema_sigla_Selectedvalue_get = "";
         Ddo_sistema_coordenacao_Activeeventkey = "";
         Ddo_sistema_coordenacao_Filteredtext_get = "";
         Ddo_sistema_coordenacao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV18DynamicFiltersSelector1 = "";
         AV19Sistema_Sigla1 = "";
         AV20Sistema_Coordenacao1 = "";
         AV22DynamicFiltersSelector2 = "";
         AV23Sistema_Sigla2 = "";
         AV24Sistema_Coordenacao2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV27Sistema_Sigla3 = "";
         AV28Sistema_Coordenacao3 = "";
         AV35TFSistema_Sigla = "";
         AV36TFSistema_Sigla_Sel = "";
         AV39TFSistema_Coordenacao = "";
         AV40TFSistema_Coordenacao_Sel = "";
         AV37ddo_Sistema_SiglaTitleControlIdToReplace = "";
         AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace = "";
         AV48Pgmname = "";
         AV16Sistema_Ativo = true;
         AV17Sistema_Tipo = "A";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV42DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Sistema_CoordenacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_sistema_sigla_Filteredtext_set = "";
         Ddo_sistema_sigla_Selectedvalue_set = "";
         Ddo_sistema_sigla_Sortedstatus = "";
         Ddo_sistema_coordenacao_Filteredtext_set = "";
         Ddo_sistema_coordenacao_Selectedvalue_set = "";
         Ddo_sistema_coordenacao_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A129Sistema_Sigla = "";
         A513Sistema_Coordenacao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19Sistema_Sigla1 = "";
         lV20Sistema_Coordenacao1 = "";
         lV23Sistema_Sigla2 = "";
         lV24Sistema_Coordenacao2 = "";
         lV27Sistema_Sigla3 = "";
         lV28Sistema_Coordenacao3 = "";
         lV35TFSistema_Sigla = "";
         lV39TFSistema_Coordenacao = "";
         A699Sistema_Tipo = "";
         H00GN2_A130Sistema_Ativo = new bool[] {false} ;
         H00GN2_A699Sistema_Tipo = new String[] {""} ;
         H00GN2_n699Sistema_Tipo = new bool[] {false} ;
         H00GN2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H00GN2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H00GN2_A513Sistema_Coordenacao = new String[] {""} ;
         H00GN2_n513Sistema_Coordenacao = new bool[] {false} ;
         H00GN2_A129Sistema_Sigla = new String[] {""} ;
         H00GN2_A127Sistema_Codigo = new int[1] ;
         H00GN3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextsistema_ativo_Jsonclick = "";
         lblFiltertextsistema_tipo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7AmbienteTecnologico_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ambientetecnologicosistemas__default(),
            new Object[][] {
                new Object[] {
               H00GN2_A130Sistema_Ativo, H00GN2_A699Sistema_Tipo, H00GN2_n699Sistema_Tipo, H00GN2_A351AmbienteTecnologico_Codigo, H00GN2_n351AmbienteTecnologico_Codigo, H00GN2_A513Sistema_Coordenacao, H00GN2_n513Sistema_Coordenacao, H00GN2_A129Sistema_Sigla, H00GN2_A127Sistema_Codigo
               }
               , new Object[] {
               H00GN3_AGRID_nRecordCount
               }
            }
         );
         AV48Pgmname = "AmbienteTecnologicoSistemas";
         /* GeneXus formulas. */
         AV48Pgmname = "AmbienteTecnologicoSistemas";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_75 ;
      private short nGXsfl_75_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_75_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSistema_Sigla_Titleformat ;
      private short edtSistema_Coordenacao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7AmbienteTecnologico_Codigo ;
      private int wcpOAV7AmbienteTecnologico_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_sistema_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_coordenacao_Datalistupdateminimumcharacters ;
      private int A351AmbienteTecnologico_Codigo ;
      private int edtAmbienteTecnologico_Codigo_Visible ;
      private int edtavTfsistema_sigla_Visible ;
      private int edtavTfsistema_sigla_sel_Visible ;
      private int edtavTfsistema_coordenacao_Visible ;
      private int edtavTfsistema_coordenacao_sel_Visible ;
      private int edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible ;
      private int A127Sistema_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV43PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavSistema_sigla1_Visible ;
      private int edtavSistema_coordenacao1_Visible ;
      private int edtavSistema_sigla2_Visible ;
      private int edtavSistema_coordenacao2_Visible ;
      private int edtavSistema_sigla3_Visible ;
      private int edtavSistema_coordenacao3_Visible ;
      private int AV49GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV44GridCurrentPage ;
      private long AV45GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_sistema_sigla_Activeeventkey ;
      private String Ddo_sistema_sigla_Filteredtext_get ;
      private String Ddo_sistema_sigla_Selectedvalue_get ;
      private String Ddo_sistema_coordenacao_Activeeventkey ;
      private String Ddo_sistema_coordenacao_Filteredtext_get ;
      private String Ddo_sistema_coordenacao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_75_idx="0001" ;
      private String AV19Sistema_Sigla1 ;
      private String AV23Sistema_Sigla2 ;
      private String AV27Sistema_Sigla3 ;
      private String AV35TFSistema_Sigla ;
      private String AV36TFSistema_Sigla_Sel ;
      private String AV48Pgmname ;
      private String AV17Sistema_Tipo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_sistema_sigla_Caption ;
      private String Ddo_sistema_sigla_Tooltip ;
      private String Ddo_sistema_sigla_Cls ;
      private String Ddo_sistema_sigla_Filteredtext_set ;
      private String Ddo_sistema_sigla_Selectedvalue_set ;
      private String Ddo_sistema_sigla_Dropdownoptionstype ;
      private String Ddo_sistema_sigla_Titlecontrolidtoreplace ;
      private String Ddo_sistema_sigla_Sortedstatus ;
      private String Ddo_sistema_sigla_Filtertype ;
      private String Ddo_sistema_sigla_Datalisttype ;
      private String Ddo_sistema_sigla_Datalistproc ;
      private String Ddo_sistema_sigla_Sortasc ;
      private String Ddo_sistema_sigla_Sortdsc ;
      private String Ddo_sistema_sigla_Loadingdata ;
      private String Ddo_sistema_sigla_Cleanfilter ;
      private String Ddo_sistema_sigla_Noresultsfound ;
      private String Ddo_sistema_sigla_Searchbuttontext ;
      private String Ddo_sistema_coordenacao_Caption ;
      private String Ddo_sistema_coordenacao_Tooltip ;
      private String Ddo_sistema_coordenacao_Cls ;
      private String Ddo_sistema_coordenacao_Filteredtext_set ;
      private String Ddo_sistema_coordenacao_Selectedvalue_set ;
      private String Ddo_sistema_coordenacao_Dropdownoptionstype ;
      private String Ddo_sistema_coordenacao_Titlecontrolidtoreplace ;
      private String Ddo_sistema_coordenacao_Sortedstatus ;
      private String Ddo_sistema_coordenacao_Filtertype ;
      private String Ddo_sistema_coordenacao_Datalisttype ;
      private String Ddo_sistema_coordenacao_Datalistproc ;
      private String Ddo_sistema_coordenacao_Sortasc ;
      private String Ddo_sistema_coordenacao_Sortdsc ;
      private String Ddo_sistema_coordenacao_Loadingdata ;
      private String Ddo_sistema_coordenacao_Cleanfilter ;
      private String Ddo_sistema_coordenacao_Noresultsfound ;
      private String Ddo_sistema_coordenacao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfsistema_sigla_Internalname ;
      private String edtavTfsistema_sigla_Jsonclick ;
      private String edtavTfsistema_sigla_sel_Internalname ;
      private String edtavTfsistema_sigla_sel_Jsonclick ;
      private String edtavTfsistema_coordenacao_Internalname ;
      private String edtavTfsistema_coordenacao_Jsonclick ;
      private String edtavTfsistema_coordenacao_sel_Internalname ;
      private String edtavTfsistema_coordenacao_sel_Jsonclick ;
      private String edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String A129Sistema_Sigla ;
      private String edtSistema_Sigla_Internalname ;
      private String edtSistema_Coordenacao_Internalname ;
      private String chkavSistema_ativo_Internalname ;
      private String scmdbuf ;
      private String lV19Sistema_Sigla1 ;
      private String lV23Sistema_Sigla2 ;
      private String lV27Sistema_Sigla3 ;
      private String lV35TFSistema_Sigla ;
      private String A699Sistema_Tipo ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavSistema_tipo_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavSistema_sigla1_Internalname ;
      private String edtavSistema_coordenacao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavSistema_sigla2_Internalname ;
      private String edtavSistema_coordenacao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavSistema_sigla3_Internalname ;
      private String edtavSistema_coordenacao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_sistema_sigla_Internalname ;
      private String Ddo_sistema_coordenacao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSistema_Sigla_Title ;
      private String edtSistema_Coordenacao_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextsistema_ativo_Internalname ;
      private String lblFiltertextsistema_ativo_Jsonclick ;
      private String lblFiltertextsistema_tipo_Internalname ;
      private String lblFiltertextsistema_tipo_Jsonclick ;
      private String cmbavSistema_tipo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavSistema_sigla1_Jsonclick ;
      private String edtavSistema_coordenacao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavSistema_sigla2_Jsonclick ;
      private String edtavSistema_coordenacao2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavSistema_sigla3_Jsonclick ;
      private String edtavSistema_coordenacao3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7AmbienteTecnologico_Codigo ;
      private String sGXsfl_75_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtSistema_Sigla_Jsonclick ;
      private String edtSistema_Coordenacao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV21DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV16Sistema_Ativo ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_sistema_sigla_Includesortasc ;
      private bool Ddo_sistema_sigla_Includesortdsc ;
      private bool Ddo_sistema_sigla_Includefilter ;
      private bool Ddo_sistema_sigla_Filterisrange ;
      private bool Ddo_sistema_sigla_Includedatalist ;
      private bool Ddo_sistema_coordenacao_Includesortasc ;
      private bool Ddo_sistema_coordenacao_Includesortdsc ;
      private bool Ddo_sistema_coordenacao_Includefilter ;
      private bool Ddo_sistema_coordenacao_Filterisrange ;
      private bool Ddo_sistema_coordenacao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n513Sistema_Coordenacao ;
      private bool A130Sistema_Ativo ;
      private bool n699Sistema_Tipo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV18DynamicFiltersSelector1 ;
      private String AV20Sistema_Coordenacao1 ;
      private String AV22DynamicFiltersSelector2 ;
      private String AV24Sistema_Coordenacao2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV28Sistema_Coordenacao3 ;
      private String AV39TFSistema_Coordenacao ;
      private String AV40TFSistema_Coordenacao_Sel ;
      private String AV37ddo_Sistema_SiglaTitleControlIdToReplace ;
      private String AV41ddo_Sistema_CoordenacaoTitleControlIdToReplace ;
      private String A513Sistema_Coordenacao ;
      private String lV20Sistema_Coordenacao1 ;
      private String lV24Sistema_Coordenacao2 ;
      private String lV28Sistema_Coordenacao3 ;
      private String lV39TFSistema_Coordenacao ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavSistema_ativo ;
      private GXCombobox cmbavSistema_tipo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00GN2_A130Sistema_Ativo ;
      private String[] H00GN2_A699Sistema_Tipo ;
      private bool[] H00GN2_n699Sistema_Tipo ;
      private int[] H00GN2_A351AmbienteTecnologico_Codigo ;
      private bool[] H00GN2_n351AmbienteTecnologico_Codigo ;
      private String[] H00GN2_A513Sistema_Coordenacao ;
      private bool[] H00GN2_n513Sistema_Coordenacao ;
      private String[] H00GN2_A129Sistema_Sigla ;
      private int[] H00GN2_A127Sistema_Codigo ;
      private long[] H00GN3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Sistema_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Sistema_CoordenacaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV42DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class ambientetecnologicosistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GN2( IGxContext context ,
                                             String AV18DynamicFiltersSelector1 ,
                                             String AV19Sistema_Sigla1 ,
                                             String AV20Sistema_Coordenacao1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             String AV23Sistema_Sigla2 ,
                                             String AV24Sistema_Coordenacao2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV27Sistema_Sigla3 ,
                                             String AV28Sistema_Coordenacao3 ,
                                             String AV36TFSistema_Sigla_Sel ,
                                             String AV35TFSistema_Sigla ,
                                             String AV40TFSistema_Coordenacao_Sel ,
                                             String AV39TFSistema_Coordenacao ,
                                             String A129Sistema_Sigla ,
                                             String A513Sistema_Coordenacao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A351AmbienteTecnologico_Codigo ,
                                             int AV7AmbienteTecnologico_Codigo ,
                                             bool A130Sistema_Ativo ,
                                             String A699Sistema_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Sistema_Ativo], [Sistema_Tipo], [AmbienteTecnologico_Codigo], [Sistema_Coordenacao], [Sistema_Sigla], [Sistema_Codigo]";
         sFromString = " FROM [Sistema] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([AmbienteTecnologico_Codigo] = @AV7AmbienteTecnologico_Codigo)";
         sWhereString = sWhereString + " and ([Sistema_Ativo] = 1)";
         sWhereString = sWhereString + " and ([Sistema_Tipo] = 'A')";
         if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Sistema_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV19Sistema_Sigla1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Sistema_Coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV20Sistema_Coordenacao1 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Sistema_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV23Sistema_Sigla2 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Sistema_Coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV24Sistema_Coordenacao2 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Sistema_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV27Sistema_Sigla3 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Sistema_Coordenacao3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV28Sistema_Coordenacao3 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV35TFSistema_Sigla)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] = @AV36TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSistema_Coordenacao)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV39TFSistema_Coordenacao)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] = @AV40TFSistema_Coordenacao_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Codigo], [Sistema_Sigla]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Codigo] DESC, [Sistema_Sigla] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Codigo], [Sistema_Coordenacao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Codigo] DESC, [Sistema_Coordenacao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GN3( IGxContext context ,
                                             String AV18DynamicFiltersSelector1 ,
                                             String AV19Sistema_Sigla1 ,
                                             String AV20Sistema_Coordenacao1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             String AV23Sistema_Sigla2 ,
                                             String AV24Sistema_Coordenacao2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV27Sistema_Sigla3 ,
                                             String AV28Sistema_Coordenacao3 ,
                                             String AV36TFSistema_Sigla_Sel ,
                                             String AV35TFSistema_Sigla ,
                                             String AV40TFSistema_Coordenacao_Sel ,
                                             String AV39TFSistema_Coordenacao ,
                                             String A129Sistema_Sigla ,
                                             String A513Sistema_Coordenacao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A351AmbienteTecnologico_Codigo ,
                                             int AV7AmbienteTecnologico_Codigo ,
                                             bool A130Sistema_Ativo ,
                                             String A699Sistema_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Sistema] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AmbienteTecnologico_Codigo] = @AV7AmbienteTecnologico_Codigo)";
         scmdbuf = scmdbuf + " and ([Sistema_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ([Sistema_Tipo] = 'A')";
         if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Sistema_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV19Sistema_Sigla1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Sistema_Coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV20Sistema_Coordenacao1 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Sistema_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV23Sistema_Sigla2 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Sistema_Coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV24Sistema_Coordenacao2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Sistema_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV27Sistema_Sigla3 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Sistema_Coordenacao3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV28Sistema_Coordenacao3 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV35TFSistema_Sigla)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] = @AV36TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFSistema_Coordenacao)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV39TFSistema_Coordenacao)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFSistema_Coordenacao_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] = @AV40TFSistema_Coordenacao_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GN2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] );
               case 1 :
                     return conditional_H00GN3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GN2 ;
          prmH00GN2 = new Object[] {
          new Object[] {"@AV7AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19Sistema_Sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@lV20Sistema_Coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23Sistema_Sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@lV24Sistema_Coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV27Sistema_Sigla3",SqlDbType.Char,25,0} ,
          new Object[] {"@lV28Sistema_Coordenacao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV36TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV39TFSistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFSistema_Coordenacao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GN3 ;
          prmH00GN3 = new Object[] {
          new Object[] {"@AV7AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19Sistema_Sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@lV20Sistema_Coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23Sistema_Sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@lV24Sistema_Coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV27Sistema_Sigla3",SqlDbType.Char,25,0} ,
          new Object[] {"@lV28Sistema_Coordenacao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV36TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV39TFSistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFSistema_Coordenacao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GN2,11,0,true,false )
             ,new CursorDef("H00GN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GN3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 25) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
