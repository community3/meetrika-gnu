/*
               File: SistemaVersaoGeneral
        Description: Sistema Versao General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:5.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemaversaogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemaversaogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemaversaogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SistemaVersao_Codigo )
      {
         this.A1859SistemaVersao_Codigo = aP0_SistemaVersao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1859SistemaVersao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAOG2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "SistemaVersaoGeneral";
               context.Gx_err = 0;
               WSOG2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Versao General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311730555");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemaversaogeneral.aspx") + "?" + UrlEncode("" +A1859SistemaVersao_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1859SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_ID", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DESCRICAO", GetSecureSignedToken( sPrefix, A1861SistemaVersao_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_EVOLUCAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_MELHORIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_CORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormOG2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemaversaogeneral.js", "?2020311730557");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaVersaoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Versao General" ;
      }

      protected void WBOG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemaversaogeneral.aspx");
            }
            wb_table1_2_OG2( true) ;
         }
         else
         {
            wb_table1_2_OG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OG2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTOG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Versao General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPOG0( ) ;
            }
         }
      }

      protected void WSOG2( )
      {
         STARTOG2( ) ;
         EVTOG2( ) ;
      }

      protected void EVTOG2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11OG2 */
                                    E11OG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12OG2 */
                                    E12OG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13OG2 */
                                    E13OG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14OG2 */
                                    E14OG2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOG0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormOG2( ) ;
            }
         }
      }

      protected void PAOG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "SistemaVersaoGeneral";
         context.Gx_err = 0;
      }

      protected void RFOG2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00OG2 */
            pr_default.execute(0, new Object[] {A1859SistemaVersao_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1865SistemaVersao_Data = H00OG2_A1865SistemaVersao_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
               A1864SistemaVersao_Correcao = H00OG2_A1864SistemaVersao_Correcao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")));
               n1864SistemaVersao_Correcao = H00OG2_n1864SistemaVersao_Correcao[0];
               A1863SistemaVersao_Melhoria = H00OG2_A1863SistemaVersao_Melhoria[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_MELHORIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")));
               n1863SistemaVersao_Melhoria = H00OG2_n1863SistemaVersao_Melhoria[0];
               A1862SistemaVersao_Evolucao = H00OG2_A1862SistemaVersao_Evolucao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_EVOLUCAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")));
               n1862SistemaVersao_Evolucao = H00OG2_n1862SistemaVersao_Evolucao[0];
               A1861SistemaVersao_Descricao = H00OG2_A1861SistemaVersao_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_DESCRICAO", GetSecureSignedToken( sPrefix, A1861SistemaVersao_Descricao));
               A1860SistemaVersao_Id = H00OG2_A1860SistemaVersao_Id[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_ID", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
               /* Execute user event: E12OG2 */
               E12OG2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBOG0( ) ;
         }
      }

      protected void STRUPOG0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "SistemaVersaoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OG2 */
         E11OG2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1860SistemaVersao_Id = cgiGet( edtSistemaVersao_Id_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_ID", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
            A1861SistemaVersao_Descricao = cgiGet( edtSistemaVersao_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_DESCRICAO", GetSecureSignedToken( sPrefix, A1861SistemaVersao_Descricao));
            A1862SistemaVersao_Evolucao = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Evolucao_Internalname), ",", "."));
            n1862SistemaVersao_Evolucao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_EVOLUCAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")));
            A1863SistemaVersao_Melhoria = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Melhoria_Internalname), ",", "."));
            n1863SistemaVersao_Melhoria = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_MELHORIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")));
            A1864SistemaVersao_Correcao = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Correcao_Internalname), ",", "."));
            n1864SistemaVersao_Correcao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CORRECAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")));
            A1865SistemaVersao_Data = context.localUtil.CToT( cgiGet( edtSistemaVersao_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
            /* Read saved values. */
            wcpOA1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1859SistemaVersao_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OG2 */
         E11OG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11OG2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12OG2( )
      {
         /* Load Routine */
      }

      protected void E13OG2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode("" +AV12SistemaVersao_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14OG2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode("" +AV12SistemaVersao_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "SistemaVersao";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "SistemaVersao_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7SistemaVersao_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_OG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_OG2( true) ;
         }
         else
         {
            wb_table2_8_OG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_OG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_OG2( true) ;
         }
         else
         {
            wb_table3_41_OG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_OG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OG2e( true) ;
         }
         else
         {
            wb_table1_2_OG2e( false) ;
         }
      }

      protected void wb_table3_41_OG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_OG2e( true) ;
         }
         else
         {
            wb_table3_41_OG2e( false) ;
         }
      }

      protected void wb_table2_8_OG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_id_Internalname, "Vers�o", "", "", lblTextblocksistemaversao_id_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Id_Internalname, StringUtil.RTrim( A1860SistemaVersao_Id), StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Id_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_descricao_Internalname, "Descri��o", "", "", lblTextblocksistemaversao_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistemaVersao_Descricao_Internalname, A1861SistemaVersao_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_evolucao_Internalname, "Evolu��o", "", "", lblTextblocksistemaversao_evolucao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Evolucao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1862SistemaVersao_Evolucao), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Evolucao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_melhoria_Internalname, "Melhoria", "", "", lblTextblocksistemaversao_melhoria_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Melhoria_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1863SistemaVersao_Melhoria), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Melhoria_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_correcao_Internalname, "Corre��o", "", "", lblTextblocksistemaversao_correcao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Correcao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1864SistemaVersao_Correcao), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Correcao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_data_Internalname, "Em", "", "", lblTextblocksistemaversao_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSistemaVersao_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Data_Internalname, context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSistemaVersao_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_OG2e( true) ;
         }
         else
         {
            wb_table2_8_OG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1859SistemaVersao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOG2( ) ;
         WSOG2( ) ;
         WEOG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1859SistemaVersao_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAOG2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemaversaogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAOG2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1859SistemaVersao_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         }
         wcpOA1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1859SistemaVersao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1859SistemaVersao_Codigo != wcpOA1859SistemaVersao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1859SistemaVersao_Codigo = cgiGet( sPrefix+"A1859SistemaVersao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1859SistemaVersao_Codigo) > 0 )
         {
            A1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1859SistemaVersao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         }
         else
         {
            A1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1859SistemaVersao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAOG2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSOG2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSOG2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1859SistemaVersao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1859SistemaVersao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1859SistemaVersao_Codigo_CTRL", StringUtil.RTrim( sCtrlA1859SistemaVersao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEOG2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311730582");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemaversaogeneral.js", "?2020311730582");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksistemaversao_id_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_ID";
         edtSistemaVersao_Id_Internalname = sPrefix+"SISTEMAVERSAO_ID";
         lblTextblocksistemaversao_descricao_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_DESCRICAO";
         edtSistemaVersao_Descricao_Internalname = sPrefix+"SISTEMAVERSAO_DESCRICAO";
         lblTextblocksistemaversao_evolucao_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_EVOLUCAO";
         edtSistemaVersao_Evolucao_Internalname = sPrefix+"SISTEMAVERSAO_EVOLUCAO";
         lblTextblocksistemaversao_melhoria_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_MELHORIA";
         edtSistemaVersao_Melhoria_Internalname = sPrefix+"SISTEMAVERSAO_MELHORIA";
         lblTextblocksistemaversao_correcao_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_CORRECAO";
         edtSistemaVersao_Correcao_Internalname = sPrefix+"SISTEMAVERSAO_CORRECAO";
         lblTextblocksistemaversao_data_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_DATA";
         edtSistemaVersao_Data_Internalname = sPrefix+"SISTEMAVERSAO_DATA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtSistemaVersao_Data_Jsonclick = "";
         edtSistemaVersao_Correcao_Jsonclick = "";
         edtSistemaVersao_Melhoria_Jsonclick = "";
         edtSistemaVersao_Evolucao_Jsonclick = "";
         edtSistemaVersao_Id_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13OG2',iparms:[{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14OG2',iparms:[{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1860SistemaVersao_Id = "";
         A1861SistemaVersao_Descricao = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00OG2_A1859SistemaVersao_Codigo = new int[1] ;
         H00OG2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         H00OG2_A1864SistemaVersao_Correcao = new int[1] ;
         H00OG2_n1864SistemaVersao_Correcao = new bool[] {false} ;
         H00OG2_A1863SistemaVersao_Melhoria = new short[1] ;
         H00OG2_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         H00OG2_A1862SistemaVersao_Evolucao = new short[1] ;
         H00OG2_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         H00OG2_A1861SistemaVersao_Descricao = new String[] {""} ;
         H00OG2_A1860SistemaVersao_Id = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocksistemaversao_id_Jsonclick = "";
         lblTextblocksistemaversao_descricao_Jsonclick = "";
         lblTextblocksistemaversao_evolucao_Jsonclick = "";
         lblTextblocksistemaversao_melhoria_Jsonclick = "";
         lblTextblocksistemaversao_correcao_Jsonclick = "";
         lblTextblocksistemaversao_data_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1859SistemaVersao_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemaversaogeneral__default(),
            new Object[][] {
                new Object[] {
               H00OG2_A1859SistemaVersao_Codigo, H00OG2_A1865SistemaVersao_Data, H00OG2_A1864SistemaVersao_Correcao, H00OG2_n1864SistemaVersao_Correcao, H00OG2_A1863SistemaVersao_Melhoria, H00OG2_n1863SistemaVersao_Melhoria, H00OG2_A1862SistemaVersao_Evolucao, H00OG2_n1862SistemaVersao_Evolucao, H00OG2_A1861SistemaVersao_Descricao, H00OG2_A1860SistemaVersao_Id
               }
            }
         );
         AV15Pgmname = "SistemaVersaoGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "SistemaVersaoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1862SistemaVersao_Evolucao ;
      private short A1863SistemaVersao_Melhoria ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1859SistemaVersao_Codigo ;
      private int wcpOA1859SistemaVersao_Codigo ;
      private int AV12SistemaVersao_SistemaCod ;
      private int A1864SistemaVersao_Correcao ;
      private int AV7SistemaVersao_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1860SistemaVersao_Id ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtSistemaVersao_Id_Internalname ;
      private String edtSistemaVersao_Descricao_Internalname ;
      private String edtSistemaVersao_Evolucao_Internalname ;
      private String edtSistemaVersao_Melhoria_Internalname ;
      private String edtSistemaVersao_Correcao_Internalname ;
      private String edtSistemaVersao_Data_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksistemaversao_id_Internalname ;
      private String lblTextblocksistemaversao_id_Jsonclick ;
      private String edtSistemaVersao_Id_Jsonclick ;
      private String lblTextblocksistemaversao_descricao_Internalname ;
      private String lblTextblocksistemaversao_descricao_Jsonclick ;
      private String lblTextblocksistemaversao_evolucao_Internalname ;
      private String lblTextblocksistemaversao_evolucao_Jsonclick ;
      private String edtSistemaVersao_Evolucao_Jsonclick ;
      private String lblTextblocksistemaversao_melhoria_Internalname ;
      private String lblTextblocksistemaversao_melhoria_Jsonclick ;
      private String edtSistemaVersao_Melhoria_Jsonclick ;
      private String lblTextblocksistemaversao_correcao_Internalname ;
      private String lblTextblocksistemaversao_correcao_Jsonclick ;
      private String edtSistemaVersao_Correcao_Jsonclick ;
      private String lblTextblocksistemaversao_data_Internalname ;
      private String lblTextblocksistemaversao_data_Jsonclick ;
      private String edtSistemaVersao_Data_Jsonclick ;
      private String sCtrlA1859SistemaVersao_Codigo ;
      private DateTime A1865SistemaVersao_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1864SistemaVersao_Correcao ;
      private bool n1863SistemaVersao_Melhoria ;
      private bool n1862SistemaVersao_Evolucao ;
      private bool returnInSub ;
      private String A1861SistemaVersao_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00OG2_A1859SistemaVersao_Codigo ;
      private DateTime[] H00OG2_A1865SistemaVersao_Data ;
      private int[] H00OG2_A1864SistemaVersao_Correcao ;
      private bool[] H00OG2_n1864SistemaVersao_Correcao ;
      private short[] H00OG2_A1863SistemaVersao_Melhoria ;
      private bool[] H00OG2_n1863SistemaVersao_Melhoria ;
      private short[] H00OG2_A1862SistemaVersao_Evolucao ;
      private bool[] H00OG2_n1862SistemaVersao_Evolucao ;
      private String[] H00OG2_A1861SistemaVersao_Descricao ;
      private String[] H00OG2_A1860SistemaVersao_Id ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class sistemaversaogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OG2 ;
          prmH00OG2 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OG2", "SELECT [SistemaVersao_Codigo], [SistemaVersao_Data], [SistemaVersao_Correcao], [SistemaVersao_Melhoria], [SistemaVersao_Evolucao], [SistemaVersao_Descricao], [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ORDER BY [SistemaVersao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OG2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
