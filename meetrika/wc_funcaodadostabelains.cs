/*
               File: WC_FuncaoDadosTabelaIns
        Description: Funcao Dados Tabela Insert
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:9.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_funcaodadostabelains : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_funcaodadostabelains( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_funcaodadostabelains( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           int aP1_Sistema_Codigo )
      {
         this.AV7FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9Sistema_Codigo = aP1_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavFuncaodados_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
                  AV9Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7FuncaoDados_Codigo,(int)AV9Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAODADOS_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvFUNCAODADOS_CODIGOAB2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
               {
                  nRC_GXsfl_14 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_14_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_14_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridtabelas_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
               {
                  A174Tabela_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A174Tabela_Ativo", A174Tabela_Ativo);
                  A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
                  AV9Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
                  A173Tabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A173Tabela_Nome", A173Tabela_Nome);
                  AV5FiltroTabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5FiltroTabela_Nome", AV5FiltroTabela_Nome);
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  AV7FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                  A189Tabela_ModuloDes = GetNextPar( );
                  n189Tabela_ModuloDes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A189Tabela_ModuloDes", A189Tabela_ModuloDes);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridtabelas_refresh( A174Tabela_Ativo, A190Tabela_SistemaCod, AV9Sistema_Codigo, A173Tabela_Nome, AV5FiltroTabela_Nome, A368FuncaoDados_Codigo, AV7FuncaoDados_Codigo, A172Tabela_Codigo, A189Tabela_ModuloDes, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAB2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               dynavFuncaodados_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
               edtavTabela_cod_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_cod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_cod_Enabled), 5, 0)));
               edtavTabela_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome_Enabled), 5, 0)));
               edtavTabela_modulodes_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes_Enabled), 5, 0)));
               GXVvFUNCAODADOS_CODIGO_htmlAB2( ) ;
               WSAB2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao Dados Tabela Insert") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311720100");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_funcaodadostabelains.aspx") + "?" + UrlEncode("" +AV7FuncaoDados_Codigo) + "," + UrlEncode("" +AV9Sistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_14", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_14), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV9Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV9Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"TABELA_ATIVO", A174Tabela_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_MODULODES", StringUtil.RTrim( A189Tabela_ModuloDes));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAB2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_funcaodadostabelains.js", "?2020311720103");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_FuncaoDadosTabelaIns" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao Dados Tabela Insert" ;
      }

      protected void WBAB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_funcaodadostabelains.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_AB2( true) ;
         }
         else
         {
            wb_table1_2_AB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AB2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTAB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao Dados Tabela Insert", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAB0( ) ;
            }
         }
      }

      protected void WSAB2( )
      {
         STARTAB2( ) ;
         EVTAB2( ) ;
      }

      protected void EVTAB2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTabela_cod_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "GRIDTABELAS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAB0( ) ;
                              }
                              nGXsfl_14_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
                              SubsflControlProps_142( ) ;
                              if ( StringUtil.Len( sPrefix) == 0 )
                              {
                                 dynavFuncaodados_codigo.Name = dynavFuncaodados_codigo_Internalname;
                                 dynavFuncaodados_codigo.CurrentValue = cgiGet( dynavFuncaodados_codigo_Internalname);
                                 AV7FuncaoDados_Codigo = (int)(NumberUtil.Val( cgiGet( dynavFuncaodados_codigo_Internalname), "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
                              }
                              AV6Flag = cgiGet( edtavFlag_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6Flag)) ? AV18Flag_GXI : context.convertURL( context.PathToRelativeUrl( AV6Flag))));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavTabela_cod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTabela_cod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTABELA_COD");
                                 GX_FocusControl = edtavTabela_cod_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV10Tabela_Cod = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_cod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Cod), 6, 0)));
                              }
                              else
                              {
                                 AV10Tabela_Cod = (int)(context.localUtil.CToN( cgiGet( edtavTabela_cod_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_cod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Cod), 6, 0)));
                              }
                              AV13Tabela_Nome = StringUtil.Upper( cgiGet( edtavTabela_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_nome_Internalname, AV13Tabela_Nome);
                              AV12Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtavTabela_modulodes_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_modulodes_Internalname, AV12Tabela_ModuloDes);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_cod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11AB2 */
                                          E11AB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_cod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12AB2 */
                                          E12AB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_cod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13AB2 */
                                          E13AB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                                /* Execute user event: E14AB2 */
                                                E14AB2 ();
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_cod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPAB0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_cod_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAB2( ) ;
            }
         }
      }

      protected void PAAB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vFUNCAODADOS_CODIGO_" + sGXsfl_14_idx;
            dynavFuncaodados_codigo.Name = GXCCtl;
            dynavFuncaodados_codigo.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltrotabela_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvFUNCAODADOS_CODIGOAB2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAODADOS_CODIGO_dataAB2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAODADOS_CODIGO_htmlAB2( )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAODADOS_CODIGO_dataAB2( ) ;
         gxdynajaxindex = 1;
         dynavFuncaodados_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncaodados_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvFUNCAODADOS_CODIGO_dataAB2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00AB2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AB2_A368FuncaoDados_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00AB2_A369FuncaoDados_Nome[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_142( ) ;
         while ( nGXsfl_14_idx <= nRC_GXsfl_14 )
         {
            sendrow_142( ) ;
            nGXsfl_14_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_14_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
            sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
            SubsflControlProps_142( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxgrGridtabelas_refresh( bool A174Tabela_Ativo ,
                                              int A190Tabela_SistemaCod ,
                                              int AV9Sistema_Codigo ,
                                              String A173Tabela_Nome ,
                                              String AV5FiltroTabela_Nome ,
                                              int A368FuncaoDados_Codigo ,
                                              int AV7FuncaoDados_Codigo ,
                                              int A172Tabela_Codigo ,
                                              String A189Tabela_ModuloDes ,
                                              String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDTABELAS_nCurrentRecord = 0;
         RFAB2( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
         edtavTabela_cod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_cod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_cod_Enabled), 5, 0)));
         edtavTabela_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome_Enabled), 5, 0)));
         edtavTabela_modulodes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes_Enabled), 5, 0)));
      }

      protected void RFAB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 14;
         /* Execute user event: E13AB2 */
         E13AB2 ();
         nGXsfl_14_idx = 1;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
         nGXsfl_14_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_142( ) ;
            /* Execute user event: E12AB2 */
            E12AB2 ();
            wbEnd = 14;
            WBAB0( ) ;
         }
         nGXsfl_14_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPAB0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0)));
         edtavTabela_cod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_cod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_cod_Enabled), 5, 0)));
         edtavTabela_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome_Enabled), 5, 0)));
         edtavTabela_modulodes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_modulodes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_modulodes_Enabled), 5, 0)));
         GXVvFUNCAODADOS_CODIGO_htmlAB2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11AB2 */
         E11AB2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5FiltroTabela_Nome = StringUtil.Upper( cgiGet( edtavFiltrotabela_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5FiltroTabela_Nome", AV5FiltroTabela_Nome);
            /* Read saved values. */
            nRC_GXsfl_14 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_14"), ",", "."));
            wcpOAV7FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_Codigo"), ",", "."));
            wcpOAV9Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV9Sistema_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11AB2 */
         E11AB2 ();
         if (returnInSub) return;
      }

      protected void E11AB2( )
      {
         /* Start Routine */
      }

      private void E12AB2( )
      {
         /* Gridtabelas_Load Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV5FiltroTabela_Nome ,
                                              A173Tabela_Nome ,
                                              A174Tabela_Ativo ,
                                              AV9Sistema_Codigo ,
                                              A190Tabela_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV5FiltroTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV5FiltroTabela_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5FiltroTabela_Nome", AV5FiltroTabela_Nome);
         /* Using cursor H00AB3 */
         pr_default.execute(1, new Object[] {AV9Sistema_Codigo, lV5FiltroTabela_Nome});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A188Tabela_ModuloCod = H00AB3_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = H00AB3_n188Tabela_ModuloCod[0];
            A172Tabela_Codigo = H00AB3_A172Tabela_Codigo[0];
            A173Tabela_Nome = H00AB3_A173Tabela_Nome[0];
            A190Tabela_SistemaCod = H00AB3_A190Tabela_SistemaCod[0];
            A174Tabela_Ativo = H00AB3_A174Tabela_Ativo[0];
            A189Tabela_ModuloDes = H00AB3_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = H00AB3_n189Tabela_ModuloDes[0];
            A189Tabela_ModuloDes = H00AB3_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = H00AB3_n189Tabela_ModuloDes[0];
            AV17GXLvl12 = 0;
            /* Using cursor H00AB4 */
            pr_default.execute(2, new Object[] {AV7FuncaoDados_Codigo, A172Tabela_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A368FuncaoDados_Codigo = H00AB4_A368FuncaoDados_Codigo[0];
               AV17GXLvl12 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            if ( AV17GXLvl12 == 0 )
            {
               AV10Tabela_Cod = A172Tabela_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_cod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela_Cod), 6, 0)));
               AV13Tabela_Nome = A173Tabela_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_nome_Internalname, AV13Tabela_Nome);
               AV12Tabela_ModuloDes = A189Tabela_ModuloDes;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavTabela_modulodes_Internalname, AV12Tabela_ModuloDes);
               AV6Flag = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFlag_Internalname, AV6Flag);
               AV18Flag_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 14;
               }
               sendrow_142( ) ;
               if ( isFullAjaxMode( ) && ( nGXsfl_14_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(14, GridtabelasRow);
               }
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void E13AB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      public void GXEnter( )
      {
         /* Execute user event: E14AB2 */
         E14AB2 ();
         if (returnInSub) return;
      }

      protected void E14AB2( )
      {
         /* Enter Routine */
         AV8FuncaoDadosTabela = new SdtFuncaoDadosTabela(context);
         AV8FuncaoDadosTabela.gxTpr_Funcaodados_codigo = AV7FuncaoDados_Codigo;
         AV8FuncaoDadosTabela.gxTpr_Tabela_codigo = AV10Tabela_Cod;
         AV8FuncaoDadosTabela.Save();
         if ( AV8FuncaoDadosTabela.Success() )
         {
            context.CommitDataStores( "WC_FuncaoDadosTabelaIns");
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else
         {
            context.RollbackDataStores( "WC_FuncaoDadosTabelaIns");
         }
      }

      protected void wb_table1_2_AB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblHeader_Internalname, tblHeader_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            wb_table2_5_AB2( true) ;
         }
         else
         {
            wb_table2_5_AB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_AB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"DivS\" data-gxgridid=\"14\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Incluir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_Codigo), 6, 0, ".", "")));
               GridtabelasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynavFuncaodados_codigo.Enabled), 5, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", context.convertURL( AV6Flag));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Tabela_Cod), 6, 0, ".", "")));
               GridtabelasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTabela_cod_Enabled), 5, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( AV13Tabela_Nome));
               GridtabelasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTabela_nome_Enabled), 5, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( AV12Tabela_ModuloDes));
               GridtabelasColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTabela_modulodes_Enabled), 5, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 14 )
         {
            wbEnd = 0;
            nRC_GXsfl_14 = (short)(nGXsfl_14_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridtabelas", GridtabelasContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AB2e( true) ;
         }
         else
         {
            wb_table1_2_AB2e( false) ;
         }
      }

      protected void wb_table2_5_AB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_nome_Internalname, "Filtrar tabelas contendo:", "", "", lblTextblocktabela_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WC_FuncaoDadosTabelaIns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'" + sGXsfl_14_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltrotabela_nome_Internalname, StringUtil.RTrim( AV5FiltroTabela_Nome), StringUtil.RTrim( context.localUtil.Format( AV5FiltroTabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,10);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltrotabela_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WC_FuncaoDadosTabelaIns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_AB2e( true) ;
         }
         else
         {
            wb_table2_5_AB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
         AV9Sistema_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAB2( ) ;
         WSAB2( ) ;
         WEAB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7FuncaoDados_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV9Sistema_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAB2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_funcaodadostabelains");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAB2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7FuncaoDados_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
            AV9Sistema_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         }
         wcpOAV7FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoDados_Codigo"), ",", "."));
         wcpOAV9Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV9Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7FuncaoDados_Codigo != wcpOAV7FuncaoDados_Codigo ) || ( AV9Sistema_Codigo != wcpOAV9Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7FuncaoDados_Codigo = AV7FuncaoDados_Codigo;
         wcpOAV9Sistema_Codigo = AV9Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7FuncaoDados_Codigo = cgiGet( sPrefix+"AV7FuncaoDados_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7FuncaoDados_Codigo) > 0 )
         {
            AV7FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7FuncaoDados_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, dynavFuncaodados_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)));
         }
         else
         {
            AV7FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7FuncaoDados_Codigo_PARM"), ",", "."));
         }
         sCtrlAV9Sistema_Codigo = cgiGet( sPrefix+"AV9Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV9Sistema_Codigo) > 0 )
         {
            AV9Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV9Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         }
         else
         {
            AV9Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV9Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAB2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAB2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoDados_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7FuncaoDados_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoDados_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7FuncaoDados_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV9Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV9Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV9Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlAV9Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117201051");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_funcaodadostabelains.js", "?20203117201051");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_142( )
      {
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO_"+sGXsfl_14_idx;
         edtavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_14_idx;
         edtavTabela_cod_Internalname = sPrefix+"vTABELA_COD_"+sGXsfl_14_idx;
         edtavTabela_nome_Internalname = sPrefix+"vTABELA_NOME_"+sGXsfl_14_idx;
         edtavTabela_modulodes_Internalname = sPrefix+"vTABELA_MODULODES_"+sGXsfl_14_idx;
      }

      protected void SubsflControlProps_fel_142( )
      {
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO_"+sGXsfl_14_fel_idx;
         edtavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_14_fel_idx;
         edtavTabela_cod_Internalname = sPrefix+"vTABELA_COD_"+sGXsfl_14_fel_idx;
         edtavTabela_nome_Internalname = sPrefix+"vTABELA_NOME_"+sGXsfl_14_fel_idx;
         edtavTabela_modulodes_Internalname = sPrefix+"vTABELA_MODULODES_"+sGXsfl_14_fel_idx;
      }

      protected void sendrow_142( )
      {
         SubsflControlProps_142( ) ;
         WBAB0( ) ;
         GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
         if ( subGridtabelas_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
            subGridtabelas_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridtabelas_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( ((int)((nGXsfl_14_idx) % (2))) == 0 )
            {
               subGridtabelas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
               }
            }
            else
            {
               subGridtabelas_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
         }
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_14_idx+"\">") ;
         }
         GXVvFUNCAODADOS_CODIGO_htmlAB2( ) ;
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         if ( ( nGXsfl_14_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vFUNCAODADOS_CODIGO_" + sGXsfl_14_idx;
            dynavFuncaodados_codigo.Name = GXCCtl;
            dynavFuncaodados_codigo.WebTags = "";
         }
         /* ComboBox */
         GridtabelasRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynavFuncaodados_codigo,(String)dynavFuncaodados_codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0)),(short)1,(String)dynavFuncaodados_codigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,dynavFuncaodados_codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         dynavFuncaodados_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7FuncaoDados_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Values", (String)(dynavFuncaodados_codigo.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavFlag_Enabled!=0)&&(edtavFlag_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'"+sPrefix+"',false,'',14)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV6Flag_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV6Flag))&&String.IsNullOrEmpty(StringUtil.RTrim( AV18Flag_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV6Flag)));
         GridtabelasRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavFlag_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV6Flag)) ? AV18Flag_GXI : context.PathToRelativeUrl( AV6Flag)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavFlag_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EENTER."+sGXsfl_14_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV6Flag_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTabela_cod_Enabled!=0)&&(edtavTabela_cod_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'"+sPrefix+"',false,'"+sGXsfl_14_idx+"',14)\"" : " ");
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTabela_cod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Tabela_Cod), 6, 0, ",", "")),((edtavTabela_cod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10Tabela_Cod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV10Tabela_Cod), "ZZZZZ9")),TempTags+((edtavTabela_cod_Enabled!=0)&&(edtavTabela_cod_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTabela_cod_Enabled!=0)&&(edtavTabela_cod_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,17);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTabela_cod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavTabela_cod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTabela_nome_Enabled!=0)&&(edtavTabela_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'"+sPrefix+"',false,'"+sGXsfl_14_idx+"',14)\"" : " ");
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTabela_nome_Internalname,StringUtil.RTrim( AV13Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( AV13Tabela_Nome, "@!")),TempTags+((edtavTabela_nome_Enabled!=0)&&(edtavTabela_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTabela_nome_Enabled!=0)&&(edtavTabela_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTabela_nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTabela_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavTabela_modulodes_Enabled!=0)&&(edtavTabela_modulodes_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 19,'"+sPrefix+"',false,'"+sGXsfl_14_idx+"',14)\"" : " ");
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTabela_modulodes_Internalname,StringUtil.RTrim( AV12Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( AV12Tabela_ModuloDes, "@!")),TempTags+((edtavTabela_modulodes_Enabled!=0)&&(edtavTabela_modulodes_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTabela_modulodes_Enabled!=0)&&(edtavTabela_modulodes_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,19);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTabela_modulodes_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTabela_modulodes_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
         GridtabelasContainer.AddRow(GridtabelasRow);
         nGXsfl_14_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_14_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
         /* End function sendrow_142 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktabela_nome_Internalname = sPrefix+"TEXTBLOCKTABELA_NOME";
         edtavFiltrotabela_nome_Internalname = sPrefix+"vFILTROTABELA_NOME";
         tblTable2_Internalname = sPrefix+"TABLE2";
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO";
         edtavFlag_Internalname = sPrefix+"vFLAG";
         edtavTabela_cod_Internalname = sPrefix+"vTABELA_COD";
         edtavTabela_nome_Internalname = sPrefix+"vTABELA_NOME";
         edtavTabela_modulodes_Internalname = sPrefix+"vTABELA_MODULODES";
         tblHeader_Internalname = sPrefix+"HEADER";
         Form.Internalname = sPrefix+"FORM";
         subGridtabelas_Internalname = sPrefix+"GRIDTABELAS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavTabela_modulodes_Jsonclick = "";
         edtavTabela_modulodes_Visible = -1;
         edtavTabela_nome_Jsonclick = "";
         edtavTabela_nome_Visible = -1;
         edtavTabela_cod_Jsonclick = "";
         edtavTabela_cod_Visible = 0;
         edtavFlag_Jsonclick = "";
         edtavFlag_Visible = -1;
         edtavFlag_Enabled = 1;
         dynavFuncaodados_codigo_Jsonclick = "";
         edtavFiltrotabela_nome_Jsonclick = "";
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Allowselection = 0;
         edtavTabela_modulodes_Enabled = 1;
         edtavTabela_nome_Enabled = 1;
         edtavTabela_cod_Enabled = 1;
         dynavFuncaodados_codigo.Enabled = 0;
         subGridtabelas_Class = "WorkWith";
         subGridtabelas_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'A174Tabela_Ativo',fld:'TABELA_ATIVO',pic:'',nv:false},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV9Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'AV5FiltroTabela_Nome',fld:'vFILTROTABELA_NOME',pic:'@!',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A189Tabela_ModuloDes',fld:'TABELA_MODULODES',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDTABELAS.LOAD","{handler:'E12AB2',iparms:[{av:'A174Tabela_Ativo',fld:'TABELA_ATIVO',pic:'',nv:false},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV9Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'AV5FiltroTabela_Nome',fld:'vFILTROTABELA_NOME',pic:'@!',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A189Tabela_ModuloDes',fld:'TABELA_MODULODES',pic:'@!',nv:''}],oparms:[{av:'AV10Tabela_Cod',fld:'vTABELA_COD',pic:'ZZZZZ9',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'AV12Tabela_ModuloDes',fld:'vTABELA_MODULODES',pic:'@!',nv:''},{av:'AV6Flag',fld:'vFLAG',pic:'',nv:''}]}");
         setEventMetadata("ENTER","{handler:'E14AB2',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'A174Tabela_Ativo',fld:'TABELA_ATIVO',pic:'',nv:false},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV9Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'AV5FiltroTabela_Nome',fld:'vFILTROTABELA_NOME',pic:'@!',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A189Tabela_ModuloDes',fld:'TABELA_MODULODES',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV10Tabela_Cod',fld:'vTABELA_COD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A173Tabela_Nome = "";
         AV5FiltroTabela_Nome = "";
         A189Tabela_ModuloDes = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6Flag = "";
         AV18Flag_GXI = "";
         AV13Tabela_Nome = "";
         AV12Tabela_ModuloDes = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00AB2_A368FuncaoDados_Codigo = new int[1] ;
         H00AB2_A369FuncaoDados_Nome = new String[] {""} ;
         GridtabelasContainer = new GXWebGrid( context);
         lV5FiltroTabela_Nome = "";
         H00AB3_A188Tabela_ModuloCod = new int[1] ;
         H00AB3_n188Tabela_ModuloCod = new bool[] {false} ;
         H00AB3_A172Tabela_Codigo = new int[1] ;
         H00AB3_A173Tabela_Nome = new String[] {""} ;
         H00AB3_A190Tabela_SistemaCod = new int[1] ;
         H00AB3_A174Tabela_Ativo = new bool[] {false} ;
         H00AB3_A189Tabela_ModuloDes = new String[] {""} ;
         H00AB3_n189Tabela_ModuloDes = new bool[] {false} ;
         H00AB4_A172Tabela_Codigo = new int[1] ;
         H00AB4_A368FuncaoDados_Codigo = new int[1] ;
         GridtabelasRow = new GXWebRow();
         AV8FuncaoDadosTabela = new SdtFuncaoDadosTabela(context);
         sStyleString = "";
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         lblTextblocktabela_nome_Jsonclick = "";
         TempTags = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7FuncaoDados_Codigo = "";
         sCtrlAV9Sistema_Codigo = "";
         ClassString = "";
         StyleString = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_funcaodadostabelains__default(),
            new Object[][] {
                new Object[] {
               H00AB2_A368FuncaoDados_Codigo, H00AB2_A369FuncaoDados_Nome
               }
               , new Object[] {
               H00AB3_A188Tabela_ModuloCod, H00AB3_n188Tabela_ModuloCod, H00AB3_A172Tabela_Codigo, H00AB3_A173Tabela_Nome, H00AB3_A190Tabela_SistemaCod, H00AB3_A174Tabela_Ativo, H00AB3_A189Tabela_ModuloDes, H00AB3_n189Tabela_ModuloDes
               }
               , new Object[] {
               H00AB4_A172Tabela_Codigo, H00AB4_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         dynavFuncaodados_codigo.Enabled = 0;
         edtavTabela_cod_Enabled = 0;
         edtavTabela_nome_Enabled = 0;
         edtavTabela_modulodes_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_14 ;
      private short nGXsfl_14_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_14_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short AV17GXLvl12 ;
      private short GRIDTABELAS_nEOF ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short nGXWrapped ;
      private short subGridtabelas_Backstyle ;
      private int AV7FuncaoDados_Codigo ;
      private int AV9Sistema_Codigo ;
      private int wcpOAV7FuncaoDados_Codigo ;
      private int wcpOAV9Sistema_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A172Tabela_Codigo ;
      private int edtavTabela_cod_Enabled ;
      private int edtavTabela_nome_Enabled ;
      private int edtavTabela_modulodes_Enabled ;
      private int AV10Tabela_Cod ;
      private int gxdynajaxindex ;
      private int subGridtabelas_Islastpage ;
      private int A188Tabela_ModuloCod ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int edtavFlag_Enabled ;
      private int edtavFlag_Visible ;
      private int edtavTabela_cod_Visible ;
      private int edtavTabela_nome_Visible ;
      private int edtavTabela_modulodes_Visible ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String dynavFuncaodados_codigo_Internalname ;
      private String sGXsfl_14_idx="0001" ;
      private String A173Tabela_Nome ;
      private String AV5FiltroTabela_Nome ;
      private String A189Tabela_ModuloDes ;
      private String GXKey ;
      private String edtavTabela_cod_Internalname ;
      private String edtavTabela_nome_Internalname ;
      private String edtavTabela_modulodes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavFlag_Internalname ;
      private String AV13Tabela_Nome ;
      private String AV12Tabela_ModuloDes ;
      private String GXCCtl ;
      private String edtavFiltrotabela_nome_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV5FiltroTabela_Nome ;
      private String sStyleString ;
      private String tblHeader_Internalname ;
      private String subGridtabelas_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String tblTable2_Internalname ;
      private String lblTextblocktabela_nome_Internalname ;
      private String lblTextblocktabela_nome_Jsonclick ;
      private String TempTags ;
      private String edtavFiltrotabela_nome_Jsonclick ;
      private String sCtrlAV7FuncaoDados_Codigo ;
      private String sCtrlAV9Sistema_Codigo ;
      private String sGXsfl_14_fel_idx="0001" ;
      private String dynavFuncaodados_codigo_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavFlag_Jsonclick ;
      private String ROClassString ;
      private String edtavTabela_cod_Jsonclick ;
      private String edtavTabela_nome_Jsonclick ;
      private String edtavTabela_modulodes_Jsonclick ;
      private bool entryPointCalled ;
      private bool A174Tabela_Ativo ;
      private bool n189Tabela_ModuloDes ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n188Tabela_ModuloCod ;
      private bool gx_refresh_fired ;
      private bool AV6Flag_IsBlob ;
      private String AV18Flag_GXI ;
      private String AV6Flag ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebColumn GridtabelasColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavFuncaodados_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00AB2_A368FuncaoDados_Codigo ;
      private String[] H00AB2_A369FuncaoDados_Nome ;
      private int[] H00AB3_A188Tabela_ModuloCod ;
      private bool[] H00AB3_n188Tabela_ModuloCod ;
      private int[] H00AB3_A172Tabela_Codigo ;
      private String[] H00AB3_A173Tabela_Nome ;
      private int[] H00AB3_A190Tabela_SistemaCod ;
      private bool[] H00AB3_A174Tabela_Ativo ;
      private String[] H00AB3_A189Tabela_ModuloDes ;
      private bool[] H00AB3_n189Tabela_ModuloDes ;
      private int[] H00AB4_A172Tabela_Codigo ;
      private int[] H00AB4_A368FuncaoDados_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private SdtFuncaoDadosTabela AV8FuncaoDadosTabela ;
   }

   public class wc_funcaodadostabelains__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AB3( IGxContext context ,
                                             String AV5FiltroTabela_Nome ,
                                             String A173Tabela_Nome ,
                                             bool A174Tabela_Ativo ,
                                             int AV9Sistema_Codigo ,
                                             int A190Tabela_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T1.[Tabela_Codigo], T1.[Tabela_Nome], T1.[Tabela_SistemaCod], T1.[Tabela_Ativo], T2.[Modulo_Nome] AS Tabela_ModuloDes FROM ([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_SistemaCod] = @AV9Sistema_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Tabela_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5FiltroTabela_Nome)) )
         {
            sWhereString = sWhereString + " and (UPPER(T1.[Tabela_Nome]) like '%' + UPPER(@lV5FiltroTabela_Nome))";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_SistemaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00AB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AB2 ;
          prmH00AB2 = new Object[] {
          } ;
          Object[] prmH00AB4 ;
          prmH00AB4 = new Object[] {
          new Object[] {"@AV7FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AB3 ;
          prmH00AB3 = new Object[] {
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV5FiltroTabela_Nome",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AB2", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AB2,0,0,true,false )
             ,new CursorDef("H00AB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AB3,100,0,true,false )
             ,new CursorDef("H00AB4", "SELECT [Tabela_Codigo], [FuncaoDados_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV7FuncaoDados_Codigo and [Tabela_Codigo] = @Tabela_Codigo ORDER BY [FuncaoDados_Codigo], [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AB4,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
