/*
               File: WWModulo
        Description:  Modulo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:33:21.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwmodulo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwmodulo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwmodulo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavSistema_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_AREATRABALHOCOD3M2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_53 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_53_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_53_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV69Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17Modulo_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Modulo_Nome1", AV17Modulo_Nome1);
               AV74TFModulo_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFModulo_Nome", AV74TFModulo_Nome);
               AV75TFModulo_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFModulo_Nome_Sel", AV75TFModulo_Nome_Sel);
               AV78TFModulo_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFModulo_Sigla", AV78TFModulo_Sigla);
               AV79TFModulo_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFModulo_Sigla_Sel", AV79TFModulo_Sigla_Sel);
               AV82TFSistema_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFSistema_Descricao", AV82TFSistema_Descricao);
               AV83TFSistema_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFSistema_Descricao_Sel", AV83TFSistema_Descricao_Sel);
               AV76ddo_Modulo_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Modulo_NomeTitleControlIdToReplace", AV76ddo_Modulo_NomeTitleControlIdToReplace);
               AV80ddo_Modulo_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Modulo_SiglaTitleControlIdToReplace", AV80ddo_Modulo_SiglaTitleControlIdToReplace);
               AV84ddo_Sistema_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Sistema_DescricaoTitleControlIdToReplace", AV84ddo_Sistema_DescricaoTitleControlIdToReplace);
               AV109Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV59Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA3M2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START3M2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117332164");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwmodulo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vMODULO_NOME1", StringUtil.RTrim( AV17Modulo_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMODULO_NOME", StringUtil.RTrim( AV74TFModulo_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMODULO_NOME_SEL", StringUtil.RTrim( AV75TFModulo_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMODULO_SIGLA", StringUtil.RTrim( AV78TFModulo_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMODULO_SIGLA_SEL", StringUtil.RTrim( AV79TFModulo_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_DESCRICAO", AV82TFSistema_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_DESCRICAO_SEL", AV83TFSistema_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_53", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_53), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMODULO_NOMETITLEFILTERDATA", AV73Modulo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMODULO_NOMETITLEFILTERDATA", AV73Modulo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMODULO_SIGLATITLEFILTERDATA", AV77Modulo_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMODULO_SIGLATITLEFILTERDATA", AV77Modulo_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_DESCRICAOTITLEFILTERDATA", AV81Sistema_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_DESCRICAOTITLEFILTERDATA", AV81Sistema_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV109Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Caption", StringUtil.RTrim( Ddo_modulo_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Tooltip", StringUtil.RTrim( Ddo_modulo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Cls", StringUtil.RTrim( Ddo_modulo_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_modulo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_modulo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_modulo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_modulo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_modulo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_modulo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_modulo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_modulo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Filtertype", StringUtil.RTrim( Ddo_modulo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_modulo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_modulo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Datalisttype", StringUtil.RTrim( Ddo_modulo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Datalistproc", StringUtil.RTrim( Ddo_modulo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_modulo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Sortasc", StringUtil.RTrim( Ddo_modulo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Sortdsc", StringUtil.RTrim( Ddo_modulo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Loadingdata", StringUtil.RTrim( Ddo_modulo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_modulo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_modulo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_modulo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Caption", StringUtil.RTrim( Ddo_modulo_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_modulo_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Cls", StringUtil.RTrim( Ddo_modulo_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_modulo_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_modulo_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_modulo_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_modulo_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_modulo_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_modulo_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_modulo_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_modulo_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_modulo_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_modulo_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_modulo_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_modulo_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_modulo_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_modulo_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_modulo_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_modulo_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_modulo_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_modulo_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_modulo_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_modulo_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_sistema_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_sistema_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_sistema_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_sistema_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_sistema_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_sistema_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_sistema_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_sistema_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_sistema_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_sistema_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_sistema_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_sistema_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_sistema_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_modulo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_modulo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_modulo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_modulo_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_modulo_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MODULO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_modulo_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_sistema_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE3M2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT3M2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwmodulo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWModulo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Modulo" ;
      }

      protected void WB3M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_3M2( true) ;
         }
         else
         {
            wb_table1_2_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_nome_Internalname, StringUtil.RTrim( AV74TFModulo_Nome), StringUtil.RTrim( context.localUtil.Format( AV74TFModulo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWModulo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_nome_sel_Internalname, StringUtil.RTrim( AV75TFModulo_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV75TFModulo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWModulo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_sigla_Internalname, StringUtil.RTrim( AV78TFModulo_Sigla), StringUtil.RTrim( context.localUtil.Format( AV78TFModulo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWModulo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmodulo_sigla_sel_Internalname, StringUtil.RTrim( AV79TFModulo_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV79TFModulo_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmodulo_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmodulo_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWModulo.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistema_descricao_Internalname, AV82TFSistema_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavTfsistema_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWModulo.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistema_descricao_sel_Internalname, AV83TFSistema_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavTfsistema_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWModulo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MODULO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname, AV76ddo_Modulo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_modulo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWModulo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MODULO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname, AV80ddo_Modulo_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWModulo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_53_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_descricaotitlecontrolidtoreplace_Internalname, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_sistema_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWModulo.htm");
         }
         wbLoad = true;
      }

      protected void START3M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Modulo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3M0( ) ;
      }

      protected void WS3M2( )
      {
         START3M2( ) ;
         EVT3M2( ) ;
      }

      protected void EVT3M2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E113M2 */
                              E113M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MODULO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E123M2 */
                              E123M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MODULO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E133M2 */
                              E133M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E143M2 */
                              E143M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E153M2 */
                              E153M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E163M2 */
                              E163M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E173M2 */
                              E173M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 28), "'DOBTNASSOCIARFUNCOESMODULO'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 28), "'DOBTNASSOCIARFUNCOESMODULO'") == 0 ) )
                           {
                              nGXsfl_53_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
                              SubsflControlProps_532( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV105Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV106Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV60Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV60Display)) ? AV107Display_GXI : context.convertURL( context.PathToRelativeUrl( AV60Display))));
                              A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
                              A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                              A143Modulo_Nome = StringUtil.Upper( cgiGet( edtModulo_Nome_Internalname));
                              A145Modulo_Sigla = StringUtil.Upper( cgiGet( edtModulo_Sigla_Internalname));
                              A128Sistema_Descricao = cgiGet( edtSistema_Descricao_Internalname);
                              n128Sistema_Descricao = false;
                              AV36btnAssociarFuncoesModulo = cgiGet( edtavBtnassociarfuncoesmodulo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarfuncoesmodulo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36btnAssociarFuncoesModulo)) ? AV108Btnassociarfuncoesmodulo_GXI : context.convertURL( context.PathToRelativeUrl( AV36btnAssociarFuncoesModulo))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E183M2 */
                                    E183M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E193M2 */
                                    E193M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E203M2 */
                                    E203M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOBTNASSOCIARFUNCOESMODULO'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E213M2 */
                                    E213M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV69Sistema_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Modulo_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMODULO_NOME1"), AV17Modulo_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmodulo_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_NOME"), AV74TFModulo_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmodulo_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_NOME_SEL"), AV75TFModulo_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmodulo_sigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_SIGLA"), AV78TFModulo_Sigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmodulo_sigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_SIGLA_SEL"), AV79TFModulo_Sigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_DESCRICAO"), AV82TFSistema_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_DESCRICAO_SEL"), AV83TFSistema_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA3M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavSistema_areatrabalhocod.Name = "vSISTEMA_AREATRABALHOCOD";
            dynavSistema_areatrabalhocod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("MODULO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD3M2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_AREATRABALHOCOD_data3M2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_AREATRABALHOCOD_html3M2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_AREATRABALHOCOD_data3M2( ) ;
         gxdynajaxindex = 1;
         dynavSistema_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV69Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_AREATRABALHOCOD_data3M2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H003M2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H003M2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H003M2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_532( ) ;
         while ( nGXsfl_53_idx <= nRC_GXsfl_53 )
         {
            sendrow_532( ) ;
            nGXsfl_53_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_53_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_532( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV69Sistema_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17Modulo_Nome1 ,
                                       String AV74TFModulo_Nome ,
                                       String AV75TFModulo_Nome_Sel ,
                                       String AV78TFModulo_Sigla ,
                                       String AV79TFModulo_Sigla_Sel ,
                                       String AV82TFSistema_Descricao ,
                                       String AV83TFSistema_Descricao_Sel ,
                                       String AV76ddo_Modulo_NomeTitleControlIdToReplace ,
                                       String AV80ddo_Modulo_SiglaTitleControlIdToReplace ,
                                       String AV84ddo_Sistema_DescricaoTitleControlIdToReplace ,
                                       String AV109Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       int A146Modulo_Codigo ,
                                       int AV59Sistema_Codigo ,
                                       int A127Sistema_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3M2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_MODULO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MODULO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "MODULO_NOME", StringUtil.RTrim( A143Modulo_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_MODULO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "MODULO_SIGLA", StringUtil.RTrim( A145Modulo_Sigla));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavSistema_areatrabalhocod.ItemCount > 0 )
         {
            AV69Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavSistema_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3M2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV109Pgmname = "WWModulo";
         context.Gx_err = 0;
      }

      protected void RF3M2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 53;
         /* Execute user event: E193M2 */
         E193M2 ();
         nGXsfl_53_idx = 1;
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_532( ) ;
         nGXsfl_53_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_532( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV96WWModuloDS_1_Sistema_areatrabalhocod ,
                                                 AV97WWModuloDS_2_Dynamicfiltersselector1 ,
                                                 AV98WWModuloDS_3_Modulo_nome1 ,
                                                 AV100WWModuloDS_5_Tfmodulo_nome_sel ,
                                                 AV99WWModuloDS_4_Tfmodulo_nome ,
                                                 AV102WWModuloDS_7_Tfmodulo_sigla_sel ,
                                                 AV101WWModuloDS_6_Tfmodulo_sigla ,
                                                 AV104WWModuloDS_9_Tfsistema_descricao_sel ,
                                                 AV103WWModuloDS_8_Tfsistema_descricao ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 A143Modulo_Nome ,
                                                 A145Modulo_Sigla ,
                                                 A128Sistema_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV98WWModuloDS_3_Modulo_nome1 = StringUtil.PadR( StringUtil.RTrim( AV98WWModuloDS_3_Modulo_nome1), 50, "%");
            lV99WWModuloDS_4_Tfmodulo_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWModuloDS_4_Tfmodulo_nome), 50, "%");
            lV101WWModuloDS_6_Tfmodulo_sigla = StringUtil.PadR( StringUtil.RTrim( AV101WWModuloDS_6_Tfmodulo_sigla), 15, "%");
            lV103WWModuloDS_8_Tfsistema_descricao = StringUtil.Concat( StringUtil.RTrim( AV103WWModuloDS_8_Tfsistema_descricao), "%", "");
            /* Using cursor H003M3 */
            pr_default.execute(1, new Object[] {AV96WWModuloDS_1_Sistema_areatrabalhocod, lV98WWModuloDS_3_Modulo_nome1, lV99WWModuloDS_4_Tfmodulo_nome, AV100WWModuloDS_5_Tfmodulo_nome_sel, lV101WWModuloDS_6_Tfmodulo_sigla, AV102WWModuloDS_7_Tfmodulo_sigla_sel, lV103WWModuloDS_8_Tfsistema_descricao, AV104WWModuloDS_9_Tfsistema_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_53_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A135Sistema_AreaTrabalhoCod = H003M3_A135Sistema_AreaTrabalhoCod[0];
               A128Sistema_Descricao = H003M3_A128Sistema_Descricao[0];
               n128Sistema_Descricao = H003M3_n128Sistema_Descricao[0];
               A145Modulo_Sigla = H003M3_A145Modulo_Sigla[0];
               A143Modulo_Nome = H003M3_A143Modulo_Nome[0];
               A127Sistema_Codigo = H003M3_A127Sistema_Codigo[0];
               A146Modulo_Codigo = H003M3_A146Modulo_Codigo[0];
               A135Sistema_AreaTrabalhoCod = H003M3_A135Sistema_AreaTrabalhoCod[0];
               A128Sistema_Descricao = H003M3_A128Sistema_Descricao[0];
               n128Sistema_Descricao = H003M3_n128Sistema_Descricao[0];
               /* Execute user event: E203M2 */
               E203M2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 53;
            WB3M0( ) ;
         }
         nGXsfl_53_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV96WWModuloDS_1_Sistema_areatrabalhocod ,
                                              AV97WWModuloDS_2_Dynamicfiltersselector1 ,
                                              AV98WWModuloDS_3_Modulo_nome1 ,
                                              AV100WWModuloDS_5_Tfmodulo_nome_sel ,
                                              AV99WWModuloDS_4_Tfmodulo_nome ,
                                              AV102WWModuloDS_7_Tfmodulo_sigla_sel ,
                                              AV101WWModuloDS_6_Tfmodulo_sigla ,
                                              AV104WWModuloDS_9_Tfsistema_descricao_sel ,
                                              AV103WWModuloDS_8_Tfsistema_descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              A128Sistema_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV98WWModuloDS_3_Modulo_nome1 = StringUtil.PadR( StringUtil.RTrim( AV98WWModuloDS_3_Modulo_nome1), 50, "%");
         lV99WWModuloDS_4_Tfmodulo_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWModuloDS_4_Tfmodulo_nome), 50, "%");
         lV101WWModuloDS_6_Tfmodulo_sigla = StringUtil.PadR( StringUtil.RTrim( AV101WWModuloDS_6_Tfmodulo_sigla), 15, "%");
         lV103WWModuloDS_8_Tfsistema_descricao = StringUtil.Concat( StringUtil.RTrim( AV103WWModuloDS_8_Tfsistema_descricao), "%", "");
         /* Using cursor H003M4 */
         pr_default.execute(2, new Object[] {AV96WWModuloDS_1_Sistema_areatrabalhocod, lV98WWModuloDS_3_Modulo_nome1, lV99WWModuloDS_4_Tfmodulo_nome, AV100WWModuloDS_5_Tfmodulo_nome_sel, lV101WWModuloDS_6_Tfmodulo_sigla, AV102WWModuloDS_7_Tfmodulo_sigla_sel, lV103WWModuloDS_8_Tfsistema_descricao, AV104WWModuloDS_9_Tfsistema_descricao_sel});
         GRID_nRecordCount = H003M4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV69Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV17Modulo_Nome1, AV74TFModulo_Nome, AV75TFModulo_Nome_Sel, AV78TFModulo_Sigla, AV79TFModulo_Sigla_Sel, AV82TFSistema_Descricao, AV83TFSistema_Descricao_Sel, AV76ddo_Modulo_NomeTitleControlIdToReplace, AV80ddo_Modulo_SiglaTitleControlIdToReplace, AV84ddo_Sistema_DescricaoTitleControlIdToReplace, AV109Pgmname, AV10GridState, A146Modulo_Codigo, AV59Sistema_Codigo, A127Sistema_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3M0( )
      {
         /* Before Start, stand alone formulas. */
         AV109Pgmname = "WWModulo";
         context.Gx_err = 0;
         GXVvSISTEMA_AREATRABALHOCOD_html3M2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E183M2 */
         E183M2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV85DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vMODULO_NOMETITLEFILTERDATA"), AV73Modulo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMODULO_SIGLATITLEFILTERDATA"), AV77Modulo_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_DESCRICAOTITLEFILTERDATA"), AV81Sistema_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavSistema_areatrabalhocod.Name = dynavSistema_areatrabalhocod_Internalname;
            dynavSistema_areatrabalhocod.CurrentValue = cgiGet( dynavSistema_areatrabalhocod_Internalname);
            AV69Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavSistema_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17Modulo_Nome1 = StringUtil.Upper( cgiGet( edtavModulo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Modulo_Nome1", AV17Modulo_Nome1);
            AV74TFModulo_Nome = StringUtil.Upper( cgiGet( edtavTfmodulo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFModulo_Nome", AV74TFModulo_Nome);
            AV75TFModulo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmodulo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFModulo_Nome_Sel", AV75TFModulo_Nome_Sel);
            AV78TFModulo_Sigla = StringUtil.Upper( cgiGet( edtavTfmodulo_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFModulo_Sigla", AV78TFModulo_Sigla);
            AV79TFModulo_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfmodulo_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFModulo_Sigla_Sel", AV79TFModulo_Sigla_Sel);
            AV82TFSistema_Descricao = cgiGet( edtavTfsistema_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFSistema_Descricao", AV82TFSistema_Descricao);
            AV83TFSistema_Descricao_Sel = cgiGet( edtavTfsistema_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFSistema_Descricao_Sel", AV83TFSistema_Descricao_Sel);
            AV76ddo_Modulo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Modulo_NomeTitleControlIdToReplace", AV76ddo_Modulo_NomeTitleControlIdToReplace);
            AV80ddo_Modulo_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Modulo_SiglaTitleControlIdToReplace", AV80ddo_Modulo_SiglaTitleControlIdToReplace);
            AV84ddo_Sistema_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_sistema_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Sistema_DescricaoTitleControlIdToReplace", AV84ddo_Sistema_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_53 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_53"), ",", "."));
            AV87GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV88GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_modulo_nome_Caption = cgiGet( "DDO_MODULO_NOME_Caption");
            Ddo_modulo_nome_Tooltip = cgiGet( "DDO_MODULO_NOME_Tooltip");
            Ddo_modulo_nome_Cls = cgiGet( "DDO_MODULO_NOME_Cls");
            Ddo_modulo_nome_Filteredtext_set = cgiGet( "DDO_MODULO_NOME_Filteredtext_set");
            Ddo_modulo_nome_Selectedvalue_set = cgiGet( "DDO_MODULO_NOME_Selectedvalue_set");
            Ddo_modulo_nome_Dropdownoptionstype = cgiGet( "DDO_MODULO_NOME_Dropdownoptionstype");
            Ddo_modulo_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MODULO_NOME_Titlecontrolidtoreplace");
            Ddo_modulo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MODULO_NOME_Includesortasc"));
            Ddo_modulo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MODULO_NOME_Includesortdsc"));
            Ddo_modulo_nome_Sortedstatus = cgiGet( "DDO_MODULO_NOME_Sortedstatus");
            Ddo_modulo_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MODULO_NOME_Includefilter"));
            Ddo_modulo_nome_Filtertype = cgiGet( "DDO_MODULO_NOME_Filtertype");
            Ddo_modulo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MODULO_NOME_Filterisrange"));
            Ddo_modulo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MODULO_NOME_Includedatalist"));
            Ddo_modulo_nome_Datalisttype = cgiGet( "DDO_MODULO_NOME_Datalisttype");
            Ddo_modulo_nome_Datalistproc = cgiGet( "DDO_MODULO_NOME_Datalistproc");
            Ddo_modulo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MODULO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_modulo_nome_Sortasc = cgiGet( "DDO_MODULO_NOME_Sortasc");
            Ddo_modulo_nome_Sortdsc = cgiGet( "DDO_MODULO_NOME_Sortdsc");
            Ddo_modulo_nome_Loadingdata = cgiGet( "DDO_MODULO_NOME_Loadingdata");
            Ddo_modulo_nome_Cleanfilter = cgiGet( "DDO_MODULO_NOME_Cleanfilter");
            Ddo_modulo_nome_Noresultsfound = cgiGet( "DDO_MODULO_NOME_Noresultsfound");
            Ddo_modulo_nome_Searchbuttontext = cgiGet( "DDO_MODULO_NOME_Searchbuttontext");
            Ddo_modulo_sigla_Caption = cgiGet( "DDO_MODULO_SIGLA_Caption");
            Ddo_modulo_sigla_Tooltip = cgiGet( "DDO_MODULO_SIGLA_Tooltip");
            Ddo_modulo_sigla_Cls = cgiGet( "DDO_MODULO_SIGLA_Cls");
            Ddo_modulo_sigla_Filteredtext_set = cgiGet( "DDO_MODULO_SIGLA_Filteredtext_set");
            Ddo_modulo_sigla_Selectedvalue_set = cgiGet( "DDO_MODULO_SIGLA_Selectedvalue_set");
            Ddo_modulo_sigla_Dropdownoptionstype = cgiGet( "DDO_MODULO_SIGLA_Dropdownoptionstype");
            Ddo_modulo_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_MODULO_SIGLA_Titlecontrolidtoreplace");
            Ddo_modulo_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MODULO_SIGLA_Includesortasc"));
            Ddo_modulo_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MODULO_SIGLA_Includesortdsc"));
            Ddo_modulo_sigla_Sortedstatus = cgiGet( "DDO_MODULO_SIGLA_Sortedstatus");
            Ddo_modulo_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MODULO_SIGLA_Includefilter"));
            Ddo_modulo_sigla_Filtertype = cgiGet( "DDO_MODULO_SIGLA_Filtertype");
            Ddo_modulo_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MODULO_SIGLA_Filterisrange"));
            Ddo_modulo_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MODULO_SIGLA_Includedatalist"));
            Ddo_modulo_sigla_Datalisttype = cgiGet( "DDO_MODULO_SIGLA_Datalisttype");
            Ddo_modulo_sigla_Datalistproc = cgiGet( "DDO_MODULO_SIGLA_Datalistproc");
            Ddo_modulo_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MODULO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_modulo_sigla_Sortasc = cgiGet( "DDO_MODULO_SIGLA_Sortasc");
            Ddo_modulo_sigla_Sortdsc = cgiGet( "DDO_MODULO_SIGLA_Sortdsc");
            Ddo_modulo_sigla_Loadingdata = cgiGet( "DDO_MODULO_SIGLA_Loadingdata");
            Ddo_modulo_sigla_Cleanfilter = cgiGet( "DDO_MODULO_SIGLA_Cleanfilter");
            Ddo_modulo_sigla_Noresultsfound = cgiGet( "DDO_MODULO_SIGLA_Noresultsfound");
            Ddo_modulo_sigla_Searchbuttontext = cgiGet( "DDO_MODULO_SIGLA_Searchbuttontext");
            Ddo_sistema_descricao_Caption = cgiGet( "DDO_SISTEMA_DESCRICAO_Caption");
            Ddo_sistema_descricao_Tooltip = cgiGet( "DDO_SISTEMA_DESCRICAO_Tooltip");
            Ddo_sistema_descricao_Cls = cgiGet( "DDO_SISTEMA_DESCRICAO_Cls");
            Ddo_sistema_descricao_Filteredtext_set = cgiGet( "DDO_SISTEMA_DESCRICAO_Filteredtext_set");
            Ddo_sistema_descricao_Selectedvalue_set = cgiGet( "DDO_SISTEMA_DESCRICAO_Selectedvalue_set");
            Ddo_sistema_descricao_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_DESCRICAO_Dropdownoptionstype");
            Ddo_sistema_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_sistema_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_DESCRICAO_Includesortasc"));
            Ddo_sistema_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_DESCRICAO_Includesortdsc"));
            Ddo_sistema_descricao_Sortedstatus = cgiGet( "DDO_SISTEMA_DESCRICAO_Sortedstatus");
            Ddo_sistema_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_DESCRICAO_Includefilter"));
            Ddo_sistema_descricao_Filtertype = cgiGet( "DDO_SISTEMA_DESCRICAO_Filtertype");
            Ddo_sistema_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_DESCRICAO_Filterisrange"));
            Ddo_sistema_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_DESCRICAO_Includedatalist"));
            Ddo_sistema_descricao_Datalisttype = cgiGet( "DDO_SISTEMA_DESCRICAO_Datalisttype");
            Ddo_sistema_descricao_Datalistproc = cgiGet( "DDO_SISTEMA_DESCRICAO_Datalistproc");
            Ddo_sistema_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_descricao_Sortasc = cgiGet( "DDO_SISTEMA_DESCRICAO_Sortasc");
            Ddo_sistema_descricao_Sortdsc = cgiGet( "DDO_SISTEMA_DESCRICAO_Sortdsc");
            Ddo_sistema_descricao_Loadingdata = cgiGet( "DDO_SISTEMA_DESCRICAO_Loadingdata");
            Ddo_sistema_descricao_Cleanfilter = cgiGet( "DDO_SISTEMA_DESCRICAO_Cleanfilter");
            Ddo_sistema_descricao_Noresultsfound = cgiGet( "DDO_SISTEMA_DESCRICAO_Noresultsfound");
            Ddo_sistema_descricao_Searchbuttontext = cgiGet( "DDO_SISTEMA_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_modulo_nome_Activeeventkey = cgiGet( "DDO_MODULO_NOME_Activeeventkey");
            Ddo_modulo_nome_Filteredtext_get = cgiGet( "DDO_MODULO_NOME_Filteredtext_get");
            Ddo_modulo_nome_Selectedvalue_get = cgiGet( "DDO_MODULO_NOME_Selectedvalue_get");
            Ddo_modulo_sigla_Activeeventkey = cgiGet( "DDO_MODULO_SIGLA_Activeeventkey");
            Ddo_modulo_sigla_Filteredtext_get = cgiGet( "DDO_MODULO_SIGLA_Filteredtext_get");
            Ddo_modulo_sigla_Selectedvalue_get = cgiGet( "DDO_MODULO_SIGLA_Selectedvalue_get");
            Ddo_sistema_descricao_Activeeventkey = cgiGet( "DDO_SISTEMA_DESCRICAO_Activeeventkey");
            Ddo_sistema_descricao_Filteredtext_get = cgiGet( "DDO_SISTEMA_DESCRICAO_Filteredtext_get");
            Ddo_sistema_descricao_Selectedvalue_get = cgiGet( "DDO_SISTEMA_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV69Sistema_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMODULO_NOME1"), AV17Modulo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_NOME"), AV74TFModulo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_NOME_SEL"), AV75TFModulo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_SIGLA"), AV78TFModulo_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMODULO_SIGLA_SEL"), AV79TFModulo_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_DESCRICAO"), AV82TFSistema_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_DESCRICAO_SEL"), AV83TFSistema_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E183M2 */
         E183M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E183M2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "MODULO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfmodulo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmodulo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_nome_Visible), 5, 0)));
         edtavTfmodulo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmodulo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_nome_sel_Visible), 5, 0)));
         edtavTfmodulo_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmodulo_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_sigla_Visible), 5, 0)));
         edtavTfmodulo_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmodulo_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmodulo_sigla_sel_Visible), 5, 0)));
         edtavTfsistema_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_descricao_Visible), 5, 0)));
         edtavTfsistema_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_descricao_sel_Visible), 5, 0)));
         Ddo_modulo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Modulo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "TitleControlIdToReplace", Ddo_modulo_nome_Titlecontrolidtoreplace);
         AV76ddo_Modulo_NomeTitleControlIdToReplace = Ddo_modulo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Modulo_NomeTitleControlIdToReplace", AV76ddo_Modulo_NomeTitleControlIdToReplace);
         edtavDdo_modulo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_modulo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_modulo_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Modulo_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "TitleControlIdToReplace", Ddo_modulo_sigla_Titlecontrolidtoreplace);
         AV80ddo_Modulo_SiglaTitleControlIdToReplace = Ddo_modulo_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_Modulo_SiglaTitleControlIdToReplace", AV80ddo_Modulo_SiglaTitleControlIdToReplace);
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "TitleControlIdToReplace", Ddo_sistema_descricao_Titlecontrolidtoreplace);
         AV84ddo_Sistema_DescricaoTitleControlIdToReplace = Ddo_sistema_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Sistema_DescricaoTitleControlIdToReplace", AV84ddo_Sistema_DescricaoTitleControlIdToReplace);
         edtavDdo_sistema_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Modulo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV85DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV85DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E193M2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV73Modulo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77Modulo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Sistema_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtModulo_Nome_Titleformat = 2;
         edtModulo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV76ddo_Modulo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Nome_Internalname, "Title", edtModulo_Nome_Title);
         edtModulo_Sigla_Titleformat = 2;
         edtModulo_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV80ddo_Modulo_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Sigla_Internalname, "Title", edtModulo_Sigla_Title);
         edtSistema_Descricao_Titleformat = 2;
         edtSistema_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV84ddo_Sistema_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Descricao_Internalname, "Title", edtSistema_Descricao_Title);
         AV87GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87GridCurrentPage), 10, 0)));
         AV88GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridPageCount), 10, 0)));
         AV69Sistema_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
         AV96WWModuloDS_1_Sistema_areatrabalhocod = AV69Sistema_AreaTrabalhoCod;
         AV97WWModuloDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV98WWModuloDS_3_Modulo_nome1 = AV17Modulo_Nome1;
         AV99WWModuloDS_4_Tfmodulo_nome = AV74TFModulo_Nome;
         AV100WWModuloDS_5_Tfmodulo_nome_sel = AV75TFModulo_Nome_Sel;
         AV101WWModuloDS_6_Tfmodulo_sigla = AV78TFModulo_Sigla;
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = AV79TFModulo_Sigla_Sel;
         AV103WWModuloDS_8_Tfsistema_descricao = AV82TFSistema_Descricao;
         AV104WWModuloDS_9_Tfsistema_descricao_sel = AV83TFSistema_Descricao_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73Modulo_NomeTitleFilterData", AV73Modulo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77Modulo_SiglaTitleFilterData", AV77Modulo_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV81Sistema_DescricaoTitleFilterData", AV81Sistema_DescricaoTitleFilterData);
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E113M2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV86PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV86PageToGo) ;
         }
      }

      protected void E123M2( )
      {
         /* Ddo_modulo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_modulo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_modulo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFModulo_Nome = Ddo_modulo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFModulo_Nome", AV74TFModulo_Nome);
            AV75TFModulo_Nome_Sel = Ddo_modulo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFModulo_Nome_Sel", AV75TFModulo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E133M2( )
      {
         /* Ddo_modulo_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_modulo_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_modulo_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_modulo_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV78TFModulo_Sigla = Ddo_modulo_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFModulo_Sigla", AV78TFModulo_Sigla);
            AV79TFModulo_Sigla_Sel = Ddo_modulo_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFModulo_Sigla_Sel", AV79TFModulo_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E143M2( )
      {
         /* Ddo_sistema_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SortedStatus", Ddo_sistema_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SortedStatus", Ddo_sistema_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV82TFSistema_Descricao = Ddo_sistema_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFSistema_Descricao", AV82TFSistema_Descricao);
            AV83TFSistema_Descricao_Sel = Ddo_sistema_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFSistema_Descricao_Sel", AV83TFSistema_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E203M2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV105Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode("" +AV59Sistema_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV106Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("modulo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode("" +AV59Sistema_Codigo);
         AV60Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV60Display);
         AV107Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV36btnAssociarFuncoesModulo = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociarfuncoesmodulo_Internalname, AV36btnAssociarFuncoesModulo);
         AV108Btnassociarfuncoesmodulo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociarfuncoesmodulo_Tooltiptext = "Clique aqu ip/ Associar as fun��es de usu�rio ao M�dulo!";
         edtModulo_Nome_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtSistema_Descricao_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 53;
         }
         sendrow_532( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_53_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(53, GridRow);
         }
      }

      protected void E153M2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E173M2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E163M2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", dynavSistema_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E213M2( )
      {
         /* 'DobtnAssociarFuncoesModulo' Routine */
         context.PopUp(formatLink("wp_associarfuncoesusuariomodulo.aspx") + "?" + UrlEncode("" +A146Modulo_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_modulo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
         Ddo_modulo_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
         Ddo_sistema_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SortedStatus", Ddo_sistema_descricao_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_modulo_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SortedStatus", Ddo_modulo_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_modulo_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SortedStatus", Ddo_modulo_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_sistema_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SortedStatus", Ddo_sistema_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavModulo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavModulo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavModulo_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MODULO_NOME") == 0 )
         {
            edtavModulo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavModulo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavModulo_nome1_Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV69Sistema_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
         AV74TFModulo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFModulo_Nome", AV74TFModulo_Nome);
         Ddo_modulo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "FilteredText_set", Ddo_modulo_nome_Filteredtext_set);
         AV75TFModulo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFModulo_Nome_Sel", AV75TFModulo_Nome_Sel);
         Ddo_modulo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SelectedValue_set", Ddo_modulo_nome_Selectedvalue_set);
         AV78TFModulo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFModulo_Sigla", AV78TFModulo_Sigla);
         Ddo_modulo_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "FilteredText_set", Ddo_modulo_sigla_Filteredtext_set);
         AV79TFModulo_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFModulo_Sigla_Sel", AV79TFModulo_Sigla_Sel);
         Ddo_modulo_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SelectedValue_set", Ddo_modulo_sigla_Selectedvalue_set);
         AV82TFSistema_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFSistema_Descricao", AV82TFSistema_Descricao);
         Ddo_sistema_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "FilteredText_set", Ddo_sistema_descricao_Filteredtext_set);
         AV83TFSistema_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFSistema_Descricao_Sel", AV83TFSistema_Descricao_Sel);
         Ddo_sistema_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SelectedValue_set", Ddo_sistema_descricao_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "MODULO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17Modulo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Modulo_Nome1", AV17Modulo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV109Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV109Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV109Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S202( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV110GXV1 = 1;
         while ( AV110GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV110GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV69Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME") == 0 )
            {
               AV74TFModulo_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFModulo_Nome", AV74TFModulo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFModulo_Nome)) )
               {
                  Ddo_modulo_nome_Filteredtext_set = AV74TFModulo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "FilteredText_set", Ddo_modulo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME_SEL") == 0 )
            {
               AV75TFModulo_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFModulo_Nome_Sel", AV75TFModulo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFModulo_Nome_Sel)) )
               {
                  Ddo_modulo_nome_Selectedvalue_set = AV75TFModulo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_nome_Internalname, "SelectedValue_set", Ddo_modulo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA") == 0 )
            {
               AV78TFModulo_Sigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFModulo_Sigla", AV78TFModulo_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFModulo_Sigla)) )
               {
                  Ddo_modulo_sigla_Filteredtext_set = AV78TFModulo_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "FilteredText_set", Ddo_modulo_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA_SEL") == 0 )
            {
               AV79TFModulo_Sigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFModulo_Sigla_Sel", AV79TFModulo_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFModulo_Sigla_Sel)) )
               {
                  Ddo_modulo_sigla_Selectedvalue_set = AV79TFModulo_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_modulo_sigla_Internalname, "SelectedValue_set", Ddo_modulo_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_DESCRICAO") == 0 )
            {
               AV82TFSistema_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFSistema_Descricao", AV82TFSistema_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFSistema_Descricao)) )
               {
                  Ddo_sistema_descricao_Filteredtext_set = AV82TFSistema_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "FilteredText_set", Ddo_sistema_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_DESCRICAO_SEL") == 0 )
            {
               AV83TFSistema_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFSistema_Descricao_Sel", AV83TFSistema_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFSistema_Descricao_Sel)) )
               {
                  Ddo_sistema_descricao_Selectedvalue_set = AV83TFSistema_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_descricao_Internalname, "SelectedValue_set", Ddo_sistema_descricao_Selectedvalue_set);
               }
            }
            AV110GXV1 = (int)(AV110GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MODULO_NOME") == 0 )
            {
               AV17Modulo_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Modulo_Nome1", AV17Modulo_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV109Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV69Sistema_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFModulo_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMODULO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFModulo_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFModulo_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMODULO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFModulo_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFModulo_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMODULO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV78TFModulo_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFModulo_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMODULO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFModulo_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFSistema_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFSistema_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFSistema_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV83TFSistema_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV109Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Modulo_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV17Modulo_Nome1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV109Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Modulo";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_3M2( true) ;
         }
         else
         {
            wb_table2_8_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_47_3M2( true) ;
         }
         else
         {
            wb_table3_47_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table3_47_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3M2e( true) ;
         }
         else
         {
            wb_table1_2_3M2e( false) ;
         }
      }

      protected void wb_table3_47_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_50_3M2( true) ;
         }
         else
         {
            wb_table4_50_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table4_50_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_47_3M2e( true) ;
         }
         else
         {
            wb_table3_47_3M2e( false) ;
         }
      }

      protected void wb_table4_50_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"53\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtModulo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtModulo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtModulo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtModulo_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtModulo_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtModulo_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV60Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A143Modulo_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtModulo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtModulo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtModulo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A145Modulo_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtModulo_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtModulo_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A128Sistema_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSistema_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36btnAssociarFuncoesModulo));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociarfuncoesmodulo_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 53 )
         {
            wbEnd = 0;
            nRC_GXsfl_53 = (short)(nGXsfl_53_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_50_3M2e( true) ;
         }
         else
         {
            wb_table4_50_3M2e( false) ;
         }
      }

      protected void wb_table2_8_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblModulotitle_Internalname, "M�dulos", "", "", lblModulotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_3M2( true) ;
         }
         else
         {
            wb_table5_13_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWModulo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_22_3M2( true) ;
         }
         else
         {
            wb_table6_22_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3M2e( true) ;
         }
         else
         {
            wb_table2_8_3M2e( false) ;
         }
      }

      protected void wb_table6_22_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextsistema_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_areatrabalhocod, dynavSistema_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0)), 1, dynavSistema_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WWModulo.htm");
            dynavSistema_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV69Sistema_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_areatrabalhocod_Internalname, "Values", (String)(dynavSistema_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_31_3M2( true) ;
         }
         else
         {
            wb_table7_31_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table7_31_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_3M2e( true) ;
         }
         else
         {
            wb_table6_22_3M2e( false) ;
         }
      }

      protected void wb_table7_31_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_34_3M2( true) ;
         }
         else
         {
            wb_table8_34_3M2( false) ;
         }
         return  ;
      }

      protected void wb_table8_34_3M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_31_3M2e( true) ;
         }
         else
         {
            wb_table7_31_3M2e( false) ;
         }
      }

      protected void wb_table8_34_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_53_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWModulo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_53_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavModulo_nome1_Internalname, StringUtil.RTrim( AV17Modulo_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Modulo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavModulo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavModulo_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWModulo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_34_3M2e( true) ;
         }
         else
         {
            wb_table8_34_3M2e( false) ;
         }
      }

      protected void wb_table5_13_3M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_3M2e( true) ;
         }
         else
         {
            wb_table5_13_3M2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3M2( ) ;
         WS3M2( ) ;
         WE3M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117332592");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwmodulo.js", "?20203117332592");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_532( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_53_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_53_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_53_idx;
         edtModulo_Codigo_Internalname = "MODULO_CODIGO_"+sGXsfl_53_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_53_idx;
         edtModulo_Nome_Internalname = "MODULO_NOME_"+sGXsfl_53_idx;
         edtModulo_Sigla_Internalname = "MODULO_SIGLA_"+sGXsfl_53_idx;
         edtSistema_Descricao_Internalname = "SISTEMA_DESCRICAO_"+sGXsfl_53_idx;
         edtavBtnassociarfuncoesmodulo_Internalname = "vBTNASSOCIARFUNCOESMODULO_"+sGXsfl_53_idx;
      }

      protected void SubsflControlProps_fel_532( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_53_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_53_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_53_fel_idx;
         edtModulo_Codigo_Internalname = "MODULO_CODIGO_"+sGXsfl_53_fel_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_53_fel_idx;
         edtModulo_Nome_Internalname = "MODULO_NOME_"+sGXsfl_53_fel_idx;
         edtModulo_Sigla_Internalname = "MODULO_SIGLA_"+sGXsfl_53_fel_idx;
         edtSistema_Descricao_Internalname = "SISTEMA_DESCRICAO_"+sGXsfl_53_fel_idx;
         edtavBtnassociarfuncoesmodulo_Internalname = "vBTNASSOCIARFUNCOESMODULO_"+sGXsfl_53_fel_idx;
      }

      protected void sendrow_532( )
      {
         SubsflControlProps_532( ) ;
         WB3M0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_53_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_53_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_53_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV105Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV105Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV106Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV106Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV60Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV60Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV107Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV60Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV60Display)) ? AV107Display_GXI : context.PathToRelativeUrl( AV60Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV60Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Nome_Internalname,StringUtil.RTrim( A143Modulo_Nome),StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtModulo_Nome_Link,(String)"",(String)"",(String)"",(String)edtModulo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtModulo_Sigla_Internalname,StringUtil.RTrim( A145Modulo_Sigla),StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtModulo_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Descricao_Internalname,(String)A128Sistema_Descricao,(String)A128Sistema_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSistema_Descricao_Link,(String)"",(String)"",(String)"",(String)edtSistema_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)53,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociarfuncoesmodulo_Enabled!=0)&&(edtavBtnassociarfuncoesmodulo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 62,'',false,'',53)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV36btnAssociarFuncoesModulo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36btnAssociarFuncoesModulo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV108Btnassociarfuncoesmodulo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36btnAssociarFuncoesModulo)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociarfuncoesmodulo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36btnAssociarFuncoesModulo)) ? AV108Btnassociarfuncoesmodulo_GXI : context.PathToRelativeUrl( AV36btnAssociarFuncoesModulo)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnassociarfuncoesmodulo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnassociarfuncoesmodulo_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOBTNASSOCIARFUNCOESMODULO\\'."+sGXsfl_53_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV36btnAssociarFuncoesModulo_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_MODULO_CODIGO"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sGXsfl_53_idx, context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sGXsfl_53_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MODULO_NOME"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sGXsfl_53_idx, StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_MODULO_SIGLA"+"_"+sGXsfl_53_idx, GetSecureSignedToken( sGXsfl_53_idx, StringUtil.RTrim( context.localUtil.Format( A145Modulo_Sigla, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_53_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_53_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_53_idx+1));
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_532( ) ;
         }
         /* End function sendrow_532 */
      }

      protected void init_default_properties( )
      {
         lblModulotitle_Internalname = "MODULOTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextsistema_areatrabalhocod_Internalname = "FILTERTEXTSISTEMA_AREATRABALHOCOD";
         dynavSistema_areatrabalhocod_Internalname = "vSISTEMA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavModulo_nome1_Internalname = "vMODULO_NOME1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtModulo_Codigo_Internalname = "MODULO_CODIGO";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         edtModulo_Nome_Internalname = "MODULO_NOME";
         edtModulo_Sigla_Internalname = "MODULO_SIGLA";
         edtSistema_Descricao_Internalname = "SISTEMA_DESCRICAO";
         edtavBtnassociarfuncoesmodulo_Internalname = "vBTNASSOCIARFUNCOESMODULO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfmodulo_nome_Internalname = "vTFMODULO_NOME";
         edtavTfmodulo_nome_sel_Internalname = "vTFMODULO_NOME_SEL";
         edtavTfmodulo_sigla_Internalname = "vTFMODULO_SIGLA";
         edtavTfmodulo_sigla_sel_Internalname = "vTFMODULO_SIGLA_SEL";
         edtavTfsistema_descricao_Internalname = "vTFSISTEMA_DESCRICAO";
         edtavTfsistema_descricao_sel_Internalname = "vTFSISTEMA_DESCRICAO_SEL";
         Ddo_modulo_nome_Internalname = "DDO_MODULO_NOME";
         edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname = "vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_modulo_sigla_Internalname = "DDO_MODULO_SIGLA";
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname = "vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_sistema_descricao_Internalname = "DDO_SISTEMA_DESCRICAO";
         edtavDdo_sistema_descricaotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBtnassociarfuncoesmodulo_Jsonclick = "";
         edtavBtnassociarfuncoesmodulo_Visible = -1;
         edtavBtnassociarfuncoesmodulo_Enabled = 1;
         edtSistema_Descricao_Jsonclick = "";
         edtModulo_Sigla_Jsonclick = "";
         edtModulo_Nome_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtModulo_Codigo_Jsonclick = "";
         edtavModulo_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavSistema_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavBtnassociarfuncoesmodulo_Tooltiptext = "Clique aqu ip/ Associar as fun��es de usu�rio ao M�dulo!";
         edtSistema_Descricao_Link = "";
         edtModulo_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtSistema_Descricao_Titleformat = 0;
         edtModulo_Sigla_Titleformat = 0;
         edtModulo_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavModulo_nome1_Visible = 1;
         edtSistema_Descricao_Title = "Descri��o";
         edtModulo_Sigla_Title = "Sigla";
         edtModulo_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_sistema_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_modulo_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfsistema_descricao_sel_Visible = 1;
         edtavTfsistema_descricao_Visible = 1;
         edtavTfmodulo_sigla_sel_Jsonclick = "";
         edtavTfmodulo_sigla_sel_Visible = 1;
         edtavTfmodulo_sigla_Jsonclick = "";
         edtavTfmodulo_sigla_Visible = 1;
         edtavTfmodulo_nome_sel_Jsonclick = "";
         edtavTfmodulo_nome_sel_Visible = 1;
         edtavTfmodulo_nome_Jsonclick = "";
         edtavTfmodulo_nome_Visible = 1;
         Ddo_sistema_descricao_Searchbuttontext = "Pesquisar";
         Ddo_sistema_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_descricao_Loadingdata = "Carregando dados...";
         Ddo_sistema_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_descricao_Datalistproc = "GetWWModuloFilterData";
         Ddo_sistema_descricao_Datalisttype = "Dynamic";
         Ddo_sistema_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_descricao_Filtertype = "Character";
         Ddo_sistema_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_descricao_Titlecontrolidtoreplace = "";
         Ddo_sistema_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_descricao_Cls = "ColumnSettings";
         Ddo_sistema_descricao_Tooltip = "Op��es";
         Ddo_sistema_descricao_Caption = "";
         Ddo_modulo_sigla_Searchbuttontext = "Pesquisar";
         Ddo_modulo_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_modulo_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_modulo_sigla_Loadingdata = "Carregando dados...";
         Ddo_modulo_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_modulo_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_modulo_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_modulo_sigla_Datalistproc = "GetWWModuloFilterData";
         Ddo_modulo_sigla_Datalisttype = "Dynamic";
         Ddo_modulo_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_modulo_sigla_Filtertype = "Character";
         Ddo_modulo_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_modulo_sigla_Titlecontrolidtoreplace = "";
         Ddo_modulo_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_modulo_sigla_Cls = "ColumnSettings";
         Ddo_modulo_sigla_Tooltip = "Op��es";
         Ddo_modulo_sigla_Caption = "";
         Ddo_modulo_nome_Searchbuttontext = "Pesquisar";
         Ddo_modulo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_modulo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_modulo_nome_Loadingdata = "Carregando dados...";
         Ddo_modulo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_modulo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_modulo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_modulo_nome_Datalistproc = "GetWWModuloFilterData";
         Ddo_modulo_nome_Datalisttype = "Dynamic";
         Ddo_modulo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_modulo_nome_Filtertype = "Character";
         Ddo_modulo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_modulo_nome_Titlecontrolidtoreplace = "";
         Ddo_modulo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_modulo_nome_Cls = "ColumnSettings";
         Ddo_modulo_nome_Tooltip = "Op��es";
         Ddo_modulo_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Modulo";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV73Modulo_NomeTitleFilterData',fld:'vMODULO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV77Modulo_SiglaTitleFilterData',fld:'vMODULO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV81Sistema_DescricaoTitleFilterData',fld:'vSISTEMA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtModulo_Nome_Titleformat',ctrl:'MODULO_NOME',prop:'Titleformat'},{av:'edtModulo_Nome_Title',ctrl:'MODULO_NOME',prop:'Title'},{av:'edtModulo_Sigla_Titleformat',ctrl:'MODULO_SIGLA',prop:'Titleformat'},{av:'edtModulo_Sigla_Title',ctrl:'MODULO_SIGLA',prop:'Title'},{av:'edtSistema_Descricao_Titleformat',ctrl:'SISTEMA_DESCRICAO',prop:'Titleformat'},{av:'edtSistema_Descricao_Title',ctrl:'SISTEMA_DESCRICAO',prop:'Title'},{av:'AV87GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV88GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_MODULO_NOME.ONOPTIONCLICKED","{handler:'E123M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_modulo_nome_Activeeventkey',ctrl:'DDO_MODULO_NOME',prop:'ActiveEventKey'},{av:'Ddo_modulo_nome_Filteredtext_get',ctrl:'DDO_MODULO_NOME',prop:'FilteredText_get'},{av:'Ddo_modulo_nome_Selectedvalue_get',ctrl:'DDO_MODULO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_modulo_nome_Sortedstatus',ctrl:'DDO_MODULO_NOME',prop:'SortedStatus'},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Sortedstatus',ctrl:'DDO_MODULO_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_descricao_Sortedstatus',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MODULO_SIGLA.ONOPTIONCLICKED","{handler:'E133M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_modulo_sigla_Activeeventkey',ctrl:'DDO_MODULO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_modulo_sigla_Filteredtext_get',ctrl:'DDO_MODULO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_modulo_sigla_Selectedvalue_get',ctrl:'DDO_MODULO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_modulo_sigla_Sortedstatus',ctrl:'DDO_MODULO_SIGLA',prop:'SortedStatus'},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Sortedstatus',ctrl:'DDO_MODULO_NOME',prop:'SortedStatus'},{av:'Ddo_sistema_descricao_Sortedstatus',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_DESCRICAO.ONOPTIONCLICKED","{handler:'E143M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_sistema_descricao_Activeeventkey',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_sistema_descricao_Filteredtext_get',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_sistema_descricao_Selectedvalue_get',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_descricao_Sortedstatus',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'SortedStatus'},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_modulo_nome_Sortedstatus',ctrl:'DDO_MODULO_NOME',prop:'SortedStatus'},{av:'Ddo_modulo_sigla_Sortedstatus',ctrl:'DDO_MODULO_SIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E203M2',iparms:[{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV60Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV36btnAssociarFuncoesModulo',fld:'vBTNASSOCIARFUNCOESMODULO',pic:'',nv:''},{av:'edtavBtnassociarfuncoesmodulo_Tooltiptext',ctrl:'vBTNASSOCIARFUNCOESMODULO',prop:'Tooltiptext'},{av:'edtModulo_Nome_Link',ctrl:'MODULO_NOME',prop:'Link'},{av:'edtSistema_Descricao_Link',ctrl:'SISTEMA_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E153M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E173M2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavModulo_nome1_Visible',ctrl:'vMODULO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E163M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Filteredtext_set',ctrl:'DDO_MODULO_NOME',prop:'FilteredText_set'},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_nome_Selectedvalue_set',ctrl:'DDO_MODULO_NOME',prop:'SelectedValue_set'},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Filteredtext_set',ctrl:'DDO_MODULO_SIGLA',prop:'FilteredText_set'},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_modulo_sigla_Selectedvalue_set',ctrl:'DDO_MODULO_SIGLA',prop:'SelectedValue_set'},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'Ddo_sistema_descricao_Filteredtext_set',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'FilteredText_set'},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_sistema_descricao_Selectedvalue_set',ctrl:'DDO_SISTEMA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavModulo_nome1_Visible',ctrl:'vMODULO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOBTNASSOCIARFUNCOESMODULO'","{handler:'E213M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Modulo_Nome1',fld:'vMODULO_NOME1',pic:'@!',nv:''},{av:'AV74TFModulo_Nome',fld:'vTFMODULO_NOME',pic:'@!',nv:''},{av:'AV75TFModulo_Nome_Sel',fld:'vTFMODULO_NOME_SEL',pic:'@!',nv:''},{av:'AV78TFModulo_Sigla',fld:'vTFMODULO_SIGLA',pic:'@!',nv:''},{av:'AV79TFModulo_Sigla_Sel',fld:'vTFMODULO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV82TFSistema_Descricao',fld:'vTFSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV83TFSistema_Descricao_Sel',fld:'vTFSISTEMA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV76ddo_Modulo_NomeTitleControlIdToReplace',fld:'vDDO_MODULO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_Modulo_SiglaTitleControlIdToReplace',fld:'vDDO_MODULO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Sistema_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV59Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_modulo_nome_Activeeventkey = "";
         Ddo_modulo_nome_Filteredtext_get = "";
         Ddo_modulo_nome_Selectedvalue_get = "";
         Ddo_modulo_sigla_Activeeventkey = "";
         Ddo_modulo_sigla_Filteredtext_get = "";
         Ddo_modulo_sigla_Selectedvalue_get = "";
         Ddo_sistema_descricao_Activeeventkey = "";
         Ddo_sistema_descricao_Filteredtext_get = "";
         Ddo_sistema_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Modulo_Nome1 = "";
         AV74TFModulo_Nome = "";
         AV75TFModulo_Nome_Sel = "";
         AV78TFModulo_Sigla = "";
         AV79TFModulo_Sigla_Sel = "";
         AV82TFSistema_Descricao = "";
         AV83TFSistema_Descricao_Sel = "";
         AV76ddo_Modulo_NomeTitleControlIdToReplace = "";
         AV80ddo_Modulo_SiglaTitleControlIdToReplace = "";
         AV84ddo_Sistema_DescricaoTitleControlIdToReplace = "";
         AV109Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV85DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV73Modulo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77Modulo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Sistema_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_modulo_nome_Filteredtext_set = "";
         Ddo_modulo_nome_Selectedvalue_set = "";
         Ddo_modulo_nome_Sortedstatus = "";
         Ddo_modulo_sigla_Filteredtext_set = "";
         Ddo_modulo_sigla_Selectedvalue_set = "";
         Ddo_modulo_sigla_Sortedstatus = "";
         Ddo_sistema_descricao_Filteredtext_set = "";
         Ddo_sistema_descricao_Selectedvalue_set = "";
         Ddo_sistema_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV105Update_GXI = "";
         AV32Delete = "";
         AV106Delete_GXI = "";
         AV60Display = "";
         AV107Display_GXI = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         A128Sistema_Descricao = "";
         AV36btnAssociarFuncoesModulo = "";
         AV108Btnassociarfuncoesmodulo_GXI = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H003M2_A5AreaTrabalho_Codigo = new int[1] ;
         H003M2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H003M2_A72AreaTrabalho_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV98WWModuloDS_3_Modulo_nome1 = "";
         lV99WWModuloDS_4_Tfmodulo_nome = "";
         lV101WWModuloDS_6_Tfmodulo_sigla = "";
         lV103WWModuloDS_8_Tfsistema_descricao = "";
         AV97WWModuloDS_2_Dynamicfiltersselector1 = "";
         AV98WWModuloDS_3_Modulo_nome1 = "";
         AV100WWModuloDS_5_Tfmodulo_nome_sel = "";
         AV99WWModuloDS_4_Tfmodulo_nome = "";
         AV102WWModuloDS_7_Tfmodulo_sigla_sel = "";
         AV101WWModuloDS_6_Tfmodulo_sigla = "";
         AV104WWModuloDS_9_Tfsistema_descricao_sel = "";
         AV103WWModuloDS_8_Tfsistema_descricao = "";
         H003M3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H003M3_A128Sistema_Descricao = new String[] {""} ;
         H003M3_n128Sistema_Descricao = new bool[] {false} ;
         H003M3_A145Modulo_Sigla = new String[] {""} ;
         H003M3_A143Modulo_Nome = new String[] {""} ;
         H003M3_A127Sistema_Codigo = new int[1] ;
         H003M3_A146Modulo_Codigo = new int[1] ;
         H003M4_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblModulotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblFiltertextsistema_areatrabalhocod_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwmodulo__default(),
            new Object[][] {
                new Object[] {
               H003M2_A5AreaTrabalho_Codigo, H003M2_A6AreaTrabalho_Descricao, H003M2_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H003M3_A135Sistema_AreaTrabalhoCod, H003M3_A128Sistema_Descricao, H003M3_n128Sistema_Descricao, H003M3_A145Modulo_Sigla, H003M3_A143Modulo_Nome, H003M3_A127Sistema_Codigo, H003M3_A146Modulo_Codigo
               }
               , new Object[] {
               H003M4_AGRID_nRecordCount
               }
            }
         );
         AV109Pgmname = "WWModulo";
         /* GeneXus formulas. */
         AV109Pgmname = "WWModulo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_53 ;
      private short nGXsfl_53_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_53_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtModulo_Nome_Titleformat ;
      private short edtModulo_Sigla_Titleformat ;
      private short edtSistema_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV69Sistema_AreaTrabalhoCod ;
      private int A146Modulo_Codigo ;
      private int AV59Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_modulo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_modulo_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfmodulo_nome_Visible ;
      private int edtavTfmodulo_nome_sel_Visible ;
      private int edtavTfmodulo_sigla_Visible ;
      private int edtavTfmodulo_sigla_sel_Visible ;
      private int edtavTfsistema_descricao_Visible ;
      private int edtavTfsistema_descricao_sel_Visible ;
      private int edtavDdo_modulo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_modulo_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_descricaotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV96WWModuloDS_1_Sistema_areatrabalhocod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV86PageToGo ;
      private int edtavModulo_nome1_Visible ;
      private int AV110GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavBtnassociarfuncoesmodulo_Enabled ;
      private int edtavBtnassociarfuncoesmodulo_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV87GridCurrentPage ;
      private long AV88GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_modulo_nome_Activeeventkey ;
      private String Ddo_modulo_nome_Filteredtext_get ;
      private String Ddo_modulo_nome_Selectedvalue_get ;
      private String Ddo_modulo_sigla_Activeeventkey ;
      private String Ddo_modulo_sigla_Filteredtext_get ;
      private String Ddo_modulo_sigla_Selectedvalue_get ;
      private String Ddo_sistema_descricao_Activeeventkey ;
      private String Ddo_sistema_descricao_Filteredtext_get ;
      private String Ddo_sistema_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_53_idx="0001" ;
      private String AV17Modulo_Nome1 ;
      private String AV74TFModulo_Nome ;
      private String AV75TFModulo_Nome_Sel ;
      private String AV78TFModulo_Sigla ;
      private String AV79TFModulo_Sigla_Sel ;
      private String AV109Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_modulo_nome_Caption ;
      private String Ddo_modulo_nome_Tooltip ;
      private String Ddo_modulo_nome_Cls ;
      private String Ddo_modulo_nome_Filteredtext_set ;
      private String Ddo_modulo_nome_Selectedvalue_set ;
      private String Ddo_modulo_nome_Dropdownoptionstype ;
      private String Ddo_modulo_nome_Titlecontrolidtoreplace ;
      private String Ddo_modulo_nome_Sortedstatus ;
      private String Ddo_modulo_nome_Filtertype ;
      private String Ddo_modulo_nome_Datalisttype ;
      private String Ddo_modulo_nome_Datalistproc ;
      private String Ddo_modulo_nome_Sortasc ;
      private String Ddo_modulo_nome_Sortdsc ;
      private String Ddo_modulo_nome_Loadingdata ;
      private String Ddo_modulo_nome_Cleanfilter ;
      private String Ddo_modulo_nome_Noresultsfound ;
      private String Ddo_modulo_nome_Searchbuttontext ;
      private String Ddo_modulo_sigla_Caption ;
      private String Ddo_modulo_sigla_Tooltip ;
      private String Ddo_modulo_sigla_Cls ;
      private String Ddo_modulo_sigla_Filteredtext_set ;
      private String Ddo_modulo_sigla_Selectedvalue_set ;
      private String Ddo_modulo_sigla_Dropdownoptionstype ;
      private String Ddo_modulo_sigla_Titlecontrolidtoreplace ;
      private String Ddo_modulo_sigla_Sortedstatus ;
      private String Ddo_modulo_sigla_Filtertype ;
      private String Ddo_modulo_sigla_Datalisttype ;
      private String Ddo_modulo_sigla_Datalistproc ;
      private String Ddo_modulo_sigla_Sortasc ;
      private String Ddo_modulo_sigla_Sortdsc ;
      private String Ddo_modulo_sigla_Loadingdata ;
      private String Ddo_modulo_sigla_Cleanfilter ;
      private String Ddo_modulo_sigla_Noresultsfound ;
      private String Ddo_modulo_sigla_Searchbuttontext ;
      private String Ddo_sistema_descricao_Caption ;
      private String Ddo_sistema_descricao_Tooltip ;
      private String Ddo_sistema_descricao_Cls ;
      private String Ddo_sistema_descricao_Filteredtext_set ;
      private String Ddo_sistema_descricao_Selectedvalue_set ;
      private String Ddo_sistema_descricao_Dropdownoptionstype ;
      private String Ddo_sistema_descricao_Titlecontrolidtoreplace ;
      private String Ddo_sistema_descricao_Sortedstatus ;
      private String Ddo_sistema_descricao_Filtertype ;
      private String Ddo_sistema_descricao_Datalisttype ;
      private String Ddo_sistema_descricao_Datalistproc ;
      private String Ddo_sistema_descricao_Sortasc ;
      private String Ddo_sistema_descricao_Sortdsc ;
      private String Ddo_sistema_descricao_Loadingdata ;
      private String Ddo_sistema_descricao_Cleanfilter ;
      private String Ddo_sistema_descricao_Noresultsfound ;
      private String Ddo_sistema_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfmodulo_nome_Internalname ;
      private String edtavTfmodulo_nome_Jsonclick ;
      private String edtavTfmodulo_nome_sel_Internalname ;
      private String edtavTfmodulo_nome_sel_Jsonclick ;
      private String edtavTfmodulo_sigla_Internalname ;
      private String edtavTfmodulo_sigla_Jsonclick ;
      private String edtavTfmodulo_sigla_sel_Internalname ;
      private String edtavTfmodulo_sigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfsistema_descricao_Internalname ;
      private String edtavTfsistema_descricao_sel_Internalname ;
      private String edtavDdo_modulo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_modulo_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtModulo_Codigo_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String A143Modulo_Nome ;
      private String edtModulo_Nome_Internalname ;
      private String A145Modulo_Sigla ;
      private String edtModulo_Sigla_Internalname ;
      private String edtSistema_Descricao_Internalname ;
      private String edtavBtnassociarfuncoesmodulo_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV98WWModuloDS_3_Modulo_nome1 ;
      private String lV99WWModuloDS_4_Tfmodulo_nome ;
      private String lV101WWModuloDS_6_Tfmodulo_sigla ;
      private String AV98WWModuloDS_3_Modulo_nome1 ;
      private String AV100WWModuloDS_5_Tfmodulo_nome_sel ;
      private String AV99WWModuloDS_4_Tfmodulo_nome ;
      private String AV102WWModuloDS_7_Tfmodulo_sigla_sel ;
      private String AV101WWModuloDS_6_Tfmodulo_sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavSistema_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavModulo_nome1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_modulo_nome_Internalname ;
      private String Ddo_modulo_sigla_Internalname ;
      private String Ddo_sistema_descricao_Internalname ;
      private String edtModulo_Nome_Title ;
      private String edtModulo_Sigla_Title ;
      private String edtSistema_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavBtnassociarfuncoesmodulo_Tooltiptext ;
      private String edtModulo_Nome_Link ;
      private String edtSistema_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblModulotitle_Internalname ;
      private String lblModulotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextsistema_areatrabalhocod_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Jsonclick ;
      private String dynavSistema_areatrabalhocod_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavModulo_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_53_fel_idx="0001" ;
      private String ROClassString ;
      private String edtModulo_Codigo_Jsonclick ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtModulo_Nome_Jsonclick ;
      private String edtModulo_Sigla_Jsonclick ;
      private String edtSistema_Descricao_Jsonclick ;
      private String edtavBtnassociarfuncoesmodulo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_modulo_nome_Includesortasc ;
      private bool Ddo_modulo_nome_Includesortdsc ;
      private bool Ddo_modulo_nome_Includefilter ;
      private bool Ddo_modulo_nome_Filterisrange ;
      private bool Ddo_modulo_nome_Includedatalist ;
      private bool Ddo_modulo_sigla_Includesortasc ;
      private bool Ddo_modulo_sigla_Includesortdsc ;
      private bool Ddo_modulo_sigla_Includefilter ;
      private bool Ddo_modulo_sigla_Filterisrange ;
      private bool Ddo_modulo_sigla_Includedatalist ;
      private bool Ddo_sistema_descricao_Includesortasc ;
      private bool Ddo_sistema_descricao_Includesortdsc ;
      private bool Ddo_sistema_descricao_Includefilter ;
      private bool Ddo_sistema_descricao_Filterisrange ;
      private bool Ddo_sistema_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n128Sistema_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV60Display_IsBlob ;
      private bool AV36btnAssociarFuncoesModulo_IsBlob ;
      private String A128Sistema_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV82TFSistema_Descricao ;
      private String AV83TFSistema_Descricao_Sel ;
      private String AV76ddo_Modulo_NomeTitleControlIdToReplace ;
      private String AV80ddo_Modulo_SiglaTitleControlIdToReplace ;
      private String AV84ddo_Sistema_DescricaoTitleControlIdToReplace ;
      private String AV105Update_GXI ;
      private String AV106Delete_GXI ;
      private String AV107Display_GXI ;
      private String AV108Btnassociarfuncoesmodulo_GXI ;
      private String lV103WWModuloDS_8_Tfsistema_descricao ;
      private String AV97WWModuloDS_2_Dynamicfiltersselector1 ;
      private String AV104WWModuloDS_9_Tfsistema_descricao_sel ;
      private String AV103WWModuloDS_8_Tfsistema_descricao ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV60Display ;
      private String AV36btnAssociarFuncoesModulo ;
      private IGxSession AV33Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavSistema_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private IDataStoreProvider pr_default ;
      private int[] H003M2_A5AreaTrabalho_Codigo ;
      private String[] H003M2_A6AreaTrabalho_Descricao ;
      private bool[] H003M2_A72AreaTrabalho_Ativo ;
      private int[] H003M3_A135Sistema_AreaTrabalhoCod ;
      private String[] H003M3_A128Sistema_Descricao ;
      private bool[] H003M3_n128Sistema_Descricao ;
      private String[] H003M3_A145Modulo_Sigla ;
      private String[] H003M3_A143Modulo_Nome ;
      private int[] H003M3_A127Sistema_Codigo ;
      private int[] H003M3_A146Modulo_Codigo ;
      private long[] H003M4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Modulo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77Modulo_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV81Sistema_DescricaoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV85DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwmodulo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003M3( IGxContext context ,
                                             int AV96WWModuloDS_1_Sistema_areatrabalhocod ,
                                             String AV97WWModuloDS_2_Dynamicfiltersselector1 ,
                                             String AV98WWModuloDS_3_Modulo_nome1 ,
                                             String AV100WWModuloDS_5_Tfmodulo_nome_sel ,
                                             String AV99WWModuloDS_4_Tfmodulo_nome ,
                                             String AV102WWModuloDS_7_Tfmodulo_sigla_sel ,
                                             String AV101WWModuloDS_6_Tfmodulo_sigla ,
                                             String AV104WWModuloDS_9_Tfsistema_descricao_sel ,
                                             String AV103WWModuloDS_8_Tfsistema_descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             String A128Sistema_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [13] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Sistema_AreaTrabalhoCod], T2.[Sistema_Descricao], T1.[Modulo_Sigla], T1.[Modulo_Nome], T1.[Sistema_Codigo], T1.[Modulo_Codigo]";
         sFromString = " FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo])";
         sOrderString = "";
         if ( ! (0==AV96WWModuloDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV96WWModuloDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV96WWModuloDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV97WWModuloDS_2_Dynamicfiltersselector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWModuloDS_3_Modulo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like '%' + @lV98WWModuloDS_3_Modulo_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like '%' + @lV98WWModuloDS_3_Modulo_nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWModuloDS_5_Tfmodulo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWModuloDS_4_Tfmodulo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like @lV99WWModuloDS_4_Tfmodulo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like @lV99WWModuloDS_4_Tfmodulo_nome)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWModuloDS_5_Tfmodulo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] = @AV100WWModuloDS_5_Tfmodulo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] = @AV100WWModuloDS_5_Tfmodulo_nome_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWModuloDS_7_Tfmodulo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWModuloDS_6_Tfmodulo_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] like @lV101WWModuloDS_6_Tfmodulo_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] like @lV101WWModuloDS_6_Tfmodulo_sigla)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWModuloDS_7_Tfmodulo_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] = @AV102WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] = @AV102WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWModuloDS_9_Tfsistema_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWModuloDS_8_Tfsistema_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] like @lV103WWModuloDS_8_Tfsistema_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] like @lV103WWModuloDS_8_Tfsistema_descricao)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWModuloDS_9_Tfsistema_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] = @AV104WWModuloDS_9_Tfsistema_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] = @AV104WWModuloDS_9_Tfsistema_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Sigla]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Sistema_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Sistema_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H003M4( IGxContext context ,
                                             int AV96WWModuloDS_1_Sistema_areatrabalhocod ,
                                             String AV97WWModuloDS_2_Dynamicfiltersselector1 ,
                                             String AV98WWModuloDS_3_Modulo_nome1 ,
                                             String AV100WWModuloDS_5_Tfmodulo_nome_sel ,
                                             String AV99WWModuloDS_4_Tfmodulo_nome ,
                                             String AV102WWModuloDS_7_Tfmodulo_sigla_sel ,
                                             String AV101WWModuloDS_6_Tfmodulo_sigla ,
                                             String AV104WWModuloDS_9_Tfsistema_descricao_sel ,
                                             String AV103WWModuloDS_8_Tfsistema_descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             String A128Sistema_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [8] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo])";
         if ( ! (0==AV96WWModuloDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV96WWModuloDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV96WWModuloDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV97WWModuloDS_2_Dynamicfiltersselector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWModuloDS_3_Modulo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like '%' + @lV98WWModuloDS_3_Modulo_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like '%' + @lV98WWModuloDS_3_Modulo_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWModuloDS_5_Tfmodulo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWModuloDS_4_Tfmodulo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like @lV99WWModuloDS_4_Tfmodulo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like @lV99WWModuloDS_4_Tfmodulo_nome)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWModuloDS_5_Tfmodulo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] = @AV100WWModuloDS_5_Tfmodulo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] = @AV100WWModuloDS_5_Tfmodulo_nome_sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWModuloDS_7_Tfmodulo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWModuloDS_6_Tfmodulo_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] like @lV101WWModuloDS_6_Tfmodulo_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] like @lV101WWModuloDS_6_Tfmodulo_sigla)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWModuloDS_7_Tfmodulo_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] = @AV102WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] = @AV102WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV104WWModuloDS_9_Tfsistema_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWModuloDS_8_Tfsistema_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] like @lV103WWModuloDS_8_Tfsistema_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] like @lV103WWModuloDS_8_Tfsistema_descricao)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWModuloDS_9_Tfsistema_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] = @AV104WWModuloDS_9_Tfsistema_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] = @AV104WWModuloDS_9_Tfsistema_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H003M3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] );
               case 2 :
                     return conditional_H003M4(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003M2 ;
          prmH003M2 = new Object[] {
          } ;
          Object[] prmH003M3 ;
          prmH003M3 = new Object[] {
          new Object[] {"@AV96WWModuloDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWModuloDS_3_Modulo_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWModuloDS_4_Tfmodulo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWModuloDS_5_Tfmodulo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWModuloDS_6_Tfmodulo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWModuloDS_7_Tfmodulo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV103WWModuloDS_8_Tfsistema_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV104WWModuloDS_9_Tfsistema_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH003M4 ;
          prmH003M4 = new Object[] {
          new Object[] {"@AV96WWModuloDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWModuloDS_3_Modulo_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV99WWModuloDS_4_Tfmodulo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWModuloDS_5_Tfmodulo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWModuloDS_6_Tfmodulo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWModuloDS_7_Tfmodulo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV103WWModuloDS_8_Tfsistema_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV104WWModuloDS_9_Tfsistema_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003M2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003M2,0,0,true,false )
             ,new CursorDef("H003M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003M3,11,0,true,false )
             ,new CursorDef("H003M4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003M4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
