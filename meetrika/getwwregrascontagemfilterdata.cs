/*
               File: GetWWRegrasContagemFilterData
        Description: Get WWRegras Contagem Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:26.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwregrascontagemfilterdata : GXProcedure
   {
      public getwwregrascontagemfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwregrascontagemfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwregrascontagemfilterdata objgetwwregrascontagemfilterdata;
         objgetwwregrascontagemfilterdata = new getwwregrascontagemfilterdata();
         objgetwwregrascontagemfilterdata.AV20DDOName = aP0_DDOName;
         objgetwwregrascontagemfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwregrascontagemfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwregrascontagemfilterdata.AV24OptionsJson = "" ;
         objgetwwregrascontagemfilterdata.AV27OptionsDescJson = "" ;
         objgetwwregrascontagemfilterdata.AV29OptionIndexesJson = "" ;
         objgetwwregrascontagemfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwregrascontagemfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwregrascontagemfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwregrascontagemfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_REGRASCONTAGEM_REGRA") == 0 )
         {
            /* Execute user subroutine: 'LOADREGRASCONTAGEM_REGRAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_REGRASCONTAGEM_RESPONSAVEL") == 0 )
         {
            /* Execute user subroutine: 'LOADREGRASCONTAGEM_RESPONSAVELOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWRegrasContagemGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWRegrasContagemGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWRegrasContagemGridState"), "");
         }
         AV56GXV1 = 1;
         while ( AV56GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV56GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "REGRASCONTAGEM_AREATRABALHOCOD") == 0 )
            {
               AV36RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA") == 0 )
            {
               AV10TFRegrasContagem_Regra = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA_SEL") == 0 )
            {
               AV11TFRegrasContagem_Regra_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_DATA") == 0 )
            {
               AV12TFRegrasContagem_Data = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV13TFRegrasContagem_Data_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_VALIDADE") == 0 )
            {
               AV14TFRegrasContagem_Validade = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV15TFRegrasContagem_Validade_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV16TFRegrasContagem_Responsavel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL_SEL") == 0 )
            {
               AV17TFRegrasContagem_Responsavel_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            AV56GXV1 = (int)(AV56GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 )
            {
               AV38RegrasContagem_Regra1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 )
            {
               AV39RegrasContagem_Data1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV40RegrasContagem_Data_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV41RegrasContagem_Responsavel1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV42DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV43DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 )
               {
                  AV44RegrasContagem_Regra2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 )
               {
                  AV45RegrasContagem_Data2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46RegrasContagem_Data_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
               {
                  AV47RegrasContagem_Responsavel2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV48DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV49DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV49DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 )
                  {
                     AV50RegrasContagem_Regra3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 )
                  {
                     AV51RegrasContagem_Data3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV52RegrasContagem_Data_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
                  {
                     AV53RegrasContagem_Responsavel3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREGRASCONTAGEM_REGRAOPTIONS' Routine */
         AV10TFRegrasContagem_Regra = AV18SearchTxt;
         AV11TFRegrasContagem_Regra_Sel = "";
         AV58WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV36RegrasContagem_AreaTrabalhoCod;
         AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV60WWRegrasContagemDS_3_Regrascontagem_regra1 = AV38RegrasContagem_Regra1;
         AV61WWRegrasContagemDS_4_Regrascontagem_data1 = AV39RegrasContagem_Data1;
         AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV40RegrasContagem_Data_To1;
         AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV41RegrasContagem_Responsavel1;
         AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV66WWRegrasContagemDS_9_Regrascontagem_regra2 = AV44RegrasContagem_Regra2;
         AV67WWRegrasContagemDS_10_Regrascontagem_data2 = AV45RegrasContagem_Data2;
         AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV46RegrasContagem_Data_To2;
         AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV47RegrasContagem_Responsavel2;
         AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV49DynamicFiltersSelector3;
         AV72WWRegrasContagemDS_15_Regrascontagem_regra3 = AV50RegrasContagem_Regra3;
         AV73WWRegrasContagemDS_16_Regrascontagem_data3 = AV51RegrasContagem_Data3;
         AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV52RegrasContagem_Data_To3;
         AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV53RegrasContagem_Responsavel3;
         AV76WWRegrasContagemDS_19_Tfregrascontagem_regra = AV10TFRegrasContagem_Regra;
         AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV11TFRegrasContagem_Regra_Sel;
         AV78WWRegrasContagemDS_21_Tfregrascontagem_data = AV12TFRegrasContagem_Data;
         AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV13TFRegrasContagem_Data_To;
         AV80WWRegrasContagemDS_23_Tfregrascontagem_validade = AV14TFRegrasContagem_Validade;
         AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV15TFRegrasContagem_Validade_To;
         AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV16TFRegrasContagem_Responsavel;
         AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV17TFRegrasContagem_Responsavel_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                              AV60WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                              AV61WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                              AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                              AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                              AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                              AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                              AV66WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                              AV67WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                              AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                              AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                              AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                              AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                              AV72WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                              AV73WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                              AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                              AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                              AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                              AV76WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                              AV78WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                              AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                              AV80WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                              AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                              AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                              AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV60WWRegrasContagemDS_3_Regrascontagem_regra1 = StringUtil.Concat( StringUtil.RTrim( AV60WWRegrasContagemDS_3_Regrascontagem_regra1), "%", "");
         lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1), 50, "%");
         lV66WWRegrasContagemDS_9_Regrascontagem_regra2 = StringUtil.Concat( StringUtil.RTrim( AV66WWRegrasContagemDS_9_Regrascontagem_regra2), "%", "");
         lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2), 50, "%");
         lV72WWRegrasContagemDS_15_Regrascontagem_regra3 = StringUtil.Concat( StringUtil.RTrim( AV72WWRegrasContagemDS_15_Regrascontagem_regra3), "%", "");
         lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3), 50, "%");
         lV76WWRegrasContagemDS_19_Tfregrascontagem_regra = StringUtil.Concat( StringUtil.RTrim( AV76WWRegrasContagemDS_19_Tfregrascontagem_regra), "%", "");
         lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = StringUtil.PadR( StringUtil.RTrim( AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel), 50, "%");
         /* Using cursor P00OV2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV60WWRegrasContagemDS_3_Regrascontagem_regra1, AV61WWRegrasContagemDS_4_Regrascontagem_data1, AV62WWRegrasContagemDS_5_Regrascontagem_data_to1, lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1, lV66WWRegrasContagemDS_9_Regrascontagem_regra2, AV67WWRegrasContagemDS_10_Regrascontagem_data2, AV68WWRegrasContagemDS_11_Regrascontagem_data_to2, lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2, lV72WWRegrasContagemDS_15_Regrascontagem_regra3, AV73WWRegrasContagemDS_16_Regrascontagem_data3, AV74WWRegrasContagemDS_17_Regrascontagem_data_to3, lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3, lV76WWRegrasContagemDS_19_Tfregrascontagem_regra, AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel, AV78WWRegrasContagemDS_21_Tfregrascontagem_data, AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to, AV80WWRegrasContagemDS_23_Tfregrascontagem_validade, AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to, lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel, AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A862RegrasContagem_Validade = P00OV2_A862RegrasContagem_Validade[0];
            n862RegrasContagem_Validade = P00OV2_n862RegrasContagem_Validade[0];
            A863RegrasContagem_Responsavel = P00OV2_A863RegrasContagem_Responsavel[0];
            A861RegrasContagem_Data = P00OV2_A861RegrasContagem_Data[0];
            A860RegrasContagem_Regra = P00OV2_A860RegrasContagem_Regra[0];
            A865RegrasContagem_AreaTrabalhoCod = P00OV2_A865RegrasContagem_AreaTrabalhoCod[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A860RegrasContagem_Regra)) )
            {
               AV22Option = A860RegrasContagem_Regra;
               AV23Options.Add(AV22Option, 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADREGRASCONTAGEM_RESPONSAVELOPTIONS' Routine */
         AV16TFRegrasContagem_Responsavel = AV18SearchTxt;
         AV17TFRegrasContagem_Responsavel_Sel = "";
         AV58WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV36RegrasContagem_AreaTrabalhoCod;
         AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV60WWRegrasContagemDS_3_Regrascontagem_regra1 = AV38RegrasContagem_Regra1;
         AV61WWRegrasContagemDS_4_Regrascontagem_data1 = AV39RegrasContagem_Data1;
         AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV40RegrasContagem_Data_To1;
         AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV41RegrasContagem_Responsavel1;
         AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV66WWRegrasContagemDS_9_Regrascontagem_regra2 = AV44RegrasContagem_Regra2;
         AV67WWRegrasContagemDS_10_Regrascontagem_data2 = AV45RegrasContagem_Data2;
         AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV46RegrasContagem_Data_To2;
         AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV47RegrasContagem_Responsavel2;
         AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV49DynamicFiltersSelector3;
         AV72WWRegrasContagemDS_15_Regrascontagem_regra3 = AV50RegrasContagem_Regra3;
         AV73WWRegrasContagemDS_16_Regrascontagem_data3 = AV51RegrasContagem_Data3;
         AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV52RegrasContagem_Data_To3;
         AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV53RegrasContagem_Responsavel3;
         AV76WWRegrasContagemDS_19_Tfregrascontagem_regra = AV10TFRegrasContagem_Regra;
         AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV11TFRegrasContagem_Regra_Sel;
         AV78WWRegrasContagemDS_21_Tfregrascontagem_data = AV12TFRegrasContagem_Data;
         AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV13TFRegrasContagem_Data_To;
         AV80WWRegrasContagemDS_23_Tfregrascontagem_validade = AV14TFRegrasContagem_Validade;
         AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV15TFRegrasContagem_Validade_To;
         AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV16TFRegrasContagem_Responsavel;
         AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV17TFRegrasContagem_Responsavel_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                              AV60WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                              AV61WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                              AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                              AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                              AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                              AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                              AV66WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                              AV67WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                              AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                              AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                              AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                              AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                              AV72WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                              AV73WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                              AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                              AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                              AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                              AV76WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                              AV78WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                              AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                              AV80WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                              AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                              AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                              AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV60WWRegrasContagemDS_3_Regrascontagem_regra1 = StringUtil.Concat( StringUtil.RTrim( AV60WWRegrasContagemDS_3_Regrascontagem_regra1), "%", "");
         lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1), 50, "%");
         lV66WWRegrasContagemDS_9_Regrascontagem_regra2 = StringUtil.Concat( StringUtil.RTrim( AV66WWRegrasContagemDS_9_Regrascontagem_regra2), "%", "");
         lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2), 50, "%");
         lV72WWRegrasContagemDS_15_Regrascontagem_regra3 = StringUtil.Concat( StringUtil.RTrim( AV72WWRegrasContagemDS_15_Regrascontagem_regra3), "%", "");
         lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3), 50, "%");
         lV76WWRegrasContagemDS_19_Tfregrascontagem_regra = StringUtil.Concat( StringUtil.RTrim( AV76WWRegrasContagemDS_19_Tfregrascontagem_regra), "%", "");
         lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = StringUtil.PadR( StringUtil.RTrim( AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel), 50, "%");
         /* Using cursor P00OV3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV60WWRegrasContagemDS_3_Regrascontagem_regra1, AV61WWRegrasContagemDS_4_Regrascontagem_data1, AV62WWRegrasContagemDS_5_Regrascontagem_data_to1, lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1, lV66WWRegrasContagemDS_9_Regrascontagem_regra2, AV67WWRegrasContagemDS_10_Regrascontagem_data2, AV68WWRegrasContagemDS_11_Regrascontagem_data_to2, lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2, lV72WWRegrasContagemDS_15_Regrascontagem_regra3, AV73WWRegrasContagemDS_16_Regrascontagem_data3, AV74WWRegrasContagemDS_17_Regrascontagem_data_to3, lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3, lV76WWRegrasContagemDS_19_Tfregrascontagem_regra, AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel, AV78WWRegrasContagemDS_21_Tfregrascontagem_data, AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to, AV80WWRegrasContagemDS_23_Tfregrascontagem_validade, AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to, lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel, AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOV3 = false;
            A865RegrasContagem_AreaTrabalhoCod = P00OV3_A865RegrasContagem_AreaTrabalhoCod[0];
            A863RegrasContagem_Responsavel = P00OV3_A863RegrasContagem_Responsavel[0];
            A862RegrasContagem_Validade = P00OV3_A862RegrasContagem_Validade[0];
            n862RegrasContagem_Validade = P00OV3_n862RegrasContagem_Validade[0];
            A861RegrasContagem_Data = P00OV3_A861RegrasContagem_Data[0];
            A860RegrasContagem_Regra = P00OV3_A860RegrasContagem_Regra[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OV3_A863RegrasContagem_Responsavel[0], A863RegrasContagem_Responsavel) == 0 ) )
            {
               BRKOV3 = false;
               A860RegrasContagem_Regra = P00OV3_A860RegrasContagem_Regra[0];
               AV30count = (long)(AV30count+1);
               BRKOV3 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A863RegrasContagem_Responsavel)) )
            {
               AV22Option = A863RegrasContagem_Responsavel;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOV3 )
            {
               BRKOV3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFRegrasContagem_Regra = "";
         AV11TFRegrasContagem_Regra_Sel = "";
         AV12TFRegrasContagem_Data = DateTime.MinValue;
         AV13TFRegrasContagem_Data_To = DateTime.MinValue;
         AV14TFRegrasContagem_Validade = DateTime.MinValue;
         AV15TFRegrasContagem_Validade_To = DateTime.MinValue;
         AV16TFRegrasContagem_Responsavel = "";
         AV17TFRegrasContagem_Responsavel_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV38RegrasContagem_Regra1 = "";
         AV39RegrasContagem_Data1 = DateTime.MinValue;
         AV40RegrasContagem_Data_To1 = DateTime.MinValue;
         AV41RegrasContagem_Responsavel1 = "";
         AV43DynamicFiltersSelector2 = "";
         AV44RegrasContagem_Regra2 = "";
         AV45RegrasContagem_Data2 = DateTime.MinValue;
         AV46RegrasContagem_Data_To2 = DateTime.MinValue;
         AV47RegrasContagem_Responsavel2 = "";
         AV49DynamicFiltersSelector3 = "";
         AV50RegrasContagem_Regra3 = "";
         AV51RegrasContagem_Data3 = DateTime.MinValue;
         AV52RegrasContagem_Data_To3 = DateTime.MinValue;
         AV53RegrasContagem_Responsavel3 = "";
         AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 = "";
         AV60WWRegrasContagemDS_3_Regrascontagem_regra1 = "";
         AV61WWRegrasContagemDS_4_Regrascontagem_data1 = DateTime.MinValue;
         AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 = DateTime.MinValue;
         AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = "";
         AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 = "";
         AV66WWRegrasContagemDS_9_Regrascontagem_regra2 = "";
         AV67WWRegrasContagemDS_10_Regrascontagem_data2 = DateTime.MinValue;
         AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 = DateTime.MinValue;
         AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = "";
         AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 = "";
         AV72WWRegrasContagemDS_15_Regrascontagem_regra3 = "";
         AV73WWRegrasContagemDS_16_Regrascontagem_data3 = DateTime.MinValue;
         AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 = DateTime.MinValue;
         AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = "";
         AV76WWRegrasContagemDS_19_Tfregrascontagem_regra = "";
         AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = "";
         AV78WWRegrasContagemDS_21_Tfregrascontagem_data = DateTime.MinValue;
         AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to = DateTime.MinValue;
         AV80WWRegrasContagemDS_23_Tfregrascontagem_validade = DateTime.MinValue;
         AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to = DateTime.MinValue;
         AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = "";
         AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = "";
         scmdbuf = "";
         lV60WWRegrasContagemDS_3_Regrascontagem_regra1 = "";
         lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 = "";
         lV66WWRegrasContagemDS_9_Regrascontagem_regra2 = "";
         lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 = "";
         lV72WWRegrasContagemDS_15_Regrascontagem_regra3 = "";
         lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 = "";
         lV76WWRegrasContagemDS_19_Tfregrascontagem_regra = "";
         lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel = "";
         A860RegrasContagem_Regra = "";
         A861RegrasContagem_Data = DateTime.MinValue;
         A863RegrasContagem_Responsavel = "";
         A862RegrasContagem_Validade = DateTime.MinValue;
         P00OV2_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         P00OV2_n862RegrasContagem_Validade = new bool[] {false} ;
         P00OV2_A863RegrasContagem_Responsavel = new String[] {""} ;
         P00OV2_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         P00OV2_A860RegrasContagem_Regra = new String[] {""} ;
         P00OV2_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         AV22Option = "";
         P00OV3_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         P00OV3_A863RegrasContagem_Responsavel = new String[] {""} ;
         P00OV3_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         P00OV3_n862RegrasContagem_Validade = new bool[] {false} ;
         P00OV3_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         P00OV3_A860RegrasContagem_Regra = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwregrascontagemfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OV2_A862RegrasContagem_Validade, P00OV2_n862RegrasContagem_Validade, P00OV2_A863RegrasContagem_Responsavel, P00OV2_A861RegrasContagem_Data, P00OV2_A860RegrasContagem_Regra, P00OV2_A865RegrasContagem_AreaTrabalhoCod
               }
               , new Object[] {
               P00OV3_A865RegrasContagem_AreaTrabalhoCod, P00OV3_A863RegrasContagem_Responsavel, P00OV3_A862RegrasContagem_Validade, P00OV3_n862RegrasContagem_Validade, P00OV3_A861RegrasContagem_Data, P00OV3_A860RegrasContagem_Regra
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV56GXV1 ;
      private int AV36RegrasContagem_AreaTrabalhoCod ;
      private int AV58WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A865RegrasContagem_AreaTrabalhoCod ;
      private long AV30count ;
      private String AV16TFRegrasContagem_Responsavel ;
      private String AV17TFRegrasContagem_Responsavel_Sel ;
      private String AV41RegrasContagem_Responsavel1 ;
      private String AV47RegrasContagem_Responsavel2 ;
      private String AV53RegrasContagem_Responsavel3 ;
      private String AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ;
      private String AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ;
      private String AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ;
      private String AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ;
      private String AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ;
      private String scmdbuf ;
      private String lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ;
      private String lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ;
      private String lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ;
      private String lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ;
      private String A863RegrasContagem_Responsavel ;
      private DateTime AV12TFRegrasContagem_Data ;
      private DateTime AV13TFRegrasContagem_Data_To ;
      private DateTime AV14TFRegrasContagem_Validade ;
      private DateTime AV15TFRegrasContagem_Validade_To ;
      private DateTime AV39RegrasContagem_Data1 ;
      private DateTime AV40RegrasContagem_Data_To1 ;
      private DateTime AV45RegrasContagem_Data2 ;
      private DateTime AV46RegrasContagem_Data_To2 ;
      private DateTime AV51RegrasContagem_Data3 ;
      private DateTime AV52RegrasContagem_Data_To3 ;
      private DateTime AV61WWRegrasContagemDS_4_Regrascontagem_data1 ;
      private DateTime AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 ;
      private DateTime AV67WWRegrasContagemDS_10_Regrascontagem_data2 ;
      private DateTime AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 ;
      private DateTime AV73WWRegrasContagemDS_16_Regrascontagem_data3 ;
      private DateTime AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 ;
      private DateTime AV78WWRegrasContagemDS_21_Tfregrascontagem_data ;
      private DateTime AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to ;
      private DateTime AV80WWRegrasContagemDS_23_Tfregrascontagem_validade ;
      private DateTime AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to ;
      private DateTime A861RegrasContagem_Data ;
      private DateTime A862RegrasContagem_Validade ;
      private bool returnInSub ;
      private bool AV42DynamicFiltersEnabled2 ;
      private bool AV48DynamicFiltersEnabled3 ;
      private bool AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 ;
      private bool AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 ;
      private bool n862RegrasContagem_Validade ;
      private bool BRKOV3 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFRegrasContagem_Regra ;
      private String AV11TFRegrasContagem_Regra_Sel ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV38RegrasContagem_Regra1 ;
      private String AV43DynamicFiltersSelector2 ;
      private String AV44RegrasContagem_Regra2 ;
      private String AV49DynamicFiltersSelector3 ;
      private String AV50RegrasContagem_Regra3 ;
      private String AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 ;
      private String AV60WWRegrasContagemDS_3_Regrascontagem_regra1 ;
      private String AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 ;
      private String AV66WWRegrasContagemDS_9_Regrascontagem_regra2 ;
      private String AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 ;
      private String AV72WWRegrasContagemDS_15_Regrascontagem_regra3 ;
      private String AV76WWRegrasContagemDS_19_Tfregrascontagem_regra ;
      private String AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ;
      private String lV60WWRegrasContagemDS_3_Regrascontagem_regra1 ;
      private String lV66WWRegrasContagemDS_9_Regrascontagem_regra2 ;
      private String lV72WWRegrasContagemDS_15_Regrascontagem_regra3 ;
      private String lV76WWRegrasContagemDS_19_Tfregrascontagem_regra ;
      private String A860RegrasContagem_Regra ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P00OV2_A862RegrasContagem_Validade ;
      private bool[] P00OV2_n862RegrasContagem_Validade ;
      private String[] P00OV2_A863RegrasContagem_Responsavel ;
      private DateTime[] P00OV2_A861RegrasContagem_Data ;
      private String[] P00OV2_A860RegrasContagem_Regra ;
      private int[] P00OV2_A865RegrasContagem_AreaTrabalhoCod ;
      private int[] P00OV3_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] P00OV3_A863RegrasContagem_Responsavel ;
      private DateTime[] P00OV3_A862RegrasContagem_Validade ;
      private bool[] P00OV3_n862RegrasContagem_Validade ;
      private DateTime[] P00OV3_A861RegrasContagem_Data ;
      private String[] P00OV3_A860RegrasContagem_Regra ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwregrascontagemfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OV2( IGxContext context ,
                                             String AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                             String AV60WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                             DateTime AV61WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                             DateTime AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                             String AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                             bool AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                             String AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                             String AV66WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                             DateTime AV67WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                             DateTime AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                             String AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                             bool AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                             String AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                             String AV72WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                             DateTime AV73WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                             DateTime AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                             String AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                             String AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                             String AV76WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                             DateTime AV78WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                             DateTime AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                             DateTime AV80WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                             DateTime AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                             String AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                             String AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [RegrasContagem_Validade], NULL AS [RegrasContagem_Responsavel], NULL AS [RegrasContagem_Data], [RegrasContagem_Regra], NULL AS [RegrasContagem_AreaTrabalhoCod] FROM ( SELECT TOP(100) PERCENT [RegrasContagem_Validade], [RegrasContagem_Responsavel], [RegrasContagem_Data], [RegrasContagem_Regra], [RegrasContagem_AreaTrabalhoCod] FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWRegrasContagemDS_3_Regrascontagem_regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV60WWRegrasContagemDS_3_Regrascontagem_regra1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWRegrasContagemDS_4_Regrascontagem_data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV61WWRegrasContagemDS_4_Regrascontagem_data1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV62WWRegrasContagemDS_5_Regrascontagem_data_to1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV62WWRegrasContagemDS_5_Regrascontagem_data_to1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWRegrasContagemDS_9_Regrascontagem_regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV66WWRegrasContagemDS_9_Regrascontagem_regra2 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWRegrasContagemDS_10_Regrascontagem_data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV67WWRegrasContagemDS_10_Regrascontagem_data2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV68WWRegrasContagemDS_11_Regrascontagem_data_to2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV68WWRegrasContagemDS_11_Regrascontagem_data_to2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 + '%')";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWRegrasContagemDS_15_Regrascontagem_regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV72WWRegrasContagemDS_15_Regrascontagem_regra3 + '%')";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWRegrasContagemDS_16_Regrascontagem_data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV73WWRegrasContagemDS_16_Regrascontagem_data3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV74WWRegrasContagemDS_17_Regrascontagem_data_to3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV74WWRegrasContagemDS_17_Regrascontagem_data_to3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 + '%')";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWRegrasContagemDS_19_Tfregrascontagem_regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV76WWRegrasContagemDS_19_Tfregrascontagem_regra)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWRegrasContagemDS_21_Tfregrascontagem_data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV78WWRegrasContagemDS_21_Tfregrascontagem_data)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWRegrasContagemDS_23_Tfregrascontagem_validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV80WWRegrasContagemDS_23_Tfregrascontagem_validade)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [RegrasContagem_Regra]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OV3( IGxContext context ,
                                             String AV59WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                             String AV60WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                             DateTime AV61WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                             DateTime AV62WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                             String AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                             bool AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                             String AV65WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                             String AV66WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                             DateTime AV67WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                             DateTime AV68WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                             String AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                             bool AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                             String AV71WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                             String AV72WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                             DateTime AV73WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                             DateTime AV74WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                             String AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                             String AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                             String AV76WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                             DateTime AV78WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                             DateTime AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                             DateTime AV80WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                             DateTime AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                             String AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                             String AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [21] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [RegrasContagem_AreaTrabalhoCod], [RegrasContagem_Responsavel], [RegrasContagem_Validade], [RegrasContagem_Data], [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWRegrasContagemDS_3_Regrascontagem_regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV60WWRegrasContagemDS_3_Regrascontagem_regra1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWRegrasContagemDS_4_Regrascontagem_data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV61WWRegrasContagemDS_4_Regrascontagem_data1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV62WWRegrasContagemDS_5_Regrascontagem_data_to1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV62WWRegrasContagemDS_5_Regrascontagem_data_to1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWRegrasContagemDS_6_Regrascontagem_responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWRegrasContagemDS_9_Regrascontagem_regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV66WWRegrasContagemDS_9_Regrascontagem_regra2 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67WWRegrasContagemDS_10_Regrascontagem_data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV67WWRegrasContagemDS_10_Regrascontagem_data2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV68WWRegrasContagemDS_11_Regrascontagem_data_to2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV68WWRegrasContagemDS_11_Regrascontagem_data_to2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV64WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWRegrasContagemDS_12_Regrascontagem_responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2 + '%')";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWRegrasContagemDS_15_Regrascontagem_regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV72WWRegrasContagemDS_15_Regrascontagem_regra3 + '%')";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWRegrasContagemDS_16_Regrascontagem_data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV73WWRegrasContagemDS_16_Regrascontagem_data3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV74WWRegrasContagemDS_17_Regrascontagem_data_to3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV74WWRegrasContagemDS_17_Regrascontagem_data_to3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV70WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWRegrasContagemDS_18_Regrascontagem_responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3 + '%')";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWRegrasContagemDS_19_Tfregrascontagem_regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV76WWRegrasContagemDS_19_Tfregrascontagem_regra)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV78WWRegrasContagemDS_21_Tfregrascontagem_data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV78WWRegrasContagemDS_21_Tfregrascontagem_data)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWRegrasContagemDS_23_Tfregrascontagem_validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV80WWRegrasContagemDS_23_Tfregrascontagem_validade)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [RegrasContagem_Responsavel]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OV2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
               case 1 :
                     return conditional_P00OV3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OV2 ;
          prmP00OV2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWRegrasContagemDS_3_Regrascontagem_regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV61WWRegrasContagemDS_4_Regrascontagem_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62WWRegrasContagemDS_5_Regrascontagem_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWRegrasContagemDS_9_Regrascontagem_regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV67WWRegrasContagemDS_10_Regrascontagem_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWRegrasContagemDS_11_Regrascontagem_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWRegrasContagemDS_15_Regrascontagem_regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV73WWRegrasContagemDS_16_Regrascontagem_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWRegrasContagemDS_17_Regrascontagem_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV76WWRegrasContagemDS_19_Tfregrascontagem_regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV78WWRegrasContagemDS_21_Tfregrascontagem_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWRegrasContagemDS_23_Tfregrascontagem_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00OV3 ;
          prmP00OV3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWRegrasContagemDS_3_Regrascontagem_regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV61WWRegrasContagemDS_4_Regrascontagem_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62WWRegrasContagemDS_5_Regrascontagem_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV63WWRegrasContagemDS_6_Regrascontagem_responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWRegrasContagemDS_9_Regrascontagem_regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV67WWRegrasContagemDS_10_Regrascontagem_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWRegrasContagemDS_11_Regrascontagem_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV69WWRegrasContagemDS_12_Regrascontagem_responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWRegrasContagemDS_15_Regrascontagem_regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV73WWRegrasContagemDS_16_Regrascontagem_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWRegrasContagemDS_17_Regrascontagem_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV75WWRegrasContagemDS_18_Regrascontagem_responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV76WWRegrasContagemDS_19_Tfregrascontagem_regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV77WWRegrasContagemDS_20_Tfregrascontagem_regra_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV78WWRegrasContagemDS_21_Tfregrascontagem_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWRegrasContagemDS_22_Tfregrascontagem_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWRegrasContagemDS_23_Tfregrascontagem_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWRegrasContagemDS_24_Tfregrascontagem_validade_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV82WWRegrasContagemDS_25_Tfregrascontagem_responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OV2,100,0,false,false )
             ,new CursorDef("P00OV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OV3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwregrascontagemfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwregrascontagemfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwregrascontagemfilterdata") )
          {
             return  ;
          }
          getwwregrascontagemfilterdata worker = new getwwregrascontagemfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
