/*
               File: PRC_GetUltimaArea
        Description: Get Ultima Area do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:21.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getultimaarea : GXProcedure
   {
      public prc_getultimaarea( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getultimaarea( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo ,
                           out int aP1_UltimaArea )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8UltimaArea = 0 ;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_UltimaArea=this.AV8UltimaArea;
      }

      public int executeUdp( ref int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8UltimaArea = 0 ;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_UltimaArea=this.AV8UltimaArea;
         return AV8UltimaArea ;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo ,
                                 out int aP1_UltimaArea )
      {
         prc_getultimaarea objprc_getultimaarea;
         objprc_getultimaarea = new prc_getultimaarea();
         objprc_getultimaarea.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_getultimaarea.AV8UltimaArea = 0 ;
         objprc_getultimaarea.context.SetSubmitInitialConfig(context);
         objprc_getultimaarea.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getultimaarea);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_UltimaArea=this.AV8UltimaArea;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getultimaarea)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00W62 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2016Usuario_UltimaArea = P00W62_A2016Usuario_UltimaArea[0];
            n2016Usuario_UltimaArea = P00W62_n2016Usuario_UltimaArea[0];
            AV8UltimaArea = A2016Usuario_UltimaArea;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00W62_A1Usuario_Codigo = new int[1] ;
         P00W62_A2016Usuario_UltimaArea = new int[1] ;
         P00W62_n2016Usuario_UltimaArea = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getultimaarea__default(),
            new Object[][] {
                new Object[] {
               P00W62_A1Usuario_Codigo, P00W62_A2016Usuario_UltimaArea, P00W62_n2016Usuario_UltimaArea
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private int AV8UltimaArea ;
      private int A2016Usuario_UltimaArea ;
      private String scmdbuf ;
      private bool n2016Usuario_UltimaArea ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00W62_A1Usuario_Codigo ;
      private int[] P00W62_A2016Usuario_UltimaArea ;
      private bool[] P00W62_n2016Usuario_UltimaArea ;
      private int aP1_UltimaArea ;
   }

   public class prc_getultimaarea__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W62 ;
          prmP00W62 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W62", "SELECT TOP 1 [Usuario_Codigo], [Usuario_UltimaArea] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W62,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
