/*
               File: PRC_DLTContratoServicos
        Description: DLT Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:45.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltcontratoservicos : GXProcedure
   {
      public prc_dltcontratoservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltcontratoservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
      }

      public int executeUdp( )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         return A160ContratoServicos_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo )
      {
         prc_dltcontratoservicos objprc_dltcontratoservicos;
         objprc_dltcontratoservicos = new prc_dltcontratoservicos();
         objprc_dltcontratoservicos.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_dltcontratoservicos.context.SetSubmitInitialConfig(context);
         objprc_dltcontratoservicos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltcontratoservicos);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltcontratoservicos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X72 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* Using cursor P00X73 */
            pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A903ContratoServicosPrazo_CntSrvCod = P00X73_A903ContratoServicosPrazo_CntSrvCod[0];
               /* Optimized DELETE. */
               /* Using cursor P00X74 */
               pr_default.execute(2, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A160ContratoServicos_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoContratoServicosPrazoRegra") ;
               /* End optimized DELETE. */
               /* Optimized DELETE. */
               /* Using cursor P00X75 */
               pr_default.execute(3, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A160ContratoServicos_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoPrazoPlnj") ;
               /* End optimized DELETE. */
               /* Using cursor P00X76 */
               pr_default.execute(4, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazo") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Optimized DELETE. */
            /* Using cursor P00X77 */
            pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosVnc") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X78 */
            pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosTelas") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X79 */
            pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrioridade") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X710 */
            pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X711 */
            pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosCusto") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X712 */
            pr_default.execute(10, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00X713 */
            pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
            /* End optimized DELETE. */
            /* Using cursor P00X714 */
            pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DLTContratoServicos");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X72_A160ContratoServicos_Codigo = new int[1] ;
         P00X73_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltcontratoservicos__default(),
            new Object[][] {
                new Object[] {
               P00X72_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00X73_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X72_A160ContratoServicos_Codigo ;
      private int[] P00X73_A903ContratoServicosPrazo_CntSrvCod ;
   }

   public class prc_dltcontratoservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X72 ;
          prmP00X72 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X73 ;
          prmP00X73 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X74 ;
          prmP00X74 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X75 ;
          prmP00X75 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X76 ;
          prmP00X76 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X77 ;
          prmP00X77 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X78 ;
          prmP00X78 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X79 ;
          prmP00X79 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X710 ;
          prmP00X710 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X711 ;
          prmP00X711 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X712 ;
          prmP00X712 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X713 ;
          prmP00X713 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X714 ;
          prmP00X714 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X72", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X72,1,0,true,true )
             ,new CursorDef("P00X73", "SELECT [ContratoServicosPrazo_CntSrvCod] FROM [ContratoServicosPrazo] WITH (UPDLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X73,1,0,true,true )
             ,new CursorDef("P00X74", "DELETE FROM [ContratoServicosPrazoContratoServicosPrazoRegra]  WHERE ([ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod and [ContratoServicosPrazoRegra_Sequencial] > 0) AND (@ContratoServicos_Codigo = @ContratoServicos_Codigo)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X74)
             ,new CursorDef("P00X75", "DELETE FROM [ContratoServicosPrazoPrazoPlnj]  WHERE ([ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod and [ContratoServicosPrazoPlnj_Sequencial] > 0) AND (@ContratoServicos_Codigo = @ContratoServicos_Codigo)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X75)
             ,new CursorDef("P00X76", "DELETE FROM [ContratoServicosPrazo]  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X76)
             ,new CursorDef("P00X77", "DELETE FROM [ContratoServicosVnc]  WHERE [ContratoSrvVnc_CntSrvCod] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X77)
             ,new CursorDef("P00X78", "DELETE FROM [ContratoServicosTelas]  WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X78)
             ,new CursorDef("P00X79", "DELETE FROM [ContratoServicosPrioridade]  WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X79)
             ,new CursorDef("P00X710", "DELETE FROM [ContratoServicosIndicador]  WHERE [ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X710)
             ,new CursorDef("P00X711", "DELETE FROM [ContratoServicosCusto]  WHERE [ContratoServicosCusto_CntSrvCod] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X711)
             ,new CursorDef("P00X712", "DELETE FROM [ContratoServicosArtefatos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X712)
             ,new CursorDef("P00X713", "DELETE FROM [ContratoServicosRmn]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosRmn_Sequencial] > 0", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X713)
             ,new CursorDef("P00X714", "DELETE FROM [ContratoServicos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00X714)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
