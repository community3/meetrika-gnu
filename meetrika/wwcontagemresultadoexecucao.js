/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:19:34.73
*/
gx.evt.autoSkip = false;
gx.define('wwcontagemresultadoexecucao', false, function () {
   this.ServerClass =  "wwcontagemresultadoexecucao" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A457ContagemResultado_Demanda=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDA") ;
      this.A493ContagemResultado_DemandaFM=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDAFM") ;
      this.AV138Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV39DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV38DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
   };
   this.Validv_Contagemresultadoexecucao_inicio1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV16ContagemResultadoExecucao_Inicio1)==0) || new gx.date.gxdate( this.AV16ContagemResultadoExecucao_Inicio1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_inicio_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV17ContagemResultadoExecucao_Inicio_To1)==0) || new gx.date.gxdate( this.AV17ContagemResultadoExecucao_Inicio_To1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV18ContagemResultadoExecucao_Fim1)==0) || new gx.date.gxdate( this.AV18ContagemResultadoExecucao_Fim1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV19ContagemResultadoExecucao_Fim_To1)==0) || new gx.date.gxdate( this.AV19ContagemResultadoExecucao_Fim_To1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV20ContagemResultadoExecucao_Prevista1)==0) || new gx.date.gxdate( this.AV20ContagemResultadoExecucao_Prevista1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV21ContagemResultadoExecucao_Prevista_To1)==0) || new gx.date.gxdate( this.AV21ContagemResultadoExecucao_Prevista_To1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_inicio2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV24ContagemResultadoExecucao_Inicio2)==0) || new gx.date.gxdate( this.AV24ContagemResultadoExecucao_Inicio2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_inicio_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV25ContagemResultadoExecucao_Inicio_To2)==0) || new gx.date.gxdate( this.AV25ContagemResultadoExecucao_Inicio_To2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV26ContagemResultadoExecucao_Fim2)==0) || new gx.date.gxdate( this.AV26ContagemResultadoExecucao_Fim2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV27ContagemResultadoExecucao_Fim_To2)==0) || new gx.date.gxdate( this.AV27ContagemResultadoExecucao_Fim_To2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV28ContagemResultadoExecucao_Prevista2)==0) || new gx.date.gxdate( this.AV28ContagemResultadoExecucao_Prevista2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV29ContagemResultadoExecucao_Prevista_To2)==0) || new gx.date.gxdate( this.AV29ContagemResultadoExecucao_Prevista_To2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_inicio3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV32ContagemResultadoExecucao_Inicio3)==0) || new gx.date.gxdate( this.AV32ContagemResultadoExecucao_Inicio3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_inicio_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV33ContagemResultadoExecucao_Inicio_To3)==0) || new gx.date.gxdate( this.AV33ContagemResultadoExecucao_Inicio_To3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Inicio_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV34ContagemResultadoExecucao_Fim3)==0) || new gx.date.gxdate( this.AV34ContagemResultadoExecucao_Fim3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_fim_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_FIM_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV35ContagemResultadoExecucao_Fim_To3)==0) || new gx.date.gxdate( this.AV35ContagemResultadoExecucao_Fim_To3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Fim_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV36ContagemResultadoExecucao_Prevista3)==0) || new gx.date.gxdate( this.AV36ContagemResultadoExecucao_Prevista3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultadoexecucao_prevista_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV37ContagemResultadoExecucao_Prevista_To3)==0) || new gx.date.gxdate( this.AV37ContagemResultadoExecucao_Prevista_To3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Contagem Resultado Execucao_Prevista_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultadoexecucao_oscod=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(152) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADOEXECUCAO_OSCOD", gx.fn.currentGridRowImpl(152));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_inicio=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_INICIO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV53TFContagemResultadoExecucao_Inicio)==0) || new gx.date.gxdate( this.AV53TFContagemResultadoExecucao_Inicio ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Inicio fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_inicio_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV54TFContagemResultadoExecucao_Inicio_To)==0) || new gx.date.gxdate( this.AV54TFContagemResultadoExecucao_Inicio_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Inicio_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_inicioauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV55DDO_ContagemResultadoExecucao_InicioAuxDate)==0) || new gx.date.gxdate( this.AV55DDO_ContagemResultadoExecucao_InicioAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Inicio Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_inicioauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo)==0) || new gx.date.gxdate( this.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Inicio Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_prevista=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV59TFContagemResultadoExecucao_Prevista)==0) || new gx.date.gxdate( this.AV59TFContagemResultadoExecucao_Prevista ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Prevista fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_prevista_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV60TFContagemResultadoExecucao_Prevista_To)==0) || new gx.date.gxdate( this.AV60TFContagemResultadoExecucao_Prevista_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Prevista_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_previstaauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate)==0) || new gx.date.gxdate( this.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Prevista Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_previstaauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo)==0) || new gx.date.gxdate( this.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Prevista Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_fim=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_FIM");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV69TFContagemResultadoExecucao_Fim)==0) || new gx.date.gxdate( this.AV69TFContagemResultadoExecucao_Fim ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Fim fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadoexecucao_fim_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV70TFContagemResultadoExecucao_Fim_To)==0) || new gx.date.gxdate( this.AV70TFContagemResultadoExecucao_Fim_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Execucao_Fim_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_fimauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV71DDO_ContagemResultadoExecucao_FimAuxDate)==0) || new gx.date.gxdate( this.AV71DDO_ContagemResultadoExecucao_FimAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Fim Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadoexecucao_fimauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo)==0) || new gx.date.gxdate( this.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Execucao_Fim Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD1","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADOEXECUCAO_INICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADOEXECUCAO_FIM" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADOEXECUCAO_PREVISTA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADO_OSFSOSFM" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADO_CONTRATADACOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTAGEMRESULTADOEXECUCAO_OSCOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD2","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible", false );
      if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADOEXECUCAO_INICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible", true );
      }
      else if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADOEXECUCAO_FIM" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible", true );
      }
      else if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADOEXECUCAO_PREVISTA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible", true );
      }
      else if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADO_OSFSOSFM" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible", true );
      }
      else if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADO_CONTRATADACOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD2","Visible", true );
      }
      else if ( this.AV23DynamicFiltersSelector2 == "CONTAGEMRESULTADOEXECUCAO_OSCOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD3","Visible", false );
      gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible", false );
      if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADOEXECUCAO_INICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible", true );
      }
      else if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADOEXECUCAO_FIM" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible", true );
      }
      else if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADOEXECUCAO_PREVISTA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible", true );
      }
      else if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADO_OSFSOSFM" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible", true );
      }
      else if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADO_CONTRATADACOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADO_CONTRATADACOD3","Visible", true );
      }
      else if ( this.AV31DynamicFiltersSelector3 == "CONTAGEMRESULTADOEXECUCAO_OSCOD" )
      {
         gx.fn.setCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible", true );
      }
   };
   this.s182_client=function()
   {
      this.AV22DynamicFiltersEnabled2 =  false  ;
      this.AV23DynamicFiltersSelector2 =  "CONTAGEMRESULTADOEXECUCAO_INICIO"  ;
      this.AV24ContagemResultadoExecucao_Inicio2 =  ''  ;
      this.AV25ContagemResultadoExecucao_Inicio_To2 =  ''  ;
      this.s122_client();
      this.AV30DynamicFiltersEnabled3 =  false  ;
      this.AV31DynamicFiltersSelector3 =  "CONTAGEMRESULTADOEXECUCAO_INICIO"  ;
      this.AV32ContagemResultadoExecucao_Inicio3 =  ''  ;
      this.AV33ContagemResultadoExecucao_Inicio_To3 =  ''  ;
      this.s132_client();
   };
   this.e32ki1_client=function()
   {
      this.clearMessages();
      this.s212_client();
      this.refreshOutputs([]);
   };
   this.s212_client=function()
   {
      this.refreshGrid('Grid') ;
   };
   this.e11ki2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_OSFSOSFM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17ki2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e18ki2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e19ki2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e20ki2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e21ki2_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e22ki2_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e31ki2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e23ki2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e24ki2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e25ki2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e26ki2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e27ki2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e33ki2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,21,23,25,27,30,32,34,36,39,41,43,44,47,49,51,52,55,57,59,60,61,62,64,65,68,70,72,74,77,79,81,82,85,87,89,90,93,95,97,98,99,100,102,103,106,108,110,112,115,117,119,120,123,125,127,128,131,133,135,136,137,138,140,141,146,149,153,154,155,156,157,158,159,160,161,162,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,191,193,195,197,199,201];
   this.GXLastCtrlId =201;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",152,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwcontagemresultadoexecucao",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",153,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",154,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(1405,155,"CONTAGEMRESULTADOEXECUCAO_CODIGO","Codigo","","ContagemResultadoExecucao_Codigo","int",0,"px",6,6,"right",null,[],1405,"ContagemResultadoExecucao_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1404,156,"CONTAGEMRESULTADOEXECUCAO_OSCOD","OS / OS Ref.","","ContagemResultadoExecucao_OSCod","int",0,"px",6,6,"right",null,[],1404,"ContagemResultadoExecucao_OSCod",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(501,157,"CONTAGEMRESULTADO_OSFSOSFM","","","ContagemResultado_OsFsOsFm","svchar",0,"px",30,30,"left",null,[],501,"ContagemResultado_OsFsOsFm",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1406,158,"CONTAGEMRESULTADOEXECUCAO_INICIO","","","ContagemResultadoExecucao_Inicio","dtime",0,"px",14,14,"right",null,[],1406,"ContagemResultadoExecucao_Inicio",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1408,159,"CONTAGEMRESULTADOEXECUCAO_PREVISTA","","","ContagemResultadoExecucao_Prevista","dtime",0,"px",14,14,"right",null,[],1408,"ContagemResultadoExecucao_Prevista",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1411,160,"CONTAGEMRESULTADOEXECUCAO_PRAZODIAS","","","ContagemResultadoExecucao_PrazoDias","int",0,"px",4,4,"right",null,[],1411,"ContagemResultadoExecucao_PrazoDias",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1407,161,"CONTAGEMRESULTADOEXECUCAO_FIM","","","ContagemResultadoExecucao_Fim","dtime",0,"px",14,14,"right",null,[],1407,"ContagemResultadoExecucao_Fim",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1410,162,"CONTAGEMRESULTADOEXECUCAO_DIAS","","","ContagemResultadoExecucao_Dias","int",0,"px",4,4,"right",null,[],1410,"ContagemResultadoExecucao_Dias",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 166, 25, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer = gx.uc.getNew(this, 190, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_OSFSOSFMContainer", "Ddo_contagemresultado_osfsosfm");
   var DDO_CONTAGEMRESULTADO_OSFSOSFMContainer = this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer;
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("DataListProc", "Datalistproc", "GetWWContagemResultadoExecucaoFilterData", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.addV2CFunction('AV48ContagemResultado_OsFsOsFmTitleFilterData', "vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV48ContagemResultado_OsFsOsFmTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA",UC.ParentObject.AV48ContagemResultado_OsFsOsFmTitleFilterData); });
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.addEventHandler("OnOptionClicked", this.e12ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_OSFSOSFMContainer);
   this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer = gx.uc.getNew(this, 192, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer", "Ddo_contagemresultadoexecucao_inicio");
   var DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer = this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer;
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.addV2CFunction('AV52ContagemResultadoExecucao_InicioTitleFilterData', "vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV52ContagemResultadoExecucao_InicioTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA",UC.ParentObject.AV52ContagemResultadoExecucao_InicioTitleFilterData); });
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.addEventHandler("OnOptionClicked", this.e13ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer);
   this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer = gx.uc.getNew(this, 194, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer", "Ddo_contagemresultadoexecucao_prevista");
   var DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer = this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer;
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.addV2CFunction('AV58ContagemResultadoExecucao_PrevistaTitleFilterData', "vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV58ContagemResultadoExecucao_PrevistaTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA",UC.ParentObject.AV58ContagemResultadoExecucao_PrevistaTitleFilterData); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.addEventHandler("OnOptionClicked", this.e14ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer);
   this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer = gx.uc.getNew(this, 196, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer", "Ddo_contagemresultadoexecucao_prazodias");
   var DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer = this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer;
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.addV2CFunction('AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData', "vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.addC2VFunction(function(UC) { UC.ParentObject.AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA",UC.ParentObject.AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.addEventHandler("OnOptionClicked", this.e15ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer);
   this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer = gx.uc.getNew(this, 198, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer", "Ddo_contagemresultadoexecucao_fim");
   var DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer = this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer;
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.addV2CFunction('AV68ContagemResultadoExecucao_FimTitleFilterData', "vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV68ContagemResultadoExecucao_FimTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA",UC.ParentObject.AV68ContagemResultadoExecucao_FimTitleFilterData); });
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.addEventHandler("OnOptionClicked", this.e16ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer);
   this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer = gx.uc.getNew(this, 200, 25, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer", "Ddo_contagemresultadoexecucao_dias");
   var DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer = this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer;
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.addV2CFunction('AV78DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.addC2VFunction(function(UC) { UC.ParentObject.AV78DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV78DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.addV2CFunction('AV74ContagemResultadoExecucao_DiasTitleFilterData', "vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.addC2VFunction(function(UC) { UC.ParentObject.AV74ContagemResultadoExecucao_DiasTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA",UC.ParentObject.AV74ContagemResultadoExecucao_DiasTitleFilterData); });
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.addEventHandler("OnOptionClicked", this.e17ki2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 165, 25, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV80GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV80GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV80GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV81GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV81GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV81GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11ki2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"CONTAGEMRESULTADOEXECUCAOTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[21]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[23]={fld:"FILTERTEXTCONTRATADA_CODIGO", format:0,grid:0};
   GXValidFnc[25]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADA_CODIGO",gxz:"ZV88Contratada_Codigo",gxold:"OV88Contratada_Codigo",gxvar:"AV88Contratada_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV88Contratada_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV88Contratada_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATADA_CODIGO",gx.O.AV88Contratada_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV88Contratada_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADA_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[27]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[30]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[32]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[34]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[36]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1",grid:0};
   GXValidFnc[39]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO1",gxz:"ZV16ContagemResultadoExecucao_Inicio1",gxold:"OV16ContagemResultadoExecucao_Inicio1",gxvar:"AV16ContagemResultadoExecucao_Inicio1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[39],ip:[39],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV16ContagemResultadoExecucao_Inicio1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16ContagemResultadoExecucao_Inicio1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO1",gx.O.AV16ContagemResultadoExecucao_Inicio1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16ContagemResultadoExecucao_Inicio1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO1")},nac:gx.falseFn};
   GXValidFnc[41]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[43]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1",gxz:"ZV17ContagemResultadoExecucao_Inicio_To1",gxold:"OV17ContagemResultadoExecucao_Inicio_To1",gxvar:"AV17ContagemResultadoExecucao_Inicio_To1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[43],ip:[43],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17ContagemResultadoExecucao_Inicio_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17ContagemResultadoExecucao_Inicio_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1",gx.O.AV17ContagemResultadoExecucao_Inicio_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17ContagemResultadoExecucao_Inicio_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1")},nac:gx.falseFn};
   GXValidFnc[44]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1",grid:0};
   GXValidFnc[47]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM1",gxz:"ZV18ContagemResultadoExecucao_Fim1",gxold:"OV18ContagemResultadoExecucao_Fim1",gxvar:"AV18ContagemResultadoExecucao_Fim1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[47],ip:[47],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18ContagemResultadoExecucao_Fim1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18ContagemResultadoExecucao_Fim1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM1",gx.O.AV18ContagemResultadoExecucao_Fim1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18ContagemResultadoExecucao_Fim1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM1")},nac:gx.falseFn};
   GXValidFnc[49]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[51]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM_TO1",gxz:"ZV19ContagemResultadoExecucao_Fim_To1",gxold:"OV19ContagemResultadoExecucao_Fim_To1",gxvar:"AV19ContagemResultadoExecucao_Fim_To1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[51],ip:[51],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19ContagemResultadoExecucao_Fim_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV19ContagemResultadoExecucao_Fim_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO1",gx.O.AV19ContagemResultadoExecucao_Fim_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV19ContagemResultadoExecucao_Fim_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO1")},nac:gx.falseFn};
   GXValidFnc[52]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1",grid:0};
   GXValidFnc[55]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA1",gxz:"ZV20ContagemResultadoExecucao_Prevista1",gxold:"OV20ContagemResultadoExecucao_Prevista1",gxvar:"AV20ContagemResultadoExecucao_Prevista1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[55],ip:[55],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20ContagemResultadoExecucao_Prevista1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20ContagemResultadoExecucao_Prevista1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA1",gx.O.AV20ContagemResultadoExecucao_Prevista1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20ContagemResultadoExecucao_Prevista1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA1")},nac:gx.falseFn};
   GXValidFnc[57]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[59]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1",gxz:"ZV21ContagemResultadoExecucao_Prevista_To1",gxold:"OV21ContagemResultadoExecucao_Prevista_To1",gxvar:"AV21ContagemResultadoExecucao_Prevista_To1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[59],ip:[59],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21ContagemResultadoExecucao_Prevista_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21ContagemResultadoExecucao_Prevista_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1",gx.O.AV21ContagemResultadoExecucao_Prevista_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21ContagemResultadoExecucao_Prevista_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1")},nac:gx.falseFn};
   GXValidFnc[60]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_OSFSOSFM1",gxz:"ZV43ContagemResultado_OsFsOsFm1",gxold:"OV43ContagemResultado_OsFsOsFm1",gxvar:"AV43ContagemResultado_OsFsOsFm1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43ContagemResultado_OsFsOsFm1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV43ContagemResultado_OsFsOsFm1=Value},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_OSFSOSFM1",gx.O.AV43ContagemResultado_OsFsOsFm1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43ContagemResultado_OsFsOsFm1=this.val()},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_OSFSOSFM1")},nac:gx.falseFn};
   GXValidFnc[61]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_CONTRATADACOD1",gxz:"ZV82ContagemResultado_ContratadaCod1",gxold:"OV82ContagemResultado_ContratadaCod1",gxvar:"AV82ContagemResultado_ContratadaCod1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV82ContagemResultado_ContratadaCod1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV82ContagemResultado_ContratadaCod1=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_CONTRATADACOD1",gx.O.AV82ContagemResultado_ContratadaCod1)},c2v:function(){if(this.val()!==undefined)gx.O.AV82ContagemResultado_ContratadaCod1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTRATADACOD1",'.')},nac:gx.falseFn};
   GXValidFnc[62]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_OSCOD1",gxz:"ZV83ContagemResultadoExecucao_OSCod1",gxold:"OV83ContagemResultadoExecucao_OSCod1",gxvar:"AV83ContagemResultadoExecucao_OSCod1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV83ContagemResultadoExecucao_OSCod1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV83ContagemResultadoExecucao_OSCod1=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD1",gx.O.AV83ContagemResultadoExecucao_OSCod1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV83ContagemResultadoExecucao_OSCod1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD1",'.')},nac:gx.falseFn};
   GXValidFnc[64]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[65]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[68]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[70]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV23DynamicFiltersSelector2",gxold:"OV23DynamicFiltersSelector2",gxvar:"AV23DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV23DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV23DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[72]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[74]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2",grid:0};
   GXValidFnc[77]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO2",gxz:"ZV24ContagemResultadoExecucao_Inicio2",gxold:"OV24ContagemResultadoExecucao_Inicio2",gxvar:"AV24ContagemResultadoExecucao_Inicio2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[77],ip:[77],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24ContagemResultadoExecucao_Inicio2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24ContagemResultadoExecucao_Inicio2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO2",gx.O.AV24ContagemResultadoExecucao_Inicio2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24ContagemResultadoExecucao_Inicio2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO2")},nac:gx.falseFn};
   GXValidFnc[79]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[81]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2",gxz:"ZV25ContagemResultadoExecucao_Inicio_To2",gxold:"OV25ContagemResultadoExecucao_Inicio_To2",gxvar:"AV25ContagemResultadoExecucao_Inicio_To2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[81],ip:[81],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25ContagemResultadoExecucao_Inicio_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25ContagemResultadoExecucao_Inicio_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2",gx.O.AV25ContagemResultadoExecucao_Inicio_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25ContagemResultadoExecucao_Inicio_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2")},nac:gx.falseFn};
   GXValidFnc[82]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2",grid:0};
   GXValidFnc[85]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM2",gxz:"ZV26ContagemResultadoExecucao_Fim2",gxold:"OV26ContagemResultadoExecucao_Fim2",gxvar:"AV26ContagemResultadoExecucao_Fim2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[85],ip:[85],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV26ContagemResultadoExecucao_Fim2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV26ContagemResultadoExecucao_Fim2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM2",gx.O.AV26ContagemResultadoExecucao_Fim2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV26ContagemResultadoExecucao_Fim2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM2")},nac:gx.falseFn};
   GXValidFnc[87]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[89]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM_TO2",gxz:"ZV27ContagemResultadoExecucao_Fim_To2",gxold:"OV27ContagemResultadoExecucao_Fim_To2",gxvar:"AV27ContagemResultadoExecucao_Fim_To2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[89],ip:[89],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV27ContagemResultadoExecucao_Fim_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV27ContagemResultadoExecucao_Fim_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO2",gx.O.AV27ContagemResultadoExecucao_Fim_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV27ContagemResultadoExecucao_Fim_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO2")},nac:gx.falseFn};
   GXValidFnc[90]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2",grid:0};
   GXValidFnc[93]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA2",gxz:"ZV28ContagemResultadoExecucao_Prevista2",gxold:"OV28ContagemResultadoExecucao_Prevista2",gxvar:"AV28ContagemResultadoExecucao_Prevista2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[93],ip:[93],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28ContagemResultadoExecucao_Prevista2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV28ContagemResultadoExecucao_Prevista2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA2",gx.O.AV28ContagemResultadoExecucao_Prevista2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28ContagemResultadoExecucao_Prevista2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA2")},nac:gx.falseFn};
   GXValidFnc[95]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[97]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2",gxz:"ZV29ContagemResultadoExecucao_Prevista_To2",gxold:"OV29ContagemResultadoExecucao_Prevista_To2",gxvar:"AV29ContagemResultadoExecucao_Prevista_To2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[97],ip:[97],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29ContagemResultadoExecucao_Prevista_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV29ContagemResultadoExecucao_Prevista_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2",gx.O.AV29ContagemResultadoExecucao_Prevista_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29ContagemResultadoExecucao_Prevista_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2")},nac:gx.falseFn};
   GXValidFnc[98]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_OSFSOSFM2",gxz:"ZV44ContagemResultado_OsFsOsFm2",gxold:"OV44ContagemResultado_OsFsOsFm2",gxvar:"AV44ContagemResultado_OsFsOsFm2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44ContagemResultado_OsFsOsFm2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV44ContagemResultado_OsFsOsFm2=Value},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_OSFSOSFM2",gx.O.AV44ContagemResultado_OsFsOsFm2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44ContagemResultado_OsFsOsFm2=this.val()},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_OSFSOSFM2")},nac:gx.falseFn};
   GXValidFnc[99]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_CONTRATADACOD2",gxz:"ZV84ContagemResultado_ContratadaCod2",gxold:"OV84ContagemResultado_ContratadaCod2",gxvar:"AV84ContagemResultado_ContratadaCod2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV84ContagemResultado_ContratadaCod2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV84ContagemResultado_ContratadaCod2=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_CONTRATADACOD2",gx.O.AV84ContagemResultado_ContratadaCod2)},c2v:function(){if(this.val()!==undefined)gx.O.AV84ContagemResultado_ContratadaCod2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTRATADACOD2",'.')},nac:gx.falseFn};
   GXValidFnc[100]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_OSCOD2",gxz:"ZV85ContagemResultadoExecucao_OSCod2",gxold:"OV85ContagemResultadoExecucao_OSCod2",gxvar:"AV85ContagemResultadoExecucao_OSCod2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV85ContagemResultadoExecucao_OSCod2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV85ContagemResultadoExecucao_OSCod2=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD2",gx.O.AV85ContagemResultadoExecucao_OSCod2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV85ContagemResultadoExecucao_OSCod2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD2",'.')},nac:gx.falseFn};
   GXValidFnc[102]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[103]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[106]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[108]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV31DynamicFiltersSelector3",gxold:"OV31DynamicFiltersSelector3",gxvar:"AV31DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV31DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV31DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV31DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[110]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[112]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3",grid:0};
   GXValidFnc[115]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO3",gxz:"ZV32ContagemResultadoExecucao_Inicio3",gxold:"OV32ContagemResultadoExecucao_Inicio3",gxvar:"AV32ContagemResultadoExecucao_Inicio3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[115],ip:[115],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV32ContagemResultadoExecucao_Inicio3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV32ContagemResultadoExecucao_Inicio3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO3",gx.O.AV32ContagemResultadoExecucao_Inicio3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV32ContagemResultadoExecucao_Inicio3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO3")},nac:gx.falseFn};
   GXValidFnc[117]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[119]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_inicio_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3",gxz:"ZV33ContagemResultadoExecucao_Inicio_To3",gxold:"OV33ContagemResultadoExecucao_Inicio_To3",gxvar:"AV33ContagemResultadoExecucao_Inicio_To3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[119],ip:[119],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV33ContagemResultadoExecucao_Inicio_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV33ContagemResultadoExecucao_Inicio_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3",gx.O.AV33ContagemResultadoExecucao_Inicio_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV33ContagemResultadoExecucao_Inicio_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3")},nac:gx.falseFn};
   GXValidFnc[120]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3",grid:0};
   GXValidFnc[123]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM3",gxz:"ZV34ContagemResultadoExecucao_Fim3",gxold:"OV34ContagemResultadoExecucao_Fim3",gxvar:"AV34ContagemResultadoExecucao_Fim3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[123],ip:[123],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV34ContagemResultadoExecucao_Fim3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV34ContagemResultadoExecucao_Fim3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM3",gx.O.AV34ContagemResultadoExecucao_Fim3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV34ContagemResultadoExecucao_Fim3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM3")},nac:gx.falseFn};
   GXValidFnc[125]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[127]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_fim_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_FIM_TO3",gxz:"ZV35ContagemResultadoExecucao_Fim_To3",gxold:"OV35ContagemResultadoExecucao_Fim_To3",gxvar:"AV35ContagemResultadoExecucao_Fim_To3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[127],ip:[127],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35ContagemResultadoExecucao_Fim_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV35ContagemResultadoExecucao_Fim_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO3",gx.O.AV35ContagemResultadoExecucao_Fim_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35ContagemResultadoExecucao_Fim_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_FIM_TO3")},nac:gx.falseFn};
   GXValidFnc[128]={fld:"TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3",grid:0};
   GXValidFnc[131]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA3",gxz:"ZV36ContagemResultadoExecucao_Prevista3",gxold:"OV36ContagemResultadoExecucao_Prevista3",gxvar:"AV36ContagemResultadoExecucao_Prevista3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[131],ip:[131],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36ContagemResultadoExecucao_Prevista3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV36ContagemResultadoExecucao_Prevista3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA3",gx.O.AV36ContagemResultadoExecucao_Prevista3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36ContagemResultadoExecucao_Prevista3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA3")},nac:gx.falseFn};
   GXValidFnc[133]={fld:"DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[135]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultadoexecucao_prevista_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3",gxz:"ZV37ContagemResultadoExecucao_Prevista_To3",gxold:"OV37ContagemResultadoExecucao_Prevista_To3",gxvar:"AV37ContagemResultadoExecucao_Prevista_To3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[135],ip:[135],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV37ContagemResultadoExecucao_Prevista_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV37ContagemResultadoExecucao_Prevista_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3",gx.O.AV37ContagemResultadoExecucao_Prevista_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV37ContagemResultadoExecucao_Prevista_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3")},nac:gx.falseFn};
   GXValidFnc[136]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_OSFSOSFM3",gxz:"ZV45ContagemResultado_OsFsOsFm3",gxold:"OV45ContagemResultado_OsFsOsFm3",gxvar:"AV45ContagemResultado_OsFsOsFm3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45ContagemResultado_OsFsOsFm3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV45ContagemResultado_OsFsOsFm3=Value},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_OSFSOSFM3",gx.O.AV45ContagemResultado_OsFsOsFm3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45ContagemResultado_OsFsOsFm3=this.val()},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_OSFSOSFM3")},nac:gx.falseFn};
   GXValidFnc[137]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADO_CONTRATADACOD3",gxz:"ZV86ContagemResultado_ContratadaCod3",gxold:"OV86ContagemResultado_ContratadaCod3",gxvar:"AV86ContagemResultado_ContratadaCod3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV86ContagemResultado_ContratadaCod3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV86ContagemResultado_ContratadaCod3=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_CONTRATADACOD3",gx.O.AV86ContagemResultado_ContratadaCod3)},c2v:function(){if(this.val()!==undefined)gx.O.AV86ContagemResultado_ContratadaCod3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTRATADACOD3",'.')},nac:gx.falseFn};
   GXValidFnc[138]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTAGEMRESULTADOEXECUCAO_OSCOD3",gxz:"ZV87ContagemResultadoExecucao_OSCod3",gxold:"OV87ContagemResultadoExecucao_OSCod3",gxvar:"AV87ContagemResultadoExecucao_OSCod3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV87ContagemResultadoExecucao_OSCod3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV87ContagemResultadoExecucao_OSCod3=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD3",gx.O.AV87ContagemResultadoExecucao_OSCod3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV87ContagemResultadoExecucao_OSCod3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADOEXECUCAO_OSCOD3",'.')},nac:gx.falseFn};
   GXValidFnc[140]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[141]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[146]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[149]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[153]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV40Update",gxold:"OV40Update",gxvar:"AV40Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV40Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(152),gx.O.AV40Update,gx.O.AV136Update_GXI)},c2v:function(){gx.O.AV136Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV40Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(152))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(152))}, gxvar_GXI:'AV136Update_GXI',nac:gx.falseFn};
   GXValidFnc[154]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV41Delete",gxold:"OV41Delete",gxvar:"AV41Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV41Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV41Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(152),gx.O.AV41Delete,gx.O.AV137Delete_GXI)},c2v:function(){gx.O.AV137Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV41Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(152))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(152))}, gxvar_GXI:'AV137Delete_GXI',nac:gx.falseFn};
   GXValidFnc[155]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_CODIGO",gxz:"Z1405ContagemResultadoExecucao_Codigo",gxold:"O1405ContagemResultadoExecucao_Codigo",gxvar:"A1405ContagemResultadoExecucao_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1405ContagemResultadoExecucao_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1405ContagemResultadoExecucao_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_CODIGO",row || gx.fn.currentGridRowImpl(152),gx.O.A1405ContagemResultadoExecucao_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1405ContagemResultadoExecucao_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOEXECUCAO_CODIGO",row || gx.fn.currentGridRowImpl(152),'.')},nac:gx.falseFn};
   GXValidFnc[156]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:this.Valid_Contagemresultadoexecucao_oscod,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_OSCOD",gxz:"Z1404ContagemResultadoExecucao_OSCod",gxold:"O1404ContagemResultadoExecucao_OSCod",gxvar:"A1404ContagemResultadoExecucao_OSCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1404ContagemResultadoExecucao_OSCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1404ContagemResultadoExecucao_OSCod=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_OSCOD",row || gx.fn.currentGridRowImpl(152),gx.O.A1404ContagemResultadoExecucao_OSCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1404ContagemResultadoExecucao_OSCod=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOEXECUCAO_OSCOD",row || gx.fn.currentGridRowImpl(152),'.')},nac:gx.falseFn};
   GXValidFnc[157]={lvl:2,type:"svchar",len:30,dec:0,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_OSFSOSFM",gxz:"Z501ContagemResultado_OsFsOsFm",gxold:"O501ContagemResultado_OsFsOsFm",gxvar:"A501ContagemResultado_OsFsOsFm",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A501ContagemResultado_OsFsOsFm=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z501ContagemResultado_OsFsOsFm=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_OSFSOSFM",row || gx.fn.currentGridRowImpl(152),gx.O.A501ContagemResultado_OsFsOsFm,0)},c2v:function(){if(this.val()!==undefined)gx.O.A501ContagemResultado_OsFsOsFm=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_OSFSOSFM",row || gx.fn.currentGridRowImpl(152))},nac:gx.falseFn};
   GXValidFnc[158]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_INICIO",gxz:"Z1406ContagemResultadoExecucao_Inicio",gxold:"O1406ContagemResultadoExecucao_Inicio",gxvar:"A1406ContagemResultadoExecucao_Inicio",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1406ContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1406ContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_INICIO",row || gx.fn.currentGridRowImpl(152),gx.O.A1406ContagemResultadoExecucao_Inicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1406ContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADOEXECUCAO_INICIO",row || gx.fn.currentGridRowImpl(152))},nac:gx.falseFn};
   GXValidFnc[159]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_PREVISTA",gxz:"Z1408ContagemResultadoExecucao_Prevista",gxold:"O1408ContagemResultadoExecucao_Prevista",gxvar:"A1408ContagemResultadoExecucao_Prevista",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1408ContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1408ContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_PREVISTA",row || gx.fn.currentGridRowImpl(152),gx.O.A1408ContagemResultadoExecucao_Prevista,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1408ContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADOEXECUCAO_PREVISTA",row || gx.fn.currentGridRowImpl(152))},nac:gx.falseFn};
   GXValidFnc[160]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_PRAZODIAS",gxz:"Z1411ContagemResultadoExecucao_PrazoDias",gxold:"O1411ContagemResultadoExecucao_PrazoDias",gxvar:"A1411ContagemResultadoExecucao_PrazoDias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1411ContagemResultadoExecucao_PrazoDias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1411ContagemResultadoExecucao_PrazoDias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_PRAZODIAS",row || gx.fn.currentGridRowImpl(152),gx.O.A1411ContagemResultadoExecucao_PrazoDias,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1411ContagemResultadoExecucao_PrazoDias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOEXECUCAO_PRAZODIAS",row || gx.fn.currentGridRowImpl(152),'.')},nac:gx.falseFn};
   GXValidFnc[161]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_FIM",gxz:"Z1407ContagemResultadoExecucao_Fim",gxold:"O1407ContagemResultadoExecucao_Fim",gxvar:"A1407ContagemResultadoExecucao_Fim",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1407ContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1407ContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_FIM",row || gx.fn.currentGridRowImpl(152),gx.O.A1407ContagemResultadoExecucao_Fim,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1407ContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADOEXECUCAO_FIM",row || gx.fn.currentGridRowImpl(152))},nac:gx.falseFn};
   GXValidFnc[162]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:152,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOEXECUCAO_DIAS",gxz:"Z1410ContagemResultadoExecucao_Dias",gxold:"O1410ContagemResultadoExecucao_Dias",gxvar:"A1410ContagemResultadoExecucao_Dias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1410ContagemResultadoExecucao_Dias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1410ContagemResultadoExecucao_Dias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOEXECUCAO_DIAS",row || gx.fn.currentGridRowImpl(152),gx.O.A1410ContagemResultadoExecucao_Dias,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1410ContagemResultadoExecucao_Dias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOEXECUCAO_DIAS",row || gx.fn.currentGridRowImpl(152),'.')},nac:gx.falseFn};
   GXValidFnc[167]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV22DynamicFiltersEnabled2",gxold:"OV22DynamicFiltersEnabled2",gxvar:"AV22DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV22DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[168]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV30DynamicFiltersEnabled3",gxold:"OV30DynamicFiltersEnabled3",gxvar:"AV30DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV30DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV30DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV30DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV30DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[169]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_OSFSOSFM",gxz:"ZV49TFContagemResultado_OsFsOsFm",gxold:"OV49TFContagemResultado_OsFsOsFm",gxvar:"AV49TFContagemResultado_OsFsOsFm",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV49TFContagemResultado_OsFsOsFm=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV49TFContagemResultado_OsFsOsFm=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_OSFSOSFM",gx.O.AV49TFContagemResultado_OsFsOsFm,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV49TFContagemResultado_OsFsOsFm=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_OSFSOSFM")},nac:gx.falseFn};
   GXValidFnc[170]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_OSFSOSFM_SEL",gxz:"ZV50TFContagemResultado_OsFsOsFm_Sel",gxold:"OV50TFContagemResultado_OsFsOsFm_Sel",gxvar:"AV50TFContagemResultado_OsFsOsFm_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV50TFContagemResultado_OsFsOsFm_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV50TFContagemResultado_OsFsOsFm_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_OSFSOSFM_SEL",gx.O.AV50TFContagemResultado_OsFsOsFm_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV50TFContagemResultado_OsFsOsFm_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_OSFSOSFM_SEL")},nac:gx.falseFn};
   GXValidFnc[171]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_inicio,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_INICIO",gxz:"ZV53TFContagemResultadoExecucao_Inicio",gxold:"OV53TFContagemResultadoExecucao_Inicio",gxvar:"AV53TFContagemResultadoExecucao_Inicio",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[171],ip:[171],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV53TFContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV53TFContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_INICIO",gx.O.AV53TFContagemResultadoExecucao_Inicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV53TFContagemResultadoExecucao_Inicio=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_INICIO")},nac:gx.falseFn};
   GXValidFnc[172]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_inicio_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO",gxz:"ZV54TFContagemResultadoExecucao_Inicio_To",gxold:"OV54TFContagemResultadoExecucao_Inicio_To",gxvar:"AV54TFContagemResultadoExecucao_Inicio_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[172],ip:[172],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV54TFContagemResultadoExecucao_Inicio_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV54TFContagemResultadoExecucao_Inicio_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO",gx.O.AV54TFContagemResultadoExecucao_Inicio_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV54TFContagemResultadoExecucao_Inicio_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO")},nac:gx.falseFn};
   GXValidFnc[173]={fld:"DDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATES",grid:0};
   GXValidFnc[174]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_inicioauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE",gxz:"ZV55DDO_ContagemResultadoExecucao_InicioAuxDate",gxold:"OV55DDO_ContagemResultadoExecucao_InicioAuxDate",gxvar:"AV55DDO_ContagemResultadoExecucao_InicioAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[174],ip:[174],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV55DDO_ContagemResultadoExecucao_InicioAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV55DDO_ContagemResultadoExecucao_InicioAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE",gx.O.AV55DDO_ContagemResultadoExecucao_InicioAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV55DDO_ContagemResultadoExecucao_InicioAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[175]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_inicioauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO",gxz:"ZV56DDO_ContagemResultadoExecucao_InicioAuxDateTo",gxold:"OV56DDO_ContagemResultadoExecucao_InicioAuxDateTo",gxvar:"AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[175],ip:[175],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV56DDO_ContagemResultadoExecucao_InicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO",gx.O.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[176]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_prevista,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA",gxz:"ZV59TFContagemResultadoExecucao_Prevista",gxold:"OV59TFContagemResultadoExecucao_Prevista",gxvar:"AV59TFContagemResultadoExecucao_Prevista",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[176],ip:[176],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV59TFContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV59TFContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA",gx.O.AV59TFContagemResultadoExecucao_Prevista,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV59TFContagemResultadoExecucao_Prevista=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA")},nac:gx.falseFn};
   GXValidFnc[177]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_prevista_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO",gxz:"ZV60TFContagemResultadoExecucao_Prevista_To",gxold:"OV60TFContagemResultadoExecucao_Prevista_To",gxvar:"AV60TFContagemResultadoExecucao_Prevista_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[177],ip:[177],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV60TFContagemResultadoExecucao_Prevista_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV60TFContagemResultadoExecucao_Prevista_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO",gx.O.AV60TFContagemResultadoExecucao_Prevista_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV60TFContagemResultadoExecucao_Prevista_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO")},nac:gx.falseFn};
   GXValidFnc[178]={fld:"DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATES",grid:0};
   GXValidFnc[179]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_previstaauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE",gxz:"ZV61DDO_ContagemResultadoExecucao_PrevistaAuxDate",gxold:"OV61DDO_ContagemResultadoExecucao_PrevistaAuxDate",gxvar:"AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[179],ip:[179],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV61DDO_ContagemResultadoExecucao_PrevistaAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE",gx.O.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[180]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_previstaauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO",gxz:"ZV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo",gxold:"OV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo",gxvar:"AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[180],ip:[180],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO",gx.O.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[181]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS",gxz:"ZV65TFContagemResultadoExecucao_PrazoDias",gxold:"OV65TFContagemResultadoExecucao_PrazoDias",gxvar:"AV65TFContagemResultadoExecucao_PrazoDias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV65TFContagemResultadoExecucao_PrazoDias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV65TFContagemResultadoExecucao_PrazoDias=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS",gx.O.AV65TFContagemResultadoExecucao_PrazoDias,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV65TFContagemResultadoExecucao_PrazoDias=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS",'.')},nac:gx.falseFn};
   GXValidFnc[182]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO",gxz:"ZV66TFContagemResultadoExecucao_PrazoDias_To",gxold:"OV66TFContagemResultadoExecucao_PrazoDias_To",gxvar:"AV66TFContagemResultadoExecucao_PrazoDias_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV66TFContagemResultadoExecucao_PrazoDias_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV66TFContagemResultadoExecucao_PrazoDias_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO",gx.O.AV66TFContagemResultadoExecucao_PrazoDias_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV66TFContagemResultadoExecucao_PrazoDias_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO",'.')},nac:gx.falseFn};
   GXValidFnc[183]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_fim,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_FIM",gxz:"ZV69TFContagemResultadoExecucao_Fim",gxold:"OV69TFContagemResultadoExecucao_Fim",gxvar:"AV69TFContagemResultadoExecucao_Fim",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[183],ip:[183],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV69TFContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV69TFContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_FIM",gx.O.AV69TFContagemResultadoExecucao_Fim,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV69TFContagemResultadoExecucao_Fim=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_FIM")},nac:gx.falseFn};
   GXValidFnc[184]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadoexecucao_fim_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO",gxz:"ZV70TFContagemResultadoExecucao_Fim_To",gxold:"OV70TFContagemResultadoExecucao_Fim_To",gxvar:"AV70TFContagemResultadoExecucao_Fim_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[184],ip:[184],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV70TFContagemResultadoExecucao_Fim_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV70TFContagemResultadoExecucao_Fim_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO",gx.O.AV70TFContagemResultadoExecucao_Fim_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV70TFContagemResultadoExecucao_Fim_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO")},nac:gx.falseFn};
   GXValidFnc[185]={fld:"DDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATES",grid:0};
   GXValidFnc[186]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_fimauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE",gxz:"ZV71DDO_ContagemResultadoExecucao_FimAuxDate",gxold:"OV71DDO_ContagemResultadoExecucao_FimAuxDate",gxvar:"AV71DDO_ContagemResultadoExecucao_FimAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[186],ip:[186],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV71DDO_ContagemResultadoExecucao_FimAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV71DDO_ContagemResultadoExecucao_FimAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE",gx.O.AV71DDO_ContagemResultadoExecucao_FimAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV71DDO_ContagemResultadoExecucao_FimAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE")},nac:gx.falseFn};
   GXValidFnc[187]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadoexecucao_fimauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO",gxz:"ZV72DDO_ContagemResultadoExecucao_FimAuxDateTo",gxold:"OV72DDO_ContagemResultadoExecucao_FimAuxDateTo",gxvar:"AV72DDO_ContagemResultadoExecucao_FimAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[187],ip:[187],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV72DDO_ContagemResultadoExecucao_FimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO",gx.O.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[188]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_DIAS",gxz:"ZV75TFContagemResultadoExecucao_Dias",gxold:"OV75TFContagemResultadoExecucao_Dias",gxvar:"AV75TFContagemResultadoExecucao_Dias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV75TFContagemResultadoExecucao_Dias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV75TFContagemResultadoExecucao_Dias=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_DIAS",gx.O.AV75TFContagemResultadoExecucao_Dias,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV75TFContagemResultadoExecucao_Dias=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTAGEMRESULTADOEXECUCAO_DIAS",'.')},nac:gx.falseFn};
   GXValidFnc[189]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO",gxz:"ZV76TFContagemResultadoExecucao_Dias_To",gxold:"OV76TFContagemResultadoExecucao_Dias_To",gxvar:"AV76TFContagemResultadoExecucao_Dias_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV76TFContagemResultadoExecucao_Dias_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV76TFContagemResultadoExecucao_Dias_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO",gx.O.AV76TFContagemResultadoExecucao_Dias_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV76TFContagemResultadoExecucao_Dias_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO",'.')},nac:gx.falseFn};
   GXValidFnc[191]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE",gxz:"ZV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace",gxold:"OV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace",gxvar:"AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE",gx.O.AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[193]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE",gxz:"ZV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace",gxold:"OV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace",gxvar:"AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE",gx.O.AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[195]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE",gxz:"ZV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace",gxold:"OV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace",gxvar:"AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE",gx.O.AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[197]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE",gxz:"ZV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace",gxold:"OV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace",gxvar:"AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE",gx.O.AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[199]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE",gxz:"ZV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace",gxold:"OV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace",gxvar:"AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE",gx.O.AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[201]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE",gxz:"ZV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace",gxold:"OV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace",gxvar:"AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE",gx.O.AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV88Contratada_Codigo = 0 ;
   this.ZV88Contratada_Codigo = 0 ;
   this.OV88Contratada_Codigo = 0 ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16ContagemResultadoExecucao_Inicio1 = gx.date.nullDate() ;
   this.ZV16ContagemResultadoExecucao_Inicio1 = gx.date.nullDate() ;
   this.OV16ContagemResultadoExecucao_Inicio1 = gx.date.nullDate() ;
   this.AV17ContagemResultadoExecucao_Inicio_To1 = gx.date.nullDate() ;
   this.ZV17ContagemResultadoExecucao_Inicio_To1 = gx.date.nullDate() ;
   this.OV17ContagemResultadoExecucao_Inicio_To1 = gx.date.nullDate() ;
   this.AV18ContagemResultadoExecucao_Fim1 = gx.date.nullDate() ;
   this.ZV18ContagemResultadoExecucao_Fim1 = gx.date.nullDate() ;
   this.OV18ContagemResultadoExecucao_Fim1 = gx.date.nullDate() ;
   this.AV19ContagemResultadoExecucao_Fim_To1 = gx.date.nullDate() ;
   this.ZV19ContagemResultadoExecucao_Fim_To1 = gx.date.nullDate() ;
   this.OV19ContagemResultadoExecucao_Fim_To1 = gx.date.nullDate() ;
   this.AV20ContagemResultadoExecucao_Prevista1 = gx.date.nullDate() ;
   this.ZV20ContagemResultadoExecucao_Prevista1 = gx.date.nullDate() ;
   this.OV20ContagemResultadoExecucao_Prevista1 = gx.date.nullDate() ;
   this.AV21ContagemResultadoExecucao_Prevista_To1 = gx.date.nullDate() ;
   this.ZV21ContagemResultadoExecucao_Prevista_To1 = gx.date.nullDate() ;
   this.OV21ContagemResultadoExecucao_Prevista_To1 = gx.date.nullDate() ;
   this.AV43ContagemResultado_OsFsOsFm1 = "" ;
   this.ZV43ContagemResultado_OsFsOsFm1 = "" ;
   this.OV43ContagemResultado_OsFsOsFm1 = "" ;
   this.AV82ContagemResultado_ContratadaCod1 = 0 ;
   this.ZV82ContagemResultado_ContratadaCod1 = 0 ;
   this.OV82ContagemResultado_ContratadaCod1 = 0 ;
   this.AV83ContagemResultadoExecucao_OSCod1 = 0 ;
   this.ZV83ContagemResultadoExecucao_OSCod1 = 0 ;
   this.OV83ContagemResultadoExecucao_OSCod1 = 0 ;
   this.AV23DynamicFiltersSelector2 = "" ;
   this.ZV23DynamicFiltersSelector2 = "" ;
   this.OV23DynamicFiltersSelector2 = "" ;
   this.AV24ContagemResultadoExecucao_Inicio2 = gx.date.nullDate() ;
   this.ZV24ContagemResultadoExecucao_Inicio2 = gx.date.nullDate() ;
   this.OV24ContagemResultadoExecucao_Inicio2 = gx.date.nullDate() ;
   this.AV25ContagemResultadoExecucao_Inicio_To2 = gx.date.nullDate() ;
   this.ZV25ContagemResultadoExecucao_Inicio_To2 = gx.date.nullDate() ;
   this.OV25ContagemResultadoExecucao_Inicio_To2 = gx.date.nullDate() ;
   this.AV26ContagemResultadoExecucao_Fim2 = gx.date.nullDate() ;
   this.ZV26ContagemResultadoExecucao_Fim2 = gx.date.nullDate() ;
   this.OV26ContagemResultadoExecucao_Fim2 = gx.date.nullDate() ;
   this.AV27ContagemResultadoExecucao_Fim_To2 = gx.date.nullDate() ;
   this.ZV27ContagemResultadoExecucao_Fim_To2 = gx.date.nullDate() ;
   this.OV27ContagemResultadoExecucao_Fim_To2 = gx.date.nullDate() ;
   this.AV28ContagemResultadoExecucao_Prevista2 = gx.date.nullDate() ;
   this.ZV28ContagemResultadoExecucao_Prevista2 = gx.date.nullDate() ;
   this.OV28ContagemResultadoExecucao_Prevista2 = gx.date.nullDate() ;
   this.AV29ContagemResultadoExecucao_Prevista_To2 = gx.date.nullDate() ;
   this.ZV29ContagemResultadoExecucao_Prevista_To2 = gx.date.nullDate() ;
   this.OV29ContagemResultadoExecucao_Prevista_To2 = gx.date.nullDate() ;
   this.AV44ContagemResultado_OsFsOsFm2 = "" ;
   this.ZV44ContagemResultado_OsFsOsFm2 = "" ;
   this.OV44ContagemResultado_OsFsOsFm2 = "" ;
   this.AV84ContagemResultado_ContratadaCod2 = 0 ;
   this.ZV84ContagemResultado_ContratadaCod2 = 0 ;
   this.OV84ContagemResultado_ContratadaCod2 = 0 ;
   this.AV85ContagemResultadoExecucao_OSCod2 = 0 ;
   this.ZV85ContagemResultadoExecucao_OSCod2 = 0 ;
   this.OV85ContagemResultadoExecucao_OSCod2 = 0 ;
   this.AV31DynamicFiltersSelector3 = "" ;
   this.ZV31DynamicFiltersSelector3 = "" ;
   this.OV31DynamicFiltersSelector3 = "" ;
   this.AV32ContagemResultadoExecucao_Inicio3 = gx.date.nullDate() ;
   this.ZV32ContagemResultadoExecucao_Inicio3 = gx.date.nullDate() ;
   this.OV32ContagemResultadoExecucao_Inicio3 = gx.date.nullDate() ;
   this.AV33ContagemResultadoExecucao_Inicio_To3 = gx.date.nullDate() ;
   this.ZV33ContagemResultadoExecucao_Inicio_To3 = gx.date.nullDate() ;
   this.OV33ContagemResultadoExecucao_Inicio_To3 = gx.date.nullDate() ;
   this.AV34ContagemResultadoExecucao_Fim3 = gx.date.nullDate() ;
   this.ZV34ContagemResultadoExecucao_Fim3 = gx.date.nullDate() ;
   this.OV34ContagemResultadoExecucao_Fim3 = gx.date.nullDate() ;
   this.AV35ContagemResultadoExecucao_Fim_To3 = gx.date.nullDate() ;
   this.ZV35ContagemResultadoExecucao_Fim_To3 = gx.date.nullDate() ;
   this.OV35ContagemResultadoExecucao_Fim_To3 = gx.date.nullDate() ;
   this.AV36ContagemResultadoExecucao_Prevista3 = gx.date.nullDate() ;
   this.ZV36ContagemResultadoExecucao_Prevista3 = gx.date.nullDate() ;
   this.OV36ContagemResultadoExecucao_Prevista3 = gx.date.nullDate() ;
   this.AV37ContagemResultadoExecucao_Prevista_To3 = gx.date.nullDate() ;
   this.ZV37ContagemResultadoExecucao_Prevista_To3 = gx.date.nullDate() ;
   this.OV37ContagemResultadoExecucao_Prevista_To3 = gx.date.nullDate() ;
   this.AV45ContagemResultado_OsFsOsFm3 = "" ;
   this.ZV45ContagemResultado_OsFsOsFm3 = "" ;
   this.OV45ContagemResultado_OsFsOsFm3 = "" ;
   this.AV86ContagemResultado_ContratadaCod3 = 0 ;
   this.ZV86ContagemResultado_ContratadaCod3 = 0 ;
   this.OV86ContagemResultado_ContratadaCod3 = 0 ;
   this.AV87ContagemResultadoExecucao_OSCod3 = 0 ;
   this.ZV87ContagemResultadoExecucao_OSCod3 = 0 ;
   this.OV87ContagemResultadoExecucao_OSCod3 = 0 ;
   this.ZV40Update = "" ;
   this.OV40Update = "" ;
   this.ZV41Delete = "" ;
   this.OV41Delete = "" ;
   this.Z1405ContagemResultadoExecucao_Codigo = 0 ;
   this.O1405ContagemResultadoExecucao_Codigo = 0 ;
   this.Z1404ContagemResultadoExecucao_OSCod = 0 ;
   this.O1404ContagemResultadoExecucao_OSCod = 0 ;
   this.Z501ContagemResultado_OsFsOsFm = "" ;
   this.O501ContagemResultado_OsFsOsFm = "" ;
   this.Z1406ContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.O1406ContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.Z1408ContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.O1408ContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.Z1411ContagemResultadoExecucao_PrazoDias = 0 ;
   this.O1411ContagemResultadoExecucao_PrazoDias = 0 ;
   this.Z1407ContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.O1407ContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.Z1410ContagemResultadoExecucao_Dias = 0 ;
   this.O1410ContagemResultadoExecucao_Dias = 0 ;
   this.AV22DynamicFiltersEnabled2 = false ;
   this.ZV22DynamicFiltersEnabled2 = false ;
   this.OV22DynamicFiltersEnabled2 = false ;
   this.AV30DynamicFiltersEnabled3 = false ;
   this.ZV30DynamicFiltersEnabled3 = false ;
   this.OV30DynamicFiltersEnabled3 = false ;
   this.AV49TFContagemResultado_OsFsOsFm = "" ;
   this.ZV49TFContagemResultado_OsFsOsFm = "" ;
   this.OV49TFContagemResultado_OsFsOsFm = "" ;
   this.AV50TFContagemResultado_OsFsOsFm_Sel = "" ;
   this.ZV50TFContagemResultado_OsFsOsFm_Sel = "" ;
   this.OV50TFContagemResultado_OsFsOsFm_Sel = "" ;
   this.AV53TFContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.ZV53TFContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.OV53TFContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.AV54TFContagemResultadoExecucao_Inicio_To = gx.date.nullDate() ;
   this.ZV54TFContagemResultadoExecucao_Inicio_To = gx.date.nullDate() ;
   this.OV54TFContagemResultadoExecucao_Inicio_To = gx.date.nullDate() ;
   this.AV55DDO_ContagemResultadoExecucao_InicioAuxDate = gx.date.nullDate() ;
   this.ZV55DDO_ContagemResultadoExecucao_InicioAuxDate = gx.date.nullDate() ;
   this.OV55DDO_ContagemResultadoExecucao_InicioAuxDate = gx.date.nullDate() ;
   this.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = gx.date.nullDate() ;
   this.ZV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = gx.date.nullDate() ;
   this.OV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = gx.date.nullDate() ;
   this.AV59TFContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.ZV59TFContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.OV59TFContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.AV60TFContagemResultadoExecucao_Prevista_To = gx.date.nullDate() ;
   this.ZV60TFContagemResultadoExecucao_Prevista_To = gx.date.nullDate() ;
   this.OV60TFContagemResultadoExecucao_Prevista_To = gx.date.nullDate() ;
   this.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = gx.date.nullDate() ;
   this.ZV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = gx.date.nullDate() ;
   this.OV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = gx.date.nullDate() ;
   this.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = gx.date.nullDate() ;
   this.ZV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = gx.date.nullDate() ;
   this.OV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = gx.date.nullDate() ;
   this.AV65TFContagemResultadoExecucao_PrazoDias = 0 ;
   this.ZV65TFContagemResultadoExecucao_PrazoDias = 0 ;
   this.OV65TFContagemResultadoExecucao_PrazoDias = 0 ;
   this.AV66TFContagemResultadoExecucao_PrazoDias_To = 0 ;
   this.ZV66TFContagemResultadoExecucao_PrazoDias_To = 0 ;
   this.OV66TFContagemResultadoExecucao_PrazoDias_To = 0 ;
   this.AV69TFContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.ZV69TFContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.OV69TFContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.AV70TFContagemResultadoExecucao_Fim_To = gx.date.nullDate() ;
   this.ZV70TFContagemResultadoExecucao_Fim_To = gx.date.nullDate() ;
   this.OV70TFContagemResultadoExecucao_Fim_To = gx.date.nullDate() ;
   this.AV71DDO_ContagemResultadoExecucao_FimAuxDate = gx.date.nullDate() ;
   this.ZV71DDO_ContagemResultadoExecucao_FimAuxDate = gx.date.nullDate() ;
   this.OV71DDO_ContagemResultadoExecucao_FimAuxDate = gx.date.nullDate() ;
   this.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = gx.date.nullDate() ;
   this.ZV72DDO_ContagemResultadoExecucao_FimAuxDateTo = gx.date.nullDate() ;
   this.OV72DDO_ContagemResultadoExecucao_FimAuxDateTo = gx.date.nullDate() ;
   this.AV75TFContagemResultadoExecucao_Dias = 0 ;
   this.ZV75TFContagemResultadoExecucao_Dias = 0 ;
   this.OV75TFContagemResultadoExecucao_Dias = 0 ;
   this.AV76TFContagemResultadoExecucao_Dias_To = 0 ;
   this.ZV76TFContagemResultadoExecucao_Dias_To = 0 ;
   this.OV76TFContagemResultadoExecucao_Dias_To = 0 ;
   this.AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = "" ;
   this.ZV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = "" ;
   this.OV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = "" ;
   this.AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "" ;
   this.ZV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "" ;
   this.OV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "" ;
   this.AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "" ;
   this.ZV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "" ;
   this.OV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "" ;
   this.AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "" ;
   this.ZV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "" ;
   this.OV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "" ;
   this.AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "" ;
   this.ZV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "" ;
   this.OV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "" ;
   this.AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "" ;
   this.ZV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "" ;
   this.OV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "" ;
   this.AV88Contratada_Codigo = 0 ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16ContagemResultadoExecucao_Inicio1 = gx.date.nullDate() ;
   this.AV17ContagemResultadoExecucao_Inicio_To1 = gx.date.nullDate() ;
   this.AV18ContagemResultadoExecucao_Fim1 = gx.date.nullDate() ;
   this.AV19ContagemResultadoExecucao_Fim_To1 = gx.date.nullDate() ;
   this.AV20ContagemResultadoExecucao_Prevista1 = gx.date.nullDate() ;
   this.AV21ContagemResultadoExecucao_Prevista_To1 = gx.date.nullDate() ;
   this.AV43ContagemResultado_OsFsOsFm1 = "" ;
   this.AV82ContagemResultado_ContratadaCod1 = 0 ;
   this.AV83ContagemResultadoExecucao_OSCod1 = 0 ;
   this.AV23DynamicFiltersSelector2 = "" ;
   this.AV24ContagemResultadoExecucao_Inicio2 = gx.date.nullDate() ;
   this.AV25ContagemResultadoExecucao_Inicio_To2 = gx.date.nullDate() ;
   this.AV26ContagemResultadoExecucao_Fim2 = gx.date.nullDate() ;
   this.AV27ContagemResultadoExecucao_Fim_To2 = gx.date.nullDate() ;
   this.AV28ContagemResultadoExecucao_Prevista2 = gx.date.nullDate() ;
   this.AV29ContagemResultadoExecucao_Prevista_To2 = gx.date.nullDate() ;
   this.AV44ContagemResultado_OsFsOsFm2 = "" ;
   this.AV84ContagemResultado_ContratadaCod2 = 0 ;
   this.AV85ContagemResultadoExecucao_OSCod2 = 0 ;
   this.AV31DynamicFiltersSelector3 = "" ;
   this.AV32ContagemResultadoExecucao_Inicio3 = gx.date.nullDate() ;
   this.AV33ContagemResultadoExecucao_Inicio_To3 = gx.date.nullDate() ;
   this.AV34ContagemResultadoExecucao_Fim3 = gx.date.nullDate() ;
   this.AV35ContagemResultadoExecucao_Fim_To3 = gx.date.nullDate() ;
   this.AV36ContagemResultadoExecucao_Prevista3 = gx.date.nullDate() ;
   this.AV37ContagemResultadoExecucao_Prevista_To3 = gx.date.nullDate() ;
   this.AV45ContagemResultado_OsFsOsFm3 = "" ;
   this.AV86ContagemResultado_ContratadaCod3 = 0 ;
   this.AV87ContagemResultadoExecucao_OSCod3 = 0 ;
   this.AV80GridCurrentPage = 0 ;
   this.AV22DynamicFiltersEnabled2 = false ;
   this.AV30DynamicFiltersEnabled3 = false ;
   this.AV49TFContagemResultado_OsFsOsFm = "" ;
   this.AV50TFContagemResultado_OsFsOsFm_Sel = "" ;
   this.AV53TFContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.AV54TFContagemResultadoExecucao_Inicio_To = gx.date.nullDate() ;
   this.AV55DDO_ContagemResultadoExecucao_InicioAuxDate = gx.date.nullDate() ;
   this.AV56DDO_ContagemResultadoExecucao_InicioAuxDateTo = gx.date.nullDate() ;
   this.AV59TFContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.AV60TFContagemResultadoExecucao_Prevista_To = gx.date.nullDate() ;
   this.AV61DDO_ContagemResultadoExecucao_PrevistaAuxDate = gx.date.nullDate() ;
   this.AV62DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = gx.date.nullDate() ;
   this.AV65TFContagemResultadoExecucao_PrazoDias = 0 ;
   this.AV66TFContagemResultadoExecucao_PrazoDias_To = 0 ;
   this.AV69TFContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.AV70TFContagemResultadoExecucao_Fim_To = gx.date.nullDate() ;
   this.AV71DDO_ContagemResultadoExecucao_FimAuxDate = gx.date.nullDate() ;
   this.AV72DDO_ContagemResultadoExecucao_FimAuxDateTo = gx.date.nullDate() ;
   this.AV75TFContagemResultadoExecucao_Dias = 0 ;
   this.AV76TFContagemResultadoExecucao_Dias_To = 0 ;
   this.AV78DDO_TitleSettingsIcons = {} ;
   this.AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace = "" ;
   this.AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "" ;
   this.AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "" ;
   this.AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "" ;
   this.AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "" ;
   this.AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "" ;
   this.A52Contratada_AreaTrabalhoCod = 0 ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A457ContagemResultado_Demanda = "" ;
   this.AV40Update = "" ;
   this.AV41Delete = "" ;
   this.A1405ContagemResultadoExecucao_Codigo = 0 ;
   this.A1404ContagemResultadoExecucao_OSCod = 0 ;
   this.A501ContagemResultado_OsFsOsFm = "" ;
   this.A1406ContagemResultadoExecucao_Inicio = gx.date.nullDate() ;
   this.A1408ContagemResultadoExecucao_Prevista = gx.date.nullDate() ;
   this.A1411ContagemResultadoExecucao_PrazoDias = 0 ;
   this.A1407ContagemResultadoExecucao_Fim = gx.date.nullDate() ;
   this.A1410ContagemResultadoExecucao_Dias = 0 ;
   this.AV6WWPContext = {} ;
   this.AV138Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV39DynamicFiltersIgnoreFirst = false ;
   this.AV38DynamicFiltersRemoving = false ;
   this.Events = {"e11ki2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12ki2_client": ["DDO_CONTAGEMRESULTADO_OSFSOSFM.ONOPTIONCLICKED", true] ,"e13ki2_client": ["DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED", true] ,"e14ki2_client": ["DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED", true] ,"e15ki2_client": ["DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED", true] ,"e16ki2_client": ["DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED", true] ,"e17ki2_client": ["DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED", true] ,"e18ki2_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e19ki2_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e20ki2_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e21ki2_client": ["'DOCLEANFILTERS'", true] ,"e22ki2_client": ["'DOINSERT'", true] ,"e31ki2_client": ["ENTER", true] ,"e23ki2_client": ["'ADDDYNAMICFILTERS1'", true] ,"e24ki2_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e25ki2_client": ["'ADDDYNAMICFILTERS2'", true] ,"e26ki2_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e27ki2_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e33ki2_client": ["CANCEL", true] ,"e32ki1_client": ["'DOATUALIZAR'", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV48ContagemResultado_OsFsOsFmTitleFilterData',fld:'vCONTAGEMRESULTADO_OSFSOSFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContagemResultadoExecucao_InicioTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContagemResultadoExecucao_PrevistaTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA',pic:'',nv:null},{av:'AV64ContagemResultadoExecucao_PrazoDiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV68ContagemResultadoExecucao_FimTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV74ContagemResultadoExecucao_DiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'CONTAGEMRESULTADO_OSFSOSFM',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_OSFSOSFM","Title")',ctrl:'CONTAGEMRESULTADO_OSFSOSFM',prop:'Title'},{ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_INICIO","Title")',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Title'},{ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_PREVISTA","Title")',ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Title'},{ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_PRAZODIAS","Title")',ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Title'},{ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_FIM","Title")',ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Title'},{ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_DIAS","Title")',ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Title'},{av:'AV80GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV81GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_OSFSOSFM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.SelectedValue_get',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'SelectedValue_get'}],[{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_get'}],[{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_get'}],[{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_get'}],[{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_get'}],[{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_get'}],[{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV40Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV41Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOEXECUCAO_INICIO","Link")',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Link'}]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD2'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD3'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD1'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD1'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD2'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD3'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD1'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD2'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD2'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD3'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD1'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD3'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'FilteredText_set'},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'this.DDO_CONTAGEMRESULTADO_OSFSOSFMContainer.SelectedValue_set',ctrl:'DDO_CONTAGEMRESULTADO_OSFSOSFM',prop:'SelectedValue_set'},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_set'},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer.FilteredTextTo_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_set'},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_set'},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer.FilteredTextTo_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_set'},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_set'},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer.FilteredTextTo_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_set'},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_set'},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer.FilteredTextTo_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_set'},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.FilteredText_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_set'},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer.FilteredTextTo_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM1","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM1',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD1'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD1","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',prop:'Visible'},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM2","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM2',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD2'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD2","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_FIM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_PREVISTA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADO_OSFSOSFM3","Visible")',ctrl:'vCONTAGEMRESULTADO_OSFSOSFM3',prop:'Visible'},{ctrl:'vCONTAGEMRESULTADO_CONTRATADACOD3'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOEXECUCAO_OSCOD3","Visible")',ctrl:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',prop:'Visible'}]];
   this.EvtParms["'DOATUALIZAR'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["ENTER"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18ContagemResultadoExecucao_Fim1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM1',pic:'99/99/99 99:99',nv:''},{av:'AV19ContagemResultadoExecucao_Fim_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultadoExecucao_Prevista1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Prevista_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_OsFsOsFm1',fld:'vCONTAGEMRESULTADO_OSFSOSFM1',pic:'',nv:''},{av:'AV82ContagemResultado_ContratadaCod1',fld:'vCONTAGEMRESULTADO_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV83ContagemResultadoExecucao_OSCod1',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV26ContagemResultadoExecucao_Fim2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM2',pic:'99/99/99 99:99',nv:''},{av:'AV27ContagemResultadoExecucao_Fim_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV28ContagemResultadoExecucao_Prevista2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA2',pic:'99/99/99 99:99',nv:''},{av:'AV29ContagemResultadoExecucao_Prevista_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV44ContagemResultado_OsFsOsFm2',fld:'vCONTAGEMRESULTADO_OSFSOSFM2',pic:'',nv:''},{av:'AV84ContagemResultado_ContratadaCod2',fld:'vCONTAGEMRESULTADO_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV85ContagemResultadoExecucao_OSCod2',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD2',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultadoExecucao_Fim3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM3',pic:'99/99/99 99:99',nv:''},{av:'AV35ContagemResultadoExecucao_Fim_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_FIM_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV36ContagemResultadoExecucao_Prevista3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA3',pic:'99/99/99 99:99',nv:''},{av:'AV37ContagemResultadoExecucao_Prevista_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV45ContagemResultado_OsFsOsFm3',fld:'vCONTAGEMRESULTADO_OSFSOSFM3',pic:'',nv:''},{av:'AV86ContagemResultado_ContratadaCod3',fld:'vCONTAGEMRESULTADO_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV87ContagemResultadoExecucao_OSCod3',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCOD3',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFContagemResultado_OsFsOsFm',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'AV50TFContagemResultado_OsFsOsFm_Sel',fld:'vTFCONTAGEMRESULTADO_OSFSOSFM_SEL',pic:'',nv:''},{av:'AV53TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV54TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV59TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV60TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV65TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV66TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV69TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV70TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV76TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV51ddo_ContagemResultado_OsFsOsFmTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_OSFSOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV138Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV39DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV38DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A457ContagemResultado_Demanda", "CONTAGEMRESULTADO_DEMANDA", 0, "svchar");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV138Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV39DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV38DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("AV138Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV39DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV38DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   GridContainer.addRefreshingVar(this.GXValidFnc[32]);
   GridContainer.addRefreshingVar(this.GXValidFnc[39]);
   GridContainer.addRefreshingVar(this.GXValidFnc[43]);
   GridContainer.addRefreshingVar(this.GXValidFnc[47]);
   GridContainer.addRefreshingVar(this.GXValidFnc[51]);
   GridContainer.addRefreshingVar(this.GXValidFnc[55]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[60]);
   GridContainer.addRefreshingVar(this.GXValidFnc[61]);
   GridContainer.addRefreshingVar(this.GXValidFnc[62]);
   GridContainer.addRefreshingVar(this.GXValidFnc[70]);
   GridContainer.addRefreshingVar(this.GXValidFnc[77]);
   GridContainer.addRefreshingVar(this.GXValidFnc[81]);
   GridContainer.addRefreshingVar(this.GXValidFnc[85]);
   GridContainer.addRefreshingVar(this.GXValidFnc[89]);
   GridContainer.addRefreshingVar(this.GXValidFnc[93]);
   GridContainer.addRefreshingVar(this.GXValidFnc[97]);
   GridContainer.addRefreshingVar(this.GXValidFnc[98]);
   GridContainer.addRefreshingVar(this.GXValidFnc[99]);
   GridContainer.addRefreshingVar(this.GXValidFnc[100]);
   GridContainer.addRefreshingVar(this.GXValidFnc[108]);
   GridContainer.addRefreshingVar(this.GXValidFnc[115]);
   GridContainer.addRefreshingVar(this.GXValidFnc[119]);
   GridContainer.addRefreshingVar(this.GXValidFnc[123]);
   GridContainer.addRefreshingVar(this.GXValidFnc[127]);
   GridContainer.addRefreshingVar(this.GXValidFnc[131]);
   GridContainer.addRefreshingVar(this.GXValidFnc[135]);
   GridContainer.addRefreshingVar(this.GXValidFnc[136]);
   GridContainer.addRefreshingVar(this.GXValidFnc[137]);
   GridContainer.addRefreshingVar(this.GXValidFnc[138]);
   GridContainer.addRefreshingVar(this.GXValidFnc[167]);
   GridContainer.addRefreshingVar(this.GXValidFnc[168]);
   GridContainer.addRefreshingVar(this.GXValidFnc[169]);
   GridContainer.addRefreshingVar(this.GXValidFnc[170]);
   GridContainer.addRefreshingVar(this.GXValidFnc[171]);
   GridContainer.addRefreshingVar(this.GXValidFnc[172]);
   GridContainer.addRefreshingVar(this.GXValidFnc[176]);
   GridContainer.addRefreshingVar(this.GXValidFnc[177]);
   GridContainer.addRefreshingVar(this.GXValidFnc[181]);
   GridContainer.addRefreshingVar(this.GXValidFnc[182]);
   GridContainer.addRefreshingVar(this.GXValidFnc[183]);
   GridContainer.addRefreshingVar(this.GXValidFnc[184]);
   GridContainer.addRefreshingVar(this.GXValidFnc[188]);
   GridContainer.addRefreshingVar(this.GXValidFnc[189]);
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar(this.GXValidFnc[191]);
   GridContainer.addRefreshingVar(this.GXValidFnc[193]);
   GridContainer.addRefreshingVar(this.GXValidFnc[195]);
   GridContainer.addRefreshingVar(this.GXValidFnc[197]);
   GridContainer.addRefreshingVar(this.GXValidFnc[199]);
   GridContainer.addRefreshingVar(this.GXValidFnc[201]);
   GridContainer.addRefreshingVar(this.GXValidFnc[25]);
   GridContainer.addRefreshingVar({rfrVar:"AV138Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV39DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV38DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A1405ContagemResultadoExecucao_Codigo", rfrProp:"Value", gxAttId:"1405"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwcontagemresultadoexecucao);
