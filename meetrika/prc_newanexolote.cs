/*
               File: PRC_NewAnexoLote
        Description: New Arquivos anexos do Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:29.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newanexolote : GXProcedure
   {
      public prc_newanexolote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newanexolote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_LoteArquivoAnexo_LoteCod )
      {
         this.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         initialize();
         executePrivate();
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
      }

      public int executeUdp( )
      {
         this.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         initialize();
         executePrivate();
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
         return A841LoteArquivoAnexo_LoteCod ;
      }

      public void executeSubmit( ref int aP0_LoteArquivoAnexo_LoteCod )
      {
         prc_newanexolote objprc_newanexolote;
         objprc_newanexolote = new prc_newanexolote();
         objprc_newanexolote.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         objprc_newanexolote.context.SetSubmitInitialConfig(context);
         objprc_newanexolote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newanexolote);
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newanexolote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13Sdt_Arquivos.FromXml(AV14Websession.Get("ArquivosEvd"), "");
         AV20GXV1 = 1;
         while ( AV20GXV1 <= AV13Sdt_Arquivos.Count )
         {
            AV12Sdt_Arquivo = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV13Sdt_Arquivos.Item(AV20GXV1));
            AV21GXLvl6 = 0;
            /* Using cursor P005V2 */
            pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_nomearq, AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_tipoarq});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A840LoteArquivoAnexo_TipoArq = P005V2_A840LoteArquivoAnexo_TipoArq[0];
               n840LoteArquivoAnexo_TipoArq = P005V2_n840LoteArquivoAnexo_TipoArq[0];
               A839LoteArquivoAnexo_NomeArq = P005V2_A839LoteArquivoAnexo_NomeArq[0];
               n839LoteArquivoAnexo_NomeArq = P005V2_n839LoteArquivoAnexo_NomeArq[0];
               A837LoteArquivoAnexo_Descricao = P005V2_A837LoteArquivoAnexo_Descricao[0];
               n837LoteArquivoAnexo_Descricao = P005V2_n837LoteArquivoAnexo_Descricao[0];
               A645TipoDocumento_Codigo = P005V2_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = P005V2_n645TipoDocumento_Codigo[0];
               A836LoteArquivoAnexo_Data = P005V2_A836LoteArquivoAnexo_Data[0];
               A838LoteArquivoAnexo_Arquivo = P005V2_A838LoteArquivoAnexo_Arquivo[0];
               n838LoteArquivoAnexo_Arquivo = P005V2_n838LoteArquivoAnexo_Arquivo[0];
               AV21GXLvl6 = 1;
               A838LoteArquivoAnexo_Arquivo = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_arquivo;
               n838LoteArquivoAnexo_Arquivo = false;
               A837LoteArquivoAnexo_Descricao = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_descricao;
               n837LoteArquivoAnexo_Descricao = false;
               if ( (0==AV12Sdt_Arquivo.gxTpr_Tipodocumento_codigo) )
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               else
               {
                  A645TipoDocumento_Codigo = AV12Sdt_Arquivo.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               /* Using cursor P005V3 */
               A840LoteArquivoAnexo_TipoArq = FileUtil.GetFileType( A838LoteArquivoAnexo_Arquivo);
               n840LoteArquivoAnexo_TipoArq = false;
               A839LoteArquivoAnexo_NomeArq = FileUtil.GetFileName( A838LoteArquivoAnexo_Arquivo);
               n839LoteArquivoAnexo_NomeArq = false;
               pr_default.execute(1, new Object[] {n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV21GXLvl6 == 0 )
            {
               /*
                  INSERT RECORD ON TABLE LoteArquivoAnexo

               */
               A836LoteArquivoAnexo_Data = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_data;
               A838LoteArquivoAnexo_Arquivo = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_arquivo;
               n838LoteArquivoAnexo_Arquivo = false;
               A839LoteArquivoAnexo_NomeArq = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_nomearq;
               n839LoteArquivoAnexo_NomeArq = false;
               A840LoteArquivoAnexo_TipoArq = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_tipoarq;
               n840LoteArquivoAnexo_TipoArq = false;
               A837LoteArquivoAnexo_Descricao = AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_descricao;
               n837LoteArquivoAnexo_Descricao = false;
               if ( (0==AV12Sdt_Arquivo.gxTpr_Tipodocumento_codigo) )
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               else
               {
                  A645TipoDocumento_Codigo = AV12Sdt_Arquivo.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               /* Using cursor P005V4 */
               A839LoteArquivoAnexo_NomeArq = FileUtil.GetFileName( A838LoteArquivoAnexo_Arquivo);
               n839LoteArquivoAnexo_NomeArq = false;
               A840LoteArquivoAnexo_TipoArq = FileUtil.GetFileType( A838LoteArquivoAnexo_Arquivo);
               n840LoteArquivoAnexo_TipoArq = false;
               pr_default.execute(2, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
               if ( (pr_default.getStatus(2) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV16LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
               AV17LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
               /* Optimized UPDATE. */
               /* Using cursor P005V5 */
               A839LoteArquivoAnexo_NomeArq = FileUtil.GetFileName( A838LoteArquivoAnexo_Arquivo);
               n839LoteArquivoAnexo_NomeArq = false;
               A840LoteArquivoAnexo_TipoArq = FileUtil.GetFileType( A838LoteArquivoAnexo_Arquivo);
               n840LoteArquivoAnexo_TipoArq = false;
               pr_default.execute(3, new Object[] {n839LoteArquivoAnexo_NomeArq, AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_nomearq, n840LoteArquivoAnexo_TipoArq, AV12Sdt_Arquivo.gxTpr_Contagemresultadoevidencia_tipoarq, AV16LoteArquivoAnexo_LoteCod, AV17LoteArquivoAnexo_Data});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
               dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
               /* End optimized UPDATE. */
            }
            AV20GXV1 = (int)(AV20GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13Sdt_Arquivos = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV14Websession = context.GetSession();
         AV12Sdt_Arquivo = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         scmdbuf = "";
         P005V2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P005V2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         P005V2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         P005V2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P005V2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P005V2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         P005V2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         P005V2_A645TipoDocumento_Codigo = new int[1] ;
         P005V2_n645TipoDocumento_Codigo = new bool[] {false} ;
         P005V2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P005V2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         P005V2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         A840LoteArquivoAnexo_TipoArq = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A837LoteArquivoAnexo_Descricao = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A838LoteArquivoAnexo_Arquivo = "";
         Gx_emsg = "";
         AV17LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newanexolote__default(),
            new Object[][] {
                new Object[] {
               P005V2_A841LoteArquivoAnexo_LoteCod, P005V2_A840LoteArquivoAnexo_TipoArq, P005V2_n840LoteArquivoAnexo_TipoArq, P005V2_A839LoteArquivoAnexo_NomeArq, P005V2_n839LoteArquivoAnexo_NomeArq, P005V2_A837LoteArquivoAnexo_Descricao, P005V2_n837LoteArquivoAnexo_Descricao, P005V2_A645TipoDocumento_Codigo, P005V2_n645TipoDocumento_Codigo, P005V2_A836LoteArquivoAnexo_Data,
               P005V2_A838LoteArquivoAnexo_Arquivo, P005V2_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV21GXLvl6 ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int AV20GXV1 ;
      private int A645TipoDocumento_Codigo ;
      private int GX_INS103 ;
      private int AV16LoteArquivoAnexo_LoteCod ;
      private String scmdbuf ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String Gx_emsg ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private DateTime AV17LoteArquivoAnexo_Data ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private IGxSession AV14Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_LoteArquivoAnexo_LoteCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005V2_A841LoteArquivoAnexo_LoteCod ;
      private String[] P005V2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] P005V2_n840LoteArquivoAnexo_TipoArq ;
      private String[] P005V2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P005V2_n839LoteArquivoAnexo_NomeArq ;
      private String[] P005V2_A837LoteArquivoAnexo_Descricao ;
      private bool[] P005V2_n837LoteArquivoAnexo_Descricao ;
      private int[] P005V2_A645TipoDocumento_Codigo ;
      private bool[] P005V2_n645TipoDocumento_Codigo ;
      private DateTime[] P005V2_A836LoteArquivoAnexo_Data ;
      private String[] P005V2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] P005V2_n838LoteArquivoAnexo_Arquivo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV13Sdt_Arquivos ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV12Sdt_Arquivo ;
   }

   public class prc_newanexolote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005V2 ;
          prmP005V2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Sdt__1Contagemresultadoev",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12Sdt__2Contagemresultadoev",SqlDbType.Char,10,0}
          } ;
          Object[] prmP005V3 ;
          prmP005V3 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP005V4 ;
          prmP005V4 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005V5 ;
          prmP005V5 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV16LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005V2", "SELECT [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [LoteArquivoAnexo_Descricao], [TipoDocumento_Codigo], [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (UPDLOCK) WHERE ([LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod) AND ([LoteArquivoAnexo_NomeArq] = @AV12Sdt__1Contagemresultadoev) AND ([LoteArquivoAnexo_TipoArq] = @AV12Sdt__2Contagemresultadoev) ORDER BY [LoteArquivoAnexo_LoteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005V2,1,0,true,false )
             ,new CursorDef("P005V3", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_Arquivo]=@LoteArquivoAnexo_Arquivo, [LoteArquivoAnexo_Descricao]=@LoteArquivoAnexo_Descricao, [TipoDocumento_Codigo]=@TipoDocumento_Codigo, [LoteArquivoAnexo_TipoArq]=@LoteArquivoAnexo_TipoArq, [LoteArquivoAnexo_NomeArq]=@LoteArquivoAnexo_NomeArq  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005V3)
             ,new CursorDef("P005V4", "INSERT INTO [LoteArquivoAnexo]([LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Arquivo], [LoteArquivoAnexo_NomeArq], [LoteArquivoAnexo_TipoArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_Verificador]) VALUES(@LoteArquivoAnexo_LoteCod, @LoteArquivoAnexo_Data, @LoteArquivoAnexo_Descricao, @LoteArquivoAnexo_Arquivo, @LoteArquivoAnexo_NomeArq, @LoteArquivoAnexo_TipoArq, @TipoDocumento_Codigo, '')", GxErrorMask.GX_NOMASK,prmP005V4)
             ,new CursorDef("P005V5", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_NomeArq]=@LoteArquivoAnexo_NomeArq, [LoteArquivoAnexo_TipoArq]=@LoteArquivoAnexo_TipoArq  WHERE [LoteArquivoAnexo_LoteCod] = @AV16LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] = @AV17LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005V5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[10])[0] = rslt.getBLOBFile(7, rslt.getString(2, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameterDatetime(7, (DateTime)parms[11]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[11]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameterDatetime(4, (DateTime)parms[5]);
                return;
       }
    }

 }

}
