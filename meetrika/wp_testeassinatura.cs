/*
               File: WP_TesteAssinatura
        Description: Teste de Assinatura
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:7:19.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_testeassinatura : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_testeassinatura( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_testeassinatura( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_PathCrtf )
      {
         this.AV14PathCrtf = aP0_PathCrtf;
         executePrivate();
         aP0_PathCrtf=this.AV14PathCrtf;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV14PathCrtf = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PathCrtf", AV14PathCrtf);
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAQD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTQD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311971926");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_testeassinatura.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV14PathCrtf))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMSG", StringUtil.RTrim( Gx_msg));
         GxWebStd.gx_hidden_field( context, "vPATHCRTF", AV14PathCrtf);
         GxWebStd.gx_hidden_field( context, "vCERTIFICATE", AV6Certificate);
         GXCCtlgxBlob = "vDOCUMENTO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV11Documento);
         GXCCtlgxBlob = "vCERTIFICADO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV10Certificado);
         GxWebStd.gx_hidden_field( context, "vDOCUMENTO_Filename", StringUtil.RTrim( edtavDocumento_Filename));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEQD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTQD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_testeassinatura.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV14PathCrtf)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_TesteAssinatura" ;
      }

      public override String GetPgmdesc( )
      {
         return "Teste de Assinatura" ;
      }

      protected void WBQD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_QD2( true) ;
         }
         else
         {
            wb_table1_2_QD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QD2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNomereal_Internalname, AV18NomeReal, StringUtil.RTrim( context.localUtil.Format( AV18NomeReal, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNomereal_Jsonclick, 0, "Attribute", "", "", "", 1, edtavNomereal_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_TesteAssinatura.htm");
         }
         wbLoad = true;
      }

      protected void STARTQD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Teste de Assinatura", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQD0( ) ;
      }

      protected void WSQD2( )
      {
         STARTQD2( ) ;
         EVTQD2( ) ;
      }

      protected void EVTQD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11QD2 */
                              E11QD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'TESTARCLIENTE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12QD2 */
                              E12QD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'TESTARSERVER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13QD2 */
                              E13QD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'MOSTRAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14QD2 */
                              E14QD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15QD2 */
                              E15QD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAQD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavDocumento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV24Pgmname = "WP_TesteAssinatura";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Pgmname", AV24Pgmname);
         context.Gx_err = 0;
         edtavNomereal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNomereal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNomereal_Enabled), 5, 0)));
      }

      protected void RFQD2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15QD2 */
            E15QD2 ();
            WBQD0( ) ;
         }
      }

      protected void STRUPQD0( )
      {
         /* Before Start, stand alone formulas. */
         AV24Pgmname = "WP_TesteAssinatura";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Pgmname", AV24Pgmname);
         context.Gx_err = 0;
         edtavNomereal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNomereal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNomereal_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11QD2 */
         E11QD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV11Documento = cgiGet( edtavDocumento_Internalname);
            AV10Certificado = cgiGet( edtavCertificado_Internalname);
            AV7CertificatePwd = cgiGet( edtavCertificatepwd_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CertificatePwd", AV7CertificatePwd);
            AV18NomeReal = cgiGet( edtavNomereal_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NomeReal", AV18NomeReal);
            /* Read saved values. */
            edtavDocumento_Filename = cgiGet( "vDOCUMENTO_Filename");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Documento)) )
            {
               GXCCtlgxBlob = "vDOCUMENTO" + "_gxBlob";
               AV11Documento = cgiGet( GXCCtlgxBlob);
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Certificado)) )
            {
               GXCCtlgxBlob = "vCERTIFICADO" + "_gxBlob";
               AV10Certificado = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11QD2 */
         E11QD2 ();
         if (returnInSub) return;
      }

      protected void E11QD2( )
      {
         /* Start Routine */
         edtavCertificado_Display = 1;
         edtavDocumento_Display = 1;
         new getserverpath(context ).execute( ref  AV24Pgmname, ref  AV20AbsolutePath) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Pgmname", AV24Pgmname);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AbsolutePath", AV20AbsolutePath);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV14PathCrtf)) )
         {
            /* Using cursor H00QD2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A330ParametrosSistema_Codigo = H00QD2_A330ParametrosSistema_Codigo[0];
               A1021ParametrosSistema_PathCrtf = H00QD2_A1021ParametrosSistema_PathCrtf[0];
               n1021ParametrosSistema_PathCrtf = H00QD2_n1021ParametrosSistema_PathCrtf[0];
               AV14PathCrtf = A1021ParametrosSistema_PathCrtf;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PathCrtf", AV14PathCrtf);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV14PathCrtf)) )
         {
            Gx_msg = "Deve configurar nos par�metros do Sistema o caminho de Certifica��o!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
         }
         else
         {
            if ( ! ( ( StringUtil.StrCmp(StringUtil.Substring( AV14PathCrtf, 2, 1), ":") == 0 ) || ( StringUtil.StrCmp(StringUtil.Substring( AV14PathCrtf, 1, 2), "\\\\") == 0 ) ) )
            {
               AV14PathCrtf = AV20AbsolutePath + AV14PathCrtf;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PathCrtf", AV14PathCrtf);
            }
         }
         /* Using cursor H00QD3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1Usuario_Codigo = H00QD3_A1Usuario_Codigo[0];
            A1017Usuario_CrtfPath = H00QD3_A1017Usuario_CrtfPath[0];
            n1017Usuario_CrtfPath = H00QD3_n1017Usuario_CrtfPath[0];
            if ( ( StringUtil.StrCmp(StringUtil.Substring( A1017Usuario_CrtfPath, 2, 1), ":") == 0 ) || ( StringUtil.StrCmp(StringUtil.Substring( A1017Usuario_CrtfPath, 1, 2), "\\\\") == 0 ) )
            {
               AV6Certificate = A1017Usuario_CrtfPath;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
            }
            else
            {
               AV6Certificate = AV20AbsolutePath + A1017Usuario_CrtfPath;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Certificate", AV6Certificate);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E12QD2( )
      {
         /* 'TestarCliente' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Documento)) )
         {
            GX_msglist.addItem("Selecione um documento para ser assinado!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Certificado)) )
         {
            GX_msglist.addItem("Seleceione o arquivo com a assinatura digital!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7CertificatePwd)) )
         {
            GX_msglist.addItem("Informe a senha do certificado digital!");
         }
         else
         {
            AV15Doc = AV11Documento;
            AV16Cert = AV10Certificado;
            AV21i = (short)(StringUtil.StringSearchRev( AV15Doc, "\\", -1));
            AV8signatureResult = StringUtil.Substring( AV15Doc, 1, AV21i) + "DA" + StringUtil.Substring( AV15Doc, AV21i+1, 200);
            Gx_msg = AV13ServerSideSign.signdocument("digitalSign.PDFDigitalSign", AV16Cert, AV7CertificatePwd, "", AV15Doc, AV8signatureResult);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
         }
         GX_msglist.addItem(Gx_msg);
      }

      protected void E13QD2( )
      {
         /* 'TestarServer' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Documento)) )
         {
            GX_msglist.addItem("Selecione um documento para ser assinado!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7CertificatePwd)) )
         {
            GX_msglist.addItem("Informe a senha do certificado digital!");
         }
         else
         {
            AV17File.Source = AV11Documento;
            AV12FileName = edtavDocumento_Filename;
            AV9unsignedDocument = AV14PathCrtf + AV12FileName;
            AV8signatureResult = AV14PathCrtf + "DA" + AV12FileName;
            if ( AV17File.Exists() )
            {
               AV17File.Rename(AV9unsignedDocument);
               AV17File.Rename(AV18NomeReal);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NomeReal", AV18NomeReal);
            }
            Gx_msg = AV13ServerSideSign.signdocument("digitalSign.PDFDigitalSign", AV6Certificate, AV7CertificatePwd, "", AV18NomeReal, AV8signatureResult);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
         }
         GX_msglist.addItem(Gx_msg);
      }

      protected void E14QD2( )
      {
         /* 'Mostrar' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Documento)) )
         {
            GX_msglist.addItem("Selecione um documento para ser assinado!");
         }
         else
         {
            AV15Doc = AV11Documento;
            AV16Cert = AV10Certificado;
            AV12FileName = edtavDocumento_Filename;
            AV9unsignedDocument = AV14PathCrtf + AV12FileName;
            AV18NomeReal = AV14PathCrtf + AV12FileName;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NomeReal", AV18NomeReal);
            GX_msglist.addItem(AV15Doc);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16Cert)) )
            {
               GX_msglist.addItem("Certificado local n�o selecionado!");
            }
            else
            {
               GX_msglist.addItem(AV16Cert);
            }
            GX_msglist.addItem(AV14PathCrtf+edtavDocumento_Filename);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV6Certificate)) )
            {
               GX_msglist.addItem("Certificado no servidor n�o encontrado!");
            }
            else
            {
               GX_msglist.addItem(AV6Certificate);
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E15QD2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_QD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Documento", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            edtavDocumento_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDocumento_Internalname, "Filetype", edtavDocumento_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11Documento)) )
            {
               gxblobfileaux.Source = AV11Documento;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavDocumento_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavDocumento_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV11Documento = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDocumento_Internalname, "URL", context.PathToRelativeUrl( AV11Documento));
                  edtavDocumento_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDocumento_Internalname, "Filetype", edtavDocumento_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDocumento_Internalname, "URL", context.PathToRelativeUrl( AV11Documento));
            }
            GxWebStd.gx_blob_field( context, edtavDocumento_Internalname, StringUtil.RTrim( AV11Documento), context.PathToRelativeUrl( AV11Documento), (String.IsNullOrEmpty(StringUtil.RTrim( edtavDocumento_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavDocumento_Filetype)) ? AV11Documento : edtavDocumento_Filetype)) : edtavDocumento_Contenttype), false, "", edtavDocumento_Parameters, edtavDocumento_Display, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavDocumento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", "", "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Certificado", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            edtavCertificado_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificado_Internalname, "Filetype", edtavCertificado_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Certificado)) )
            {
               gxblobfileaux.Source = AV10Certificado;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavCertificado_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavCertificado_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV10Certificado = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificado_Internalname, "URL", context.PathToRelativeUrl( AV10Certificado));
                  edtavCertificado_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificado_Internalname, "Filetype", edtavCertificado_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCertificado_Internalname, "URL", context.PathToRelativeUrl( AV10Certificado));
            }
            GxWebStd.gx_blob_field( context, edtavCertificado_Internalname, StringUtil.RTrim( AV10Certificado), context.PathToRelativeUrl( AV10Certificado), (String.IsNullOrEmpty(StringUtil.RTrim( edtavCertificado_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavCertificado_Filetype)) ? AV10Certificado : edtavCertificado_Filetype)) : edtavCertificado_Contenttype), false, "", edtavCertificado_Parameters, edtavCertificado_Display, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavCertificado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", "", "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Senha", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCertificatepwd_Internalname, AV7CertificatePwd, StringUtil.RTrim( context.localUtil.Format( AV7CertificatePwd, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCertificatepwd_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, -1, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnCheck";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttMostrar_Internalname, "", "Mostrar", bttMostrar_Jsonclick, 5, "Mostrar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'MOSTRAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton3_Internalname, "", "Testar Cliente", bttButton3_Jsonclick, 5, "Testar Cliente", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'TESTARCLIENTE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton4_Internalname, "", "Testar Server", bttButton4_Jsonclick, 5, "Testar Server", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'TESTARSERVER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_TesteAssinatura.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QD2e( true) ;
         }
         else
         {
            wb_table1_2_QD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV14PathCrtf = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PathCrtf", AV14PathCrtf);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQD2( ) ;
         WSQD2( ) ;
         WEQD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311971952");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_testeassinatura.js", "?2020311971952");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDocumento_Internalname = "vDOCUMENTO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavCertificado_Internalname = "vCERTIFICADO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavCertificatepwd_Internalname = "vCERTIFICATEPWD";
         bttMostrar_Internalname = "MOSTRAR";
         bttButton3_Internalname = "BUTTON3";
         bttButton4_Internalname = "BUTTON4";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         edtavNomereal_Internalname = "vNOMEREAL";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCertificatepwd_Jsonclick = "";
         edtavCertificado_Jsonclick = "";
         edtavCertificado_Parameters = "";
         edtavCertificado_Contenttype = "";
         edtavCertificado_Filetype = "";
         edtavCertificado_Display = 0;
         edtavDocumento_Jsonclick = "";
         edtavDocumento_Parameters = "";
         edtavDocumento_Contenttype = "";
         edtavDocumento_Filetype = "";
         edtavDocumento_Display = 0;
         edtavNomereal_Jsonclick = "";
         edtavNomereal_Enabled = 1;
         edtavDocumento_Filename = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Teste de Assinatura";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'TESTARCLIENTE'","{handler:'E12QD2',iparms:[{av:'AV11Documento',fld:'vDOCUMENTO',pic:'',nv:''},{av:'AV10Certificado',fld:'vCERTIFICADO',pic:'',nv:''},{av:'AV7CertificatePwd',fld:'vCERTIFICATEPWD',pic:'',nv:''},{av:'Gx_msg',fld:'vMSG',pic:'',nv:''}],oparms:[{av:'Gx_msg',fld:'vMSG',pic:'',nv:''}]}");
         setEventMetadata("'TESTARSERVER'","{handler:'E13QD2',iparms:[{av:'AV11Documento',fld:'vDOCUMENTO',pic:'',nv:''},{av:'AV7CertificatePwd',fld:'vCERTIFICATEPWD',pic:'',nv:''},{av:'edtavDocumento_Filename',ctrl:'vDOCUMENTO',prop:'Filename'},{av:'AV14PathCrtf',fld:'vPATHCRTF',pic:'',nv:''},{av:'AV18NomeReal',fld:'vNOMEREAL',pic:'',nv:''},{av:'AV6Certificate',fld:'vCERTIFICATE',pic:'',nv:''},{av:'Gx_msg',fld:'vMSG',pic:'',nv:''}],oparms:[{av:'Gx_msg',fld:'vMSG',pic:'',nv:''}]}");
         setEventMetadata("'MOSTRAR'","{handler:'E14QD2',iparms:[{av:'AV11Documento',fld:'vDOCUMENTO',pic:'',nv:''},{av:'AV10Certificado',fld:'vCERTIFICADO',pic:'',nv:''},{av:'edtavDocumento_Filename',ctrl:'vDOCUMENTO',prop:'Filename'},{av:'AV14PathCrtf',fld:'vPATHCRTF',pic:'',nv:''},{av:'AV6Certificate',fld:'vCERTIFICATE',pic:'',nv:''}],oparms:[{av:'AV18NomeReal',fld:'vNOMEREAL',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV14PathCrtf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Gx_msg = "";
         AV6Certificate = "";
         GXCCtlgxBlob = "";
         AV11Documento = "";
         AV10Certificado = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV18NomeReal = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV24Pgmname = "";
         AV7CertificatePwd = "";
         AV20AbsolutePath = "";
         scmdbuf = "";
         H00QD2_A330ParametrosSistema_Codigo = new int[1] ;
         H00QD2_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         H00QD2_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         A1021ParametrosSistema_PathCrtf = "";
         H00QD3_A1Usuario_Codigo = new int[1] ;
         H00QD3_A1017Usuario_CrtfPath = new String[] {""} ;
         H00QD3_n1017Usuario_CrtfPath = new bool[] {false} ;
         A1017Usuario_CrtfPath = "";
         AV15Doc = "";
         AV16Cert = "";
         AV8signatureResult = "";
         AV13ServerSideSign = new SdtServerSideSign(context);
         AV17File = new GxFile(context.GetPhysicalPath());
         AV12FileName = "";
         AV9unsignedDocument = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock2_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock1_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         bttMostrar_Jsonclick = "";
         bttButton3_Jsonclick = "";
         bttButton4_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_testeassinatura__default(),
            new Object[][] {
                new Object[] {
               H00QD2_A330ParametrosSistema_Codigo, H00QD2_A1021ParametrosSistema_PathCrtf, H00QD2_n1021ParametrosSistema_PathCrtf
               }
               , new Object[] {
               H00QD3_A1Usuario_Codigo, H00QD3_A1017Usuario_CrtfPath, H00QD3_n1017Usuario_CrtfPath
               }
            }
         );
         AV24Pgmname = "WP_TesteAssinatura";
         /* GeneXus formulas. */
         AV24Pgmname = "WP_TesteAssinatura";
         context.Gx_err = 0;
         edtavNomereal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short edtavCertificado_Display ;
      private short edtavDocumento_Display ;
      private short AV21i ;
      private short nGXWrapped ;
      private int edtavNomereal_Enabled ;
      private int A330ParametrosSistema_Codigo ;
      private int A1Usuario_Codigo ;
      private int idxLst ;
      private String edtavDocumento_Filename ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gx_msg ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavNomereal_Internalname ;
      private String edtavNomereal_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDocumento_Internalname ;
      private String AV24Pgmname ;
      private String edtavCertificado_Internalname ;
      private String edtavCertificatepwd_Internalname ;
      private String AV20AbsolutePath ;
      private String scmdbuf ;
      private String AV12FileName ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavDocumento_Filetype ;
      private String edtavDocumento_Contenttype ;
      private String edtavDocumento_Parameters ;
      private String edtavDocumento_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavCertificado_Filetype ;
      private String edtavCertificado_Contenttype ;
      private String edtavCertificado_Parameters ;
      private String edtavCertificado_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavCertificatepwd_Jsonclick ;
      private String bttMostrar_Internalname ;
      private String bttMostrar_Jsonclick ;
      private String bttButton3_Internalname ;
      private String bttButton3_Jsonclick ;
      private String bttButton4_Internalname ;
      private String bttButton4_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool n1017Usuario_CrtfPath ;
      private String AV14PathCrtf ;
      private String wcpOAV14PathCrtf ;
      private String AV6Certificate ;
      private String AV18NomeReal ;
      private String AV7CertificatePwd ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String A1017Usuario_CrtfPath ;
      private String AV15Doc ;
      private String AV16Cert ;
      private String AV8signatureResult ;
      private String AV9unsignedDocument ;
      private String AV11Documento ;
      private String AV10Certificado ;
      private GxFile gxblobfileaux ;
      private SdtServerSideSign AV13ServerSideSign ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_PathCrtf ;
      private IDataStoreProvider pr_default ;
      private int[] H00QD2_A330ParametrosSistema_Codigo ;
      private String[] H00QD2_A1021ParametrosSistema_PathCrtf ;
      private bool[] H00QD2_n1021ParametrosSistema_PathCrtf ;
      private int[] H00QD3_A1Usuario_Codigo ;
      private String[] H00QD3_A1017Usuario_CrtfPath ;
      private bool[] H00QD3_n1017Usuario_CrtfPath ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxFile AV17File ;
      private GXWebForm Form ;
   }

   public class wp_testeassinatura__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QD2 ;
          prmH00QD2 = new Object[] {
          } ;
          Object[] prmH00QD3 ;
          prmH00QD3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QD2", "SELECT TOP 1 [ParametrosSistema_Codigo], [ParametrosSistema_PathCrtf] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QD2,1,0,false,true )
             ,new CursorDef("H00QD3", "SELECT TOP 1 [Usuario_Codigo], [Usuario_CrtfPath] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = 12 ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QD3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
